//

//#include <entry.dl>
//#include <next-state-slicing-path-insensitive.dl>

//each function's address range (lower to upper bound)
.decl addr_func(rgn:Func, addr_lower:number, addr_upper:number)
.input addr_func

//in the same basic block => n:address, m:next_address
.decl flow(n:number, m:number)
.input flow

//different basic blocks => n:address (the last instruction of the BB), m:address_successor
.decl edge(n:number, m:number)
.input edge

/*********** Four kinds of edges *******************/
//both addresses in the same rgn, just different BB
.decl intra_edge(addr1:number, addr2:number)

//call -> enter edge
.decl call_edge(addr_caller:number, addr_callee:number)

//exit -> end-call edge
.decl return_edge(addr_callee:number, addr_caller:number)

.decl inter_succ(addr_caller_enter:number, addr_caller_after:number)

.decl call_enter_for_lib(addr_caller_enter:number)

/***************************************************/

/************ Generate the sets of edges, these sets are disjoint ************/

//1. intra_edge
intra_edge(addr1, addr2) :-
  addr_func(func, addr_lower, addr_upper),
  !compiler_build_in(func),
  !lib_func(func,_),
  flow(addr1, addr2),
  addr_lower <= addr1, addr1 < addr_upper,
  addr_lower < addr2, addr2 < addr_upper. //Note: addr2=addr_lower means a recursive call, should be a call_edge, but not intra_edge. 

intra_edge(addr1, addr2) :-
  addr_func(func, addr_lower, addr_upper),
  !compiler_build_in(func),
  !lib_func(func,_),
  edge(addr1, addr2),
  //!return_edge(addr1, addr2), //there may be return_edge for recursive function that points from the ending to the middle of function
  (!return_instruction_pattern(addr1); addr2 = 0),
  addr_lower <= addr1, addr1 < addr_upper,
  addr_lower < addr2, addr2 < addr_upper. //Note: addr2=addr_lower means a recursive call, should be a call_edge, but not intra_edge.

//2. inter_succ: totally generated at Ocaml level
.input inter_succ

//3. call_edge, pattern-matching RTL instructions to detect call_edge
/*Pattern 1*/
call_edge(addr_caller, addr_callee) :-
  edge(addr_caller, addr_callee),
  addr_func(func_step_into, addr_callee, _),
  !lib_func(func_step_into,_), //distinguish library function in has_val
  !compiler_build_in(func_step_into),

  set_mem_rtl(addr_caller, 1, 4, PC, ESP_minus_4), // ESP - 4 <-- PC
  get_loc_rtl_exp(PC, "PC"),
  arith_rtl_exp(ESP_minus_4, _, "-", ESP, INT_4),
  get_loc_rtl_exp(ESP, "ESP"),
  imm_rtl_exp(INT_4, _, 4),

  set_loc_rtl(addr_caller, 2, _, ESP_minus_4, ESP), // ESP <-- ESP - 4
  set_loc_rtl(addr_caller, 3, _, PC_arith, PC), // PC <-- PC + ?
  arith_rtl_exp(PC_arith, _, _, _, _),
  addr_callee != 0. // the last return of main does not have to be modeled as return_edge
  //cfg_since_entry(addr_caller, _).

/*Pattern 2*/
call_edge(addr_caller, addr_callee) :-
  edge(addr_caller, addr_callee),
  addr_func(func_step_into, addr_callee, _),
  !lib_func(func_step_into,_),
  !compiler_build_in(func_step_into),

  set_mem_rtl(addr_prev, 1, 4, _, ESP_minus_4), // ESP - 4 <-- (*)
  set_loc_rtl(addr_prev, 2, _, ESP_minus_4, ESP), // ESP <-- ESP - 4
  arith_rtl_exp(ESP_minus_4, _, "-", ESP, INT_4),
  get_loc_rtl_exp(ESP, "ESP"),
  imm_rtl_exp(INT_4, _, 4),

  set_loc_rtl(addr_caller, 1, _, PC_arith, PC), // PC <-- PC + ?
  get_loc_rtl_exp(PC, "PC"),
  arith_rtl_exp(PC_arith, _, _, _, _),

  //next_state(addr_prev, 2, addr_caller, 1), // call_edge can also depend on next_state
  max_order(addr_prev, 2),
  edge(addr_prev, addr_caller),
  
  addr_callee != 0.
  //cfg_since_entry(addr_caller, 1).

.output call_edge

call_enter_for_lib(addr_caller) :-
  edge(addr_caller, addr_callee),
  addr_func(func_step_into, addr_callee, _),
  (lib_func(func_step_into,_); compiler_build_in(func_step_into)),

  set_mem_rtl(addr_caller, 1, 4, PC, ESP_minus_4), // ESP - 4 <-- PC
  get_loc_rtl_exp(PC, "PC"),
  arith_rtl_exp(ESP_minus_4, _, "-", ESP, INT_4),
  get_loc_rtl_exp(ESP, "ESP"),
  imm_rtl_exp(INT_4, _, 4),

  set_loc_rtl(addr_caller, 2, _, ESP_minus_4, ESP), // ESP <-- ESP - 4
  set_loc_rtl(addr_caller, 3, _, PC_arith, PC), // PC <-- PC + ?
  arith_rtl_exp(PC_arith, _, _, _, _),
  addr_callee != 0.

call_enter_for_lib(addr_caller) :-
  edge(addr_caller, addr_callee),
  addr_func(func_step_into, addr_callee, _),
  (lib_func(func_step_into,_); compiler_build_in(func_step_into)),

  set_mem_rtl(addr_prev, 1, 4, _, ESP_minus_4), // ESP - 4 <-- (*)
  set_loc_rtl(addr_prev, 2, _, ESP_minus_4, ESP), // ESP <-- ESP - 4
  arith_rtl_exp(ESP_minus_4, _, "-", ESP, INT_4),
  get_loc_rtl_exp(ESP, "ESP"),
  imm_rtl_exp(INT_4, _, 4),

  set_loc_rtl(addr_caller, 1, _, PC_arith, PC), // PC <-- PC + ?
  get_loc_rtl_exp(PC, "PC"),
  arith_rtl_exp(PC_arith, _, _, _, _),

  max_order(addr_prev, 2),
  edge(addr_prev, addr_caller),
  
  addr_callee != 0.

.output call_enter_for_lib

//4. return_edge
.decl return_instruction_pattern(addr_callee: number)

return_instruction_pattern(addr_callee) :-
  set_loc_rtl(addr_callee, 1, _, mem_ESP, PC), // PC <-- *(ESP)
  get_mem_rtl_exp(mem_ESP, _, ESP),
  get_loc_rtl_exp(ESP, "ESP"),

  get_loc_rtl_exp(PC, "PC"),

  set_loc_rtl(addr_callee, 2, _, ESP_plus_4, ESP), // ESP <-- (ESP + 4)
  arith_rtl_exp(ESP_plus_4, _, "+", ESP, INT_4),
  imm_rtl_exp(INT_4, _, 4).

return_edge(addr_callee, addr_caller) :-
  edge(addr_callee, addr_caller),
  return_instruction_pattern(addr_callee),
  addr_caller != 0.

return_edge(addr_ret_from, addr_ret_to) :-
  call_edge(addr_call_from, addr_call_to),
  addr_func(func, addr_call_to, func_end),
  !lib_func(func,_),
  !compiler_build_in(func),
  return_instruction_pattern(addr_ret_from),
  addr_call_to <= addr_ret_from, addr_ret_from < func_end,
  inter_succ(addr_call_from, addr_ret_to).  

.output return_edge

/**************************************************************************************/
/* Define the next_state_helper used in the inter-procedural analysis. The "next_state" will form two paths:
1) (addr_caller_enter, max_order1) --> (addr_callee_start, 0) --> (addr_callee_start, 1) --> ... -->
   (addr_callee_end, max_order2) --> (addr_caller_after, 0) --> (addr_caller_after, 1) --> ...
2) (addr_caller_enter, max_order1) --> (addr_caller_after, 1)
We provide has_val rules for each transition in these paths. The second path is only used to skip the library functions in the inter-procedural analysis. 
*/
next_state_helper(addr_prev, order_prev, addr_next, 0) :-
  max_order(addr_prev, order_prev),
  call_edge(addr_prev, addr_next).

next_state_helper(addr_callee, 0, addr_callee, 1) :-
  call_edge(_, addr_callee).

next_state_helper(addr_prev, order_prev, addr_next, 0) :-
  max_order(addr_prev, order_prev),
  return_edge(addr_prev, addr_next).

next_state_helper(addr_caller, 0, addr_caller, 1) :-
  return_edge(_, addr_caller).


/* For the calling to the library functions, there will be NO return_edge back to the current procedure.
This rule helps to ensure the points-to analysis can go along the inter_succ edge to after the calling of library function */
next_state_helper(addr_caller_enter, order_prev, addr_caller_after, 1) :- 
  //order_helper(addr_caller_enter, order_prev),
  max_order(addr_caller_enter, order_prev),
  order_helper(addr_caller_after, 1),

  /*call_edge(addr_caller_enter, addr_callee),
  addr_func(func_step_into, addr_callee, _),
  lib_func(func_step_into, _),*/
  call_enter_for_lib(addr_caller_enter),

  inter_succ(addr_caller_enter, addr_caller_after).


/***************************************************************************************/

//to make order connected => e.g. (0,1), (1,2), (2,3) ... 
.decl order_conn(n:number, m:number)
.input order_conn

////// to find out maximum_order of each instruction (address) //// 
.decl max_order(addr:number, order:number)
.decl order_helper(addr:number, order:number)

order_helper(addr, order) :-
  set_loc_rtl(addr, order, _, _, _).

order_helper(addr, order) :-
  set_ps_reg_rtl(addr, order, _, _, _).

order_helper(addr, order) :-
  set_array_rtl(addr, order, _, _, _, _).

order_helper(addr, order) :-
  set_byte_rtl(addr, order, _, _).

order_helper(addr, order) :-
  set_mem_rtl(addr, order, _, _, _).

order_helper(addr, order) :-
  advance_oracle_rtl(addr, order).

order_helper(addr, order) :-
  if_rtl(addr, order, _, _).

order_helper(addr, order) :-
  error_rtl(addr, order).

order_helper(addr, order) :-
  trap_rtl(addr, order).

max_order(addr, order) :-
  order_helper(addr, _),
  order = max O : order_helper(addr, O).

/* Define the states that can be reached from the entry point. Helps to avoid traverse the ast unrelated to the points-to analysis*/
.decl exit_address(addr:number)

exit_address(addr_ret_from) :-
  entry(entry_func, addr_lower),
  addr_func(entry_func, addr_lower, addr_upper),
  return_instruction_pattern(addr_ret_from),
  addr_lower <= addr_ret_from, addr_ret_from < addr_upper.

.output exit_address

.decl cfg_since_entry(addr:number, order:number)

cfg_since_entry(addr, 0) :-
  entry(_, addr).

cfg_since_entry(addr_next, order_next) :-
  cfg_since_entry(addr, order),
  next_state(addr, order, addr_next, order_next),
  !exit_address(addr).

.output cfg_since_entry

/* Collect some special address for the ps_reg points-to analysis */
.decl nop_get_ps_reg_address(addr:number, order:number)

nop_get_ps_reg_address(addr, order+2) :-
  set_ps_reg_rtl(addr, order, _, id_eax, num_ps_reg),
  set_loc_rtl(addr, order+1, _, id_eax, id_eax),
  set_loc_rtl(addr, order+2, _, id_ps_reg, id_eax),
  get_ps_reg_rtl_exp(id_ps_reg, _, num_ps_reg),
  get_loc_rtl_exp(id_eax, "EAX"),
  cfg_since_entry(addr, order).

.output nop_get_ps_reg_address

.decl op_get_ps_reg_address(addr:number, order:number)

op_get_ps_reg_address(addr, order) :-
  (set_ps_reg_rtl(addr, order, _, _, _);
   set_byte_rtl(addr, order, _, _);
   set_mem_rtl(addr, order, _, _, _);
   if_rtl(addr, order, _, _);
   set_loc_rtl(addr, order, _, _, id_not_eax), get_loc_rtl_exp(id_not_eax, reg_name), reg_name !="EAX"
  ),
  traverse_ast(addr, order, id),
  get_ps_reg_rtl_exp(id, _, _).
  //!nop_get_ps_reg_address(addr, order).//We cannot use !nop_get_ps_reg_address when edge computation becomes in fixpoint

op_get_ps_reg_address(addr, order) :-
  get_ps_reg_rtl_exp(id, _, _),
  traverse_ast(addr, order, id),
  set_loc_rtl(addr, order, _, _, id_eax),
  get_loc_rtl_exp(id_eax, "EAX"),
  (!set_ps_reg_rtl(addr, order-2, _, id_eax, _);
   !set_loc_rtl(addr, order-1, _, id_eax, id_eax)
  ).

.output op_get_ps_reg_address

/* Control the expression id collected is from the current CFG */
.decl traverse_ast(addr:number, order:number, id:Rtl_exp)

traverse_ast(addr, order, id_new) :-
  cfg_since_entry(addr, order),
  traverse_ast(addr, order, id),
  (arith_rtl_exp(id, _, _, id_new, _);
   arith_rtl_exp(id, _, _, _, id_new)).

traverse_ast(addr, order, id_new) :-
  cfg_since_entry(addr, order),
  traverse_ast(addr, order, id),
  (test_rtl_exp(id, _, _, id_new, _);
   test_rtl_exp(id, _, _, _, id_new)).

traverse_ast(addr, order, id) :-
  cfg_since_entry(addr, order),
  if_rtl(addr, order, id, _).

traverse_ast(addr, order, id) :-
  cfg_since_entry(addr, order),
  set_loc_rtl(addr, order, _, id, _).

traverse_ast(addr, order, id) :-
  cfg_since_entry(addr, order),
  (set_mem_rtl(addr, order, _, id, _);
   set_mem_rtl(addr, order, _, _, id)).

traverse_ast(addr, order, id_new) :-
  cfg_since_entry(addr, order),
  traverse_ast(addr, order, id),
  get_mem_rtl_exp(id, _, id_new).

traverse_ast(addr, order, id_new) :-
  cfg_since_entry(addr, order),
  traverse_ast(addr, order, id),
  get_byte_rtl_exp(id, id_new).

traverse_ast(addr, order, id_new) :-
  cfg_since_entry(addr, order),
  traverse_ast(addr, order, id),
  cast_u_rtl_exp(id, _, _, id_new).

traverse_ast(addr, order, id_new) :-
  cfg_since_entry(addr, order),
  traverse_ast(addr, order, id),
  cast_s_rtl_exp(id, _, _, id_new).

traverse_ast(addr, order, id_new) :-
  cfg_since_entry(addr, order),
  traverse_ast(addr, order, id),
  (if_rtl_exp(id, _, _, id_new, _);
   if_rtl_exp(id, _, _, _, id_new)).


//addr's rgn (helper relation for call & return_edge)
//categorizes which rgn each address in edge belongs to
.decl addr_rgn(addr:number, rgn:MemRgn)
addr_rgn(addr, rgn) :-
  addr_func(rgn, addr_lower, addr_upper),
  edge(addr, _),
  addr_lower <= addr, addr < addr_upper.

addr_rgn(addr, rgn) :-
  addr_func(rgn, addr_lower, addr_upper),
  edge(_, addr),
  addr_lower <= addr, addr < addr_upper.

.output addr_rgn

.decl is_recursive(rgn:MemRgn)
is_recursive(rgn) :-
  return_edge(addr_callee, addr_caller),
  addr_rgn(addr_callee, rgn),
  addr_rgn(addr_caller, rgn).

.output is_recursive


//
.decl get_caller_enter_from_callee_end(addr_callee_end:number, addr_caller_enter:number)

get_caller_enter_from_callee_end(addr_callee_end, addr_caller_enter) :-
  return_edge(addr_callee_end, addr_caller_after),
  call_edge(addr_caller_enter, addr_callee_begin),

  addr_rgn(addr_caller_enter, rgn_caller),
  addr_rgn(addr_caller_after, rgn_caller),

  addr_rgn(addr_callee_begin, rgn_callee),
  addr_rgn(addr_callee_end, rgn_callee).



.output get_caller_enter_from_callee_end

