//




// figuring out equivalence-relations of phi-location in minimization phase
.decl loc_phi_equiv_rel(loc:Loc, func:Func, i:number, j:number) //eqrel


.decl ssa_loc_phi_equiv_class(loc:Loc, func:Func, index:number, equiv_index:number)
.decl ssa_loc_phi_after_min_phase(loc:Loc, index:number, index_func:Func, equiv_index:number, equiv_index_func:Func)




//initialization for loc_phi_equiv_rel  equivalence-relation
//souffle's "eqrel" currently only supports two parameters

//reflexive
loc_phi_equiv_rel(loc, func, i, i) :-
  loc_phi_raw(loc, i, func, _, _).
loc_phi_equiv_rel(loc, func, i, i) :-
  loc_phi_raw(loc, _, _, i, func).

/*
//symmetric
loc_phi_equiv_rel(loc, func, i, j):-
  loc_phi_equiv_rel(loc, func, j, i).

//transitive
loc_phi_equiv_rel(loc, func, i, k):-
  loc_phi_equiv_rel(loc, func, i, j),
  loc_phi_equiv_rel(loc, func, j, k).

*/

///**********************************

//loc_phi_equiv_rel(loc, func, i, j):-
//  loc_phi_raw(loc, i, func, j, func),
//  locPhiLargestID(loc, func, i, 0).

/*

// preprocessing step for location level minimization phase
.decl locIndexOfPred(loc:Loc, func:Func, phi_id:number, phi_pred:number, order:number)
.decl locIndexOfPredHelper(loc:Loc, func:Func, phi_id:number, phi_pred:number, order:number)

.decl locPhiLargestID(loc:Loc, func:Func, phi_id:number, highest_order:number)
.decl locAllPredEqEitherUpTo(loc:Loc, func:Func, i:number, j:number, id:number)
.decl locAllPredEqEither(loc:Loc, func:Func, i:number, j:number)



//helper function creating locIndexOfPred
locIndexOfPredHelper(loc, func, phi_id, i, j) :-
  loc_phi_raw(loc, phi_id, func, i, func),
  loc_phi_raw(loc, phi_id, func, j, func),
  i < j.

// locIndexOfPred - make orders of input-values in phi-functions
// suppose eax_1 = phi(eax_2, eax_3):
// then order 0 is assigned to eax_2
//////  order 1 is assigned to eax_3
locIndexOfPred(loc, func, phi_id, id, 0) :-
  loc_phi_raw(loc, phi_id, func, id, func),
  id = min smallest_id : loc_phi_raw(loc, phi_id, func, smallest_id, func).


locIndexOfPred(loc, func, phi_id, id, counter) :-
  locIndexOfPredHelper(loc, func, phi_id, _, id),
  counter = count:locIndexOfPredHelper(loc, func, phi_id, _, id).



//helper relation - find largest ID of input-value of phi-function
locPhiLargestID(loc, func, i, id):-
  locIndexOfPred(loc, func, i, _, _),
  id = max max_id: locIndexOfPred(loc, func, i, _, max_id).



//with the support of locIndexOfPred,
//locAllPredEqEitherUpTo checks equiv-relativity of IDs one by one (starting from 0 indexed ID)

locAllPredEqEitherUpTo(loc, func, i, j, 0) :-
  loc_phi_raw(loc, i, func, j, func),
  locIndexOfPred(loc, func, i, j2, 0),
  (loc_phi_equiv_rel(loc, func, i, j2); loc_phi_equiv_rel(loc, func, j, j2)).



locAllPredEqEitherUpTo(loc, func, i, j, id + 1) :-
  locAllPredEqEitherUpTo(loc, func, i, j, id),
  locIndexOfPred(loc, func, i, j2, id + 1),
  (loc_phi_equiv_rel(loc, func, i, j2); loc_phi_equiv_rel(loc, func, j, j2)).



/// if all input-values of phi-function are equivalent,
/// we store them to equiv-relation
locAllPredEqEither(loc, func, i, j) :-
  locAllPredEqEitherUpTo(loc, func, i, j, id),
  locPhiLargestID(loc, func, i, id).
*/



//adding to equiv_rel
//loc_phi_equiv_rel(loc, func, i, j) :-
//  locAllPredEqEither(loc, func, i, j).
//******************************************/





/////////////////constructing equiv-classes ////////



///// inter-procedural analysis: need to treat ESP differently for call_enter and return_exit edges
//// also, need to treate EAX and EBP differently for return_exit edges


//ssa_loc_phi_equiv_class(loc, func, index, index) :-
//  loc_phi_equiv_rel(loc, func, index, _).

ssa_loc_phi_after_min_phase(loc, index, index_func, pred, pred_func) :-
  loc_phi_raw(loc, index, index_func, pred, pred_func). 

//ssa_loc_phi_equiv_class(loc, func, index, equiv_index) :-
//  loc_phi_equiv_rel(loc, func, index, _),
//  equiv_index = min min_index : loc_phi_equiv_rel(loc, func, index, min_index).//,
//  //loc != "ESP", loc != "EBP", loc != "EAX".


/*
ssa_loc_phi_equiv_class(loc, func, index, equiv_index) :-
  loc_phi_equiv_rel(loc, func, index, _),
  equiv_index = min min_index : loc_phi_equiv_rel(loc, func, index, min_index),
  loc = "ESP",

  !call_enter_ESP(index, func),
  !return_exit_ESP_dest(index, func).

ssa_loc_phi_equiv_class(loc, func, index, equiv_index) :-
  loc_phi_equiv_rel(loc, func, index, _),
  equiv_index = min min_index : loc_phi_equiv_rel(loc, func, index, min_index),
  loc = "EBP",
  !return_exit_EBP_dest(index, func).

ssa_loc_phi_equiv_class(loc, func, index, equiv_index) :-
  loc_phi_equiv_rel(loc, func, index, _),
  equiv_index = min min_index : loc_phi_equiv_rel(loc, func, index, min_index),
  loc = "EAX",
  !return_exit_EAX_dest(index, func).

*/

//ssa_loc_phi_after_min_phase(loc, index, index_func, pred, pred_func) :-
//  loc_phi_raw(loc, raw_index, index_func, raw_pred, pred_func),
//  ssa_loc_phi_equiv_class(loc, index_func, raw_index, index),
//  ssa_loc_phi_equiv_class(loc, pred_func, raw_pred, pred),
//  index != pred.



/// we don't need this rule, because in call-enter relation, callee's side ESP -> 0, so we don't need caller side's ESP info
/*
ssa_loc_phi_after_min_phase("ESP", index, index_func, pred_equiv, pred_func) :-
  call_enter_ESP(index, index_func),
  loc_phi_raw("ESP", index, index_func, pred, pred_func),
  ssa_loc_phi_equiv_class("ESP", pred_func, pred, pred_equiv).
*/

//.decl call_enter_ESP_caller_index(index:number, index_func:Func, pred_equiv: number, pred_func:Func)
//call_enter_ESP_caller_index(index, index_func, pred_equiv, pred_func) :-
//  call_enter_ESP(index, index_func),
//  loc_phi_raw("ESP", index, index_func, pred, pred_func),
//  ssa_loc_phi_equiv_class("ESP", pred_func, pred, pred_equiv).

/*****


ssa_loc_phi_after_min_phase("ESP", index, index_func, pred_equiv, pred_func) :-
  return_exit_ESP_dest(index, index_func),
  loc_phi_raw("ESP", index, index_func, pred, pred_func),
  ssa_loc_phi_equiv_class("ESP", pred_func, pred, pred_equiv).


ssa_loc_phi_after_min_phase("EBP", index, index_func, pred_equiv, pred_func) :-
  return_exit_EBP_dest(index, index_func),
  loc_phi_raw("EBP", index, index_func, pred, pred_func),
  ssa_loc_phi_equiv_class("EBP", pred_func, pred, pred_equiv),
  pred_func = index_func.


ssa_loc_phi_after_min_phase("EAX", index, index_func, pred_equiv, pred_func) :-
  return_exit_EAX_dest(index, index_func),
  loc_phi_raw("EAX", index, index_func, pred, pred_func),
  ssa_loc_phi_equiv_class("EAX", pred_func, pred, pred_equiv),
  pred_func != index_func.

*****/


//
