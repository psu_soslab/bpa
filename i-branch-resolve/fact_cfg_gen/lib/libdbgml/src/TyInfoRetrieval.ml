module P = Printf
module H = Hashtbl

type c_type = (** include size of type **)
| (*B0*) C_base of int * string (* like int, float including void *) 
| (*B1*) C_const of c_type
| (*B2*) C_array of c_type * (int list) (* int list for dimension *)
| (*B3*) C_pointer of int * c_type
| (*B4*) C_typedef of string * c_type (* string for typedef name *)
| (*B5*) C_subroutine of c_type * (c_type list) (* first: return type, second: parameter type list *)
| (*B6*) C_enumeration of int * string * ((string * int) list) (* enum_name, {enumerator_name, enumerator_value} *)
| (*B7*) C_union of int * string * ((c_type * string) list) (* union_name, {field_type, field_name} *)
| (*B8*) C_structure of int * string * ((int * c_type * string) list) (* size, struct_name, {fdoff, fdtype, fdname} *)
| (*B9*) C_volatile of c_type
| (*A0*) C_unspecified_param
| (*B10*) C_wrong_type of string (* including an error message *)
| (*A1*) C_void

let rec str_of_c_type ctype =
  match ctype with
  | C_void -> "void"
  | C_base (size,name) -> 
    P.sprintf "%s<%d>" name size
  | C_const ct -> P.sprintf "const %s" (str_of_c_type ct)
  | C_array (ct,dimlst) -> 
    let dimension_str = String.concat "" (List.map (fun dim -> P.sprintf "[%d]" dim) dimlst) in
    P.sprintf "(%s%s)" (str_of_c_type ct) dimension_str
  | C_pointer (size,ct) -> 
    P.sprintf "%s *<%d>" (str_of_c_type ct) size
  | C_typedef (typedef_name, ct) ->
    P.sprintf "typedef(%s:%s)" typedef_name (str_of_c_type ct)
  | C_subroutine (ret_ct, param_ct_list) ->
    let ret_ct_str = str_of_c_type ret_ct in
    let param_ct_str = String.concat "," (List.map str_of_c_type param_ct_list) in
    P.sprintf "SUBRTN(%s)(%s)" ret_ct_str param_ct_str 
  | C_enumeration (size, enumname, etor_list) -> 
    (*let etor_list_str = 
      List.fold_left (fun str (fn, fv) ->
	P.sprintf "%s%s %d; " str fn fv
      ) "" etor_list
    in*)
    if List.length etor_list = 0 then
      P.sprintf "ENUM %s<%d>{ }" enumname size
    else
      P.sprintf "ENUM %s<%d>{%d}" enumname size (List.length etor_list)
      (*P.sprintf "ENUM %s<%d>{\n%s}" enumname size etor_list_str*)
  | C_union (size, unionname, umem_list) -> 
    let umem_list_str = 
      List.fold_left (fun str (ft, fn) ->
	P.sprintf "%s%s %s;\n" str (str_of_c_type ft) fn
      ) "" umem_list
    in
    if List.length umem_list = 0 then
      P.sprintf "UNION %s<%d>{ }" unionname size
    else
      P.sprintf "UNION %s<%d>{\n%s}" unionname size umem_list_str
  | C_structure (size, original_name, smem_list) ->
    let smem_list_str =
      List.fold_left (fun str (off, ft, fn) ->
	P.sprintf "%s[%d] %s %s;\n" str off (str_of_c_type ft) fn
      ) "" smem_list
    in
    if List.length smem_list = 0 then
      P.sprintf "STRUCT %s<%d>{ }" original_name size
    else
      P.sprintf "STRUCT %s<%d>{\n%s}" original_name size smem_list_str
  | C_unspecified_param ->
    "..."    
  | C_wrong_type errmsg ->
    let () = P.printf "Wrong type: %s" errmsg in
    P.sprintf "wrong_type(%s)" errmsg   
  | C_volatile ct ->
    P.sprintf "volatile %s" (str_of_c_type ct)

type loclist = (string * string * string list) list

type type_info = (c_type * loclist) list

external retrieve_raw_gti: string -> type_info = "retrieve_gti"

external retrieve_namemap: string -> c_type list = "retrieve_namemap"

external retrieve_raw_ginfo: string -> c_type list * type_info = "retrieve_ginfo"

external retrieve_raw_pinfo: string -> string -> string * type_info = "retrieve_pinfo"

(** retrieve_funsig_info:
    returns a list of type signatures with low-pc strings **)
external retrieve_funsig_info: string -> (c_type * string) list = "retrieve_fun_typesig"

(** retrieve_libfunsig_info:
    returns a list of type signatures with library function names **)
(**
external retrieve_libfunsig_info: string -> (c_type * string) list = "retrieve_libfun_typesig"
**)

let retrieve_gti targetfile =
  let tyinfo = retrieve_raw_gti targetfile in
  List.map (fun (ty,loclist) ->
    ty, List.map (fun (lo,hi,exprs) ->
      lo, hi, List.rev exprs
    ) loclist
  ) tyinfo

let retrieve_ginfo targetfile = 
  let namemap, tyinfo = retrieve_raw_ginfo targetfile in
  namemap, List.map (fun (ty,loclist) ->
    ty, List.map (fun (lo,hi,exprs) ->
      lo, hi, List.rev exprs
    ) loclist
  ) tyinfo

let retrieve_pinfo targetfile funclopc = 
  let framebase, tyinfo = retrieve_raw_pinfo targetfile funclopc in
  framebase, List.map (fun (ty,loclist) ->
    ty, List.map (fun (lo,hi,exprs) ->
      lo, hi, List.rev exprs
    ) loclist
  ) tyinfo

let dump_type_info tyinfo =
  let tyinfo_size = List.length tyinfo in
  P.printf "We have %d type info entries\n" tyinfo_size;
  flush stdout;
  List.iter (fun (ty,loclist) ->
    P.printf "%s\n" (str_of_c_type ty);
    List.iter (fun (lo,hi,locexprs) ->
      P.printf "%s\n%s\n" lo hi;
      List.iter (fun locexpr ->
	P.printf "%s\n" locexpr
      ) locexprs
    ) loclist;
    flush stdout
  ) tyinfo
    
let dump_ginfo ginfo =
  let structmap, tyinfo = ginfo in
  P.printf "Struct map contains %d entries:\n" (List.length structmap) ;
  flush stdout;
  List.iter (fun ty ->
    P.printf "%s\n" (str_of_c_type ty);
  ) structmap;
  dump_type_info tyinfo

let dump_pinfo pinfo =
  let framebase, tyinfo = pinfo in
  P.printf "With frame base: %s\n" framebase;
  dump_type_info tyinfo

let dump_funsig_info funsig_info =
  List.iter (fun (sigtype, lopcstr) ->
    P.printf "Function at %s:\n%s\n"
      lopcstr (str_of_c_type sigtype)
  ) funsig_info


let cons_namety_hashtbl namety_list = 
  let global_struct_map = H.create 32 in
  let global_union_map = H.create 32 in
  let global_typedef_map = H.create 32 in
  List.iter (fun namety ->
    match namety with
    | C_structure(size,name,fdlist) ->
      assert(name <> "");
      if not (H.mem global_struct_map name) then
	H.add global_struct_map name namety
    | C_union(size,name,fdlist) ->
      assert(name <> "");
      if not (H.mem global_union_map name) then
	H.add global_union_map name namety      
    | C_typedef(defnm, ty') ->
      assert(defnm <> "");
      begin match ty' with
      | C_structure _ ->
	if not (H.mem global_typedef_map defnm) then
	  H.add global_typedef_map defnm ty'
      | C_union _ ->
	if not (H.mem global_union_map defnm) then
	  H.add global_union_map defnm ty'
      | _ ->
	(** Impossible **)
	assert(false);
      end
    | _ ->
      (** Impossible **)
      assert(false);
  ) namety_list;
  (global_union_map, global_struct_map, global_typedef_map)

let dump_namety_hashtbl struct_hashtbl =
  H.iter (fun symbol struct_type ->
    P.printf "%s -->\n" symbol;
    P.printf "%s\n\n" (str_of_c_type struct_type);
  ) struct_hashtbl
