#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "shared.h"
#include "dwarf.h"
#include "libdwarf.h"
#include "config.h"
/*
char** struct_map = NULL;
int smapsize = 0;
char** typedef_map = NULL;
int tmapsize = 0;
char** union_map = NULL;
int umapsize = 0;
*/
static value
work_on_name_map(Dwarf_Debug dbg, Dwarf_Die in_die, int level, value inval)
{
  CAMLparam1( inval );
  CAMLlocal1( cons );

  int res,i;
  Dwarf_Error error = 0;
  Dwarf_Half tag = 0;  
  Dwarf_Bool hastype = 0;
  Dwarf_Bool hasname = 0;
  Dwarf_Bool hassize = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Off type_off = 0;
  Dwarf_Die type_die = 0;
  char* die_name = 0;

  /* Encode struct type information including struct and typedef-ed struct */

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag");
    assert(0);
  }
  
  if(tag != DW_TAG_structure_type && tag != DW_TAG_typedef && tag != DW_TAG_union_type) {
    CAMLreturn( inval ); // not a structure, typedef, or union; do nothing but return
  }
  
  res = dwarf_hasattr(in_die,DW_AT_name,&hasname,&error);
  if(res != DW_DLV_OK || hasname == 0) {
    CAMLreturn( inval ); //no name no use
  }

  if(tag == DW_TAG_structure_type) {
    res = dwarf_hasattr(in_die,DW_AT_byte_size,&hassize,&error);
    if(res != DW_DLV_OK) {
      perror("error hasattr");
      assert(0);
    }
    if(hassize == 0) {
      // this could be possible
      // however, we do not care about this for struct map
      CAMLreturn( inval );
    }
    res = dwarf_diename(in_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      assert(0);
    }
    /*
    if(in_map(die_name,struct_map,smapsize)) {
      dwarf_dealloc(dbg, die_name, DW_DLA_STRING);
      CAMLreturn( inval );
    }
    assert(smapsize < 1024);
    smapsize++;
    struct_map[smapsize-1] = malloc(strlen(die_name)+1);
    memset(struct_map[smapsize-1], '\0', strlen(die_name)+1);
    sprintf(struct_map[smapsize-1], "%s", die_name);
    dwarf_dealloc(dbg, die_name, DW_DLA_STRING);
    */
    cons = caml_alloc(2,0);
    Store_field( cons, 0, //Val_int(1) );
    		 encode_detail(dbg,in_die) );
  } else if(tag == DW_TAG_typedef) {
    res = dwarf_hasattr(in_die,DW_AT_type,&hastype,&error);
    if(res != DW_DLV_OK || hastype == 0) {
      // no type info; decide whether "void"
      // in typedef, the type could be void; but we do not care about this case
      // so still do nothing
      CAMLreturn( inval );
    }
    
    res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
    if(res == DW_DLV_ERROR) {
      perror("type attrlist error!");
      assert(0);
    }

    for(i = 0; i < attrcount; ++i) {
      Dwarf_Half aform;
      res = dwarf_whatattr(attrbuf[i], &aform, &error);
      if(res == DW_DLV_OK) {
	if(aform == DW_AT_type) {
	  get_type_off(attrbuf[i], &type_off);
	}
      }
      dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
    }
    dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);

    res = dwarf_offdie(dbg, type_off, &type_die, &error);
    if(res != DW_DLV_OK) {
      perror("No type or wrong offset in work_on_struct_map");
      assert(0);
    }
    
    res = dwarf_tag(type_die, &tag, &error);
    if(res != DW_DLV_OK) {
      perror("Error in dwarf_tag");
      assert(0);
    }

    if(tag != DW_TAG_structure_type && tag != DW_TAG_union_type) {
      CAMLreturn( inval );// heuristics here
    }

    res = dwarf_hasattr(type_die,DW_AT_byte_size,&hassize,&error);
    if(res != DW_DLV_OK) {
      perror("error hasattr");
      assert(0);
    }
    dwarf_dealloc(dbg,type_die,DW_DLA_DIE);

    if(hassize == 0) {CAMLreturn( inval );}

    res = dwarf_diename(in_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      assert(0);
    }
    /*
    if(in_map(die_name,typedef_map,tmapsize)) {
      dwarf_dealloc(dbg, die_name, DW_DLA_STRING);
      CAMLreturn( inval );
    } 
    assert(tmapsize < 1024);
    tmapsize++;
    typedef_map[tmapsize-1] = malloc(strlen(die_name)+1);
    memset(typedef_map[tmapsize-1], '\0', strlen(die_name)+1);
    sprintf(typedef_map[tmapsize-1], "%s", die_name);
    dwarf_dealloc(dbg, die_name, DW_DLA_STRING);
    */
    cons = caml_alloc(2,0);
    Store_field( cons, 0,// Val_int(1) );
    		 encode_detail(dbg, in_die) );
  } else {
    // union type
    res = dwarf_hasattr(in_die,DW_AT_byte_size,&hassize,&error);
    if(res != DW_DLV_OK) {
      perror("error hasattr");
      assert(0);
    }
    if(hassize == 0) {
      // this could be possible
      // however, we do not care about this for name map
      CAMLreturn( inval );
    }
    res = dwarf_diename(in_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      assert(0);
    }
    /*
    if(in_map(die_name,union_map,umapsize)) {
      dwarf_dealloc(dbg, die_name, DW_DLA_STRING);
      CAMLreturn( inval );
    }
    assert(umapsize < 1024);
    umapsize++;
    union_map[umapsize-1] = malloc(strlen(die_name)+1);
    memset(union_map[umapsize-1], '\0', strlen(die_name)+1);
    sprintf(union_map[umapsize-1], "%s", die_name);
    dwarf_dealloc(dbg, die_name, DW_DLA_STRING);
    */
    cons = caml_alloc(2,0);
    Store_field( cons, 0, //Val_int(1) );
    		 encode_detail(dbg,in_die) );
  }

  Store_field( cons, 1, inval );
  inval = cons;
  CAMLreturn( inval );
}

static value
work_on_globals(Dwarf_Debug dbg, Dwarf_Die in_die, int level, value inval)
{
  CAMLparam1( inval );

  int res = 0;
  int i = 0;
  Dwarf_Error error = 0;

  Dwarf_Half tag = 0;  
  Dwarf_Bool hastype = 0;
  Dwarf_Bool hasloc = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Off type_off = 0;

  /* Encode global type information including external and static global variables */

  if(level != 1) {
    CAMLreturn( inval ); // not a global entry; do nothing but return
  }

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag");
    assert(0);
  }
  if(tag != DW_TAG_variable) {
    CAMLreturn( inval ); // not a variable; do nothing but return
  }
  
  res = dwarf_hasattr(in_die,DW_AT_type,&hastype,&error);
  if(res != DW_DLV_OK || hastype == 0) {
    // no type info; decide whether "void"
    // here, variable cannot be of type void; only return value could be
    // so still do nothing
    CAMLreturn( inval );
  }

  res = dwarf_hasattr(in_die,DW_AT_location,&hasloc,&error);
  if(res != DW_DLV_OK || hasloc == 0) {
    CAMLreturn( inval );
  }

  res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
  if(res != DW_DLV_OK) {
    perror("Error, fail attrlist when there is type attr!");
    assert(0);
  }

  for(i = 0; i < attrcount; ++i) {
    Dwarf_Half aform;
    res = dwarf_whatattr(attrbuf[i], &aform, &error);
    if(res == DW_DLV_OK) {
      if(aform == DW_AT_type) {
	get_type_off(attrbuf[i], &type_off);
      }
    }
    dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
  }
  dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);

  CAMLlocal2( type, loclist );
  CAMLlocal2( pair, cons );

  type = encode_type(dbg,type_off);
  loclist = encode_sinfo(dbg,in_die);

  pair = caml_alloc(2,0);
  cons = caml_alloc(2,0);

  Store_field( pair, 0, type );
  Store_field( pair, 1, loclist );

  Store_field( cons, 0, pair );
  Store_field( cons, 1, inval );

  inval = cons;

  CAMLreturn( inval );
}

static value
work_on_gti_and_namemap(Dwarf_Debug dbg, Dwarf_Die in_die, int level, value inval)
{
  CAMLparam1( inval );
  CAMLlocal3( namemap, tyinfo, outval );
 
  namemap = Field( inval, 0 );
  tyinfo = Field( inval, 1 );
  
  namemap = work_on_name_map(dbg,in_die,level,namemap);
  tyinfo = work_on_globals(dbg,in_die,level,tyinfo);

  outval = caml_alloc(2,0);
  Store_field( outval, 0, namemap );
  Store_field( outval, 1, tyinfo );
  
  inval = outval;
  CAMLreturn( inval );
}

CAMLprim value
retrieve_gti(value ml_string)
{
  CAMLparam1( ml_string );
  CAMLlocal1( tyinfo );
 
  int fd = -1;
  char *filepath = String_val( ml_string );

  Dwarf_Debug dbg = 0;
  int res = DW_DLV_ERROR; 
  Dwarf_Error error;
  /* TODO: Unknown purpose for now */
  Dwarf_Handler errhand = 0;
  Dwarf_Ptr errarg = 0;

  /* open target file */
  fd = open_a_file(filepath);
  if(fd < 0) {
    printf("Failure attempting to open %s\n", filepath);
    exit(1);
  }

  /* initialize dwarf handling environment */

  res = dwarf_init(fd, DW_DLC_READ,errhand,errarg,&dbg,&error);
  if(res != DW_DLV_OK) {
    printf("Giving up, cannot do DWARF processing\n");
    exit(1);
  }

  tyinfo = collect_info( dbg, work_on_globals, Val_emptylist );

  res = dwarf_finish(dbg, &error);
  if(res != DW_DLV_OK) {
    printf("dwarf_finish failed!\n");
  }

  close_a_file(fd); 
    
  CAMLreturn( tyinfo );
}

CAMLprim value
retrieve_namemap(value ml_string)
{
  CAMLparam1( ml_string );
  CAMLlocal1( namemap );
 
  int fd = -1;
  int i;
  char *filepath = String_val( ml_string );

  Dwarf_Debug dbg = 0;
  int res = DW_DLV_ERROR; 
  Dwarf_Error error;
  /* TODO: Unknown purpose for now */
  Dwarf_Handler errhand = 0;
  Dwarf_Ptr errarg = 0;

  /* open target file */
  fd = open_a_file(filepath);
  if(fd < 0) {
    printf("Failure attempting to open %s\n", filepath);
    exit(1);
  }

  /* initialize dwarf handling environment */

  res = dwarf_init(fd, DW_DLC_READ,errhand,errarg,&dbg,&error);
  if(res != DW_DLV_OK) {
    printf("Giving up, cannot do DWARF processing\n");
    exit(1);
  }
  /*
  struct_map = (char**)malloc(1024*sizeof(char*));
  typedef_map = (char**)malloc(2048*sizeof(char*));
  union_map = (char**)malloc(1024*sizeof(char*));
  */
  namemap = collect_info( dbg, work_on_name_map, Val_emptylist );
  /*
  for(i=0;i<smapsize;i++) {
    free(struct_map[i]);
  }
  free(struct_map);

  for(i=0;i<tmapsize;i++) {
    free(typedef_map[i]);
  }
  free(typedef_map);

  for(i=0;i<umapsize;i++) {
    free(union_map[i]);
  }
  free(union_map);
  */
  res = dwarf_finish(dbg, &error);
  if(res != DW_DLV_OK) {
    printf("dwarf_finish failed!\n");
  }
  
  close_a_file(fd); 
    
  CAMLreturn( namemap );
}

CAMLprim value
retrieve_ginfo(value ml_string)
{
  CAMLparam1( ml_string );
  CAMLlocal3( namemap, tyinfo, info );
 
  int fd = -1;
  char *filepath = String_val( ml_string );

  Dwarf_Debug dbg = 0;
  int res = DW_DLV_ERROR; 
  Dwarf_Error error;
  /* TODO: Unknown purpose for now */
  Dwarf_Handler errhand = 0;
  Dwarf_Ptr errarg = 0;
  int i;

  /* open target file */
  fd = open_a_file(filepath);
  if(fd < 0) {
    printf("Failure attempting to open %s\n", filepath);
    exit(1);
  }

  /* initialize dwarf handling environment */

  res = dwarf_init(fd, DW_DLC_READ,errhand,errarg,&dbg,&error);
  if(res != DW_DLV_OK) {
    printf("Giving up, cannot do DWARF processing\n");
    exit(1);
  }
  /*
  struct_map = (char**)malloc(1024*sizeof(char*));
  typedef_map = (char**)malloc(2048*sizeof(char*));
  union_map = (char**)malloc(1024*sizeof(char*));
  */
  namemap = collect_info(dbg, work_on_name_map, Val_emptylist);
  tyinfo = collect_info(dbg, work_on_globals, Val_emptylist);

  info = caml_alloc(2,0);
  Store_field( info, 0, namemap );
  Store_field( info, 1, tyinfo );
  /*
  for(i=0;i<smapsize;i++) {
    free(struct_map[i]);
  }
  free(struct_map);

  for(i=0;i<tmapsize;i++) {
    free(typedef_map[i]);
  }
  free(typedef_map);

  for(i=0;i<umapsize;i++) {
    free(union_map[i]);
  }
  free(union_map);
  */
  res = dwarf_finish(dbg, &error);
  if(res != DW_DLV_OK) {
    printf("dwarf_finish failed!\n");
  }

  close_a_file(fd); 

  CAMLreturn( info );
}

char* funclopc = NULL;

static value
work_on_pinfo(Dwarf_Debug dbg, Dwarf_Die in_die, int level, value inval)
{
  CAMLparam1( inval );
  CAMLlocal1( pair );

  int res, i, j;
  Dwarf_Error error = 0;
  Dwarf_Half tag = 0;  
  Dwarf_Bool hastype = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Off type_off = 0;
  char* die_name = NULL; 
  Dwarf_Bool haslopc = 0;
  Dwarf_Addr lopc = 0;
  char lopcstr[100];
  Dwarf_Bool hasabstr = 0;
  Dwarf_Bool hasfbase = 0;
  Dwarf_Die child_die = 0;
  Dwarf_Locdesc **loclist = NULL;
  Dwarf_Signed loc_num = 0;

  /*
    Encode procedural type information including parameters, 
    local variables, and frame base
  */

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag");
    assert(0);
  }
  if(tag != DW_TAG_subprogram) {CAMLreturn( inval );} // not a procedure; do nothing but return

  res = dwarf_hasattr(in_die, DW_AT_low_pc, &haslopc, &error);
  if(res != DW_DLV_OK) {
    perror("Error in hasattr");
    assert(0);
  }
  if(haslopc == 0) {// if no lopc, the subprogram does not occur in code
    CAMLreturn( inval );
  } 

  res = dwarf_attrlist(in_die,&attrbuf,&attrcount,&error);
  if(res != DW_DLV_OK) {
    perror("Error in attrlist");
    assert(0);
  }

  for(i = 0; i < attrcount; i++) {
    Dwarf_Half aform;
    res = dwarf_whatattr(attrbuf[i], &aform, &error);
    if(res == DW_DLV_OK && aform == DW_AT_low_pc) {
      get_addr(attrbuf[i], &lopc);
    }
    dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
  }
  dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);

  sprintf(lopcstr,"%" DW_PR_DUx,lopc);
  
  // we only compare low pc to decide target
  if(strcmp(funclopc, lopcstr)) {
    //not the target function
    CAMLreturn( inval );
  }
  // target function found and
  // location information must be available
  // first, let's retrieve frame base
  res = dwarf_hasattr(in_die,DW_AT_frame_base,&hasfbase,&error);
  if(res != DW_DLV_OK || hasfbase == 0) {
    perror("Error in hasattr");
    assert(0);
  }
  
  pair = caml_alloc(2,0);

  res = dwarf_attrlist(in_die,&attrbuf,&attrcount,&error);
  if(res != DW_DLV_OK) {
    perror("Error in attrlist");
    assert(0);
  }

  for(i = 0; i < attrcount; i++) {
    Dwarf_Half aform;
    res = dwarf_whatattr(attrbuf[i], &aform, &error);
    if(res == DW_DLV_OK && aform == DW_AT_frame_base) {
      res = dwarf_loclist_n(attrbuf[i],&loclist,&loc_num,&error);
      if(res != DW_DLV_OK) {
	perror("error localist_n");
	assert(0);
      }
      
      assert( loc_num == 1 );
      assert( loclist[0]->ld_cents = 1 );

      Dwarf_Small op = loclist[0]->ld_s[0].lr_atom;
      const char* op_name = NULL;
      res = dwarf_get_OP_name(op, &op_name);
      Store_field( pair, 0, caml_copy_string(op_name) );

      for(j=0; j < loc_num;j++){
	dwarf_dealloc(dbg,loclist[j]->ld_s,DW_DLA_LOC_BLOCK);
      }
      dwarf_dealloc(dbg,loclist,DW_DLA_LOCDESC);
    }
    dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
  }
  dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);

  // next, let's find procedural information among children

  // before that, we have to decide where type information 
  // and location information are.
  // Actually, we do not need to distinguish them at this 
  // level; we can delay it into children level

  res = dwarf_child(in_die, &child_die, &error);
  if(res == DW_DLV_OK) {
    Store_field( pair, 1,// Val_emptylist );
		 encode_relatives(dbg,child_die,Val_emptylist) );
    dwarf_dealloc(dbg,child_die,DW_DLA_DIE);
  } else if(res == DW_DLV_NO_ENTRY) {
    Store_field( pair, 1, Val_emptylist );
  } else {
    perror("error in dwarf_child");
    assert(0);
  }

  CAMLreturn( pair ) ;
}

CAMLprim value
retrieve_pinfo(value ml_string1, value ml_string2)
{
  CAMLparam2( ml_string1, ml_string2 );
  CAMLlocal1( info );
 
  int fd = -1;
  char *filepath = String_val( ml_string1 );
  funclopc = String_val( ml_string2 );

  Dwarf_Debug dbg = 0;
  int res = DW_DLV_ERROR; 
  Dwarf_Error error;
  /* TODO: Unknown purpose for now */
  Dwarf_Handler errhand = 0;
  Dwarf_Ptr errarg = 0;
  int i;

  /* open target file */
  fd = open_a_file(filepath);
  if(fd < 0) {
    printf("Failure attempting to open %s\n", filepath);
    exit(1);
  }

  /* initialize dwarf handling environment */

  res = dwarf_init(fd, DW_DLC_READ,errhand,errarg,&dbg,&error);
  if(res != DW_DLV_OK) {
    printf("Giving up, cannot do DWARF processing\n");
    exit(1);
  }

  CAMLlocal1( initpair );
  initpair = caml_alloc(2,0);
  Store_field( initpair, 0, 
	       caml_copy_string("none") ); // framebase
  Store_field( initpair, 1, Val_emptylist ); // type-info

  info = collect_info( dbg, work_on_pinfo, initpair );

  res = dwarf_finish(dbg, &error);
  if(res != DW_DLV_OK) {
    printf("dwarf_finish failed!\n");
  }

  close_a_file(fd); 

  CAMLreturn( info );
}

static value
work_on_fun_typesig(Dwarf_Debug dbg, Dwarf_Die in_die, int level, value inval)
{
  CAMLparam1( inval );

  int res = 0;
  int i = 0;
  Dwarf_Error error = 0;

  Dwarf_Half tag = 0;  
  Dwarf_Bool hastype = 0;
  Dwarf_Bool haslopc = 0;
  Dwarf_Bool hasabstr = 0;
  Dwarf_Bool haslinkage = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Off type_off = 0;
  Dwarf_Die child_die = 0;

  /* Encode function type signatures and the starting address */

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag");
    assert(0);
  }
  if(tag != DW_TAG_subprogram) {
    CAMLreturn( inval ); // not a function; do nothing but return
  }
  
  res = dwarf_hasattr(in_die,DW_AT_low_pc,&haslopc,&error);

  if(res != DW_DLV_OK) {CAMLreturn( inval );}

  res = dwarf_hasattr(in_die,DW_AT_linkage_name,&haslinkage,&error);

  if(res != DW_DLV_OK) {CAMLreturn( inval );}
  
  if( haslopc == 0 && haslinkage == 0 ) {
    CAMLreturn( inval );
  }

  assert(haslopc == 1 || haslinkage == 1);

  // real function in code; either an application function or a library 
  // function stub
  // retrieve low pc attribute or linkage name

  CAMLlocal3( cons, info, ident );

  res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
  if(res != DW_DLV_OK) {
    perror("Error, fail attrlist when there is type attr!");
    assert(0);
  }

  for(i = 0; i < attrcount; ++i) {
    Dwarf_Half aform;
    res = dwarf_whatattr(attrbuf[i], &aform, &error);
    if(res == DW_DLV_OK) {
      if(aform == DW_AT_low_pc) {
	Dwarf_Addr funAddr = 0;
	char lopcstr[100];
	get_addr(attrbuf[i], &funAddr);
	sprintf(lopcstr, "0x%" DW_PR_DUx, funAddr);
	ident = caml_copy_string(lopcstr);
      }
      if(aform == DW_AT_linkage_name) {
	char* namestr;
	get_name(attrbuf[i], &namestr);
	ident = caml_copy_string(namestr);
	dwarf_dealloc(dbg,namestr,DW_DLA_STRING);
      }
    }
    dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
  }
  dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);

  // decide where to find return type
  CAMLlocal1( typesig );

  res = dwarf_hasattr(in_die,DW_AT_abstract_origin,&hasabstr,&error);
  if(res != DW_DLV_OK) {
    perror("error hasattr");
    assert(0);
  }
  
  if(hasabstr == 0) {
    // normal DIE: look for type information locally
    res = dwarf_hasattr(in_die,DW_AT_type,&hastype,&error);
    if(res != DW_DLV_OK || hastype == 0) {
      // no type info; decide whether "void"
      // here, function may have type void
      // encode return type to be void
      typesig = create_subroutine(create_void(), Val_emptylist);
    } else {
      res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
      if(res != DW_DLV_OK) {
	perror("Error, fail attrlist when there is type attr!");
	assert(0);
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    get_type_off(attrbuf[i], &type_off);
	    typesig = 
	      create_subroutine(encode_type(dbg,type_off),
				Val_emptylist);
	  }
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
    }
  } else {
    // special DIE; look for type information remotely
    Dwarf_Die origin_die = 0;
    res = dwarf_attrlist(in_die,&attrbuf,&attrcount,&error);
    if(res != DW_DLV_OK) {
      perror("error attrlist");
      assert(0);
    }
    for(i = 0; i< attrcount; i++) {
      Dwarf_Half aform;
      res = dwarf_whatattr(attrbuf[i], &aform, &error);
      if(res == DW_DLV_OK && aform == DW_AT_abstract_origin) {
	Dwarf_Off origin_off = 0;
	get_type_off(attrbuf[i], &origin_off);
	res = dwarf_offdie(dbg,origin_off,&origin_die,&error);
	if(res != DW_DLV_OK) {
	  perror("error offdie");
	  assert(0);
	}
      }
      dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
    }
    dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);

    // now we have origin DIE; go to find return type
    res = dwarf_hasattr(origin_die,DW_AT_type,&hastype,&error);
    if(res != DW_DLV_OK || hastype == 0) {
      // no type info; decide whether "void"
      // here, function may have type void
      // encode return type to be void
      typesig = create_subroutine(create_void(), Val_emptylist);
    } else {
      res = dwarf_attrlist(origin_die, &attrbuf, &attrcount, &error);
      if(res != DW_DLV_OK) {
	perror("Error, fail attrlist when there is type attr!");
	assert(0);
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    get_type_off(attrbuf[i], &type_off);
	    typesig = 
	      create_subroutine(encode_type(dbg,type_off),
				Val_emptylist);
	  }
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
    }   
  }
  
  res = dwarf_child(in_die,&child_die,&error);
  if(res == DW_DLV_NO_ENTRY) {
    Store_field( typesig, 1, Val_emptylist );
  } else if(res == DW_DLV_OK) {
    Store_field( typesig, 1, //Val_emptylist);
		 encode_subprogram_parameters(dbg,child_die) );
  } else {
    perror("Error: when encoding parameters for fun-typesig\n");
    assert(0);
  }
  
  info = caml_alloc(2,0);
  Store_field( info, 0, typesig);
  Store_field( info, 1, ident);

  cons = caml_alloc(2,0);
  Store_field( cons, 0, info );
  Store_field( cons, 1, inval );

  inval = cons;

  CAMLreturn( inval );
}


CAMLprim value
retrieve_fun_typesig(value ml_string1)
{
  CAMLparam1( ml_string1 );
  CAMLlocal1( info );
 
  int fd = -1;
  char *filepath = String_val( ml_string1 );

  Dwarf_Debug dbg = 0;
  int res = DW_DLV_ERROR; 
  Dwarf_Error error;
  /* TODO: Unknown purpose for now */
  Dwarf_Handler errhand = 0;
  Dwarf_Ptr errarg = 0;
  int i;

  /* open target file */
  fd = open_a_file(filepath);
  if(fd < 0) {
    printf("Failure attempting to open %s\n", filepath);
    exit(1);
  }

  /* initialize dwarf handling environment */

  res = dwarf_init(fd, DW_DLC_READ,errhand,errarg,&dbg,&error);
  if(res != DW_DLV_OK) {
    printf("Giving up, cannot do DWARF processing\n");
    exit(1);
  }

  info = collect_info( dbg, work_on_fun_typesig, Val_emptylist );

  res = dwarf_finish(dbg, &error);
  if(res != DW_DLV_OK) {
    printf("dwarf_finish failed!\n");
  }

  close_a_file(fd); 

  CAMLreturn( info );
}
