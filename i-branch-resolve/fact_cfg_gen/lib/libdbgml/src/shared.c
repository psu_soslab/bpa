#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#include "config.h"
#include "dwarf.h"
#include "libdwarf.h"
#include "shared.h"

static Dwarf_Addr addrbase = 0;

//extern char** struct_map;
//extern int mapsize;

//extern char enc_buf[1024*1024]; // increase size on demand
//extern int used;

int open_a_file(const char* name) {
  /* Only consider on X86 platform here; the original libdwarf targets
     at different platforms, so they have an open-file wrapper */
  int f = -1;
  f = open(name, O_RDONLY);
  return f;
}

void close_a_file(int f) {
  /* to match open_a_file */
  close(f);
}

void
resetsrcfiles(Dwarf_Debug dbg,struct srcfilesdata *sf)
{
  Dwarf_Signed sri = 0;
  for (sri = 0; sri < sf->srcfilescount; ++sri) {
    dwarf_dealloc(dbg, sf->srcfiles[sri], DW_DLA_STRING);
  }
  dwarf_dealloc(dbg, sf->srcfiles, DW_DLA_LIST);
  sf->srcfilesres = DW_DLV_ERROR;
  sf->srcfiles = 0;
  sf->srcfilescount = 0;
}

void
get_addr(Dwarf_Attribute attr, Dwarf_Addr *val) {
  Dwarf_Error error = 0;
  int res;
  Dwarf_Addr uval = 0;
  
  /* Translate attribute to address */
  res =dwarf_formaddr(attr, &uval, &error);
  if(res == DW_DLV_OK) {
    *val = uval;
    return;
  }
  perror("error in get_addr");
  assert(0);
}

void
get_uvalue(Dwarf_Die in_die, Dwarf_Attribute attr, Dwarf_Unsigned* ret_value) {
  Dwarf_Unsigned local_ret_value = 0;
  int res;
  Dwarf_Error error = 0;
  
  res = dwarf_formudata(attr,&local_ret_value,&error);
  if(res == DW_DLV_OK) {
    *ret_value = local_ret_value;
  }
  else {
    perror("Warning: unsigned value formation failed!");
    Dwarf_Off die_off = 0;
    res = dwarf_dieoffset(in_die,&die_off,&error);
    printf("The problem happens for %" DW_PR_DUx "\n", die_off);
    assert(0);
  }
}

void
get_svalue(Dwarf_Attribute attr, Dwarf_Signed* ret_value) {
  Dwarf_Signed local_ret_value = 0;
  int res;
  Dwarf_Error error = 0;
  
  res = dwarf_formsdata(attr,&local_ret_value,&error);
  if(res == DW_DLV_OK) {
    *ret_value = local_ret_value;
  }
  else {
    perror("Warning: signed value formation failed!");
    assert(0);
  }
}

void
get_type_off(Dwarf_Attribute attr, Dwarf_Off* type_off) {
  Dwarf_Off local_off = 0;
  Dwarf_Error error = 0;
  int res;

  res = dwarf_global_formref(attr, &local_off, &error);
  if(res == DW_DLV_OK) {
    *type_off = local_off;
    return;
  }
  perror("error get_type_off");
  assert(0);
}

Dwarf_Unsigned
get_type_size(Dwarf_Debug dbg, Dwarf_Die in_die)
{
  int res = 0;
  int i = 0;
  Dwarf_Error error = 0;
  
  Dwarf_Bool hassize = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Unsigned size = 0;

  Dwarf_Die cur_die = in_die;

  res = dwarf_hasattr(in_die,DW_AT_byte_size,&hassize,&error);
  if(res != DW_DLV_OK || hassize == 0) 
    {
      Dwarf_Half tag = 0;
      const char* tagname = NULL;
      res = dwarf_tag(in_die,&tag,&error);
      res = dwarf_get_TAG_name(tag,&tagname);
      printf("%s\n",tagname);
      perror("No size attribute or error in retrieving size attribute");
      exit(1);
    }
  
  if(hassize == 1) {
    res = dwarf_attrlist(in_die,&attrbuf,&attrcount,&error);
    if(res != DW_DLV_OK) {
      perror("attrlist error in base type");	  
      exit(1);
    }
    for(i=0;i<attrcount;i++) {
      Dwarf_Half aform;
      res = dwarf_whatattr(attrbuf[i],&aform,&error);
      if(res == DW_DLV_OK) {
	if(aform == DW_AT_byte_size) {
	  get_uvalue(in_die, attrbuf[i],&size);
	}
      }
      dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
    }
    dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
  }

  assert(cur_die == in_die);

  return size;
}


int 
count_tag(Dwarf_Debug dbg, Dwarf_Die start_die, Dwarf_Half target_tag) 
{
  int res = 0;
  Dwarf_Error error = 0;
  Dwarf_Die curr_die = start_die;
  int num_tag = 0;
  Dwarf_Half tag = 0;

  res = dwarf_tag(start_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag in count_tag!");
    exit(1);
  }
  
  for(;;) {
    Dwarf_Die sib_die = 0;
    if(tag == target_tag) num_tag++;
    res = dwarf_siblingof(dbg,curr_die,&sib_die,&error);
    if(res == DW_DLV_OK) {
      res = dwarf_tag(sib_die,&tag,&error);
      if(res != DW_DLV_OK) {
	perror("Error in dwarf_tag in count_tag!");
	exit(1);
      }
      if(curr_die != start_die) {
	dwarf_dealloc(dbg,curr_die,DW_DLA_DIE);
      }
      curr_die = sib_die;
    } else if(res == DW_DLV_NO_ENTRY) {
      return num_tag;
    } else {
      perror("Error in dwarf_siblingof in count_tag!");
      exit(1);
    }
  }
}

void
get_name(Dwarf_Attribute attr, char ** name) {
  Dwarf_Error error = 0;
  int res;
  char* localname = 0;
  
  res = dwarf_formstring(attr, &localname, &error);
  if(res == DW_DLV_OK) {
    *name = localname;
    return;
  }
  perror("error get_name");
  assert(0);
}

Dwarf_Unsigned
get_memloc(Dwarf_Debug dbg, Dwarf_Die in_die) {
  Dwarf_Error error = 0;
  int res = 0;
  int i = 0;
    
  Dwarf_Bool hasmemloc = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Unsigned memloc = 0;

  res = dwarf_hasattr(in_die,DW_AT_data_member_location,&hasmemloc,&error);
  if(res != DW_DLV_OK || hasmemloc == 0) 
    { 
      perror("No data member location attribute or error in retrieving memloc attribute");
      exit(1);
    }
  
  if(hasmemloc == 1) {
    res = dwarf_attrlist(in_die,&attrbuf,&attrcount,&error);
    if(res != DW_DLV_OK) {
      perror("attrlist error in base type");	  
      exit(1);
    }
    for(i=0;i<attrcount;i++) {
      Dwarf_Half aform;
      res = dwarf_whatattr(attrbuf[i],&aform,&error);
      if(res == DW_DLV_OK) {
	if(aform == DW_AT_data_member_location) {
	  Dwarf_Unsigned offset = 0;
	  Dwarf_Half form = 0;
	  res = dwarf_whatform(attrbuf[i], &form, &error);
	  if (form == DW_FORM_data1 || form == DW_FORM_data2 ||
	      form == DW_FORM_data2 || form == DW_FORM_data4 ||
	      form == DW_FORM_data8 || form == DW_FORM_udata) {
	    dwarf_formudata(attrbuf[i], &offset, &error);
	    memloc = offset;
	    //printf("%" DW_PR_DUu "\n", offset);
	  } else if (form == DW_FORM_sdata) {
	    Dwarf_Signed soffset;
	    dwarf_formsdata(attrbuf[i], &soffset, 0);
	    if (soffset < 0) {
	      printf("unsupported negative offset\n");
	      /* FAIL */
	    }
	    offset = (Dwarf_Unsigned) soffset;
	    memloc = offset;
	    //printf("%" DW_PR_DUu "\n", offset);
	  } else {
	    Dwarf_Locdesc **locdescs;
	    Dwarf_Signed len;
	    if (dwarf_loclist_n(attrbuf[i], &locdescs, &len,  &error) == DW_DLV_ERROR) {
	      printf("unsupported member offset\n");
	      /* FAIL */
	    }
	    if (len != 1
		|| locdescs[0]->ld_cents != 1
		|| (locdescs[0]->ld_s[0]).lr_atom != DW_OP_plus_uconst) {
	      printf("unsupported location expression\n");
	      /* FAIL */
	    }
	    offset = (locdescs[0]->ld_s[0]).lr_number;
	    memloc = offset;
	    int j = 0;
	    for(j=0; j < len;j++){	
	      dwarf_dealloc(dbg,locdescs[j]->ld_s,DW_DLA_LOC_BLOCK);
	    }
	    dwarf_dealloc(dbg,locdescs,DW_DLA_LOCDESC);
	    //printf("%" DW_PR_DUu "\n", offset);
	  }
	}
      }
      dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
    }
    dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
  }

  return memloc;
}

static value
get_die_and_siblings(Dwarf_Debug dbg, Dwarf_Die in_die, int is_info,
		     int in_level, work_method work, value inval)
{
  int res = DW_DLV_ERROR;
  Dwarf_Die cur_die = in_die;
  Dwarf_Die child = 0;
  Dwarf_Error error = 0;

  Dwarf_Half tag = 0;
  Dwarf_Bool haslopc = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;

  int i;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag");
    exit(1);
  }

  if(tag == DW_TAG_compile_unit) {
    res = dwarf_hasattr(in_die, DW_AT_low_pc, &haslopc, &error);
    if(res != DW_DLV_OK) { perror("CU no lowpc\n"); exit(1);}
    if(haslopc == 0) addrbase = 0;
    else {
      res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
      if(res != DW_DLV_OK) { perror("CU no attrlist\n"); exit(1);}
      for(i = 0; i < attrcount; i++) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_low_pc) {
	    get_addr(attrbuf[i], &addrbase);
	  }
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
    }
  }
  
  inval = work(dbg, in_die, in_level, inval);
  
  for(;;) {
    Dwarf_Die sib_die = 0;
    res = dwarf_child(cur_die, &child, &error);
    if(res == DW_DLV_ERROR) {
      printf("Error in dwarf_child , level %d \n",in_level);
      exit(1);
    }
    if(res == DW_DLV_OK) {
      inval = get_die_and_siblings(dbg,child,is_info,in_level+1,work,inval);
      dwarf_dealloc(dbg,child,DW_DLA_DIE);
      }
    /* res == DW_DLV_NO_ENTRY */
    res = dwarf_siblingof_b(dbg,cur_die,is_info,&sib_die,&error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_siblingof , level %d :%s \n",in_level,em);
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Done at this level. */
      break;
    }
    /* res == DW_DLV_OK */
    if(cur_die != in_die) {
      dwarf_dealloc(dbg,cur_die,DW_DLA_DIE);
    }
    cur_die = sib_die;
    inval = work(dbg,cur_die,in_level,inval);
  }
  return inval;
}

value
collect_info(Dwarf_Debug dbg, work_method work, value inval)
{

  //printf("In C, in collect_info\n");

  addrbase = 0;

  int cu_number = 0;

  /* Following are all parameters for function "dwarf_next_cu_header_d" */
  Dwarf_Unsigned cu_header_length = 0;
  Dwarf_Half version_stamp = 0;
  Dwarf_Unsigned abbrev_offset = 0;
  Dwarf_Half address_size = 0;
  Dwarf_Half offset_size = 0;
  Dwarf_Half extension_size = 0;
  Dwarf_Sig8 signature;
  Dwarf_Unsigned typeoffset = 0;
  Dwarf_Unsigned next_cu_header = 0;
  Dwarf_Half header_cu_type = DW_UT_compile;
  Dwarf_Bool is_info = 1; 
  Dwarf_Error error;
  
  for(;;++cu_number) {
    Dwarf_Die no_die = 0;
    Dwarf_Die cu_die = 0;
    int res = DW_DLV_ERROR;
    struct srcfilesdata sf;
    sf.srcfilesres = DW_DLV_ERROR;
    sf.srcfiles = 0;
    sf.srcfilescount = 0;
    memset(&signature, 0, sizeof(signature));
    
    /* find the next cu header; TODO: what is cu header? Compile Unit?*/
    res = dwarf_next_cu_header_d(dbg, is_info, &cu_header_length,
				 &version_stamp, &abbrev_offset,
				 &address_size, &offset_size,
				 &extension_size, &signature,
				 &typeoffset, &next_cu_header,
				 &header_cu_type, &error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_next_cu_header: %s\n",em);
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Done. */
      return inval;
    }

    /* find the sibling of  */
    res = dwarf_siblingof_b(dbg, no_die, is_info, &cu_die, &error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_siblingof on CU die: %s\n",em);
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Impossible case. */
      printf("no entry! in dwarf_siblingof on CU die \n");
      exit(1);
    }

    inval = get_die_and_siblings(dbg, cu_die, is_info, 0, work, inval);
    dwarf_dealloc(dbg, cu_die, DW_DLA_DIE);
    resetsrcfiles(dbg, &sf);
  }
  return inval;
}

value
create_void()
{
  CAMLparam0();

  CAMLreturn( Val_int(1) );
}

value
create_base(int size, char* basename)
{
  CAMLparam0();
  CAMLlocal1( basetype );
  
  basetype = caml_alloc(2,0);
  Store_field( basetype, 0, Val_int(size) );
  Store_field( basetype, 1, caml_copy_string(basename) );
  
  CAMLreturn( basetype );  
}

value
create_const(value ctype )
{
  CAMLparam1( ctype );
  CAMLlocal1( consttype );
  
  consttype = caml_alloc(1,1);
  Store_field( consttype, 0, ctype );
  
  CAMLreturn( consttype );
}

value
create_array(value ctype, value dimlist)
{
  CAMLparam2( ctype, dimlist );
  CAMLlocal1( arraytype );
  
  arraytype = caml_alloc(2,2);
  Store_field( arraytype, 0, ctype );
  Store_field( arraytype, 1, dimlist );

  CAMLreturn( arraytype );
}

value
create_pointer(int size, value ctype)
{
  CAMLparam1( ctype );
  CAMLlocal1( pointertype );
  
  pointertype = caml_alloc(2,3);
  Store_field( pointertype, 0, Val_int(size) );
  Store_field( pointertype, 1, ctype );

  CAMLreturn( pointertype );
}

value
create_typedef(char* name, value ctype)
{
  CAMLparam1( ctype );
  CAMLlocal1( typedeftype );
  
  typedeftype = caml_alloc(2,4);
  Store_field( typedeftype, 0, caml_copy_string(name) );
  Store_field( typedeftype, 1, ctype );
  
  CAMLreturn( typedeftype );
}

value
create_subroutine(value rettype, value paramlist)
{
  CAMLparam2( rettype, paramlist );
  CAMLlocal1( subroutine );
  
  subroutine = caml_alloc(2,5);
  Store_field( subroutine, 0, rettype );
  Store_field( subroutine, 1, paramlist );
  
  CAMLreturn( subroutine );
}

value
create_enumeration(int size, char* name, value etorlist )
{
  CAMLparam1( etorlist );
  CAMLlocal1( enumeration );
  
  enumeration = caml_alloc(3,6);
  Store_field( enumeration, 0, Val_int(size) );
  Store_field( enumeration, 1, caml_copy_string(name) );
  Store_field( enumeration, 2, etorlist );
  
  CAMLreturn( enumeration );
}

value
create_union(int size, char* name, value memlist)
{
  CAMLparam1( memlist );
  CAMLlocal1( uniontype );
  
  uniontype = caml_alloc(3,7);
  Store_field( uniontype, 0, Val_int(size) );
  Store_field( uniontype, 1, caml_copy_string(name) );
  Store_field( uniontype, 2, memlist );
  
  CAMLreturn( uniontype );
}

value
create_structure(int size, char* name, value memlist)
{
  CAMLparam1( memlist );
  CAMLlocal1( structtype );
  
  structtype = caml_alloc(3,8);
  Store_field( structtype, 0, Val_int(size) );
  Store_field( structtype, 1, caml_copy_string(name) );
  Store_field( structtype, 2, memlist );
  
  CAMLreturn( structtype );
}

value
create_volatile(value ctype)
{
  CAMLparam1( ctype );
  CAMLlocal1( volatiletype );
  
  volatiletype = caml_alloc(1,9);
  Store_field( volatiletype, 0, ctype );
  
  CAMLreturn( volatiletype );
}

value
create_unspec()
{
  CAMLparam0();
  
  CAMLreturn( Val_int(0) );
}

value
create_wrongtype(char* errmsg)
{
  CAMLparam0();
  CAMLlocal1( wrongtype );
  
  wrongtype = caml_alloc(1, 10);
  Store_field( wrongtype, 0, caml_copy_string(errmsg) );
  
  CAMLreturn( wrongtype );
}

static value
encode_subranges(Dwarf_Debug dbg, Dwarf_Die in_die)
{
  CAMLparam0();
  CAMLlocal1( dimlist );

  int res;
  Dwarf_Error error;
  Dwarf_Die sib_die = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Unsigned i = 0;
  Dwarf_Bool hasbound = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag in encode_subranges");
    assert(0);
  }
  
  if(tag == DW_TAG_subrange_type) {
    res = dwarf_hasattr(in_die,DW_AT_upper_bound,&hasbound,&error);
    if(res != DW_DLV_OK) {
      perror("error hasattr");
      assert(0);
    }
    
    dimlist = caml_alloc(2,0);
    
    if(hasbound == 1) {
      res = dwarf_attrlist(in_die,&attrbuf,&attrcount,&error);
      if(res != DW_DLV_OK) {
	perror("Error in dwarf_attrlist in encode_subranges");
        assert(0);
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK && aform == DW_AT_upper_bound) {
	  Dwarf_Unsigned upper_bound = 0;
	  Dwarf_Half attrform = 0; 
	  res = dwarf_whatform(attrbuf[i], &attrform, &error);
	  if(attrform == DW_FORM_data1 || attrform == DW_FORM_data2) {
	    get_uvalue(in_die, attrbuf[i], &upper_bound);
	    Store_field( dimlist, 0, Val_int(upper_bound+1) );
	  } else {
	    Store_field( dimlist, 0, Val_int(0) );
	  }
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else {
      Store_field( dimlist, 0, Val_int(0) );
    }
  }
  
  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_OK) {
    Store_field( dimlist, 1,
		 encode_subranges(dbg,sib_die) );
    dwarf_dealloc(dbg, sib_die, DW_DLA_DIE);
  }
  else if (res == DW_DLV_NO_ENTRY) {
    Store_field( dimlist, 1, Val_emptylist );
  }
  
  CAMLreturn( dimlist );
}

static value
encode_dimension(Dwarf_Debug dbg, Dwarf_Die parent_die)
{
  CAMLparam0();
  CAMLlocal1( dimlist );

  int res;
  Dwarf_Error error = 0;
  Dwarf_Die child_die = 0;
  int dimension = 0;

  res = dwarf_child(parent_die,&child_die,&error);
  if(res != DW_DLV_OK) {
    printf("Warning: no subrange child or error happened!");
    dimlist = Val_emptylist;
  } else {
    //dimension = count_tag(dbg,child_die,DW_TAG_subrange_type);
    dimlist = encode_subranges(dbg,child_die);
    dwarf_dealloc(dbg, child_die, DW_DLA_DIE);
  }
  
  CAMLreturn( dimlist );

}

value
encode_parameters(Dwarf_Debug dbg, Dwarf_Die in_die)
{
  CAMLparam0();
  CAMLlocal1( paramlist );

  int res;
  Dwarf_Error error;
  Dwarf_Die sib_die = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Unsigned i = 0;
  Dwarf_Bool hastype = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag");
    assert(0);
  }

  if(tag == DW_TAG_formal_parameter) {
    res = dwarf_hasattr(in_die,DW_AT_type,&hastype,&error);
    if(res != DW_DLV_OK) {
      perror("encode param hasattr error");
      assert(0);
    }
    if(hastype == 1) {
      paramlist = caml_alloc(2,0);
      res = dwarf_attrlist(in_die,&attrbuf,&attrcount,&error);
      if(res != DW_DLV_OK) {
	perror("Error in dwarf_attrlist");
	assert(0);
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK && aform == DW_AT_type) {
	  Dwarf_Off next_type_off = 0;
	  get_type_off(attrbuf[i], &next_type_off);
	  Store_field( paramlist, 0,
		       encode_type(dbg, next_type_off) );
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
    } else {
      //impossible case
      perror("Warning: Theoretically, a parameter cannot have void type");
      assert(0);
      //printf("0(void)");
    }
  } else if(tag == DW_TAG_unspecified_parameters) {
    paramlist = caml_alloc(2,0);    
    Store_field( paramlist, 0, create_unspec() );
  } else {
    perror("subroutine type contains non-param child dies\n");
    assert(0);
  }
  
  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_OK) {
    Store_field( paramlist, 1,
		 encode_parameters(dbg,sib_die) );
    dwarf_dealloc(dbg, sib_die, DW_DLA_DIE);
  }
  else if (res == DW_DLV_NO_ENTRY) {
    Store_field( paramlist, 1, Val_emptylist );
  } else {
    perror("siblingof error\n");
    assert(0);
  }
  
  CAMLreturn( paramlist );
}

value
encode_subprogram_parameters(Dwarf_Debug dbg, Dwarf_Die in_die)
{
  CAMLparam0( );
  CAMLlocal1( paramlist );

  int res;
  Dwarf_Error error;
  Dwarf_Die sib_die = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Unsigned i = 0;
  Dwarf_Bool hastype = 0;
  Dwarf_Bool hasabstr = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    // no-tag DIE means "end"
    paramlist = Val_emptylist;
    CAMLreturn( paramlist );
  }

  if(tag == DW_TAG_formal_parameter) {
    // decide where to find type information
    res = dwarf_hasattr(in_die,DW_AT_abstract_origin,&hasabstr,&error);
    if(res != DW_DLV_OK) {
      perror("error hasattr");
      assert(0);
    }
    if(hasabstr == 0) {
      // normal DIE; look for type information locally   
      res = dwarf_hasattr(in_die,DW_AT_type,&hastype,&error);
      if(res != DW_DLV_OK) {
	perror("encode param hasattr error");
	assert(0);
      }
      if(hastype == 1) {
	paramlist = caml_alloc(2,0);
	res = dwarf_attrlist(in_die,&attrbuf,&attrcount,&error);
	if(res != DW_DLV_OK) {
	  perror("Error in dwarf_attrlist");
	  assert(0);
	}
	for(i = 0; i < attrcount; ++i) {
	  Dwarf_Half aform;
	  res = dwarf_whatattr(attrbuf[i], &aform, &error);
	  if(res == DW_DLV_OK && aform == DW_AT_type) {
	    Dwarf_Off next_type_off = 0;
	    get_type_off(attrbuf[i], &next_type_off);
	    Store_field( paramlist, 0,
			 encode_type(dbg, next_type_off) );
	  }
	  dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
	}
	dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
      } else {
	//impossible case
	perror("Warning: Theoretically, a parameter cannot have void type");
	//printf("Abnormal parameter is\n");
	assert(0);
	//paramlist = caml_alloc(2,0);
	//Store_field( paramlist, 0, create_void() );
      }
    } else {
      // special DIE; look for type information remotely
      Dwarf_Die origin_die = 0;
      res = dwarf_attrlist(in_die,&attrbuf,&attrcount,&error);
      if(res != DW_DLV_OK) {
	perror("error attrlist");
	assert(0);
      }
      for(i = 0; i< attrcount; i++) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK && aform == DW_AT_abstract_origin) {
	  Dwarf_Off origin_off = 0;
	  get_type_off(attrbuf[i], &origin_off);
	  res = dwarf_offdie(dbg,origin_off,&origin_die,&error);
	  if(res != DW_DLV_OK) {
	    perror("error offdie");
	    assert(0);
	  }
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
      // now we have origin DIE
      res = dwarf_hasattr(origin_die,DW_AT_type,&hastype,&error);
      if(res != DW_DLV_OK) {
	perror("encode param hasattr error");
	assert(0);
      }
      if(hastype == 1) {
	paramlist = caml_alloc(2,0);
	res = dwarf_attrlist(origin_die,&attrbuf,&attrcount,&error);
	if(res != DW_DLV_OK) {
	  perror("Error in dwarf_attrlist");
	  assert(0);
	}
	for(i = 0; i < attrcount; ++i) {
	  Dwarf_Half aform;
	  res = dwarf_whatattr(attrbuf[i], &aform, &error);
	  if(res == DW_DLV_OK && aform == DW_AT_type) {
	    Dwarf_Off next_type_off = 0;
	    get_type_off(attrbuf[i], &next_type_off);
	    Store_field( paramlist, 0,
			 encode_type(dbg, next_type_off) );
	  }
	  dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
	}
	dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
      } else {
	//impossible case
	perror("Warning: Theoretically, a parameter cannot have void type");
	//printf("Abnormal parameter is\n");
	assert(0);
	//paramlist = caml_alloc(2,0);
	//Store_field( paramlist, 0, create_void() );
      }     
    }
  } else if(tag == DW_TAG_unspecified_parameters) {
    paramlist = caml_alloc(2,0);
    Store_field( paramlist, 0, create_unspec() );
  } else {
    res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
    if(res == DW_DLV_OK) {
      paramlist = //Val_emptylist;
	encode_subprogram_parameters(dbg,sib_die);
      dwarf_dealloc(dbg, sib_die, DW_DLA_DIE);
      CAMLreturn( paramlist );
    }
    else if (res == DW_DLV_NO_ENTRY) {
      CAMLreturn( Val_emptylist );
    } else {
      perror("siblingof error\n");
      assert(0);
    }
  }
  
  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_OK) {
    Store_field( paramlist, 1,
		 encode_subprogram_parameters(dbg,sib_die) );
    dwarf_dealloc(dbg, sib_die, DW_DLA_DIE);
  }
  else if (res == DW_DLV_NO_ENTRY) {
    Store_field( paramlist, 1, Val_emptylist );
  } else {
    perror("siblingof error\n");
    assert(0);
  }
  
  CAMLreturn( paramlist );
}

static value
encode_enumerators_i(Dwarf_Debug dbg, Dwarf_Die in_die)
{
  CAMLparam0( );
  CAMLlocal2( etorlist, pair );

  int i, res, res0;
  Dwarf_Error error;
  Dwarf_Die sib_die = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Bool hasname = 0;
  Dwarf_Bool hasval = 0;
  char* name = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag in encode_enumerators!");
    assert(0);
  }
   
  if(tag == DW_TAG_enumerator) {
    etorlist = caml_alloc(2,0);
    pair = caml_alloc(2,0);
    res = dwarf_hasattr(in_die,DW_AT_name,&hasname,&error);
    if(res != DW_DLV_OK) {
      perror("error hasattr");
      assert(0);
    }
    res = dwarf_hasattr(in_die,DW_AT_const_value,&hasval,&error);
    if(res != DW_DLV_OK) {
      perror("error hasattr");
      assert(0);
    }
    if(hasname == 1 && hasval == 1) {
      Store_field( pair,0,caml_copy_string("dummy etor") );
      res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
      if(res != DW_DLV_OK) {
	perror("Error in dwarf_attrlist in encode_enumerators");
        assert(0);
      }

      res = dwarf_diename(in_die,&name,&error);
      if(res != DW_DLV_OK) {
	perror("enumerator does not have name");
	assert(0);
      }
      //printf("etorname: %s\n", name);
      Store_field( pair, 0, caml_copy_string(name) );
      dwarf_dealloc(dbg, name, DW_DLA_STRING);

      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK && aform == DW_AT_const_value) {
	  Dwarf_Signed v = 0;
	  get_svalue(attrbuf[i], &v);
	  //printf("etorvalue: %" DW_PR_DSd "\n", v);
	  Store_field( pair, 1, Val_int(v) );
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      dwarf_dealloc(dbg,attrbuf,DW_DLA_LIST);

      Store_field( etorlist, 0, pair);
      res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
      if(res == DW_DLV_OK) {
	Store_field( etorlist, 1, //Val_emptylist );
		     encode_enumerators(dbg,sib_die) );
	dwarf_dealloc(dbg, sib_die, DW_DLA_DIE);
      } else if (res == DW_DLV_NO_ENTRY) {
	Store_field( etorlist, 1, Val_emptylist );
      } else {
	perror("siblingof error");
	assert(0);
      }
      CAMLreturn( etorlist );
    } else {
      // For now, stay strict to discover corner cases
      perror("Error no name or no value for enumerator in encode_enumerators!");
      assert(0);
    }
  } else {
    printf("ENUM's non-enumerator...\n");
    res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
    if(res == DW_DLV_OK) {
      etorlist = encode_enumerators(dbg,sib_die);
      dwarf_dealloc(dbg,sib_die,DW_DLA_DIE);
      CAMLreturn( etorlist );
    } else if (res == DW_DLV_NO_ENTRY) {
      CAMLreturn( Val_emptylist );
    } else {
      perror("siblingof error");
      assert(0);
    }
  }
  printf("Magical place!\n");
}

static value
encode_enumerators(Dwarf_Debug dbg, Dwarf_Die in_die)
{
  CAMLparam0();
  CAMLlocal1( etorlist );
  
  etorlist = encode_enumerators_i(dbg, in_die);
  
  CAMLreturn( etorlist );
}

static value
encode_union_members(Dwarf_Debug dbg, Dwarf_Die in_die)
{
  CAMLparam0();
  CAMLlocal2( pair, umemlist );

  int i, res;
  Dwarf_Error error;
  Dwarf_Die sib_die = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  char* die_name = 0;
  Dwarf_Bool hastype = 0;


  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag in encode_union_members!");
    assert(0);
  }
  
  if(tag == DW_TAG_member) {
    pair = caml_alloc(2,0);
    res = dwarf_hasattr(in_die, DW_AT_type, &hastype, &error);
    if(res == DW_DLV_OK && hastype == 1) {
      res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
      if(res != DW_DLV_OK) {
	perror("Error in dwarf_attrlist");
	assert(0);
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK && aform == DW_AT_type) {
	  Dwarf_Off next_type_off = 0;
	  get_type_off(attrbuf[i], &next_type_off);
	  Store_field( pair, 0, 
		       encode_type(dbg, next_type_off) );
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else {
      // impossible case;
      perror("Theoretically, a member cannot have void type");
      assert(0);
    }
    res = dwarf_diename(in_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      // impossible case
      printf("Warning: a member does not have name!\n");
      Store_field( pair, 1, caml_copy_string("anonymous"));
      //assert(0);
    } else {
      Store_field( pair, 1, caml_copy_string(die_name) );
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
  } else {
    perror("including non-member die");
    assert(0);
  }
  umemlist = caml_alloc(2,0);
  Store_field( umemlist, 0, pair );
  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_OK) {
    Store_field( umemlist, 1, 
    		 encode_union_members(dbg,sib_die) );
    dwarf_dealloc(dbg, sib_die, DW_DLA_DIE);
  }
  else if (res == DW_DLV_NO_ENTRY) {
    Store_field( umemlist, 1, Val_emptylist );
  } else {
    perror("error siblingof");
    assert(0);
  }

  CAMLreturn( umemlist );
}

value
encode_struct_members(Dwarf_Debug dbg, Dwarf_Die in_die)
{
  CAMLparam0();
  CAMLlocal2( tuple, smemlist );

  int res, i;
  Dwarf_Error error;
  Dwarf_Die sib_die = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  char* die_name = 0;
  Dwarf_Bool hastype = 0;
  Dwarf_Unsigned memloc = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag in encode_struct_members!");
    assert(0);
  }
  
  if(tag == DW_TAG_member) {
    tuple = caml_alloc(3,0);
    memloc = get_memloc(dbg, in_die);
    Store_field( tuple, 0, Val_int(memloc) );
    res = dwarf_hasattr(in_die, DW_AT_type, &hastype, &error);
    if(res == DW_DLV_OK && hastype == 1) {
      res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
      if(res != DW_DLV_OK) {
	perror("Error in dwarf_attrlist");
        assert(0);
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK && aform == DW_AT_type) {
	  Dwarf_Off next_type_off = 0;
	  get_type_off(attrbuf[i], &next_type_off);
	  Store_field( tuple, 1, //create_void() );
		       encode_type(dbg, next_type_off) );
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      dwarf_dealloc(dbg,attrbuf, DW_DLA_LIST);
    } else {
      // impossible case;
      perror("Theoretically, a member cannot have void type");
      assert(0);
      //printf("0(void)");
    }

    res = dwarf_diename(in_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      // impossible case
      Store_field( tuple, 2, caml_copy_string("dummy_name") );
      /*
      perror("failed to retrieve the name of a member!\n");
      Dwarf_Bool hasname = 0;
      res = dwarf_hasattr(in_die, DW_AT_name, &hasname, &error);
      if (res == DW_DLV_OK && hasname == 1) 
	perror("dwarf_diename cannot handle the name\n");
      else if (res == DW_DLV_OK)
	perror("the member indeed does not have a name\n");
      else
	perror("Error in hasattr");
      assert(0);
      */
    } else {
      Store_field( tuple, 2, caml_copy_string(die_name) );
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
    smemlist = caml_alloc(2,0);
    Store_field( smemlist, 0, tuple );
    res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
    if(res == DW_DLV_OK) {
      Store_field( smemlist, 1, //Val_emptylist );
		   encode_struct_members(dbg,sib_die) );
      dwarf_dealloc(dbg,sib_die,DW_DLA_DIE);
      CAMLreturn( smemlist );
    } else if (res == DW_DLV_NO_ENTRY) {
      Store_field( smemlist, 1, Val_emptylist );
      CAMLreturn( smemlist );
    } else {
      perror("error siblingof");
      assert(0);
    }
  } else {
    res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
    if(res == DW_DLV_OK) {
      smemlist = encode_struct_members(dbg,sib_die);
      dwarf_dealloc(dbg,sib_die,DW_DLA_DIE);
      CAMLreturn( smemlist );
    } else if (res == DW_DLV_NO_ENTRY) {
      smemlist = Val_emptylist;
      CAMLreturn( smemlist );
    } else {
      perror("error siblingof");
      assert(0);
    }
  }
  
}

static int in_typedef = 0;

value
encode_type_i(Dwarf_Debug dbg, Dwarf_Off type_off)
{
  CAMLparam0();
  CAMLlocal1( type );

  int res, i;
  Dwarf_Error error = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Die start_die = 0;
  Dwarf_Die child_die = 0;
  char * die_name = 0;
  Dwarf_Bool hastype;
  Dwarf_Bool hassize;
  Dwarf_Unsigned size = 0;

  res = dwarf_offdie(dbg, type_off, &start_die, &error);
  if(res != DW_DLV_OK) {
    printf("offset:\n%" DW_PR_DUx "\n",type_off);
    perror("No type or wrong offset in encode_type_i");
    assert(0);
  }
  
  res = dwarf_tag(start_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag in encode_type_i");
    assert(0);
  }

  switch(tag) {
  case DW_TAG_base_type: //$B0 Done
    in_typedef = 0;
    res = dwarf_diename(start_die, &die_name, &error);
    if(res != DW_DLV_OK) {
      perror("ERROR: base type does not have name!");
      assert(0);
    } else {
      size = get_type_size(dbg,start_die);
      type = create_base(size,die_name);
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
    break;

  case DW_TAG_const_type: //$B1 Done  
    res = dwarf_hasattr(start_die,DW_AT_type,&hastype,&error);
    if(res != DW_DLV_OK) {
      perror("error hasattr");
      assert(0);
    }
    if(hastype == 1) {
      res = dwarf_attrlist(start_die, &attrbuf, &attrcount, &error);
      if(res == DW_DLV_ERROR) {
	perror("Attrlist error!");
	assert(0);
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK && aform == DW_AT_type) {
	  Dwarf_Off next_type_off = 0;
	  get_type_off(attrbuf[i], &next_type_off);
	  type = create_const(encode_type_i(dbg,next_type_off));
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
    } else {
      // possible case:
      // const (void *) is represented as (const void) *
      if(in_typedef > 0) in_typedef--;
      type = create_const(create_void());
    }
    break;

  case DW_TAG_array_type: //$2 Done
    in_typedef = 0;
    res = dwarf_hasattr(start_die,DW_AT_type,&hastype,&error);
    if(res == DW_DLV_OK && hastype == 1) {
      res = dwarf_attrlist(start_die, &attrbuf, &attrcount, &error);
      if(res == DW_DLV_ERROR) {
	perror("Attrlist error!");
	assert(0);
      }      
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK && aform == DW_AT_type) {
	  Dwarf_Off next_type_off = 0;
	  get_type_off(attrbuf[i], &next_type_off);
	  type = create_array(encode_type_i(dbg,next_type_off),
			      encode_dimension(dbg,start_die));
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
    } else {
      perror("Error: Array of no type!");
      assert(0);
    }
    break;

  case DW_TAG_pointer_type: //$B3 Done
    in_typedef = 0;
    res = dwarf_hasattr(start_die, DW_AT_type, &hastype, &error);
    if(res != DW_DLV_OK) {
      perror("error hasattr");
      assert(0);
    }
    size = get_type_size(dbg,start_die);
    if(hastype == 1) {
      res = dwarf_attrlist(start_die, &attrbuf, &attrcount, &error);
      if(res == DW_DLV_ERROR) {
	perror("Attrlist error!");
	assert(0);
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK && aform == DW_AT_type) {
	  Dwarf_Off next_type_off = 0;
	  get_type_off(attrbuf[i], &next_type_off);
	  type = create_pointer(size,
				encode_type_i(dbg,next_type_off));
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
    } else {
      type = create_pointer(size,
			    create_void());
    }
    break;
 
  case DW_TAG_typedef: //$B4 Done
    if(in_typedef == 0) in_typedef = 1;
    res = dwarf_diename(start_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      perror("typedef does not have def-name!");
      assert(0);
    }
    res = dwarf_hasattr(start_die, DW_AT_type, &hastype, &error);
    if(res != DW_DLV_OK) {
      perror("typedef hastype error!");
      assert(0);
    }
    
    if(hastype == 1) {
      res = dwarf_attrlist(start_die, &attrbuf, &attrcount, &error);
      if(res == DW_DLV_ERROR) {
	perror("Attrlist error!");
	assert(0);
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK && aform == DW_AT_type) {
	  Dwarf_Off next_type_off = 0;
	  get_type_off(attrbuf[i], &next_type_off);
	  type = create_typedef( die_name, //create_void() );
				 encode_type_i(dbg,next_type_off));
	  dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
    } else {
      //possible case: void
      assert(in_typedef == 1);
      in_typedef = 0;
      type = create_typedef(die_name,
			    create_void());
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
    break;

  case DW_TAG_subroutine_type: //$B5 Done
    in_typedef = 0;
    res = dwarf_hasattr(start_die, DW_AT_type, &hastype, &error);
    if(res != DW_DLV_OK) {
      perror("subroutine hastype error!");
      assert(0);
    }
    if(hastype == 1) {
      res = dwarf_attrlist(start_die, &attrbuf, &attrcount, &error);
      if(res == DW_DLV_ERROR) {
	perror("Attrlist error!");
	assert(0);
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK && aform == DW_AT_type) {
	  Dwarf_Off next_type_off = 0;
	  get_type_off(attrbuf[i], &next_type_off);
	  type = create_subroutine(encode_type_i(dbg, next_type_off), Val_emptylist);
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
    } else {
      type = create_subroutine(create_void(), Val_emptylist);
    }
    
    res = dwarf_child(start_die,&child_die,&error);
    if(res == DW_DLV_NO_ENTRY) {
      Store_field( type, 1, Val_emptylist );
    } else if(res == DW_DLV_OK){
      Store_field( type, 1,
		   encode_parameters(dbg,child_die) );
      dwarf_dealloc(dbg, child_die, DW_DLA_DIE);
    } else {
      perror("Error: when encoding PARAMETERS!\n");
      assert(0);
    }
    break;
    
  case DW_TAG_enumeration_type://$B6 Done
    in_typedef = 0;
    size = get_type_size(dbg,start_die);
    res = dwarf_diename(start_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      // possible case with typedef;
      type = create_enumeration(size,"",Val_emptylist);
    } else {
      type = create_enumeration(size,die_name,Val_emptylist);
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
    res = dwarf_child(start_die,&child_die,&error);
    if(res == DW_DLV_NO_ENTRY) {
      // impossible case
      perror("no enumerator child or dbg error happened!");
      assert(0);
    } else if(res == DW_DLV_OK) {
      Store_field( type, 2, //Val_emptylist );
		   encode_enumerators(dbg,child_die) );
      dwarf_dealloc(dbg, child_die, DW_DLA_DIE);
    } else {
      perror("Error: when encoding ENUMERATORS");
      assert(0);
    }
    break;
    
  case DW_TAG_union_type: //$B7 Done
    res = dwarf_hasattr(start_die,DW_AT_byte_size,&hassize,&error);
    if(res != DW_DLV_OK) {
      perror("error hasattr");
      assert(0);
    }
    if(hassize == 1) {
      size = get_type_size(dbg,start_die);
    } else {
      size = 0;
    }
    
    res = dwarf_diename(start_die,&die_name,&error);
    if(res != DW_DLV_OK && in_typedef == 0) {
      type = create_union(size,"",Val_emptylist);
      res = dwarf_child(start_die,&child_die,&error);
      if(res == DW_DLV_NO_ENTRY) {
	Store_field( type, 2, Val_emptylist );
      } else if(res == DW_DLV_OK) {
	Store_field( type, 2, 
		     encode_union_members(dbg,child_die) );
	dwarf_dealloc(dbg, child_die, DW_DLA_DIE);
      } else {
	perror("Error: while encoding UNION MEMBERS");
	assert(0);
      }
    } else if(res != DW_DLV_OK) {
      assert(in_typedef == 1);
      in_typedef = 0;
      type = create_union(size,"",Val_emptylist);
    } else {
      assert(res == DW_DLV_OK);
      in_typedef = 0;
      type = create_union(size,die_name,Val_emptylist);
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
    break;

  case DW_TAG_structure_type: //$B8 Done
    res = dwarf_hasattr(start_die,DW_AT_byte_size,&hassize,&error);
    if(res != DW_DLV_OK) {
      perror("error hasattr");
      assert(0);
    }
    if(hassize == 0) {
      size = 0;
    } else {
      size = get_type_size(dbg,start_die);
    }
    res = dwarf_diename(start_die,&die_name,&error);
    if(res != DW_DLV_OK && in_typedef == 0) {
      CAMLlocal1( memlist );
      res = dwarf_child(start_die,&child_die,&error);
      if(res == DW_DLV_NO_ENTRY) {
	memlist = Val_emptylist;
	type = create_structure(size, "", memlist);
      } else if(res == DW_DLV_OK) {	
	memlist = //Val_emptylist;
	  encode_struct_members(dbg,child_die);
	type = create_structure(size, "", memlist);		
	dwarf_dealloc(dbg,child_die,DW_DLA_DIE);
      } else {
	perror("Error: while encoding STRUCT MEMBERS");
	assert(0);
      }
    } else if(res != DW_DLV_OK) {
      assert(in_typedef == 1);
      in_typedef = 0;
      type = create_structure(size,"",Val_emptylist);
    } else {
      assert(res == DW_DLV_OK);
      in_typedef = 0;
      type = create_structure(size,die_name,Val_emptylist);
      dwarf_dealloc(dbg, die_name, DW_DLA_STRING);
    }
    break;

  case DW_TAG_volatile_type: //$B9
    res = dwarf_hasattr(start_die, DW_AT_type, &hastype, &error);
    if(res == DW_DLV_OK && hastype == 1) {
      res = dwarf_attrlist(start_die, &attrbuf, &attrcount, &error);
      if(res == DW_DLV_ERROR) {
	perror("Attrlist error!");
	assert(0);
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK && aform == DW_AT_type) {
	  Dwarf_Off next_type_off = 0;
	  get_type_off(attrbuf[i], &next_type_off);
	  type = create_volatile(encode_type_i(dbg, next_type_off));
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
    } else {
      // impossible case??? found it possible in practice
      // printf("Warning: Volatile void!");
      type = create_volatile(create_void());
    }
    break;

  default:
    {
      in_typedef = 0;
      type = create_wrongtype("unsupported");
      break;
    }
  }
  
  dwarf_dealloc(dbg, start_die, DW_DLA_DIE);
  CAMLreturn( type );
}

value
encode_type(Dwarf_Debug dbg, Dwarf_Off type_off)
{
  CAMLparam0( );
  CAMLlocal1( encoded_type );
  
  in_typedef = 0;
  encoded_type = encode_type_i(dbg,type_off);
  assert(in_typedef == 0);

  CAMLreturn( encoded_type );
}

struct operation_descr_s {
    int op_code;
    int op_count;
    const char * op_1type;
};

struct operation_descr_s opdesc[]= {
    {DW_OP_addr,1,"addr" },
    {DW_OP_deref,0 },
    {DW_OP_const1u,1,"1u" },
    {DW_OP_const1s,1,"1s" },
    {DW_OP_const2u,1,"2u" },
    {DW_OP_const2s,1,"2s" },
    {DW_OP_const4u,1,"4u" },
    {DW_OP_const4s,1,"4s" },
    {DW_OP_const8u,1,"8u" },
    {DW_OP_const8s,1,"8s" },
    {DW_OP_constu,1,"uleb" },
    {DW_OP_consts,1,"sleb" },
    {DW_OP_dup,0,""},
    {DW_OP_drop,0,""},
    {DW_OP_over,0,""},
    {DW_OP_pick,1,"1u"},
    {DW_OP_swap,0,""},
    {DW_OP_rot,0,""},
    {DW_OP_xderef,0,""},
    {DW_OP_abs,0,""},
    {DW_OP_and,0,""},
    {DW_OP_div,0,""},
    {DW_OP_minus,0,""},
    {DW_OP_mod,0,""},
    {DW_OP_mul,0,""},
    {DW_OP_neg,0,""},
    {DW_OP_not,0,""},
    {DW_OP_or,0,""},
    {DW_OP_plus,0,""},
    {DW_OP_plus_uconst,1,"uleb"},
    {DW_OP_shl,0,""},
    {DW_OP_shr,0,""},
    {DW_OP_shra,0,""},
    {DW_OP_xor,0,""},
    {DW_OP_skip,1,"2s"},
    {DW_OP_bra,1,"2s"},
    {DW_OP_eq,0,""},
    {DW_OP_ge,0,""},
    {DW_OP_gt,0,""},
    {DW_OP_le,0,""},
    {DW_OP_lt,0,""},
    {DW_OP_ne,0,""},
    /* lit0 thru reg31 handled specially, no operands */
    /* breg0 thru breg31 handled specially, 1 operand */
    {DW_OP_regx,1,"uleb"},
    {DW_OP_fbreg,1,"sleb"},
    {DW_OP_bregx,2,"uleb"},
    {DW_OP_piece,1,"uleb"},
    {DW_OP_deref_size,1,"1u"},
    {DW_OP_xderef_size,1,"1u"},
    {DW_OP_nop,0,""},
    {DW_OP_push_object_address,0,""},
    {DW_OP_call2,1,"2u"},
    {DW_OP_call4,1,"4u"},
    {DW_OP_call_ref,1,"off"},
    {DW_OP_form_tls_address,0,""},
    {DW_OP_call_frame_cfa,0,""},
    {DW_OP_bit_piece,2,"uleb"},
    {DW_OP_implicit_value,2,"u"},
    {DW_OP_stack_value,0,""},
    {DW_OP_GNU_uninit,0,""},
    {DW_OP_GNU_encoded_addr,1,"addr"},
    {DW_OP_implicit_pointer,2,"addr" }, /* DWARF5 */
    {DW_OP_GNU_implicit_pointer,2,"addr" },
    {DW_OP_entry_value,2,"val" }, /* DWARF5 */
    {DW_OP_GNU_entry_value,2,"val" },
    {DW_OP_const_type,3,"uleb" }, /* DWARF5 */
    {DW_OP_GNU_const_type,3,"uleb" },
    {DW_OP_regval_type,2,"uleb" }, /* DWARF5 */
    {DW_OP_GNU_regval_type,2,"uleb" },
    {DW_OP_deref_type,1,"val" }, /* DWARF5 */
    {DW_OP_GNU_deref_type,1,"val" },
    {DW_OP_convert,1,"uleb" }, /* DWARF5 */
    {DW_OP_GNU_convert,1,"uleb" },
    {DW_OP_reinterpret,1,"uleb" }, /* DWARF5 */
    {DW_OP_GNU_reinterpret,1,"uleb" },

    {DW_OP_GNU_parameter_ref,1,"val" },
    {DW_OP_GNU_const_index,1,"val" },
    {DW_OP_GNU_push_tls_address,0,"" },

    {DW_OP_addrx,1,"uleb" }, /* DWARF5 */
    {DW_OP_GNU_addr_index,1,"val" },
    {DW_OP_constx,1,"u" }, /* DWARF5 */
    {DW_OP_GNU_const_index,1,"u" },

    {DW_OP_GNU_parameter_ref,1,"u" },

    {DW_OP_xderef,0,"" }, /* DWARF5 */
    {DW_OP_xderef_type,2,"1u" }, /* DWARF5 */
    /* terminator */
    {0,0,""}
};

static int
op_has_no_operands(int op)
{
    unsigned i = 0;
    if (op >= DW_OP_lit0 && op <= DW_OP_reg31) {
        return 1;
    }
    for (; ; ++i) {
        struct operation_descr_s *odp = opdesc+i;
        if (odp->op_code == 0) {
            break;
        }
        if (odp->op_code != op) {
            continue;
        }
        if (odp->op_count == 0) {
            return 1;
        }
        return 0;
    }
    return 0;
}

static void
show_contents(unsigned int length, const unsigned char * bp, char** expr_enc)
{
    unsigned int i = 0;

    if(!length) {
        return;
    }
    if(!bp) {
      sprintf(*expr_enc,"%s contents 0x", *expr_enc);
      for (; i < length; ++i) {
	/*  Do not use DW_PR_DUx here,
	    the value  *bp is a const unsigned char. */
	sprintf(*expr_enc,"%s%02x", *expr_enc, 0);
      }
    } else {
      sprintf(*expr_enc,"%s contents 0x", *expr_enc);
      for (; i < length; ++i,++bp) {
	/*  Do not use DW_PR_DUx here,
	    the value  *bp is a const unsigned char. */
	sprintf(*expr_enc,"%s%02x", *expr_enc, *bp);
      }
    }
}

void
encode_expr(Dwarf_Loc expr, int index, char** expr_enc)
{
  int res;

  Dwarf_Small op = expr.lr_atom;
  const char *op_name = NULL;
  Dwarf_Unsigned opd1 = expr.lr_number;
  Dwarf_Unsigned opd2 = expr.lr_number2;
  Dwarf_Unsigned opd3 = 0;
  Dwarf_Unsigned offsetforbranch = expr.lr_offset;

  *expr_enc = malloc(1024);
  memset(*expr_enc, '\0', 1024);

  res = dwarf_get_OP_name(op, &op_name);
  //printf("op_name:%s\n", op_name);
  if(res == DW_DLV_OK) 
    sprintf(*expr_enc, "%s%s", *expr_enc, op_name);
  else {
    perror("op_name error\n");
    assert(0);
  }
  if(op_has_no_operands(op)) {
    /* Nothing to add */
  } else if(op >= DW_OP_breg0 && op <= DW_OP_breg31) {
    sprintf(*expr_enc,"%s %" DW_PR_DSd, *expr_enc, (Dwarf_Signed) opd1);
  } else {
    switch (op) {
        case DW_OP_addr:
	  sprintf(*expr_enc, "%s 0x%" DW_PR_DUx, *expr_enc, opd1);
	  break;
        case DW_OP_const1s:
        case DW_OP_const2s:
        case DW_OP_const4s:
        case DW_OP_const8s:
        case DW_OP_consts:
        case DW_OP_skip:
        case DW_OP_bra:
        case DW_OP_fbreg:
	  sprintf(*expr_enc, "%s %" DW_PR_DSd, *expr_enc, (Dwarf_Signed) opd1);
	  break;
        case DW_OP_GNU_addr_index: /* unsigned val */
        case DW_OP_addrx: /* DWARF5: unsigned val */
        case DW_OP_GNU_const_index:
        case DW_OP_constx: /* DWARF5: unsigned val */
        case DW_OP_const1u:
        case DW_OP_const2u:
        case DW_OP_const4u:
        case DW_OP_const8u:
        case DW_OP_constu:
        case DW_OP_pick:
        case DW_OP_plus_uconst:
        case DW_OP_regx:
        case DW_OP_piece:
        case DW_OP_deref_size:
        case DW_OP_xderef_size:
	  sprintf(*expr_enc, "%s %" DW_PR_DUu, *expr_enc, opd1);
	  break;
        case DW_OP_bregx:
	  sprintf(*expr_enc, "%s 0x%"  DW_PR_DUx, *expr_enc, opd1);
	  sprintf(*expr_enc, "%s %" DW_PR_DSd, *expr_enc, (Dwarf_Signed) opd2);
	  break;
        case DW_OP_call2:
	  sprintf(*expr_enc,"%s 0x%"  DW_PR_DUx, *expr_enc, opd1);
	  break;
        case DW_OP_call4:
	  sprintf(*expr_enc,"%s 0x%"  DW_PR_DUx, *expr_enc, opd1);
	  break;
        case DW_OP_call_ref:
	  sprintf(*expr_enc,"%s 0x%"  DW_PR_DUx, *expr_enc, opd1);
	  break;
        case DW_OP_bit_piece:
	  sprintf(*expr_enc,"%s 0x%"   DW_PR_DUx, *expr_enc, opd1);
	  sprintf(*expr_enc,"%s offset 0x%" DW_PR_DUx, *expr_enc, (Dwarf_Signed) opd2);
	  break;
        case DW_OP_implicit_value:
            {
#define IMPLICIT_VALUE_PRINT_MAX 12
                unsigned int print_len = 0;
                sprintf(*expr_enc,"%s 0x%"  DW_PR_DUx, *expr_enc, opd1);
                /*  The other operand is a block of opd1 bytes. */
                /*  FIXME */
                print_len = opd1;
                if (print_len > IMPLICIT_VALUE_PRINT_MAX) {
                    print_len = IMPLICIT_VALUE_PRINT_MAX;
                }
#undef IMPLICIT_VALUE_PRINT_MAX
                {
                    const unsigned char *bp = 0;
                    /*  This is a really ugly cast, a way
                        to implement DW_OP_implicit value in
                        this libdwarf context. */
                    bp = (const unsigned char *) opd2;
                    show_contents(print_len,bp,expr_enc);
                }
            }
            break;

        /* We do not know what the operands, if any, are. */
        case DW_OP_HP_unknown:
        case DW_OP_HP_is_value:
        case DW_OP_HP_fltconst4:
        case DW_OP_HP_fltconst8:
        case DW_OP_HP_mod_range:
        case DW_OP_HP_unmod_range:
        case DW_OP_HP_tls:
        case DW_OP_INTEL_bit_piece:
            break;
        case DW_OP_stack_value:  /* DWARF4 */
            break;
        case DW_OP_GNU_uninit:  /* DW_OP_APPLE_uninit */
            /* No operands. */
            break;
        case DW_OP_GNU_encoded_addr:
            sprintf(*expr_enc,"%s 0x%"   DW_PR_DUx, *expr_enc, opd1);
            break;
        case DW_OP_implicit_pointer:       /* DWARF5 */
        case DW_OP_GNU_implicit_pointer:
            sprintf(*expr_enc,"%s 0x%"   DW_PR_DUx, *expr_enc, opd1);
            sprintf(*expr_enc,"%s %" DW_PR_DSd, *expr_enc, (Dwarf_Signed)opd2);
            break;
        case DW_OP_entry_value:       /* DWARF5 */
        case DW_OP_GNU_entry_value: {
            const unsigned char *bp = 0;
            unsigned int length = 0;
            length = opd1;
            sprintf(*expr_enc,"%s 0x%"   DW_PR_DUx, *expr_enc, opd1);
            bp = (Dwarf_Small *) opd2;
            if (!bp) {
	      perror("ERROR: Null databyte pointer DW_OP_entry_value");
	      exit(1);
            } else {
	      show_contents(length,bp,expr_enc);
            }
	}
	  break;
        case DW_OP_const_type:           /* DWARF5 */
        case DW_OP_GNU_const_type:
            {
            const unsigned char *bp = 0;
            unsigned int length = 0;
            sprintf(*expr_enc,"%s 0x%"   DW_PR_DUx, *expr_enc, opd1);

            length = opd2;
	    sprintf(*expr_enc,"%s const length: ", *expr_enc);
            sprintf(*expr_enc,"%s%u", *expr_enc, length);

            /* Now point to the data bytes of the const. */
            bp = (Dwarf_Small *) opd3;
	    /*
            if (!bp) {
	      printf("%d\n", length);
	      perror("ERROR: Null databyte pointer DW_OP_const_type ");
	      exit(1);
	      } else {*/
	    show_contents(length,bp,expr_enc);

            }
            break;
        case DW_OP_regval_type:           /* DWARF5 */
        case DW_OP_GNU_regval_type:
            sprintf(*expr_enc,"%s 0x%" DW_PR_DUx, *expr_enc, opd1);
            sprintf(*expr_enc,"%s 0x%"   DW_PR_DUx, *expr_enc, opd2);
            break;
        case DW_OP_deref_type: /* DWARF5 */
        case DW_OP_GNU_deref_type:
            sprintf(*expr_enc,"%s 0x%02" DW_PR_DUx, *expr_enc, opd1);
            sprintf(*expr_enc,"%s 0x%"   DW_PR_DUx, *expr_enc, opd2);
            break;
        case DW_OP_convert: /* DWARF5 */
        case DW_OP_GNU_convert:
        case DW_OP_reinterpret: /* DWARF5 */
        case DW_OP_GNU_reinterpret:
        case DW_OP_GNU_parameter_ref:
            sprintf(*expr_enc,"%s 0x%02"  DW_PR_DUx, *expr_enc, opd1);
            break;
        default:
            {
                sprintf(*expr_enc,"%s dwarf_op unknown 0x%x", *expr_enc, (unsigned)op);
            }
            break;
        }
  }

  return;
}

value
encode_locexprs(Dwarf_Debug dbg, Dwarf_Loc* exprs, Dwarf_Unsigned num)
{
  CAMLparam0();
  CAMLlocal1( locexprs );
  CAMLlocal1( cons );

  int res, i;
  Dwarf_Error error;
  char* expr_enc = NULL;
  
  locexprs = Val_emptylist;
  
  for(i=0; i<num; i++) {
    encode_expr(exprs[i], i, &expr_enc);
    //printf("expr_enc: %s\n", expr_enc);
    cons = caml_alloc(2,0);
    Store_field( cons, 0, caml_copy_string(expr_enc) );
    Store_field( cons, 1, locexprs );
    locexprs = cons;
    free(expr_enc);
  }
  
  CAMLreturn( locexprs );
}

value
encode_sinfo(Dwarf_Debug dbg, Dwarf_Die in_die)
{
  CAMLparam0();
  CAMLlocal2( locenc, locexprs );
  CAMLlocal2( triple, cons);

  int res;
  int i, j;
  Dwarf_Error error;
  
  Dwarf_Bool haslocation;
  Dwarf_Attribute *attrbuf;
  Dwarf_Signed attrcount = 0;

  Dwarf_Signed loc_num;
  Dwarf_Locdesc **loclist;
  
  char lopcstr[100];
  char hipcstr[100];
  
  locenc = Val_emptylist;

  res = dwarf_hasattr(in_die, DW_AT_location, &haslocation, &error);
  if(res != DW_DLV_OK) {
    perror("Error in hasattr in sinfo\n");
    assert(0);
  }
  if(haslocation != 1) {
    perror("Error: no location in encode_sinfo");
    //printf("NULL\n}");
    assert(0);
  }
  
  res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
  if(res != DW_DLV_OK) {
    perror("error attrlist in encode_sinfo");
    assert(0);
  }

  for(i = 0; i < attrcount; ++i) {
    Dwarf_Half aform;
    res = dwarf_whatattr(attrbuf[i], &aform, &error);
    if(res == DW_DLV_OK && aform == DW_AT_location) {
      res = dwarf_loclist_n(attrbuf[i], &loclist, &loc_num, &error);
      if(res != DW_DLV_OK) {
	perror("error loclist_n");
	assert(0);
      }
    }
    dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
  }
  dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);

  for(j = 0; j < loc_num; ++j) {
    if(loclist[j]->ld_lopc != 0 
       && loclist[j]->ld_lopc < addrbase 
       && loclist[j]->ld_hipc < addrbase
       && loclist[j]->ld_hipc != -1 ) {
      sprintf(lopcstr, "0x%" DW_PR_DUx, loclist[j]->ld_lopc + addrbase);
      sprintf(hipcstr, "0x%" DW_PR_DUx, loclist[j]->ld_hipc + addrbase);
    } else {
      sprintf(lopcstr, "0x%" DW_PR_DUx, loclist[j]->ld_lopc);
      sprintf(hipcstr, "0x%" DW_PR_DUx, loclist[j]->ld_hipc);
    }
    triple = caml_alloc(3,0);
    Store_field( triple, 0, caml_copy_string(lopcstr) );
    Store_field( triple, 1, caml_copy_string(hipcstr) );
	    
    locexprs = encode_locexprs(dbg, loclist[j]->ld_s, loclist[j]->ld_cents);
    Store_field( triple, 2, locexprs );

    cons = caml_alloc(2,0);
    Store_field( cons, 0, triple );
    Store_field( cons, 1, locenc) ;

    locenc = cons;

    dwarf_dealloc(dbg, loclist[j]->ld_s, DW_DLA_LOC_BLOCK);
  }
  dwarf_dealloc(dbg, loclist, DW_DLA_LOCDESC);

  CAMLreturn( locenc );
}

value
encode_type_info(Dwarf_Debug dbg, Dwarf_Off type_off, 
		 Dwarf_Die in_die, value inval)
{
  CAMLparam1(inval);
  CAMLlocal2(tyenc, locenc);
  CAMLlocal2(pair,cons);

  tyenc = encode_type(dbg, type_off);
  locenc = encode_sinfo(dbg, in_die);
    
  pair = caml_alloc(2,0);
  cons = caml_alloc(2,0);
    
  Store_field( pair, 0, tyenc );
  Store_field( pair, 1, locenc );
    
  Store_field( cons, 0, pair);
  Store_field( cons, 1, inval);
    
  inval = cons;
    
  CAMLreturn(inval);
}

int
in_map(char* name, char** struct_map, int mapsize)
{
  int i = 0;
  if(mapsize == 0) return 0;

  for(i=0;i<mapsize;i++) {
    if(strcmp(name,struct_map[i]) == 0) return 1;
  }
  
  return 0;
}

static int
inc_map(char* name, char** struct_map, int mapsize)
{
  mapsize++;
  struct_map[mapsize-1] = malloc(100);
  memset(struct_map[mapsize-1], '\0', 100);
  sprintf(struct_map[mapsize-1],"%s",name);
  return mapsize;
}

value
encode_detail(Dwarf_Debug dbg, Dwarf_Die in_die)
{  
  CAMLparam0();
  CAMLlocal1( type );
  int i, res;
  Dwarf_Error error;
  Dwarf_Half tag = 0;
  Dwarf_Attribute* attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  char* die_name = 0;
  Dwarf_Bool hastype = 0;
  Dwarf_Off type_off = 0;
  Dwarf_Die type_die = 0;
  Dwarf_Die child_die = 0;
  Dwarf_Unsigned size;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag");
    assert(0);
  }

  res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
  if(res == DW_DLV_ERROR) {
    perror("Attrlist error!");
    assert(0);
  }

  switch(tag) {
  case DW_TAG_structure_type: //$8
    type = caml_alloc(3,8);
    size = get_type_size(dbg,in_die);
    Store_field( type, 0, Val_int(size) );
    res = dwarf_diename(in_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      Store_field( type, 1, caml_copy_string("") );
    } else {
      Store_field( type, 1, caml_copy_string(die_name) );
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }

    res = dwarf_child(in_die,&child_die,&error);
    if(res == DW_DLV_NO_ENTRY) {
      Store_field( type, 2, Val_emptylist );
    } else if(res == DW_DLV_OK) {
      Store_field( type, 2, //Val_emptylist );
            	   encode_struct_members(dbg,child_die) );
      dwarf_dealloc(dbg,child_die,DW_DLA_DIE);
    } else {
      perror("Error: while encoding STRUCT MEMBERS");
      assert(0);
    }
    break;
    
  case DW_TAG_typedef: //$B4
    type = caml_alloc(2,4);
    res = dwarf_diename(in_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      //impossible
      assert(0);
    } else {
      Store_field( type, 0, caml_copy_string(die_name) );
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
    
    for(i = 0; i < attrcount; ++i) {
      Dwarf_Half aform;
      res = dwarf_whatattr(attrbuf[i], &aform, &error);
      if(res == DW_DLV_OK && aform == DW_AT_type) {
	get_type_off(attrbuf[i], &type_off);
      }
      dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
    }

    res = dwarf_offdie(dbg, type_off, &type_die, &error);
    if(res != DW_DLV_OK) {
      perror("No type or wrong offset in typedef in encode_type_rec");
      assert(0);
    }
    Store_field( type, 1,// Val_int(1) );
    		 encode_detail(dbg, type_die) );
    dwarf_dealloc(dbg,type_die,DW_DLA_DIE);
    break;
    
  case DW_TAG_union_type:
    size = get_type_size(dbg,in_die);
    res = dwarf_diename(in_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      type = create_union(size, "", Val_emptylist);
    } else {
      type = create_union(size, die_name, Val_emptylist);
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }

    res = dwarf_child(in_die,&child_die,&error);
    if(res == DW_DLV_NO_ENTRY) {
      Store_field( type, 2, Val_emptylist );
    } else if(res == DW_DLV_OK) {
      Store_field( type, 2, //Val_emptylist );
            	   encode_union_members(dbg,child_die) );
      dwarf_dealloc(dbg,child_die,DW_DLA_DIE);
    } else {
      perror("Error: while encoding STRUCT MEMBERS");
      assert(0);
    }
    break;

  default: 
    // should be impossible
    perror("should be impossible");
    assert(0);
  }
    
  CAMLreturn( type );
}

value
encode_relatives(Dwarf_Debug dbg, Dwarf_Die in_die, value inval)
{
  CAMLparam1( inval );
  CAMLlocal2( pair, cons );

  int i, res;
  Dwarf_Error error;

  Dwarf_Half tag = 0;
  Dwarf_Die sib_die = 0;
  Dwarf_Die child_die = 0;
  Dwarf_Attribute* attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  
  Dwarf_Bool hasabstr = 0;
  Dwarf_Bool hastype = 0;
  Dwarf_Bool hasloc = 0;

  Dwarf_Off type_off = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag");
    assert(0);
  }

  // retrieve type and location
  // first, decide retrievability (try not to depend on tag)
  // found one incorrect debugging information entry for an inlined routine's
  // formal parameter location information;
  // may need to ignore inlining information,

  res = dwarf_hasattr(in_die,DW_AT_abstract_origin,&hasabstr,&error);
  if(res != DW_DLV_OK) {
    perror("error hasattr");
    assert(0);
  }

  if(hasabstr == 0) {
    // normal DIE: look for both information locally
    res = dwarf_hasattr(in_die,DW_AT_type,&hastype,&error);
    if(res != DW_DLV_OK) {
      perror("error hasattr");
      assert(0);
    }
    res = dwarf_hasattr(in_die,DW_AT_location,&hasloc,&error);
    if(res != DW_DLV_OK) {
      perror("error hasattr");
      assert(0);
    }

    if(hastype == 1 && hasloc ==1) {
      // here, in_die has both information
      res = dwarf_attrlist(in_die,&attrbuf,&attrcount,&error);
      if(res != DW_DLV_OK) {
	perror("error attrlist");
	assert(0);
      }
      for(i = 0; i< attrcount; i++) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK && aform == DW_AT_type) {
	  get_type_off(attrbuf[i], &type_off);
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);

      pair = caml_alloc(2,0);

      Store_field( pair, 0,// Val_int(1) );
		   encode_type(dbg,type_off) );
      Store_field( pair, 1,// Val_emptylist );
		   encode_sinfo(dbg,in_die) );

      cons = caml_alloc(2,0);
      Store_field( cons, 0, pair );
      Store_field( cons, 1, inval );
      
      inval = cons;
    }
  } else {
    // special DIE
    // look for location information locally
    res = dwarf_hasattr(in_die,DW_AT_location,&hasloc,&error);
    if(res != DW_DLV_OK) {
      perror("error hasattr");
      assert(0);
    }
    // look for type information remotely
    Dwarf_Die origin_die = 0;
    res = dwarf_attrlist(in_die,&attrbuf,&attrcount,&error);
    if(res != DW_DLV_OK) {
      perror("error attrlist");
      assert(0);
    }
    for(i = 0; i< attrcount; i++) {
      Dwarf_Half aform;
      res = dwarf_whatattr(attrbuf[i], &aform, &error);
      if(res == DW_DLV_OK && aform == DW_AT_abstract_origin) {
	Dwarf_Off origin_off = 0;
	get_type_off(attrbuf[i], &origin_off);
	res = dwarf_offdie(dbg,origin_off,&origin_die,&error);
	if(res != DW_DLV_OK) {
	  perror("error offdie");
	  assert(0);
	}
      }
      dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
    }
    dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
    // now, we found origin DIE
    // look for type information in origin die
    res = dwarf_hasattr(origin_die,DW_AT_type,&hastype,&error);
    if(res != DW_DLV_OK) {
      perror("error hasattr");
      assert(0);
    }
    if(hastype == 1 && hasloc == 1) {
      // has both
      res = dwarf_attrlist(origin_die,&attrbuf,&attrcount,&error);
      if(res != DW_DLV_OK) {
	perror("error attrlist");
	assert(0);
      } 
      for(i = 0; i< attrcount; i++) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK && aform == DW_AT_type) {
	  get_type_off(attrbuf[i], &type_off);
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      dwarf_dealloc(dbg, attrbuf, DW_DLA_LIST);
      dwarf_dealloc(dbg, origin_die, DW_DLA_DIE);

      pair = caml_alloc(2,0);

      Store_field( pair, 0,// Val_int(1) );
		   encode_type(dbg,type_off) );
      Store_field( pair, 1,// Val_emptylist );
		   encode_sinfo(dbg,in_die) );

      cons = caml_alloc(2,0);
      Store_field( cons, 0, pair );
      Store_field( cons, 1, inval );
      
      inval = cons;
    }
  }

  // continue on children if in_die is not inlined subroutine
  //if(tag != DW_TAG_inlined_subroutine) {
    res = dwarf_child(in_die, &child_die, &error);
    if(res == DW_DLV_NO_ENTRY) {
      // stop on child
    } else if(res == DW_DLV_OK) {
      inval = encode_relatives(dbg,child_die,inval);
      dwarf_dealloc(dbg,child_die,DW_DLA_DIE);
    } else {
      perror("error in dwarf_child");
      assert(0);
    }
    //}
  // continue on sibling after child
  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_NO_ENTRY) {
    // stop on sibling
  }
  else if(res == DW_DLV_OK) {
    inval = encode_relatives(dbg,sib_die,inval);
    dwarf_dealloc(dbg,sib_die,DW_DLA_DIE);
  }
  else {
    perror("error in siblingof!");
    assert(0);
  }

  CAMLreturn( inval );
}
