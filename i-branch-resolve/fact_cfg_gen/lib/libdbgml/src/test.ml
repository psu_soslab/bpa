open TyInfoRetrieval

let _ =
  let targetfile = 
    if Array.length Sys.argv <> 2 then
      raise (Failure "command line parsing error")
    else
      Sys.argv.(1)
  in
  let ginfo = retrieve_ginfo targetfile in
  dump_ginfo ginfo
