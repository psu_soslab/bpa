#include <caml/mlvalues.h>

#include "config.h"
#include "dwarf.h"
#include "libdwarf.h"

#define BUFSIZE 1024*1024

struct srcfilesdata {
  char ** srcfiles;
  Dwarf_Signed srcfilescount;
  int srcfilesres;
};

typedef value (*work_method)
(Dwarf_Debug, Dwarf_Die, int, value);

int open_a_file(const char* name) ;

void close_a_file(int f) ;

void
resetsrcfiles(Dwarf_Debug dbg,struct srcfilesdata *sf) ;

void
get_addr(Dwarf_Attribute attr, Dwarf_Addr *val);

void
get_uvalue(Dwarf_Die in_die, Dwarf_Attribute attr, Dwarf_Unsigned* ret_value);

void
get_svalue(Dwarf_Attribute attr, Dwarf_Signed* ret_value);

void
get_type_off(Dwarf_Attribute attr, Dwarf_Off* type_off);

Dwarf_Unsigned
get_type_size(Dwarf_Debug dbg, Dwarf_Die in_die);

int 
count_tag(Dwarf_Debug dbg, Dwarf_Die start_die, Dwarf_Half target_tag);

void
get_name(Dwarf_Attribute attr, char ** name);

Dwarf_Unsigned
get_memloc(Dwarf_Debug dbg, Dwarf_Die in_die);

static value
get_die_and_siblings(Dwarf_Debug dbg, Dwarf_Die in_die, int is_info, 
		     int in_level, work_method work, value inval);

value
collect_info(Dwarf_Debug dbg, work_method work, value inval);

value 
create_void();

value 
create_base(int size, char* basename);

value 
create_const(value ctype);

value
create_array(value ctype, value dimlist);

value
create_pointer(int size, value ctype);

value
create_typedef(char* name, value ctype);

value
create_subroutine(value rettype, value paramlist);

value
create_enumeration(int size, char* name, value etorlist );

value
create_union(int size, char* name, value memlist);

value
create_structure(int size, char* name, value memlist);

value
create_volatile(value ctype);

value
create_wrongtype(char* errmsg);

value
create_unspec();

static value
encode_subranges(Dwarf_Debug dbg, Dwarf_Die in_die);

static value
encode_dimension(Dwarf_Debug dbg, Dwarf_Die parent_die);

value
encode_parameters(Dwarf_Debug dbg, Dwarf_Die in_die);

value
encode_subprogram_parameters(Dwarf_Debug dbg, Dwarf_Die in_die);

static value
encode_enumerators(Dwarf_Debug dbg, Dwarf_Die in_die);

static value
encode_union_members(Dwarf_Debug dbg, Dwarf_Die in_die);

value
encode_struct_members(Dwarf_Debug dbg, Dwarf_Die in_die);

void
encode_type_rec(Dwarf_Debug dbg, Dwarf_Off type_off, char** enc);

value
encode_type_i(Dwarf_Debug dbg, Dwarf_Off type_off);

value
encode_type(Dwarf_Debug dbg, Dwarf_Off type_off);

static int
op_has_no_operands(int op);

static void
show_contents(unsigned int length, const unsigned char * bp, char** expr_enc);

void
encode_expr(Dwarf_Loc expr, int index, char** expr_enc);

value
encode_locexprs(Dwarf_Debug dbg, Dwarf_Loc* exprs, Dwarf_Unsigned num);

value
encode_sinfo(Dwarf_Debug dbg, Dwarf_Die in_die);

value
encode_type_info(Dwarf_Debug dbg, Dwarf_Off type_off, 
		 Dwarf_Die in_die, value inval);

int
in_map(char* name, char** struct_map, int mapsize);

static int
inc_map(char* name, char** struct_map, int mapsize);

value
encode_detail(Dwarf_Debug dbg, Dwarf_Die in_die);

value
encode_relatives(Dwarf_Debug dbg, Dwarf_Die in_die, value inval);
