module P = Printf
module H = Hashtbl
type c_type =
    C_base of int * string
  | C_const of c_type
  | C_array of c_type * int list
  | C_pointer of int * c_type
  | C_typedef of string * c_type
  | C_subroutine of c_type * c_type list
  | C_enumeration of int * string * (string * int) list
  | C_union of int * string * (c_type * string) list
  | C_structure of int * string * (int * c_type * string) list
  | C_volatile of c_type
  | C_unspecified_param
  | C_wrong_type of string
  | C_void
val str_of_c_type : c_type -> string
type loclist = (string * string * string list) list
type type_info = (c_type * loclist) list
external retrieve_raw_gti : string -> type_info = "retrieve_gti"
external retrieve_namemap : string -> c_type list = "retrieve_namemap"
external retrieve_raw_ginfo : string -> c_type list * type_info
  = "retrieve_ginfo"
external retrieve_raw_pinfo : string -> string -> string * type_info
  = "retrieve_pinfo"
external retrieve_funsig_info : string -> (c_type * string) list
  = "retrieve_fun_typesig"
val retrieve_gti :
  string -> (c_type * (string * string * string list) list) list
val retrieve_ginfo :
  string ->
  c_type list * (c_type * (string * string * string list) list) list
val retrieve_pinfo :
  string ->
  string -> string * (c_type * (string * string * string list) list) list
val dump_type_info :
  (c_type * (string * string * string list) list) list -> unit
val dump_ginfo :
  c_type list * (c_type * (string * string * string list) list) list -> unit
val dump_pinfo :
  string * (c_type * (string * string * string list) list) list -> unit
val dump_funsig_info : (c_type * string) list -> unit
val cons_namety_hashtbl :
  c_type list ->
  (string, c_type) H.t * (string, c_type) H.t * (string, c_type) H.t
val dump_namety_hashtbl : (string, c_type) H.t -> unit
