open Batteries
open BatOptParse
module H = Hashtbl

(**** Global command-line parser ****)
let usagemsg = 
  "main.native [options] target\nrun with -h or --help to see the list of options"

let optpsr = OptParser.make ?usage:(Some usagemsg) ()

(**** Global definitions for controling program behaviors ****)
(** For Ibaux: **)
let static_lib = ref false
let staticlib_opt = StdOpt.store_true ()

(** For Advdis: **)
let m_on = ref false
let dfam_opt = StdOpt.store_true ()
let dm_on = ref true

let no_optimization = ref true
let o_zero_opt = StdOpt.store_false ()

(** For MetaCFG: **)
let fini_eqcg = ref false 
let feqcg_opt = StdOpt.store_true ()

let cfg_m_on = ref true
let cfgm_opt = StdOpt.store_false ()

type cfg_type =
| P_Unknown
| P_Base
| P_Arity
| P_Mcfi
| P_Ctype
| P_Top
    
let plevel = ref P_Top
let plevel_opt = StdOpt.any_option ?default:(Some (Some P_Top))
  (fun str -> 
    match str with
    | "base" -> P_Base
    | "arity" -> P_Arity
    | "mcfi" -> P_Mcfi
    | "ctype" -> P_Ctype
    | "top" -> P_Top
    | _ -> P_Unknown )

(** For BackTaint: **)
(*let weak_bt = ref false*)
let input_span = ref None
let input_span_opt = StdOpt.float_option ()

(** For Main: **)
let libc_path_option = ref None
let libc_path_opt = StdOpt.str_option ()

let target_lbl = ref None
let target_lbl_opt = StdOpt.str_option ()

let aibt_on = ref false
let aibt_opt = StdOpt.store_true ()

let time_on = ref false
let time_opt = StdOpt.store_true ()

let baseCFG_on = ref false
let baseCFG_opt = StdOpt.store_true ()

let path_detail = ref false
let path_detail_opt = StdOpt.store_true ()

let policy_check = ref false
let policy_check_opt = StdOpt.store_true ()

let eval_all_callsites = ref false

(** For All: **)
let verbose = ref false
let verbose_opt = StdOpt.store_true ()

let soundness_warn = ref false

let parse_args () =
  (** Parse for Ibaux: **)
  OptParser.add optpsr ?help:(Some "Deal with statically-linked binary") 
    ?long_name:(Some "static") staticlib_opt;
  
  (** Parse for Advdis: **)
  OptParser.add optpsr ?help:(Some "Marshal coq_DFA") 
    ?long_name:(Some "dfam") dfam_opt;
  OptParser.add optpsr ?help:(Some "Is compiler optimization on?") 
    ?long_name:(Some "nozero") o_zero_opt;

  (** Parse for MetaCFG: **)
  OptParser.add optpsr ?help:(Some "Output solved EQCG") 
    ?long_name:(Some "feqcg") feqcg_opt;
  OptParser.add optpsr ?help:(Some "Marshal generated CFG") 
    ?long_name:(Some "cfgm") cfgm_opt;
  OptParser.add optpsr ?help:(Some "Specify CFG precision level")
    ?long_name: (Some "CFG") plevel_opt;

  (** Parse for BackTaint: **)
  OptParser.add optpsr ?help:(Some "Specify time-span for BackTaint")
    ?long_name: (Some "span") input_span_opt;

  (** Parse for Main: **)
  OptParser.add optpsr ?help:(Some "Specify libc binary path") 
    ?long_name:(Some "libc") libc_path_opt;
  OptParser.add optpsr ?help:(Some "Target single task")
    ?short_name:(Some 't') target_lbl_opt;
  OptParser.add optpsr ?help:(Some "Report AIBT") 
    ?long_name:(Some "aibt") aibt_opt;
  OptParser.add optpsr ?help:(Some "Report timing")
    ?long_name:(Some "time") time_opt;
  OptParser.add optpsr ?help:(Some "Generate base-CFG only") 
    ?long_name:(Some "base") baseCFG_opt;
  OptParser.add optpsr ?help:(Some "Output instructions for each path")
    ?long_name:(Some "path-detail") path_detail_opt;
  OptParser.add optpsr ?help:(Some "Check sensitive callsites")
    ?long_name:(Some "policy-check") policy_check_opt;
  
  (** Parse for All: **)
  OptParser.add optpsr ?help:(Some "Turn on processing report") 
    ?short_name:(Some 'v') verbose_opt;

  (** Parse for Local **)
  let bt_quota_opt = StdOpt.int_option () in
  OptParser.add optpsr ?help:(Some "Specify taint quota") 
    ?short_name:(Some 'q') bt_quota_opt;
  
  let se_quota_opt = StdOpt.int_option () in
  OptParser.add optpsr ?help:(Some "Specify symbolic execution quota") 
    ?long_name:(Some "sq") se_quota_opt;
  
  let path_quota_opt = StdOpt.int_option () in
  OptParser.add optpsr ?help:(Some "Specify path quota") 
    ?short_name:(Some 'p') path_quota_opt;

  let argvs = OptParser.parse_argv optpsr in
  let target_path = 
    if argvs = [] then 
      raise (Failure "No target file specified!")
    else
      List.hd argvs
  in
  static_lib := Opt.get staticlib_opt;

  fini_eqcg := Opt.get feqcg_opt;
  cfg_m_on := Opt.get cfgm_opt;

  m_on := Opt.get dfam_opt;
  no_optimization := Opt.get o_zero_opt;
  plevel := Opt.get plevel_opt;

  input_span := Opt.opt input_span_opt;

  libc_path_option := Opt.opt libc_path_opt;
  target_lbl := Opt.opt target_lbl_opt;
  aibt_on := Opt.get aibt_opt;
  time_on := Opt.get time_opt; 
  baseCFG_on := Opt.get baseCFG_opt;
  path_detail := Opt.get path_detail_opt;
  policy_check := Opt.get policy_check_opt;
  verbose := Opt.get verbose_opt;

  let btquota_option = Opt.opt bt_quota_opt in
  let sequota_option = Opt.opt se_quota_opt in
  let pathquota_option = Opt.opt path_quota_opt in
(*
  let bt_quota = 
    if btquota_option = None then 50
    else Option.get btquota_option
  in

  let se_quota =
    if sequota_option = None then 30
    else Option.get sequota_option
  in
*)
  target_path, btquota_option, sequota_option, pathquota_option
    
    
