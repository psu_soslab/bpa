(*** Some modifications by Sun **)

open Batteries
open X86Syntax
open X86Semantics
open RTL
module R=X86_RTL

open Printf







let rec prune_cast re =
  match re with
  | R.Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    begin match bvop with
    | R.Coq_shl_op 
    | R.Coq_shr_op 
    | R.Coq_shru_op
    | R.Coq_or_op -> prune_cast re1
    | _ ->
      R.Coq_arith_rtl_exp(s,bvop,prune_cast re1,prune_cast re2)
    end
  | R.Coq_test_rtl_exp(s,top,re1,re2) ->
    R.Coq_test_rtl_exp(s,top,prune_cast re1,prune_cast re2)
  | R.Coq_if_rtl_exp(s, ge,te,ee) ->
    R.Coq_if_rtl_exp(s, prune_cast ge, prune_cast te, prune_cast ee)
  | R.Coq_cast_s_rtl_exp(s,_,re') -> 
    prune_cast re' 
  | R.Coq_cast_u_rtl_exp(s,_,re') -> 
    prune_cast re'
  | R.Coq_imm_rtl_exp(s,_)
  | R.Coq_get_loc_rtl_exp(s,_)
  | R.Coq_get_ps_reg_rtl_exp(s,_) 
  | R.Coq_get_array_rtl_exp(_,s,_,_) ->
    re
  | R.Coq_get_byte_rtl_exp(re') ->
    R.Coq_get_byte_rtl_exp(prune_cast re')
  | R.Coq_get_random_rtl_exp(s) ->
    re
  | _ ->
    assert false


























let rec constant_fold re =
  match re with
  | R.Coq_arith_rtl_exp (s, bvop, re1, re2) ->
    let cf_re1 = constant_fold re1 in
    let cf_re2 = constant_fold re2 in
    let default_new_re = 
      R.Coq_arith_rtl_exp(s, bvop, cf_re1, cf_re2) 
    in



    begin match cf_re1, cf_re2 with
	| R.Coq_get_loc_rtl_exp(_,loc1), _ -> (*get rid of Start__ *)
	   begin match loc1 with 
	    | X86_MACHINE.Coq_seg_reg_start_loc(_) ->
  		  cf_re2
		| _ -> default_new_re

	   end		

    | R.Coq_imm_rtl_exp(_,v1), R.Coq_imm_rtl_exp(_,v2) ->
	  if (Big_int.to_int v1) < 134512640 then  (*shouldn't add address with ints*)
        begin match bvop with
      	 | R.Coq_add_op -> R.Coq_imm_rtl_exp(s, Big.add v1 v2) 
      	 | R.Coq_sub_op -> R.Coq_imm_rtl_exp(s, Big.sub v1 v2) 
      	 | R.Coq_mul_op -> R.Coq_imm_rtl_exp(s, Big.mult v1 v2)
      	 | R.Coq_xor_op -> R.Coq_imm_rtl_exp(s, Big_int.xor_big_int v1 v2)
      	 | R.Coq_shru_op -> R.Coq_imm_rtl_exp(s, Big.add v1 v2) 
      	 | _ -> default_new_re
      	end
	  else default_new_re
    | R.Coq_imm_rtl_exp(_,v), _ -> (* v op () *) (********* another important *********)
      begin match bvop with
       | R.Coq_add_op -> (* v + () *) (*********************** important ***********************)
	begin match cf_re2 with
	| R.Coq_arith_rtl_exp(_,bvop',cf_re1',cf_re2') -> (* v + (_ op _) *)
	  begin match cf_re1', cf_re2' with
	  	| R.Coq_imm_rtl_exp(_,v'), _ -> (* v + (v' op _) *)
	      begin match bvop' with
	    	| R.Coq_add_op -> (* v + (v' + _) *)
	        let new_v = R.Coq_imm_rtl_exp(s,Big.add v v') in
	        R.Coq_arith_rtl_exp(s,R.Coq_add_op,new_v,cf_re2')
	    	| R.Coq_sub_op -> (* v + (v' - _) *)
	      	let new_v = R.Coq_imm_rtl_exp(s,Big.add v v') in
	      	R.Coq_arith_rtl_exp(s,R.Coq_sub_op,new_v,cf_re2')
	    	| _ -> default_new_re
	      end
	  	| _, R.Coq_imm_rtl_exp(_,v') -> (* v + (_ op v') *)
	      begin match bvop' with
	    	| R.Coq_add_op -> (* v + (_ + v') *)
	      	let new_v = R.Coq_imm_rtl_exp(s,Big.add v v') in
	      	R.Coq_arith_rtl_exp(s,R.Coq_add_op,cf_re1',new_v)
	    	| R.Coq_sub_op -> (* v + (_ - v') *)
	      	let new_v = R.Coq_imm_rtl_exp(s,Big.sub v' v) in
	      	R.Coq_arith_rtl_exp(s,R.Coq_sub_op,cf_re1',new_v)
	    	| _ -> default_new_re
	      end
	  |_ -> default_new_re
	  end
	| _ -> default_new_re
	end



      | R.Coq_sub_op -> (* v - () *) (*********************** important ***********************)
	begin match cf_re2 with
	| R.Coq_arith_rtl_exp(_,bvop',cf_re1',cf_re2') -> (* v - (_ op _) *)
	  begin match cf_re1', cf_re2' with
	  | R.Coq_imm_rtl_exp(_,v'), _ -> (* v - (v' op _) *)
	    begin match bvop' with
	    | R.Coq_add_op -> (* v - (v' + _) *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.sub v v') in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,new_v,cf_re2')
	    | R.Coq_sub_op -> (* v - (v' - _) *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.sub v v') in
	      R.Coq_arith_rtl_exp(s,R.Coq_add_op,new_v,cf_re2')
	    | _ -> default_new_re
	    end
	  | _, R.Coq_imm_rtl_exp(_,v') -> (* v - (_ op v') *)
	    begin match bvop' with
	    | R.Coq_add_op -> (* v - (_ + v') *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.sub v v') in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,new_v,cf_re1')
	    | R.Coq_sub_op -> (* v - (_ - v') *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v v') in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,new_v,cf_re1')
	    | _ -> default_new_re
	    end
	  |_ -> default_new_re
	  end
	| _ -> default_new_re
	end



(*



      | R.Coq_shru_op -> (* v >>u () *) (*********************** important ***********************)
	begin match cf_re2 with
	| R.Coq_arith_rtl_exp(_,bvop',cf_re1',cf_re2') -> (* v >>u (_ op _) *)
	  begin match cf_re1', cf_re2' with
	  | R.Coq_imm_rtl_exp(_,v'), _ -> (* v >>u (v' op _) *)
	    begin match bvop' with
	    | R.Coq_add_op -> (* v + (v' + _) *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v v') in
	      R.Coq_arith_rtl_exp(s,R.Coq_add_op,new_v,cf_re2')
	    | R.Coq_sub_op -> (* v + (v' - _) *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v v') in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,new_v,cf_re2')
	    | R.Coq_shru_op -> (* v + (v' >>u _) *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v v') in
	      R.Coq_arith_rtl_exp(s,R.Coq_shru_op,new_v,cf_re2')
	    | _ -> default_new_re
	    end
	  | _, R.Coq_imm_rtl_exp(_,v') -> (* v + (_ op v') *)
	    begin match bvop' with
	    | R.Coq_add_op -> (* v + (_ + v') *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v v') in
	      R.Coq_arith_rtl_exp(s,R.Coq_add_op,cf_re1',new_v)
	    | R.Coq_sub_op -> (* v + (_ - v') *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.sub v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,cf_re1',new_v)
	    | R.Coq_shru_op -> (* v + (_ + v') *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v v') in
	      R.Coq_arith_rtl_exp(s,R.Coq_shru_op,cf_re1',new_v)

	    | _ -> default_new_re
	    end
	  |_ -> default_new_re
	  end
	| _ -> default_new_re
	end


*)





      | _ -> default_new_re
      end
    | _, R.Coq_imm_rtl_exp(_,v) -> (* () op v *) (********* another important *********)
    begin match bvop with
     | R.Coq_add_op -> (* () + v *) (*********************** important ***********************)
	 begin match cf_re1 with
	  | R.Coq_arith_rtl_exp(_,bvop',cf_re1',cf_re2') -> (* (_ op _) + v *)
		  begin match cf_re1', cf_re2' with
		  | _, R.Coq_imm_rtl_exp(_,v') -> (* (_ op v') + v *) (***)
			begin match bvop' with
			| R.Coq_add_op -> (* (_ + v') + v *)
			  let new_v = R.Coq_imm_rtl_exp(s,Big.add v' v) in
			  R.Coq_arith_rtl_exp(s,R.Coq_add_op,cf_re1',new_v)
			| R.Coq_sub_op -> (* (_ - v') + v*)
			  let new_v = R.Coq_imm_rtl_exp(s,Big.sub v' v) in
			  R.Coq_arith_rtl_exp(s,R.Coq_sub_op,cf_re1',new_v)
			| _ -> default_new_re
			end
		  | R.Coq_imm_rtl_exp(_,v'), _ -> (* (v' op _) + v *)
			begin match bvop' with
			| R.Coq_add_op -> (* (v' + _) + v *)
			  let new_v = R.Coq_imm_rtl_exp(s,Big.add v' v) in
			  R.Coq_arith_rtl_exp(s,R.Coq_add_op,new_v,cf_re2')
			| R.Coq_sub_op -> (* (v' - _) + v *)
			  let new_v = R.Coq_imm_rtl_exp(s,Big.add v' v) in
			  R.Coq_arith_rtl_exp(s,R.Coq_sub_op,new_v,cf_re2')
			| _ -> default_new_re
			end

		  |_ -> default_new_re
		  end
	| _ -> default_new_re
	end
      | R.Coq_sub_op -> (* () - v *) (*********************** important ***********************)
	begin match cf_re1 with
	| R.Coq_arith_rtl_exp(_,bvop',cf_re1',cf_re2') -> (* (_ op _) - v *)
	  begin match cf_re1', cf_re2' with
	  | _, R.Coq_imm_rtl_exp(_,v') -> (* (_ op v') - v *)
	    begin match bvop' with
	    | R.Coq_add_op -> (* (_ + v') - v *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.sub v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_add_op,cf_re1',new_v)
	    | R.Coq_sub_op -> (* (_ - v') - v *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,cf_re1',new_v)
	    | _ -> default_new_re
	    end

	  | R.Coq_imm_rtl_exp(_,v'), _ -> (* (v' op _) -v *)
	    begin match bvop' with
	    | R.Coq_add_op -> (* (v' + _) - v *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.sub v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_add_op,new_v,cf_re2')
	    | R.Coq_sub_op -> (* (v' - _) - v *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.sub v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,new_v,cf_re2')
	    | _ -> default_new_re
	    end

	  |_ -> default_new_re
	  end
	| _ -> default_new_re
	end










      | R.Coq_shru_op -> (* () >>u v *) (*********************** important ***********************)
	begin match cf_re1 with
	| R.Coq_arith_rtl_exp(_,bvop',cf_re1',cf_re2') -> (* (_ op _) + v *)
	  begin match cf_re1', cf_re2' with

	  | _, R.Coq_imm_rtl_exp(_,v') -> (* (_ op v') + v *)
	    begin match bvop' with
	    | R.Coq_add_op -> (* (_ + v') + v *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_add_op,cf_re1',new_v)
	    | R.Coq_sub_op -> (* (_ - v') + v*)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.sub v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,cf_re1',new_v)

	    | R.Coq_shru_op -> (* (_ >>u v') + v *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_shru_op,cf_re1',new_v)

	    | _ -> default_new_re
	    end

	  | R.Coq_imm_rtl_exp(_,v'), _ -> (* (v' op _) + v *)
	    begin match bvop' with
	    | R.Coq_add_op -> (* (v' + _) + v *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_add_op,new_v,cf_re2')
	    | R.Coq_sub_op -> (* (v' - _) + v *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,new_v,cf_re2')

(*
	    | R.Coq_shru_op -> (* (v' >>u _) >>u v *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_shru_op,new_v,cf_re2')
*)


	    | _ -> default_new_re
	    end

	  |_ -> default_new_re
	  end
	| _ -> default_new_re
	end


      | _ -> default_new_re
      end
    | _ ->
      default_new_re
    end


  | R.Coq_test_rtl_exp (s, top, re1, re2) ->
    let cf_re1 = constant_fold re1 in
    let cf_re2 = constant_fold re2 in
    let default_new_re =
      R.Coq_test_rtl_exp (s, top, cf_re1, cf_re2)
    in
    begin match cf_re1, cf_re2 with 
    | R.Coq_imm_rtl_exp(s1,v1), R.Coq_imm_rtl_exp(_,v2) ->
      begin match top with
      | R.Coq_eq_op -> 
	if Big.eq v1 v2 then R.Coq_imm_rtl_exp(Big.zero, Big.one)
	else R.Coq_imm_rtl_exp(Big.zero, Big.zero)
      | R.Coq_lt_op ->
	if Big.lt v1 v2 then R.Coq_imm_rtl_exp(Big.zero, Big.one)
	else R.Coq_imm_rtl_exp(Big.zero, Big.zero)
      | R.Coq_ltu_op -> default_new_re
      end
    | _ ->
      default_new_re
    end
  | R.Coq_if_rtl_exp(s,re1,re2,re3) ->
    let cf_re1 = constant_fold re1 in
    let cf_re2 = constant_fold re2 in
    let cf_re3 = constant_fold re3 in
    let default_new_re =
      R.Coq_if_rtl_exp(s,cf_re1,cf_re2,cf_re3)
    in
    begin match cf_re1 with
    | R.Coq_imm_rtl_exp(_,b) ->
      if Big.eq b Big.one then cf_re2
      else cf_re3
    | _ -> 
      default_new_re
    end
  | R.Coq_cast_s_rtl_exp(s1,s2,re') ->
    let cf_re' = constant_fold re' in
    R.Coq_cast_s_rtl_exp(s1,s2,cf_re')
  | R.Coq_cast_u_rtl_exp(s1,s2,re') ->
    let cf_re' = constant_fold re' in
    R.Coq_cast_u_rtl_exp(s1,s2,cf_re')
  | R.Coq_get_array_rtl_exp(l,s,arr,re') ->
    let cf_re' = constant_fold re' in
    R.Coq_get_array_rtl_exp(l,s,arr,cf_re')
  | R.Coq_get_byte_rtl_exp(re') ->
    let cf_re' = constant_fold re' in
    R.Coq_get_byte_rtl_exp(cf_re')
  | R.Coq_farith_rtl_exp(s1,s2,faop,re1,re2,re3) ->
    let cf_re1 = constant_fold re1 in
    let cf_re2 = constant_fold re2 in
    let cf_re3 = constant_fold re3 in
    R.Coq_farith_rtl_exp(s1,s2,faop,cf_re1,cf_re2,cf_re3)
  | R.Coq_fcast_rtl_exp(s1,s2,s3,s4,re1,re2) ->
    let cf_re1 = constant_fold re1 in
    let cf_re2 = constant_fold re2 in
    R.Coq_fcast_rtl_exp(s1,s2,s3,s4,cf_re1,cf_re2)
  | _ -> re

let rec neg_opt re =
  match re with
  | R.Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    let opt_re1 = neg_opt re1 in
    let opt_re2 = neg_opt re2 in
    let default_new_re = 
      R.Coq_arith_rtl_exp(s,bvop,opt_re1,opt_re2)
    in
    begin match bvop with
    | R.Coq_add_op -> (* _ + _ *)
      begin match opt_re1, opt_re2 with
      | _, R.Coq_imm_rtl_exp(_,v) -> (* _ + v *)
	if Big.lt v Big.zero then
	  let new_v = R.Coq_imm_rtl_exp(s, Big.opp v) in
	  R.Coq_arith_rtl_exp(s,R.Coq_sub_op,opt_re1,new_v)
	else if Big.eq v Big.zero then
	  opt_re1
	else
	  default_new_re
      | _ -> default_new_re
      end
    | R.Coq_sub_op -> (* _ - _ *)
      begin match opt_re1, opt_re2 with
      | _, R.Coq_imm_rtl_exp(_,v) -> (* _ - v *)
	if Big.lt v Big.zero then
	  let new_v = R.Coq_imm_rtl_exp(s, Big.opp v) in
	  R.Coq_arith_rtl_exp(s,R.Coq_add_op,opt_re1,new_v)
	else if Big.eq v Big.zero then
	  opt_re1
	else
	  default_new_re
      | _ -> default_new_re
      end
    | _ -> default_new_re
    end
  | R.Coq_test_rtl_exp (s, top, re1, re2) ->
    let opt_re1 = neg_opt re1 in
    let opt_re2 = neg_opt re2 in
    R.Coq_test_rtl_exp (s, top, opt_re1, opt_re2)
  | R.Coq_if_rtl_exp(s,re1,re2,re3) ->
    let opt_re1 = neg_opt re1 in
    let opt_re2 = neg_opt re2 in
    let opt_re3 = neg_opt re3 in
    R.Coq_if_rtl_exp(s,opt_re1,opt_re2,opt_re3)
  | R.Coq_cast_s_rtl_exp(s1,s2,re') ->
    let opt_re' = neg_opt re' in
    R.Coq_cast_s_rtl_exp(s1,s2,opt_re')
  | R.Coq_cast_u_rtl_exp(s1,s2,re') ->
    let opt_re' = neg_opt re' in
    R.Coq_cast_u_rtl_exp(s1,s2,opt_re')
  | R.Coq_get_array_rtl_exp(l,s,arr,re') ->
    let opt_re' = neg_opt re' in
    R.Coq_get_array_rtl_exp(l,s,arr,opt_re')
  | R.Coq_get_byte_rtl_exp(re') ->
    let opt_re' = neg_opt re' in
    R.Coq_get_byte_rtl_exp(opt_re')
  | R.Coq_farith_rtl_exp(s1,s2,faop,re1,re2,re3) ->
    let opt_re1 = neg_opt re1 in
    let opt_re2 = neg_opt re2 in
    let opt_re3 = neg_opt re3 in
    R.Coq_farith_rtl_exp(s1,s2,faop,opt_re1,opt_re2,opt_re3)
  | R.Coq_fcast_rtl_exp(s1,s2,s3,s4,re1,re2) ->
    let opt_re1 = neg_opt re1 in
    let opt_re2 = neg_opt re2 in
    R.Coq_fcast_rtl_exp(s1,s2,s3,s4,opt_re1,opt_re2)
  | _ -> re

let rec simp_re simpre_list re =
  List.fold_left (fun re simpre -> simpre re) re simpre_list

let simpre_all_methods = 
  [constant_fold; neg_opt]

  (*[constant_fold; prune_cast; neg_opt]*)
  (*[prune_cast; neg_opt]*)

let simp_set_loc simpre_list rtl_instr =
  match rtl_instr with
  | R.Coq_set_loc_rtl(s,re,loc) ->
    let new_re = simp_re simpre_list re in
    R.Coq_set_loc_rtl(s,new_re,loc)
  | _ -> rtl_instr

let simp_set_ps_reg simpre_list rtl_instr =
  match rtl_instr with
  | R.Coq_set_ps_reg_rtl(s,re,ps_reg) ->
    let new_re = simp_re simpre_list re in
    R.Coq_set_ps_reg_rtl(s, new_re, ps_reg)
  | _ -> rtl_instr

let simp_set_arr simpre_list rtl_instr =
  match rtl_instr with
  | R.Coq_set_array_rtl(l,s,arr,re1,re2) ->
    let new_re1 = simp_re simpre_list re1 in
    let new_re2 = simp_re simpre_list re2 in
    R.Coq_set_array_rtl(l,s,arr,new_re1,new_re2)
  | _ -> rtl_instr

let simp_set_byte simpre_list rtl_instr =
  match rtl_instr with
  | R.Coq_set_byte_rtl(re1,re2) ->
    let new_re1 = simp_re simpre_list re1 in
    let new_re2 = simp_re simpre_list re2 in
    R.Coq_set_byte_rtl(new_re1,new_re2)
  | _ -> rtl_instr

let simp_ident_assign_op rtl_instr = 
  match rtl_instr with
  | R.Coq_set_loc_rtl(s,rhs_re,lhs_loc) ->
    begin match rhs_re with
    | R.Coq_get_loc_rtl_exp(_,rhs_loc) ->
      if rhs_loc = lhs_loc then None
      else Some rtl_instr
    | _ -> Some rtl_instr
    end
  | R.Coq_set_ps_reg_rtl(s,rhs_re,lhs_ps_reg) ->
    begin match rhs_re with
    | R.Coq_get_ps_reg_rtl_exp(_,rhs_ps_reg) ->
      if rhs_ps_reg = lhs_ps_reg then None
      else Some rtl_instr
    | _ -> Some rtl_instr
    end
  | R.Coq_set_array_rtl(l,s,arr,ind_re,rhs_re) ->
    begin match rhs_re with
    | R.Coq_get_array_rtl_exp(rhs_l,rhs_s,rhs_arr,rhs_ind_re) ->
      if rhs_l = l && rhs_s = s && rhs_arr = arr && rhs_ind_re = ind_re then
	None
      else Some rtl_instr
    | _ -> Some rtl_instr
    end
  | R.Coq_set_byte_rtl(rhs_re,lhs_re) ->
    begin match rhs_re with
    | R.Coq_get_byte_rtl_exp(rhs_re') ->
      if rhs_re' = lhs_re then None
      else Some rtl_instr
    | _ -> Some rtl_instr
    end
  | _ -> Some rtl_instr

let simp_ident_assign ri_list =
  List.filter_map simp_ident_assign_op ri_list

let rec simp_ri simpri_list simpre_list ri =
  match ri with
  | R.Coq_if_rtl(re, ri') ->
    let new_re = simp_re simpre_list re in
    let new_ri = simp_ri simpri_list simpre_list ri' in
    R.Coq_if_rtl(new_re, new_ri)
  | _ ->
    List.fold_right (fun simpri ri -> simpri simpre_list ri) simpri_list ri

let simplify_rtl simpri_methods simpre_methods rtl_list =
  List.map (fun rtl_instr -> 
    simp_ri simpri_methods simpre_methods rtl_instr
  ) rtl_list

let simpri_all_methods = 
  [
    simp_set_loc;
    simp_set_ps_reg;
    simp_set_arr;
    simp_set_byte
  ]
