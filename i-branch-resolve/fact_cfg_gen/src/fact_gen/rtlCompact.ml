(*** Sun Hyoung Kim, for Datalog facts generation ***)

open Batteries
open X86Syntax
open X86Semantics
open RTL

module R=X86_RTL

open Big
open Printf

open Config
open Abbrev


open Printer

module P = Printf

let str_of_size s = Big.to_string (succ s)

let str_of_bvop bvop =
  match bvop with
  | R.Coq_add_op ->  "+"
  | R.Coq_sub_op ->  "-"
  | R.Coq_mul_op ->  "*"
  | R.Coq_divs_op ->  "/s"
  | R.Coq_divu_op ->  "/u"
  | R.Coq_modu_op ->  "%u"
  | R.Coq_mods_op ->  "%s"
  | R.Coq_and_op ->  "&"
  | R.Coq_or_op ->  "|"
  | R.Coq_xor_op ->  "^"
  | R.Coq_shl_op ->  "<<"
  | R.Coq_shr_op ->  ">>"
  | R.Coq_shru_op ->  ">>u"
  | R.Coq_ror_op ->  "ROR"
  | R.Coq_rol_op ->  "ROL"

let str_of_reg reg =
  match reg with
  | EAX ->  "eax"
  | ECX ->  "ecx"
  | EDX ->  "edx"
  | EBX ->  "ebx"
  | ESP ->  "esp"
  | EBP ->  "ebp"
  | ESI ->  "esi"
  | EDI ->  "edi"

let to_str s = "\"" ^ s ^ "\""


let str_of_loc loc =
  match loc with
  | X86_MACHINE.Coq_reg_loc(reg) ->
    str_of_reg reg
  | _ -> "null"


let gen_fact lst = 
  let fact = "" in
  let rec iterate lst fact = 
    match lst with
    | [] -> fact
    | [x] -> fact ^ x ^ "\n"
  	| x::xs -> iterate xs (fact ^ x ^ "\t")  
  in iterate lst fact


let str_of_big_int bvop i =
  let bvop_str = str_of_bvop bvop in let i_sign = Big_int.sign_big_int i in let i = Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int i)) in
    match bvop_str with
	| "+" -> string_of_int i
	| "-" -> string_of_int (-i)
	| _ -> "null" (*should ignore*)


(*1~3 => (addr, 1,2) , (addr, 2,3) *)
let make_rtl_flow addr counter =
  if counter = 1 then [] 
  else let ret_lst = [] in 
  let rec iterate ret_lst counter = 
	let fact = addr ^ "\t" ^ (string_of_int (counter - 1)) ^ "\t" ^ (string_of_int counter) ^ "\n" in
	if (counter - 1) = 1 then (fact::ret_lst)
	else iterate (fact::ret_lst) (counter - 1)
  in iterate ret_lst counter


(*need only one*)
let add_store_int lst fact counter = 
  if (List.length lst) = 0 then ((fact::lst), counter + 1)
  else (lst, counter)


let rtl_instr_compact rtl_instr_lst addr memRgn =
  let move = [] in
  let move_const = [] in (*includes move_xor*)
  let move_reg = [] in (*e.g.: eax = edx + esp*)
  let store = [] in
  let store_int = [] in
  let load = [] in
  let counter = 1 in 
  let memRgn = "\"" ^ memRgn ^ "\"" in  
  let rec iterate rtl_instr_lst move move_const move_reg store store_int load counter =
   match rtl_instr_lst with
    | [] -> ((move, move_const, move_reg, store, store_int, load), (make_rtl_flow addr counter))  
    | rtl_instr::xs ->
    begin match rtl_instr with
	 | R.Coq_set_byte_rtl(rtl_e1, rtl_e2) -> (***set_byte_rtl instructions***)
	  begin match rtl_e2 with
	   | R.Coq_arith_rtl_exp(_,bvop, rtl_e21, rtl_e22)-> (****store****)
		begin match rtl_e1 with
		 | R.Coq_cast_u_rtl_exp(_,_,rtl_exp)->
		  begin match rtl_e21, rtl_e22 with
		   |R.Coq_get_loc_rtl_exp(_,reg1), R.Coq_imm_rtl_exp(_, i) ->

			let reg1 = str_of_loc reg1 in 
			if (reg1 = "null") then iterate xs move move_const move_reg store store_int load counter  
			else let i = str_of_big_int bvop i in
			begin match rtl_exp with
 		 	 |R.Coq_imm_rtl_exp(_,i2) ->
	   		   let i2 = string_of_int (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int i2))) in
			   let fact = gen_fact [addr; (string_of_int counter); memRgn; (to_str reg1) ; i ; i2; "4"] in 
			   let (store_int, counter) = add_store_int store_int fact counter in
				iterate xs move move_const move_reg store store_int load counter  (***Ex: *(ESP - 4) <-- (CastU<32,8> 0) ***)  
			 |R.Coq_get_loc_rtl_exp(_,reg2) -> 
			   let reg2 = str_of_loc reg2 in 
				 if (reg2 = "null" || i = "null") then iterate xs move move_const move_reg store store_int load counter  
			     else let fact =  gen_fact [addr; (string_of_int counter); memRgn; (to_str reg1) ; i ; (to_str reg2); "0"; "4"] in 
				   iterate xs move move_const move_reg (fact::store) store_int load (counter + 1)   (***Ex: *(ESP - 4) <-- (CastU<32,8> ECX) ***) (******** used in genDatalogFact_RTL_share.ml *******)
			 |R.Coq_arith_rtl_exp(_,_, rtl_exp, _) ->
			  begin match rtl_exp with
			  |R.Coq_cast_u_rtl_exp(_,_,rtl_exp) ->
			   begin match rtl_exp with
   			   |R.Coq_get_byte_rtl_exp(rtl_exp) ->
			    begin match rtl_exp with
				|R.Coq_arith_rtl_exp(_, bvop, rtl_exp1, rtl_exp2) ->
				 begin match rtl_exp1, rtl_exp2 with
			 	  | R.Coq_get_loc_rtl_exp(_,reg2), R.Coq_imm_rtl_exp(_, i2) -> 
					let reg2 = str_of_loc reg2 in 
					let i2 =  str_of_big_int bvop i2 in if (reg2 = "null" || i2 = "null") then 
					  iterate xs move move_const move_reg store store_int load counter 
					else let fact = gen_fact [addr; (string_of_int counter); memRgn; (to_str reg1);i ;(to_str reg2) ; i2; "4"] in 
					 iterate xs move move_const move_reg (fact::store) store_int load (counter + 1)   (***Ex: *(ESP - 4) <-- (CastU<32,8> ((CastU<8,32> (ECX + -4)) | (((CastU<8,32> (ECX + -3)) | (((CastU<8,32> (ECX + -2)) | ((CastU<8,32> (ECX + -1)) << 8)) << 8)) << 8)))***) (******** used in genDatalogFact_RTL_share.ml *******)
				  | _, _ -> iterate xs move move_const move_reg store store_int load counter end
				| _ -> iterate xs move move_const move_reg store store_int load counter end
			   | _ -> iterate xs move move_const move_reg store store_int load counter end
			  |_ -> iterate xs move move_const move_reg store store_int load counter end
			 |_ -> iterate xs move move_const move_reg store store_int load counter end

		   |_,_ -> iterate xs move move_const move_reg store store_int load counter end

		 | _ -> iterate xs move move_const move_reg store store_int load counter end

	   | _-> iterate xs move move_const move_reg store store_int load counter end


	 | R.Coq_set_loc_rtl(s, rtl_e, loc) -> (***set_loc_rtl instructions***) (****move & load (counter + 1)  ****)
	  begin match loc with
      | X86_MACHINE.Coq_reg_loc(reg1) -> let reg1 = str_of_reg reg1 in
	   begin match rtl_e with
		| R.Coq_imm_rtl_exp(_,i) ->
   		  let i = string_of_int (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int i))) in (** assume int only positive**)
		  let fact = gen_fact [addr; (string_of_int counter); memRgn; (to_str reg1); i ] 
		  in iterate xs move (fact::move_const) move_reg store store_int load (counter + 1)  (*** EAX <-- 0 ***) (*sun: need revision (another predicate)**)

		| R.Coq_get_loc_rtl_exp(_,reg2)-> 
		  let reg2 = str_of_loc reg2 in 
		  let fact = gen_fact [addr; (string_of_int counter); memRgn; (to_str reg1); (to_str reg2); "0"] in
		   iterate xs (fact::move) move_const move_reg store store_int load (counter + 1)  (***Ex: ESP <-- EBP ***)
		| R.Coq_arith_rtl_exp(_, bvop, rtl_e1, rtl_e2) ->
		 begin match rtl_e1, rtl_e2 with
          | R.Coq_get_loc_rtl_exp(_,reg2), R.Coq_get_loc_rtl_exp(_,reg3) ->
          let reg2 = str_of_loc reg2 in let reg3 = str_of_loc reg3 in 
			if (reg2 = "null" || reg3 = "null") then 
			 iterate xs move move_const move_reg store store_int load counter  
			else let str_bvop = str_of_bvop bvop in
		      if ( str_bvop = "^" && reg1 = reg2 && reg1= reg3) then 
				let fact = gen_fact [addr; (string_of_int counter); memRgn; (to_str reg1); "0"] in 
				iterate xs move (fact::move_const) move_reg store store_int load (counter + 1)  (***Ex: EBP <-- (EBP ^ EBP) ***)
		      else
				if str_bvop = "+" then
				let fact = gen_fact [addr; (string_of_int counter); memRgn; (to_str reg1); (to_str reg2); (to_str reg3)]
				in iterate xs move move_const (fact::move_reg) store store_int load (counter + 1)   (***Ex: EAX <-- (EAX + EDX) ***)
				else iterate xs move move_const move_reg store store_int load counter 

 		  | R.Coq_get_loc_rtl_exp(_,reg2), R.Coq_imm_rtl_exp(_, i) -> 
		   let reg2 = str_of_loc reg2 in 
		   let i = str_of_big_int bvop i in 
		   if (reg2 = "null"|| i = "null") then 
			iterate xs move move_const move_reg store store_int load counter 
      	   else let fact = gen_fact [addr; (string_of_int counter); memRgn; (to_str reg1); (to_str reg2); i] in
		      iterate xs (fact::move) move_const move_reg store store_int load (counter + 1)  (***Ex: ESP <-- (ESP + 4) ***)
		  | R.Coq_cast_u_rtl_exp(_,_,rtl_exp), _ -> (****load (counter + 1) ****)
		   begin match rtl_exp with
		   | R.Coq_get_byte_rtl_exp(rtl_exp) ->
		    begin match rtl_exp with
		    | R.Coq_arith_rtl_exp(_, bvop, rtl_exp1, rtl_exp2) ->
			 begin match rtl_exp1, rtl_exp2 with
	 		 | R.Coq_get_loc_rtl_exp(_,reg2), R.Coq_imm_rtl_exp(_, i) -> 
			  let reg2 = str_of_loc reg2 in 
			  let i =  str_of_big_int bvop i in 
				if (reg2 = "null"|| i = "null") then iterate xs move move_const move_reg store store_int load counter 
			  else let fact = gen_fact [addr; (string_of_int counter); memRgn; (to_str reg1); (to_str reg2); i ;"0"; "4"] in
			   iterate xs move move_const move_reg store store_int (fact::load) (counter + 1) (** EBP <-- ((CastU<8,32> (ESP + 32)) | (((CastU<8,32> (ESP + 33)) | (((CastU<8,32> (ESP + 34)) | ((CastU<8,32> (ESP + 35)) << 8)) << 8)) << 8)) (******** used in genDatalogFact_RTL_share.ml *******)
 ***)
			 | _, _ -> iterate xs move move_const move_reg store store_int load counter end

		    | R.Coq_get_loc_rtl_exp(_,reg2) -> 
			  let reg2 = str_of_loc reg2 in 
				if reg2 = "null" then iterate xs move move_const move_reg store store_int load counter
			    else let fact = gen_fact [addr; (string_of_int counter); memRgn; (to_str reg1); (to_str reg2); "0"; "0"; "4"] in
			      iterate xs move move_const move_reg store store_int (fact::load) (counter + 1) (****Ex: EBP <-- ((CastU<8,32> ESP) | (((CastU<8,32> (ESP + 1)) | (((CastU<8,32> (ESP + 2)) | ((CastU<8,32> (ESP + 3)) << 8)) << 8)) << 8))****)
			| _ -> iterate xs move move_const move_reg store store_int load counter end

		   | _ -> iterate xs move move_const move_reg store store_int load counter end

		  | _,_ -> iterate xs move move_const move_reg store store_int load counter end

		| _ -> iterate xs move move_const move_reg store store_int load counter end

   	  | _ -> iterate xs move move_const move_reg store store_int load counter (*end*) (*****????****)

	 | _ -> iterate xs move move_const move_reg store store_int load counter end

	| _ -> iterate xs move move_const move_reg store store_int load counter end
  in iterate rtl_instr_lst move move_const move_reg store store_int load counter 


(******)
