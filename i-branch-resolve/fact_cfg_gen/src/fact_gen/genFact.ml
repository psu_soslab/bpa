open Batteries
open X86Syntax
open X86Semantics
open RTL
open Big
open Printf
open Config
open Abbrev

open BB

(*
open Gen_edge
*)

open GenFactHelper
open SimplifyFlag

module P = Printf
module H = Hashtbl



module R=X86_RTL



let rec sort_tuple = function
    | [] -> []
    | x :: l -> insert x (sort_tuple l)
  and insert (k,v) = function
    | [] -> [(k,v)]
    | (k1,v1) :: l -> if v < v1 then (k,v) :: (k1,v1) :: l
                else (k1,v1) :: insert (k,v) l


let int_to_str i = "e" ^ (string_of_int i)
let i_int_to_str i = "i" ^ (string_of_int i)


let value_exist tbl value =
  let ret = ref "Not_found" in
  H.iter (fun k v -> if v = value then ret := k) tbl;
  !ret

let arith_tbl = H.create 32;;
let imm_tbl = H.create 32;;
let test_tbl = H.create 32;;
let if_tbl = H.create 32;;
let cast_s_tbl = H.create 32;;
let cast_u_tbl = H.create 32;;
let loc_tbl = H.create 32;;
let ps_reg_tbl = H.create 4;;
let arr_tbl = H.create 4;;
let byte_tbl = H.create 32;;
let random_tbl = H.create 4;;
let farith_tbl = H.create 4;;
let fcast_tbl = H.create 4;;

let get_mem_tbl = H.create 32;;



(*
let compiler_symbol_set = Set.empty;;
let compiler_symbol_set = List.fold_right Set.add ["deregister_tm_clones"; 
"register_tm_clones"; 
"frame_dummy";
"__libc_start_main";
"__libc_csu_init";
"__libc_csu_fini";
"__do_global_dtors_aux";
"_fini";
"__x86.get_pc_thunk.bx";
"__x86.get_pc_thunk.dx";
"_start";
"__plt_dispatcher";
"_init";
"__divsc3";
"__mulsc3"] compiler_symbol_set;;
*)



let counter = ref 0 (*rtl_exp*)
let i_counter = ref 1 (*rtl_instr*)


let str_of_size s = Big.to_string (succ s)

let str_of_bvop bvop =
  match bvop with
  | R.Coq_add_op ->  "+"
  | R.Coq_sub_op ->  "-"
  | R.Coq_mul_op ->  "*"
  | R.Coq_divs_op ->  "/s"
  | R.Coq_divu_op ->  "/u"
  | R.Coq_modu_op ->  "%u"
  | R.Coq_mods_op ->  "%s"
  | R.Coq_and_op ->  "&"
  | R.Coq_or_op ->  "|"
  | R.Coq_xor_op ->  "^"
  | R.Coq_shl_op ->  "<<"
  | R.Coq_shr_op ->  ">>"
  | R.Coq_shru_op ->  ">>u"
  | R.Coq_ror_op ->  "ROR"
  | R.Coq_rol_op ->  "ROL"

(*change this to test_op? *)
let str_of_top top =
  match top with
  | R.Coq_eq_op ->  "=="
  | R.Coq_lt_op ->  "<"
  | R.Coq_ltu_op ->  "<u"

let str_of_value v = to_string v

let str_of_reg reg =
  match reg with
  | EAX ->  "EAX"
  | ECX ->  "ECX"
  | EDX ->  "EDX"
  | EBX ->  "EBX"
  | ESP ->  "ESP"
  | EBP ->  "EBP"
  | ESI ->  "ESI"
  | EDI ->  "EDI"

let str_of_ps_reg ps_reg =
  to_string ps_reg

let str_of_segreg segreg= match segreg with
    ES ->  "ES"
  | CS ->  "CS"
  | SS ->  "SS"
  | DS ->  "DS"
  | FS ->  "FS"
  | GS ->  "GS"

let str_of_flag f =
  match f with
  | X86_MACHINE.ID ->  "ID"
  | X86_MACHINE.VIP ->  "VIP"
  | X86_MACHINE.VIF ->  "VIF"
  | X86_MACHINE.AC ->  "AC"
  | X86_MACHINE.VM ->  "VM"
  | X86_MACHINE.RF ->  "RF"
  | X86_MACHINE.NT ->  "NT"
  | X86_MACHINE.IOPL ->  "IOPL"
  | X86_MACHINE.OF ->  "OF"
  | X86_MACHINE.DF ->  "DF"
  | X86_MACHINE.IF_flag ->  "IF_flag"
  | X86_MACHINE.TF ->  "TF"
  | X86_MACHINE.SF ->  "SF"
  | X86_MACHINE.ZF ->  "ZF"
  | X86_MACHINE.AF ->  "AF"
  | X86_MACHINE.PF ->  "PF"
  | X86_MACHINE.CF ->  "CF"

let str_of_ctrlseg ctrlseg =
  match ctrlseg with
  | CR0 ->  "CR0"
  | CR2 ->  "CR2"
  | CR3 ->  "CR3"
  | CR4 ->  "CR4"

let str_of_dbgreg dbgreg =
  match dbgreg with
  | DR0 ->  "DR0"
  | DR1 ->  "DR1"
  | DR2 ->  "DR2"
  | DR3 ->  "DR3"
  | DR6 ->  "DR6"
  | DR7 ->  "DR7"

let str_of_fpu_flag ff = match ff with
    X86_MACHINE.F_Busy ->  "F_Busy"
  | X86_MACHINE.F_C3 ->  "F_C3"
  | X86_MACHINE.F_C2 ->  "F_C2"
  | X86_MACHINE.F_C1 ->  "F_C1"
  | X86_MACHINE.F_C0 ->  "F_C0"
  | X86_MACHINE.F_ES ->  "F_ES"
  | X86_MACHINE.F_SF ->  "F_SF"
  | X86_MACHINE.F_PE ->  "F_PE"
  | X86_MACHINE.F_UE ->  "F_UE"
  | X86_MACHINE.F_OE ->  "F_OE"
  | X86_MACHINE.F_ZE ->  "F_ZE"
  | X86_MACHINE.F_DE ->  "F_DE"
  | X86_MACHINE.F_IE ->  "F_IE"

let str_of_fpu_ctrl_flag fcf = match fcf with
    X86_MACHINE.F_Res15 ->  "F_Res15"
  | X86_MACHINE.F_Res14 ->  "F_Res14"
  | X86_MACHINE.F_Res13 ->  "F_Res13"
  | X86_MACHINE.F_Res7 ->  "F_Res7"
  | X86_MACHINE.F_Res6 ->  "F_Res6"
  | X86_MACHINE.F_IC ->  "F_IC"
  | X86_MACHINE.F_PM ->  "F_PM"
  | X86_MACHINE.F_UM ->  "F_UM"
  | X86_MACHINE.F_OM ->  "F_OM"
  | X86_MACHINE.F_ZM ->  "F_ZM"
  | X86_MACHINE.F_DM ->  "F_DM"
  | X86_MACHINE.F_IM ->  "F_IM"

(*X86_MACHINE - module in X86Semantics.ml,   "get_location" *)
let str_of_loc loc =
  match loc with
  | X86_MACHINE.Coq_reg_loc(reg) ->
    str_of_reg reg
  | X86_MACHINE.Coq_seg_reg_start_loc(segreg) ->
	(*(str_of_segreg segreg)*)

	(*"segreg"*)

    (str_of_segreg segreg) ^ "_S"
    (*"*" *)
  | X86_MACHINE.Coq_seg_reg_limit_loc(segreg) ->
	(str_of_segreg segreg) ^ "_L"
	(*"Limit"*)
    (*"Limit" ^ (str_of_segreg segreg)*)
    (*(str_of_segreg segreg) *)

  | X86_MACHINE.Coq_flag_loc(f) ->
    str_of_flag f
  | X86_MACHINE.Coq_control_register_loc(ctrlreg) ->
    str_of_ctrlseg ctrlreg
  | X86_MACHINE.Coq_debug_register_loc (dbgreg) ->
    str_of_dbgreg dbgreg
  | X86_MACHINE.Coq_pc_loc -> "PC"
  | X86_MACHINE.Coq_fpu_stktop_loc -> "FPU_stktop"
  | X86_MACHINE.Coq_fpu_flag_loc(ff) ->
    str_of_fpu_flag ff
  | X86_MACHINE.Coq_fpu_rctrl_loc -> "FPU_RCtrl"
  | X86_MACHINE.Coq_fpu_pctrl_loc -> "FPU_PCtrl"
  | X86_MACHINE.Coq_fpu_ctrl_flag_loc(fcf) ->
    str_of_fpu_ctrl_flag fcf
  | X86_MACHINE.Coq_fpu_lastInstrPtr_loc -> "FPU_lastInstrPtr"
  | X86_MACHINE.Coq_fpu_lastDataPtr_loc -> "FPU_lastDataPtr"
  | X86_MACHINE.Coq_fpu_lastOpcode_loc -> "FPU_lastOpcode"

let str_of_array arr = match arr with
  | X86_MACHINE.Coq_fpu_datareg -> "FPU_datareg"
  | X86_MACHINE.Coq_fpu_tag -> "FPU_tag"

let str_of_faop faop = match faop with
    R.Coq_fadd_op -> "+."
  | R.Coq_fsub_op -> "-."
  | R.Coq_fmul_op -> "*."
  | R.Coq_fdiv_op -> "/."




let gen_fact lst =
  let fact = "" in
  let rec iterate lst fact =
    match lst with
    | [] -> fact
    | [x] -> fact ^ x ^ "\n"
  	| x::xs -> iterate xs (fact ^ x ^ "\t")
  in iterate lst fact



let rec str_of_rtl_exp_less_size lst rtl_e =
  match rtl_e with
  | R.Coq_arith_rtl_exp(s, bvop, rtl_e1, rtl_e2) ->
 	let id1 = str_of_rtl_exp_less_size lst rtl_e1 in
	let id2 = str_of_rtl_exp_less_size lst rtl_e2 in
	let bvop_str = (str_of_bvop bvop) in
	let size = str_of_size s in
	let value = (size, bvop_str, id1, id2) in

	let key = value_exist arith_tbl value in
	if key = "Not_found" then
   	  (counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; size; bvop_str; id1; id2]
      in let temp = List.nth lst 0 in temp := fact::!temp;
	  H.add arith_tbl key value;
	  key)
	else key


  | R.Coq_test_rtl_exp(s, top, rtl_e1, rtl_e2) ->
 	let id1 = str_of_rtl_exp_less_size lst rtl_e1 in
	let id2 = str_of_rtl_exp_less_size lst rtl_e2 in
	let top_str = (str_of_top top) in
	let size = str_of_size s in
	let value = (size, top_str, id1, id2) in

	let key = value_exist test_tbl value in
	if key = "Not_found" then
   	  (counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; size; top_str; id1; id2]
      in let temp = List.nth lst 1 in temp := fact::!temp;
	  H.add test_tbl key value;
	  key)
	else key


  | R.Coq_if_rtl_exp(s, rtl_e1, rtl_e2, rtl_e3) ->
 	let id1 = str_of_rtl_exp_less_size lst rtl_e1 in
	let id2 = str_of_rtl_exp_less_size lst rtl_e2 in
	let id3 = str_of_rtl_exp_less_size lst rtl_e3 in
	let size = str_of_size s in
	let value = (size, id1, id2, id3) in

	let key = value_exist if_tbl value in
	if key = "Not_found" then
   	  (counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; size; id1; id2; id3]
      in let temp = List.nth lst 2 in temp := fact::!temp;
	  H.add if_tbl key value;
	  key)
	else key

  | R.Coq_cast_s_rtl_exp(s1, s2, rtl_e') ->
 	let id' = str_of_rtl_exp_less_size lst rtl_e' in
	let size1 = str_of_size s1 in
	let size2 = str_of_size s2 in
	let value = (size1, size2, id') in

	let key = value_exist cast_s_tbl value in
	if key = "Not_found" then
   	  (counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; size1; size2; id']
      in let temp = List.nth lst 3 in temp := fact::!temp;
	  H.add cast_s_tbl key value;
	  key)
	else key


  | R.Coq_cast_u_rtl_exp(s1, s2, rtl_e') -> (* from s1 to s2 *)

  begin match rtl_e' with
 | R.Coq_get_byte_rtl_exp(rtl_e'') ->
  let id = str_of_rtl_exp_less_size lst rtl_e'' in
  let size = string_of_int ((int_of_string (str_of_size s2)) / 8) in
  let value = (size, id) in

  let key = value_exist get_mem_tbl value in
  if key = "Not_found" then
    (counter := !counter + 1;
    let key = int_to_str (!counter) in
    let fact = gen_fact [ key; size; id]
    in let temp = List.nth lst 22 in temp := fact::!temp;
    H.add get_mem_tbl key value;
    key)
  else key
 | _ ->

 	let id' = str_of_rtl_exp_less_size lst rtl_e' in
	let size1 = str_of_size s1 in
	let size2 = str_of_size s2 in
	let value = (size1, size2, id') in

	let key = value_exist cast_u_tbl value in
	if key = "Not_found" then
   	  (counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; size1; size2; id']
      in let temp = List.nth lst 4 in temp := fact::!temp;
	  H.add cast_u_tbl key value;
	  key)
	else key
  end

  | R.Coq_imm_rtl_exp(s, v) ->
	let imm_value = string_of_int (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int v))) in
	let size = str_of_size s in
	let value = (size,imm_value) in
	let key = value_exist imm_tbl value in
	if key = "Not_found" then
	  (counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; size; imm_value]
      in let temp = List.nth lst 5 in temp := fact::!temp;
	  H.add imm_tbl key value;
	  key)
	else key

  | R.Coq_get_loc_rtl_exp(s, loc) ->
	let loc_value = (str_of_loc loc) in
	let key = value_exist loc_tbl loc_value in
	if key = "Not_found" then
	  (counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; loc_value]
      in let temp = List.nth lst 6 in temp := fact::!temp;
	  H.add loc_tbl key loc_value;
	  key)
	else key

  | R.Coq_get_ps_reg_rtl_exp(s, ps_reg) ->
	let ps_reg_value = str_of_ps_reg ps_reg in
	let size = str_of_size s in
	let value = (size, ps_reg_value) in
	let key = value_exist ps_reg_tbl value in
	if key = "Not_found" then
   	  (counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; size; ps_reg_value]
      in let temp = List.nth lst 7 in temp := fact::!temp;
	  H.add ps_reg_tbl key value;
	  key)
	else key

  | R.Coq_get_array_rtl_exp(l, s, arr, rtl_e') ->
 	let id' = str_of_rtl_exp_less_size lst rtl_e' in
	let arr_value = (str_of_array arr) in (**may not include in the fact**)
	let size = str_of_size s in
	let l_str = (to_string l) in
	let value = (size, arr_value, l_str, id') in
	let key = value_exist arr_tbl value in
	if key = "Not_found" then
   	  (counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; size; l_str; id']
      in let temp = List.nth lst 8 in temp := fact::!temp;
	  H.add arr_tbl key value;
	  key)
	else key

  | R.Coq_get_byte_rtl_exp(rtl_e') ->
 	let id' = str_of_rtl_exp_less_size lst rtl_e' in
	let key = value_exist byte_tbl id' in
	if key = "Not_found" then
   	  (counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; id']
      in let temp = List.nth lst 9 in temp := fact::!temp;
	  H.add byte_tbl key id';
	  key)
	else key

  | R.Coq_get_random_rtl_exp(s) ->
	let size = str_of_size s in
	let key = value_exist random_tbl size in
	if key = "Not_found" then
	  (counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; size]
      in let temp = List.nth lst 10 in temp := fact::!temp;
	  H.add random_tbl key size;
	  key)
	else key

  | R.Coq_farith_rtl_exp(ew,mw,faop,rtl_rm,rtl_e1,rtl_e2) ->
	let size_ew = str_of_size ew in (**sizes may not be used **)
	let size_mw = str_of_size mw in
	let faop_str = (str_of_faop faop) in

 	let id_rm = str_of_rtl_exp_less_size lst rtl_rm in
	let id1   = str_of_rtl_exp_less_size lst rtl_e1 in
	let id2   = str_of_rtl_exp_less_size lst rtl_e2 in

	let value = (size_ew, size_mw, faop_str, id_rm, id1, id2) in
	let key = value_exist farith_tbl value in
	if key = "Not_found" then
   	  (counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; faop_str; id_rm; id1; id2]
      in let temp = List.nth lst 11 in temp := fact::!temp;
	  H.add farith_tbl key value;
	  key)
	else key

  | R.Coq_fcast_rtl_exp(s1,s2,s3,s4,rtl_e1,rtl_e2) ->
	let size1 = str_of_size s1 in (**sizes may not be used **)
	let size2 = str_of_size s2 in
	let size3 = str_of_size s3 in
	let size4 = str_of_size s4 in

	let id1   = str_of_rtl_exp_less_size lst rtl_e1 in
	let id2   = str_of_rtl_exp_less_size lst rtl_e2 in

	let value = (size1, size2, size3, size4, id1, id2) in
	let key = value_exist fcast_tbl value in
	if key = "Not_found" then
   	  (counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; id1; id2]
      in let temp = List.nth lst 12 in temp := fact::!temp;
	  H.add fcast_tbl key value;
	  key)
	else key

(*
let get_byte_to_get_mem lst rtl_e size =
  let id = str_of_rtl_exp_less_size lst rtl_e in
  let size = string_of_int ((int_of_string (str_of_size size)) / 8) in
  let value = (size, id) in

  let key = value_exist get_mem_tbl value in
  if key = "Not_found" then
    (counter := !counter + 1;
    let key = int_to_str (!counter) in
    let fact = gen_fact [ key; size; id]
    in let temp = List.nth lst 22 in temp := fact::!temp;
    H.add get_mem_tbl key value;
    key)
  else key
*)


let gen_get_mem lst rtl_e size =

  let id = str_of_rtl_exp_less_size lst rtl_e in
  let value = (size, id) in
  let key = value_exist get_mem_tbl value in
  if key = "Not_found" then
    (counter := !counter + 1;
    let key = int_to_str (!counter) in
    let fact = gen_fact [ key; size; id]
    in let temp = List.nth lst 22 in temp := fact::!temp;
    H.add get_mem_tbl key value;
    key)
  else key


let combine_rtl_expr_add_dword lst rtl_e1 rtl_e2 s bvop get_mem_size =
  let id1 = gen_get_mem lst rtl_e1 get_mem_size in
  let id2 = str_of_rtl_exp_less_size lst rtl_e2 in
  let bvop_str = (str_of_bvop bvop) in
  let size = str_of_size s in
  let value = (size, bvop_str, id1, id2) in
  let key = value_exist arith_tbl value in
  if key = "Not_found" then
    (counter := !counter + 1;
    let key = int_to_str (!counter) in
    let fact = gen_fact [ key; size; bvop_str; id1; id2]
    in let temp = List.nth lst 0 in temp := fact::!temp;
    H.add arith_tbl key value;
    key)
  else key

let combine_rtl_expr lst rtl_e1 rtl_e2 s bvop get_mem_size =
  let id1 = str_of_rtl_exp_less_size lst rtl_e1 in
  let id2 = gen_get_mem lst rtl_e2 get_mem_size in

  let bvop_str = (str_of_bvop bvop) in
  let size = str_of_size s in
  let value = (size, bvop_str, id1, id2) in
  let key = value_exist arith_tbl value in
  if key = "Not_found" then
    (counter := !counter + 1;
    let key = int_to_str (!counter) in
    let fact = gen_fact [ key; size; bvop_str; id1; id2]
    in let temp = List.nth lst 0 in temp := fact::!temp;
    H.add arith_tbl key value;
    key)
  else key





let rec str_of_rtl_instr_less_size lst i addr rtl_i  =
  match rtl_i with
  (*rtl_e - source, loc - destination //// format=>  dest <-- source*)
  | R.Coq_set_loc_rtl(s, rtl_e, loc) ->
   let size = str_of_size s in
   let loc_value = (str_of_loc loc) in
   let (rtl_e_upd, sim, get_mem_size) = GenFactHelper.simplify_rtl_exp rtl_e in (**simplify if possible**)
   let key = value_exist loc_tbl loc_value in
  let loc = str_of_loc loc in

  if (loc = "CF" ||
	 loc = "OF" ||
	 loc = "ZF" ||
	 loc = "SF" ||
	 loc = "AF") then (


	if key = "Not_found" then (
	  counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; loc_value]
      in let temp = List.nth lst 6 in temp := fact::!temp;
	  H.add loc_tbl key loc_value;
      let id = str_of_rtl_exp_less_size lst rtl_e in
  	  let fact = gen_fact [addr; (string_of_int i); size; id; key] in (** id = rtl_e /// key = loc**)
      let temp = List.nth lst 13 in temp := fact::!temp;
	) else (
      let id = str_of_rtl_exp_less_size lst rtl_e in
	  let fact = gen_fact [addr; (string_of_int i); size; id; key] in
      let temp = List.nth lst 13 in temp := fact::!temp;
	)


  )
  else

   if sim = 0 then (
	if key = "Not_found" then (
	  counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; loc_value]
      in let temp = List.nth lst 6 in temp := fact::!temp;
	  H.add loc_tbl key loc_value;
      let id = str_of_rtl_exp_less_size lst rtl_e_upd in

  	  let fact = gen_fact [addr; (string_of_int i); size; id; key] in (** id = rtl_e /// key = loc**)
      let temp = List.nth lst 13 in temp := fact::!temp;
	) else (
      let id = str_of_rtl_exp_less_size lst rtl_e_upd in
	  let fact = gen_fact [addr; (string_of_int i); size; id; key] in
      let temp = List.nth lst 13 in temp := fact::!temp;
	))
  else if sim = 1 || sim = 3 then (

	if key = "Not_found" then (
	  counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; loc_value]
      in let temp = List.nth lst 6 in temp := fact::!temp;
	  H.add loc_tbl key loc_value;
      let id = gen_get_mem lst rtl_e_upd get_mem_size in
  	  let fact = gen_fact [addr; (string_of_int i); size; id; key] in (** id = rtl_e /// key = loc**)
      let temp = List.nth lst 13 in temp := fact::!temp;
	) else (
      let id = gen_get_mem lst rtl_e_upd get_mem_size in
	  let fact = gen_fact [addr; (string_of_int i); size; id; key] in
      let temp = List.nth lst 13 in temp := fact::!temp;
	))


  else if sim = 4 then (
   begin match rtl_e with
    | R.Coq_arith_rtl_exp(s, bvop, rtl_e1, _) ->

		if key = "Not_found" then (
		  counter := !counter + 1;
		  let key = int_to_str (!counter) in
		  let fact = gen_fact [ key; loc_value]
		  in let temp = List.nth lst 6 in temp := fact::!temp;
		  H.add loc_tbl key loc_value;
          let id = combine_rtl_expr lst rtl_e1 rtl_e_upd s bvop get_mem_size in
	  	  let fact = gen_fact [addr; (string_of_int i); size; id; key] in (** id = rtl_e /// key = loc**)
		  let temp = List.nth lst 13 in temp := fact::!temp;
		) else (
          let id = combine_rtl_expr lst rtl_e1 rtl_e_upd s bvop get_mem_size in
		  let fact = gen_fact [addr; (string_of_int i); size; id; key] in
		  let temp = List.nth lst 13 in temp := fact::!temp;
		)
    | _ -> ( raise (Failure (addr ^ "**")) )
   end

  )
  else (
	if key = "Not_found" then (
	  counter := !counter + 1;
	  let key = int_to_str (!counter) in
	  let fact = gen_fact [ key; loc_value]
      in let temp = List.nth lst 6 in temp := fact::!temp;
	  H.add loc_tbl key loc_value;
      let id = str_of_rtl_exp_less_size lst rtl_e_upd in

  	  let fact = gen_fact [addr; (string_of_int i); size; id; key] in (** id = rtl_e /// key = loc**)
      let temp = List.nth lst 13 in temp := fact::!temp;
	) else (
      let id = str_of_rtl_exp_less_size lst rtl_e_upd in

	  let fact = gen_fact [addr; (string_of_int i); size; id; key] in
      let temp = List.nth lst 13 in temp := fact::!temp;
	)

  );
    i_counter := !i_counter + 1

  | R.Coq_set_ps_reg_rtl(s, rtl_e, ps_reg) ->
	let id = str_of_rtl_exp_less_size lst rtl_e in
	let size = str_of_size s in
	let ps_reg_value = str_of_ps_reg ps_reg in

	let fact = gen_fact [addr; (string_of_int i); size; id; ps_reg_value] in
	let temp = List.nth lst 14 in temp := fact::!temp;
	i_counter := !i_counter + 1

  | R.Coq_set_array_rtl(l,s,arr,rtl_e1,rtl_e2) ->
	let id1 = str_of_rtl_exp_less_size lst rtl_e1 in
	let id2 = str_of_rtl_exp_less_size lst rtl_e2 in

  	let fact = gen_fact [addr; (string_of_int i); (to_string l); (str_of_array arr); id1; id2]
	in let temp = List.nth lst 15 in temp := fact::!temp;
	i_counter := !i_counter + 1

  | R.Coq_set_byte_rtl(rtl_e1, rtl_e2) ->
	let (rtl_e1,_, _) = GenFactHelper.simplify_rtl_exp rtl_e1 in (**simplify if possible**)

	let id1 = str_of_rtl_exp_less_size lst rtl_e1 in
	let id2 = str_of_rtl_exp_less_size lst rtl_e2 in

	let fact = gen_fact [addr; (string_of_int i); id1; id2]
	in let temp = List.nth lst 16 in temp := fact::!temp;
	i_counter := !i_counter + 1

  | R.Coq_advance_oracle_rtl ->
	let fact = gen_fact [addr; (string_of_int i)]
	in let temp = List.nth lst 17 in temp := fact::!temp;
	i_counter := !i_counter + 1

  | R.Coq_if_rtl(rtl_e, rtl_i') ->
	let id = str_of_rtl_exp_less_size lst rtl_e in

	let fact = gen_fact [addr; (string_of_int i); id; (i_int_to_str(!i_counter))]
	in let temp = List.nth lst 18 in temp := fact::!temp;
	(str_of_rtl_instr_less_size lst i addr rtl_i');
	i_counter := !i_counter + 1

  | R.Coq_error_rtl ->
	let fact = gen_fact [addr; (string_of_int i)]
	in let temp = List.nth lst 19 in temp := fact::!temp;
	i_counter := !i_counter + 1

  | R.Coq_trap_rtl ->
	let fact = gen_fact [addr; (string_of_int i)]
	in let temp = List.nth lst 20 in temp := fact::!temp;
	i_counter := !i_counter + 1



let get_size_from_rtl_exp rtl_e =
  match rtl_e with
  | R.Coq_cast_u_rtl_exp(size_bit,_,_) ->
	let size_byte = (((Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int size_bit))) + 1) / 8) in

	size_byte
  | _ -> 4

let print_rtl_instr addr new_ll inter_succ_lst =
  let l0 = ref [] in let l1 = ref [] in let l2 = ref [] in let l3 = ref [] in let l4 = ref [] in
  let l5 = ref [] in let l6 = ref [] in let l7 = ref [] in let l8 = ref [] in
  let l9 = ref [] in let l10 = ref [] in let l11 = ref [] in let l12 = ref [] in
  let l13 = ref [] in let l14 = ref [] in let l15 = ref [] in let l16 = ref [] in
  let l17 = ref [] in let l18 = ref [] in let l19 = ref [] in let l20 = ref [] in
  let l21 = ref [] in let l22 = ref [] in
  let lst = [l0;l1;l2;l3;l4;l5;l6;l7;l8;l9;l10;l11;l12;l13;l14;l15;l16;l17;l18;l19;l20;l21;l22] in

  let rec iterate i new_ll lst =
   match new_ll with
   |[] ->
      i_counter := 1;
	  (!l0,!l1,!l2,!l3,!l4,!l5,!l6,!l7,!l8,!l9,!l10,!l11,!l12,!l13,!l14,!l15,!l16,!l17,!l18,!l19,!l20,!l21,!l22, inter_succ_lst)
   |rtl_i::l ->
 begin match rtl_i with
  | R.Coq_set_byte_rtl(rtl_e1, rtl_e2) ->
   let size_byte = string_of_int(get_size_from_rtl_exp rtl_e1) in
   let (rtl_e1,sim, get_mem_size) = GenFactHelper.simplify_rtl_exp rtl_e1 in (**simplify if possible**)

	if sim = 0 then (

	  let rtl_e1 = GenFactHelper.get_byte_prune_cast rtl_e1 in
	  let id1 = str_of_rtl_exp_less_size lst rtl_e1 in
	  (**let id2 = str_of_rtl_exp_less_size lst rtl_e2 in**)
	  let id2 = str_of_rtl_exp_less_size lst rtl_e2 in


	  let fact = gen_fact [addr; string_of_int(i); size_byte; id1; id2] in (*size 4 in the third parameter*)
	  let temp = List.nth lst 21 in temp := fact::!temp) (***** set_byte -> 16, set_mem -> 21 **********)
	else if sim = 1 then (
	  let id1 = str_of_rtl_exp_less_size lst rtl_e1 in
	  let id2 = str_of_rtl_exp_less_size lst rtl_e2 in

	  let fact = gen_fact [addr; string_of_int(i); size_byte; id1; id2] in
	  let temp = List.nth lst 21 in temp := fact::!temp)

	else if sim = 3 || sim = 4 then (
	  let id1 = gen_get_mem lst rtl_e1 get_mem_size in
	  (**let id1 = str_of_rtl_exp_less_size lst rtl_e1 in**)
	  let id2 = str_of_rtl_exp_less_size lst rtl_e2 in

	  let fact = gen_fact [addr; string_of_int(i); size_byte; id1; id2] in
	  let temp = List.nth lst 21 in temp := fact::!temp)

	else (

	  begin match rtl_e1 with
	   | R.Coq_arith_rtl_exp(s, bvop, rtl_e11, rtl_e22) ->
		begin match rtl_e11 with
		 | R.Coq_arith_rtl_exp(_, _, rtl_e', _) ->
		   begin match rtl_e' with
			 | R.Coq_cast_u_rtl_exp(_,cast_size,rtl_e'') ->
			  begin match rtl_e'' with
			   R.Coq_get_byte_rtl_exp(rtl_e''') ->
	 		    let str_cast_size = size_bit_to_byte_str cast_size in
	  		    let id1 = combine_rtl_expr_add_dword lst rtl_e''' rtl_e22 s bvop str_cast_size in
	  		    let id2 = str_of_rtl_exp_less_size lst rtl_e2 in
	  		    let fact = gen_fact [addr; string_of_int(i); size_byte; id1; id2] in
	  		    let temp = List.nth lst 21 in temp := fact::!temp
			   | _ -> ()
			  end
			 | _ -> ()
		   end
		 | _ -> ()
		end
	   | _ -> ()
	  end);

	 iterate (i+1) l lst

  | R.Coq_set_loc_rtl(_, _, loc) ->
  	if (str_of_loc loc) = "PF" then (iterate i l lst)

	else (str_of_rtl_instr_less_size lst i addr rtl_i;
		 iterate (i+1) l lst)
  | _ ->
    str_of_rtl_instr_less_size lst i addr rtl_i;
	iterate (i+1) l lst
  end

 in iterate 1 new_ll lst


let gen_call_length ins loc len =
  match ins with
  | CALL (_,_,_,_) ->
	let addr = str_of_mword_dec (MWord.of_int loc) in
	let next_addr = string_of_int ((int_of_string addr) + len) in
	let fact = gen_fact [addr; next_addr] in
	[fact]
  | _ -> []



let rtl_to_datalog pre ins loc len =
  let ll = X86_Compile.instr_to_rtl pre ins in
  let ll = SimplifyFlag.simpFlag ll in
  let simpri = RTL_opt.simpri_all_methods in
  let simpre = RTL_opt.simpre_all_methods in
  let new_ll = GenFactHelper.remove_instrs (RTL_opt.simplify_rtl simpri simpre ll) in
  let addr = str_of_mword_dec (MWord.of_int loc) in
  let inter_succ_lst = gen_call_length ins loc len in
  print_rtl_instr addr new_ll inter_succ_lst


let concat_tuple lst =
  let l0 = [] in let l1 = [] in let l2 = [] in let l3 = [] in let l4 = [] in let l5 = [] in let l6 = [] in
  let l7 = [] in let l8 = [] in let l9 = [] in let l10 = [] in let l11 = [] in let l12 = [] in
  let l13 = [] in let l14 = [] in let l15 = [] in let l16 = [] in let l17 = [] in let l18 = [] in
  let l19 = [] in let l20 = [] in let l21 = [] in let l22 = [] in let l23 = [] in

  let rec iterate lst l0 l1 l2 l3 l4 l5 l6 l7 l8 l9 l10 l11 l12 l13 l14 l15 l16 l17 l18 l19 l20 l21 l22 l23 =
    match lst with
    |[] -> (l0,l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12,l13,l14,l15,l16,l17,l18,l19,l20, l21,l22, l23)
    | e::l ->
	  let (e0,e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12,e13,e14,e15,e16,e17,e18,e19,e20,e21,e22, e23) = e in
   	  iterate l (List.append e0 l0) (List.append e1 l1) (List.append e2 l2)
	  (List.append e3 l3) (List.append e4 l4) (List.append e5 l5) (List.append e6 l6)
	  (List.append e7 l7) (List.append e8 l8) (List.append e9 l9) (List.append e10 l10)
	  (List.append e11 l11) (List.append e12 l12) (List.append e13 l13) (List.append e14 l14)
	  (List.append e15 l15) (List.append e16 l16) (List.append e17 l17) (List.append e18 l18)
	  (List.append e19 l19) (List.append e20 l20) (List.append e21 l21) (List.append e22 l22)
	  (List.append e23 l23)

  in iterate lst l0 l1 l2 l3 l4 l5 l6 l7 l8 l9 l10 l11 l12 l13 l14 l15 l16 l17 l18 l19 l20 l21 l22 l23



let fact_bb_to_ins bb =
  let bb_instrs = BB.instrs_with_starts (bb.bb_relAddr) bb.bb_instrs in
  concat_tuple (List.map
	(fun instr -> let (loc,pre,ins,len) = instr
				  in rtl_to_datalog pre ins loc len) bb_instrs)



let gen_addr_bounds t =
  let ret_lst = [] in
  let rec iterate ret_lst t =
   match t with
  | [] -> ret_lst
  | e1::e2::l ->
    let (func, addr_lower) = e1 in
	let (_, addr_upper) = e2 in
    iterate ((addr_lower - 134512640, addr_upper - 134512640, func)::ret_lst) (e2::l)
  | e::l ->
    let (func, addr_lower) = e in
	((addr_lower - 134512640 , 1073741823 - 134512640, func)::ret_lst)
  in iterate ret_lst t


let gen_datalog_fact bbtbl =

  let coq_arith_rtl_exp_fact = open_out "arith_rtl_exp.facts" in (*0*)
  let coq_test_rtl_exp_fact = open_out "test_rtl_exp.facts" in (*1*)
  let coq_if_rtl_exp_fact = open_out "if_rtl_exp.facts" in (*2*)
  let coq_cast_s_rtl_exp_fact = open_out "cast_s_rtl_exp.facts" in (*3*)
  let coq_cast_u_rtl_exp_fact = open_out "cast_u_rtl_exp.facts" in (*4*)
  let coq_imm_rtl_exp_fact = open_out "imm_rtl_exp.facts" in (*5*)
  let coq_get_loc_rtl_exp_fact = open_out "get_loc_rtl_exp.facts" in (*6*)
  let coq_get_ps_reg_rtl_exp_fact = open_out "get_ps_reg_rtl_exp.facts" in (*7*)
  let coq_get_array_rtl_exp_fact = open_out "get_array_rtl_exp.facts" in (*8*)
  let coq_get_byte_rtl_exp_fact = open_out "get_byte_rtl_exp.facts" in (*9*)
  let coq_get_random_rtl_exp_fact = open_out "get_random_rtl_exp.facts" in (*10*)
  let coq_farith_rtl_exp_fact = open_out "farith_rtl_exp.facts" in (*11*)
  let coq_fcast_rtl_exp_fact = open_out "fcast_rtl_exp.facts" in (*12*)

  let coq_set_loc_rtl_fact = open_out "set_loc_rtl.facts" in (*13*)
  let coq_set_ps_reg_rtl_fact = open_out "set_ps_reg_rtl.facts" in (*14*)
  let coq_set_array_rtl_fact = open_out "set_array_rtl.facts" in (*15*)
  let coq_set_byte_rtl_fact = open_out "set_byte_rtl.facts" in (*16*)
  let coq_advance_oracle_rtl_fact = open_out "advance_oracle_rtl.facts" in (*17*)
  let coq_if_rtl_fact = open_out "if_rtl.facts" in (*18*)
  let coq_error_rtl_fact = open_out "error_rtl.facts" in (*19*)
  let coq_trap_rtl_fact = open_out "trap_rtl.facts" in (*20*)

  let set_mem_rtl_fact = open_out "set_mem_rtl.facts" in (*21*)
  let get_mem_rtl_exp_fact = open_out "get_mem_rtl_exp.facts" in (*22*)

  let inter_succ_fact = open_out "inter_succ.facts" in (*23*)


  let bb_lst = H.fold (fun lbl bb lst -> (bb::lst)) bbtbl [] in
  let bb_symbol_lst = H.fold (fun lbl bb lst ->
    if (String.length bb.bb_symbol_label) > 0 then
      let bb_label_int = int_of_string ("0x" ^ bb.bb_label) in
	    ((bb.bb_symbol_label, bb_label_int)::lst)  
(*
	    if Set.mem bb.bb_symbol_label compiler_symbol_set then lst
		else ((bb.bb_symbol_label, bb_label_int)::lst)  
*)
    else lst ) bbtbl [] in

  let sorted_bb_symbol_lst = sort_tuple bb_symbol_lst in
  let addr_bounds = gen_addr_bounds sorted_bb_symbol_lst in

  let addr_func = open_out "addr_func.facts" in
  List.iter (fun (addr_lower, addr_upper, func) -> P.fprintf addr_func "%s\t%d\t%d\n" func addr_lower addr_upper) addr_bounds;
  close_out addr_func;

  let (l0,l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12,l13,l14,l15,l16,l17,l18,l19,l20,l21,l22, l23) = concat_tuple (List.map (fun bb -> fact_bb_to_ins bb) bb_lst) in

  List.iter (fun s -> P.fprintf coq_arith_rtl_exp_fact "%s" s) l0;
  List.iter (fun s -> P.fprintf coq_test_rtl_exp_fact "%s" s) l1;
  List.iter (fun s -> P.fprintf coq_if_rtl_exp_fact "%s" s) l2;
  List.iter (fun s -> P.fprintf coq_cast_s_rtl_exp_fact "%s" s) l3;
  List.iter (fun s -> P.fprintf coq_cast_u_rtl_exp_fact "%s" s) l4;
  List.iter (fun s -> P.fprintf coq_imm_rtl_exp_fact "%s" s) l5;
  List.iter (fun s -> P.fprintf coq_get_loc_rtl_exp_fact "%s" s) l6;
  List.iter (fun s -> P.fprintf coq_get_ps_reg_rtl_exp_fact "%s" s) l7;
  List.iter (fun s -> P.fprintf coq_get_array_rtl_exp_fact "%s" s) l8;
  List.iter (fun s -> P.fprintf coq_get_byte_rtl_exp_fact "%s" s) l9;
  List.iter (fun s -> P.fprintf coq_get_random_rtl_exp_fact "%s" s) l10;
  List.iter (fun s -> P.fprintf coq_farith_rtl_exp_fact "%s" s) l11;
  List.iter (fun s -> P.fprintf coq_fcast_rtl_exp_fact "%s" s) l12;

  List.iter (fun s -> P.fprintf coq_set_loc_rtl_fact "%s" s) l13;
  List.iter (fun s -> P.fprintf coq_set_ps_reg_rtl_fact "%s" s) l14;
  List.iter (fun s -> P.fprintf coq_set_array_rtl_fact "%s" s) l15;
  List.iter (fun s -> P.fprintf coq_set_byte_rtl_fact "%s" s) l16;
  List.iter (fun s -> P.fprintf coq_advance_oracle_rtl_fact "%s" s) l17;
  List.iter (fun s -> P.fprintf coq_if_rtl_fact "%s" s) l18;
  List.iter (fun s -> P.fprintf coq_error_rtl_fact "%s" s) l19;
  List.iter (fun s -> P.fprintf coq_trap_rtl_fact "%s" s) l20;

  List.iter (fun s -> P.fprintf set_mem_rtl_fact "%s" s) l21;
  List.iter (fun s -> P.fprintf get_mem_rtl_exp_fact "%s" s) l22;

  List.iter (fun s -> P.fprintf inter_succ_fact "%s" s) l23;

  close_out coq_arith_rtl_exp_fact;
  close_out coq_test_rtl_exp_fact;
  close_out coq_if_rtl_exp_fact;
  close_out coq_cast_s_rtl_exp_fact;
  close_out coq_cast_u_rtl_exp_fact;
  close_out coq_imm_rtl_exp_fact;
  close_out coq_get_loc_rtl_exp_fact;
  close_out coq_get_ps_reg_rtl_exp_fact;
  close_out coq_get_array_rtl_exp_fact;
  close_out coq_get_byte_rtl_exp_fact;
  close_out coq_get_random_rtl_exp_fact;
  close_out coq_farith_rtl_exp_fact;
  close_out coq_fcast_rtl_exp_fact;


  close_out coq_set_loc_rtl_fact;
  close_out coq_set_ps_reg_rtl_fact;
  close_out coq_set_array_rtl_fact;
  close_out coq_set_byte_rtl_fact;
  close_out coq_advance_oracle_rtl_fact;
  close_out coq_if_rtl_fact;
  close_out coq_error_rtl_fact;
  close_out coq_trap_rtl_fact;

  close_out set_mem_rtl_fact;
  close_out get_mem_rtl_exp_fact;
  close_out inter_succ_fact

