


module H = Hashtbl
module P = Printf

open Util

open Config

open Bits
open X86Syntax
open Abbrev
open Instr
open Elf


(*** added by Sun ***)
open X86Semantics
open RtlCompact

open Printer
(*
module H = Hashtbl

**)



(*******Sun: will merge this file (test.ml) and makeFacts.ml (in binary/cfg_gen/src/cfggen/advancedis file later ******)



let uniq lst =
  let seen = H.create (List.length lst) in
  List.filter (fun x -> let tmp = not (H.mem seen x) in
                        H.replace seen x ();
                        tmp) lst


(*** locals:     (string * int) list   <=> [(func,[sorted offsets); ... ]    ***)
let func_with_offsets locals = 
 let func_off_map = H.create 32 in
   List.iter ( fun(s, i) -> H.add func_off_map s i ) locals;
 let func_lst = uniq (H.fold (fun func off lst -> func::lst) func_off_map [] ) in 
 func_off_map,func_lst


(*******
(*e.g.  (var_-4, 4) *)
let local_aloc_gen_pos lst prev func init_addr =
  let comma = "," in
  let quota = "\"" in
  let closing_bracket = ")." in
  let fact_front = "local_aloc(" ^ quota ^ func ^ quota ^ comma in
  let ret_lst = [] in
  let rec iterate lst ret_lst prev  =
    match lst with
	| [] -> ret_lst
	| e::l ->
	  let var = "\"var_" ^ (string_of_int (-e)) ^ quota in
	  let aloc = fact_front ^ var ^ comma ^ (string_of_int (e)) ^ comma in
	  let size = string_of_int (abs(e - prev)) in
	  let aloc = aloc ^ size ^ closing_bracket in
		iterate l ( aloc::ret_lst) e
  in iterate lst ret_lst prev


(*e.g.  (var_44, -44) *)
let local_aloc_gen_neg lst last func init_addr =
  let comma = "," in
  let quota = "\"" in
  let closing_bracket = ")." in
  let start_addr = "start_addr(" ^ quota ^ func ^ quota ^ comma ^ quota ^ init_addr ^ quota ^ closing_bracket in
  let fact_front = "local_aloc(" ^ quota ^ func ^ quota ^ comma in
  let ret_lst = [start_addr] in

  let rec iterate lst ret_lst =
    match lst with
   | a::b:: l ->
	 let fact = fact_front ^ "\"var_" ^ (string_of_int (-a)) ^ quota ^ comma ^ (string_of_int a) ^ comma ^ (string_of_int( b - a)) ^ closing_bracket
     in iterate (b::l) (fact::ret_lst)
   | a:: l ->
	 let fact = fact_front ^ "\"var_" ^ (string_of_int (-a)) ^ quota ^ comma ^ (string_of_int a) ^ comma ^ (string_of_int( last - a)) ^ closing_bracket
     in iterate l (fact::ret_lst)
   | [] -> ret_lst
  in iterate lst ret_lst 



let local_aloc_gen locals func_relAddr = 
  let (func_off_map,func_lst) = func_with_offsets locals in 
  List.flatten (List.map (fun func -> 
 	 (*let lst = List.sort compare (H.find_all func_off_map func) in *)
	 let lst = H.find_all func_off_map func in
	 let lst_pos = List.filter (fun x -> x > 0) lst in
	 let lst_pos = List.sort compare lst_pos in
	 let lst_neg = List.filter (fun x -> x < 0) lst in
	 let lst_neg = List.sort compare lst_neg in
  
	 let (_,init_addr) = List.find (fun (symbol_label, _) -> func = symbol_label) func_relAddr in 
	 let new_lst = List.append (local_aloc_gen_pos lst_pos 0 func init_addr) (local_aloc_gen_neg lst_neg 0 func init_addr) in 
	 new_lst ) func_lst)

********)

(*e.g.  (var_-4, 4) *)
let local_aloc_gen_pos lst prev func init_addr =
  let ret_lst = [] in
  let rec iterate lst ret_lst prev  =
    match lst with
	| [] -> ret_lst
	| e::l ->
	  let var = "var_" ^ (string_of_int (-e)) in
	  let size = abs(e - prev) in
	  let aloc = (func, var, e, size) in (* e = offset*)
		iterate l ( aloc::ret_lst) e
  in iterate lst ret_lst prev


(*e.g.  (var_44, -44) *)
let local_aloc_gen_neg lst last func init_addr =
  let ret_lst = [] in
  let rec iterate lst ret_lst =
    match lst with
   | a::b:: l ->
  	 let var = "var_" ^ (string_of_int (-a)) in
	 let size = b - a in 
	 let fact = (func, var, a, size)
     in iterate (b::l) (fact::ret_lst)
   | a:: l ->
  	 let var = "var_" ^ (string_of_int (-a)) in
	 let size = last - a in 
	 let fact = (func, var, a, size)
     in iterate l (fact::ret_lst)
   | [] -> ret_lst
  in iterate lst ret_lst 



let local_aloc_gen locals func_relAddr = 
  let (func_off_map,func_lst) = func_with_offsets locals in 
  List.flatten (List.map (fun func -> 
 	 (*let lst = List.sort compare (H.find_all func_off_map func) in *)
	 let lst = H.find_all func_off_map func in
	 let lst_pos = List.filter (fun x -> x > 0) lst in
	 let lst_pos = List.sort compare lst_pos in
	 let lst_neg = List.filter (fun x -> x < 0) lst in
	 let lst_neg = List.sort compare lst_neg in
  
	 let (_,init_addr) = List.find (fun (symbol_label, _) -> func = symbol_label) func_relAddr in 
	 let new_lst = List.append (local_aloc_gen_pos lst_pos 0 func init_addr) (local_aloc_gen_neg lst_neg 0 func init_addr) in 
	 new_lst ) func_lst)




(**
let global_aloc_gen lst lower upper sec =
  (*let fact_front = "aloc(\"global\"," in*)
  let lst = lower::lst in
  let fact_front = "global_aloc(" in
  let ret_lst = [] in
  let rec iterate lst ret_lst =
    match lst with
   | a::b:: l ->
	 let fact = fact_front ^ (string_of_int a) ^ "," ^ (string_of_int (b - a)) ^ ")."	 
	 in 
	(*for debugging*)
	 P.printf "fact: %s // hex format: (%x, %d) //section: %s\n" fact a (b - a) sec;
	 iterate (b::l) (fact::ret_lst)
   | a :: l -> 
	 let fact = fact_front ^ (string_of_int a) ^ "," ^ (string_of_int (upper - a)) ^ ")."
	 in
	(*for debugging*)
	 P.printf "fact: %s // hex format: (%x, %d) //section: %s\n" fact a (upper - a) sec;
	 iterate l (fact::ret_lst) 
   | [] -> ret_lst
  in iterate lst ret_lst
**)


let global_aloc_gen lst lower upper sec =
  let lst = lower::lst in
  let ret_lst = [] in
  let rec iterate lst ret_lst =
    match lst with
   | a::b:: l ->
	 let fact = (a , b - a)	in 
	 iterate (b::l) (fact::ret_lst)
   | a :: l -> 
	 let fact = ( a, upper - a) in
	 iterate l (fact::ret_lst) 
   | [] -> ret_lst
  in iterate lst ret_lst




let gen_rtl pre ins loc bb_symbol_label =

    let ll = X86_Compile.instr_to_rtl pre ins in
	let simpri = RTL_opt.simpri_all_methods in
	let simpre = RTL_opt.simpre_all_methods in
	let new_ll = RTL_opt.simplify_rtl simpri simpre ll in
	let addr = str_of_mword_dec (MWord.of_int loc) in
	RtlCompact.rtl_instr_compact new_ll addr bb_symbol_label
    


let gen_edge_fact instr_lst =
  let fact_lst = [] in
  let rec iterate instr_lst fact_lst = match instr_lst with
 	|[] -> fact_lst
    |[x] -> fact_lst
    |x::y::xs -> let (loc1,_,_,_) = x in let (loc2,_,_,_) = y in
	let loc1 = str_of_mword_dec (MWord.of_int loc1) in let loc2 = str_of_mword_dec (MWord.of_int loc2) in

    let edge_inp = RtlCompact.gen_fact [loc1; loc2]
    in iterate (y::xs) (edge_inp::fact_lst) (**maybe stdout instead??*)
  in iterate instr_lst fact_lst


(*positive only*)
let hex_to_flex_dec hex = 
  let hex = "0x" ^ hex in
  if hex = "0xffffffff" then "0"
  else string_of_int ((int_of_string hex) - 134512640) 

let gen_succ_edge_fact last_instr succ =
  let fact_lst = [] in let (loc1,_,_,_) = last_instr in let loc1 = str_of_mword_dec (MWord.of_int loc1) in
  let rec iterate succ fact_lst = match succ with
  | [] -> fact_lst
  | x::xs -> let loc2 = hex_to_flex_dec x in
	let succ_edge_inp = RtlCompact.gen_fact [loc1; loc2] 
	in iterate xs (succ_edge_inp::fact_lst)
  in iterate succ fact_lst





