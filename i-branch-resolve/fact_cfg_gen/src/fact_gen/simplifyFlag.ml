open Batteries
open X86Syntax
open X86Semantics
open RTL
open Big
open Printer


(**********
(**for Dongrui**)

let rec prune_cast re =
  match re with
  | X86_RTL.Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    begin match bvop with
    | X86_RTL.Coq_shl_op
    | X86_RTL.Coq_shr_op
    | X86_RTL.Coq_shru_op ->
      prune_cast re1
    | X86_RTL.Coq_or_op ->
      begin match re1, re2 with
      | X86_RTL.Coq_cast_u_rtl_exp(s,_,re'), _ ->
		prune_cast re'
	  | _, _ -> X86_RTL.Coq_arith_rtl_exp(s,bvop,prune_cast re1,prune_cast re2)
      end
    | _ ->
      X86_RTL.Coq_arith_rtl_exp(s,bvop,prune_cast re1,prune_cast re2)
    end
  | X86_RTL.Coq_test_rtl_exp(s,top,re1,re2) ->
    X86_RTL.Coq_test_rtl_exp(s,top,prune_cast re1,prune_cast re2)
  | X86_RTL.Coq_if_rtl_exp(s, ge,te,ee) ->
    X86_RTL.Coq_if_rtl_exp(s, prune_cast ge, prune_cast te, prune_cast ee)
  | X86_RTL.Coq_cast_s_rtl_exp(s,_,re') ->
    prune_cast re'
  | X86_RTL.Coq_cast_u_rtl_exp(s,_,re') ->
    prune_cast re'
  | X86_RTL.Coq_imm_rtl_exp(s,_)
  | X86_RTL.Coq_get_loc_rtl_exp(s,_)
  | X86_RTL.Coq_get_ps_reg_rtl_exp(s,_)
  | X86_RTL.Coq_get_array_rtl_exp(_,s,_,_) ->
    re
  | X86_RTL.Coq_get_byte_rtl_exp(re') ->
    X86_RTL.Coq_get_byte_rtl_exp(prune_cast re')
  | X86_RTL.Coq_get_random_rtl_exp(s) ->
    re
  | _ ->
    assert false

**********)






let rec prune_cast re =
  match re with
  | X86_RTL.Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    begin match bvop with
    | X86_RTL.Coq_shl_op
    | X86_RTL.Coq_shr_op
    | X86_RTL.Coq_shru_op ->
	  begin match re2 with
	  | X86_RTL.Coq_imm_rtl_exp(s,_)
	  | X86_RTL.Coq_get_loc_rtl_exp(s,_)
	  | X86_RTL.Coq_get_ps_reg_rtl_exp(s,_)
	  | X86_RTL.Coq_get_array_rtl_exp(_,s,_,_) ->
		X86_RTL.Coq_arith_rtl_exp(s,bvop, prune_cast re1, re2)	  
	  | _ -> prune_cast re1
	  end
    | X86_RTL.Coq_or_op ->
      begin match re1, re2 with
      | X86_RTL.Coq_cast_u_rtl_exp(s,_,re'), _ ->
		begin match re2 with
		| X86_RTL.Coq_imm_rtl_exp(s,_)
		| X86_RTL.Coq_get_loc_rtl_exp(s,_)
		| X86_RTL.Coq_get_ps_reg_rtl_exp(s,_)
		| X86_RTL.Coq_get_array_rtl_exp(_,s,_,_) ->		
		  X86_RTL.Coq_arith_rtl_exp(s,bvop, re1, re2)
	    | X86_RTL.Coq_cast_s_rtl_exp(s,_,re') ->
		  X86_RTL.Coq_arith_rtl_exp(s,bvop, re1, prune_cast re')
	    | X86_RTL.Coq_cast_u_rtl_exp(s,_,re') ->
		  X86_RTL.Coq_arith_rtl_exp(s,bvop, re1, prune_cast re')

		| _ -> re1
		end
	  | _, _ -> X86_RTL.Coq_arith_rtl_exp(s,bvop,prune_cast re1,prune_cast re2)
      end
    | _ ->
      X86_RTL.Coq_arith_rtl_exp(s,bvop,prune_cast re1,prune_cast re2)
    end
  | X86_RTL.Coq_test_rtl_exp(s,top,re1,re2) ->
    X86_RTL.Coq_test_rtl_exp(s,top,prune_cast re1,prune_cast re2)
  | X86_RTL.Coq_if_rtl_exp(s, ge,te,ee) ->
    X86_RTL.Coq_if_rtl_exp(s, prune_cast ge, prune_cast te, prune_cast ee)
  | X86_RTL.Coq_cast_s_rtl_exp(s,_,re') ->
    prune_cast re'
  | X86_RTL.Coq_cast_u_rtl_exp(s,_,re') ->
    prune_cast re'
  | X86_RTL.Coq_imm_rtl_exp(s,_)
  | X86_RTL.Coq_get_loc_rtl_exp(s,_)
  | X86_RTL.Coq_get_ps_reg_rtl_exp(s,_)
  | X86_RTL.Coq_get_array_rtl_exp(_,s,_,_) ->
    re
  | X86_RTL.Coq_get_byte_rtl_exp(re') ->
    X86_RTL.Coq_get_byte_rtl_exp(prune_cast re')
  | X86_RTL.Coq_get_random_rtl_exp(s) ->
    re
  | _ ->
    assert false




let get_flag_compute rtl_e loc = 
  let loc = Printer.str_of_loc loc in
  if (loc = "CF" ||
	 loc = "OF" ||
	 loc = "ZF" ||
	 loc = "SF" ||
	 loc = "AF") then (prune_cast rtl_e)
  else rtl_e 



let simplify_flag rtl_i =
  match rtl_i with
  | X86_RTL.Coq_set_loc_rtl(s, rtl_e, loc) ->
  	let rtl_e' = get_flag_compute rtl_e loc
	in X86_RTL.Coq_set_loc_rtl(s, rtl_e', loc)
  | _ -> rtl_i

let simpFlag ll =
  (List.map (fun rtl_i -> simplify_flag rtl_i) ll )













