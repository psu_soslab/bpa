open Batteries
open BatOptParse
open Printf
module H = Hashtbl
module F = Format

open X86Syntax
open X86Semantics
open Elf
open Config

open BB
open CFGGen
open Config_cfg
open PostStat

open Bits

(** The main function *)
let theMain () = 
  let usagemsg = 
    "main.native [options] target\nrun with -h or --help to see the list of options"
  in
  let optpsr = OptParser.make ?usage:(Some usagemsg) () in
  let ls_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "use linear-sweep method") ?long_name:(Some "ls") ls_opt;

  let type_match_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "turn on type matching mechanism") ?long_name:(Some "TypeOn") type_match_opt;

  let top_trace_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "turn on top type tracing") ?long_name:(Some "trace") top_trace_opt; 

  let unsupport_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "turn on unsupport instruction report") ?long_name:(Some "unsup") unsupport_opt; 

  let target_func_opt = StdOpt.str_option () in
  OptParser.add optpsr ?help:(Some "target one single function") ?short_name:(Some 't') target_func_opt;

  let verbose_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "turn on processing report") ?short_name:(Some 'v') verbose_opt;

  let target_all_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "target all functions") ?long_name:(Some "all") target_all_opt;
  
  let aibt_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "report AIBT") ?long_name:(Some "aibt") aibt_opt;

  let stat_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "report statistics") ?long_name:(Some "stat") stat_opt;

  let icfg_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "output indirect control flows") ?long_name:(Some "icfg") icfg_opt;

  let time_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "report timing") ?long_name:(Some "time") time_opt;

  let check_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "report checking") ?long_name:(Some "check") check_opt;

  let coarseCFG_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "generate coarse-CFG only") ?long_name:(Some "coarse") coarseCFG_opt;

  let baseCFG_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "generate base-CFG only") ?long_name:(Some "base") baseCFG_opt;

  let staticlib_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "deal with statically-linked binary") ?long_name:(Some "static") staticlib_opt;

  let dfam_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "marshal coq_DFA") ?long_name:(Some "dfam") dfam_opt;

  let cfgm_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "marshal generated CFG") ?long_name:(Some "cfgm") cfgm_opt;

  let ieqcg_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "Output initial EQCG") ?long_name:(Some "ieqcg") ieqcg_opt;

  let feqcg_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "Output solved EQCG") ?long_name:(Some "feqcg") feqcg_opt;

  let argvs = OptParser.parse_argv optpsr in
  let target_path = 
    if argvs = [] then 
      raise (Failure "No target file specified!")
    else
      List.hd argvs
  in
  let ls_on = Opt.get ls_opt in
  cfg_m_on := Opt.get cfgm_opt;
  Advdis.m_on := Opt.get dfam_opt;
  let target_func_option = Opt.opt target_func_opt in
  let target_all = Opt.get target_all_opt in
  let aibt_on = Opt.get aibt_opt in
  (*let stat_on = Opt.get stat_opt in*)
  (*let icfg_on = Opt.get icfg_opt in*)
  let time_on = Opt.get time_opt in
  (*let check_on = Opt.get check_opt in*)
  Advdis.coarseCFG_on := Opt.get coarseCFG_opt;
  let baseCFG_on = Opt.get baseCFG_opt in
  Ibaux.static_lib := Opt.get staticlib_opt;
  init_eqcg := Opt.get ieqcg_opt;
  fini_eqcg := Opt.get feqcg_opt;

  let elf = read_elf_file target_path in

  (*F.printf "%a" pp_elf_file elf;*)

  let _, bbtbl = Stats.time ("fine-grained CFG generation")
    enhanced_cfggen elf
  in

  if aibt_on then bbtbl_stat_report bbtbl;

  flush stdout;

  if time_on then
    Stats.print Pervasives.stdout ("Timings:\n")
 
let _ = 
  Stats.reset Stats.SoftwareTimer;
  theMain ()
 
