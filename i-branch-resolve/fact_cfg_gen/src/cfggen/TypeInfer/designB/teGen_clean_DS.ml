open Batteries
module P = Printf
module H = Hashtbl

open BB

type tvName = string

type tvIndex = int

type super_ctype = 
| (*Top Type*) SC_top 
| (*Type Variable*) SC_typeVar of tvName * tvIndex
| (*All Variable*) SC_allTypeVar of tvIndex
| (*0*) SC_base of string (* like int, float including void *) 
| (*1*) SC_const of super_ctype
| (*2*) SC_array of super_ctype * (int list) (* int list for dimension *)
| (*3*) SC_pointer of super_ctype
| (*4*) SC_typedef of string * super_ctype (* string for typedef name *)
| (*5*) SC_subroutine of super_ctype * (super_ctype list) (* first: return type, second: parameter type list *)
| (*6*) SC_enumeration of string * ((string * int) list) (* enum_name, {enumerator_name, enumerator_value} *)
| (*7*) SC_union of string * ((super_ctype * string) list) (* union_name, {field_type, field_name} *)
| (*8*) SC_structure of string * string * ((super_ctype * string) list) (* struct_name, new_struct_name, {field_type, field_name} *)
| (*9*) SC_unspecified_param
| (*a*) SC_wrong_type of string (* including an error message *)
| (*b*) SC_volatile of super_ctype

type extended_sctype = super_ctype list
(*
| EC_top
| EC_single of super_ctype
| EC_alt of extended_sctype * extended_sctype
*)
type ctypeEquation = {index: int; equation: extended_sctype * extended_sctype}
(*{index: int; equation: super_ctype * super_ctype}*)

let mkCTEQ ndx varL varR =
  {index = ndx; equation = (varL,varR)}

module CtypeEquationOrder = struct
  type t = ctypeEquation
  let compare (cte1:ctypeEquation) (cte2:ctypeEquation) = 
    Pervasives.compare cte1.index cte2.index
end

module CtypeEqSet = Set.Make (CtypeEquationOrder)

(** SG_condition contains: an equation Set + an index record Gamma **)
type condition = CtypeEqSet.t (* * (tvName, extended_sctype) H.t*)


let rec str_of_sctype sctype =
  match sctype with
  | SC_top ->
    P.sprintf "<T>"
  | SC_typeVar(tvnm,tvndx) -> 
    P.sprintf "%s_%x" tvnm tvndx
  | SC_allTypeVar tvndx ->
    P.sprintf "all_%x" tvndx
  | SC_base str -> str (* like int, float including void *) 
  | SC_const sct -> P.sprintf "const %s" (str_of_sctype sct)
  | SC_pointer sct -> P.sprintf "%s *" (str_of_sctype sct)
  | _ ->
    "Do no matter now..."

let rec str_of_ectype ectype =
  match ectype with
  | [] -> ""
  | h :: t -> (str_of_sctype h) ^ "; " ^ (str_of_ectype t) 
(*
  match ectype with
  | EC_top -> "<T>"
  | EC_single sctype -> str_of_sctype sctype
  | EC_alt(ectype1,ectype2) ->
    P.sprintf "%s | %s" (str_of_ectype ectype1) (str_of_ectype ectype2)
*)
      
let print_ctypeEquation cteq =
  let print_equation eqn = 
    let varl, varr = eqn in
    P.printf "[%s] = [%s]\n" (str_of_ectype varl) (str_of_ectype varr)
  in
  P.printf "#%x: " cteq.index;
  print_equation cteq.equation

let print_cond cond =
  P.printf "Type Equation Set:\n";
  CtypeEqSet.iter print_ctypeEquation cond

let rec eq_sctype sctype1 sctype2 =
  match sctype1,sctype2 with
  | SC_top, SC_top -> true
  | SC_typeVar (tvnm1,tvndx1), SC_typeVar (tvnm2,tvndx2) ->
    tvnm1 = tvnm2 && tvndx1 = tvndx2
  | SC_allTypeVar tvndx1, SC_allTypeVar tvndx2 ->
    tvndx1 = tvndx2
  | SC_base name1, SC_base name2 -> name1 = name2
  | SC_const sct1, SC_const sct2 -> eq_sctype sct1 sct2
  | SC_array (sct1,dimlist1), SC_array (sct2,dimlist2) -> 
    let type_consis = eq_sctype sct1 sct2 in 
    let dim_consis = 
      if List.length dimlist1 = List.length dimlist2 then 
	List.fold_right2 
	  (fun dim1 dim2 b -> 
	    if dim1 = 0 || dim2 = 0 then b 
	    else (dim1 = dim2) && b) dimlist1 dimlist2 true
      else false
    in
    type_consis && dim_consis
  | SC_array (sct1,_), SC_pointer sct2 -> eq_sctype sct1 sct2
  | SC_pointer sct1, SC_pointer sct2 -> eq_sctype sct1 sct2
  | SC_pointer sct1, SC_array (sct2,_) -> eq_sctype sct1 sct2
  | SC_typedef (_,sct1), _ -> eq_sctype sct1 sctype2
  | _, SC_typedef (_,sct2) -> eq_sctype sctype1 sct2
  | SC_subroutine (ret1,param1), SC_subroutine (ret2,param2) ->
    let ret_consis = eq_sctype ret1 ret2 in
    let param_consis = param_equal param1 param2 in
    ret_consis && param_consis
  | SC_enumeration (enumname1,etorlist1), SC_enumeration (enumname2,etorlist2) ->
    enumname1 = enumname2
  | SC_union (unionname1,umemlist1), SC_union (unionname2,umemlist2) ->
    (** when one has name but no type and the other does not have name but have type, they may
	still be the same **)
    if unionname1 = unionname2 then true
    else if List.length umemlist1 = List.length umemlist2 then
      List.fold_right2 (fun (sct1,str1) (sct2,str2) b -> b && (eq_sctype sct1 sct2) && (str1 = str2)) umemlist1 umemlist2 true
    else false
  | SC_structure (structname1,levelname1,smemlist1), SC_structure (structname2,levelname2,smemlist2) -> 
    if structname1 = structname2 then true
    else if levelname1 = levelname2 && (List.length smemlist1) = (List.length smemlist2) then
      if (List.length smemlist1) = 0 then true
      else
	List.fold_right2 (fun (sct1,str1) (sct2,str2) b -> b && eq_sctype sct1 sct2 && str1 = str2) smemlist1 smemlist2 true
    else false
  | SC_unspecified_param, SC_unspecified_param ->  
    true
  | SC_wrong_type errmsg1, _ -> 
    let () = P.printf "In eq_sctype, the first type is wrong type: %s" errmsg1 in
    false
  | _, SC_wrong_type errmsg2 ->
    let () = P.printf "In eq_sctype, the second type is wrong type: %s" errmsg2 in
    false
  | SC_volatile ct1, SC_volatile ct2 -> eq_sctype ct1 ct2
  | _ -> false
    
and param_equal param1 param2 =
  match param1,param2 with
  | [], [] -> true
  | hd_param1 :: tl_param1, hd_param2 :: tl_param2 ->
    eq_sctype hd_param1 hd_param2 && param_equal tl_param1 tl_param2
  | _ -> false

let eq_ectype ect1 ect2 =
  if List.length ect1 <> List.length ect2 then false
  else
    List.fold_right (fun sct1 b ->
      b && List.exists (fun sct2 -> eq_sctype sct1 sct2) ect2
    ) ect1 true

let eq_equation eqn1 eqn2 =
  if eqn1.index <> eqn2.index then false
  else
    let eqn1_ect1, eqn1_ect2 = eqn1.equation in
    let eqn2_ect1, eqn2_ect2 = eqn2.equation in
    eq_ectype eqn1_ect1 eqn2_ect1 && eq_ectype eqn1_ect2 eqn2_ect2

let eq_s s1 s2 =
  CtypeEqSet.fold (fun elt1 b ->
    b && CtypeEqSet.exists (fun elt2 -> eq_equation elt1 elt2) s2
  ) s1 true

let rec elim_dup cmp al1 =
  match al1 with
  | [] -> []
  | h :: t ->
    if List.exists (cmp h) t then elim_dup cmp t
    else h :: (elim_dup cmp t)

let add_new_eq_to_s eqn1 s =
  let outs = 
    if not (CtypeEqSet.exists (eq_equation eqn1) s) then (
      let map_success = ref false in
      let mapped_s = 
	CtypeEqSet.map (fun eqn2 ->
	  let eqn1_ect1, eqn1_ect2 = eqn1.equation in
	  let eqn2_ect1, eqn2_ect2 = eqn2.equation in
	  if eq_ectype eqn1_ect1 eqn2_ect1 then (
	    map_success := true;
	    {eqn2 with equation = (eqn2_ect1, (elim_dup eq_sctype (eqn1_ect2 @ eqn2_ect2)))}
     	  ) else eqn2
	) s
      in
      if !map_success then mapped_s
      else CtypeEqSet.add eqn1 s
    ) else s
  in
  outs
  
let union_s s1 s2 =
  CtypeEqSet.fold (fun eqn1 s ->
    add_new_eq_to_s eqn1 s
  ) s1 s2

type tyinfbb = {
  mutable ti_bb: basicblock;
  mutable ti_inter_preds: string list;
  mutable ti_eqset: CtypeEqSet.t;
}
  


let mkTIBB bb inter_preds eqset = 
  {ti_bb=bb;ti_inter_preds=inter_preds;ti_eqset=eqset}

let dump_tibb tibb =
  P.printf "BB:%s\n" tibb.ti_bb.bb_label;
  P.printf "Equation set:\n";
  print_cond tibb.ti_eqset;
  flush stdout
