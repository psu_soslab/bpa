open Batteries
module P = Printf
module H = Hashtbl

open X86Syntax
open X86Semantics
open X86_MACHINE
open X86_RTL
open Config
open Abbrev

open TeGen_DS
open BB
open RTL_opt
open Printer
open Config_cfg

let is_trivial ins =
  match ins with
  | RET(_,_) | IRET | JMP(_,_,_,_) | Jcc(_,_) | JCXZ(_) -> true
  | CMP _ | TEST _ | ADD _ | SUB _ -> false
  | _ -> false

let split_at_eq re =
  match re with
  | Coq_test_rtl_exp(s,Coq_eq_op,re1,re2) ->
    begin match re1 with
    | Coq_get_loc_rtl_exp(s,Coq_reg_loc reg) ->
      reg, re2
    | _ ->
      raise (Failure "Should be no other expressions other than get_loc")
    end
  | _ ->
    raise (Failure "Should be no other expressions other than test")

let reverse_cfa_expr re =
  let cfa_ps_reg = Coq_get_ps_reg_rtl_exp(Big.of_int 31, Big.of_int (-1)) in
  match re with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    begin match re1 with
    | Coq_get_loc_rtl_exp(s, Coq_reg_loc reg) ->
      begin match bvop with
      | Coq_add_op ->
       	reg, Coq_arith_rtl_exp(s,Coq_sub_op,cfa_ps_reg,re2)
      | Coq_sub_op ->
	reg, Coq_arith_rtl_exp(s,Coq_add_op,cfa_ps_reg,re2)
      | _ ->
	raise (Failure "Should be no other operators")
      end
    | _ -> 
      raise (Failure "Should not exist things other than registers")
    end
  | Coq_get_loc_rtl_exp(s,Coq_reg_loc reg) ->
    reg, cfa_ps_reg
  | _ ->
    P.printf "%s\n" (str_of_rtl_exp_less_size re); flush stdout;
    raise (Failure "Should not exist expressions other than arith")

let tvNameGen_naive funCFA op l = 
  let str_of_scale scl =
    match scl with
    | Scale1 -> 1
    | Scale2 -> 2
    | Scale4 -> 4
    | Scale8 -> 8
  in
  let rtl_add_big re bigv =
    simp_re simpre_all_methods (Coq_arith_rtl_exp(Big.of_int 31, Coq_add_op, re, Coq_imm_rtl_exp(Big.of_int 31, bigv)))
  in
  match op with
  | Imm_op imm -> 
    P.sprintf "$0x%s" (str_of_mword_flex (MWord.of_big_int (unsigned32 imm)))
  | Reg_op reg ->
    str_of_reg reg
  | Address_op addr -> 
    begin match addr with
    | {addrDisp = big_v; addrBase = None; addrIndex = None} ->
      P.sprintf "(%s)" (str_of_mword_flex (MWord.of_big_int big_v))
    | {addrDisp = big_v; addrBase = Some reg; addrIndex = None} ->
      let _, l_cfa =
	try
	  List.find (fun (lopc,cfaexp) ->
	    lopc = l
	  ) funCFA
	with Not_found ->
	  P.printf "not found cfaexp at %x\n" l; flush stdout;
	  List.last funCFA
	  (*raise (Failure "in tvNameGen_naive")*)
      in
      let cfaoff = List.map (fun (lvl,re) ->
	if lvl > 0 then split_at_eq re
	else 
	  reverse_cfa_expr re
      ) l_cfa 
      in
      begin try 
	let _, reg_in_cfa = List.find (fun (register, re) ->
	  register = reg
	) cfaoff
	in
	let op_cfa_expr = rtl_add_big reg_in_cfa big_v in
	str_of_rtl_exp_less_size op_cfa_expr
      with Not_found ->
	P.sprintf "%d(%s_%x)" 
	  (*(Util.int_of_int32 (Util.int32_of_int64 (MWord.of_big_int big_v)))*)
	  (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int (big_v))))
	  (str_of_reg reg)
	  l
      end
    | {addrDisp = big_v; addrBase = None ; addrIndex = Some (scl,reg)} ->
      P.sprintf "%s(,%s_%x,%d)"
	(str_of_mword_flex (MWord.of_big_int ( big_v)))
	(str_of_reg reg)
	l
	(str_of_scale scl)
    | {addrDisp = big_v; addrBase = Some regbase ; addrIndex = Some (scl,regindex)} ->
      P.sprintf "%s(%s_%x,%s_%x,%d)"
	(str_of_mword_flex (MWord.of_big_int ( big_v)))
	(str_of_reg regbase)
	l
	(str_of_reg regindex)
	l
	(str_of_scale scl)
    end
  | Offset_op off ->
    P.sprintf "(%s)" (str_of_mword_flex (MWord.of_big_int (off)))
  
let tvNameGen funCFA op l =
  let rtl_add_big re bigv =
    simp_re simpre_all_methods (Coq_arith_rtl_exp(Big.of_int 31, Coq_add_op, re, Coq_imm_rtl_exp(Big.of_int 31, bigv)))
  in
  match op with
  | Imm_op imm -> TV_imm (str_of_mword_flex (MWord.of_big_int imm))
  | Reg_op reg -> TV_reg reg
  | Address_op addr ->
    let addr_name = {disp=str_of_mword_flex (MWord.of_big_int (addrDisp addr));base=addrBase addr;index=addrIndex addr} 
    in 
    let transform_cfa_expr re =
      match re with
      | Coq_arith_rtl_exp(_,bvop,re1,re2) ->
	begin match re1 with
	| Coq_get_ps_reg_rtl_exp(_,ps_reg) when Big.to_int ps_reg = (-1) ->
	  begin match bvop with
	  | Coq_add_op ->
	    begin match re2 with
	    | Coq_imm_rtl_exp (_,v) -> 
	      TV_cfa (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int v)))
	    | _ -> 
	      TV_addr addr_name
	    end
	  | Coq_sub_op ->
	    begin match re2 with
	    | Coq_imm_rtl_exp (_,v) -> 
	      TV_cfa (-(Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int v))))
	    | _ -> 
	      TV_addr addr_name
	    end
	  | _ -> TV_addr addr_name
	  end
	| _ -> TV_addr addr_name
	end
      | Coq_get_ps_reg_rtl_exp(_,ps_reg) when Big.to_int ps_reg = (-1) ->
	TV_cfa 0
      | _ -> TV_addr addr_name
    in
    begin match addr with
    | {addrDisp = big_v; addrBase = Some reg; addrIndex = None} ->
      let _, l_cfa =
	try
	  List.find (fun (lopc,cfaexp) ->
	    lopc = l
	  ) funCFA
	with Not_found ->
	  P.printf "not found cfaexp at %x\n" l; flush stdout;
	  List.last funCFA
	  (*raise (Failure "in tvNameGen_naive")*)
      in
      let cfaoff = List.map (fun (lvl,re) ->
	if lvl > 0 then split_at_eq re
	else 
	  reverse_cfa_expr re
      ) l_cfa 
      in
      begin 
	try 
	  let _, reg_in_cfa = List.find (fun (register, re) ->
	    register = reg
	  ) cfaoff
	  in
	  let op_cfa_expr = rtl_add_big reg_in_cfa big_v in
	  transform_cfa_expr op_cfa_expr
	with Not_found ->
	  TV_addr addr_name 
      end
    | _ ->
      TV_addr addr_name
    end
  | Offset_op off ->
    TV_addr {disp = str_of_mword_flex (MWord.of_big_int off); base = None; index = None}

let tvGen funCFA op l tvNameGen =
  let varnm = tvNameGen funCFA op l in
  [SC_typeVar (varnm,l)]
    
let gamma_update_all inGamma new_index =
  H.filter_map_inplace (fun tvnm ectype ->
    Some [SC_typeVar (tvnm,new_index)]
  ) inGamma;
  inGamma

let reg_without_esp_ebp =
  ["EAX";"ECX";"EDX";"EBX";"ESI";"EDI"]

let gamma_update_varlist inGamma varlist new_index =
  H.filter_map_inplace (fun tvnm ectype ->
    if List.mem (str_of_tvnm tvnm) varlist then
      Some [SC_typeVar (tvnm,new_index)]
    else
      Some ectype
  ) inGamma;
  inGamma

let instr_type_infer funCFA preSGcond l_instr tvNameGen track_worth =
  let l, (pre,ins,len) = l_instr in
  let inSet,inGamma = preSGcond in

  let add_dummy_eqs tvnm_changed set =
    let dummy_eqs = List.filter_map (fun tvnm ->
      if not (List.mem tvnm tvnm_changed) then
	let varl = [SC_typeVar (tvnm,(l+len))] in
	let varr = [SC_typeVar (tvnm,l)] in
	let dummy_eq = mkCTEQ l varl varr true in
	Some dummy_eq
      else None
    ) track_worth
    in
    List.fold_right add_new_eq_to_s dummy_eqs set
  in

  let rule_push width op =
    let tvNameOp = tvNameGen funCFA op l in
    let currstack = Address_op 
      {addrDisp=Big.zero; addrBase=Some ESP; addrIndex=None;}
    in
    let tvNameCurrStack = tvNameGen funCFA currstack (l+len) in
    let varOp = 
      try 
	H.find inGamma tvNameOp
      with Not_found ->
	[SC_typeVar (tvNameOp,l)]
    in
    let varCurrStack = [SC_typeVar (tvNameCurrStack,(l+len))] in
    let new_eq = mkCTEQ l varCurrStack varOp false in
    let outSet = add_new_eq_to_s new_eq inSet in
    let outSet = add_dummy_eqs [tvNameCurrStack] outSet in 
    H.replace inGamma tvNameCurrStack varCurrStack;
    let outGamma = inGamma in
    outSet,outGamma
  in

  let rule_mov width ops opd =
    let tvNameOps = tvNameGen funCFA ops l in
    let tvNameOpd = tvNameGen funCFA opd (l+len) in
    let varOps = 
      try 
	H.find inGamma tvNameOps 
      with Not_found ->
	[SC_typeVar (tvNameOps,l)]
    in
    let varOpd = [SC_typeVar (tvNameOpd,(l+len))] in
    let new_eq = mkCTEQ l varOpd varOps false in
    let outSet = add_new_eq_to_s new_eq inSet in
    let outSet = add_dummy_eqs [tvNameOpd] outSet in 
    H.replace inGamma tvNameOpd varOpd;
    let outGamma = inGamma in
    outSet, outGamma
  in

  let rule_pop op =
    let tvNameOp = tvNameGen funCFA op (l+len) in
    let currstack = Address_op 
      {addrDisp=Big.zero; addrBase=Some ESP; addrIndex=None;}
    in
    let tvNameCurrStack = tvNameGen funCFA currstack l in
    let varCurrStack = 
      try 
	H.find inGamma tvNameCurrStack
      with Not_found ->
	[SC_typeVar (tvNameCurrStack,l)]
    in
    let varOp = [SC_typeVar (tvNameOp,(l+len))] in
    let new_eq = mkCTEQ l varOp varCurrStack false in
    let outSet = add_new_eq_to_s new_eq inSet in
    let outSet = add_dummy_eqs [tvNameOp] outSet in 
    H.replace inGamma tvNameOp varOp;
    let outGamma = inGamma in
    outSet,outGamma
  in
  
  let rule_leave () =
    let ops = Reg_op EBP in
    let opd = Reg_op ESP in
    let tvNameOps = tvNameGen funCFA ops l in
    let tvNameOpd = tvNameGen funCFA opd (l+len) in
    let varOps = 
      try 
	H.find inGamma tvNameOps 
      with Not_found ->
	[SC_typeVar (tvNameOps,l)]
    in
    let varOpd = [SC_typeVar (tvNameOpd,(l+len))] in
    let new_eq = mkCTEQ l varOpd varOps false in
    let outSet' = add_new_eq_to_s new_eq inSet in
    let outSet' = add_dummy_eqs [tvNameOpd] outSet' in 
    H.replace inGamma tvNameOpd varOpd;

    let op = ops in
    let tvNameOp = tvNameGen funCFA op (l+len) in
    let currstack = Address_op 
      {addrDisp=Big.of_int (-4); addrBase=Some ESP; addrIndex=None;}
    in
    let tvNameCurrStack = tvNameGen funCFA currstack (l+len) in
    let varCurrStack = 
      try 
	H.find inGamma tvNameCurrStack
      with Not_found ->
	[SC_typeVar (tvNameCurrStack,l)]
    in
    let varOp = [SC_typeVar (tvNameOp,(l+len))] in
    let new_eq = mkCTEQ l varOp varCurrStack false in
    let outSet = add_new_eq_to_s new_eq outSet' in
    let outSet = add_dummy_eqs [tvNameOp] outSet in 
    H.replace inGamma tvNameOp varOp;
    let outGamma = inGamma in
    outSet,outGamma
  in

  let rule_add width ops opd =
    let tvNameOpd = tvNameGen funCFA opd (l+len) in
    let varOpdr = 
      try 
	H.find inGamma tvNameOpd
      with Not_found ->
	[SC_typeVar (tvNameOpd,l)]
    in
    let varOpdl = [SC_typeVar (tvNameOpd,(l+len))] in
    let new_eq = mkCTEQ l varOpdl varOpdr true in
    let outSet = add_new_eq_to_s new_eq inSet in
    let outSet = add_dummy_eqs [tvNameOpd] outSet in 
    H.replace inGamma tvNameOpd varOpdl;
    let outGamma = inGamma in
    outSet, outGamma
  in    

  let rule_sub width ops opd =
    rule_add width ops opd
  in
  
  let rule_lea ops opd =
    let tvNameOps = tvNameGen funCFA ops l in
    let tvNameOpd = tvNameGen funCFA opd (l+len) in
    let varOps = 
      try 
	H.find inGamma tvNameOps 
      with Not_found ->
	[SC_typeVar (tvNameOps,l)]
    in
    let varOps_ptr =
      List.map (fun sctype -> SC_pointer sctype) varOps
    in
    let varOpd = [SC_typeVar (tvNameOpd,(l+len))] in
    let new_eq = mkCTEQ l varOpd varOps_ptr false in
    let outSet = add_new_eq_to_s new_eq inSet in
    let outSet = add_dummy_eqs [tvNameOpd] outSet in 
    H.replace inGamma tvNameOpd varOpd;
    let outGamma = inGamma in
    outSet, outGamma
  in    

  let rule_cmp width ops1 ops2 =
    let tvNameOps1 = tvNameGen funCFA ops1 l in
    let tvNameOps2 = tvNameGen funCFA ops2 l in
    let varOps1 = 
      try 
	H.find inGamma tvNameOps1
      with Not_found ->
	[SC_typeVar (tvNameOps1,l)]
    in
    let varOps2 = 
      try
	H.find inGamma tvNameOps2
      with Not_found ->
	[SC_typeVar (tvNameOps2,l)]    
    in
    let new_eq = mkCTEQ l varOps1 varOps2 true in
    let outSet = add_new_eq_to_s new_eq inSet in
    let outSet = add_dummy_eqs [] outSet in 
    let outGamma = inGamma in
    outSet, outGamma
  in   

  let rule_call dis abs op slctr =
    let _, l_cfa = 
      try 
	List.find (fun (lopc,cfaexp) ->
	  lopc = l
	) funCFA
      with Not_found ->
	P.printf "not found cfaexp at %x\n" l; flush stdout;
	raise (Failure "in rule_call")
    in
    let cfaoff = List.map (fun (lvl,re) ->
      if lvl > 0 then split_at_eq re
      else 
	reverse_cfa_expr re
    ) l_cfa 
    in
    if List.exists (fun (reg,exp) -> reg = EBP) cfaoff then
      inSet, gamma_update_varlist inGamma reg_without_esp_ebp (l+len)
    else
      inSet, gamma_update_varlist inGamma ("EBP" :: reg_without_esp_ebp) (l+len)
  in

  let rule_test width ops1 ops2 =
    rule_cmp width ops1 ops2
  in

  if is_trivial ins then preSGcond
  else begin match ins with
  | PUSH(width,op) -> rule_push width op
  | MOV(width,opd,ops) -> rule_mov width ops opd
  | POP(op) -> rule_pop op
  | LEAVE -> rule_leave ()
  | ADD(width,opd,ops) -> rule_add width ops opd
  | SUB(width,opd,ops) -> rule_sub width ops opd
  | LEA(opd,ops) -> rule_lea ops opd
  | CMP(width,ops1,ops2) -> rule_cmp width ops1 ops2
  | CALL(dis,abs,op,slctr) -> rule_call dis abs op slctr
  | TEST(width,ops1,ops2) -> rule_test width ops1 ops2
  | _ -> 
    inSet, inGamma (*gamma_update_all inGamma (l+len)*)
  end

let bb_type_infer funCFA preSGcond bb tvNameGen track_worth =
  let s,g = preSGcond in
  let postg = H.copy g in
  let preSGcond = s,postg in
  let addrcount = ref bb.bb_relAddr in
  let l_instr_list = 
    List.map (fun (pre,ins,len) -> 
      let outaddr = !addrcount in
      addrcount := len + !addrcount;
      outaddr, (pre,ins,len)
    ) bb.bb_instrs 
  in
  List.fold_left (fun pre l_instr -> 
    try
      instr_type_infer funCFA pre l_instr tvNameGen track_worth
    with Not_found ->
      P.printf "in bb %s\n" bb.bb_label; flush stdout;
      raise (Failure "not found inside instr_type_infer")
  ) preSGcond l_instr_list

exception Equation_Conflict
exception Gamma_Conflict

let worth_tracking funCFA asm_fun tvNameGen =
  elim_dup (fun e1 e2 -> e1 = e2) (
    List.flatten (List.map (fun bb ->  
      let addrcount = ref bb.bb_relAddr in
      let l_instr_list = 
	List.map (fun (pre,ins,len) -> 
	  let outaddr = !addrcount in
	  addrcount := len + !addrcount;
	  outaddr, (pre,ins,len)
	) bb.bb_instrs 
      in
      List.flatten (List.map (fun (l, (pre,ins,len)) ->
	if is_trivial ins then []
	else match ins with
	| PUSH(width,op) ->
	  let tvNameOp = tvNameGen funCFA op l in
	  let currstack = Address_op 
	    {addrDisp=Big.zero; addrBase=Some ESP; addrIndex=None;}
	  in
	  let tvNameCurrStack = tvNameGen funCFA currstack (l+len) in
	  [tvNameOp;tvNameCurrStack]
	| MOV(width,opd,ops) -> 
	  let tvNameOpd = tvNameGen funCFA opd l in
	  let tvNameOps = tvNameGen funCFA ops l in
	  [tvNameOpd;tvNameOps]
	| POP(op) -> 
	  let tvNameOp = tvNameGen funCFA op l in
	  let currstack = Address_op 
	    {addrDisp=Big.zero; addrBase=Some ESP; addrIndex=None;}
	  in
	  let tvNameCurrStack = tvNameGen funCFA currstack (l+len) in
	  [tvNameOp;tvNameCurrStack]
	| LEAVE -> 
	  let ops = Reg_op EBP in
	  let opd = Reg_op ESP in
	  let tvNameOps = tvNameGen funCFA ops l in
	  let tvNameOpd = tvNameGen funCFA opd (l+len) in
	  (*let op = ops in
	  let tvNameOp = tvNameGen funCFA op (l+len) in*)
	  let currstack = Address_op 
	    {addrDisp=Big.of_int (-4); addrBase=Some ESP; addrIndex=None;}
	  in
	  let tvNameCurrStack = tvNameGen funCFA currstack (l+len) in
	  [tvNameOps;tvNameOpd;tvNameCurrStack]
	| ADD(width,opd,ops) -> 
	  let tvNameOpd = tvNameGen funCFA opd l in
	  [tvNameOpd]
	| SUB(width,opd,ops) -> 
	  let tvNameOpd = tvNameGen funCFA opd l in
	  [tvNameOpd]
	| LEA(opd,ops) -> 
	  let tvNameOpd = tvNameGen funCFA opd l in
	  let tvNameOps = tvNameGen funCFA ops l in
	  [tvNameOpd;tvNameOps]
	| CMP(width,ops1,ops2) -> 
	  let tvNameOps1 = tvNameGen funCFA ops1 l in
	  let tvNameOps2 = tvNameGen funCFA ops2 l in
	  [tvNameOps1;tvNameOps2]
	| CALL(dis,abs,op,slctr) ->
	  let tvNameOp = tvNameGen funCFA op l in
	  [tvNameOp]
	| TEST(width,ops1,ops2) ->
	  let tvNameOps1 = tvNameGen funCFA ops1 l in
	  let tvNameOps2 = tvNameGen funCFA ops2 l in
	  [tvNameOps1;tvNameOps2]
	| _ -> []
      ) l_instr_list)
    ) asm_fun.func_bbs)
  )

(** func_type_infer: cfa_repr -> sg_condition -> func -> (...) -> CtypeEqSet.t **)
let func_type_infer funCFA preSGcond asm_fun tvNameGen =
  let track_worth = (* list of interesting names *)
    worth_tracking funCFA asm_fun tvNameGen 
  in 

  let workq = Queue.create () in
  
  let entry_bb_label = (List.hd asm_fun.func_bbs).bb_label in
  
  Queue.add (preSGcond, entry_bb_label) workq;

  let add_new_task task_tibb post =
    if not (does_bb_ret task_tibb.ti_bb) then (
      List.iter (fun bb_label ->
	if List.exists (fun func_bb -> 
	  func_bb.bb_label = bb_label
	) asm_fun.func_bbs && bb_label <> asm_fun.func_low then (
	  (*P.printf "add_new_task %s\n" bb_label; flush stdout;*)
	  Queue.add (post, bb_label) workq
	)
      ) (task_tibb.ti_bb.bb_succs @ task_tibb.ti_bb.bb_inter_succs)
    ) 
  in

  let tibbl = ref [] in
  
  while (not (Queue.is_empty workq)) do
    (*P.printf "Stack depth:%d\n" (Stack.length workq); flush stdout;*)
    let task_pre, task_bb_label = Queue.take workq in
    try
      let task_tibb = 
	List.find (fun tibb ->
	  tibb.ti_bb.bb_label = task_bb_label
	) !tibbl 
      in
      if Option.is_none task_tibb.ti_pre then (
	P.printf "not handled properly\n"; flush stdout;
	task_tibb.ti_pre <- Some task_pre;
	let post = bb_type_infer funCFA task_pre task_tibb.ti_bb tvNameGen track_worth in
	task_tibb.ti_post <- Some post;
	add_new_task task_tibb post
      ) else (
	let old_pre = Option.get task_tibb.ti_pre in
	let union_pre = union_sg old_pre task_pre in
	if not (eq_sgcond old_pre union_pre) then (
	  (*P.printf "old_pre and task_pre are different at %s\n" task_bb_label; 
	  flush stdout;*)
	  (*P.printf "OLD:\n";
	  H.iter print_gamma_record (snd old_pre);
	  P.printf "\nTASK:\n";
	  H.iter print_gamma_record (snd task_pre);
	  P.printf "\n"; flush stdout;*)
	  (*let union_pre = union_sg old_pre task_pre in*)
	  task_tibb.ti_pre <- Some union_pre;
	  let post = bb_type_infer funCFA union_pre task_tibb.ti_bb tvNameGen track_worth in
	  task_tibb.ti_post <- Some post;
	  add_new_task task_tibb post
	)
      )
    with Not_found ->
      try
	let task_bb = List.find (fun func_bb -> 
	  func_bb.bb_label = task_bb_label
	) asm_fun.func_bbs
	in
	let task_tibb = mkTIBB (Some task_pre) task_bb None in
	let post = 
	  bb_type_infer funCFA task_pre task_bb tvNameGen track_worth in
	task_tibb.ti_post <- Some post;
	tibbl := task_tibb :: !tibbl;
	add_new_task task_tibb post
      with Not_found ->
        P.printf "missing basic block: %s\n" task_bb_label; flush stdout;
	raise (Failure "basic block label not found in function")
  done;
  let sorted_tibbl =
    List.sort (fun tibb1 tibb2 ->
      tibb1.ti_bb.bb_relAddr - tibb2.ti_bb.bb_relAddr
    ) !tibbl
  in
  let func_eqns = 
    List.fold_right (fun tibb set ->
      try
	let s,g = Option.get tibb.ti_post in
	union_s s set
      with _ -> (* ti_post is NONE *)
	set
    ) sorted_tibbl CtypeEqSet.empty
  in
  (*CtypeEqSet.iter print_ctypeEquation func_eqns;*)
  (*
  P.printf "%d\n\n" (Util.int_of_int32 (Util.int32_of_int64 (MWord.of_string "0xffffffff"))); flush stdout;*)
  (*List.iter dump_tibb sorted_tibbl; flush stdout;*)
  func_eqns

let eqc_graph_of teSet =
  let moSet, biSet = CtypeEqSet.partition (fun eq ->
    not eq.bi_direct
  ) teSet
  in

  let no_top scvar_list =
    List.filter (fun scvar ->
      scvar <> SC_top
    ) scvar_list
  in

  let mo_base_eqcg = List.flatten (
    List.map (fun eq ->
      let varl,varr = eq.equation in
      List.filter_map (fun scvar ->
	if scvar <> SC_top then
	  Some (mkEQCNode [scvar])
	else
	  None
      ) (varl @ varr)
    ) (CtypeEqSet.elements moSet)
  )
  in
  let bi_base_eqcg = List.map (fun eq ->
    let varl,varr = eq.equation in
    mkEQCNode (no_top (varl @ varr))
  ) (CtypeEqSet.elements biSet)
  in
  let base_eqcg = mo_base_eqcg @ bi_base_eqcg in
  let rec merge_eqcg eqcg =
    match eqcg with
    | [] -> []
    | eqch :: eqct ->
      try
	let eqc_in_t = 
	  List.find (fun eqc -> 
	    not (TypeVarSet.is_empty (TypeVarSet.inter eqch.eqc_vars eqc.eqc_vars))
	  ) eqct
	in
	eqc_in_t.eqc_vars <- (TypeVarSet.union eqch.eqc_vars eqc_in_t.eqc_vars);
	merge_eqcg eqct
      with Not_found ->
	eqch :: (merge_eqcg eqct)
  in
  let base_eqcg = ref (merge_eqcg base_eqcg) in
  CtypeEqSet.iter (fun eq ->
    let varl, varr = eq.equation in
    if List.length varl <> 1 then 
      raise (Failure "We have multi-destination...")
    else
      let varl_eqc = 
	try
	  List.find (fun eqc ->
	    TypeVarSet.mem (List.hd varl) eqc.eqc_vars
	  ) !base_eqcg
	with Not_found ->
	  let new_node = mkEQCNode varl in
	  base_eqcg := new_node :: !base_eqcg;
	  new_node
      in
      List.iter (fun sctype -> 
	let sctype_eqc = 
	  try 
	    List.find (fun eqc ->
	      TypeVarSet.mem sctype eqc.eqc_vars
	    ) !base_eqcg
	  with Not_found ->
	    let new_node = mkEQCNode [sctype] in
	    base_eqcg := new_node :: !base_eqcg;
	    new_node
	in
	sctype_eqc.eqc_succs <- varl_eqc :: sctype_eqc.eqc_succs;
	varl_eqc.eqc_preds <- sctype_eqc :: varl_eqc.eqc_preds
      ) varr
  ) moSet;
  let eqcg = !base_eqcg in
  (*List.iter dump_eqcnode eqcg;*)
  eqcg

let get_var_name var =
  match var with
  | SC_typeVar (tvnm,_) -> tvnm
  | _ -> 
    P.printf "%s is not a simple type variable\n" (str_of_sctype var); flush stdout;
    raise (Failure "Complex type variable")

let get_var_index var =
  match var with
  | SC_typeVar (_,tvndx) -> tvndx
  | _ -> 
    P.printf "%s is not a simple type variable\n" (str_of_sctype var); flush stdout;
    raise (Failure "Complex type variable")

let assign_typeinfo dbgTypeInfo eqcg =
  let rec find_typeinfo var = 
    match var with
    (*| SC_top -> [SC_top]*)
    | SC_typeVar (nm,ndx) -> begin 
      try
	let _, _, ect = 
	  List.find (fun (tvnm,rng,ectype) ->
	    tvnm = nm && in_range ndx rng
	  ) dbgTypeInfo
	in
	ect
      with Not_found -> [] 
    end
    | SC_pointer(sctype) -> 
      List.map (fun sct -> 
	SC_pointer sct
      ) (find_typeinfo sctype)
    | _ -> 
      P.printf "%s is not a simple type variable\n" (str_of_sctype var); flush stdout;
      [] (** TODO: add other super_ctype **)
  in
  
  List.iter (fun eqc ->
    let vars = eqc.eqc_vars in
    TypeVarSet.iter (fun var ->
      let ect = find_typeinfo var in
      let new_typeinfo =
	elim_dup eq_sctype (ect @ eqc.eqc_typeinfo)
      in
      eqc.eqc_typeinfo <- new_typeinfo
    ) vars
  ) eqcg;
  eqcg

(** direction (true) follows successors; 
    direction (false) follows predecessors **)
let rec collect_reachable_eqcs direction start_eqc reachable_eqcs = 
  let next eqc = 
    if direction then start_eqc.eqc_succs else start_eqc.eqc_preds
  in
  if List.mem start_eqc reachable_eqcs then reachable_eqcs
  else (
    let ref_reachable_eqcs = ref (start_eqc :: reachable_eqcs) in
    List.iter (fun eqc ->
      ref_reachable_eqcs := collect_reachable_eqcs direction eqc !ref_reachable_eqcs
    ) (next start_eqc);
    !ref_reachable_eqcs
  )
      
let type_solve_single dbgTypeInfo teSet (target:super_ctype) =
  let eqcg = eqc_graph_of teSet in
  let typed_eqcg = assign_typeinfo dbgTypeInfo eqcg in
  try
    let start_eqc = 
      List.find (fun eqc ->
	TypeVarSet.mem target eqc.eqc_vars
      ) typed_eqcg
    in
    let reachable_eqcs = 
      collect_reachable_eqcs false start_eqc []
    in
    (*List.iter dump_eqcnode reachable_eqcs*)
    let reachable_typeinfo =
      elim_dup eq_sctype (
	List.flatten (
	  List.map (fun eqc ->
	    eqc.eqc_typeinfo
	  ) reachable_eqcs
	)
      )
    in
    let solution = [mkTR target reachable_typeinfo] in
    solution
  with Not_found -> 
    P.printf "Not in current teSet\n";flush stdout;
    []
(*    raise (Failure "Target is not within current EQC-graph")*)


let type_solve dbgTypeInfo teSet = 
  let eqcg = eqc_graph_of teSet in
  let typed_eqcg = assign_typeinfo dbgTypeInfo eqcg in
  let start_eqcs = List.filter (fun eqc ->
    not (List.is_empty eqc.eqc_typeinfo)
  ) typed_eqcg
  in
  let solution = ref [] in
  List.iter (fun start_eqc ->
    let reachable_eqcs =
      collect_reachable_eqcs true start_eqc []
    in
    let reachable_vars = List.flatten (
      List.map (fun eqc ->
	TypeVarSet.elements eqc.eqc_vars
      ) reachable_eqcs
    )
    in
    List.iter (fun var ->
      try
	let trvar = List.find (fun tr ->
	  eq_sctype tr.tr_var var
	) !solution
	in
	let new_val = elim_dup eq_sctype (start_eqc.eqc_typeinfo @ trvar.tr_val) in
	trvar.tr_val <- new_val
      with Not_found ->
	let new_val = start_eqc.eqc_typeinfo in
	let trvar = mkTR var new_val in
	solution := trvar :: !solution
    ) reachable_vars
  ) start_eqcs;
  List.sort Pervasives.compare !solution
