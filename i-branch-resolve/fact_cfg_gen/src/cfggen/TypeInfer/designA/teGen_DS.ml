open Batteries
module P = Printf
module H = Hashtbl

open Bits
open X86Syntax
open X86Semantics
open X86_MACHINE
open Config
open Abbrev
open Elf

open BB
open Ibaux
open Config_cfg
open Printer

type tvIndex = int
  
type addrName = {disp:string;base:register option;index:(scale * register) option}

let mkADNM indisp inbase inindex =
  {disp = indisp; base = inbase; index = inindex}

type tvName = 
| TV_imm of string
| TV_reg of register
| TV_cfa of int
| TV_addr of addrName    

type super_ctype = 
| (*Top Type*) SC_top 
| (*Type Variable*) SC_typeVar of tvName * tvIndex
| (*0*) SC_base of string (* like int, float including void *) 
| (*1*) SC_const of super_ctype
| (*2*) SC_array of super_ctype * (int list) (* int list for dimension *)
| (*3*) SC_pointer of super_ctype
| (*4*) SC_typedef of string * super_ctype (* string for typedef name *)
| (*5*) SC_subroutine of super_ctype * (super_ctype list) (* first: return type, second: parameter type list *)
| (*6*) SC_enumeration of string * ((string * int) list) (* enum_name, {enumerator_name, enumerator_value} *)
| (*7*) SC_union of string * ((super_ctype * string) list) (* union_name, {field_type, field_name} *)
| (*8*) SC_structure of string * string * ((super_ctype * string) list) (* struct_name, new_struct_name, {field_type, field_name} *)
| (*9*) SC_unspecified_param
| (*a*) SC_wrong_type of string (* including an error message *)
| (*b*) SC_volatile of super_ctype

type extended_sctype = super_ctype list
(*
| EC_top
| EC_single of super_ctype
| EC_alt of extended_sctype * extended_sctype
*)
type ctypeEquation = 
  {
    index: int; 
    equation: extended_sctype * extended_sctype; 
    bi_direct: bool;
  }
(*{index: int; equation: super_ctype * super_ctype}*)

let mkCTEQ ndx varL varR bi =
  {index = ndx; equation = (varL,varR); bi_direct = bi;}

module CtypeEquationOrder = struct
  type t = ctypeEquation
  let compare (cte1:ctypeEquation) (cte2:ctypeEquation) = 
    Pervasives.compare cte1 cte2
end

module CtypeEqSet = Set.Make (CtypeEquationOrder)

(** SG_condition contains: an equation Set + an index record Gamma **)
type sg_condition = CtypeEqSet.t * (tvName, extended_sctype) H.t


let str_of_scale scl =
  match scl with
  | Scale1 -> 1
  | Scale2 -> 2
  | Scale4 -> 4
  | Scale8 -> 8

let str_of_tvnm tvnm =
  match tvnm with
  | TV_imm imm -> 
    P.sprintf "$0x%s" imm
  | TV_reg reg ->
    str_of_reg reg
  | TV_cfa off ->
    P.sprintf "%d<CFA>" off
  | TV_addr addr ->
    begin match addr with
    | {disp = strv; base = None; index = None} ->
      P.sprintf "(%s)" strv
    | {disp = strv; base = Some reg; index = None} ->
      P.sprintf "%s(%s)"
	strv
	(str_of_reg reg)
    | {disp = strv; base = None ; index = Some (scl,reg)} ->
      P.sprintf "%s(,%s,%d)"
	strv
	(str_of_reg reg)
	(str_of_scale scl)
    | {disp = strv; base = Some regbase ; index = Some (scl,regindex)} ->
      P.sprintf "%s(%s,%s,%d)"
	strv
	(str_of_reg regbase)
	(str_of_reg regindex)
	(str_of_scale scl)
    end
      
let print_tvname tvnm =
  P.printf "%s" (str_of_tvnm tvnm)

let rec str_of_sctype sctype =
  match sctype with
  | SC_top ->
    P.sprintf "<T>"
  | SC_typeVar(tvnm,tvndx) -> 
    P.sprintf "%s_%x" (str_of_tvnm tvnm) tvndx
  | SC_base str -> str (* like int, float including void *) 
  | SC_const sct -> P.sprintf "const %s" (str_of_sctype sct)
  | SC_array (sct,dimlst) ->
    let dimension_str = String.concat "" (List.map (fun dim -> P.sprintf "[%d]" dim) dimlst) in
    P.sprintf "(%s%s)" (str_of_sctype sct) dimension_str
  | SC_pointer sct -> P.sprintf "%s *" (str_of_sctype sct)
  | SC_typedef (typedef_name,sct) -> 
    P.sprintf "typedef(%s:%s)" typedef_name (str_of_sctype sct)
  | SC_subroutine(ret_sc, param_sc_list) -> (* first: return type, second: parameter type list *)
    let ret_sc_str = str_of_sctype ret_sc in
    let param_sc_str = String.concat "," (List.map str_of_sctype param_sc_list) in
    P.sprintf "(%s)(%s)" ret_sc_str param_sc_str 
  | SC_enumeration (enumname,etor_list) ->
    let etor_list_str = String.concat ";" (List.map (fun (etorname,etorval) -> P.sprintf "%s(%d)" etorname etorval) etor_list) in
    P.sprintf "ENUM %s{%s}" enumname etor_list_str
  | SC_union (unionname,umem_list) ->
    let umem_list_str = String.concat ";" (List.map (fun (umemtype,umemname) -> P.sprintf "%s %s" (str_of_sctype umemtype) umemname) umem_list) in
    P.sprintf "UNION %s{%s}" unionname umem_list_str
  | SC_structure (original_name, level_name, smem_list) ->
    let smem_list_str = String.concat ";" (List.map (fun (smemtype,smemname) -> P.sprintf "%s %s" (str_of_sctype smemtype) smemname) smem_list) in
    P.sprintf "STRUCT %s(%s){%s}" original_name level_name smem_list_str
  | SC_unspecified_param ->
    "..."    
  | SC_wrong_type errmsg ->
    let () = P.printf "Wrong type: %s" errmsg in
    P.sprintf "wrong_type(%s)" errmsg   
  | SC_volatile sct ->
    P.sprintf "volatile %s" (str_of_sctype sct)

let rec str_of_ectype ectype =
  match ectype with
  | [] -> ""
  | h :: t -> (str_of_sctype h) ^ "; " ^ (str_of_ectype t) 
(*
  match ectype with
  | EC_top -> "<T>"
  | EC_single sctype -> str_of_sctype sctype
  | EC_alt(ectype1,ectype2) ->
    P.sprintf "%s | %s" (str_of_ectype ectype1) (str_of_ectype ectype2)
*)
      
let print_ctypeEquation cteq =
  let print_equation eqn = 
    let varl, varr = eqn in
    P.printf "[%s] = [%s]\n" (str_of_ectype varl) (str_of_ectype varr)
  in
  P.printf "#%x: " cteq.index;
  print_equation cteq.equation

let print_gamma_record tvnm ectype =
  P.printf "%s: %s\n" (str_of_tvnm tvnm) (str_of_ectype ectype)

let print_sgcond sgcond =
  let teSet, gamma = sgcond in
  P.printf "Type Equation Set:\n";
  CtypeEqSet.iter print_ctypeEquation teSet;
  P.printf "Index record Gamma:\n";
  H.iter print_gamma_record gamma

let rec eq_sctype sctype1 sctype2 =
  match sctype1,sctype2 with
  | SC_top, SC_top -> true
  | SC_typeVar (tvnm1,tvndx1), SC_typeVar (tvnm2,tvndx2) ->
    tvnm1 = tvnm2 && tvndx1 = tvndx2
  | SC_base name1, SC_base name2 -> name1 = name2
  | SC_const sct1, SC_const sct2 -> eq_sctype sct1 sct2
  | SC_array (sct1,dimlist1), SC_array (sct2,dimlist2) -> 
    let type_consis = eq_sctype sct1 sct2 in 
    let dim_consis = 
      if List.length dimlist1 = List.length dimlist2 then 
	List.fold_right2 
	  (fun dim1 dim2 b -> 
	    if dim1 = 0 || dim2 = 0 then b 
	    else (dim1 = dim2) && b) dimlist1 dimlist2 true
      else false
    in
    type_consis && dim_consis
  | SC_array (sct1,_), SC_pointer sct2 -> eq_sctype sct1 sct2
  | SC_pointer sct1, SC_pointer sct2 -> eq_sctype sct1 sct2
  | SC_pointer sct1, SC_array (sct2,_) -> eq_sctype sct1 sct2
  | SC_typedef (_,sct1), _ -> eq_sctype sct1 sctype2
  | _, SC_typedef (_,sct2) -> eq_sctype sctype1 sct2
  | SC_subroutine (ret1,param1), SC_subroutine (ret2,param2) ->
    let ret_consis = eq_sctype ret1 ret2 in
    let param_consis = param_equal param1 param2 in
    ret_consis && param_consis
  | SC_enumeration (enumname1,etorlist1), SC_enumeration (enumname2,etorlist2) ->
    enumname1 = enumname2
  | SC_union (unionname1,umemlist1), SC_union (unionname2,umemlist2) ->
    (** when one has name but no type and the other does not have name but have type, they may
	still be the same **)
    if unionname1 = unionname2 then true
    else if List.length umemlist1 = List.length umemlist2 then
      List.fold_right2 (fun (sct1,str1) (sct2,str2) b -> b && (eq_sctype sct1 sct2) && (str1 = str2)) umemlist1 umemlist2 true
    else false
  | SC_structure (structname1,levelname1,smemlist1), SC_structure (structname2,levelname2,smemlist2) -> 
    if structname1 = structname2 then true
    else if levelname1 = levelname2 && (List.length smemlist1) = (List.length smemlist2) then
      if (List.length smemlist1) = 0 then true
      else
	List.fold_right2 (fun (sct1,str1) (sct2,str2) b -> b && eq_sctype sct1 sct2 && str1 = str2) smemlist1 smemlist2 true
    else false
  | SC_unspecified_param, SC_unspecified_param ->  
    true
  | SC_wrong_type errmsg1, _ -> 
    let () = P.printf "In eq_sctype, the first type is wrong type: %s" errmsg1 in
    false
  | _, SC_wrong_type errmsg2 ->
    let () = P.printf "In eq_sctype, the second type is wrong type: %s" errmsg2 in
    false
  | SC_volatile ct1, SC_volatile ct2 -> eq_sctype ct1 ct2
  | _ -> false
    
and param_equal param1 param2 =
  match param1,param2 with
  | [], [] -> true
  | hd_param1 :: tl_param1, hd_param2 :: tl_param2 ->
    eq_sctype hd_param1 hd_param2 && param_equal tl_param1 tl_param2
  | _ -> false

let eq_ectype ect1 ect2 =
  if List.length ect1 <> List.length ect2 then false
  else
    List.fold_right (fun sct1 b ->
      b && List.exists (fun sct2 -> eq_sctype sct1 sct2) ect2
    ) ect1 true

let eq_equation eqn1 eqn2 =
  if eqn1.index <> eqn2.index then false
  else
    let eqn1_ect1, eqn1_ect2 = eqn1.equation in
    let eqn2_ect1, eqn2_ect2 = eqn2.equation in
    eq_ectype eqn1_ect1 eqn2_ect1 && eq_ectype eqn1_ect2 eqn2_ect2

let eq_s s1 s2 =
  CtypeEqSet.fold (fun elt1 b ->
    b && CtypeEqSet.exists (fun elt2 -> eq_equation elt1 elt2) s2
  ) s1 true

let eq_g g1 g2 =
  H.fold (fun key dest b ->
    try
      b && eq_ectype dest (H.find g2 key)
    with Not_found ->
      false
  ) g1 true

let eq_sgcond sg1 sg2 =
  let set1, gamma1 = sg1 in
  let set2, gamma2 = sg2 in
  (*eq_s set1 set2 && *)eq_g gamma1 gamma2

let rec elim_dup cmp al1 =
  match al1 with
  | [] -> []
  | h :: t ->
    if List.exists (cmp h) t then elim_dup cmp t
    else h :: (elim_dup cmp t)

let add_new_eq_to_s eqn1 s =
  let outs = 
    if not (CtypeEqSet.exists (eq_equation eqn1) s) then (
      let map_success = ref false in
      let mapped_s = 
	CtypeEqSet.map (fun eqn2 ->
	  let eqn1_ect1, eqn1_ect2 = eqn1.equation in
	  let eqn2_ect1, eqn2_ect2 = eqn2.equation in
	  if eq_ectype eqn1_ect1 eqn2_ect1 then (
	    map_success := true;
	    {eqn2 with equation = (eqn2_ect1, (elim_dup eq_sctype (eqn1_ect2 @ eqn2_ect2)))}
     	  ) else eqn2
	) s
      in
      if !map_success then mapped_s
      else CtypeEqSet.add eqn1 s
    ) else s
  in
  outs
  
let union_s s1 s2 =
  CtypeEqSet.fold (fun eqn1 s ->
    add_new_eq_to_s eqn1 s
  ) s1 s2

let union_g g1 g2 =
  let ug = H.copy g1 in
  H.iter (fun tvnm ectype ->
    if H.mem ug tvnm then (
      let ectype_in_ug = H.find ug tvnm in
      if not (eq_ectype ectype_in_ug ectype) then (
	(*P.printf "G1:\n";
	H.iter print_gamma_record g1;
	P.printf "G2:\n";
	H.iter print_gamma_record g2;*)
	H.replace ug tvnm (elim_dup eq_sctype (ectype_in_ug @ ectype));
	(*P.printf "UG:\n";
	H.iter print_gamma_record ug;
	flush stdout;*)
      )
    )
    else H.add ug tvnm ectype
  ) g2;
  ug

let union_sg sg1 sg2 =
  let set1, gamma1 = sg1 in
  let set2, gamma2 = sg2 in
  union_s set1 set2, union_g gamma1 gamma2

type tyinfbb = {
  mutable ti_bb: basicblock;
  mutable ti_pre: sg_condition option;
  mutable ti_post: sg_condition option;
}

let mkTIBB pre bb post = {ti_bb=bb;ti_pre=pre;ti_post=post}

let dump_tibb tibb =
  P.printf "BB:%s\n" tibb.ti_bb.bb_label;
  P.printf "PRE:\n";
  if Option.is_some tibb.ti_pre then
    print_sgcond (Option.get tibb.ti_pre);
  P.printf "POST:\n";
  if Option.is_some tibb.ti_post then
    print_sgcond (Option.get tibb.ti_post)

module TypeVarOrder = struct
  type t = super_ctype
  let compare (sc1:super_ctype) (sc2:super_ctype) = 
    Pervasives.compare sc1 sc2
end

module TypeVarSet = Set.Make (TypeVarOrder)

type eqcNode = 
  {
    mutable eqc_name: string;
    mutable eqc_vars: TypeVarSet.t;
    mutable eqc_succs: eqcNode list;
    mutable eqc_preds: eqcNode list;
    mutable eqc_dbgtype: extended_sctype;
    mutable eqc_flowtype: extended_sctype;
  }

let mkEQCNode vars = 
  {
    eqc_name = str_of_sctype (List.hd vars);
    eqc_vars = List.fold_right (fun var s -> TypeVarSet.add var s) vars TypeVarSet.empty; 
    eqc_succs = []; eqc_preds = []; eqc_dbgtype = []; eqc_flowtype = []
  }

let dump_eqcnode eqcnode =
  P.printf "EQC_node %s contains variables:\n" eqcnode.eqc_name;
  TypeVarSet.iter (fun var ->
    P.printf "%s " (str_of_sctype var)
  ) eqcnode.eqc_vars;
  P.printf "\n";
  P.printf "SUCCS:\n";
  List.iter (fun eqc ->
    P.printf "%s " eqc.eqc_name
  ) eqcnode.eqc_succs;
  P.printf "\n";
  P.printf "PREDS:\n";
  List.iter (fun eqc ->
    P.printf "%s " eqc.eqc_name
  ) eqcnode.eqc_preds;
  P.printf "\n";
  P.printf "TypeInfo:\n";
  List.iter (fun sctype -> 
    P.printf "%s " (str_of_sctype sctype)
  ) eqcnode.eqc_typeinfo;
  P.printf "\n";
  flush stdout

type eqcGraph = (sctype,eqcNode) H.t

type eqcg_add eqcg equation =
  if H.mem eqcg sctype then
    let node = H.find eqcg sctype in
    

type eqcg_union eqcg sctype new_node =
  H.add eqcg sctype new_node 


type range = 
| R_all
| R_range of int * int
    
let in_range addr range =
  match range with
  | R_all -> true
  | R_range(lo,hi) -> 
    (* WARNING: range may overlap from debugging information *)
    lo <= addr && addr <= hi

type dbg_type_info = (tvName * range * extended_sctype) list

type type_relation = 
  {
    mutable tr_var: super_ctype;
    mutable tr_val: extended_sctype;
  }

let mkTR trVar trVal =
  {tr_var = trVar; tr_val = trVal}

type type_env = type_relation list

let dump_type_env tenv =
  P.printf "Type Environment:\n";
  List.iter (fun tr ->
    P.printf "%s = %s\n" (str_of_sctype tr.tr_var) (str_of_ectype tr.tr_val)
  ) tenv
    
let collect_visible_typeinfo elf target_name =
  let start = MWord.of_string "0x8048000" in
  let resultfilename = P.sprintf "./visible_typeinfo_%s_%s.txt" elf.fname target_name in
  let void = C_base "void" in
  let size_t = C_typedef ("size_t", C_base "unsinged int") in
  let ptr ct = C_pointer ct in
  let fp ret paramlst= C_subroutine (ret, paramlst) in
  if target_name = "deregister_tm_clones" then
    "", [mkvfp (fp void [ptr void]) VS_unknown []]
  else if target_name = "register_tm_clones" then
    "", [mkvfp (fp void [ptr void;size_t ]) VS_unknown []]
  else if target_name = "frame_dummy" then
    let fstvfp = mkvfp (fp void [void]) VS_unknown [] in
    let sndvfp = mkvfp (fp void [ptr void]) VS_unknown [] in
    "", [fstvfp; sndvfp]
  else if Sys.file_exists elf.fname then (
    let cmd = 
      P.sprintf "./dbg/collect_visible_typeinfo %s %s > %s" elf.fname target_name resultfilename 
    in
    let _ = if not (Sys.file_exists resultfilename) then Sys.command cmd else 0 in
    let ic = open_in resultfilename in
    let vfps = ref [] in
    let sinfo_count = ref 0 in
    let frame_base = ref "none" in
    let () = 
      try
	let frame_base_indicator = input_line ic in
	if frame_base_indicator = "Frame base" then
	  frame_base := input_line ic;
	while true do 
	  sinfo_count := !sinfo_count + 1;
	  let visisrc_e = input_line ic in
	  let vs = decode_visisrc visisrc_e in
	  let indicator = input_line ic in
	  let ctype = 
	    try decode_c_type indicator 
	    with WrongTypeEncoding ->
	      let () = 
		P.printf "Error! Now in file %s, decoding number %d sinfo entry!\n" resultfilename !sinfo_count; 
		flush stdout
	      in 
	      C_wrong_type "debugging"
	  in
	  let sinfo_start = input_line ic in
	  if sinfo_start <> "{" then (
	    print_endline "Error: unknown encoding after type encoding\n"; 
	    P.printf "in file %s at entry %d\n" resultfilename !sinfo_count;
	    flush stdout; 
	    exit 1)
	  else
	    let first_line = ref (input_line ic) in
	    let locdesc_list = ref [] in
	    while !first_line <> "}" do
	      if !first_line = "NULL" then (
		let vfp_entry = mkvfp ctype vs [] in
		vfps := !vfps @ [vfp_entry];
		first_line := input_line ic
	      )
	      else (
		let lopc_e = !first_line in
		let hipc_e = input_line ic in
		let lopc = MWord.of_string lopc_e in
		let hipc = MWord.of_string hipc_e in
		let lopc_rel =
		  if MWord.(>=%) lopc start then get_relative_addr lopc start 
		  else MWord.to_int lopc
		in
		let hipc_rel =
		  if MWord.(>=%) hipc start then get_relative_addr hipc start 
		  else MWord.to_int hipc
		in
		  (* 
		     let le_start = input_line ic in
		     if le_start <> "Le:" then ( simple_warn "Error: unknown things after location expression start string, le"; exit 1 )
		     else
		  *)
		let locexpr_list = ref [] in
		let curr_expr = ref (input_line ic) in
		while !curr_expr <> "" do
		  locexpr_list := !locexpr_list @ [!curr_expr];
		  curr_expr := input_line ic
		done;
		let locdesc_entry = mkdesc lopc_rel hipc_rel !locexpr_list in
		locdesc_list := !locdesc_list @ [locdesc_entry];
		first_line := input_line ic
	      )
	    done;
	    let vfp_entry = mkvfp ctype vs !locdesc_list in
	    vfps := !vfps @ [vfp_entry]
	done
      with End_of_file ->
	close_in ic
    in
      (*let () = List.iter print_vfp !vfps in*)
    let _ = Sys.command (P.sprintf "rm %s" resultfilename) in
    !frame_base,!vfps
  )
  else (
    P.printf "No file %s exists!" resultfilename; flush stdout;
    ("none", [])
  )

let interp_locexprs frame_base locexprs =
  if List.mem "DW_OP_stack_value" locexprs then None
  else if List.length locexprs <> 1 then None (* do not support currently *)
  else 
    let locexpr = List.hd locexprs in
    if String.exists locexpr "DW_OP_reg" then
      let op = String.strip locexpr in
      match op with
      | "DW_OP_reg0" -> Some (TV_reg EAX)
      | "DW_OP_reg1" -> Some (TV_reg ECX)
      | "DW_OP_reg2" -> Some (TV_reg EDX)
      | "DW_OP_reg3" -> Some (TV_reg EBX)
      | "DW_OP_reg4" -> Some (TV_reg ESP)
      | "DW_OP_reg5" -> Some (TV_reg EBP)
      | "DW_OP_reg6" -> Some (TV_reg ESI)
      | "DW_OP_reg7" -> Some (TV_reg EDI)
      | _ -> None
    else if String.exists locexpr "DW_OP_fbreg" then
      let op,off_str = String.split locexpr " " in
      match frame_base with
      | "DW_OP_call_frame_cfa" -> 
	let off = int_of_string off_str in
	Some (TV_cfa off)
      | "DW_OP_breg0" -> 
	let addrname = mkADNM off_str (Some EAX) None in
	Some (TV_addr addrname)
      | "DW_OP_breg1" -> 
	let addrname = mkADNM off_str (Some ECX) None in
	Some (TV_addr addrname)
      | "DW_OP_breg2" -> 
	let addrname = mkADNM off_str (Some EDX) None in
	Some (TV_addr addrname)
      | "DW_OP_breg3" -> 
	let addrname = mkADNM off_str (Some EBX) None in
	Some (TV_addr addrname)
      | "DW_OP_breg4" -> 
	let addrname = mkADNM off_str (Some ESP) None in
	Some (TV_addr addrname)
      | "DW_OP_breg5" ->
	let addrname = mkADNM off_str (Some EBP) None in
	Some (TV_addr addrname)
      | "DW_OP_breg6" ->
	let addrname = mkADNM off_str (Some ESI) None in
	Some (TV_addr addrname)
      | "DW_OP_breg7" -> 
	let addrname = mkADNM off_str (Some EDI) None in
	Some (TV_addr addrname)
      | _ -> None
    else if String.exists locexpr "DW_OP_breg" then
      let op,off_str = String.split locexpr " " in
      match op with
      | "DW_OP_breg0" -> 
	let addrname = mkADNM off_str (Some EAX) None in
	Some (TV_addr addrname)
      | "DW_OP_breg1" -> 
	let addrname = mkADNM off_str (Some ECX) None in
	Some (TV_addr addrname)
      | "DW_OP_breg2" -> 
	let addrname = mkADNM off_str (Some EDX) None in
	Some (TV_addr addrname)
      | "DW_OP_breg3" -> 
	let addrname = mkADNM off_str (Some EBX) None in
	Some (TV_addr addrname)
      | "DW_OP_breg4" -> 
	let addrname = mkADNM off_str (Some ESP) None in
	Some (TV_addr addrname)
      | "DW_OP_breg5" ->
	let addrname = mkADNM off_str (Some EBP) None in
	Some (TV_addr addrname)
      | "DW_OP_breg6" ->
	let addrname = mkADNM off_str (Some ESI) None in
	Some (TV_addr addrname)
      | "DW_OP_breg7" -> 
	let addrname = mkADNM off_str (Some EDI) None in
	Some (TV_addr addrname)
      | _ -> None
    else if String.exists locexpr "DW_OP_addr" then
      let op,addr = String.split locexpr " " in
      let addrname = mkADNM addr None None in
      Some (TV_addr addrname)
    else 
      None

let trans_raw_dbgtypeinfo frame_base raw_dbg =
  List.filter_map (fun (locexprs,rng,sctype) ->
    let loc = interp_locexprs frame_base locexprs in
    let range = 
      if snd rng = (-1) then R_all
      else R_range ((fst rng),(snd rng))
    in
    if Option.is_some loc then
      Some (Option.get loc,range,[sctype])
    else None
  ) raw_dbg

let rec ctype_to_sctype ctype = 
  match ctype with
  | C_base name -> SC_base name 
  | C_const ct -> SC_const (ctype_to_sctype ct)
  | C_array (ct,dimlst) -> 
    SC_array ((ctype_to_sctype ct),dimlst)
  | C_pointer ct -> 
    SC_pointer (ctype_to_sctype ct)
  | C_typedef (typedef_name, ct) ->
    SC_typedef (typedef_name,(ctype_to_sctype ct))
  | C_subroutine (ret_ct, param_ct_list) ->
    SC_subroutine ((ctype_to_sctype ret_ct), (List.map ctype_to_sctype param_ct_list))
  | C_enumeration (enumname, etor_list) -> 
    SC_enumeration (enumname, etor_list)
  | C_union (unionname, umem_list) -> 
    SC_union (unionname, (List.map (fun (umemtype,umemname) -> (ctype_to_sctype umemtype), umemname) umem_list))
  | C_structure (original_name, level_name, smem_list) ->
    SC_structure (original_name, level_name, (List.map (fun (smemtype,smemname) -> (ctype_to_sctype smemtype), smemname) smem_list))
  | C_unspecified_param ->
    SC_unspecified_param
  | C_wrong_type errmsg ->
    SC_wrong_type errmsg
  | C_volatile ct ->
    SC_volatile (ctype_to_sctype ct)

let retrieve_dbgtypeinfo elf asm_fun_name =
  let frame_base, visible_tyinfo = collect_visible_typeinfo elf asm_fun_name in
  let raw_dbgtypeinfo = 
    List.flatten (
      List.map (fun vfp ->
	let ctype = vfp.vfp_tsig in
	let sctype = ctype_to_sctype ctype in
	let range_and_location =
	  List.map (fun locdesc -> 
	    let range = (locdesc.ld_lopc, locdesc.ld_hipc) in
	    range, locdesc.ld_locexprs
	  ) vfp.vfp_sinfo
	in
	List.map (fun (rng,locexprs) ->
	  locexprs,rng,sctype
	) range_and_location
      ) visible_tyinfo
    )
  in
  let dbgtypeinfo = trans_raw_dbgtypeinfo frame_base raw_dbgtypeinfo in
  dbgtypeinfo

let dbgTypeGen elf asm_fun_list =
  List.map (fun asm_fun ->
    let asm_fun_dbg = retrieve_dbgtypeinfo elf asm_fun.func_name in
    asm_fun, asm_fun_dbg
  ) asm_fun_list
(*
  [
  "(*-1 + 0)", (R_all), [tint];
  "(*-1 + 4)", (R_all), [SC_pointer (SC_pointer tchar)];
  "(*-1 + -24)", (R_all), [tint];
  "(*-1 - 24)", (R_all), [tint];
  "(*-1 + -20)", (R_all), [tint];
  "(*-1 - 20)", (R_all), [tint];
  "(*-1 + -28)", (R_all), [SC_pointer (SC_subroutine (tint, [SC_pointer tchar]))];
  "(*-1 - 28)", (R_all), [SC_pointer (SC_subroutine (tint, [SC_pointer tchar]))];
  ]
*)

