open Batteries
module P = Printf
module H = Hashtbl

open X86Syntax
open X86Semantics
open X86_MACHINE
open X86_RTL
open Config
open Abbrev

open BB
open RTL_opt
open Printer
open Config_cfg

open TeGenB_DS
open TeGenB

let visited_eqcs = ref []

let influenced_eqcs = ref []

let rec collect_reachable_eqcs eqcg direction start_eqc trace =
  let next eqc = 
    if direction then 
      List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) eqc.eqc_succs)
    else
      List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) eqc.eqc_preds)
  in
  if not (List.mem start_eqc !visited_eqcs) then (
    visited_eqcs := start_eqc :: !visited_eqcs;
    if not (List.is_empty start_eqc.eqc_dbgtype) then
      influenced_eqcs := (start_eqc,trace) :: !influenced_eqcs
    else if List.is_empty start_eqc.eqc_preds then
      influenced_eqcs := (start_eqc,trace) :: !influenced_eqcs
    else if not (List.is_empty start_eqc.eqc_fflowtype) then
      influenced_eqcs := (start_eqc,trace) :: !influenced_eqcs
    else
      List.iter (fun (eqc,edge) ->
	if fst edge <> 0 then
	  let next_trace = (eqc.eqc_name,edge) :: trace in
	  collect_reachable_eqcs eqcg direction eqc next_trace
	else
	  collect_reachable_eqcs eqcg direction eqc trace
      ) (next start_eqc)
  )
    
let collect_influenced_eqcs eqcg direction start_eqc edgestack =
  visited_eqcs := [];
  influenced_eqcs := [];
  collect_reachable_eqcs eqcg direction start_eqc edgestack;
  !influenced_eqcs

let retrieve_ectype_from_eqc eqc trace struct_map =
  let edgestack = List.map snd trace in 
  if List.is_empty eqc.eqc_dbgtype then (
    List.fold_left (fun ect edge ->
      let edgetype, offset = edge in
      if edgetype = 0 then
	ect
      else if edgetype = 2 then
	List.map (fun sctype ->
	  SC_pointer sctype
	) ect
      (*raise (Failure "Magical edgetype: 2")*)
      else
	List.flatten (
	  List.filter_map (fun sctype ->
	    try 
	      Some (strict_deref_sctype struct_map sctype offset)
	    with Failure errmsg ->
	      P.printf "%s" errmsg; flush stdout;
	      P.printf "at eqc: %s fflowtype\n" eqc.eqc_name;
	      P.printf "with edgestack: %s\n" (str_of_trace trace);
	      P.printf "aiming at sctype:%s with offset:%s\n" (str_of_sctype sctype) offset;
	      flush stdout;
	      None
	  ) ect
	)
    ) eqc.eqc_fflowtype edgestack		
  )
  else (
    List.fold_left (fun ect edge ->
      let edgetype, offset = edge in
      if edgetype = 0 then
	ect
      else if edgetype = 2 then
	List.map (fun sctype ->
	  SC_pointer sctype
	) ect
      (*raise (Failure "Magical edgetype: 2")*)
      else
	List.flatten (
	  List.filter_map (fun sctype ->
	    try 
	      Some (strict_deref_sctype struct_map sctype offset)
	    with Failure errmsg ->
	      P.printf "%s" errmsg; flush stdout;
	      P.printf "at eqc: %s dbgtype\n" eqc.eqc_name;
	      P.printf "with trace: %s\n" (str_of_trace trace);
	      P.printf "aiming at sctype:%s with offset:%s\n" (str_of_sctype sctype) offset;
	      flush stdout;
	      None
	  ) ect
	)
    ) eqc.eqc_dbgtype edgestack					     
  )

let rec propagate_fast eqcg start_eqc start_ectype struct_map =
  let direction = true in
  let next eqc = 
    if direction then 
      List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) eqc.eqc_succs)
    else
      List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) eqc.eqc_preds)
  in
  visited_eqcs := start_eqc :: !visited_eqcs;
  List.iter (fun (eqc,edge) ->
    if not (List.mem eqc !visited_eqcs) then (
      if List.is_empty eqc.eqc_dbgtype then (
	let processed_ectype = 
	  try
	    reason_ectype start_ectype [edge] struct_map 
	  with Failure errmsg ->
	    P.printf "%s" errmsg;
	    P.printf "at eqc: %s\n" eqc.eqc_name;
	    P.printf "aiming with edge %s at %s\n" 
	      (str_of_edge edge) (str_of_ectype start_ectype);
	    flush stdout;
	    []
	in
	if not (List.is_empty processed_ectype) && 
	  not (sub_ectype processed_ectype eqc.eqc_fflowtype)
	then (
	  eqc.eqc_fflowtype <- 
	    List.fold_left (fun rettype sctype -> 
	      if List.exists (eq_sctype sctype) rettype then rettype
	      else sctype :: rettype
	    ) eqc.eqc_fflowtype processed_ectype;
	  propagate_fast eqcg eqc processed_ectype struct_map
	)
      ) 
    ) else ()
  ) (next start_eqc)
      
let rec propagate_top_fast eqcg start_eqc start_ectype =
  let next_eqcs =  
    List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) start_eqc.eqc_succs)
  in
  visited_eqcs := start_eqc :: !visited_eqcs;
  List.iter (fun (eqc,edge) ->
    if not (List.mem eqc !visited_eqcs) then (
      if List.is_empty eqc.eqc_dbgtype then (
	let processed_ectype =
	  if !report_top_trace then
	    List.map (fun sctype ->
	      match sctype with
	      | SC_top pretrace -> 
		SC_top ((start_eqc.eqc_name,edge) :: pretrace)
	      | _ -> sctype
	    ) start_ectype
	  else start_ectype
	in
	if not (sub_ectype processed_ectype eqc.eqc_fflowtype)
	then (
	  eqc.eqc_fflowtype <- processed_ectype @ eqc.eqc_fflowtype;
	  propagate_top_fast eqcg eqc processed_ectype
	)
      )
    ) else ()
  ) next_eqcs

let rec propagate eqcg start_eqc start_ectype struct_map =
  let direction = true in
  let next eqc = 
    if direction then 
      List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) eqc.eqc_succs)
    else
      List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) eqc.eqc_preds)
  in
  (*
    if not (List.mem start_eqc visited) then (
    let new_visited = start_eqc :: visited in*)
  List.iter (fun (eqc,edge) ->
    if List.is_empty eqc.eqc_dbgtype then
      let processed_ectype = 
	try
	  reason_ectype start_ectype [edge] struct_map 
	with Failure errmsg ->
	  P.printf "%s" errmsg;
	  P.printf "at eqc: %s\n" eqc.eqc_name;
	  P.printf "aiming with edge %s at %s\n" 
	    (str_of_edge edge) (str_of_ectype start_ectype);
	  flush stdout;
	  []
      in
      if not (eq_ectype eqc.eqc_fflowtype start_ectype) then (
	eqc.eqc_fflowtype <- 
	  List.fold_left (fun rettype sctype -> 
	    match sctype with
	    | SC_top _ -> 
	      sctype :: rettype
	    | _ ->
	      if List.exists (eq_sctype sctype) rettype then rettype
	      else sctype :: rettype
	  ) eqc.eqc_fflowtype processed_ectype;
	propagate eqcg eqc eqc.eqc_fflowtype struct_map
      )
  ) (next start_eqc)
      
let rec propagate_top eqcg start_eqc start_ectype =
  let next_eqcs =  
    List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) start_eqc.eqc_succs)
  in
  (*
  if not (List.mem start_eqc visited) then (
    let new_visited = start_eqc :: visited in*)
  List.iter (fun (eqc,edge) ->
    if List.is_empty eqc.eqc_dbgtype then
      let processed_ectype =
	if !report_top_trace then
	  List.map (fun sctype ->
	    match sctype with
	    | SC_top pretrace -> 
	      SC_top ((start_eqc.eqc_name,edge) :: pretrace)
	    | _ -> sctype
	  ) start_ectype
	else start_ectype
      in
      if not (eq_ectype eqc.eqc_fflowtype start_ectype) then (
	eqc.eqc_fflowtype <- 
	  List.fold_left (fun rettype sctype -> 
	    match sctype with
	    | SC_top _ -> 
	      sctype :: rettype
	    | _ ->
	      if List.exists (eq_sctype sctype) rettype then rettype
	      else sctype :: rettype
	  ) eqc.eqc_fflowtype processed_ectype;
	propagate_top eqcg eqc eqc.eqc_fflowtype
      )
  ) next_eqcs

let type_solve_full_fast struct_map eqcg =
  let fast = true in
  let _,nodes = eqcg in
  let rem_nodes = ref (H.length nodes) in
  if !verbose_on then (
    P.printf "start with %d nodes\n" !rem_nodes;
    flush stdout
  );
  H.iter (fun repr eqc ->
    if not (List.is_empty eqc.eqc_dbgtype) then (
      if fast then
	propagate_fast eqcg eqc eqc.eqc_dbgtype struct_map
      else
	propagate eqcg eqc eqc.eqc_dbgtype struct_map;
      visited_eqcs := []
    )
    else if List.is_empty eqc.eqc_preds then ( 
      eqc.eqc_fflowtype <- [SC_top[]];
      propagate_top_fast eqcg eqc [SC_top[]];
      (*
      if fast then
	propagate_top_fast eqcg eqc [SC_top[]]
      else 
	propagate_top eqcg eqc [SC_top[]];
      *)
      visited_eqcs := []
    )
    else ();
    rem_nodes := !rem_nodes - 1;
    if !verbose_on then (
      P.printf "%d nodes remaining...\n" !rem_nodes;
      flush stdout
    )
  ) nodes

let type_solve_full_wl struct_map eqcg =
  let direction = true in
  let verbose = false in
  let next eqc = 
    if direction then 
      List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) eqc.eqc_succs)
    else
      List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) eqc.eqc_preds)
  in
  let _, nodes = eqcg in
  let workq = Queue.create () in
  H.iter (fun repr eqc ->
    if not (List.is_empty eqc.eqc_dbgtype) then
      Queue.add eqc workq
    else if List.is_empty eqc.eqc_preds then (
      eqc.eqc_fflowtype <- [SC_top[]];
      visited_eqcs := [];
      propagate_top_fast eqcg eqc [SC_top[]]
      (*Queue.add eqc workq*)
    )    
    else ()
  ) nodes;
  while (not (Queue.is_empty workq)) do
    if verbose then
      (P.printf "TASKS remaining: %d\n" (Queue.length workq); flush stdout);
    let task_eqc = Queue.take workq in
    let prop_type = 
      if List.is_empty task_eqc.eqc_dbgtype then
	task_eqc.eqc_fflowtype
      else
	task_eqc.eqc_dbgtype
    in
    assert (not (List.is_empty prop_type));
    List.iter (fun (eqc,edge) ->
      if List.is_empty eqc.eqc_dbgtype then
	let processed_ectype = 
      	  try
	    reason_ectype prop_type [edge] struct_map 
	  with Failure errmsg ->
	    P.printf "%s" errmsg;
	    P.printf "at eqc: %s\n" eqc.eqc_name;
	    P.printf "aiming with edge %s at %s\n" 
	      (str_of_edge edge) (str_of_ectype prop_type);
	    flush stdout;
	    []
	in
	if verbose then (
	  P.printf "comparing types: %d : %d\n" 
	    (List.length processed_ectype)
	    (List.length eqc.eqc_fflowtype);
	  flush stdout
	);
	if sub_ectype processed_ectype eqc.eqc_fflowtype then
	  ()
	else (
	  eqc.eqc_fflowtype <- 
	    List.fold_left (fun rettype sctype -> 
	      if List.exists (eq_sctype sctype) rettype then rettype
	      else sctype :: rettype
	    ) eqc.eqc_fflowtype processed_ectype;
	  Queue.add eqc workq
	)
      else
	()
    ) (next task_eqc)
  done

let type_solve_full struct_map eqcg =
  let _, nodes = eqcg in
  let rem_nodes = ref (H.length nodes) in
  P.printf "start with %d nodes\n" !rem_nodes;
  flush stdout;
  H.iter (fun repr eqc ->
    if List.is_empty eqc.eqc_dbgtype && List.is_empty eqc.eqc_fflowtype then
      if List.is_empty eqc.eqc_preds then eqc.eqc_fflowtype <- [SC_top []]
      else (
	let backward_reachable = collect_influenced_eqcs eqcg false eqc [(eqc.eqc_name, (0,"none"))] in
	(*P.printf "backward_reachable collected!\n"; flush stdout;*)
	(*P.printf "EQC:%s has backward_reachable:%d\n" 
	  eqc.eqc_name (List.length backward_reachable); 
	List.iter (fun (eqc',edgestack) -> P.printf "%s " eqc'.eqc_name) backward_reachable;
	P.printf "\n";
	flush stdout;*)
	
	List.iter (fun (eqc',trace) ->
	  (*P.printf "Processing EQC:%s <-- EQC:%s\n" 
	    eqc.eqc_name eqc'.eqc_name; 
	    flush stdout;*)
	  let processed_type = retrieve_ectype_from_eqc eqc' trace struct_map in
	  (*P.printf "possible composite type processed\n"; flush stdout;*)
	  let processed_type =
	    if !report_top_trace then
	      List.map (fun sctype ->
		match sctype with
		| SC_top pretrace -> 
		  (*P.printf "%s:%s" eqc.eqc_name (str_of_trace trace); flush stdout;*)
		  SC_top (trace @ pretrace)
		| _ -> sctype
	      ) processed_type
	    else processed_type
	  in
	  eqc.eqc_fflowtype <- 
	    List.unique ?eq:(Some eq_sctype) (processed_type @ eqc.eqc_fflowtype)
	    (*
	    List.fold_left (fun rettype sctype -> 
	      match sctype with
	      | SC_top _ -> 
		sctype :: rettype
	      | _ ->
		if List.exists (eq_sctype sctype) rettype then rettype
		else sctype :: rettype
	    ) eqc.eqc_fflowtype processed_type*)
	) backward_reachable;
	rem_nodes := !rem_nodes - 1;
	P.printf "%d nodes remaining...\n" !rem_nodes;
	flush stdout;
      )
  ) nodes

(*
let rec type_solve_target eqcg eqc =
  if List.is_empty eqc.eqc_dbgtype && List.is_empty eqc.eqc_fflowtype then (
    if List.is_empty eqc.eqc_preds then (
      eqc.eqc_fflowtype <- [SC_top];
      [SC_top]
    ) else (
      let preds_types = 
	List.flatten (
	  List.map (fun (pred_eqc, edge) ->
	    let edgetype, offset = edge in
	    if edgetype = 0 then
	      type_solve_target eqcg pred_eqc
	    else if edgetype = 1 then
	      let pred_type = type_solve_target eqcg pred_eqc
	  ) eqc.eqc_preds
	)
      in
    )
  ) else if List.is_empty eqc.eqc_dbgtype then
      eqc.eqc_fflowtype
    else
      eqc.eqc_dbgtype
*)

(*
module SolverA = struct
  let eqc_graph_of teSet =
    let moSet, biSet = CtypeEqSet.partition (fun eq ->
      not eq.bi_direct
    ) teSet
    in

    let no_top scvar_list =
      List.filter (fun scvar ->
	scvar <> SC_top
      ) scvar_list
    in

    let mo_base_eqcg = List.flatten (
      List.map (fun eq ->
	let varl,varr = eq.equation in
	List.filter_map (fun scvar ->
	  if scvar <> SC_top then
	    Some (mkEQCNode [scvar])
	  else
	    None
	) (varl @ varr)
      ) (CtypeEqSet.elements moSet)
    )
    in
    let bi_base_eqcg = List.map (fun eq ->
      let varl,varr = eq.equation in
      mkEQCNode (no_top (varl @ varr))
    ) (CtypeEqSet.elements biSet)
  in
    let base_eqcg = mo_base_eqcg @ bi_base_eqcg in
    let rec merge_eqcg eqcg =
      match eqcg with
      | [] -> []
      | eqch :: eqct ->
	try
	  let eqc_in_t = 
	    List.find (fun eqc -> 
	      not (TypeVarSet.is_empty (TypeVarSet.inter eqch.eqc_vars eqc.eqc_vars))
	    ) eqct
	  in
	  eqc_in_t.eqc_vars <- (TypeVarSet.union eqch.eqc_vars eqc_in_t.eqc_vars);
	  merge_eqcg eqct
	with Not_found ->
	  eqch :: (merge_eqcg eqct)
    in
    let base_eqcg = ref (merge_eqcg base_eqcg) in
    CtypeEqSet.iter (fun eq ->
      let varl, varr = eq.equation in
      if List.length varl <> 1 then 
	raise (Failure "We have multi-destination...")
      else
	let varl_eqc = 
	  try
	    List.find (fun eqc ->
	      TypeVarSet.mem (List.hd varl) eqc.eqc_vars
	    ) !base_eqcg
	  with Not_found ->
	    let new_node = mkEQCNode varl in
	    base_eqcg := new_node :: !base_eqcg;
	    new_node
	in
	List.iter (fun sctype -> 
	  let sctype_eqc = 
	    try 
	      List.find (fun eqc ->
		TypeVarSet.mem sctype eqc.eqc_vars
	      ) !base_eqcg
	    with Not_found ->
	      let new_node = mkEQCNode [sctype] in
	      base_eqcg := new_node :: !base_eqcg;
	      new_node
	  in
	  sctype_eqc.eqc_succs <- varl_eqc :: sctype_eqc.eqc_succs;
	  varl_eqc.eqc_preds <- sctype_eqc :: varl_eqc.eqc_preds
	) varr
    ) moSet;
    let eqcg = !base_eqcg in
  (*List.iter dump_eqcnode eqcg;*)
    eqcg

  let assign_typeinfo dbgTypeInfo eqcg =
    let rec find_typeinfo var = 
      match var with
    (*| SC_top -> [SC_top]*)
      | SC_typeVar (nm,ndx) -> begin 
	try
	  let _, _, ect = 
	    List.find (fun (tvnm,rng,ectype) ->
	      tvnm = nm && in_range ndx rng
	    ) dbgTypeInfo
	  in
	  ect
	with Not_found -> [] 
      end
      | SC_pointer(sctype) -> 
	List.map (fun sct -> 
	  SC_pointer sct
	) (find_typeinfo sctype)
      | _ -> 
	P.printf "%s is not a simple type variable\n" (str_of_sctype var); flush stdout;
	[] (** TODO: add other super_ctype **)
    in
    
    List.iter (fun eqc ->
      let vars = eqc.eqc_vars in
      TypeVarSet.iter (fun var ->
	let ect = find_typeinfo var in
	let new_typeinfo =
	  elim_dup eq_sctype (ect @ eqc.eqc_typeinfo)
	in
	eqc.eqc_typeinfo <- new_typeinfo
      ) vars
    ) eqcg;
    eqcg

  let type_solve_single dbgTypeInfo teSet (target:super_ctype) =
    let eqcg = eqc_graph_of teSet in
    let typed_eqcg = assign_typeinfo dbgTypeInfo eqcg in
    try
      let start_eqc = 
	List.find (fun eqc ->
	  TypeVarSet.mem target eqc.eqc_vars
	) typed_eqcg
      in
      let reachable_eqcs = 
	collect_reachable_eqcs false start_eqc []
      in
	     (*List.iter dump_eqcnode reachable_eqcs*)
      let reachable_typeinfo =
	elim_dup eq_sctype (
	  List.flatten (
	    List.map (fun eqc ->
	      eqc.eqc_typeinfo
	    ) reachable_eqcs
	  )
	)
      in
      let solution = [mkTR target reachable_typeinfo] in
      solution
    with Not_found -> 
      P.printf "Not in current teSet\n";flush stdout;
      []

  let type_solve dbgTypeInfo teSet = 
    let eqcg = eqc_graph_of teSet in
    let typed_eqcg = assign_typeinfo dbgTypeInfo eqcg in
    let start_eqcs = List.filter (fun eqc ->
      not (List.is_empty eqc.eqc_typeinfo)
    ) typed_eqcg
    in
    let solution = ref [] in
    List.iter (fun start_eqc ->
      let reachable_eqcs =
	collect_reachable_eqcs true start_eqc []
      in
      let reachable_vars = List.flatten (
	List.map (fun eqc ->
	  TypeVarSet.elements eqc.eqc_vars
	) reachable_eqcs
      )
      in
      List.iter (fun var ->
	try
	  let trvar = List.find (fun tr ->
	    eq_sctype tr.tr_var var
	  ) !solution
	  in
	  let new_val = elim_dup eq_sctype (start_eqc.eqc_typeinfo @ trvar.tr_val) in
	  trvar.tr_val <- new_val
	with Not_found ->
	  let new_val = start_eqc.eqc_typeinfo in
	  let trvar = mkTR var new_val in
	  solution := trvar :: !solution
      ) reachable_vars
    ) start_eqcs;
    List.sort Pervasives.compare !solution
end
*)

(*
  P.printf "pos = %d; size = %d; off_remain = %d\n" !pos (List.length fieldlist) !off; flush stdout;
  find_struct_map struct_map (fst (List.at fieldlist !pos))
*)

(*
let rec deref_sctype struct_map sctype offset =
  let rec inner_deref_sctype sct =
    match sct with
    | SC_top -> SC_top
    | SC_const sct' | SC_volatile sct' -> inner_deref_sctype sct'
    | SC_typedef (defnm,sct') ->
      begin match sct' with
      | SC_structure _ -> sct
      | _ -> inner_deref_sctype sct'
      end
    | _ -> sct
  in
  match sctype with
  | SC_top -> SC_top
  | SC_const sct | SC_volatile sct -> 
    deref_sctype struct_map sct offset
  | SC_typedef (defnm,sct) -> 
    let real_sctype = find_struct_map struct_map sctype in
    begin match real_sctype with
    | SC_structure (_,_,fieldlist) ->
      if offset = "all" then SC_top
      else if List.is_empty fieldlist then 
	raise (Failure "struct map contains dummy struct type")
      else (
	let off = ref (int_of_string offset) in
	let pos = ref 0 in
	if !off >= Option.get (size_of_sctype real_sctype) then
	  let errmsg = P.sprintf "offset %s(%d) > size_of_structure %d" 
	    offset !off (Option.get (size_of_sctype real_sctype))
	  in
	  raise (Failure errmsg)
	else (
	  List.iter (fun (ft,fn) ->
	    if !off > 0 then (
	      let ftsize = Option.get (size_of_sctype ft) in
	      begin match ft with
	      | SC_array _ -> off := !off - ftsize
	      | _ ->
		if ftsize <= 4 then 
		  off := !off - 4
		else
		  off := !off - ftsize;
	      end;
	      pos := !pos + 1
	    )
	  ) fieldlist;
	  P.printf "pos = %d; size = %d; off_remain = %d\n" !pos (List.length fieldlist) !off; flush stdout;
	  find_struct_map struct_map (fst (List.at fieldlist !pos))
	)
      )
    | _ -> 
      deref_sctype struct_map sct offset
    end
  | SC_array (sct,dimlist) -> 
    if offset = "all" then sct
    else
      let bound = List.fold_right (fun dim mult ->
	dim * mult
      ) dimlist 1
      in
      let sctsize = Option.get (size_of_sctype sct) in
      let off = int_of_string offset in
      let index = off / sctsize in
      if index < bound then
	sct
      else
	raise (Failure "dereference out of bound for array")
  | SC_pointer sct ->
    let real_target_sct = inner_deref_sctype sct in
    begin match real_target_sct with
    | SC_top -> SC_top
    | SC_typedef (defnm,sct')->
      let real_struct = find_struct_map struct_map real_target_sct in
      begin match real_struct with
      | SC_structure (_,_,fieldlist) ->
	if offset = "all" then SC_top
	else if List.is_empty fieldlist then 
	  raise (Failure "struct map contains dummy struct type")
	else (
	  let off = ref (int_of_string offset) in
	  let pos = ref 0 in
	  if !off >= Option.get (size_of_sctype real_struct) then
	    let errmsg = P.sprintf "offset %s(%d) > size_of_structure %d" 
	      offset !off (Option.get (size_of_sctype real_struct))
	    in
	    raise (Failure errmsg)
	  else (
	    List.iter (fun (ft,fn) ->
	      if !off > 0 then (
		let ftsize = Option.get (size_of_sctype ft) in
		begin match ft with
		| SC_array _ -> off := !off - ftsize
		| _ ->
		  if ftsize <= 4 then 
		    off := !off - 4
		  else
		    off := !off - ftsize;
		end;
		pos := !pos + 1
	      )
	    ) fieldlist;
	    try
	      find_struct_map struct_map (fst (List.at fieldlist !pos))
	    with Invalid_argument errmsg ->
	      P.printf "pos = %d; size = %d; off_remain = %d\n" 
		!pos (List.length fieldlist) !off; 
	      flush stdout;
	      raise (Failure errmsg)
	      
	  )
	)
      | _ -> 
        raise (Failure "struct map contains non-struct")
      end
    | SC_array (sct,dimlist) -> 
      if offset = "all" then sct
      else 
	let bound = List.fold_right (fun dim mult ->
	  dim * mult
	) dimlist 1
	in
	let sctsize = Option.get (size_of_sctype sct) in
	let off = int_of_string  offset in
	let index = off / sctsize in
	if index < bound then
	  sct
	else
	  raise (Failure "dereference out of bound for array")
    | SC_structure (_,_,_) ->
      let real_struct = find_struct_map struct_map real_target_sct in
      let fieldlist = match real_struct with
	| SC_structure (_,_,fdlist) -> fdlist
	| _ -> raise (Failure "struct_map does not only contain struct")
      in
      if offset = "all" then SC_top
      else if List.is_empty fieldlist then 
	raise (Failure "struct map contains dummy struct type")
      else (
	let off = ref (int_of_string offset) in
	let pos = ref 0 in
	if !off > Option.get (size_of_sctype real_struct) then
	  let errmsg = P.sprintf "offset %s(%d) > size_of_structure %d" 
	    offset !off (Option.get (size_of_sctype real_struct))
	  in
	  raise (Failure errmsg)
	else (
	  List.iter (fun (ft,fn) ->
	    if !off > 0 then (
	      let ftsize = Option.get (size_of_sctype ft) in
	      begin match ft with
	      | SC_array _ -> off := !off - ftsize
	      | _ ->
		if ftsize <= 4 then 
		  off := !off - 4
		else
		  off := !off - ftsize;
	      end;
	      pos := !pos + 1
	    )
	  ) fieldlist;
	  P.printf "pos = %d; size = %d; off_remain = %d\n" !pos (List.length fieldlist) !off; flush stdout;
	  find_struct_map struct_map (fst (List.at fieldlist !pos))
	)
      )
    | _ ->
      if offset <> "0" then
	P.printf "WARNING: dereference pointer with offset!\n"; flush stdout;
      real_target_sct
    end
  | SC_structure (_,_,_) ->
    let real_struct = find_struct_map struct_map sctype in
    let fieldlist = match real_struct with
      | SC_structure (_,_,fdlist) -> fdlist
      | _ -> raise (Failure "struct_map does not contain struct")
    in
    if offset = "all" then SC_top
    else if List.is_empty fieldlist then 
      raise (Failure "struct map contains dummy struct type")
    else (
      let off = ref (int_of_string offset) in
      let pos = ref 0 in
      if !off >= Option.get (size_of_sctype real_struct) then
	let errmsg = P.sprintf "offset %s(%d) > size_of_structure %d" 
	  offset !off (Option.get (size_of_sctype real_struct))
	in
	raise (Failure errmsg)
      else (
	List.iter (fun (ft,fn) ->
	  if !off > 0 then (
	    let ftsize = Option.get (size_of_sctype ft) in
	    begin match ft with
	    | SC_array _ -> off := !off - ftsize
	    | _ ->
	      if ftsize <= 4 then 
		off := !off - 4
	      else
		off := !off - ftsize;
	    end;
	    pos := !pos + 1
	  )
	) fieldlist;
	P.printf "pos = %d; size = %d; off_remain = %d\n" !pos (List.length fieldlist) !off; flush stdout;
	find_struct_map struct_map (fst (List.at fieldlist !pos))
      )
    )
  | _ ->
    let errmsg = P.sprintf "dereference wrong type: %s" 
      (str_of_sctype sctype)
    in
    raise (Failure errmsg)
*)
(*
let rec inc_ptrlv sctype lv =
  if lv = 0 then sctype
  else
    SC_pointer (inc_ptrlv sctype (lv-1))

let rec look_for_sctype sctype ptrlv =
  if ptrlv = 0 then 
    match sctype with
    | SC_union (_,umemlist) ->
      List.flatten (List.map (fun (sct,str) -> look_for_sctype sct 0) umemlist)
    | SC_structure (_,_,fieldlist) ->
      List.flatten (List.map (fun (sct,str) -> look_for_sctype sct 0) fieldlist)
    | _ -> [sctype]
  else
    match sctype with
    | SC_typedef (_,sct) -> 
      look_for_sctype sct ptrlv
    | SC_pointer sct ->
      look_for_sctype sct (ptrlv-1)
    | SC_array (sct,_) ->
      look_for_sctype sct (ptrlv-1)
    | SC_unspecified_param -> [SC_top]
    | SC_top -> [SC_top]
    | SC_base typename -> [SC_top]
    (*
      if String.exists typename "int" then [SC_top]
      else (
      P.printf "cannot dereference %s for %d times\n" (str_of_sctype sctype) ptrlv; flush stdout;
      raise (Failure "dereference from non-dereferencable type!")
      )*)
    | _ ->
      P.printf "cannot dereference %s for %d times\n" (str_of_sctype sctype) ptrlv; flush stdout;
      raise (Failure "dereference from non-dereferencable type!")
*)
(*
let rec add_struct_map struct_map sctype =
  match sctype with
  | SC_structure (name,new_name,fieldlist) ->
    if H.mem struct_map new_name then ()
    else if List.length fieldlist > 0 then
      H.add struct_map new_name sctype;
    List.iter (fun (ft,fn) -> 
      add_struct_map struct_map ft
    ) fieldlist
  | _ -> ()
*)
(*

let rec deref_sctype struct_map sctype offset =
  let rec find_derefable sct =
    match sct with
    | SC_top -> Some SC_top
    | SC_typeVar _ -> raise (Failure "deref type variable")
    | SC_const sct' | SC_volatile sct' -> find_derefable sct'
    | SC_base str ->
      P.printf "WARNING: try to deref base type: %s\n" str;
      flush stdout;
      Some sct
    | SC_array _ | SC_pointer _ -> Some sct
    | SC_typedef (defnm,sct') ->
      if H.mem struct_map defnm then
	Some (H.find struct_map defnm)
      else
	find_derefable sct'
    | SC_structure (nm,lvnm,fdlist) ->
      if H.mem struct_map nm then
	Some (H.find struct_map nm)
      else
	raise (Failure "struct map may be incomplete")
    | _ ->
      P.printf "WARNING: try to deref type: %s\n" (str_of_sctype sct);
      flush stdout;
      None
  in

  let rec deref sct off =
    let derefable_option = find_derefable sct in
    if derefable_option = None then 
    match sct with
    | SC_top -> SC_top
    | SC_base str -> 
      P.printf "WARNING: deref base type: %s to get TOP\n" str;
      flush stdout;
      SC_top
    | SC_array (sct',dimlist) ->
      let real_sct' = find_struct_map struct_map sct' in
      if List.is_empty dimlist then 
	real_sct'
      else
	let bound = calc_array_bound dimlist in
	let sctsize = size_of_sctype struct_map real_sct' in
	let residue = off mod sctsize in
	if residue = 0 then
	  let index = off / sctsize in
	  if index < bound then
	    real_sct'
	  else
	    raise (Failure "dereference out of bound for array")
	else
	  let index = (off-residue) / sctsize in
	  if index < bound then
	    deref real_sct' residue
	  else
	    raise (Failure "deref out of bound for array with residue") 
    | SC_structure (nm,lvnm,fdlist) -> 
      if List.is_empty fdlist then 
        raise (Failure "struct map contains dummy struct type")
      else (
	let off_remain = ref off in
	let pos = ref 0 in
	if off >= size_of_sctype struct_map sct then
	  let errmsg = P.sprintf "offset %s(%d) > size_of_structure %d" 
	    offset off (size_of_sctype struct_map sct)
	  in
	  raise (Failure errmsg)
	else (
	  List.iter (fun (ft,fn) ->
	    if !off_remain > 0 then (
	      let real_ft = find_struct_map struct_map ft in
	      let ftsize = size_of_sctype struct_map real_ft in
	      begin match real_ft with
	      | SC_array _ -> off_remain := !off_remain - ftsize
	      | _ ->
		if ftsize <= 4 then 
		  off_remain := !off_remain - 4
		else
		  off_remain := !off_remain - ftsize
	      end;
	      pos := !pos + 1
	    )
	  ) fdlist;
	  if !off_remain = 0 then
	    find_struct_map struct_map (fst (List.at fdlist !pos))
	  else if !off_remain < 0 then
	    let deep_type = 
	      find_struct_map struct_map (fst (List.at fdlist (!pos-1)))
	    in
	    let deep_type_size = size_of_sctype struct_map deep_type in
	    let next_off = deep_type_size + !off_remain in
	    deref deep_type next_off
	  else
	    raise (Failure "Impossible")
	)
      )
    | SC_pointer sct' ->
      begin match sct' with
      | SC_array _ | SC_structure _ -> deref sct' off
      | _ -> 
	P.printf "WARNING: dereference pointer with offset:%d\n" off;
	flush stdout;
	sct'
      end
    | _ ->
      raise (Failure "deref non-derefable type")
  in

  if offset <> "all" && offset <> "none" then
    let off = int_of_string offset in
    let derefed = deref sctype off in
    derefed
  else if offset = "none" then
    raise (Failure "Magically, we have (1:none) edge")
  else if offset = "all" then
    begin match derefable with
    | SC_array (sct,_) 
    | SC_pointer sct -> 
      find_struct_map struct_map sct
    | _ -> SC_top
    end
  else
*)
	(*
	eqc.eqc_fflowtype <-
	  List.flatten (
	    List.map (fun (eqc',trace) ->
	      P.printf "Processing EQC:%s <-- EQC:%s\n" 
		eqc.eqc_name eqc'.eqc_name; 
	      flush stdout;
	      let processed_type = retrieve_ectype_from_eqc eqc' trace struct_map in
	      let processed_type =
		if !report_top_trace then
		  List.map (fun sctype ->
		    match sctype with
		    | SC_top pretrace -> 
			(*P.printf "%s:%s" eqc.eqc_name (str_of_trace trace); flush stdout;*)
		      SC_top (trace @ pretrace)
		    | _ -> sctype
		  ) processed_type
		else processed_type
	      in
	      processed_type
	    ) backward_reachable
	  )
	*)
