open Batteries
module P = Printf
module H = Hashtbl

open Bits
open X86Syntax
open X86Semantics
open X86_MACHINE
open X86_RTL
open Config
open Abbrev
open Elf

open BB
open RTL_opt
open Ibaux
open Config_cfg
open Printer

let report_top_trace = ref false
let report_unsupport = ref false


type tvIndex = int
  
type addrName = {disp:string;base:register option;index:(scale * register) option}

let mkADNM indisp inbase inindex =
  {disp = indisp; base = inbase; index = inindex}

type tvName = 
| TV_imm of string
| TV_reg of register
| TV_cfa of int
| TV_addr of addrName    

let is_trivial ins =
  match ins with
  | NOP _ -> true
  | JMP(_,_,_,_) | Jcc(_,_) | JCXZ(_) -> true
  | CMP _ | TEST _ -> true
  | ADD _ | SUB _ | IMUL _ | MUL _ -> true
  | BT _ | BTC _ | BTR _ | BTS _ -> true
  (*| MOVZX _ -> true*)
  (** float-point instructions **) 
  | F2XM1 
  | FABS 
  | FADD _ 
  | FADDP _ 
  | FBLD _
  | FBSTP _
  | FCHS
  | FCMOVcc _ 
  | FCOM _ 
  | FCOMP _ 
  | FCOMPP
  | FCOMIP _
  | FCOS
  | FDECSTP
  | FDIV _
  | FDIVP _
  | FDIVR _
  | FDIVRP _
  | FFREE _
  | FIADD _
  | FICOM _
  | FICOMP _
  | FIDIV _
  | FIDIVR _
  | FILD _
  | FIMUL _
  | FINCSTP
  | FIST _
  | FISTP _
  | FISUB _
  | FISUBR _
  | FLD _
  | FLD1
  | FLDCW _
  | FLDENV _
  | FLDL2E
  | FLDL2T
  | FLDLG2
  | FLDLN2
  | FLDPI
  | FLDZ
  | FMUL _
  | FMULP _
  | FNCLEX
  | FNINIT
  | FNOP
  | FNSAVE _
  | FNSTCW _
  | FNSTSW _
  | FPATAN
  | FPREM
  | FPREM1
  | FPTAN
  | FRNDINT
  | FRSTOR _
  | FSCALE
  | FSIN
  | FSINCOS
  | FSQRT
  | FST _
  | FSTENV _
  | FSTP _
  | FSUB _
  | FSUBP _
  | FSUBR _
  | FSUBRP _
  | FTST
  | FUCOM _
  | FUCOMP _
  | FUCOMPP
  | FUCOMI _
  | FUCOMIP _
  | FXAM
  | FXCH _
  | FXTRACT
  | FYL2X
  | FYL2XP1
  | FWAIT -> true
  | _ -> false

let is_evil ins =
  match ins with
  | RET(_,_) | IRET -> true
  | _ -> false

let split_at_eq re =
  match re with
  | Coq_test_rtl_exp(s,Coq_eq_op,re1,re2) ->
    begin match re1 with
    | Coq_get_loc_rtl_exp(s,Coq_reg_loc reg) ->
      reg, re2
    | _ ->
      raise (Failure "Should be no other expressions other than get_loc")
    end
  | _ ->
    raise (Failure "Should be no other expressions other than test")

let reverse_cfa_expr re =
  let cfa_ps_reg = Coq_get_ps_reg_rtl_exp(Big.of_int 31, Big.of_int (-1)) in
  match re with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    begin match re1 with
    | Coq_get_loc_rtl_exp(s, Coq_reg_loc reg) ->
      begin match bvop with
      | Coq_add_op ->
       	reg, Coq_arith_rtl_exp(s,Coq_sub_op,cfa_ps_reg,re2)
      | Coq_sub_op ->
	reg, Coq_arith_rtl_exp(s,Coq_add_op,cfa_ps_reg,re2)
      | _ ->
	raise (Failure "Should be no other operators")
      end
    | _ -> 
      raise (Failure "Should not exist things other than registers")
    end
  | Coq_get_loc_rtl_exp(s,Coq_reg_loc reg) ->
    reg, cfa_ps_reg
  | _ ->
    P.printf "%s\n" (str_of_rtl_exp_less_size re); flush stdout;
    raise (Failure "Should not exist expressions other than arith")


let transform_cfa_expr re addr_name =
  match re with
  | Coq_arith_rtl_exp(_,bvop,re1,re2) ->
    begin match re1 with
    | Coq_get_ps_reg_rtl_exp(_,ps_reg) when Big.to_int ps_reg = (-1) ->
      begin match bvop with
      | Coq_add_op ->
	begin match re2 with
	| Coq_imm_rtl_exp (_,v) -> 
	  TV_cfa (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int v)))
	| _ -> 
	  TV_addr addr_name
	end
      | Coq_sub_op ->
	begin match re2 with
	| Coq_imm_rtl_exp (_,v) -> 
	  TV_cfa (-(Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int v))))
	| _ -> 
	  TV_addr addr_name
	end
      | _ -> TV_addr addr_name
      end
    | _ -> TV_addr addr_name
    end
  | Coq_get_ps_reg_rtl_exp(_,ps_reg) when Big.to_int ps_reg = (-1) ->
    TV_cfa 0
  | _ -> TV_addr addr_name

let rtl_add_big re bigv =
  simp_re simpre_all_methods (Coq_arith_rtl_exp(Big.of_int 31, Coq_add_op, re, Coq_imm_rtl_exp(Big.of_int 31, bigv)))

let tvNameGen funCFA op l =
  match op with
  | Imm_op imm -> TV_imm (str_of_mword_flex (MWord.of_big_int imm))
  | Reg_op reg -> TV_reg reg
  | Address_op addr ->
    let addr_name = {
      disp= str_of_mword_flex (MWord.of_big_int (addrDisp addr));
      base= addrBase addr;
      index= addrIndex addr} 
    in 
    begin match addr with
    | {addrDisp = big_v; addrBase = Some reg; addrIndex = None} ->
      let _, l_cfa =
	try
	  List.find (fun (lopc,cfaexp) ->
	    lopc = l
	  ) funCFA
	with Not_found ->
	  P.printf "not found cfaexp at %x\n" l; flush stdout;
	  List.last funCFA
	  (*raise (Failure "in tvNameGen_naive")*)
      in
      let cfaoff = List.map (fun (lvl,re) ->
	if lvl > 0 then split_at_eq re
	else 
	  reverse_cfa_expr re
      ) l_cfa 
      in
      begin 
	try 
	  let _, reg_in_cfa = List.find (fun (register, re) ->
	    register = reg
	  ) cfaoff
	  in
	  let op_cfa_expr = rtl_add_big reg_in_cfa big_v in
	  transform_cfa_expr op_cfa_expr addr_name
	with Not_found ->
	  TV_addr addr_name 
      end
    | _ ->
      TV_addr addr_name
    end
  | Offset_op off ->
    TV_addr {disp = str_of_mword_flex (MWord.of_big_int off); base = None; index = None}

type typeVar = {
  tv_name: tvName;
  tv_index: tvIndex;
  tv_crossbb: bool;
}
(*
type flow_edge = {
  fe_type: int;
  fe_offset: str;
}
*)

exception Boring_absval

(* Edge type: 0 for normal; 1 for dereference; 2 for reference *)
let mkTV tvnm tvndx tvcros =
  (** add another layer to transform TV_addr names **)
  match tvnm with
  | TV_addr addrnm ->
    begin match addrnm with
    | {disp=off_str;base=Some reg;index=None} ->
      let tv = { 
	tv_name = TV_reg reg;
	tv_index = tvndx;
	tv_crossbb = tvcros;
      }
      in
      tv, (1,off_str)
    | {disp=off_str;base=Some reg;index=Some (scl,ndxreg)} ->
      let tv = {
	tv_name = TV_reg reg;
	tv_index = tvndx;
	tv_crossbb = tvcros;
      }
      in
      let offrange_str = "all" in 
      (** A better precision may be achieved by assuming ndxregs only contain
	  positive numbers **)
      tv, (1,offrange_str)
    | {disp=off_str; base=None; index=Some (scl,ndxreg)} ->
      tvnm
    | _ ->
      tvnm
  in
  let inner_tvNameGen funCFA op l =
    trans_tvName (tvNameGen funCFA op l)
  in
  List.unique (
    List.flatten (List.map (fun bb ->  
      let addrcount = ref bb.bb_relAddr in
      let l_instr_list = 
	List.map (fun (pre,ins,len) -> 
	  let outaddr = !addrcount in
	  addrcount := len + !addrcount;
	  outaddr, (pre,ins,len)
	) bb.bb_instrs 
      in
      List.flatten (List.map (fun (l, (pre,ins,len)) ->
	if is_trivial ins then []
	else match ins with
	| PUSH(width,op) ->
	  let tvNameOp = inner_tvNameGen funCFA op l in
	  let currstack = Address_op 
	    {addrDisp=Big.zero; addrBase=Some ESP; addrIndex=None;}
	  in
	  let tvNameCurrStack = inner_tvNameGen funCFA currstack (l+len) in
	  [tvNameOp;tvNameCurrStack]
	| MOV(width,opd,ops) -> 
	  let tvNameOpd = inner_tvNameGen funCFA opd l in
	  let tvNameOps = inner_tvNameGen funCFA ops l in
	  [tvNameOpd;tvNameOps]
	| POP(op) -> 
	  let tvNameOp = inner_tvNameGen funCFA op l in
	  let currstack = Address_op 
	    {addrDisp=Big.zero; addrBase=Some ESP; addrIndex=None;}
	  in
	  let tvNameCurrStack = inner_tvNameGen funCFA currstack (l+len) in
	  [tvNameOp;tvNameCurrStack]
	| LEAVE -> 
	  let ops = Reg_op EBP in
	  let opd = Reg_op ESP in
	  let tvNameOps = inner_tvNameGen funCFA ops l in
	  let tvNameOpd = inner_tvNameGen funCFA opd (l+len) in
	  (*let op = ops in
	  let tvNameOp = inner_tvNameGen funCFA op (l+len) in*)
	  let currstack = Address_op 
	    {addrDisp=Big.of_int (-4); addrBase=Some ESP; addrIndex=None;}
	  in
	  let tvNameCurrStack = inner_tvNameGen funCFA currstack (l+len) in
	  [tvNameOps;tvNameOpd;tvNameCurrStack]
	| ADD(width,opd,ops) -> 
	  let tvNameOpd = inner_tvNameGen funCFA opd l in
	  [tvNameOpd]
	| SUB(width,opd,ops) -> 
	  let tvNameOpd = inner_tvNameGen funCFA opd l in
	  [tvNameOpd]
	| LEA(opd,ops) -> 
	  let tvNameOpd = inner_tvNameGen funCFA opd l in
	  let tvNameOps = inner_tvNameGen funCFA ops l in
	  [tvNameOpd;tvNameOps]
	| CMP(width,ops1,ops2) -> 
	  let tvNameOps1 = inner_tvNameGen funCFA ops1 l in
	  let tvNameOps2 = inner_tvNameGen funCFA ops2 l in
	  [tvNameOps1;tvNameOps2]
	| CALL(dis,abs,op,slctr) ->
	  begin match op with
	  | Imm_op _ -> []
	  | _ ->
	    let tvNameOp = inner_tvNameGen funCFA op l in
	    [tvNameOp]
	  end
	| JMP(dis,abs,op,slctr) ->
	  begin match op with
	  | Imm_op _ -> []
	  | _ ->
	    let tvNameOp = inner_tvNameGen funCFA op l in
	    [tvNameOp]
	  end
	| TEST(width,ops1,ops2) ->
	  let tvNameOps1 = inner_tvNameGen funCFA ops1 l in
	  let tvNameOps2 = inner_tvNameGen funCFA ops2 l in
	  [tvNameOps1;tvNameOps2]
	| _ -> []
      ) l_instr_list)
    ) asm_fun.func_bbs)
  )

let define_worth_tracking funCFA asm_fun =
  let registers = [EAX;ECX;EDX;EBX;ESI;EDI;ESP;EBP] in
  let regs_tvnms = List.map (fun reg -> TV_reg reg) registers in
  let stack_tvnms = [] in
  regs_tvnms @ stack_tvnms

type range = 
| R_all
| R_range of int * int
    
let in_range addr range =
  match range with
  | R_all -> true
  | R_range(lo,hi) -> 
    (* WARNING: range may overlap from debugging information *)
    lo <= addr && addr < hi

let str_of_range rng =
  match rng with
  | R_all -> "all"
  | R_range(lo,hi) ->
    P.sprintf "(%x,%x)" lo hi

type dbg_type_info = (tvName * range * extended_sctype) list

let dump_dbgTypeInfo dbgTypeInfo =
  List.iter (fun (asm_fun, dbgtype) ->
    P.printf "Function %s:\n" asm_fun.func_name;
    List.iter (fun (tvnm,rng,ectype) ->
      P.printf "%s %s\n%s\n" (str_of_tvnm tvnm) (str_of_range rng) (str_of_ectype ectype)
    ) dbgtype
  ) dbgTypeInfo

type type_relation = 
  {
    mutable tr_var: typeVar;
    mutable tr_val: extended_sctype;
  }

let mkTR trVar trVal = {tr_var = trVar; tr_val = trVal}

type type_env = type_relation list

let dump_type_env tenv =
  P.printf "Type Environment:\n";
  List.iter (fun tr ->
    P.printf "%s = %s\n" (str_of_tv tr.tr_var) (str_of_ectype tr.tr_val)
  ) tenv
    
let collect_visible_typeinfo elf target_name =
  let start = MWord.of_string "0x8048000" in
  let resultdir = P.sprintf "%s_type" elf.fname in
  let resultfilename = P.sprintf "%s/visible_typeinfo_%s.txt" resultdir target_name in
  let void = C_base "void" in
  let size_t = C_typedef ("size_t", C_base "unsinged int") in
  let ptr ct = C_pointer ct in
  let fp ret paramlst= C_subroutine (ret, paramlst) in
  if target_name = "deregister_tm_clones" then
    "", [mkvfp (fp void [ptr void]) VS_unknown []]
  else if target_name = "register_tm_clones" then
    "", [mkvfp (fp void [ptr void;size_t ]) VS_unknown []]
  else if target_name = "frame_dummy" then
    let fstvfp = mkvfp (fp void [void]) VS_unknown [] in
    let sndvfp = mkvfp (fp void [ptr void]) VS_unknown [] in
    "", [fstvfp; sndvfp]
  else if Sys.file_exists elf.fname then (
    let cmd = 
      P.sprintf "./dbg/collect_visible_typeinfo %s %s > %s" elf.fname target_name resultfilename 
    in
    (*P.printf "collect elf.fname %s: %s\n" elf.fname target_name;*) 
    let exit_code = if not (Sys.file_exists resultfilename) then Sys.command cmd else -1 in
    (*P.printf "exit_code: %d\n" exit_code; *)
    if exit_code <> (-1) then
      P.printf "collect elf.fname %s: %s with exit code -1\n" elf.fname target_name;
    flush stdout;
    let ic = open_in resultfilename in
    let vfps = ref [] in
    let sinfo_count = ref 0 in
    let frame_base = ref "none" in
    let () = 
      try
	let frame_base_indicator = input_line ic in
	if frame_base_indicator = "Frame base" then
	  frame_base := input_line ic;
	while true do 
	  sinfo_count := !sinfo_count + 1;
	  let visisrc_e = input_line ic in
	  let vs = decode_visisrc visisrc_e in
	  let indicator = input_line ic in
	  let ctype = 
	    try decode_c_type indicator 
	    with WrongTypeEncoding ->
	      let () = 
		P.printf "Error! Now in file %s, decoding number %d sinfo entry!\n" resultfilename !sinfo_count; 
		flush stdout
	      in 
	      C_wrong_type "debugging"
	  in
	  let sinfo_start = input_line ic in
	  if sinfo_start <> "{" then (
	    print_endline "Error: unknown encoding after type encoding\n"; 
	    P.printf "in file %s at entry %d\n" resultfilename !sinfo_count;
	    flush stdout; 
	    exit 1)
	  else
	    let first_line = ref (input_line ic) in
	    let locdesc_list = ref [] in
	    while !first_line <> "}" do
	      if !first_line = "NULL" then (
		let vfp_entry = mkvfp ctype vs [] in
		vfps := !vfps @ [vfp_entry];
		first_line := input_line ic
	      )
	      else (
		let lopc_e = !first_line in
		let hipc_e = input_line ic in
		let lopc = MWord.of_string lopc_e in
		let hipc = MWord.of_string hipc_e in
		let lopc_rel =
		  if MWord.(>=%) lopc start then get_relative_addr lopc start 
		  else MWord.to_int lopc
		in
		let hipc_rel =
		  if MWord.(>=%) hipc start then get_relative_addr hipc start 
		  else MWord.to_int hipc
		in
		(* 
		   let le_start = input_line ic in
		   if le_start <> "Le:" then ( simple_warn "Error: unknown things after location expression start string, le"; exit 1 )
		   else
		*)
		let locexpr_list = ref [] in
		let curr_expr = ref (input_line ic) in
		while !curr_expr <> "" do
		  locexpr_list := !locexpr_list @ [!curr_expr];
		  curr_expr := input_line ic
		done;
		let locdesc_entry = mkdesc lopc_rel hipc_rel !locexpr_list in
		locdesc_list := !locdesc_list @ [locdesc_entry];
		first_line := input_line ic
	      )
	    done;
	    let vfp_entry = mkvfp ctype vs !locdesc_list in
	    vfps := !vfps @ [vfp_entry]
	done
      with End_of_file ->
	close_in ic
    in
    (*let () = List.iter print_vfp !vfps in*)
    (*let _ = Sys.command (P.sprintf "rm %s" resultfilename) in*)
    !frame_base,!vfps
  )
  else (
    P.printf "No file %s exists!" resultfilename; flush stdout;
    ("none", [])
  )

let interp_locexprs frame_base funCFA locexprs range =
  if List.mem "DW_OP_stack_value" locexprs then None
  else if List.length locexprs <> 1 then None (* do not support currently *)
  else 
    let locexpr = List.hd locexprs in
    if String.exists locexpr "DW_OP_reg" then
      let op = String.strip locexpr in
      match op with
      | "DW_OP_reg0" -> Some (TV_reg EAX)
      | "DW_OP_reg1" -> Some (TV_reg ECX)
      | "DW_OP_reg2" -> Some (TV_reg EDX)
      | "DW_OP_reg3" -> Some (TV_reg EBX)
      | "DW_OP_reg4" -> Some (TV_reg ESP)
      | "DW_OP_reg5" -> Some (TV_reg EBP)
      | "DW_OP_reg6" -> Some (TV_reg ESI)
      | "DW_OP_reg7" -> Some (TV_reg EDI)
      | _ -> None
    else if String.exists locexpr "DW_OP_fbreg" then
      let op,off_str = String.split locexpr " " in
      let off_big_v = Big.of_int (int_of_string off_str) in
      let change_frame_base fbreg funCFA =
	let lvre_list = List.flatten ( 
	  List.filter_map (fun (relAddr,lvre_list) ->
	    if in_range relAddr range then
	      Some lvre_list
	    else None
	  ) funCFA
	)
	in
	let cfa_name_list = List.filter_map (fun (lv,re) -> 
	  let reg, reg_in_cfa = if lv > 0 then split_at_eq re
	    else reverse_cfa_expr re
	  in
	  if reg = fbreg then
	    let op_cfa_expr = rtl_add_big reg_in_cfa off_big_v in
	    let addr_name = mkADNM off_str (Some fbreg) None in
	    Some (transform_cfa_expr op_cfa_expr addr_name)
	  else
	    None
	) lvre_list
	in
	let unique_name_list = List.unique cfa_name_list in
	if List.length unique_name_list = 1 then
	  Some (List.hd unique_name_list)
	else
	  let errmsg = P.sprintf "%s frame base is not uniformed" (str_of_reg fbreg) in 
	  raise (Failure errmsg)
      in
      match frame_base with
      | "DW_OP_call_frame_cfa" -> 
	let off = int_of_string off_str in
	Some (TV_cfa off)
      (** TODO: check correctness: interp different frame base into CFA **)
      (** (relAddr * ((level * rtl_exp) list)) list **)
      | "DW_OP_breg0" -> 
	(*let addrname = mkADNM off_str (Some EAX) None in*)
	change_frame_base EAX funCFA
      | "DW_OP_breg1" -> 
	change_frame_base ECX funCFA
      | "DW_OP_breg2" -> 
	change_frame_base EDX funCFA
      | "DW_OP_breg3" -> 
	change_frame_base EBX funCFA
      | "DW_OP_breg4" -> 
	change_frame_base ESP funCFA
      | "DW_OP_breg5" ->
	change_frame_base EBP funCFA
      | "DW_OP_breg6" ->
	change_frame_base ESI funCFA
      | "DW_OP_breg7" -> 
	change_frame_base EDI funCFA
      | _ -> None
    else if String.exists locexpr "DW_OP_breg" then
      (** OPTIMIZE: such situation may be useless **)
      let op,off_str = String.split locexpr " " in
      match op with
      | "DW_OP_breg0" -> 
	let addrname = mkADNM off_str (Some EAX) None in
	Some (TV_addr addrname)
      | "DW_OP_breg1" -> 
	let addrname = mkADNM off_str (Some ECX) None in
	Some (TV_addr addrname)
      | "DW_OP_breg2" -> 
	let addrname = mkADNM off_str (Some EDX) None in
	Some (TV_addr addrname)
      | "DW_OP_breg3" -> 
	let addrname = mkADNM off_str (Some EBX) None in
	Some (TV_addr addrname)
      | "DW_OP_breg4" -> 
	let addrname = mkADNM off_str (Some ESP) None in
	Some (TV_addr addrname)
      | "DW_OP_breg5" ->
	let addrname = mkADNM off_str (Some EBP) None in
	Some (TV_addr addrname)
      | "DW_OP_breg6" ->
	let addrname = mkADNM off_str (Some ESI) None in
	Some (TV_addr addrname)
      | "DW_OP_breg7" -> 
	let addrname = mkADNM off_str (Some EDI) None in
	Some (TV_addr addrname)
      | _ -> None
    else if String.exists locexpr "DW_OP_addr" then
      let op,addr = String.split locexpr " " in
      let abs_addr_str = str_of_mword_flex (MWord.of_string addr) in
      let addrname = mkADNM abs_addr_str None None in
      Some (TV_addr addrname)
    else 
      None

let trans_raw_dbgtypeinfo frame_base funCFA raw_dbg =
  List.filter_map (fun (locexprs,rng,sctype) ->
    let range = 
      if snd rng = (-1) then R_all
      else R_range ((fst rng),(snd rng))
    in
    let loc = interp_locexprs frame_base funCFA locexprs range in
    if Option.is_some loc then
      Some (Option.get loc,range,[sctype])
    else None
  ) raw_dbg

let rec ctype_to_sctype ctype = 
  match ctype with
  | C_base name -> SC_base name 
  | C_const ct -> SC_const (ctype_to_sctype ct)
  | C_array (ct,dimlst) -> 
    SC_array ((ctype_to_sctype ct),dimlst)
  | C_pointer ct -> 
    SC_pointer (ctype_to_sctype ct)
  | C_typedef (typedef_name, ct) ->
    SC_typedef (typedef_name,(ctype_to_sctype ct))
  | C_subroutine (ret_ct, param_ct_list) ->
    SC_subroutine ((ctype_to_sctype ret_ct), (List.map ctype_to_sctype param_ct_list))
  | C_enumeration (enumname, etor_list) -> 
    SC_enumeration (enumname, etor_list)
  | C_union (unionname, umem_list) -> 
    SC_union (unionname, (List.map (fun (umemtype,umemname) -> (ctype_to_sctype umemtype), umemname) umem_list))
  | C_structure (original_name, level_name, smem_list) ->
    SC_structure (original_name, level_name, (List.map (fun (smemtype,smemname) -> (ctype_to_sctype smemtype), smemname) smem_list))
  | C_unspecified_param ->
    SC_unspecified_param
  | C_wrong_type errmsg ->
    SC_wrong_type errmsg
  | C_volatile ct ->
    SC_volatile (ctype_to_sctype ct)

let collect_func_tyinfo elf =
  let start = MWord.of_string "0x8048000" in
  let all_functions = collect_all_functions elf start in
  let funcsig_list_raw = collect_funcsig_list elf start in
  let funcsig_list = configure_funcsig_list_lowpc funcsig_list_raw all_functions in
  List.flatten (
    List.map (fun funcsig ->
      let rng = R_all in
      let ret_ctype = decode_c_type funcsig.func_ret in
      let param_ctypes = List.map decode_c_type funcsig.func_param in
      let ret_sctype = ctype_to_sctype ret_ctype in
      let param_sctypes = List.map ctype_to_sctype param_ctypes in
      let sctype = SC_pointer (SC_subroutine (ret_sctype, param_sctypes)) in
      let func_abs_lopc =
	str_of_mword_flex (MWord.(+%) (MWord.of_int funcsig.func_rel_lowpc) start)
      in
    (*
      let locexprs =
      P.sprintf "DW_OP_addr %s" func_abs_lopc
      in
    *)
      [((TV_imm func_abs_lopc), rng, [sctype]); ((TV_addr (mkADNM func_abs_lopc None None)), rng, [sctype])]
    ) funcsig_list
  )

let retrieve_dbgtypeinfo elf funCFA asm_fun_name =
  let frame_base, visible_tyinfo = collect_visible_typeinfo elf asm_fun_name in
  let raw_dbgtypeinfo = 
    List.flatten (
      List.map (fun vfp ->
	let ctype = vfp.vfp_tsig in
	let sctype = ctype_to_sctype ctype in
	let range_and_location =
	  List.map (fun locdesc -> 
	    let range = (locdesc.ld_lopc, locdesc.ld_hipc) in
	    range, locdesc.ld_locexprs
	  ) vfp.vfp_sinfo
	in
	List.map (fun (rng,locexprs) ->
	  locexprs,rng,sctype
	) range_and_location
      ) visible_tyinfo
    )
  in
  let func_tyinfo = collect_func_tyinfo elf in
  let dbgtypeinfo = trans_raw_dbgtypeinfo frame_base funCFA raw_dbgtypeinfo in
  dbgtypeinfo @ func_tyinfo

let enrich_struct_map struct_map dbgTypeInfo memmap =
  let rec add_struct sctype =
    match sctype with
    | SC_typedef (defnm,sct) ->
      begin match sct with
      | SC_structure (oldnm,_,fieldlist) ->
	if not (List.is_empty fieldlist) then (
	  if not (H.mem struct_map defnm) then
	    let _,size,memoff = 
	      try
		List.find (fun (nm,sz,memoff) ->
		  nm = defnm
		) memmap 
	      with Not_found ->
		let errmsg = P.sprintf "struct %s not found!" defnm in
		raise (Failure errmsg)
	    in
	    let newstruct = mkSTRUCT sct size memoff in 
	    H.add struct_map defnm newstruct
	  else if oldnm <> "" && not (H.mem struct_map oldnm) then
	    let _,size,memoff =
	      try
		List.find (fun (nm,sz,memoff) ->
		  nm = oldnm
		) memmap
	      with Not_found ->
		let errmsg = P.sprintf "struct %s not found!" oldnm in
		raise (Failure errmsg)
	    in
	    let newstruct = mkSTRUCT sct size memoff in 
	    H.add struct_map oldnm newstruct
	);
	List.iter (fun (ft,fn) -> add_struct ft) fieldlist
      | _ -> 
	add_struct sct
      end
    | SC_structure (oldnm,_,fieldlist) ->
      if oldnm <> "" then (
	if not ((List.is_empty fieldlist) || (H.mem struct_map oldnm)) then
	  let _,size,memoff = 
	    try
	      List.find (fun (nm,sz,memoff) ->
		nm = oldnm
	      ) memmap 
	    with Not_found ->
	      let errmsg = P.sprintf "struct %s not found!" oldnm in
	      raise (Failure errmsg)
	  in
	  let newstruct = mkSTRUCT sctype size memoff in
	  H.add struct_map oldnm newstruct
      );
      List.iter (fun (ft,fn) -> add_struct ft) fieldlist
    | SC_const sct | SC_volatile sct
    | SC_array (sct,_) | SC_pointer sct ->
      add_struct sct
    | SC_subroutine (ret,paramlist) ->
      add_struct ret;
      List.iter (fun ft -> add_struct ft) paramlist
    | SC_union (_,memlist) ->
      List.iter (fun (ft,fn) -> add_struct ft) memlist
    | _ -> ()
  in
  List.iter (fun (tvnm,rng,ectype) ->
    List.iter (fun sctype ->
      add_struct sctype
    ) ectype
  ) dbgTypeInfo

(*type struct_map = (string,struct_type) H.t*)

let collect_memmap elf =
  let resultdir = P.sprintf "%s_type" elf.fname in
  let resultfilename = P.sprintf "%s/struct_memmap.txt" resultdir in
  let cmd = 
    P.sprintf "./dbg/collect_struct_memmap %s > %s" elf.fname resultfilename
  in
  let _ = if not (Sys.file_exists resultfilename) then Sys.command cmd else -1 in
  let ic = open_in resultfilename in
  let memmap = ref [] in
  let currnm = ref "" in
  let currsz = ref 0 in
  let currmemoff = ref [] in
  let () = 
    try
      while true do
	let line = input_line ic in
	if String.exists line ":" then (
	  if !currnm <> "" && !currsz <> 0 then (
	    memmap := (!currnm,!currsz,!currmemoff) :: !memmap;
	    currmemoff := []
	  );
	  let nm,sz = String.split line ":" in
	  currnm := nm;
	  currsz := int_of_string sz
	) else
	  let off = int_of_string line in
	  currmemoff := !currmemoff @ [off]
      done
    with End_of_file ->
      if !currnm <> "" && !currsz <> 0 then (
	memmap := (!currnm,!currsz,!currmemoff) :: !memmap;
	currmemoff := []
      ); 
      close_in ic
  in
  (*
  let rmcmd = P.sprintf "rm %s" resultfilename in
  let _ = Sys.command rmcmd in*)
  (*List.iter (fun (nm,sz,memoff) -> 
    P.printf "%s:%d {\n" nm sz;
    List.iter (fun off -> 
      P.printf "%d\n" off
    ) memoff;
    P.printf "}\n"
  ) !memmap;*)
  !memmap

let dbgTypeGen elf cfaInfo asm_fun_list =
  let global_struct_map = H.create 32 in
  let memmap = collect_memmap elf in
  List.map (fun asm_fun ->
    let _,funCFA = List.find (fun (func,cfa_repr) ->
      func.func_name = asm_fun.func_name
    ) cfaInfo 
    in
    let asm_fun_dbg = retrieve_dbgtypeinfo elf funCFA asm_fun.func_name in
    enrich_struct_map global_struct_map asm_fun_dbg memmap;
    asm_fun, asm_fun_dbg
  ) asm_fun_list, global_struct_map

let dbgtype_tag tv dbgTypeInfo =
  if not tv.tv_crossbb then (
    let tag =
      match tv.tv_name with
      | TV_imm _ | TV_reg _ | TV_addr _ ->
	List.unique ?eq:(Some eq_sctype) (
	  List.flatten (
	    List.filter_map (fun (tvnm,rng,ectype) ->
	      if tvnm = tv.tv_name && in_range tv.tv_index rng then
		Some ectype
	      else 
		None
	    ) dbgTypeInfo
	  )
	)
      | TV_cfa off ->
	List.unique ?eq:(Some eq_sctype) (
	  List.flatten (
	    List.filter_map (fun (tvnm,rng,ectype) ->
	      if in_range tv.tv_index rng then (
		match tvnm with
		| TV_cfa off' ->
		  if off < off' then None
		  else if off = off' then Some ectype
		  else (
		    let array_types = List.filter (fun sctype ->
		      match sctype with
		      | SC_array _ -> true
		      | _ -> false
		    ) ectype
		    in
		    if List.length array_types > 1 then
		      raise (Failure "Type conflict with arrays")
		    else if List.is_empty array_types then None
		    else
		      let array_type = List.hd array_types in
		      match array_type with
		      | SC_array (sct,dimlist) ->
			let size = List.fold_right (fun dim mult ->
			  dim * mult
			) dimlist 1
			in
			let resi = (off - off') mod 4 in
			if resi = 0 && ((off - off')/4) <= size then
			  Some [sct]
			else
			  None
		      | _ -> None
		  )
		| _ -> None
	      )
	      else None
	    ) dbgTypeInfo
	  )
	)
    in
    tag
  )
  else []
(* TODO: do this later
    | TV_addr addrnm ->
      let abs_addr = 
	try
	  int_of_string ("0x"^addrnm.disp)
	with Failure errmsg ->
	  let newerr = P.sprintf "%s:%s\n" errmsg addrnm.disp in
	  raise (Failure newerr)
      in
      List.unique ?eq:(Some eq_sctype) (
	List.flatten (
	  List.filter_map (fun (tvnm,rng,ectype) ->
	    match tvnm with
	    | TV_addr addrnm' ->
	      begin match addrnm' with
	      | {disp=abs_addr_str;base=None;index=None}
	      | {disp=abs_addr_str;base=None;index=Some _}
	      | {disp=abs_addr_str;base=Some _;index=Some _} ->
		let abs_addr' = 
		  try
		    int_of_string ("0x"^addrnm'.disp)
		  with Failure errmsg ->
		    let newerr = P.sprintf "%s:%s\n" errmsg addrnm'.disp in
		    raise (Failure newerr)
		in
		if abs_addr < abs_addr' then None
		else (
		  let array_types = List.filter (fun sctype ->
		    match sctype with
		    | SC_array _ -> true
		    | _ -> false
		  ) ectype
		  in
		  if List.length array_types > 1 then
		    raise (Failure "Type conflict with arrays")
		  else if List.is_empty array_types then None
		  else
		    let array_type = List.hd array_types in
		    match array_type with
		    | SC_array (sct,dimlist) ->
		      let size = List.fold_right (fun dim mult ->
			dim * mult
		      ) dimlist 1
		      in
		      let resi = (abs_addr - abs_addr') mod 4 in
		      if resi = 0 && ((abs_addr - abs_addr')/4) <= size then
			Some [sct]
		      else
			None
		    | _ -> None
		)
	      | _ -> None
	      end
	    | _ -> None
  ) dbgTypeInfo
  )
      )
*)

let deref_sctype struct_map sctype offset =
  let rec find_derefable sct =
    match sct with
    | SC_top _ -> sct
    | SC_typeVar _ -> raise (Failure "deref type variable")
    | SC_const sct' | SC_volatile sct' -> find_derefable sct'
    | SC_base str ->
      raise Not_found
    | SC_array _ | SC_pointer _ -> sct
    | SC_typedef (defnm,sct') ->
      if H.mem struct_map defnm then
	SC_typedef (defnm, (H.find struct_map defnm).struct_type)
      else
	find_derefable sct'
    | SC_structure (nm,lvnm,fdlist) ->
      if not (List.is_empty fdlist) then sct
      else if H.mem struct_map nm then
        (H.find struct_map nm).struct_type
      else
	raise (Failure "struct map may be incomplete")
    | _ ->
      (*
      let errmsg = P.sprintf "WARNING: try to deref type: %s\n" (str_of_sctype sct) in
      P.printf "%s" errmsg; flush stdout;
      sct*)
      raise Not_found
  in
  
  let find_memoff memoff off =
    let rec aux memoff off ndx =
      match memoff with
      | [] -> 
	raise (Failure "a structure does not have member!")
      | h :: [] -> ndx, h
      | h1 :: h2 :: t ->
	if h1 <= off && off < h2 then
	  ndx, h1
	else
	  aux (h2 :: t) off (ndx+1)
    in
    aux memoff off 0
  in
  
  let rec deref sct off = 
    match sct with
    | SC_top _ -> sct
    | SC_base str ->
      let errmsg =
	P.sprintf "WARNING: deref base type: %s\n" str
      in
      (*flush stdout;*)
      raise (Failure errmsg)
    | SC_const sct' | SC_volatile sct' -> 
      (*let target_sct =
	try
	find_derefable sct 
	with Not_found ->
	let errmsg = P.sprintf "derefarable not found in %s" (str_of_sctype sctype) in
	raise (Failure errmsg)
	in*)
      deref sct' off
    | SC_typedef (defnm,sct') ->
      if H.mem struct_map defnm then
	let target_struct = H.find struct_map defnm in
	begin match target_struct.struct_type with
	| SC_structure (oldnm,lvnm,fdlist) ->
	  if List.is_empty fdlist then
	    raise (Failure "struct map contains dummy typedef struct type")
	  else
	    let target_size = target_struct.struct_size in
	    let target_memoff = target_struct.struct_memoff in
	    if off >= target_size then
	      let errmsg = P.sprintf "offset %s(%d) : size_of_structure %d" 
		offset off target_size
	      in
	      P.printf "WARNING:%s\n" errmsg; flush stdout;
	      let real_off = off - target_size in
	      deref sct real_off
	    else if off < 0 then
	      let errmsg = P.sprintf "offset %s(%d) : size_of_structure %d" 
		offset off target_size
	      in
	      P.printf "WARNING:%s\n" errmsg; flush stdout;
	      let real_off = off + target_size in
	      deref sct real_off
	    else
	      let pos, posoff = find_memoff target_memoff off in
	      if posoff = off then
		fst (List.nth fdlist pos)
	      else if posoff < off then
		let inner_type = fst (List.nth fdlist pos) in
		let next_off = off - posoff in
		deref inner_type next_off
	      else
		raise (Failure "Impossible")
	| _ ->
	  raise (Failure "Magically we have non-struct in struct map")
	end
      else
	(*
	  let target_sct = 
	  try
	  find_derefable sct 
	  with Not_found ->
	  let errmsg = P.sprintf "derefarable not found in %s" (str_of_sctype sctype) in
	  raise (Failure errmsg)
	  in*)
	deref sct' off
    | SC_array (sct',dimlist) -> 
      if List.is_empty dimlist then 
	sct'
      else
	let bound = calc_array_bound dimlist in
	let sctsize = size_of_sctype struct_map sct' in
	let residue = off mod sctsize in
	if residue = 0 then
	  let index = off / sctsize in
	  if index < bound then
	    sct'
	  else
	    raise (Failure "dereference out of bound for array")
	else
	  let index = (off-residue) / sctsize in
	  if index < bound then
	    deref sct' residue
	  else
	    raise (Failure "deref out of bound for array with residue") 
    | SC_structure (nm,_,_) ->
      if H.mem struct_map nm then (
	let target_struct = H.find struct_map nm in
	match target_struct.struct_type with
	| SC_structure (_,_,fdlist) ->
	  if List.is_empty fdlist then 
	    raise (Failure "struct map contains dummy struct type")
	  else
	    let target_size = target_struct.struct_size in
	    let target_memoff = target_struct.struct_memoff in
	    if off >= target_size then
	      let errmsg = P.sprintf "offset %s(%d) >= size_of_structure %d" 
		offset off target_size
	      in
	      P.printf "WARNING:%s\n" errmsg; flush stdout;
	      let real_off = off - target_size in
	      deref sct real_off
	    else if off < 0 then
	      let errmsg = P.sprintf "offset %s(%d) < size_of_structure %d" 
		offset off target_size
	      in
	      P.printf "WARNING:%s\n" errmsg; flush stdout;
	      let real_off = off + target_size in
	      deref sct real_off
	    else
	      let pos, posoff = find_memoff target_memoff off in
	      if posoff = off then
		fst (List.nth fdlist pos)
	      else if posoff < off then
		let inner_type = fst (List.nth fdlist pos) in
		let next_off = off - posoff in
		deref inner_type next_off
	      else
		raise (Failure "Impossible")
	| _ ->
	  raise (Failure "Magically we have non-struct in struct map")
      )
      else
	raise (Failure "met a structure with no name!")
    | SC_pointer sct' -> (
      try
	let target_sct' = find_derefable sct' in
	begin match target_sct' with
	| SC_typedef _ 
	| SC_structure _ -> deref target_sct' off
	| _ -> sct'
	end
      with Not_found ->
	sct'
    )
    | _ ->
      let errmsg = 
	P.sprintf "deref non-derefable type: %s with off: %d" 
	  (str_of_sctype sct) off 
      in 
      raise (Failure errmsg) 
  in
  
  let rec deref_all sct =
    (*let target_sct = 
      try
	find_derefable sct 
      with Not_found ->
	let errmsg = P.sprintf "derefarable not found in %s" (str_of_sctype sctype) in
	raise (Failure errmsg)
    in*)
    match sct with
    | SC_top _ -> sct
    | SC_base str ->
      P.printf "WARNING: deref_all base type: %s\n" str; flush stdout;
      SC_top []
    | SC_const sct' | SC_volatile sct' -> deref_all sct'
    | SC_typedef (defnm,sct') ->
      if H.mem struct_map defnm then SC_top []
      else deref_all sct'
    | SC_array (sct',_) -> sct'
    | SC_structure (nm,lvnm,fdlist) -> SC_top []
    | SC_pointer sct' -> (
      try
	let target_sct' = find_derefable sct' in
	begin match target_sct' with
	| SC_typedef _
	| SC_structure _ -> deref_all target_sct'
	| _ -> sct'
	end	
      with Not_found ->
	sct'
    )
    | _ ->
      raise (Failure "deref_all non-derefable type") 
  in

  if offset <> "all" && offset <> "none" then
    let off = Int32.to_int (Int32.of_string ("0x"^offset)) in
    deref sctype off
  else if offset = "all" then
    deref_all sctype
  else 
    let errmsg = "Magically, we have off string " ^ offset in
    raise (Failure errmsg)

let reason_ectype ectype edgestack struct_map =
  if List.is_empty ectype || eq_ectype ectype [SC_top []] then
    [SC_top []]
  else (
    List.fold_left (fun ect edge ->
      let edgetype, offset = edge in
      if edgetype = 0 then
	ect
      else if edgetype = 2 then
	List.map (fun sctype ->
	  SC_pointer sctype
	) ect
      (*raise (Failure "Magical edgetype: 2")*)
      else
	List.filter_map (fun sctype ->
	  try 
	    Some (deref_sctype struct_map sctype offset)
	  with Failure errmsg ->
	    P.printf "%s" errmsg; flush stdout;
	    P.printf "aiming at sctype:%s with offset:%s\n" (str_of_sctype sctype) offset;
	    flush stdout;
	    None
	) ect
    ) ectype edgestack					     
  )
      
let eqcg_search eqcg struct_map target_list = 
  List.map (fun target ->
    let tv, edge = target in
    let tv_eqc = eqcg_find eqcg tv in
    let tv_type = 
      if List.is_empty tv_eqc.eqc_dbgtype then
	reason_ectype tv_eqc.eqc_fflowtype [edge] struct_map 
      else
	reason_ectype tv_eqc.eqc_dbgtype [edge] struct_map
    in
    (target, tv_type)
  ) target_list
    
let icall_exists asm_fun =
  List.fold_left (fun b bb ->
    let _,ins,_ = List.last bb.bb_instrs in
    let icall_bb = 
      match ins with
      | CALL (dis,abs,op,slctr) ->
	begin match op with
	| Imm_op _ -> false
	| _ -> true
	end
      | _ -> false
    in
    b || icall_bb
  ) false asm_fun.func_bbs

let icall_ops funCFA asm_fun = 
  (*let trans_tvName tvnm =
    match tvnm with
    | TV_addr addrnm ->
      begin match addrnm with
      | {disp=off_str;base=Some reg;index=None} ->
	TV_reg reg
      | {disp=off_str;base=Some reg;index=Some (scl,ndxreg)} ->
	TV_reg reg
      | {disp=off_str; base=None; index=Some (scl,ndxreg)} ->
	TV_addr (mkADNM off_str None None)
      | _ ->
	tvnm
      end
    | _ ->
      tvnm
  in*)
  let inner_tvNameGen funCFA op l =
    tvNameGen funCFA op l
  in
  List.flatten(
    List.map (fun bb ->  
      let addrcount = ref bb.bb_relAddr in
      let l_instr_list = 
	List.map (fun (pre,ins,len) -> 
	  let outaddr = !addrcount in
	  addrcount := len + !addrcount;
	  outaddr, (pre,ins,len)
	) bb.bb_instrs 
      in
      List.filter_map (fun (l,(pre,ins,len)) ->
	match ins with
	| CALL(dis,abs,op,slctr) ->
	  begin match op with
	  | Imm_op _ -> None
	  | _ ->
	    let tvNameOp = inner_tvNameGen funCFA op l in
	    Some (mkTV tvNameOp l false)
	  end
	| _ -> None
      ) l_instr_list
    ) asm_fun.func_bbs
  )

let icall_checking funCFA asm_fun eqcg struct_map =
  P.printf "start icall checking...\n"; flush stdout;
  let icall_targets = icall_ops funCFA asm_fun in
  let target_types = eqcg_search eqcg struct_map icall_targets in
  let abnormal_icall = 
    List.filter (fun (target, ect) ->
      List.length ect <> 1 || 
	List.exists (fun sct -> 
	  match sct with
	  | SC_top _ -> true
	  | _ -> false
	) ect 
    ) target_types
  in
  if not (List.is_empty abnormal_icall) then (
    P.printf "In function %s\n" asm_fun.func_name;
    P.printf "# icall: %d\n" (List.length target_types);
    P.printf "# abnormal icall: %d\n" (List.length abnormal_icall);
    List.iter (fun (target,ect) ->
      let tv, _ = target in
      P.printf "icall at %x:\n" tv.tv_index;
      P.printf "%s\n" (str_of_ectype ect);
    ) target_types
  )

type ctypeEquation = 
  {
    index: int; 
    equation: extended_sctype * extended_sctype; 
    bi_direct: bool;
  }

let mkCTEQ ndx varL varR bi =
  {index = ndx; equation = (varL,varR); bi_direct = bi;}

module CtypeEquationOrder = struct
  type t = ctypeEquation
  let compare (cte1:ctypeEquation) (cte2:ctypeEquation) = 
    Pervasives.compare cte1 cte2
end

module CtypeEqSet = Set.Make (CtypeEquationOrder)


(*
let expand_dbgtypeinfo dbgTypeInfo =
  List.flatten (
    List.map (fun (tvnm, rng, ectype) ->
      match tvnm with
      | TV_cfa off -> (
	try (
	  if List.length ectype <> 1 then
	    raise (Failure "Further check multiple type while expanding Stack")
	  else
	    let sctype = List.hd ectype in
	    begin match sctype with
	    | SC_array (sct,dimlist) ->
	      let length = List.fold_right (fun dim mult ->
		dim * mult
	      ) dimlist 1
	      in
	      let sctsize = Option.get (size_of_sctype sct) in
	      List.init length (fun i ->
		let new_off = off + sctsize * i in
		(TV_cfa new_off, rng, [sct])
	      )
	    | SC_structure (_,_,fieldlist) ->
	      let new_off = ref off in
	      List.map (fun (ft,fn) ->
		let foff = !new_off in
		new_off := !new_off + (Option.get (size_of_sctype ft));
		(TV_cfa foff, rng, [ft])
	      ) fieldlist
	    | _ -> [(tvnm,rng,ectype)]
	    end
	)
	with Failure errmsg -> begin
	  let new_errmsg = errmsg ^ "in TV_CFA" in
	  raise (Failure new_errmsg) end
      )
      | TV_addr addrnm -> (
	try (
	begin match addrnm with
	| {disp=addr_str;base=None;index=None} ->
	  let addr_mword = MWord.of_string ("0x"^addr_str) in
	  if List.length ectype <> 1 then
	    raise (Failure "Further check multiple type while expanding absAddr")
	  else
	    let sctype = List.hd ectype in
	    begin match sctype with
	    | SC_array (sct,dimlist) ->
	      let length = List.fold_right (fun dim mult ->
		dim * mult
	      ) dimlist 1
	      in
	      let sctsize = Option.get (size_of_sctype sct) in
	      List.init length (fun i ->
		let new_addr_mword = 
		  MWord.(+%) addr_mword (MWord.of_int (sctsize * i))
		in
		let new_addr_str = str_of_mword_flex new_addr_mword in
		let new_addrnm = mkADNM new_addr_str None None in
		(TV_addr new_addrnm, rng, [sct])
	      )
	    | SC_structure (_,_,fieldlist) ->
	      let new_addr_mword = ref addr_mword in
	      List.map (fun (ft,fn) ->
		let faddr_mword = !new_addr_mword in
		let faddr_mword_str = str_of_mword_flex faddr_mword in
		new_addr_mword := MWord.(+%) !new_addr_mword (MWord.of_int (Option.get (size_of_sctype ft)));
		let new_addrnm = mkADNM faddr_mword_str None None in
		(TV_addr new_addrnm, rng, [ft])
	      ) fieldlist
	    | _ -> [(tvnm,rng,ectype)]
	    end  
	| _ -> 
	  [tvnm,rng,ectype]
	end
	)
	with Failure errmsg -> begin
	  let new_errmsg = errmsg ^ " in TV_addr" in
	  raise (Failure new_errmsg) end
      )
      | _ -> [(tvnm,rng,ectype)]
    ) dbgTypeInfo
  )
*)
(*
let throw_trash_dbg dbgTypeInfo =
  (** TODO: there are problems in dbg_collection; also representation 
      of structure is important. Maybe we should collect all structure
      types into a global structure type record
  **)
  let rec filter_useful_sctype sctype =
    match sctype with
    | SC_const sct | SC_volatile sct | SC_typedef (_,sct)-> 
      filter_useful_sctype sct
    | SC_array (sct,_) -> filter_useful_sctype sct
    | SC_pointer sct -> filter_useful_sctype sct
    | SC_structure (_,_,fieldlist) ->
      List.length fieldlist <> 0
    | _ -> true
  in
  List.filter_map (fun (tvnm,rng,ectype) ->
    let useful_ectype = List.filter (fun sctype ->
      filter_useful_sctype sctype
    ) ectype
    in
    if List.is_empty useful_ectype then None
    else
      Some (tvnm,rng,useful_ectype)
  ) dbgTypeInfo
*)
(*
let eqcg_multi_connect eqcg tv1 tv1_tag tv2 tv2_tag lv =
  let tv1_repr = eqcg_find_repr eqcg tv1 in
  let tv1_node =
    if Option.is_none tv1_repr then
      let tv_node = mkEQCNode tv1 0 in
      tv_node.eqc_dbgtype <- tv1_tag;
      eqcg_add_repr eqcg tv1 tv_node.eqc_name;
      eqcg_add_node eqcg tv_node.eqc_name tv_node;
      tv_node
    else
      eqcg_find_node eqcg (Option.get tv1_repr)
  in
  let tv2_repr = eqcg_find_repr eqcg tv2 in
  let tv2_node =
    if Option.is_none tv2_repr then
      let tv_node = mkEQCNode tv2 0 in
      tv_node.eqc_dbgtype <- tv2_tag;
      eqcg_add_repr eqcg tv2 tv_node.eqc_name;
      eqcg_add_node eqcg tv_node.eqc_name tv_node;
      tv_node
    else
      eqcg_find_node eqcg (Option.get tv2_repr)
  in
  let tv2_lv_nodes = List.init lv (fun i -> mkEQCNode tv2 (i+1)) in 
  List.iter (fun tv_node ->
    eqcg_add_node eqcg tv_node.eqc_name tv_node
  ) tv2_lv_nodes;
  let tv_node_chain = (tv1_node :: (List.rev tv2_lv_nodes)) @ [tv2_node] in
  List.iteri (fun ndx tv_node ->
    if ndx = 0 then
      tv_node.eqc_preds <- (List.nth tv_node_chain (ndx+1)) :: tv_node.eqc_preds
    else if ndx = ((List.length tv_node_chain) - 1) then
      tv_node.eqc_succs <- (List.nth tv_node_chain (ndx-1)) :: tv_node.eqc_succs
    else (
      tv_node.eqc_preds <- (List.nth tv_node_chain (ndx+1)) :: tv_node.eqc_preds;
      tv_node.eqc_succs <- (List.nth tv_node_chain (ndx-1)) :: tv_node.eqc_succs
    )
  ) tv_node_chain
*)

let dbgtype_tag tv dbgTypeInfo =
  let tag =
    match tv.tv_name with
    | TV_imm _ | TV_reg _ | TV_addr _ ->
      List.unique ?eq:(Some eq_sctype) (
	List.flatten (
	  List.filter_map (fun (tvnm,rng,ectype) ->
	    if tvnm = tv.tv_name && in_range tv.tv_index rng then
	      Some ectype
	    else 
	      None
	  ) dbgTypeInfo
	)
      )
    | TV_cfa off ->
      List.unique ?eq:(Some eq_sctype) (
	List.flatten (
	  List.filter_map (fun (tvnm,rng,ectype) ->
	    if in_range tv.tv_index rng then (
	      match tvnm with
	      | TV_cfa off' ->
		if off < off' then None
		else if off = off' then Some ectype
		else (
		  let array_types = List.filter (fun sctype ->
		    match sctype with
		    | SC_array _ -> true
		    | _ -> false
		  ) ectype
		  in
		  if List.length array_types > 1 then
		    raise (Failure "Type conflict with arrays")
		  else if List.is_empty array_types then None
		  else
		    let array_type = List.hd array_types in
		    match array_type with
		    | SC_array (sct,dimlist) ->
		      let size = List.fold_right (fun dim mult ->
			dim * mult
		      ) dimlist 1
		      in
		      let resi = (off - off') mod 4 in
		      if resi = 0 && ((off - off')/4) <= size then
			Some [sct]
		      else
			None
		    | _ -> None
		)
	      | _ -> None
	    )
	    else None
	  ) dbgTypeInfo
	)
      )
  in
  tag
(* TODO: do this later
    | TV_addr addrnm ->
      let abs_addr = 
	try
	  int_of_string ("0x"^addrnm.disp)
	with Failure errmsg ->
	  let newerr = P.sprintf "%s:%s\n" errmsg addrnm.disp in
	  raise (Failure newerr)
      in
      List.unique ?eq:(Some eq_sctype) (
	List.flatten (
	  List.filter_map (fun (tvnm,rng,ectype) ->
	    match tvnm with
	    | TV_addr addrnm' ->
	      begin match addrnm' with
	      | {disp=abs_addr_str;base=None;index=None}
	      | {disp=abs_addr_str;base=None;index=Some _}
	      | {disp=abs_addr_str;base=Some _;index=Some _} ->
		let abs_addr' = 
		  try
		    int_of_string ("0x"^addrnm'.disp)
		  with Failure errmsg ->
		    let newerr = P.sprintf "%s:%s\n" errmsg addrnm'.disp in
		    raise (Failure newerr)
		in
		if abs_addr < abs_addr' then None
		else (
		  let array_types = List.filter (fun sctype ->
		    match sctype with
		    | SC_array _ -> true
		    | _ -> false
		  ) ectype
		  in
		  if List.length array_types > 1 then
		    raise (Failure "Type conflict with arrays")
		  else if List.is_empty array_types then None
		  else
		    let array_type = List.hd array_types in
		    match array_type with
		    | SC_array (sct,dimlist) ->
		      let size = List.fold_right (fun dim mult ->
			dim * mult
		      ) dimlist 1
		      in
		      let resi = (abs_addr - abs_addr') mod 4 in
		      if resi = 0 && ((abs_addr - abs_addr')/4) <= size then
			Some [sct]
		      else
			None
		    | _ -> None
		)
	      | _ -> None
	      end
	    | _ -> None
  ) dbgTypeInfo
  )
      )
*)
