open Batteries
module P = Printf
module H = Hashtbl

open Bits
open X86Syntax
open X86Semantics
open X86_MACHINE
open X86_RTL
open Config
open Abbrev
open Elf

open BB
open RTL_opt
open Ibaux
open Config_cfg
open Printer

let report_top_trace = ref false
let report_unsupport = ref false
let verbose_on = ref false

(** used in CFGrefine **)
let ic_handled = ref 0
let ic_from_dbg = ref 0
let ic_from_inf = ref 0
let ic_guess = ref 0
let ic_cmplr = ref 0
let ic_cmplr_dbg = ref 0
let ic_cmplr_inf = ref 0
let ic_types = ref 0

let ij_handled = ref 0
let ij_class = ref 0
let ij_tcall = ref 0
let ij_cmplr = ref 0
let ij_cmplr_class = ref 0
let ij_cmplr_tcall = ref 0
let tc_from_dbg = ref 0
let tc_from_inf = ref 0

let ij_conflict = ref 0


type tvIndex = int
  
type addrName = {disp:string;base:register option;index:(scale * register) option}

let mkADNM indisp inbase inindex =
  {disp = indisp; base = inbase; index = inindex}

type tvName = 
| TV_imm of string
| TV_reg of register
| TV_cfa of int
| TV_addr of addrName    

let is_trivial ins =
  match ins with
  | PUSH _ | POP _ | LEAVE -> true
  | NOP _ -> true
  | JMP(_,_,_,_) | Jcc(_,_) | JCXZ(_) -> true
  | CMP _ | TEST _ | CMPS _ -> true
  | ADD _ | SUB _ | IMUL _ | MUL _ | IDIV _ | DIV _ | SBB _ | ADC _ -> true
  | AND _ | OR _ | NEG _ -> true
  | BT _ | BTC _ | BTR _ | BTS _ -> true
  | DEC _ | INC _ -> true
  | CWDE -> true
  | MOVS _ -> true
  | CLD -> true
  | SCAS _ -> true
  | SHL _ | SHR _ | SHLD _ | SHRD _ | SAR _ -> true
  (** float-point instructions **) 
  | F2XM1 
  | FABS 
  | FADD _ 
  | FADDP _ 
  | FBLD _
  | FBSTP _
  | FCHS
  | FCMOVcc _ 
  | FCOM _ 
  | FCOMP _ 
  | FCOMPP
  | FCOMIP _
  | FCOS
  | FDECSTP
  | FDIV _
  | FDIVP _
  | FDIVR _
  | FDIVRP _
  | FFREE _
  | FIADD _
  | FICOM _
  | FICOMP _
  | FIDIV _
  | FIDIVR _
  | FILD _
  | FIMUL _
  | FINCSTP
  | FIST _
  | FISTP _
  | FISUB _
  | FISUBR _
  | FLD _
  | FLD1
  | FLDCW _
  | FLDENV _
  | FLDL2E
  | FLDL2T
  | FLDLG2
  | FLDLN2
  | FLDPI
  | FLDZ
  | FMUL _
  | FMULP _
  | FNCLEX
  | FNINIT
  | FNOP
  | FNSAVE _
  | FNSTCW _
  | FNSTSW _
  | FPATAN
  | FPREM
  | FPREM1
  | FPTAN
  | FRNDINT
  | FRSTOR _
  | FSCALE
  | FSIN
  | FSINCOS
  | FSQRT
  | FST _
  | FSTENV _
  | FSTP _
  | FSUB _
  | FSUBP _
  | FSUBR _
  | FSUBRP _
  | FTST
  | FUCOM _
  | FUCOMP _
  | FUCOMPP
  | FUCOMI _
  | FUCOMIP _
  | FXAM
  | FXCH _
  | FXTRACT
  | FYL2X
  | FYL2XP1
  | FWAIT -> true
  | _ -> false

let is_evil ins =
  match ins with
  | RET(_,_) | IRET -> true
  | _ -> false

let split_at_eq re =
  match re with
  | Coq_test_rtl_exp(s,Coq_eq_op,re1,re2) ->
    begin match re1 with
    | Coq_get_loc_rtl_exp(s,Coq_reg_loc reg) ->
      reg, re2
    | _ ->
      raise (Failure "Should be no other expressions other than get_loc")
    end
  | _ ->
    raise (Failure "Should be no other expressions other than test")

let reverse_cfa_expr re =
  let cfa_ps_reg = Coq_get_ps_reg_rtl_exp(Big.of_int 31, Big.of_int (-1)) in
  match re with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    begin match re1 with
    | Coq_get_loc_rtl_exp(s, Coq_reg_loc reg) ->
      begin match bvop with
      | Coq_add_op ->
       	reg, Coq_arith_rtl_exp(s,Coq_sub_op,cfa_ps_reg,re2)
      | Coq_sub_op ->
	reg, Coq_arith_rtl_exp(s,Coq_add_op,cfa_ps_reg,re2)
      | _ ->
	raise (Failure "Should be no other operators")
      end
    | _ -> 
      raise (Failure "Should not exist things other than registers")
    end
  | Coq_get_loc_rtl_exp(s,Coq_reg_loc reg) ->
    reg, cfa_ps_reg
  | _ ->
    P.printf "%s\n" (str_of_rtl_exp_less_size re); flush stdout;
    raise (Failure "Should not exist expressions other than arith")

let transform_cfa_expr re addr_name =
  match re with
  | Coq_arith_rtl_exp(_,bvop,re1,re2) ->
    begin match re1 with
    | Coq_get_ps_reg_rtl_exp(_,ps_reg) when Big.to_int ps_reg = (-1) ->
      begin match bvop with
      | Coq_add_op ->
	begin match re2 with
	| Coq_imm_rtl_exp (_,v) -> 
	  TV_cfa (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int v)))
	| _ -> 
	  TV_addr addr_name
	end
      | Coq_sub_op ->
	begin match re2 with
	| Coq_imm_rtl_exp (_,v) -> 
	  TV_cfa (-(Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int v))))
	| _ -> 
	  TV_addr addr_name
	end
      | _ -> TV_addr addr_name
      end
    | _ -> TV_addr addr_name
    end
  | Coq_get_ps_reg_rtl_exp(_,ps_reg) when Big.to_int ps_reg = (-1) ->
    TV_cfa 0
  | _ -> TV_addr addr_name

let rtl_add_big re bigv =
  simp_re simpre_all_methods (Coq_arith_rtl_exp(Big.of_int 31, Coq_add_op, re, Coq_imm_rtl_exp(Big.of_int 31, bigv)))

let tvNameGen funCFA op l =
  match op with
  | Imm_op imm -> TV_imm (str_of_mword_flex (MWord.of_big_int imm))
  | Reg_op reg -> TV_reg reg
  | Address_op addr ->
    let addr_name = {
      disp= str_of_mword_flex (MWord.of_big_int (addrDisp addr));
      base= addrBase addr;
      index= addrIndex addr} 
    in 
    begin match addr with
    | {addrDisp = big_v; addrBase = Some reg; addrIndex = _} ->
      let _, l_cfa =
	try
	  List.find (fun (lopc,cfaexp) ->
	    lopc = l
	  ) funCFA
	with Not_found ->
	  P.printf "not found cfaexp at %x\n" l; flush stdout;
	  List.last funCFA
	  (*raise (Failure "in tvNameGen")*)
      in
      let cfaoff = List.map (fun (lvl,re) ->
	if lvl > 0 then split_at_eq re
	else 
	  reverse_cfa_expr re
      ) l_cfa 
      in
      begin 
	try 
	  let _, reg_in_cfa = List.find (fun (register, re) ->
	    register = reg
	  ) cfaoff
	  in
	  let op_cfa_expr = rtl_add_big reg_in_cfa big_v in
	  transform_cfa_expr op_cfa_expr addr_name
	with Not_found ->
	  TV_addr addr_name 
      end
    | _ ->
      TV_addr addr_name
    end
  | Offset_op off ->
    TV_addr {disp = str_of_mword_flex (MWord.of_big_int off); base = None; index = None}

type typeVar = {
  tv_name: tvName;
  tv_index: tvIndex;
  tv_crossbb: bool;
}

(* Edge type: 0 for normal; 1 for dereference; 2 for reference *)
let mkTV tvnm tvndx tvcros =
  (** add another layer to transform TV_addr names **)
  match tvnm with
  | TV_addr addrnm ->
    begin match addrnm with
    | {disp=off_str;base=Some reg;index=None} ->
      let off_int = Util.int_of_int32 (Int64.to_int32 (MWord.of_string ("0x"^off_str))) in
      if MWord.(>=%) (MWord.of_string ("0x"^off_str)) (MWord.of_string "0x8048000") && off_int > 0
      then
	let tv = {
	  tv_name = TV_addr (mkADNM off_str None None);
	  tv_index = tvndx;
	  tv_crossbb = tvcros;
	}
	in
	tv, (1,"all")
      else
	let tv = {
	  tv_name = TV_reg reg;
	  tv_index = tvndx;
	  tv_crossbb = tvcros;
	}
	in
	tv, (1,off_str)
    | {disp=off_str;base=Some reg;index= Some _} ->
      let off_int = Util.int_of_int32 (Int64.to_int32 (MWord.of_string ("0x"^off_str))) in
      if MWord.(>=%) (MWord.of_string ("0x"^off_str)) (MWord.of_string "0x8048000") && off_int > 0
      then
	let tv = {
	  tv_name = TV_addr (mkADNM off_str None None);
	  tv_index = tvndx;
	  tv_crossbb = tvcros;
	}
	in
	tv, (1,"all")
      else
	let tv = {
	  tv_name = TV_reg reg;
	  tv_index = tvndx;
	  tv_crossbb = tvcros;
	}
	in
	tv, (1,"all")
    | {disp=off_str; base=None; index=Some (scl,ndxreg)} ->
      let tv = {
	tv_name = TV_addr (mkADNM off_str None None);
	tv_index = tvndx;
	tv_crossbb = tvcros;
      }
      in
      tv, (1,"all")
    | _ ->
      let tv = {
	tv_name = tvnm;
	tv_index = tvndx;
	tv_crossbb = tvcros;
      }
      in
      tv, (1,"0")
    end
  | _ ->
    let tv = {
      tv_name = tvnm;
      tv_index = tvndx;
      tv_crossbb = tvcros;
    }
    in
    tv, (0,"0")

type super_ctype = 
| (*Top Type*) SC_top of (string * (int * string)) list
| (*Type Variable*) SC_typeVar of typeVar
| (*0*) SC_base of string (* like int, float including void *) 
| (*1*) SC_const of super_ctype
| (*2*) SC_array of super_ctype * (int list) (* int list for dimension *)
| (*3*) SC_pointer of super_ctype
| (*4*) SC_typedef of string * super_ctype (* string for typedef name *)
| (*5*) SC_subroutine of super_ctype * (super_ctype list) (* first: return type, second: parameter type list *)
| (*6*) SC_enumeration of string * ((string * int) list) (* enum_name, {enumerator_name, enumerator_value} *)
| (*7*) SC_union of string * ((super_ctype * string) list) (* union_name, {field_type, field_name} *)
| (*8*) SC_structure of string * string * ((super_ctype * string) list) (* struct_name, new_struct_name, {field_type, field_name} *)
| (*9*) SC_unspecified_param
| (*a*) SC_wrong_type of string (* including an error message *)
| (*b*) SC_volatile of super_ctype

let rec sctype_to_ctype sctype =
  match sctype with
  | SC_base name -> C_base name 
  | SC_const ct -> C_const (sctype_to_ctype ct)
  | SC_array (ct,dimlst) -> 
    C_array ((sctype_to_ctype ct),dimlst)
  | SC_pointer ct -> 
    C_pointer (sctype_to_ctype ct)
  | SC_typedef (typedef_name, ct) ->
    C_typedef (typedef_name,(sctype_to_ctype ct))
  | SC_subroutine (ret_ct, param_ct_list) ->
    C_subroutine ((sctype_to_ctype ret_ct), (List.map sctype_to_ctype param_ct_list))
  | SC_enumeration (enumname, etor_list) -> 
    C_enumeration (enumname, etor_list)
  | SC_union (unionname, umem_list) -> 
    C_union (unionname, (List.map (fun (umemtype,umemname) -> (sctype_to_ctype umemtype), umemname) umem_list))
  | SC_structure (original_name, level_name, smem_list) ->
    C_structure (original_name, level_name, (List.map (fun (smemtype,smemname) -> (sctype_to_ctype smemtype), smemname) smem_list))
  | SC_unspecified_param ->
    C_unspecified_param
  | SC_wrong_type errmsg ->
    C_wrong_type errmsg
  | SC_volatile ct ->
    C_volatile (sctype_to_ctype ct)
  | _ ->
    C_wrong_type "pseudo"

type extended_sctype = super_ctype list

let str_of_scale scl =
  match scl with
  | Scale1 -> 1
  | Scale2 -> 2
  | Scale4 -> 4
  | Scale8 -> 8

let str_of_tvnm tvnm =
  match tvnm with
  | TV_imm imm -> 
    P.sprintf "$0x%s" imm
  | TV_reg reg ->
    str_of_reg reg
  | TV_cfa off ->
    P.sprintf "%d<CFA>" off
  | TV_addr addr ->
    begin match addr with
    | {disp = strv; base = None; index = None} ->
      P.sprintf "(%s)" strv
    | {disp = strv; base = Some reg; index = None} ->
      P.sprintf "%s(%s)"
	strv
	(str_of_reg reg)
    | {disp = strv; base = None ; index = Some (scl,reg)} ->
      P.sprintf "%s(,%s,%d)"
	strv
	(str_of_reg reg)
	(str_of_scale scl)
    | {disp = strv; base = Some regbase ; index = Some (scl,regindex)} ->
      P.sprintf "%s(%s,%s,%d)"
	strv
	(str_of_reg regbase)
	(str_of_reg regindex)
	(str_of_scale scl)
    end

let print_tvname tvnm =
  P.printf "%s" (str_of_tvnm tvnm)

let str_of_tv tv =
  if tv.tv_crossbb then
    P.sprintf "%s_%x+" (str_of_tvnm tv.tv_name) tv.tv_index
  else
    P.sprintf "%s_%x" (str_of_tvnm tv.tv_name) tv.tv_index

let print_tv tv =
  P.printf "%s" (str_of_tv tv)
    
let str_of_edge edge =
  P.sprintf "(%d:%s)" (fst edge) (snd edge)
    
let str_of_trace trace =
  List.fold_left (fun str trace_point ->
    P.sprintf "%s%s%s" str (fst trace_point) (str_of_edge (snd trace_point))
  ) "" trace
    
let rec str_of_sctype sctype =
  match sctype with
  | SC_top trace->
    if !report_top_trace then
      P.sprintf "<T> through %s"
	(str_of_trace trace)
    else "<T>"
  | SC_typeVar tv -> 
    P.sprintf "%s" (str_of_tv tv)
  | SC_base str -> str (* like int, float including void *) 
  | SC_const sct -> P.sprintf "const %s" (str_of_sctype sct)
  | SC_array (sct,dimlst) ->
    let dimension_str = String.concat "" (List.map (fun dim -> P.sprintf "[%d]" dim) dimlst) in
    P.sprintf "(%s%s)" (str_of_sctype sct) dimension_str
  | SC_pointer sct -> P.sprintf "%s *" (str_of_sctype sct)
  | SC_typedef (typedef_name,sct) -> 
    P.sprintf "typedef(%s:%s)" typedef_name (str_of_sctype sct)
  | SC_subroutine(ret_sc, param_sc_list) -> (* first: return type, second: parameter type list *)
    let ret_sc_str = str_of_sctype ret_sc in
    let param_sc_str = String.concat "," (List.map str_of_sctype param_sc_list) in
    P.sprintf "(%s)(%s)" ret_sc_str param_sc_str 
  | SC_enumeration (enumname,etor_list) ->
    let etor_list_str = String.concat ";" (List.map (fun (etorname,etorval) -> P.sprintf "%s(%d)" etorname etorval) etor_list) in
    P.sprintf "ENUM %s{%s}" enumname etor_list_str
  | SC_union (unionname,umem_list) ->
    let umem_list_str = String.concat ";" (List.map (fun (umemtype,umemname) -> P.sprintf "%s %s" (str_of_sctype umemtype) umemname) umem_list) in
    P.sprintf "UNION %s{%s}" unionname umem_list_str
  | SC_structure (original_name, level_name, smem_list) ->
    let smem_list_str = String.concat ";" (List.map (fun (smemtype,smemname) -> P.sprintf "%s %s" (str_of_sctype smemtype) smemname) smem_list) in
    P.sprintf "STRUCT %s(%s){%s}" original_name level_name smem_list_str
  | SC_unspecified_param ->
    "..."    
  | SC_wrong_type errmsg ->
    let () = P.printf "Wrong type: %s" errmsg in
    P.sprintf "wrong_type(%s)" errmsg   
  | SC_volatile sct ->
    P.sprintf "volatile %s" (str_of_sctype sct)

let rec str_of_ectype ectype =
  match ectype with
  | [] -> ""
  | h :: t -> (str_of_sctype h) ^ ";\n" ^ (str_of_ectype t) 
      
let rec eq_sctype sctype1 sctype2 =
  match sctype1,sctype2 with
  (*| SC_top trace1, SC_top trace2 -> 
      trace1 = trace2
  *)
  | SC_top _, SC_top _ -> true
  | SC_typeVar tv1, SC_typeVar tv2 ->
    tv1 = tv2
  | SC_base name1, SC_base name2 -> name1 = name2
  | SC_const sct1, SC_const sct2 -> eq_sctype sct1 sct2
  | SC_array (sct1,dimlist1), SC_array (sct2,dimlist2) -> 
    let type_consis = eq_sctype sct1 sct2 in 
    let dim_consis = 
      if List.length dimlist1 = List.length dimlist2 then 
	List.fold_right2 
	  (fun dim1 dim2 b -> 
	    if dim1 = 0 || dim2 = 0 then b 
	    else (dim1 = dim2) && b) dimlist1 dimlist2 true
      else false
    in
    type_consis && dim_consis
  | SC_array (sct1,_), SC_pointer sct2 -> eq_sctype sct1 sct2
  | SC_pointer sct1, SC_pointer sct2 -> eq_sctype sct1 sct2
  | SC_pointer sct1, SC_array (sct2,_) -> eq_sctype sct1 sct2
  | SC_typedef (_,sct1), _ -> eq_sctype sct1 sctype2
  | _, SC_typedef (_,sct2) -> eq_sctype sctype1 sct2
  | SC_subroutine (ret1,param1), SC_subroutine (ret2,param2) ->
    let ret_consis = eq_sctype ret1 ret2 in
    let param_consis = param_equal param1 param2 in
    ret_consis && param_consis
  | SC_enumeration (enumname1,etorlist1), SC_enumeration (enumname2,etorlist2) ->
    enumname1 = enumname2
  | SC_union (unionname1,umemlist1), SC_union (unionname2,umemlist2) ->
    (** when one has name but no type and the other does not have name but have type, they may
	still be the same **)
    if unionname1 = unionname2 then true
    else if List.length umemlist1 = List.length umemlist2 then
      List.fold_right2 (fun (sct1,str1) (sct2,str2) b -> b && (eq_sctype sct1 sct2) && (str1 = str2)) umemlist1 umemlist2 true
    else false
  | SC_structure (structname1,levelname1,smemlist1), SC_structure (structname2,levelname2,smemlist2) -> 
    if structname1 = structname2 then true
    else if levelname1 = levelname2 && (List.length smemlist1) = (List.length smemlist2) then
      if (List.length smemlist1) = 0 then true
      else
	List.fold_right2 (fun (sct1,str1) (sct2,str2) b -> b && eq_sctype sct1 sct2 && str1 = str2) smemlist1 smemlist2 true
    else false
  | SC_unspecified_param, SC_unspecified_param ->  
    true
  | SC_wrong_type errmsg1, _ -> 
    let () = P.printf "In eq_sctype, the first type is wrong type: %s" errmsg1 in
    false
  | _, SC_wrong_type errmsg2 ->
    let () = P.printf "In eq_sctype, the second type is wrong type: %s" errmsg2 in
    false
  | SC_volatile ct1, SC_volatile ct2 -> eq_sctype ct1 ct2
  | _ -> false
    
and param_equal param1 param2 =
  match param1,param2 with
  | [], [] -> true
  | hd_param1 :: tl_param1, hd_param2 :: tl_param2 ->
    eq_sctype hd_param1 hd_param2 && param_equal tl_param1 tl_param2
  | _ -> false

let eq_ectype ect1 ect2 =
  if List.length ect1 <> List.length ect2 then false
  else
    List.fold_right (fun sct1 b ->
      b && List.exists (fun sct2 -> eq_sctype sct1 sct2) ect2
    ) ect1 true

let sub_ectype ect1 ect2 =
  List.fold_right (fun sct1 b ->
    b && List.exists (fun sct2 -> eq_sctype sct1 sct2) ect2
  ) ect1 true

type struct_type = {
  struct_type: super_ctype;
  struct_size: int;
  struct_memoff: int list;
}

let mkSTRUCT ty size memoff = {
  struct_type = ty;
  struct_size = size;
  struct_memoff = memoff;
}

let str_of_struct stty =
  let stru = P.sprintf "%s: (%d)\n" (str_of_sctype stty.struct_type) stty.struct_size in
  List.fold_left (fun str d ->
    str ^ (P.sprintf "%d " d)
  ) stru stty.struct_memoff

let print_struct stty = 
  P.printf "%s\n" (str_of_struct stty)

let calc_array_bound dimlist =
  List.fold_right (fun dim mult ->
    dim * mult
  ) dimlist 1

let find_real_type struct_map sctype =
  match sctype with
  | SC_typedef (defnm,sct) ->
    if H.mem struct_map defnm then
      SC_typedef (defnm, H.find struct_map defnm)
    else
      sctype
  | SC_structure (name,_,fieldlist) ->
    if not (List.is_empty fieldlist) then sctype
    else if H.mem struct_map name then
      H.find struct_map name
    else
      raise (Failure "struct_map is not complete!")
  | _ -> sctype

let rec size_of_sctype struct_map sctype =
  match sctype with
  | SC_top _ | SC_typeVar _ -> 
    raise (Failure "Psudo-type does not have size")
  | SC_base basename ->
    begin match basename with
    | "char" -> 1
    | "unsigned char" -> 1
    | "signed char" -> 1
    | "short int" -> 2
    | "short unsigned int" -> 2
    | "long long int" -> 8
    | "long long unsigned int" -> 8
    | "double" -> 8
    | _ -> 4
    end
  | SC_const sct -> 
    size_of_sctype struct_map sct
  | SC_array (sct,dimlist) -> 
    (** dimension may not be known **)
    let bound = calc_array_bound dimlist in
    let typesize = size_of_sctype struct_map sct in
    bound * typesize
  | SC_pointer _ -> 4
  | SC_typedef (defnm,sct) ->
    if H.mem struct_map defnm then
      (H.find struct_map defnm).struct_size
    else
      size_of_sctype struct_map sct
  | SC_subroutine _ -> 
    raise (Failure "subroutine type theoretically does not have a size!")    
  | SC_enumeration (enumname,enumlist) ->
    (** may be wrong **)
    4 (*(List.length enumlist) * 4*)
  | SC_union (unionname, unionlist) ->
    List.max (List.map (fun (ft,fn) -> size_of_sctype struct_map ft) unionlist)
  | SC_structure (structname,newstructname,fieldlist) ->
    if H.mem struct_map structname then
      (H.find struct_map structname).struct_size
    else
      let errmsg = 
	P.sprintf "struct not in struct_map:\n %s" (str_of_sctype sctype)
      in
      raise (Failure errmsg)
    (*
    if not (List.is_empty fieldlist) then
      List.fold_right (fun (ft,fn) sum -> 
	let ftsize = size_of_sctype struct_map ft in
	match ft with
	| SC_array (sct',dimlist) ->
	  let sctsize' = size_of_sctype struct_map sct' in
	  let padding = sum mod sctsize' in
	  sum + ftsize + padding
	| _ ->
	  if ftsize <= 4 then
	    let padding = sum mod ftsize in
	    sum + ftsize + padding
	  else if ftsize = 8 then
	    let padding = sum mod 4 in
	    sum + ftsize + padding
	  else
	    sum + ftsize
      ) fieldlist 0
    else if H.mem struct_map structname then
      size_of_sctype struct_map (H.find struct_map structname)
    else
      raise (Failure "size of structure error!")
    *)
  | SC_unspecified_param ->
    raise (Failure "size of unsepc")
  | SC_wrong_type _ -> 
    raise (Failure "size of SC_wrong_type")
  | SC_volatile sct -> size_of_sctype struct_map sct

module TypeVarOrder = struct
  type t = typeVar
  let compare (tv1:typeVar) (tv2:typeVar) = 
    Pervasives.compare tv1 tv2
end

module TypeVarSet = Set.Make (TypeVarOrder)

type eqcNode = 
  {
    mutable eqc_name: string;
    mutable eqc_vars: TypeVarSet.t;
    (*mutable eqc_ptrlv: int;*)
    mutable eqc_succs: (typeVar*(int*string)) list;
    mutable eqc_preds: (typeVar*(int*string)) list;
    mutable eqc_dbgtype: extended_sctype;
    mutable eqc_fflowtype: extended_sctype;
    mutable eqc_bflowtype: extended_sctype;
  }

let mkEQCNode tv = 
  {
    eqc_name = str_of_tv tv;
    eqc_vars = TypeVarSet.singleton tv;
    (*eqc_ptrlv = lv; *)
    eqc_succs = []; eqc_preds = []; 
    eqc_dbgtype = []; 
    eqc_fflowtype = [];
    eqc_bflowtype = [];
  }

let dump_eqcnode eqcnode =
  P.printf "EQC_node %s contains variables:\n" 
    eqcnode.eqc_name;
  TypeVarSet.iter (fun var ->
    P.printf "%s " (str_of_tv var)
  ) eqcnode.eqc_vars;
  P.printf "\n";
  P.printf "SUCCS:\n";
  List.iter (fun (tv,edge) ->
    P.printf "(%s,%s) " (str_of_tv tv) (str_of_edge edge)
  ) eqcnode.eqc_succs;
  P.printf "\n";
  P.printf "PREDS:\n";
  List.iter (fun (tv,edge) ->
    P.printf "(%s,%s) " (str_of_tv tv) (str_of_edge edge)
  ) eqcnode.eqc_preds;
  P.printf "\n";
  P.printf "TypeInfo from DBG:\n";
  List.iter (fun sctype -> 
    P.printf "%s\n" (str_of_sctype sctype)
  ) eqcnode.eqc_dbgtype;
  P.printf "\n";
  P.printf "TypeInfo from Forward-Flow:\n";
  List.iter (fun sctype -> 
    P.printf "%s\n" (str_of_sctype sctype)
  ) eqcnode.eqc_fflowtype;
  P.printf "\n";
  P.printf "TypeInfo from Backward-Flow:\n";
  List.iter (fun sctype -> 
    P.printf "%s\n" (str_of_sctype sctype)
  ) eqcnode.eqc_bflowtype;
  P.printf "\n";
  flush stdout

(** modify node1 **)
let eqcnode_merge node1 node2 =
  node1.eqc_vars <- TypeVarSet.union node1.eqc_vars node2.eqc_vars;
  node1.eqc_succs <- List.unique (node1.eqc_succs @ node2.eqc_succs);
  node1.eqc_preds <- List.unique (node1.eqc_preds @ node2.eqc_preds);
  node1.eqc_dbgtype <- List.unique ?eq:(Some eq_sctype) (node1.eqc_dbgtype @ node2.eqc_dbgtype);
  node1.eqc_fflowtype <- List.unique ?eq:(Some eq_sctype) (node1.eqc_fflowtype @ node2.eqc_fflowtype);
  node1.eqc_bflowtype <- List.unique ?eq:(Some eq_sctype) (node1.eqc_bflowtype @ node2.eqc_bflowtype)
(*
  {
    eqc_name = node1.eqc_name;
    eqc_vars = TypeVarSet.union node1.eqc_vars node2.eqc_vars;
    eqc_ptrlv = 
      if node1.eqc_ptrlv = node2.eqc_ptrlv then node1.eqc_ptrlv
      else raise (Failure ("Merge different level nodes!"));
    eqc_succs = List.unique (node1.eqc_succs @ node2.eqc_succs);
    eqc_preds = List.unique (node1.eqc_preds @ node2.eqc_preds);
    eqc_dbgtype = List.unique ?eq:(Some eq_sctype) (node1.eqc_dbgtype @ node2.eqc_dbgtype);
    eqc_fflowtype = List.unique ?eq:(Some eq_sctype) (node1.eqc_fflowtype @ node2.eqc_fflowtype);
    eqc_bflowtype = List.unique ?eq:(Some eq_sctype) (node1.eqc_bflowtype @ node2.eqc_bflowtype);
  }
*)

(*type eqcGraph = eqcNode list*)
(** union-find hash-tables**)
type eqcGraph = (typeVar, string) H.t * (string, eqcNode) H.t

let dump_eqcg eqcg =
  let varmap, nodes = eqcg in
  H.iter (fun tv1 str ->
    print_tv tv1;
    P.printf " --> %s" str;
    P.printf "\n";
    flush stdout
  ) varmap;
  H.iter (fun repr node ->
    P.printf "%s -->\n" repr;
    dump_eqcnode node;
    P.printf "\n";
    flush stdout
  ) nodes

let eqcg_find_repr eqcg tv =
  let varmap, _ = eqcg in
  try
    Some (H.find varmap tv)
  with Not_found ->
    None

let eqcg_find_node eqcg repr =
  let _, nodes = eqcg in
  try
    H.find nodes repr
  with Not_found ->
    raise (Failure "One repr does not have correspondent node!")

let eqcg_add_repr eqcg tv repr =
  let varmap, _ = eqcg in
  H.add varmap tv repr

let eqcg_add_node eqcg repr node =
  let _, nodes = eqcg in
  H.add nodes repr node

let eqcg_find eqcg tv =
  let varmap, nodes = eqcg in
  H.find nodes (H.find varmap tv)

let eqcg_update eqcg tv node =
  let varmap,nodes = eqcg in
  H.replace varmap tv node.eqc_name;
(*
  TypeVarSet.iter (fun tv ->
    H.replace varmap tv node.eqc_name;
  ) node.eqc_vars;*)
  H.replace nodes node.eqc_name node

let eqcg_replace_repr eqcg tv repr =
  let varmap, _ = eqcg in
  H.replace varmap tv repr

let eqcg_replace_node eqcg repr node =
  let _,nodes = eqcg in
  H.replace nodes repr node

let eqcg_remove_node eqcg repr =
  let _, nodes = eqcg in
  H.remove_all nodes repr

let eqcg_connect eqcg tv1 tv1_tag edge1 tagged_tv_list =
  if (fst edge1) <> 0 then ()
  else (
    let tv1_repr = eqcg_find_repr eqcg tv1 in
    let tv1_node =
      if Option.is_none tv1_repr then
	let tv_node = mkEQCNode tv1 in
	tv_node.eqc_dbgtype <- tv1_tag;
	eqcg_add_repr eqcg tv1 tv_node.eqc_name;
	eqcg_add_node eqcg tv_node.eqc_name tv_node;
	tv_node
      else
	let tv_node = eqcg_find_node eqcg (Option.get tv1_repr) in
	tv_node.eqc_dbgtype <- List.unique (tv1_tag @ tv_node.eqc_dbgtype);
	tv_node
    in
    let node_unique node_list = 
      List.unique ?eq:(Some (fun (nodename1,edge1) (nodename2,edge2) ->
	nodename1 = nodename2 && edge1 = edge2   
      )) node_list
    in
    List.iter (fun (tv2, tv2_tag, edge2) ->
      let tv2_repr = eqcg_find_repr eqcg tv2 in
      if Option.is_none tv2_repr then (
	let tv2_node = mkEQCNode tv2 in
	tv2_node.eqc_dbgtype <- tv2_tag;
	tv2_node.eqc_succs <- node_unique ((tv1,edge2) :: tv2_node.eqc_succs);
	tv1_node.eqc_preds <- node_unique ((tv2,edge2) :: tv1_node.eqc_preds);
	eqcg_add_repr eqcg tv2 tv2_node.eqc_name;
	eqcg_add_node eqcg tv2_node.eqc_name tv2_node;
	eqcg_update eqcg tv1 tv1_node
      ) else (
	let tv2_node = eqcg_find_node eqcg (Option.get tv2_repr) in
	tv2_node.eqc_dbgtype <- List.unique ?eq:(Some eq_sctype) (tv2_tag @ tv2_node.eqc_dbgtype);
	tv2_node.eqc_succs <- node_unique ((tv1,edge2) :: tv2_node.eqc_succs);
	tv1_node.eqc_preds <- node_unique ((tv2,edge2) :: tv1_node.eqc_preds);
	eqcg_update eqcg tv1 tv1_node;
	eqcg_update eqcg tv2 tv2_node
      )
    ) tagged_tv_list
  )

let eqcg_merge eqcg tv1 tv1_tag edge1 tv2 tv2_tag edge2 = 
  assert ((fst edge1) = 0);
  if (List.is_empty tv1_tag && List.is_empty tv2_tag) || (eq_ectype tv1_tag tv2_tag) then (
    let dbgtag = tv1_tag in
    let tv1_repr = eqcg_find_repr eqcg tv1 in
    let tv2_repr = eqcg_find_repr eqcg tv2 in
    if Option.is_none tv1_repr && Option.is_none tv2_repr then (
      let tv1_node = mkEQCNode tv1 in
      let tv2_node = mkEQCNode tv2 in
      eqcnode_merge tv2_node tv1_node;
      tv2_node.eqc_dbgtype <- dbgtag;
      eqcg_add_repr eqcg tv1 tv2_node.eqc_name;
      eqcg_add_repr eqcg tv2 tv2_node.eqc_name;
      eqcg_add_node eqcg tv2_node.eqc_name tv2_node
    ) else if Option.is_none tv1_repr then (
      let tv1_node = mkEQCNode tv1 in
      let tv2_node = eqcg_find_node eqcg (Option.get tv2_repr) in
      eqcnode_merge tv2_node tv1_node;
      tv2_node.eqc_dbgtype <- List.unique ?eq:(Some eq_sctype) (dbgtag @ tv2_node.eqc_dbgtype);
      eqcg_add_repr eqcg tv1 (Option.get tv2_repr);
      eqcg_replace_node eqcg (Option.get tv2_repr) tv2_node
    ) else if Option.is_none tv2_repr then (
      let tv2_node = mkEQCNode tv1 in
      let tv1_node = eqcg_find_node eqcg (Option.get tv1_repr) in
      eqcnode_merge tv1_node tv2_node;
      tv1_node.eqc_dbgtype <- List.unique ?eq:(Some eq_sctype) (dbgtag @ tv1_node.eqc_dbgtype);
      eqcg_add_repr eqcg tv2 (Option.get tv1_repr);
      eqcg_replace_node eqcg (Option.get tv1_repr) tv1_node
    ) else (
      let tv1_node = eqcg_find_node eqcg (Option.get tv1_repr) in
      let tv2_node = eqcg_find_node eqcg (Option.get tv2_repr) in
      eqcnode_merge tv2_node tv1_node;
      tv2_node.eqc_dbgtype <- List.unique ?eq:(Some eq_sctype) (dbgtag @ tv2_node.eqc_dbgtype);
      eqcg_replace_repr eqcg tv1 (Option.get tv2_repr);
      eqcg_remove_node eqcg (Option.get tv1_repr);
      eqcg_replace_node eqcg (Option.get tv2_repr) tv2_node
    )
  ) else(
    eqcg_connect eqcg tv1 tv1_tag edge1 [tv2,tv2_tag,edge2]
  )

let infered_by_merge = ref 0

let eqcg_merge_consrv eqcg tv1 tv1_tag edge1 tv2 tv2_tag edge2 = 
  assert ((fst edge1) = 0);
  if List.is_empty tv1_tag && List.is_empty tv2_tag then (
    let tv1_repr = eqcg_find_repr eqcg tv1 in
    let tv2_repr = eqcg_find_repr eqcg tv2 in
    if Option.is_none tv1_repr && Option.is_none tv2_repr then (
      let tv1_node = mkEQCNode tv1 in
      let tv2_node = mkEQCNode tv2 in
      eqcnode_merge tv2_node tv1_node;
      eqcg_add_repr eqcg tv1 tv2_node.eqc_name;
      eqcg_add_repr eqcg tv2 tv2_node.eqc_name;
      eqcg_add_node eqcg tv2_node.eqc_name tv2_node
    ) else if Option.is_none tv1_repr then (
      let tv1_node = mkEQCNode tv1 in
      let tv2_node = eqcg_find_node eqcg (Option.get tv2_repr) in
      eqcnode_merge tv2_node tv1_node;
      eqcg_add_repr eqcg tv1 (Option.get tv2_repr);
      eqcg_replace_node eqcg (Option.get tv2_repr) tv2_node
    ) else if Option.is_none tv2_repr then (
      let tv2_node = mkEQCNode tv1 in
      let tv1_node = eqcg_find_node eqcg (Option.get tv1_repr) in
      eqcnode_merge tv1_node tv2_node;
      eqcg_add_repr eqcg tv2 (Option.get tv1_repr);
      eqcg_replace_node eqcg (Option.get tv1_repr) tv1_node
    ) else (
      let tv1_node = eqcg_find_node eqcg (Option.get tv1_repr) in
      let tv2_node = eqcg_find_node eqcg (Option.get tv2_repr) in
      eqcnode_merge tv2_node tv1_node;
      eqcg_replace_repr eqcg tv1 (Option.get tv2_repr);
      eqcg_remove_node eqcg (Option.get tv1_repr);
      eqcg_replace_node eqcg (Option.get tv2_repr) tv2_node
    )
  ) else(
    eqcg_connect eqcg tv1 tv1_tag edge1 [tv2,tv2_tag,edge2]
  )

exception Not_interested

let worth_tracking funCFA asm_fun =
  let trans_tvName tvnm =
    match tvnm with
    | TV_addr addrnm ->
      begin match addrnm with
      | {disp=off_str;base=Some reg;index=_} ->
	let off_int = Util.int_of_int32 (Int64.to_int32 (MWord.of_string ("0x"^off_str))) in
	if MWord.(>=%) (MWord.of_string ("0x"^off_str)) (MWord.of_string "0x8048000") && off_int > 0
	then
	  [TV_addr (mkADNM off_str None None)]
	else
	  [TV_reg reg]
      (*
	| {disp=off_str;base=Some reg;index=Some (scl,ndxreg)} ->
	let off_int = Util.int_of_int32 (Int64.to_int32 (MWord.of_string ("0x"^off_str))) in
	if MWord.(>=%) (MWord.of_string ("0x"^off_str)) (MWord.of_string "0x8048000") && off_int > 0
	then
	[TV_addr (mkADNM off_str None None)]
	else
	[TV_reg reg]
      *)
      | {disp=off_str; base=None; index=Some (scl,ndxreg)} ->
	[TV_addr (mkADNM off_str None None)]
      | _ ->
	[tvnm]
      end
    | TV_imm _ -> []
    | _ ->
      [tvnm]
  in
  let inner_tvNameGen funCFA op l =
    trans_tvName (tvNameGen funCFA op l)
  in
  List.unique (
    List.flatten (List.map (fun bb ->  
      let addrcount = ref bb.bb_relAddr in
      let l_instr_list = 
	List.map (fun (pre,ins,len) -> 
	  let outaddr = !addrcount in
	  addrcount := len + !addrcount;
	  outaddr, (pre,ins,len)
	) bb.bb_instrs 
      in
      List.flatten (List.map (fun (l, (pre,ins,len)) ->
	if is_trivial ins then []
	else match ins with
	| PUSH(width,op) ->
	  let tvNameOp = inner_tvNameGen funCFA op l in
	  let currstack = Address_op 
	    {addrDisp=Big.zero; addrBase=Some ESP; addrIndex=None;}
	  in
	  let tvNameCurrStack = inner_tvNameGen funCFA currstack (l+len) in
	  tvNameOp @ tvNameCurrStack
	| MOV(width,opd,ops) -> 
	  let tvNameOpd = inner_tvNameGen funCFA opd l in
	  let tvNameOps = inner_tvNameGen funCFA ops l in
	  tvNameOpd@tvNameOps
	| POP(op) -> 
	  let tvNameOp = inner_tvNameGen funCFA op l in
	  let currstack = Address_op 
	    {addrDisp=Big.zero; addrBase=Some ESP; addrIndex=None;}
	  in
	  let tvNameCurrStack = inner_tvNameGen funCFA currstack (l+len) in
	  tvNameOp@tvNameCurrStack
	| LEAVE -> 
	  let ops = Reg_op EBP in
	  let opd = Reg_op ESP in
	  let tvNameOps = inner_tvNameGen funCFA ops l in
	  let tvNameOpd = inner_tvNameGen funCFA opd (l+len) in
	    (*let op = ops in
	      let tvNameOp = inner_tvNameGen funCFA op (l+len) in*)
	  let currstack = Address_op 
	    {addrDisp=Big.of_int (-4); addrBase=Some ESP; addrIndex=None;}
	  in
	  let tvNameCurrStack = inner_tvNameGen funCFA currstack (l+len) in
	  tvNameOps@tvNameOpd@tvNameCurrStack
	| ADD(width,opd,ops) -> 
	  let tvNameOpd = inner_tvNameGen funCFA opd l in
	  tvNameOpd
	| SUB(width,opd,ops) -> 
	  let tvNameOpd = inner_tvNameGen funCFA opd l in
	  tvNameOpd
	| LEA(opd,ops) -> 
	  let tvNameOpd = inner_tvNameGen funCFA opd l in
	  let tvNameOps = inner_tvNameGen funCFA ops l in
	  tvNameOpd@tvNameOps
	| CMP(width,ops1,ops2) -> 
	  let tvNameOps1 = inner_tvNameGen funCFA ops1 l in
	  let tvNameOps2 = inner_tvNameGen funCFA ops2 l in
	  tvNameOps1@tvNameOps2
	| CALL(dis,abs,op,slctr) 
	| JMP(dis,abs,op,slctr) ->
	  begin match op with
	  | Imm_op _ -> []
	  | _ ->
	    let tvNameOp = inner_tvNameGen funCFA op l in
	    tvNameOp
	  end
	| TEST(width,ops1,ops2) ->
	  let tvNameOps1 = inner_tvNameGen funCFA ops1 l in
	  let tvNameOps2 = inner_tvNameGen funCFA ops2 l in
	  tvNameOps1@tvNameOps2
	| _ -> []
      ) l_instr_list)
    ) asm_fun.func_bbs)
  )
  
let consrv_worth_tracking funCFA asm_fun =
  let inner_tvNameGen funCFA op l =
    let tvnm = tvNameGen funCFA op l in
    let tv,_ = mkTV tvnm l false in
    match tv.tv_name with
    | TV_cfa off when off < (-4) ->
      [tv.tv_name]
    | TV_reg reg when reg <> ESP -> 
      [tv.tv_name]
    | _ -> []
  in
  let tvnm_of_ops op_list l =
    List.flatten (List.map (fun op ->
      inner_tvNameGen funCFA op l
    ) op_list)
  in
  List.unique (
    List.flatten (List.map (fun bb ->  
      let addrcount = ref bb.bb_relAddr in
      let l_instr_list = 
	List.map (fun (pre,ins,len) -> 
	  let outaddr = !addrcount in
	  addrcount := len + !addrcount;
	  outaddr, (pre,ins,len)
	) bb.bb_instrs 
      in
      List.flatten (List.map (fun (l, (pre,ins,len)) ->
	if is_trivial ins then []
	else match ins with
	| PUSH(_,op)
	| POP(op) 
	| NEG(_,op) 
	| NOT(_,op)
	| SHL(_,op,_) 
	| SHR(_,op,_)
	| SAR(_,op,_)
	| ROL(_,op,_)
	| ROR(_,op,_)
	| SETcc(_,op) -> tvnm_of_ops [op] l

	| MOV(_,opd,ops)
	| MOVZX(_,opd,ops)
	| MOVSX(_,opd,ops)
	| LEA(opd,ops)
	| CMOVcc(_,opd,ops)
	| XOR(_,opd,ops)
	| AND(_,opd,ops)
	| OR(_,opd,ops)

	| XCHG(_,opd,ops) -> tvnm_of_ops [opd;ops] l

	| SHLD(_,reg,_)
	| SHRD(_,reg,_)
   	| BSWAP reg -> tvnm_of_ops [Reg_op reg] l

	| ADD(width,opd,ops) -> 
	  let tvNameOpd = inner_tvNameGen funCFA opd l in
	  tvNameOpd
	| SUB(width,opd,ops) -> 
	  let tvNameOpd = inner_tvNameGen funCFA opd l in
	  tvNameOpd
	| CMP(width,ops1,ops2) -> 
	  let tvNameOps1 = inner_tvNameGen funCFA ops1 l in
	  let tvNameOps2 = inner_tvNameGen funCFA ops2 l in
	  tvNameOps1@tvNameOps2
	| CALL(dis,abs,op,slctr) 
	| JMP(dis,abs,op,slctr) ->
	  begin match op with
	  | Imm_op _ -> []
	  | _ ->
	    let tvNameOp = inner_tvNameGen funCFA op l in
	    tvNameOp
	  end
	| TEST(width,ops1,ops2) ->
	  let tvNameOps1 = inner_tvNameGen funCFA ops1 l in
	  let tvNameOps2 = inner_tvNameGen funCFA ops2 l in
	  tvNameOps1@tvNameOps2
	| _ -> []
      ) l_instr_list)
    ) asm_fun.func_bbs)
  )

let aggres_worth_tracking funCFA asm_fun =
  let inner_tvNameGen funCFA op l =
    let tvnm = tvNameGen funCFA op l in
    let tv,_ = mkTV tvnm l false in
    match tv.tv_name with
    | TV_cfa off when off < (-4) ->
      [tv.tv_name]
    | TV_reg reg when reg <> ESP -> 
      [tv.tv_name]
    | _ -> []
  in
  let tvnm_of_ops op_list l =
    List.flatten (List.map (fun op ->
      inner_tvNameGen funCFA op l
    ) op_list)
  in
  List.unique (
    List.flatten (List.map (fun bb ->  
      let addrcount = ref bb.bb_relAddr in
      let l_instr_list = 
	List.map (fun (pre,ins,len) -> 
	  let outaddr = !addrcount in
	  addrcount := len + !addrcount;
	  outaddr, (pre,ins,len)
	) bb.bb_instrs 
      in
      List.flatten (List.map (fun (l, (pre,ins,len)) ->
	if is_trivial ins then []
	else match ins with
	| MOV(_,opd,ops) -> tvnm_of_ops [opd;ops] l
	(*| MOVZX(_,opd,ops)
	| MOVSX(_,opd,ops)*)
	| LEA(opd,ops)
	| CMOVcc(_,opd,ops) -> tvnm_of_ops [opd;ops] l
	(*| XCHG(_,opd,ops) -> tvnm_of_ops [opd;ops] l*)

	| CALL(dis,abs,op,slctr) 
	| JMP(dis,abs,op,slctr) ->
	  begin match op with
	  | Imm_op _ -> []
	  | _ ->
	    let tvNameOp = inner_tvNameGen funCFA op l in
	    tvNameOp
	  end
	| _ -> []
      ) l_instr_list)
    ) asm_fun.func_bbs)
  )

let all_cfa funCFA =
  let depth = ref 0 in
 
  let find_off cfarepr =
    List.iter (fun (lv,re) -> 
      if lv = 0 then
	let _, reg_in_cfa = reverse_cfa_expr re in
        let currdepth = 
	  match reg_in_cfa with
	  | Coq_arith_rtl_exp(_,bvop,re1,re2) ->
	    begin match re1 with
	    | Coq_get_ps_reg_rtl_exp(_,ps_reg) when Big.to_int ps_reg = (-1) ->
	      begin match bvop with
	      | Coq_add_op ->
		begin match re2 with
		| Coq_imm_rtl_exp (_,v) -> 
	          -(Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int v)))
		| _ -> 
		  assert false
		end
	      | Coq_sub_op ->
		begin match re2 with
		| Coq_imm_rtl_exp (_,v) -> 
		  Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int v))
		| _ -> 
		  assert false
		end
	      | _ -> 
		assert false
	      end
	    | _ -> 
	      assert false
	    end
	  | Coq_get_ps_reg_rtl_exp(_,ps_reg) when Big.to_int ps_reg = (-1) ->
	    0
	  | _ -> assert false
	in
	if !depth < currdepth then
	  depth := currdepth
    ) cfarepr
  in
 
  List.iter (fun (lopc,cfaexpr) ->
    find_off cfaexpr
  ) funCFA;
  assert ((!depth mod 4) = 0);
  List.init (!depth/4 - 1) (fun i ->
    TV_cfa (-(i+2)*4)
  ) 

let worth_cfa funCFA asm_fun =
  let inner_tvNameGen funCFA op l =
    let tvnm = tvNameGen funCFA op l in
    match tvnm with
    | TV_cfa _ -> [tvnm]
    | _ -> []
  in
  List.unique (
    List.flatten (List.map (fun bb ->  
      let addrcount = ref bb.bb_relAddr in
      let l_instr_list = 
	List.map (fun (pre,ins,len) -> 
	  let outaddr = !addrcount in
	  addrcount := len + !addrcount;
	  outaddr, (pre,ins,len)
	) bb.bb_instrs 
      in
      List.flatten (List.map (fun (l, (pre,ins,len)) ->
	if is_trivial ins then []
	else match ins with
	| MOV(width,opd,ops) -> 
	  let tvNameOpd = inner_tvNameGen funCFA opd l in
	  let tvNameOps = inner_tvNameGen funCFA ops l in
	  tvNameOpd@tvNameOps
	| ADD(width,opd,ops) -> 
	  let tvNameOpd = inner_tvNameGen funCFA opd l in
	  tvNameOpd
	| SUB(width,opd,ops) -> 
	  let tvNameOpd = inner_tvNameGen funCFA opd l in
	  tvNameOpd
	| LEA(opd,ops) -> 
	  let tvNameOpd = inner_tvNameGen funCFA opd l in
	  let tvNameOps = inner_tvNameGen funCFA ops l in
	  tvNameOpd@tvNameOps
	| CALL(dis,abs,op,slctr) 
	| JMP(dis,abs,op,slctr) ->
	  begin match op with
	  | Imm_op _ -> []
	  | _ ->
	    let tvNameOp = inner_tvNameGen funCFA op l in
	    tvNameOp
	  end
	| _ -> []
      ) l_instr_list)
    ) asm_fun.func_bbs)
  )

let define_worth_tracking funCFA asm_fun =
  let registers = (*ESP :: *)[EAX;ECX;EDX;EBX;ESI;EDI;EBP] in
  let regs_tvnms = List.map (fun reg -> TV_reg reg) registers in
  let stack_tvnms =
    worth_cfa funCFA asm_fun
    (*all_cfa funCFA *)
  in
  regs_tvnms @ stack_tvnms

type range = 
| R_all
| R_range of int * int
    
let in_range addr range =
  match range with
  | R_all -> true
  | R_range(lo,hi) -> 
    (* WARNING: range may overlap from debugging information *)
    lo <= addr && addr < hi

let str_of_range rng =
  match rng with
  | R_all -> "all"
  | R_range(lo,hi) ->
    P.sprintf "(%x,%x)" lo hi

type dbg_type_info = (tvName * range * extended_sctype) list

let dump_dbgTypeInfo dbgTypeInfo =
  List.iter (fun (asm_fun, dbgtype) ->
    P.printf "Function %s:\n" asm_fun.func_name;
    List.iter (fun (tvnm,rng,ectype) ->
      P.printf "%s %s\n%s\n" (str_of_tvnm tvnm) (str_of_range rng) (str_of_ectype ectype)
    ) dbgtype
  ) dbgTypeInfo

type type_relation = 
  {
    mutable tr_var: typeVar;
    mutable tr_val: extended_sctype;
  }

let mkTR trVar trVal = {tr_var = trVar; tr_val = trVal}

type type_env = type_relation list

let dump_type_env tenv =
  P.printf "Type Environment:\n";
  List.iter (fun tr ->
    P.printf "%s = %s\n" (str_of_tv tr.tr_var) (str_of_ectype tr.tr_val)
  ) tenv
    
let collect_global_typeinfo elf =
  let start = MWord.of_string "0x8048000" in
  let resultdir = P.sprintf "%s_type" elf.fname in
  let resultfilename = P.sprintf "%s/global_typeinfo.txt" resultdir in
  if Sys.file_exists elf.fname then (
    let cmd = 
      P.sprintf "./dbg/collect_global_typeinfo %s > %s" elf.fname resultfilename 
    in
    (*P.printf "collect elf.fname %s: %s\n" elf.fname target_name;*) 
    let exit_code = if not (Sys.file_exists resultfilename) then Sys.command cmd else -1 in
    (*P.printf "exit_code: %d\n" exit_code; *)
    if exit_code <> (-1) then
      P.printf "collect global elf.fname %s with exit code -1\n" elf.fname;
    flush stdout;
    let ic = open_in resultfilename in
    let vfps = ref [] in
    let sinfo_count = ref 0 in
    (*let frame_base = ref "none" in*)
    let () = 
      try
	(*
	let frame_base_indicator = input_line ic in
	if frame_base_indicator = "Frame base" then
	  frame_base := input_line ic;
	*)
	while true do 
	  sinfo_count := !sinfo_count + 1;
	  let visisrc_e = input_line ic in
	  let vs = decode_visisrc visisrc_e in
	  let indicator = input_line ic in
	  let ctype = 
	    try decode_c_type indicator 
	    with WrongTypeEncoding ->
	      let () = 
		P.printf "Error! Now in file %s, decoding number %d sinfo entry!\n" resultfilename !sinfo_count; 
		flush stdout
	      in 
	      C_wrong_type "debugging"
	  in
	  let sinfo_start = input_line ic in
	  if sinfo_start <> "{" then (
	    print_endline "Error: unknown encoding after type encoding\n"; 
	    P.printf "in file %s at entry %d\n" resultfilename !sinfo_count;
	    flush stdout; 
	    exit 1)
	  else
	    let first_line = ref (input_line ic) in
	    let locdesc_list = ref [] in
	    while !first_line <> "}" do
	      if !first_line = "NULL" then (
		(*let vfp_entry = mkvfp ctype vs [] in
		vfps := !vfps @ [vfp_entry];*)
		first_line := input_line ic
	      )
	      else (
		let lopc_e = !first_line in
		let hipc_e = input_line ic in
		let lopc = MWord.of_string lopc_e in
		let hipc = MWord.of_string hipc_e in
		let lopc_rel =
		  if MWord.(>=%) lopc start then get_relative_addr lopc start 
		  else MWord.to_int lopc
		in
		let hipc_rel =
		  if MWord.(>=%) hipc start then get_relative_addr hipc start 
		  else MWord.to_int hipc
		in
		(* 
		   let le_start = input_line ic in
		   if le_start <> "Le:" then ( simple_warn "Error: unknown things after location expression start string, le"; exit 1 )
		   else
		*)
		let locexpr_list = ref [] in
		let curr_expr = ref (input_line ic) in
		while !curr_expr <> "" do
		  locexpr_list := !locexpr_list @ [!curr_expr];
		  curr_expr := input_line ic
		done;
		let locdesc_entry = mkdesc lopc_rel hipc_rel !locexpr_list in
		locdesc_list := !locdesc_list @ [locdesc_entry];
		first_line := input_line ic
	      )
	    done;
	    let vfp_entry = mkvfp ctype vs !locdesc_list in
	    vfps := !vfps @ [vfp_entry]
	done
      with End_of_file ->
	close_in ic
    in
    (*let () = List.iter print_vfp !vfps in*)
    (*let _ = Sys.command (P.sprintf "rm %s" resultfilename) in*)
    !vfps
  )
  else (
    P.printf "No file %s exists!" resultfilename; flush stdout;
    []
  )

let collect_visible_typeinfo elf target_name =
  let start = MWord.of_string "0x8048000" in
  let resultdir = P.sprintf "%s_type" elf.fname in
  let resultfilename = P.sprintf "%s/visible_typeinfo_%s.txt" resultdir target_name in
  let void = C_base "void" in
  let size_t = C_typedef ("size_t", C_base "unsinged int") in
  let ptr ct = C_pointer ct in
  let fp ret paramlst= C_subroutine (ret, paramlst) in
  if target_name = "deregister_tm_clones" then
    "", [mkvfp (fp void [ptr void]) VS_unknown []]
  else if target_name = "register_tm_clones" then
    "", [mkvfp (fp void [ptr void;size_t ]) VS_unknown []]
  else if target_name = "frame_dummy" then
    let fstvfp = mkvfp (fp void [void]) VS_unknown [] in
    let sndvfp = mkvfp (fp void [ptr void]) VS_unknown [] in
    "", [fstvfp; sndvfp]
  else if Sys.file_exists elf.fname then (
    let cmd = 
      P.sprintf "./dbg/collect_visible_typeinfo %s %s > %s" elf.fname target_name resultfilename 
    in
    (*P.printf "collect elf.fname %s: %s\n" elf.fname target_name;*) 
    let exit_code = if not (Sys.file_exists resultfilename) then Sys.command cmd else -1 in
    (*P.printf "exit_code: %d\n" exit_code; *)
    if exit_code <> (-1) then
      P.printf "collect elf.fname %s: %s with exit code -1\n" elf.fname target_name;
    flush stdout;
    let ic = open_in resultfilename in
    let vfps = ref [] in
    let sinfo_count = ref 0 in
    let frame_base = ref "none" in
    let () = 
      try
	let frame_base_indicator = input_line ic in
	if frame_base_indicator = "Frame base" then
	  frame_base := input_line ic;
	while true do 
	  sinfo_count := !sinfo_count + 1;
	  let visisrc_e = input_line ic in
	  let vs = decode_visisrc visisrc_e in
	  let indicator = input_line ic in
	  let ctype = 
	    try decode_c_type indicator 
	    with WrongTypeEncoding ->
	      let () = 
		P.printf "Error! Now in file %s, decoding number %d sinfo entry!\n" resultfilename !sinfo_count; 
		flush stdout
	      in 
	      C_wrong_type "debugging"
	  in
	  let sinfo_start = input_line ic in
	  if sinfo_start <> "{" then (
	    print_endline "Error: unknown encoding after type encoding\n"; 
	    P.printf "in file %s at entry %d\n" resultfilename !sinfo_count;
	    flush stdout; 
	    exit 1)
	  else
	    let first_line = ref (input_line ic) in
	    let locdesc_list = ref [] in
	    while !first_line <> "}" do
	      if !first_line = "NULL" then (
		(*let vfp_entry = mkvfp ctype vs [] in
		vfps := !vfps @ [vfp_entry];*)
		first_line := input_line ic
	      )
	      else (
		let lopc_e = !first_line in
		let hipc_e = input_line ic in
		let lopc = MWord.of_string lopc_e in
		let hipc = MWord.of_string hipc_e in
		let lopc_rel =
		  if MWord.(>=%) lopc start then get_relative_addr lopc start 
		  else MWord.to_int lopc
		in
		let hipc_rel =
		  if MWord.(>=%) hipc start then get_relative_addr hipc start 
		  else MWord.to_int hipc
		in
		(* 
		   let le_start = input_line ic in
		   if le_start <> "Le:" then ( simple_warn "Error: unknown things after location expression start string, le"; exit 1 )
		   else
		*)
		let locexpr_list = ref [] in
		let curr_expr = ref (input_line ic) in
		while !curr_expr <> "" do
		  locexpr_list := !locexpr_list @ [!curr_expr];
		  curr_expr := input_line ic
		done;
		let locdesc_entry = mkdesc lopc_rel hipc_rel !locexpr_list in
		locdesc_list := !locdesc_list @ [locdesc_entry];
		first_line := input_line ic
	      )
	    done;
	    let vfp_entry = mkvfp ctype vs !locdesc_list in
	    vfps := !vfps @ [vfp_entry]
	done
      with End_of_file ->
	close_in ic
    in
    (*let () = List.iter print_vfp !vfps in*)
    (*let _ = Sys.command (P.sprintf "rm %s" resultfilename) in*)
    !frame_base,!vfps
  )
  else (
    P.printf "No file %s exists!" resultfilename; flush stdout;
    ("none", [])
  )

let interp_locexprs frame_base funCFA locexprs range =
  if List.mem "DW_OP_stack_value" locexprs then []
  else if List.length locexprs <> 1 then [] (* do not support currently *)
  else 
    let locexpr = List.hd locexprs in
    if String.exists locexpr "DW_OP_reg" then
      let op = String.strip locexpr in
      match op with
      | "DW_OP_reg0" -> [TV_reg EAX]
      | "DW_OP_reg1" -> [TV_reg ECX]
      | "DW_OP_reg2" -> [TV_reg EDX]
      | "DW_OP_reg3" -> [TV_reg EBX]
      | "DW_OP_reg4" -> [TV_reg ESP]
      | "DW_OP_reg5" -> [TV_reg EBP]
      | "DW_OP_reg6" -> [TV_reg ESI]
      | "DW_OP_reg7" -> [TV_reg EDI]
      | _ -> []
    else if String.exists locexpr "DW_OP_fbreg" then
      let op,off_str = String.split locexpr " " in
      let off_big_v = Big.of_int (int_of_string off_str) in
      let change_frame_base fbreg funCFA =
	let lvre_list = List.flatten ( 
	  List.filter_map (fun (relAddr,lvre_list) ->
	    if in_range relAddr range then
	      Some lvre_list
	    else None
	  ) funCFA
	)
	in
	let cfa_name_list = List.filter_map (fun (lv,re) -> 
	  let reg, reg_in_cfa = if lv > 0 then split_at_eq re
	    else reverse_cfa_expr re
	  in
	  if reg = fbreg then
	    let op_cfa_expr = rtl_add_big reg_in_cfa off_big_v in
	    let addr_name = mkADNM off_str (Some fbreg) None in
	    Some (transform_cfa_expr op_cfa_expr addr_name)
	  else
	    None
	) lvre_list
	in
	let unique_name_list = List.unique cfa_name_list in
	if List.length unique_name_list = 1 then
	  [List.hd unique_name_list]
	else
	  let errmsg = P.sprintf "%s frame base is not uniformed" (str_of_reg fbreg) in 
	  raise (Failure errmsg)
      in
      match frame_base with
      | "DW_OP_call_frame_cfa" -> 
	let off = int_of_string off_str in
	[TV_cfa off]
      (** TODO: check correctness: interp different frame base into CFA **)
      (** (relAddr * ((level * rtl_exp) list)) list **)
      | "DW_OP_breg0" -> 
	(*let addrname = mkADNM off_str (Some EAX) None in*)
	change_frame_base EAX funCFA
      | "DW_OP_breg1" -> 
	change_frame_base ECX funCFA
      | "DW_OP_breg2" -> 
	change_frame_base EDX funCFA
      | "DW_OP_breg3" -> 
	change_frame_base EBX funCFA
      | "DW_OP_breg4" -> 
	change_frame_base ESP funCFA
      | "DW_OP_breg5" ->
	change_frame_base EBP funCFA
      | "DW_OP_breg6" ->
	change_frame_base ESI funCFA
      | "DW_OP_breg7" -> 
	change_frame_base EDI funCFA
      | _ -> []
    (*
    else if String.exists locexpr "DW_OP_breg" then
      (** OPTIMIZE: such situation may be useless **)
      let op,off_str = String.split locexpr " " in
      match op with
      | "DW_OP_breg0" -> 
	let addrname = mkADNM off_str (Some EAX) None in
	[TV_addr addrname]
      | "DW_OP_breg1" -> 
	let addrname = mkADNM off_str (Some ECX) None in
	[TV_addr addrname]
      | "DW_OP_breg2" -> 
	let addrname = mkADNM off_str (Some EDX) None in
	[TV_addr addrname]
      | "DW_OP_breg3" -> 
	let addrname = mkADNM off_str (Some EBX) None in
	[TV_addr addrname]
      | "DW_OP_breg4" -> 
	let addrname = mkADNM off_str (Some ESP) None in
	[TV_addr addrname]
      | "DW_OP_breg5" ->
	let addrname = mkADNM off_str (Some EBP) None in
	[TV_addr addrname]
      | "DW_OP_breg6" ->
	let addrname = mkADNM off_str (Some ESI) None in
	[TV_addr addrname]
      | "DW_OP_breg7" -> 
	let addrname = mkADNM off_str (Some EDI) None in
	[TV_addr addrname]
      | _ -> []
    *)
    else if String.exists locexpr "DW_OP_addr" then
      let op,addr = String.split locexpr " " in
      let abs_addr_str = str_of_mword_flex (MWord.of_string addr) in
      let addrname = mkADNM abs_addr_str None None in
      [TV_addr addrname; TV_imm abs_addr_str]
    else 
      []

let trans_raw_dbgtypeinfo frame_base funCFA raw_dbg =
  let rec is_memchunk sctype =
    match sctype with
    | SC_pointer (SC_subroutine _) -> false
    | SC_pointer _ -> false
    | SC_array _ -> true
    | SC_structure _ -> true
    | SC_union _ -> false
    | SC_const sct | SC_typedef (_,sct) | SC_volatile sct ->
      is_memchunk sct
    | _ -> false
  in
  let rec type_of_addr sctype =
    if is_memchunk sctype then
      sctype
    else
      SC_pointer sctype
  in
  List.flatten (
    List.map (fun (locexprs,rng,sctype) ->
      let range = 
	if snd rng = (-1) then R_all
	else R_range (fst rng, snd rng)
      in
      let locs = interp_locexprs frame_base funCFA locexprs range in
      List.map (fun loc -> 
	match loc with
	| TV_imm _ ->
	  loc,range,[type_of_addr sctype]
	| TV_addr _ ->
	  loc,range,[type_of_addr sctype]
	| _ -> 
	  loc,range,[sctype]
      ) locs
    ) raw_dbg
  )

let rec ctype_to_sctype ctype = 
  match ctype with
  | C_base name -> SC_base name 
  | C_const ct -> SC_const (ctype_to_sctype ct)
  | C_array (ct,dimlst) -> 
    SC_array ((ctype_to_sctype ct),dimlst)
  | C_pointer ct -> 
    SC_pointer (ctype_to_sctype ct)
  | C_typedef (typedef_name, ct) ->
    SC_typedef (typedef_name,(ctype_to_sctype ct))
  | C_subroutine (ret_ct, param_ct_list) ->
    SC_subroutine ((ctype_to_sctype ret_ct), (List.map ctype_to_sctype param_ct_list))
  | C_enumeration (enumname, etor_list) -> 
    SC_enumeration (enumname, etor_list)
  | C_union (unionname, umem_list) -> 
    SC_union (unionname, (List.map (fun (umemtype,umemname) -> (ctype_to_sctype umemtype), umemname) umem_list))
  | C_structure (original_name, level_name, smem_list) ->
    SC_structure (original_name, level_name, (List.map (fun (smemtype,smemname) -> (ctype_to_sctype smemtype), smemname) smem_list))
  | C_unspecified_param ->
    SC_unspecified_param
  | C_wrong_type errmsg ->
    SC_wrong_type errmsg
  | C_volatile ct ->
    SC_volatile (ctype_to_sctype ct)

let collect_func_tyinfo elf =
  let start = MWord.of_string "0x8048000" in
  let all_functions = collect_all_functions elf start in
  let funcsig_list_raw = collect_funcsig_list elf start in
  let funcsig_list = configure_funcsig_list_lowpc funcsig_list_raw all_functions in
  List.flatten (
    List.map (fun funcsig ->
      let rng = R_all in
      let ret_ctype = decode_c_type funcsig.func_ret in
      let param_ctypes = List.map decode_c_type funcsig.func_param in
      let ret_sctype = ctype_to_sctype ret_ctype in
      let param_sctypes = List.map ctype_to_sctype param_ctypes in
      let sctype = SC_pointer (SC_subroutine (ret_sctype, param_sctypes)) in
      let func_abs_lopc =
	str_of_mword_flex (MWord.(+%) (MWord.of_int funcsig.func_rel_lowpc) start)
      in
    (*
      let locexprs =
      P.sprintf "DW_OP_addr %s" func_abs_lopc
      in
    *)
      [((TV_imm func_abs_lopc), rng, [sctype]); 
       ((TV_addr (mkADNM func_abs_lopc None None)), rng, [sctype]);
      ]
    ) funcsig_list
  )

let rawtrans_dbgtypeinfo dbgtypeinfo =
  List.flatten (
    List.map (fun vfp ->
      let ctype = vfp.vfp_tsig in
      let sctype = ctype_to_sctype ctype in
      let range_and_location =
	List.map (fun locdesc -> 
	  let range = (locdesc.ld_lopc, locdesc.ld_hipc) in
	  range, locdesc.ld_locexprs
	) vfp.vfp_sinfo
      in
      List.map (fun (rng,locexprs) ->
	locexprs,rng,sctype
      ) range_and_location
    ) dbgtypeinfo
  )
    
let retrieve_fun_dbgtypeinfo elf funCFA asm_fun_name =
  let frame_base, visible_tyinfo = collect_visible_typeinfo elf asm_fun_name in
  let fvfps = 
    List.length (
      List.unique (List.flatten (List.map (fun vfp -> 
	search_for_subroutines_t vfp.vfp_tsig []
      ) visible_tyinfo))
    )
  in
  let raw_dbgtypeinfo = rawtrans_dbgtypeinfo visible_tyinfo in
  let dbgtypeinfo = trans_raw_dbgtypeinfo frame_base funCFA raw_dbgtypeinfo in
  fvfps, dbgtypeinfo

(*
let construct_fun_dbgmap elf funCFA asm_fun =
  let rec is_ptr_or_arr sctype =
    match sctype with
    | SC_pointer (SC_subroutine _) -> false
    | SC_pointer _ -> false
    | SC_array _ -> true
    | SC_const sct | SC_typedef (_,sct) | SC_volatile sct ->
      is_ptr_or_arr sct
    | _ -> false
  in
  let rec type_of_addr sctype =
    if is_ptr_or_arr sctype then
      sctype
    else
      SC_pointer sctype
  in

  let all_indices = 
    List.flatten (
      List.map (fun bb ->
	let addrcount = ref bb.bb_relAddr in 
	List.map (fun (pre,ins,len) ->
	  let outaddr = !addrcount in
	  addrcount := len + !addrcount;
	  outaddr
	) bb.bb_instrs
      ) asm_fun.func_bbs
    )
  in
  let frame_base, visible_tyinfo = collect_visible_typeinfo elf asm_fun.func_name in
  let fun_dbgmap = H.create 32 in

  let update_fun_dbgmap loc index sctype =
    if not H.mem fun_dbgmap (loc,index) then
      H.add fun_dbgmap (loc,index) [sctype]
    else
      let newect = 
	List.unique ?eq:(Some eq_sctype) (sctype :: (H.find fun_dbgmap (loc,index)))
      in
      H.replace fun_dbgmap (loc,index) newect
  in

  List.iter (fun vfp ->
    let sctype = ctype_to_sctype vfp.vfp_tsig in
    let range = 
      if locdesc.ld_hipc < 0 then R_all
      else R_range (locdesc.ld_lopc, locdesc.ld_hipc)
    in
    let covered_indices =
      if locdesc.ld_hipc < 0 then all_indices
      else 
	List.filter (fun index -> 
	  locdesc.ld_lopc <= index && index < locdesc.ld_hipc
	) all_indices
    in
    List.iter (fun locdesc ->
      let locs = interp_locexprs frame_base funCFA locdesc.ld_locexprs range in
      List.iter (fun index ->
	List.iter (fun loc -> 
	  match loc with
	  | TV_addr _ ->
	    update_fun_dbgmap loc index (type_of_addr sctype)
	  | _ -> 
	    update_fun_dbgmap loc index sctype
	) locs
      ) covered_indices      
    ) vfp.vfp_sinfo
  ) visible_tyinfo;
  fun_dbgmap
*)

let rec find_arr sctype struct_map =
  match sctype with
  | SC_array (sct,dimlist) ->
    begin
      try
	let bound = calc_array_bound dimlist in
	let eltsize = size_of_sctype struct_map sct in
	Some (sct,eltsize,bound)
      with _ -> None
    end
  | SC_const sct | SC_typedef (_,sct) | SC_volatile sct ->
    find_arr sct struct_map
  | _ -> None

let rec is_memchunk sctype =
  match sctype with
  | SC_pointer (SC_subroutine _) -> false
  | SC_pointer _ -> false
  | SC_array _ -> true
  | SC_structure _ -> true
  | SC_union _ -> false
  | SC_const sct | SC_typedef (_,sct) | SC_volatile sct ->
    is_memchunk sct
  | _ -> false


let rec find_struct sctype struct_map =
  match sctype with
  | SC_const sct' | SC_volatile sct' -> 
    find_struct sct' struct_map
  | SC_typedef (defnm,sct') ->
    if H.mem struct_map defnm then
      Some (H.find struct_map defnm)
    else
      find_struct sct' struct_map
  | SC_structure (oldnm,_,_) ->
    if H.mem struct_map oldnm then
      Some (H.find struct_map oldnm)
    else
      None
  | _ ->
    None
	
let construct_fun_dbgmap asm_fun funDBG struct_map =
  let all_indices = 
    List.flatten (
      List.map (fun bb ->
	let addrcount = ref bb.bb_relAddr in 
	List.map (fun (pre,ins,len) ->
	  let outaddr = !addrcount in
	  addrcount := len + !addrcount;
	  outaddr
	) bb.bb_instrs
      ) asm_fun.func_bbs
    )
  in
  let fun_dbgmap = H.create 32 in
  List.iter (fun (tvnm,rng,ect) ->
    let covered_indices =
      match rng with
      | R_all -> all_indices
      | R_range (lopc,hipc) ->
	List.filter (fun index -> 
	  lopc <= index && index < hipc
	) all_indices
    in
    match tvnm with
    | TV_imm _       
    | TV_addr _ ->
      begin match rng with
      | R_all -> 
	if not (H.mem fun_dbgmap (tvnm,(-1))) then
	  H.add fun_dbgmap (tvnm,(-1)) ect
	else
	  let newect = 
	    List.unique ?eq:(Some eq_sctype) (ect @ (H.find fun_dbgmap (tvnm,(-1))))
	  in
	  H.replace fun_dbgmap (tvnm,(-1)) newect
      | _ ->
	List.iter (fun index ->
	  if not (H.mem fun_dbgmap (tvnm,index)) then
	    H.add fun_dbgmap (tvnm,index) ect
	  else
	    let newect = 
	      List.unique ?eq:(Some eq_sctype) (ect @ (H.find fun_dbgmap (tvnm,index)))
	    in
	    H.replace fun_dbgmap (tvnm,index) newect
	) covered_indices
      end
    | TV_cfa off ->
      List.iter (fun sct ->
	let arr_option = find_arr sct struct_map in
	if Option.is_some arr_option then
	  let arrsct, eltsize, arrbound = Option.get arr_option in
	  let newtvnms = List.init arrbound (fun i ->
	    TV_cfa (off+i*eltsize)
	  ) in
	  List.iter (fun tvnm' ->
	    List.iter (fun index ->
	      if not (H.mem fun_dbgmap (tvnm',index)) then
		H.add fun_dbgmap (tvnm',index) [arrsct]
	      else
		let newect = 
		  List.unique ?eq:(Some eq_sctype) (arrsct :: (H.find fun_dbgmap (tvnm',index)))
		in
		H.replace fun_dbgmap (tvnm,index) newect
	    ) covered_indices
	  ) newtvnms
	else
	  List.iter (fun index ->
	    if not (H.mem fun_dbgmap (tvnm,index)) then
	      H.add fun_dbgmap (tvnm,index) [sct]
	    else
	      let newect = 
		List.unique ?eq:(Some eq_sctype) (sct :: (H.find fun_dbgmap (tvnm,index)))
	      in
	      H.replace fun_dbgmap (tvnm,index) newect
	  ) covered_indices
      ) ect
    | TV_reg _ -> 
      List.iter (fun index ->
	if not (H.mem fun_dbgmap (tvnm,index)) then
	  H.add fun_dbgmap (tvnm,index) ect
	else
	  let newect = 
	    List.unique ?eq:(Some eq_sctype) (ect @ (H.find fun_dbgmap (tvnm,index)))
	  in
	  H.replace fun_dbgmap (tvnm,index) newect
      ) covered_indices
  ) funDBG;
  fun_dbgmap

let rec find_memchunk sct struct_map =
  match sct with
  | SC_typeVar _ -> 
    assert false
  | SC_const sct' | SC_volatile sct' -> 
    find_memchunk sct' struct_map
  | SC_array _ -> 
    Some sct
  | SC_typedef (defnm,sct') ->
    if H.mem struct_map defnm then
      Some sct
    else
      find_memchunk sct' struct_map
  | SC_structure (oldnm,_,_) ->
    if H.mem struct_map oldnm then
      Some sct
    else (
      P.printf "WARNING: struct map may be incomplete in find_memchunk\n";
      flush stdout;
      None
    )
  | _ ->
    None
      (*raise Not_found*)

let rec unfold_sctype dbgmap sctype tvnm index struct_map =
  match tvnm with
  | TV_reg _ ->
    (**Impossible Cases**)
    assert false
  | TV_cfa _ ->
    assert false
  | TV_imm startAddr
  | TV_addr {disp=startAddr;base=_;index=_} ->
    let memchunk_option = find_memchunk sctype struct_map in
    if Option.is_none memchunk_option then
      if not (H.mem dbgmap (tvnm,index)) then
	H.add dbgmap (tvnm,index) [SC_pointer sctype]
      else
	let newect = 
	  List.unique ?eq:(Some eq_sctype) ((SC_pointer sctype) :: (H.find dbgmap (tvnm,index)))
	in
	H.replace dbgmap (tvnm,index) newect
    else
      let startAddr = MWord.of_string ("0x"^startAddr) in
      let memchunk_type = Option.get memchunk_option in
      begin match memchunk_type with
      | SC_array (arrsct,dimlist) ->
	begin 
	  try
	    let bound = calc_array_bound dimlist in
	    let eltsize = size_of_sctype struct_map arrsct in
	    let newtvnms = List.init (bound-1) (fun i ->
	      match tvnm with
	      | TV_imm _ ->
		TV_imm (str_of_mword_flex (MWord.(+%) startAddr (MWord.of_int ((i+1)*eltsize))))
	      | TV_addr _ ->
		TV_addr (
		  mkADNM (
		    str_of_mword_flex (MWord.(+%) startAddr (MWord.of_int ((i+1)*eltsize)))
		  ) None None
		)
	      | _ -> assert false
	    ) in
	    List.iter (fun tvnm' ->
	      if not (H.mem dbgmap (tvnm',index)) then
		H.add dbgmap (tvnm',index) [SC_pointer arrsct]
	      else
		let newect = 
		  List.unique ?eq:(Some eq_sctype) 
		    ((SC_pointer arrsct) :: (H.find dbgmap (tvnm,index)))
	      in
		H.replace dbgmap (tvnm,index) newect;
	      unfold_sctype dbgmap arrsct tvnm' index struct_map
	    ) newtvnms
	  with _ -> 
	    if not (H.mem dbgmap (tvnm,index)) then
	      H.add dbgmap (tvnm,index) [sctype]
	    else
	      let newect = 
		List.unique ?eq:(Some eq_sctype) 
		  (sctype :: (H.find dbgmap (tvnm,index)))
	      in
	      H.replace dbgmap (tvnm,index) newect
	    
	end
      | SC_typedef (nm,_)
      | SC_structure (nm,_,_) ->
	assert (H.mem struct_map nm);
	let struct_entry = H.find struct_map nm in
	begin match struct_entry.struct_type with
	| SC_structure (_,_,fdlist) ->	  
	  assert (not (List.is_empty fdlist));
	  assert ((List.length fdlist) = (List.length struct_entry.struct_memoff));
	  List.iter2 (fun off (ft,fn) ->
	    let newtvnm = 
	      match tvnm with
	      | TV_addr _ ->
		TV_addr (
		  mkADNM (str_of_mword_flex (MWord.(+%) startAddr (MWord.of_int off))) 
		    None None
		)
	      | TV_imm _ ->
		TV_imm (
		  str_of_mword_flex (MWord.(+%) startAddr (MWord.of_int off))
		)
	      | _ -> assert false
	    in
	    unfold_sctype dbgmap ft newtvnm index struct_map
	  ) struct_entry.struct_memoff fdlist
	| _ -> assert false
	end
      | _ -> assert false
      end	
(*
let rec unfold_sctype2 dbgmap sctype tvnm index struct_map =
  match tvnm with
  | TV_reg _ ->
    (**Impossible Cases**)
    assert false
  | TV_cfa _ ->
    assert false
  | TV_imm startAddr
  | TV_addr {disp=startAddr;base=_;index=_} ->
    let memchunk_option = find_memchunk sctype struct_map in
    if Option.is_none memchunk_option then ()
    else
      let startAddr = MWord.of_string ("0x"^startAddr) in
      let memchunk_type = Option.get memchunk_option in
      begin match memchunk_type with
      | SC_array (arrsct,dimlist) ->
	begin 
	  try
	    let bound = calc_array_bound dimlist in
	    let eltsize = size_of_sctype struct_map arrsct in
	    let newtvnms = List.init (bound-1) (fun i ->
	      match tvnm with
	      | TV_imm _ ->
		TV_imm (str_of_mword_flex (MWord.(+%) startAddr (MWord.of_int ((i+1)*eltsize))))
	      | TV_addr _ ->
		TV_addr (
		  mkADNM (
		    str_of_mword_flex (MWord.(+%) startAddr (MWord.of_int ((i+1)*eltsize)))
		  ) None None
		)
	      | _ -> assert false
	    ) in
	    List.iter (fun tvnm' ->
	      if not (H.mem dbgmap (tvnm',index)) then
		H.add dbgmap (tvnm',index) [SC_pointer arrsct]
	      else
		let newect = 
		  List.unique ?eq:(Some eq_sctype) 
		    ((SC_pointer arrsct) :: (H.find dbgmap (tvnm,index)))
	      in
		H.replace dbgmap (tvnm,index) newect;
	      unfold_sctype dbgmap arrsct tvnm' index struct_map
	    ) newtvnms
	  with _ -> 
	    if not (H.mem dbgmap (tvnm,index)) then
	      H.add dbgmap (tvnm,index) [sctype]
	    else
	      let newect = 
		List.unique ?eq:(Some eq_sctype) 
		  (sctype :: (H.find dbgmap (tvnm,index)))
	      in
	      H.replace dbgmap (tvnm,index) newect
	    
	end
      | SC_typedef (nm,_)
      | SC_structure (nm,_,_) ->
	assert (H.mem struct_map nm);
	let struct_entry = H.find struct_map nm in
	begin match struct_entry.struct_type with
	| SC_structure (_,_,fdlist) ->	  
	  assert (not (List.is_empty fdlist));
	  assert ((List.length fdlist) = (List.length struct_entry.struct_memoff));
	  List.iter2 (fun off (ft,fn) ->
	    let newtvnm = 
	      match tvnm with
	      | TV_addr _ ->
		TV_addr (
		  mkADNM (str_of_mword_flex (MWord.(+%) startAddr (MWord.of_int off))) 
		    None None
		)
	      | TV_imm _ ->
		TV_imm (
		  str_of_mword_flex (MWord.(+%) startAddr (MWord.of_int off))
		)
	      | _ -> assert false
	    in
	    unfold_sctype dbgmap ft newtvnm index struct_map
	  ) struct_entry.struct_memoff fdlist
	| _ -> assert false
	end
      | _ -> assert false
      end	

let deep_unfold_memchunk dbgmap struct_map =
  let unfolded_dbgmap = H.copy dbgmap in
  H.iter (fun (tvnm,index) ect ->
    match tvnm with
    | TV_imm _
    | TV_addr _ ->
      List.iter (fun sct ->
	let memchunk_option = find_memchunk struct_map sct in
	if Option.is_none memchunk_option then
	  if H.mem 
	unfold_sctype unfolded_dbgmap sct tvnm index struct_map
      ) ect
    | _ -> ()
  ) dbgmap;
  unfolded_dbgmap  
  *)
let unfold_global_struct dbgmap struct_map = 
  let unfolded_dbgmap = H.copy dbgmap in
  H.iter (fun (tvnm,index) ect ->
    match tvnm with
    | TV_imm imm ->
      let startAddr = MWord.of_string ("0x"^imm) in
      List.iter (fun sct ->
	let struct_option = find_struct sct struct_map in
	if Option.is_some struct_option then
	  let struct_entry = Option.get struct_option in
	  match struct_entry.struct_type with
	  | SC_structure (_,_,fdlist) ->
	    assert ((List.length fdlist) = (List.length struct_entry.struct_memoff));
	    List.iter2 (fun off (ft,fn) ->
	      let newtvnm = 
		TV_imm (
		  str_of_mword_flex (MWord.(+%) startAddr (MWord.of_int off))
		)
	      in
	      if not (H.mem unfolded_dbgmap (newtvnm,(-1))) then
		H.add unfolded_dbgmap (newtvnm,(-1)) [SC_pointer ft]
	      else
		let newect =
		  if off <> 0 then
		    List.unique ?eq:(Some eq_sctype) 
		      ((SC_pointer ft) :: (H.find unfolded_dbgmap (newtvnm,(-1))))
		  else
		    [SC_pointer ft]
		in
		H.replace unfolded_dbgmap (newtvnm,(-1)) newect
	    ) struct_entry.struct_memoff fdlist
	  | _ -> assert false
	else
	  ()
      ) ect
    | TV_addr addrnm ->
      assert(addrnm.base = None && addrnm.index = None);
      let startAddr = MWord.of_string ("0x"^addrnm.disp) in
      List.iter (fun sct ->
	let struct_option = find_struct sct struct_map in
	if Option.is_some struct_option then
	  let struct_entry = Option.get struct_option in
	  match struct_entry.struct_type with
	  | SC_structure (_,_,fdlist) ->
	    assert ((List.length fdlist) = (List.length struct_entry.struct_memoff));
	    List.iter2 (fun off (ft,fn) ->
	      let newtvnm = 
		TV_addr (
		  mkADNM (str_of_mword_flex (MWord.(+%) startAddr (MWord.of_int off))) 
		    None None
		)
	      in
	      if not (H.mem unfolded_dbgmap (newtvnm,(-1))) then
		H.add unfolded_dbgmap (newtvnm,(-1)) [SC_pointer ft]
	      else
		let newect =
		  if off <> 0 then
		    List.unique ?eq:(Some eq_sctype) 
		      ((SC_pointer ft) :: (H.find unfolded_dbgmap (newtvnm,(-1))))
		  else
		    [SC_pointer ft]
		in
		H.replace unfolded_dbgmap (newtvnm,(-1)) newect
	    ) struct_entry.struct_memoff fdlist
	  | _ -> assert false
	else
	  ()
      ) ect
    | _ -> ()
  ) dbgmap;
  unfolded_dbgmap

let deep_unfold_global_struct dbgmap struct_map = 
  let unfolded_dbgmap = H.copy dbgmap in
  H.iter (fun (tvnm,index) ect ->
    match tvnm with
    | TV_imm _
    | TV_addr _ ->
      List.iter (fun sct ->
	unfold_sctype unfolded_dbgmap sct tvnm index struct_map
      ) ect
    | _ -> ()
  ) dbgmap;
  unfolded_dbgmap

let deep_construct_global_dbgmap dbg struct_map =
  let dbgmap = H.create 32 in
  List.iter (fun (tvnm,rng,ect) ->
    match tvnm with
    | TV_reg _ | TV_cfa _ ->
      if not (H.mem dbgmap (tvnm,(-1))) then
	H.add dbgmap (tvnm,(-1)) ect
      else
	let newect = 
	  List.unique ?eq:(Some eq_sctype) (ect @ (H.find dbgmap (tvnm,(-1))))
	in
	H.replace dbgmap (tvnm,(-1)) newect;
    | _ ->
      List.iter (fun sct ->
	unfold_sctype dbgmap sct tvnm (-1) struct_map
      ) ect
  ) dbg;
  dbgmap

let construct_global_dbgmap globalDBG struct_map =
  let global_dbgmap = H.create 32 in
  List.iter (fun (tvnm,rng,ect) ->
    match tvnm with
    | TV_imm imm ->
      let startAddr = MWord.of_string ("0x"^imm) in
      List.iter (fun sct ->
	let arr_option = find_arr sct struct_map in
	if Option.is_some arr_option then
	  let arrsct, eltsize, arrbound = Option.get arr_option in
	  let newsct = 
	    if is_memchunk arrsct then arrsct
	    else (SC_pointer arrsct)
	  in
	  let newtvnms = List.init arrbound (fun i ->
	    TV_imm (str_of_mword_flex (MWord.(+%) startAddr (MWord.of_int (i*eltsize)))) 
	  ) in
	  List.iter (fun tvnm' ->
	    if not (H.mem global_dbgmap (tvnm',(-1))) then
	      H.add global_dbgmap (tvnm',(-1)) [newsct]
	    else
	      let newect = 
		List.unique ?eq:(Some eq_sctype)
		  (newsct :: (H.find global_dbgmap (tvnm',(-1))))
		  (*((SC_pointer arrsct) :: (H.find global_dbgmap (tvnm',(-1))))*)
	      in
	      H.replace global_dbgmap (tvnm',(-1)) newect
	  ) newtvnms
	else
	  if not (H.mem global_dbgmap (tvnm,(-1))) then
	    H.add global_dbgmap (tvnm,(-1)) [sct]
	  else
	    let newect = 
	      List.unique ?eq:(Some eq_sctype) (sct :: (H.find global_dbgmap (tvnm,(-1))))
	    in
	    H.replace global_dbgmap (tvnm,(-1)) newect
      ) ect
    | TV_addr addrnm ->
      assert(addrnm.base = None && addrnm.index = None);
      let startAddr = MWord.of_string ("0x"^addrnm.disp) in
      List.iter (fun sct ->
	let arr_option = find_arr sct struct_map in
	if Option.is_some arr_option then
	  let arrsct, eltsize, arrbound = Option.get arr_option in
	  let newsct =
	    if is_memchunk arrsct then arrsct
	    else (SC_pointer arrsct)
	  in
	  let newtvnms = List.init arrbound (fun i ->
	    TV_addr 
	      (mkADNM (str_of_mword_flex (MWord.(+%) startAddr (MWord.of_int (i*eltsize)))) None None) 
	  ) in
	  List.iter (fun tvnm' ->
	    if not (H.mem global_dbgmap (tvnm',(-1))) then
	      H.add global_dbgmap (tvnm',(-1)) [newsct]
	    else
	      let newect = 
		List.unique ?eq:(Some eq_sctype) 
		  (newsct :: (H.find global_dbgmap (tvnm',(-1))))
	      in
	      H.replace global_dbgmap (tvnm',(-1)) newect
	  ) newtvnms
	else
	  if not (H.mem global_dbgmap (tvnm,(-1))) then
	    H.add global_dbgmap (tvnm,(-1)) [sct]
	  else
	    let newect = 
	      List.unique ?eq:(Some eq_sctype) (sct :: (H.find global_dbgmap (tvnm,(-1))))
	    in
	    H.replace global_dbgmap (tvnm,(-1)) newect
      ) ect
    | _ ->
      if not (H.mem global_dbgmap (tvnm,(-1))) then
	H.add global_dbgmap (tvnm,(-1)) ect
      else
	let newect = 
	  List.unique ?eq:(Some eq_sctype) (ect @ (H.find global_dbgmap (tvnm,(-1))))
	in
	H.replace global_dbgmap (tvnm,(-1)) newect
  ) globalDBG;
  global_dbgmap
    
let construct_global_dbgmap_consrv globalDBG struct_map =
  let global_dbgmap = H.create 32 in
  List.iter (fun (tvnm,rng,ect) ->
    if not (H.mem global_dbgmap (tvnm,(-1))) then
      H.add global_dbgmap (tvnm,(-1)) ect
    else
      let newect = 
	List.unique ?eq:(Some eq_sctype) (ect @ (H.find global_dbgmap (tvnm,(-1))))
      in
      H.replace global_dbgmap (tvnm,(-1)) newect
  ) globalDBG;
  global_dbgmap

let enrich_struct_map struct_map dbgTypeInfo memmap =
  let rec add_struct sctype =
    match sctype with
    | SC_typedef (defnm,sct) ->
      begin match sct with
      | SC_structure (oldnm,_,fieldlist) ->
	if not (List.is_empty fieldlist) then (
	  if not (H.mem struct_map defnm) then
	    let _,size,memoff = 
	      try
		List.find (fun (nm,sz,memoff) ->
		  nm = defnm
		) memmap 
	      with Not_found ->
		let errmsg = P.sprintf "struct %s not found!" defnm in
		raise (Failure errmsg)
	    in
	    let newstruct = mkSTRUCT sct size memoff in 
	    H.add struct_map defnm newstruct
	  else if oldnm <> "" && not (H.mem struct_map oldnm) then
	    let _,size,memoff =
	      try
		List.find (fun (nm,sz,memoff) ->
		  nm = oldnm
		) memmap
	      with Not_found ->
		let errmsg = P.sprintf "struct %s not found!" oldnm in
		raise (Failure errmsg)
	    in
	    let newstruct = mkSTRUCT sct size memoff in 
	    H.add struct_map oldnm newstruct
	);
	List.iter (fun (ft,fn) -> add_struct ft) fieldlist
      | _ -> 
	add_struct sct
      end
    | SC_structure (oldnm,_,fieldlist) ->
      if oldnm <> "" then (
	if not ((List.is_empty fieldlist) || (H.mem struct_map oldnm)) then
	  let _,size,memoff = 
	    try
	      List.find (fun (nm,sz,memoff) ->
		nm = oldnm
	      ) memmap 
	    with Not_found ->
	      let errmsg = P.sprintf "struct %s not found!" oldnm in
	      raise (Failure errmsg)
	  in
	  let newstruct = mkSTRUCT sctype size memoff in
	  H.add struct_map oldnm newstruct
      );
      List.iter (fun (ft,fn) -> add_struct ft) fieldlist
    | SC_const sct | SC_volatile sct
    | SC_array (sct,_) | SC_pointer sct ->
      add_struct sct
    | SC_subroutine (ret,paramlist) ->
      add_struct ret;
      List.iter (fun ft -> add_struct ft) paramlist
    | SC_union (_,memlist) ->
      List.iter (fun (ft,fn) -> add_struct ft) memlist
    | _ -> ()
  in
  List.iter (fun (tvnm,rng,ectype) ->
    List.iter (fun sctype ->
      add_struct sctype
    ) ectype
  ) dbgTypeInfo

(*type struct_map = (string,struct_type) H.t*)

let collect_memmap elf =
  let resultdir = P.sprintf "%s_type" elf.fname in
  let resultfilename = P.sprintf "%s/struct_memmap.txt" resultdir in
  let cmd = 
    P.sprintf "./dbg/collect_struct_memmap %s > %s" elf.fname resultfilename
  in
  let _ = if not (Sys.file_exists resultfilename) then Sys.command cmd else -1 in
  let ic = open_in resultfilename in
  let memmap = ref [] in
  let currnm = ref "" in
  let currsz = ref 0 in
  let currmemoff = ref [] in
  let () = 
    try
      while true do
	let line = input_line ic in
	if String.exists line ":" then (
	  if !currnm <> "" && !currsz <> 0 then (
	    memmap := (!currnm,!currsz,!currmemoff) :: !memmap;
	    currmemoff := []
	  );
	  let nm,sz = String.split line ":" in
	  currnm := nm;
	  currsz := int_of_string sz
	) else
	  let off = int_of_string line in
	  currmemoff := !currmemoff @ [off]
      done
    with End_of_file ->
      if !currnm <> "" && !currsz <> 0 then (
	memmap := (!currnm,!currsz,!currmemoff) :: !memmap;
	currmemoff := []
      ); 
      close_in ic
  in
  (*
  let rmcmd = P.sprintf "rm %s" resultfilename in
  let _ = Sys.command rmcmd in*)
  (*List.iter (fun (nm,sz,memoff) -> 
    P.printf "%s:%d {\n" nm sz;
    List.iter (fun off -> 
      P.printf "%d\n" off
    ) memoff;
    P.printf "}\n"
  ) !memmap;*)
  !memmap

let dbgTypeGen elf cfaInfo asm_fun_list =
  let global_struct_map = H.create 32 in
  let memmap = collect_memmap elf in
  let global_typeinfo = collect_global_typeinfo elf in
  let globalDBG = trans_raw_dbgtypeinfo "" [] (**theoretically we don't need these**)
    (rawtrans_dbgtypeinfo global_typeinfo) in
  let func_tyinfo = collect_func_tyinfo elf in
  let globalDBG = globalDBG @ func_tyinfo in
  enrich_struct_map global_struct_map globalDBG memmap;
  List.map (fun asm_fun ->
    let _,funCFA = List.find (fun (func,cfa_repr) ->
      func.func_name = asm_fun.func_name
    ) cfaInfo 
    in
    let _, asm_fun_dbg = retrieve_fun_dbgtypeinfo elf funCFA asm_fun.func_name in
    enrich_struct_map global_struct_map asm_fun_dbg memmap;
    asm_fun, asm_fun_dbg
  ) asm_fun_list, global_struct_map, memmap

let dbgtype_tag tv dbgTypeInfo =
  (** Expand dbgTypeInfo instead of having complicated dbgtag **)
  let fun_dbgmap, global_dbgmap = dbgTypeInfo in
  if tv.tv_crossbb then []
  else
    let tvnm = tv.tv_name in
    let index = tv.tv_index in
    match tvnm with
    | TV_imm _ | TV_addr _ ->
      begin 
	try
	  H.find global_dbgmap (tvnm,(-1))
	with Not_found ->
	  begin 
	    try
	      H.find fun_dbgmap (tvnm,(-1))
	    with Not_found ->
	      []
	  end
      end
    | TV_cfa _ | TV_reg _ ->
      begin 
	try
	  H.find fun_dbgmap (tvnm,index)
	with Not_found ->
	  []
      end
    
(*
let dbgtype_tag_slow tv dbgTypeInfo struct_map =
  Stats.time ("Query DBG") (fun _ ->
  if not tv.tv_crossbb then (
    let fun_dbgmap, global_dbgmap = dbgTypeInfo in
    let tvnm = tv.tv_name in
    let index = tv.tv_index in
    let tag =
      match tv.tv_name with
      | TV_imm _ 
      | TV_reg _ 
      | TV_addr _ ->
	List.unique ?eq:(Some eq_sctype) (
	  List.flatten (
	    List.filter_map (fun (tvnm,rng,ectype) ->
	      if tvnm = tv.tv_name && in_range tv.tv_index rng then
		Some ectype
	      else 
		None
	    ) dbgTypeInfo
	  )
	)
      | TV_cfa off ->
	List.unique ?eq:(Some eq_sctype) (
	  List.flatten (
	    List.filter_map (fun (tvnm,rng,ectype) ->
	      if in_range tv.tv_index rng then (
		match tvnm with
		| TV_cfa off' ->
		  if off < off' then None
		  else if off = off' then Some ectype
		  else (
		    let array_types = List.filter (fun sctype ->
		      match sctype with
		      | SC_array _ -> true
		      | _ -> false
		    ) ectype
		    in
		    if List.length array_types > 1 then
		      raise (Failure "Type conflict with arrays")
		    else if List.is_empty array_types then None
		    else
		      let array_type = List.hd array_types in
		      match array_type with
		      | SC_array (sct,dimlist) ->
			let size = List.fold_right (fun dim mult ->
			  dim * mult
			) dimlist 1
			in
			let sctsize = size_of_sctype struct_map sct in
			let resi = (off - off') mod sctsize in
			if resi = 0 && ((off - off')/sctsize) <= size then
			  Some [sct]
			else
			  None
		      | _ -> None
		  )
		| _ -> None
	      )
	      else None
	    ) dbgTypeInfo
	  )
	)
	  (*
      | TV_addr addrnm ->
	begin match addrnm with
	| {disp=off;base=None;index=None} ->
	  let off = 
	    try
	      int_of_string ("0x"^off) 
	    with Failure errmsg ->
	      let errmsg = P.sprintf "%s:%s" errmsg off in
	      raise (Failure errmsg)
	  in
	  List.unique ?eq:(Some eq_sctype) (List.flatten (
	    List.filter_map (fun (tvnm,rng,ectype) ->
	      if in_range tv.tv_index rng then (
		begin match tvnm with
		| TV_addr addrnm' ->
		  if addrnm'.index = None && addrnm'.base = None then (
		    let off' = 
		      try
			int_of_string ("0x"^addrnm'.disp) 
		      with Failure errmsg ->
			let errmsg = P.sprintf "%s:%s from dbg" errmsg addrnm'.disp in
			raise (Failure errmsg)
		    in
		    if off < off' then None
		    else if off = off' then Some ectype
		    else (
		      let array_types = List.filter (fun sctype ->
			match sctype with
			| SC_array _ -> true
			| _ -> false
		      ) ectype
		      in
		      if List.length array_types > 1 then
			raise (Failure "Type conflict with arrays")
		      else if List.is_empty array_types then None
		      else
			let array_type = List.hd array_types in
			begin match array_type with
			| SC_array (sct,dimlist) ->
			  let size = List.fold_right (fun dim mult ->
			    dim * mult
			  ) dimlist 1
			  in
			  let sctsize = size_of_sctype struct_map sct in
			  let resi = (off - off') mod sctsize in
			  if resi = 0 && ((off - off')/sctsize) <= size then
			    Some [sct]
			  else
			    None
			| _ -> None
			end
		    )
		  )
		  else None
		| _ -> None
		end
	      )
	      else None
	    ) dbgTypeInfo
	  ))
	| _ -> 
	  raise (Failure "TV address name is not absolute")
	end
	  *)
    in
    tag
  )
  else []
  ) ()
    *)

let deref_sctype struct_map sctype offset =
  let rec find_derefable sct =
    match sct with
    | SC_top _ -> sct
    | SC_typeVar _ -> raise (Failure "deref type variable")
    | SC_const sct' | SC_volatile sct' -> find_derefable sct'
    | SC_base str ->
      if str = "long long unsigned int" || str = "long long int" then
	SC_base str
      else
	raise Not_found
    | SC_array _ | SC_pointer _ -> sct
    | SC_typedef (defnm,sct') ->
      if H.mem struct_map defnm then
	SC_typedef (defnm, (H.find struct_map defnm).struct_type)
      else
	find_derefable sct'
    | SC_structure (nm,lvnm,fdlist) ->
      if not (List.is_empty fdlist) then sct
      else if H.mem struct_map nm then
        (H.find struct_map nm).struct_type
      else
	raise (Failure "struct map may be incomplete")
    | SC_union _ -> SC_top []
    | _ ->
      (*
      let errmsg = P.sprintf "WARNING: try to deref type: %s\n" (str_of_sctype sct) in
      P.printf "%s" errmsg; flush stdout;
      sct*)
      raise Not_found
  in
  
  let find_memoff memoff off =
    let rec aux memoff off ndx =
      match memoff with
      | [] -> 
	raise (Failure "a structure does not have member!")
      | h :: [] -> ndx, h
      | h1 :: h2 :: t ->
	if h1 <= off && off < h2 then
	  ndx, h1
	else
	  aux (h2 :: t) off (ndx+1)
    in
    aux memoff off 0
  in
  
  let rec deref sct off = 
    match sct with
    | SC_top _ -> sct
    | SC_base str ->
      if str = "long long unsigned int" then
	SC_base "unsigned int"
      else if str = "long long int" then
	SC_base "int"
      else (
	let errmsg =
	  P.sprintf "WARNING: deref base type: %s\n" str
	in
	(*flush stdout;*)
	raise (Failure errmsg)
      )
    | SC_const sct' | SC_volatile sct' -> 
      (*let target_sct =
	try
	find_derefable sct 
	with Not_found ->
	let errmsg = P.sprintf "derefarable not found in %s" (str_of_sctype sctype) in
	raise (Failure errmsg)
	in*)
      deref sct' off
    | SC_typedef (defnm,sct') ->
      if H.mem struct_map defnm then
	let target_struct = H.find struct_map defnm in
	begin match target_struct.struct_type with
	| SC_structure (oldnm,lvnm,fdlist) ->
	  if List.is_empty fdlist then
	    raise (Failure "struct map contains dummy typedef struct type")
	  else
	    let target_size = target_struct.struct_size in
	    let target_memoff = target_struct.struct_memoff in
	    if off >= target_size then
	      let errmsg = P.sprintf "offset %s(%d) : size_of_structure %d" 
		offset off target_size
	      in
	      P.printf "WARNING:%s\n" errmsg; flush stdout;
	      let real_off = off - target_size in
	      deref sct real_off
	    else if off < 0 then
	      let errmsg = P.sprintf "offset %s(%d) : size_of_structure %d" 
		offset off target_size
	      in
	      P.printf "WARNING:%s\n" errmsg; flush stdout;
	      let real_off = off + target_size in
	      deref sct real_off
	    else
	      let pos, posoff = find_memoff target_memoff off in
	      if posoff = off then
		fst (List.nth fdlist pos)
	      else if posoff < off then
		let inner_type = fst (List.nth fdlist pos) in
		let next_off = off - posoff in
		deref inner_type next_off
	      else
		raise (Failure "Impossible")
	| _ ->
	  raise (Failure "Magically we have non-struct in struct map")
	end
      else
	(*
	  let target_sct = 
	  try
	  find_derefable sct 
	  with Not_found ->
	  let errmsg = P.sprintf "derefarable not found in %s" (str_of_sctype sctype) in
	  raise (Failure errmsg)
	  in*)
	deref sct' off
    | SC_array (sct',dimlist) -> 
      if List.is_empty dimlist then 
	sct'
      else
	let bound = calc_array_bound dimlist in
	let sctsize = size_of_sctype struct_map sct' in
	let residue = off mod sctsize in
	if residue = 0 then
	  let index = off / sctsize in
	  if index < bound then
	    sct'
	  else
	    raise (Failure "dereference out of bound for array")
	else
	  let index = (off-residue) / sctsize in
	  if index < bound then
	    deref sct' residue
	  else
	    raise (Failure "deref out of bound for array with residue") 
    | SC_structure (nm,_,_) ->
      if H.mem struct_map nm then (
	let target_struct = H.find struct_map nm in
	match target_struct.struct_type with
	| SC_structure (_,_,fdlist) ->
	  if List.is_empty fdlist then 
	    raise (Failure "struct map contains dummy struct type")
	  else
	    let target_size = target_struct.struct_size in
	    let target_memoff = target_struct.struct_memoff in
	    if off >= target_size then
	      let errmsg = P.sprintf "offset %s(%d) >= size_of_structure %d" 
		offset off target_size
	      in
	      P.printf "WARNING:%s\n" errmsg; flush stdout;
	      let real_off = off - target_size in
	      deref sct real_off
	    else if off < 0 then
	      let errmsg = P.sprintf "offset %s(%d) < size_of_structure %d" 
		offset off target_size
	      in
	      P.printf "WARNING:%s\n" errmsg; flush stdout;
	      let real_off = off + target_size in
	      deref sct real_off
	    else
	      let pos, posoff = find_memoff target_memoff off in
	      if posoff = off then
		fst (List.nth fdlist pos)
	      else if posoff < off then
		let inner_type = fst (List.nth fdlist pos) in
		let next_off = off - posoff in
		deref inner_type next_off
	      else
		raise (Failure "Impossible")
	| _ ->
	  raise (Failure "Magically we have non-struct in struct map")
      )
      else
	raise (Failure "met a structure with no name!")
    | SC_pointer sct' -> (
      try
	let target_sct' = find_derefable sct' in
	begin match target_sct' with
	| SC_typedef _ 
	| SC_structure _ -> deref target_sct' off
	| _ -> sct'
	end
      with Not_found ->
	sct'
    )
    | SC_union _ -> SC_top []
    | _ ->
      let errmsg = 
	P.sprintf "deref non-derefable type: %s with off: %d" 
	  (str_of_sctype sct) off 
      in 
      raise (Failure errmsg) 
  in
  
  let rec deref_all sct =
    (*let target_sct = 
      try
	find_derefable sct 
      with Not_found ->
	let errmsg = P.sprintf "derefarable not found in %s" (str_of_sctype sctype) in
	raise (Failure errmsg)
    in*)
    match sct with
    | SC_top _ -> sct
    | SC_base str ->
      if str = "long long unsigned int" then
	SC_base "unsigned int"
      else if str = "long long int" then
	SC_base "int"
      else (
	P.printf "WARNING: deref_all base type: %s\n" str; flush stdout;
	SC_top []
      )
    | SC_const sct' | SC_volatile sct' -> deref_all sct'
    | SC_typedef (defnm,sct') ->
      if H.mem struct_map defnm then SC_top []
      else deref_all sct'
    | SC_array (sct',_) -> sct'
    | SC_structure (nm,lvnm,fdlist) -> SC_top []
    | SC_pointer sct' -> (
      try
	let target_sct' = find_derefable sct' in
	begin match target_sct' with
	| SC_typedef _
	| SC_structure _ -> deref_all target_sct'
	| _ -> sct'
	end	
      with Not_found ->
	sct'
    )
    | SC_union _ -> SC_top []
    | _ ->
      raise (Failure "deref_all non-derefable type") 
  in
  if offset <> "all" then
    let off = Int32.to_int (Int32.of_string ("0x"^offset)) in
    deref sctype off
  else if offset = "all" then
    deref_all sctype
  else 
    let errmsg = "Magically, we have off string " ^ offset in
    raise (Failure errmsg)

let collect_types_in_union union_sctype =
  match union_sctype with
  | SC_union (uname,fdlist) ->
    List.unique ?eq:(Some eq_sctype) (List.map fst fdlist)
  | _ -> assert false

let rec is_derefable struct_map sctype =
    (** only consider the type is for a register **)
  match sctype with
  | SC_top _ -> true
  | SC_base str -> 
    if str = "void" then true
    else false
  | SC_const sct | SC_volatile sct -> is_derefable struct_map sct
  | SC_typedef (defnm,sct) ->
    if H.mem struct_map defnm then
      let struct_entry = H.find struct_map defnm in
      begin match struct_entry.struct_type with
      | SC_structure (_,_,fdlist) ->
	is_derefable struct_map (fst (List.hd fdlist))
      | _ -> assert false
      end
    else
      is_derefable struct_map sct
  | SC_array (sct,dimlist) -> true
  | SC_pointer sct -> true
  | SC_union (_,umemlist) ->
    List.exists (fun (ft,fn) ->
      is_derefable struct_map ft
    ) umemlist
  | SC_structure (oldnm,_,fdlist) -> 
    P.printf "a register has structure type when dereferenced"; flush stdout;
    if H.mem struct_map oldnm then
      let struct_entry = H.find struct_map oldnm in
      begin match struct_entry.struct_type with
      | SC_structure (_,_,fdlist) ->
	is_derefable struct_map (fst (List.hd fdlist))
      | _ -> assert false
      end
    else if not (List.is_empty fdlist) then
      is_derefable struct_map (fst (List.hd fdlist))
    else false
  | _ -> false

let strict_deref_sctype struct_map sctype offset = 
  (** in strict deref, only array, pointer, and top can be dereferenced **)
  let rec find_struct sct =
    match sct with
    | SC_typeVar _ -> 
      assert false
    | SC_const sct' | SC_volatile sct' -> 
      find_struct sct'
    | SC_typedef (defnm,sct') ->
      if H.mem struct_map defnm then
        sct
      else
	find_struct sct'
    | SC_structure (oldnm,_,_) ->
      if H.mem struct_map oldnm then
        sct
      else
	raise (Failure "struct map may be incomplete")
    | _ ->
      raise Not_found
  in

  let rec find_memchunk sct =
    match sct with
    | SC_typeVar _ -> 
      assert false
    | SC_const sct' | SC_volatile sct' -> 
      find_memchunk sct'
    | SC_array _ -> sct
    | SC_typedef (defnm,sct') ->
      if H.mem struct_map defnm then
	sct
      else
	find_memchunk sct'
    | SC_structure (oldnm,_,_) ->
      if H.mem struct_map oldnm then
	sct
      else 
	raise (Failure "struct map may be incomplete during deref")
    | _ ->
      raise Not_found
  in

  let pointerize sct =
    try 
      let chunktype = find_memchunk sct in
      match chunktype with
      | SC_array _ -> chunktype
      | SC_structure _ -> SC_pointer chunktype
      | SC_typedef _ -> SC_pointer chunktype
      | _ -> assert false
    with Not_found ->
      SC_top []
  in

  let find_memoff memoff off =
    let rec aux memoff off ndx =
      match memoff with
      | [] -> 
	raise (Failure "a structure does not have member!")
      | h :: [] -> ndx, h
      | h1 :: h2 :: t ->
	if h1 <= off && off < h2 then
	  ndx, h1
	else
	  aux (h2 :: t) off (ndx+1)
    in
    aux memoff off 0
  in

  let rec strict_deref sct off =
    match sct with
    | SC_top _ -> [sct]
    | SC_base str ->
      if str = "void" then [sct]
      else assert false
      (*assert false*)
    | SC_const sct' | SC_volatile sct' -> strict_deref sct' off
    | SC_typedef (defnm,sct') -> 
      if H.mem struct_map defnm then
	let struct_entry = H.find struct_map defnm in
	begin match struct_entry.struct_type with
	| SC_structure (_,_,fdlist) ->
	  if off <> 0 then []
	  else 
	    strict_deref (fst (List.hd fdlist)) 0
	| _ -> assert false
	end
      else
	strict_deref sct' off
    | SC_array (sct',dimlist) -> 
      if List.is_empty dimlist then [sct']
      else
	let bound = calc_array_bound dimlist in
	let sctsize = size_of_sctype struct_map sct' in
	let residue = off mod sctsize in
	if residue = 0 then
	  let index = off / sctsize in
	  if index < bound then
	    strict_deref (SC_pointer (sct')) 0
	  else
	    raise (Failure "dereference out of bound for array")
	else
	  let index = (off-residue) / sctsize in
	  if index < bound then
	    strict_deref (pointerize sct') residue
	  else
	    raise (Failure "deref out of bound for array with residue")
    | SC_union (umemnm,umemlist) -> 
      (** dereference all plausible fields to over-approximate **)
      List.unique ?eq:(Some eq_sctype) 
	(List.flatten (
	  List.map (fun (ft,fn) ->
	    if is_derefable struct_map ft then
	      strict_deref ft off
	    else
	      []
	  ) umemlist
	 ))
      (*[SC_top []]*)
    | SC_structure (nm,lvnm,fdlist) ->
      if off <> 0 then []
      else if not (List.is_empty fdlist) then
	strict_deref (fst (List.hd fdlist)) 0 
      else if H.mem struct_map nm then
	let struct_entry = H.find struct_map nm in
	begin match struct_entry.struct_type with
	| SC_structure (_,_,fdlist) ->
	  strict_deref (fst (List.hd fdlist)) 0
	| _ -> assert false
	end
      else []
    | SC_pointer sct' -> (
      try
	let target_sct' = find_struct sct' in
	let target_struct =
	  begin match target_sct' with
	  | SC_typedef (defnm,_) ->
	    H.find struct_map defnm
	  | SC_structure (oldnm,_,fdlist) ->
	    H.find struct_map oldnm
	  | _ -> assert false
	  end
	in
	begin match target_struct.struct_type with
	| SC_structure (_,_,fdlist) ->
	  if List.is_empty fdlist then 
	    raise (Failure "struct map contains dummy struct type")
	  else
	    let target_size = target_struct.struct_size in
	    let target_memoff = target_struct.struct_memoff in
	    if off >= target_size then
	      let errmsg = P.sprintf "offset %s(%d) >= size_of_structure %d" 
		offset off target_size
	      in
	      P.printf "WARNING:%s\n" errmsg; flush stdout;
	      let real_off = off mod target_size in
	      strict_deref sct real_off
	    else if off < 0 then (
	      let errmsg = P.sprintf "offset %s(%d) < size_of_structure %d" 
		offset off target_size
	      in
	      P.printf "WARNING:%s\n" errmsg; flush stdout;
	      let real_off = ref off in
	      while !real_off < 0 do
		real_off := !real_off + target_size
	      done;
	      strict_deref sct !real_off
	    )
	    else
	      let pos, posoff = find_memoff target_memoff off in
	      if posoff = off then
		strict_deref (SC_pointer (fst (List.nth fdlist pos))) 0
	      else if posoff < off then
		let inner_type = fst (List.nth fdlist pos) in
		let next_off = off - posoff in
		strict_deref (pointerize inner_type) next_off
	      else
		assert false
	| _ -> assert false
	end
      with Not_found ->
	[sct']
    )
    | _ ->
      raise (Failure "deref non-derefable type") 
  in

  let rec strict_deref_all sct =
    match sct with
    | SC_top _ -> [sct]
    | SC_base str ->
      if str = "void" then [sct]
      else assert false
    | SC_const sct' | SC_volatile sct' -> strict_deref_all sct'
    | SC_typedef (defnm,sct') ->
      if H.mem struct_map defnm then
	let struct_entry = H.find struct_map defnm in
	begin match struct_entry.struct_type with
	| SC_structure (_,_,fdlist) ->
	  strict_deref_all (fst (List.hd fdlist))
	| _ -> assert false
	end
      else
      strict_deref_all sct'
    | SC_array (sct',_) -> [sct']
    | SC_structure (nm,lvnm,fdlist) -> 
      if not (List.is_empty fdlist) then
	strict_deref_all (fst (List.hd fdlist)) 
      else if H.mem struct_map nm then
	let struct_entry = H.find struct_map nm in
	begin match struct_entry.struct_type with
	| SC_structure (_,_,fdlist) ->
	  strict_deref_all (fst (List.hd fdlist))
	| _ -> assert false
	end
      else []
    | SC_pointer sct' -> (
      try
	let target_sct' = find_struct sct' in
	begin match target_sct' with
	| SC_typedef _
	| SC_structure _ -> [SC_top []]
	| _ -> assert false
	end	
      with Not_found ->
	[sct']
    )
    | SC_union (_,umemlist) -> 
      List.unique ?eq:(Some eq_sctype) 
	(List.flatten (
	  List.map (fun (ft,fn) ->
	    if is_derefable struct_map ft then
	      strict_deref_all ft
	    else
	      []
	  ) umemlist
	 ))
      (*[SC_top []]*)
    | _ ->
      raise (Failure "deref_all non-derefable type")
  in

  if is_derefable struct_map sctype then (
    if offset <> "all" then
      let off = Int32.to_int (Int32.of_string ("0x"^offset)) in
      strict_deref sctype off
    else if offset = "all" then
      strict_deref_all sctype
    else
      assert false
  ) else (
    raise (Failure "deref non-derefable for register")
  )
(*
let collect_all ect =
  let rec collect_all_sct sctype =
    match sctype with
    | SC_top _ -> Some sctype
    | SC_base _ -> None
    | SC_pointer sct -> 
  in
  List.filter_map (fun sct ->
    collect_all_sct sct
  ) ect
*)
let reason_ectype ectype edgestack struct_map =
  if List.is_empty ectype || eq_ectype ectype [SC_top []] then
    ectype
  else (
    List.fold_left (fun ect edge ->
      let edgetype, offset = edge in
      if edgetype = 0 then (
	ect
      )
      else if edgetype = 2 then
	(** without lea, no edgetype 2 **)
	List.map (fun sctype ->
	  SC_pointer sctype
	) ect
      else (** edgetype = 1 **)
	List.unique ?eq:(Some eq_sctype) (List.flatten (
	  List.filter_map (fun sctype ->
	    if is_derefable struct_map sctype then
	      Some 
		(Stats.time ("dereference sctype") 
		   (strict_deref_sctype struct_map sctype) offset)
	    else
	      None
	  ) ect
	))
    ) ectype edgestack					     
  )
 
let dbgtype_count = ref 0
let flowtype_count = ref 0

let eqcg_search dbgmap eqcg struct_map target_list = 
  dbgtype_count := 0;
  flowtype_count := 0;
  List.map (fun target ->
    let tv, edge = target in
    match tv.tv_name with
    | TV_addr _ ->
      if snd edge <> "0" then
	flowtype_count := !flowtype_count + 1
      else
	dbgtype_count := !dbgtype_count + 1;
      target, reason_ectype (dbgtype_tag tv dbgmap) [edge] struct_map
    | TV_cfa off when off >= 0 ->
      dbgtype_count := !dbgtype_count + 1;
      target, reason_ectype (dbgtype_tag tv dbgmap) [edge] struct_map
    | _ ->
      try
	let tv_eqc = eqcg_find eqcg tv in
	let tv_type = 
	  if List.is_empty tv_eqc.eqc_dbgtype then (
	    flowtype_count := !flowtype_count + 1;
	    reason_ectype tv_eqc.eqc_fflowtype [edge] struct_map
	  ) 
	  else (
	    dbgtype_count := !dbgtype_count + 1;
	    reason_ectype tv_eqc.eqc_dbgtype [edge] struct_map
	  )
	in
	(target, tv_type)
      with Not_found ->
	begin match tv.tv_name with
	| TV_addr _ ->
	  if snd edge <> "0" then
	    flowtype_count := !flowtype_count + 1
	  else
	    dbgtype_count := !dbgtype_count + 1;
	  target, reason_ectype (dbgtype_tag tv dbgmap) [edge] struct_map
	| TV_cfa off ->
	  dbgtype_count := !dbgtype_count + 1;
          target, reason_ectype (dbgtype_tag tv dbgmap) [edge] struct_map
	| TV_imm _ ->
	  dbgtype_count := !dbgtype_count + 1;
          target, reason_ectype (dbgtype_tag tv dbgmap) [edge] struct_map
	| TV_reg _ ->
	  if fst edge = 0 then
	    dbgtype_count := !dbgtype_count + 1	    
	  else
	    flowtype_count := !flowtype_count + 1;
	  target, reason_ectype (dbgtype_tag tv dbgmap) [edge] struct_map
	end
  ) target_list
    
let icall_exists asm_fun =
  List.fold_left (fun b bb ->
    let _,ins,_ = List.last bb.bb_instrs in
    let icall_bb = 
      match ins with
      | CALL (dis,abs,op,slctr) ->
	begin match op with
	| Imm_op _ -> false
	| _ -> true
	end
      | _ -> false
    in
    b || icall_bb
  ) false asm_fun.func_bbs

let ijump_exists asm_fun =
  List.fold_left (fun b bb ->
    let _,ins,_ = List.last bb.bb_instrs in
    let ijump_bb = 
      match ins with
      | JMP (dis,abs,op,slctr) ->
	begin match op with
	| Imm_op _ -> false
	| _ -> true
	end
      | _ -> false
    in
    b || ijump_bb
  ) false asm_fun.func_bbs

let undecided_ijump_exists elf asm_fun start all_functions =
  List.exists (fun bb ->
    let _,ins,len = List.last bb.bb_instrs in
    let branch_site = bb.bb_relAddr + bb.bb_size - len in
    match ins with
    | JMP (dis,abs,op,slctr) ->
      begin match op with
      | Imm_op _ -> false
      | _ ->
	let ijump_type = ijclassify_global elf op branch_site start all_functions in
	begin match ijump_type with
	| IJ_unclassified_t -> true
	| IJ_function_t -> true
	| _ -> false
	end
      end
    | _ -> false
  ) asm_fun.func_bbs

let icall_ops funCFA asm_fun = 
  let inner_tvNameGen funCFA op l =
    tvNameGen funCFA op l
  in
  List.flatten(
    List.map (fun bb ->  
      let addrcount = ref bb.bb_relAddr in
      let l_instr_list = 
	List.map (fun (pre,ins,len) -> 
	  let outaddr = !addrcount in
	  addrcount := len + !addrcount;
	  outaddr, (pre,ins,len)
	) bb.bb_instrs 
      in
      List.filter_map (fun (l,(pre,ins,len)) ->
	match ins with
	| CALL(dis,abs,op,slctr) ->
	  begin match op with
	  | Imm_op _ -> None
	  | _ ->
	    let tvNameOp = inner_tvNameGen funCFA op l in
	    Some (mkTV tvNameOp l false)
	  end
	| _ -> None
      ) l_instr_list
    ) asm_fun.func_bbs
  )

let ijump_ops funCFA asm_fun = 
  let inner_tvNameGen funCFA op l =
    tvNameGen funCFA op l
  in
  List.flatten(
    List.map (fun bb ->  
      let addrcount = ref bb.bb_relAddr in
      let l_instr_list = 
	List.map (fun (pre,ins,len) -> 
	  let outaddr = !addrcount in
	  addrcount := len + !addrcount;
	  outaddr, (pre,ins,len)
	) bb.bb_instrs 
      in
      List.filter_map (fun (l,(pre,ins,len)) ->
	match ins with
	| JMP (dis,abs,op,slctr) ->
	  begin match op with
	  | Imm_op _ -> None
	  | _ ->
	    let tvNameOp = inner_tvNameGen funCFA op l in
	    Some (mkTV tvNameOp l false)
	  end
	| _ -> None
      ) l_instr_list
    ) asm_fun.func_bbs
  )

let icall_total = ref 0 
let dbgicall = ref 0
let flowicall = ref 0
let abnormal_icall_total = ref 0
let ab1icall_total = ref 0
let ab2icall_total = ref 0
let ab3icall_total = ref 0
let ab4icall_total = ref 0


let ijump_total = ref 0
let dbgijump = ref 0
let flowijump = ref 0
let tc_total = ref 0
let correct_ijump_total = ref 0
let multi_ijump_total = ref 0
let topalone_ijump_total = ref 0
let topexist_ijump_total = ref 0

let typesig_total = ref 0
let vt = ref 0 
let nvt = ref 0
let type_total = ref 0
let evt = ref 0
let ivt = ref 0 
let ab1_total = ref 0
let ab2_total = ref 0
let ab3_total = ref 0
let ab4_total = ref 0
let dbgvt = ref 0 

let tv_total = ref 0
let tv_from_dbg = ref 0
let tv_from_inf = ref 0

let rec eq_to_fp sctype =
  match sctype with
  | SC_pointer (SC_subroutine _) -> true
  | SC_typedef (defnm,sct) -> eq_to_fp sct
  | SC_const sct | SC_volatile sct -> eq_to_fp sct
  | _ -> false

let ijump_checking funCFA dbgmap asm_fun eqcg struct_map =
  P.printf "start ijump checking...\n"; flush stdout;
  let ijump_targets = ijump_ops funCFA asm_fun in
  ijump_total := !ijump_total + (List.length ijump_targets);
  let target_types = eqcg_search dbgmap eqcg struct_map ijump_targets in
  dbgijump := !dbgtype_count + !dbgijump;
  flowijump := !flowtype_count + !flowijump;
  let tail_call_ijump =
    List.filter (fun (target, ect) ->
      List.exists (fun sct -> 
	eq_to_fp sct
      ) ect
    ) target_types
  in
  let correct_ijump = 
    List.filter (fun (target, ect) ->
      List.length ect = 1 && 
	List.exists (fun sct -> 
	  eq_to_fp sct
	) ect
    ) tail_call_ijump
  in
  let multi_fp_ijump =
    List.filter (fun (target, ect) ->
      List.length ect <> 1 && 
	List.for_all (fun sct -> 
	  eq_to_fp sct
	) ect
    ) tail_call_ijump
  in
  let topexist_ijump =
    List.filter (fun (target, ect) ->
      List.length ect <> 1 && 
	List.exists (fun sct -> 
	  match sct with
	  | SC_top _ -> true
	  | _ -> false
	) ect
    ) tail_call_ijump
  in 
  tc_total := !tc_total + (List.length tail_call_ijump);
  correct_ijump_total := !correct_ijump_total + (List.length correct_ijump);
  multi_ijump_total := !multi_ijump_total + (List.length multi_fp_ijump);
  (*topalone_ijump := !topalone_ijump + (List.length topalone_ijump);*)
  topexist_ijump_total := !topexist_ijump_total + (List.length topexist_ijump);
  if not (List.is_empty tail_call_ijump) then (
    P.printf "# tail call ijump: %d\n" (List.length tail_call_ijump);
    List.iter (fun (target,ect) ->
      let tv, _ = target in
      P.printf "ijump at %x:\n" tv.tv_index;
      let tailcall_ect = List.filter (fun sct -> not (eq_to_fp sct)) ect in
      P.printf "%s\n" (str_of_ectype tailcall_ect);
    ) target_types
  )

let icall_checking funCFA dbgmap asm_fun eqcg struct_map =
  P.printf "start icall checking...\n"; flush stdout;
  let icall_targets = icall_ops funCFA asm_fun in
  icall_total := !icall_total + (List.length icall_targets);
  let target_types = eqcg_search dbgmap eqcg struct_map icall_targets in
  dbgicall := !dbgtype_count + !dbgicall;
  flowicall := !flowtype_count + !flowicall;
  let abnormal_icall =
    List.filter (fun (target, ect) ->
      List.length ect <> 1 || 
	List.exists (fun sct -> 
	  not (eq_to_fp sct)
	) ect
    ) target_types
  in
  let ab1_icall = 
    List.filter (fun (target, ect) ->
      List.length ect = 1 && 
	List.exists (fun sct -> 
	  match sct with
	  | SC_top _ -> true
	  | _ -> false
	) ect
    ) target_types
  in
  let ab2_icall =
    List.filter (fun (target, ect) ->
      List.length ect = 1 && 
	List.exists (fun sct -> 
	  not (eq_to_fp sct) &&
	    match sct with
	    | SC_top _ -> false
	    | _ -> true
	) ect
    ) target_types
  in 
  let ab3_icall = 
    List.filter (fun (target, ect) ->
      List.length ect > 1 &&
	List.for_all (fun sct -> 
	  eq_to_fp sct
	) ect
    ) target_types
  in
  let ab4_icall =
    List.filter (fun (target, ect) ->
      List.length ect > 1 && 
	List.exists (fun sct -> 
	  not (eq_to_fp sct)
	) ect && 
	List.exists (fun sct ->
	  eq_to_fp sct
	) ect
    ) target_types
  in
  abnormal_icall_total := !abnormal_icall_total + (List.length abnormal_icall);
  ab1icall_total := !ab1icall_total + (List.length ab1_icall);
  ab2icall_total := !ab2icall_total + (List.length ab2_icall);
  ab3icall_total := !ab3icall_total + (List.length ab3_icall);
  ab4icall_total := !ab4icall_total + (List.length ab4_icall);
  (*P.printf "In function %s\n" asm_fun.func_name;*)
  (*P.printf "# icall: %d\n" (List.length target_types);*)
  if not (List.is_empty abnormal_icall) then (
    P.printf "# abnormal icall: %d\n" (List.length abnormal_icall);
    List.iter (fun (target,ect) ->
      let tv, _ = target in
      P.printf "icall at %x:\n" tv.tv_index;
      (*let abnormal_ect = List.filter (fun sct -> not (eq_to_fp sct)) ect in*)
      P.printf "%s\n" (str_of_ectype ect);
    ) abnormal_icall
  )

let calc_percent i1 i2 =
  100.0 *. (float_of_int i1) /. (float_of_int i2)

let eqcg_stat funCFA dbgmap asm_fun eqcg struct_map =
  let _,nodes = eqcg in
  let nodes_total = H.length nodes in
  P.printf "Nodes in total: %d\n" nodes_total;
  icall_checking funCFA dbgmap asm_fun eqcg struct_map;
  ijump_checking funCFA dbgmap asm_fun eqcg struct_map;
  H.iter (fun repr eqc ->
    tv_total := !tv_total + (TypeVarSet.cardinal eqc.eqc_vars);
    vt := !vt + (TypeVarSet.cardinal eqc.eqc_vars);
    
    if not (List.is_empty (eqc.eqc_fflowtype @ eqc.eqc_dbgtype)) && 
      not (List.exists (fun sct ->
	match sct with
	| SC_top _ -> true
	| _ -> false
      ) eqc.eqc_fflowtype)
    then (
      nvt := !nvt + (TypeVarSet.cardinal eqc.eqc_vars);
      type_total := 
	!type_total + 
	(List.length (eqc.eqc_fflowtype @ eqc.eqc_dbgtype)) * 
	(TypeVarSet.cardinal eqc.eqc_vars);
    );
    if List.is_empty (eqc.eqc_dbgtype @ eqc.eqc_fflowtype) then
      evt := !evt + (TypeVarSet.cardinal eqc.eqc_vars)
    else if List.exists (fun sct -> 
      match sct with
      | SC_top _ -> true
      | _ -> false
    ) (eqc.eqc_dbgtype @ eqc.eqc_fflowtype) 
    then 
      ivt := !ivt + (TypeVarSet.cardinal eqc.eqc_vars);
    if not (List.is_empty eqc.eqc_dbgtype) then (
      tv_from_dbg := !tv_from_dbg + (TypeVarSet.cardinal eqc.eqc_vars);
      dbgvt := !dbgvt + (TypeVarSet.cardinal eqc.eqc_vars)
    )
    else if List.length eqc.eqc_fflowtype = 1 && 
	   List.exists (fun sct -> 
	     match sct with
	     | SC_top _ -> true
	     | _ -> false
	   ) eqc.eqc_fflowtype 
    then
      ab1_total := !ab1_total + (TypeVarSet.cardinal eqc.eqc_vars)
    else if List.length eqc.eqc_fflowtype = 1 then
      tv_from_inf := !tv_from_inf + (TypeVarSet.cardinal eqc.eqc_vars)
    else if List.length eqc.eqc_fflowtype <> 1 &&
	List.for_all (fun sct -> 
	  match sct with
	  | SC_top _ -> false
	  | _ -> true
	) eqc.eqc_fflowtype
    then (
      tv_from_inf := !tv_from_inf + (TypeVarSet.cardinal eqc.eqc_vars);
      ab2_total := !ab2_total + (TypeVarSet.cardinal eqc.eqc_vars)
    )
    else if List.length eqc.eqc_fflowtype <> 1 &&
	List.exists (fun sct ->
	  match sct with
	  | SC_top _ -> true
	  | _ -> false
	) eqc.eqc_fflowtype
    then
      ab3_total := !ab3_total + (TypeVarSet.cardinal eqc.eqc_vars);	
  ) nodes
    
let stat_report () =
  P.printf "ICALL need inference %%: %d/%d(%f%%)\n" 
    (!icall_total - !dbgicall) !icall_total
    (calc_percent (!icall_total - !dbgicall) !icall_total);
  P.printf "Infered ICALL %%: %d/%d(%f%%)\n" 
    (!icall_total - !abnormal_icall_total - !dbgicall) !icall_total
    (calc_percent (!icall_total - !abnormal_icall_total - !dbgicall) !icall_total);
  P.printf "Abnormal ICALL %%: %d/%d(%f%%)\n" 
    !abnormal_icall_total 
    !icall_total 
    (calc_percent !abnormal_icall_total !icall_total);
  P.printf "? alnone ICALL %%: %d/%d(%f%%)\n" 
    !ab1icall_total 
    !icall_total 
    (calc_percent !ab1icall_total !icall_total);
  P.printf "t alnone ICALL %%: %d/%d(%f%%)\n" 
    !ab2icall_total 
    !icall_total 
    (calc_percent !ab1icall_total !icall_total);
  P.printf "multi-fp ICALL %%: %d/%d(%f%%)\n" 
    !ab3icall_total 
    !icall_total 
    (calc_percent !ab3icall_total !icall_total);
  P.printf "polluted-fp ICALL %%: %d/%d(%f%%)\n" 
    !ab4icall_total 
    !icall_total 
    (calc_percent !ab4icall_total !icall_total);

  P.printf "Infered TCALL %%: %d/%d(%f%%)\n"
    !tc_total !ijump_total
    (calc_percent !tc_total !ijump_total);
  P.printf "Correct TCALL %%: %d/%d(%f%%)\n"
    !correct_ijump_total !tc_total
    (calc_percent !correct_ijump_total !tc_total);
  P.printf "Multi-fp TCALL %%: %d/%d(%f%%)\n"
    !multi_ijump_total !tc_total
    (calc_percent !multi_ijump_total !tc_total);
  P.printf "top exist TCALL %%: %d/%d(%f%%)\n"
    !topexist_ijump_total !tc_total
    (calc_percent !topexist_ijump_total !tc_total);

  P.printf "TypeVar needs inference %%: %d/%d(%f%%)\n"
    (!vt - !dbgvt) !vt
    (calc_percent (!vt - !dbgvt) !vt);
  P.printf "Error TypeVar %%: %d/%d(%f%%)\n" 
    !evt !vt
    (calc_percent !evt !vt);
  P.printf "Incomplete TypeVar %%: %d/%d(%f%%)\n" 
    !ivt !vt
    (calc_percent !ivt !vt);
  let abvt = !ivt + !evt in
  P.printf "? alone TypeVar %%: %d/%d(%f%%)\n" 
    !ab1_total !vt
    (calc_percent !ab1_total !vt);
  P.printf "multi-t TypeVar %%: %d/%d(%f%%)\n" 
    !ab2_total !vt
    (calc_percent !ab2_total !vt);
  P.printf "polluted-t TypeVar %%: %d/%d(%f%%)\n" 
    !ab3_total !vt
    (calc_percent !ab3_total !vt);
  P.printf "Completely Infered TypeVar %%: %d/%d(%f%%)\n" 
    (!vt - abvt - !dbgvt) (!vt - !dbgvt)
    (calc_percent (!vt - abvt - !dbgvt) (!vt - !dbgvt));
  P.printf "Acceptable Infered TypeVar %%: %d/%d(%f%%)\n"
    (!ab3_total + !nvt - !dbgvt) (!vt - !dbgvt)
    (calc_percent (!ab3_total + !nvt - !dbgvt) (!vt - !dbgvt));    
  P.printf "Average type #: %f\n"
    ((float_of_int !type_total) /. (float_of_int !nvt))
  
type ctypeEquation = 
  {
    index: int; 
    equation: extended_sctype * extended_sctype; 
    bi_direct: bool;
  }

let mkCTEQ ndx varL varR bi =
  {index = ndx; equation = (varL,varR); bi_direct = bi;}

module CtypeEquationOrder = struct
  type t = ctypeEquation
  let compare (cte1:ctypeEquation) (cte2:ctypeEquation) = 
    Pervasives.compare cte1 cte2
end

module CtypeEqSet = Set.Make (CtypeEquationOrder)


(*
let expand_dbgtypeinfo dbgTypeInfo =
  List.flatten (
    List.map (fun (tvnm, rng, ectype) ->
      match tvnm with
      | TV_cfa off -> (
	try (
	  if List.length ectype <> 1 then
	    raise (Failure "Further check multiple type while expanding Stack")
	  else
	    let sctype = List.hd ectype in
	    begin match sctype with
	    | SC_array (sct,dimlist) ->
	      let length = List.fold_right (fun dim mult ->
		dim * mult
	      ) dimlist 1
	      in
	      let sctsize = Option.get (size_of_sctype sct) in
	      List.init length (fun i ->
		let new_off = off + sctsize * i in
		(TV_cfa new_off, rng, [sct])
	      )
	    | SC_structure (_,_,fieldlist) ->
	      let new_off = ref off in
	      List.map (fun (ft,fn) ->
		let foff = !new_off in
		new_off := !new_off + (Option.get (size_of_sctype ft));
		(TV_cfa foff, rng, [ft])
	      ) fieldlist
	    | _ -> [(tvnm,rng,ectype)]
	    end
	)
	with Failure errmsg -> begin
	  let new_errmsg = errmsg ^ "in TV_CFA" in
	  raise (Failure new_errmsg) end
      )
      | TV_addr addrnm -> (
	try (
	begin match addrnm with
	| {disp=addr_str;base=None;index=None} ->
	  let addr_mword = MWord.of_string ("0x"^addr_str) in
	  if List.length ectype <> 1 then
	    raise (Failure "Further check multiple type while expanding absAddr")
	  else
	    let sctype = List.hd ectype in
	    begin match sctype with
	    | SC_array (sct,dimlist) ->
	      let length = List.fold_right (fun dim mult ->
		dim * mult
	      ) dimlist 1
	      in
	      let sctsize = Option.get (size_of_sctype sct) in
	      List.init length (fun i ->
		let new_addr_mword = 
		  MWord.(+%) addr_mword (MWord.of_int (sctsize * i))
		in
		let new_addr_str = str_of_mword_flex new_addr_mword in
		let new_addrnm = mkADNM new_addr_str None None in
		(TV_addr new_addrnm, rng, [sct])
	      )
	    | SC_structure (_,_,fieldlist) ->
	      let new_addr_mword = ref addr_mword in
	      List.map (fun (ft,fn) ->
		let faddr_mword = !new_addr_mword in
		let faddr_mword_str = str_of_mword_flex faddr_mword in
		new_addr_mword := MWord.(+%) !new_addr_mword (MWord.of_int (Option.get (size_of_sctype ft)));
		let new_addrnm = mkADNM faddr_mword_str None None in
		(TV_addr new_addrnm, rng, [ft])
	      ) fieldlist
	    | _ -> [(tvnm,rng,ectype)]
	    end  
	| _ -> 
	  [tvnm,rng,ectype]
	end
	)
	with Failure errmsg -> begin
	  let new_errmsg = errmsg ^ " in TV_addr" in
	  raise (Failure new_errmsg) end
      )
      | _ -> [(tvnm,rng,ectype)]
    ) dbgTypeInfo
  )
*)
(*
let throw_trash_dbg dbgTypeInfo =
  (** TODO: there are problems in dbg_collection; also representation 
      of structure is important. Maybe we should collect all structure
      types into a global structure type record
  **)
  let rec filter_useful_sctype sctype =
    match sctype with
    | SC_const sct | SC_volatile sct | SC_typedef (_,sct)-> 
      filter_useful_sctype sct
    | SC_array (sct,_) -> filter_useful_sctype sct
    | SC_pointer sct -> filter_useful_sctype sct
    | SC_structure (_,_,fieldlist) ->
      List.length fieldlist <> 0
    | _ -> true
  in
  List.filter_map (fun (tvnm,rng,ectype) ->
    let useful_ectype = List.filter (fun sctype ->
      filter_useful_sctype sctype
    ) ectype
    in
    if List.is_empty useful_ectype then None
    else
      Some (tvnm,rng,useful_ectype)
  ) dbgTypeInfo
*)
(*
let eqcg_multi_connect eqcg tv1 tv1_tag tv2 tv2_tag lv =
  let tv1_repr = eqcg_find_repr eqcg tv1 in
  let tv1_node =
    if Option.is_none tv1_repr then
      let tv_node = mkEQCNode tv1 0 in
      tv_node.eqc_dbgtype <- tv1_tag;
      eqcg_add_repr eqcg tv1 tv_node.eqc_name;
      eqcg_add_node eqcg tv_node.eqc_name tv_node;
      tv_node
    else
      eqcg_find_node eqcg (Option.get tv1_repr)
  in
  let tv2_repr = eqcg_find_repr eqcg tv2 in
  let tv2_node =
    if Option.is_none tv2_repr then
      let tv_node = mkEQCNode tv2 0 in
      tv_node.eqc_dbgtype <- tv2_tag;
      eqcg_add_repr eqcg tv2 tv_node.eqc_name;
      eqcg_add_node eqcg tv_node.eqc_name tv_node;
      tv_node
    else
      eqcg_find_node eqcg (Option.get tv2_repr)
  in
  let tv2_lv_nodes = List.init lv (fun i -> mkEQCNode tv2 (i+1)) in 
  List.iter (fun tv_node ->
    eqcg_add_node eqcg tv_node.eqc_name tv_node
  ) tv2_lv_nodes;
  let tv_node_chain = (tv1_node :: (List.rev tv2_lv_nodes)) @ [tv2_node] in
  List.iteri (fun ndx tv_node ->
    if ndx = 0 then
      tv_node.eqc_preds <- (List.nth tv_node_chain (ndx+1)) :: tv_node.eqc_preds
    else if ndx = ((List.length tv_node_chain) - 1) then
      tv_node.eqc_succs <- (List.nth tv_node_chain (ndx-1)) :: tv_node.eqc_succs
    else (
      tv_node.eqc_preds <- (List.nth tv_node_chain (ndx+1)) :: tv_node.eqc_preds;
      tv_node.eqc_succs <- (List.nth tv_node_chain (ndx-1)) :: tv_node.eqc_succs
    )
  ) tv_node_chain
*)
(*

let deref_sctype struct_map sctype offset =
  let rec find_derefable sct =
    match sct with
    | SC_top _ -> sct
    | SC_typeVar _ -> raise (Failure "deref type variable")
    | SC_const sct' | SC_volatile sct' -> find_derefable sct'
    | SC_base str ->
      P.printf "WARNING: try to deref base type: %s\n" str;
      flush stdout;
      sct
    | SC_array _ | SC_pointer _ -> sct
    | SC_typedef (defnm,sct') ->
      if H.mem struct_map defnm then
	SC_typedef (defnm, (H.find struct_map defnm).struct_type)
      else
	find_derefable sct'
    | SC_structure (nm,lvnm,fdlist) ->
      if not (List.is_empty fdlist) then sct
      else if H.mem struct_map nm then
        (H.find struct_map nm).struct_type
      else
	raise (Failure "struct map may be incomplete")
    | _ ->
      let errmsg = P.sprintf "WARNING: try to deref type: %s\n" (str_of_sctype sct) in
      P.printf "%s" errmsg; flush stdout;
      sct
  in
  
  let find_memoff memoff off =
    let rec aux memoff off ndx =
      match memoff with
      | [] -> 
	raise (Failure "a structure does not have member!")
      | h :: [] -> ndx, h
      | h1 :: h2 :: t ->
	if h1 <= off && off < h2 then
	  ndx, h1
	else
	  aux (h2 :: t) off (ndx+1)
    in
    aux memoff off 0
  in
  
  let rec deref sct off = 
    match sct with
    | SC_top _ -> sct
    | SC_base str ->
      P.printf "WARNING: deref base type: %s\n" str; flush stdout;
      SC_top []
    | SC_const _ | SC_volatile _ -> 
      let target_sct = find_derefable sct in
      deref target_sct off
    | SC_typedef (defnm,sct') ->
      if H.mem struct_map defnm then
	let target_struct = H.find struct_map defnm in
	match target_struct.struct_type with
	| SC_structure (oldnm,lvnm,fdlist) ->
	  if List.is_empty fdlist then
	    raise (Failure "struct map contains dummy typedef struct type")
	  else
	    let target_size = target_struct.struct_size in
	    let target_memoff = target_struct.struct_memoff in
	    if off >= target_size then
	      let errmsg = P.sprintf "offset %s(%d) : size_of_structure %d" 
		offset off target_size
	      in
	      P.printf "WARNING:%s\n" errmsg; flush stdout;
	      let real_off = off - target_size in
	      deref sct real_off
	    else if off < 0 then
	      let errmsg = P.sprintf "offset %s(%d) : size_of_structure %d" 
		offset off target_size
	      in
	      P.printf "WARNING:%s\n" errmsg; flush stdout;
	      let real_off = off + target_size in
	      deref sct real_off
	    else
	      let pos, posoff = find_memoff target_memoff off in
	      if posoff = off then
		fst (List.nth fdlist pos)
	      else if posoff < off then
		let inner_type = fst (List.nth fdlist pos) in
		let next_off = off - posoff in
		deref inner_type next_off
	      else
		raise (Failure "Impossible")
	| _ ->
	  raise (Failure "Magically we have non-struct in struct map")
      else
	let target_sct = find_derefable sct in
	deref target_sct off
    | SC_array (sct',dimlist) -> 
      if List.is_empty dimlist then 
	sct'
      else
	let bound = calc_array_bound dimlist in
	let sctsize = size_of_sctype struct_map sct' in
	let residue = off mod sctsize in
	if residue = 0 then
	  let index = off / sctsize in
	  if index < bound then
	    sct'
	  else
	    raise (Failure "dereference out of bound for array")
	else
	  let index = (off-residue) / sctsize in
	  if index < bound then
	    deref sct' residue
	  else
	    raise (Failure "deref out of bound for array with residue") 
    | SC_structure (nm,_,_) ->
      if H.mem struct_map nm then (
	let target_struct = H.find struct_map nm in
	match target_struct.struct_type with
	| SC_structure (_,_,fdlist) ->
	  if List.is_empty fdlist then 
	    raise (Failure "struct map contains dummy struct type")
	  else
	    let target_size = target_struct.struct_size in
	    let target_memoff = target_struct.struct_memoff in
	    if off >= target_size then
	      let errmsg = P.sprintf "offset %s(%d) >= size_of_structure %d" 
		offset off target_size
	      in
	      P.printf "WARNING:%s\n" errmsg; flush stdout;
	      let real_off = off - target_size in
	      deref sct real_off
	    else if off < 0 then
	      let errmsg = P.sprintf "offset %s(%d) < size_of_structure %d" 
		offset off target_size
	      in
	      P.printf "WARNING:%s\n" errmsg; flush stdout;
	      let real_off = off + target_size in
	      deref sct real_off
	    else
	      let pos, posoff = find_memoff target_memoff off in
	      if posoff = off then
		fst (List.nth fdlist pos)
	      else if posoff < off then
		let inner_type = fst (List.nth fdlist pos) in
		let next_off = off - posoff in
		deref inner_type next_off
	      else
		raise (Failure "Impossible")
	| _ ->
	  raise (Failure "Magically we have non-struct in struct map")
      )
      else
	raise (Failure "met a structure with no name!")
    | SC_pointer sct' ->
      let target_sct' = find_derefable sct' in
      begin match target_sct' with
      | SC_typedef _ 
      | SC_structure _ -> deref target_sct' off
      | _ -> sct'
      end
    | _ ->
      let errmsg = 
	P.sprintf "deref non-derefable type: %s with off: %d" 
	  (str_of_sctype sct) off 
      in 
      raise (Failure errmsg) 
  in
  
  let rec deref_all sct =
    let target_sct = find_derefable sct in
    match target_sct with
    | SC_top _ -> sct
    | SC_base str ->
      P.printf "WARNING: deref_all base type: %s\n" str; flush stdout;
      SC_top []
    | SC_array (sct',_) -> sct'
    | SC_pointer sct' -> 
      let target_sct' = find_derefable sct' in
      begin match target_sct' with
      | SC_typedef _
      | SC_structure _ -> deref_all
      | _ -> sct'
      end
    | SC_structure (nm,lvnm,fdlist) -> SC_top []
    | _ ->
      raise (Failure "deref_all non-derefable type") 
  in

  if offset <> "all" && offset <> "none" then
    let off = Int32.to_int (Int32.of_string ("0x"^offset)) in
    deref sctype off
  else if offset = "all" then
    deref_all sctype
  else 
    let errmsg = "Magically, we have off string " ^ offset in
    raise (Failure errmsg)
*)
