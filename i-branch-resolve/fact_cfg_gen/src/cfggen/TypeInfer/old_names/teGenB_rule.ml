open Batteries
module P = Printf
module H = Hashtbl

open X86Syntax
open X86Semantics
open X86_MACHINE
open X86_RTL
open Config
open Abbrev

open TeGenB_DS
open BB
open RTL_opt
open Printer
open Config_cfg

let tvGen funCFA op l exe =
  let varnm = tvNameGen funCFA op l in
  {tv_name = varnm; tv_index = l; tv_executed = exe;}



let reg_without_esp_ebp =
  ["EAX";"ECX";"EDX";"EBX";"ESI";"EDI"]

let instr_type_infer funCFA l_instr tvNameGen track_worth eqcg =
  let l, (pre,ins,len) = l_instr in
  let inSet = preCond in

  let add_dummy_eqs tvnm_changed set =
    let dummy_eqs = List.filter_map (fun tvnm ->
      if not (List.mem tvnm tvnm_changed) then
	let varl = [SC_typeVar (tvnm,(l+len))] in
	let varr = [SC_typeVar (tvnm,l)] in
	let dummy_eq = mkCTEQ l varl varr true in
	Some dummy_eq
      else None
    ) track_worth
    in
    List.fold_right add_new_eq_to_s dummy_eqs set
  in

  let rule_pop op =
    let tvNameOp = tvNameGen funCFA op l in
    let currstack = Address_op 
      {addrDisp=Big.of_int (-4); addrBase=Some ESP; addrIndex=None;}
    in
    let tvNameCurrStack = tvNameGen funCFA currstack l in
    let varCurrStack = [SC_typeVar (tvNameCurrStack,l)] in
    let varOp = [SC_typeVar (tvNameOp,(l+len))] in
    let new_eq = mkCTEQ l varOp varCurrStack in
    let outSet = add_new_eq_to_s new_eq inSet in
    outSet
  in
  
  let rule_leave () =
    let tvNameESP = "ESP" in
    let tvNameEBP = "EBP" in
    let varESPls = [SC_typeVar (tvNameESP,(l+len))] in
    let varEBPl = [SC_typeVar (tvNameEBP,l)] in
    let new_eq1 = mkCTEQ l varESPls varEBPl in
    let outset' = add_new_eq_to_s new_eq1 inSet in
    let varEBPls = [SC_typeVar (tvNameEBP,(l+len))] in
    let currstack = Address_op
      {addrDisp=Big.zero; addrBase=Some ESP; addrIndex=None;}
    in
    let tvNameCurrStack = tvNameGen funCFA currstack l in
    let varCurrStack = [SC_typeVar (tvNameCurrStack,l)] in
    let new_eq2 = mkCTEQ l varEBPls varCurrStack in
    let outset = add_new_eq_to_s new_eq2 outset' in
    outset
  in

  let rule_add width ops opd =
    let tvNameOpd = tvNameGen funCFA opd l in
    let varOpdr = [SC_typeVar (tvNameOpd,l)] in
    let varOpdl = [SC_typeVar (tvNameOpd,(l+len))] in
    let new_eq = mkCTEQ l varOpdl varOpdr in
    let outSet = add_new_eq_to_s new_eq inSet in
    outSet
  in

  let rule_sub width ops opd =
    rule_add width ops opd
  in
  
  let rule_lea ops opd =
    let tvNameOps = tvNameGen funCFA ops l in
    let tvNameOpd = tvNameGen funCFA opd l in
    let varOps = [SC_typeVar (tvNameOps,l)] in
    let varOps_ptr =
      List.map (fun sctype -> SC_pointer sctype) varOps
    in
    let varOpd = [SC_typeVar (tvNameOpd,(l+len))] in
    let new_eq = mkCTEQ l varOpd varOps_ptr in
    let outSet = add_new_eq_to_s new_eq inSet in
    outSet
  in    

  let rule_cmp width ops opd =
    let tvNameOps = tvNameGen funCFA ops l in
    let tvNameOpd = tvNameGen funCFA opd l in
    let varOps = [SC_typeVar (tvNameOps,l)] in
    let varOpd = [SC_typeVar (tvNameOpd,l)] in
    let new_eq = mkCTEQ l varOpd varOps in
    let outSet = add_new_eq_to_s new_eq inSet in
    outSet
  in

  let rule_call dis abs op slctr =
    let _, l_cfa = List.find (fun (lopc,cfaexp) ->
      lopc = l
    ) funCFA
    in
    let cfaoff = List.map (fun (lvl,re) ->
      if lvl > 0 then split_at_eq re
      else 
	reverse_cfa_expr re
    ) l_cfa 
    in
    if List.exists (fun (reg,exp) -> reg = EBP) cfaoff then
      List.fold_right (fun regnm s ->
	let new_eq = mkCTEQ (l+len) [SC_typeVar (regnm,(l+len))] [SC_top] in
	add_new_eq_to_s new_eq s
      ) reg_without_esp_ebp preCond
    else
      List.fold_right (fun regnm s ->
	let new_eq = mkCTEQ (l+len) [SC_typeVar (regnm,(l+len))] [SC_top] in
	add_new_eq_to_s new_eq s
      ) ("EBP" :: reg_without_esp_ebp) preCond
  in

