open Batteries
module P = Printf
module H = Hashtbl

open X86Syntax
open X86Semantics
open X86_MACHINE
open X86_RTL
open Config
open Abbrev

open TeGenB_DS
open BB
open RTL_opt
open Printer
open Config_cfg

let instr_eqcgGen l_instr funCFA dbgTypeInfo eqcg track_worth crossbb struct_map =
  let l, (pre,ins,len) = l_instr in
  
  let dbgtag tv =
    dbgtype_tag tv dbgTypeInfo
  in
  
  let consrv = false in

  let inner_eqcg_merge eqcg tv1 tv1_tag edge1 tv2 tv2_tag edge2 =
    if consrv then
      eqcg_merge_consrv eqcg tv1 tv1_tag edge1 tv2 tv2_tag edge2
    else
      eqcg_merge eqcg tv1 tv1_tag edge1 tv2 tv2_tag edge2
  in
  
  let mktv tvnm l crossbb =
    if crossbb then
      mkTV tvnm (l-len) true
    else
      mkTV tvnm l false
  in

  let add_trivial donthandle doconnect =
    List.iter (fun tvnm ->
      if not (List.mem tvnm donthandle) then
	let tv1,edge1 = mktv tvnm (l+len) crossbb in
	let tv1_tag = dbgtag tv1 in
	let tv2,edge2 = mktv tvnm l false in
	let tv2_tag = dbgtag tv2 in
	if not (List.mem tvnm doconnect) then
	  inner_eqcg_merge eqcg tv1 tv1_tag edge1 tv2 tv2_tag edge2
	else
	  eqcg_connect eqcg tv1 tv1_tag edge1 [(tv2,tv2_tag,edge2)]
    ) track_worth
  in

  let deref_assign opd ops = (** op1:dest; op2:src **)
    let tvnmd = tvNameGen funCFA opd (l+len) in
    match tvnmd with
    | TV_imm _ | TV_addr _ -> (** impossible and meaningless cases **)
      add_trivial [] []
    | _ -> (** destination is meaningful **)
      let tvd,edged = mktv tvnmd (l+len) crossbb in (** edged should be 0**)
      assert(fst edged = 0);
      let tvd_tag = dbgtag tvd in
      let tvnms = tvNameGen funCFA ops l in
      let tvs,edges = mktv tvnms l false in
      begin match tvs.tv_name with
      (*| TV_cfa off when off < 0 ->
      	let tvs_tag = dbgtag tvs in
	eqcg_connect eqcg tvd tvd_tag edged [(tvs,tvs_tag,edges)];
	add_trivial [tvd.tv_name] [tvs.tv_name]*)
      | TV_imm _ 
      | TV_reg _ | TV_cfa _ ->
	let tvs_tag = dbgtag tvs in
	if fst edges = 0 then
	  eqcg_merge eqcg tvd tvd_tag edged tvs tvs_tag edges
	else 
	  eqcg_connect eqcg tvd tvd_tag edged [(tvs,tvs_tag,edges)];
	add_trivial [tvd.tv_name] []
      | TV_addr _ ->
	assert (fst edges = 1);
	let tvs_tag = dbgtag tvs in
	(*let evaluated_tag = reason_ectype tvs_tag [edges] struct_map in*)
	eqcg_connect eqcg tvd tvd_tag edged [tvs,tvs_tag,edges];
	add_trivial [tvd.tv_name] [] 
      end
  in
  
  let rule_lea_consrv ops opd =
    let tvnmopd = tvNameGen funCFA opd (l+len) in
    add_trivial [tvnmopd] []
  in

  let rule_lea ops opd =
    let tvnmd = tvNameGen funCFA opd (l+len) in
    match tvnmd with
    | TV_imm _ | TV_addr _ -> (** impossible and meaningless cases **)
      add_trivial [] []
    | _ ->
      let tvd,edged = mktv tvnmd (l+len) crossbb in (** edged should be 0**)
      let tvd_tag = dbgtag tvd in      
      assert((fst edged) = 0);
      let tvnms = tvNameGen funCFA ops l in
      begin match tvnms with
      | TV_imm _ | TV_reg _ -> (** impossible cases **)
        assert false
      | TV_cfa _ | TV_addr _ -> (** two kinds of addresses **)
	let tvs,edges = mktv tvnms l false in
	begin match tvs.tv_name with
	| TV_imm _ -> (** impossible case **)
	  assert false
	| TV_reg _ -> (** must from off(reg,_) address **)
	  assert ((fst edges) = 1);
	  let tvs_tag = dbgtag tvs in
	  eqcg_merge eqcg tvd tvd_tag edged tvs tvs_tag (0,snd edges);
	  (*eqcg_connect eqcg tvd tvd_tag edged [(tvs,tvs_tag,(0, snd edges))];*)
	  add_trivial [tvd.tv_name] []
	| TV_cfa _ ->
	  let tvs_tag = dbgtag tvs in
	  eqcg_connect eqcg tvd tvs_tag edged [(tvs,tvs_tag,(2,snd edges))];
	  add_trivial [tvd.tv_name] []
	| TV_addr addrnm -> (** only abs address **)
	  assert (addrnm.base = None && addrnm.index = None);
	  assert ((fst edges) = 1);
	  (*let dbgident,_ = mktv (TV_imm addrnm.disp) l false in*)
	  let tvs_tag = dbgtag tvs in
	  (*let evaluated_tag = reason_ectype tvs_tag [edges] struct_map in*)
	  eqcg_merge eqcg tvd tvd_tag edged tvs tvs_tag (0,snd edges); 
	  (*eqcg_connect eqcg tvd tvd_tag edged [tvs,tvs_tag,(0,snd edges)];*)
	  add_trivial [tvd.tv_name] []
	end
      end
  in

  let rule_push width op =
    let currstack = Address_op 
      {addrDisp=Big.zero; addrBase=Some ESP; addrIndex=None;}
    in
    deref_assign currstack op;
  in

  let rule_mov width ops opd =
    deref_assign opd ops
  in

  let rule_pop op =
    let currstack = Address_op
      {addrDisp = Big.zero; addrBase = Some ESP; addrIndex=None;}
    in
    deref_assign op currstack
  in
  
  let rule_leave () =
    let opEBP = Reg_op EBP in
    let opESP = Reg_op ESP in
    let tvEBP_name = tvNameGen funCFA opEBP l in
    let tvESP_name = tvNameGen funCFA opESP (l+len) in
    let tvEBP,edgeEBP = mktv tvEBP_name l false in
    let tvESP,edgeESP = mktv tvESP_name (l+len) crossbb in
    let tvEBP_tag = dbgtag tvEBP in
    let tvESP_tag = dbgtag tvESP in
    inner_eqcg_merge eqcg tvESP tvESP_tag edgeEBP tvEBP tvEBP_tag edgeESP;

    let currstack = Address_op
      {addrDisp=Big.zero; addrBase=Some ESP; addrIndex=None;}
    in
    let tv1_name = tvEBP_name in
    let tv1,edge1 = mktv tv1_name (l+len) crossbb in
    let tv2_name = tvNameGen funCFA currstack (l+len) in
    let tv2,edge2 = mktv tv2_name l false in
    let tv1_tag = dbgtag tv1 in
    let tv2_tag = dbgtag tv2 in
    assert ((fst edge1 = 0) && (fst edge2 = 0));
    eqcg_merge eqcg tv1 tv1_tag edge1 tv2 tv2_tag edge2;
    (*eqcg_connect eqcg tv1 tv1_tag edge1 [(tv2,tv2_tag,edge2)];*)
    add_trivial [tvESP_name;tvEBP_name] []
  in

  let rule_cmov ops opd =
    let tvnmd = tvNameGen funCFA opd (l+len) in
    match tvnmd with
    | TV_imm _ | TV_addr _ -> (** impossible and meaningless cases **)
      add_trivial [] []
    | _ -> (** destination is meaningful **)
      let tvd,edged = mktv tvnmd (l+len) crossbb in (** edged should be 0**)
      assert(fst edged = 0);
      let tvd_tag = dbgtag tvd in
      let tvnms = tvNameGen funCFA ops l in
      let tvs,edges = mktv tvnms l false in
      begin match tvs.tv_name with
      (*| TV_cfa off when off < 0 ->
	let tvs_tag = dbgtag tvs in
	eqcg_connect eqcg tvd tvd_tag edged [(tvs,tvs_tag,edges)];
	add_trivial [] [tvs.tv_name;tvd.tv_name]*)
      | TV_imm _
      | TV_reg _ | TV_cfa _->
	let tvs_tag = dbgtag tvs in
	eqcg_connect eqcg tvd tvd_tag edged [(tvs,tvs_tag,edges)];
	add_trivial [] [tvd.tv_name]
      | TV_addr _ ->
	let tvs_tag = dbgtag tvs in
	(*let evaluated_tag = reason_ectype tvs_tag [edges] struct_map in*)
	(*eqcg_connect eqcg tvd evaluated_tag edged [];*)
	eqcg_connect eqcg tvd tvd_tag edged [tvs,tvs_tag,edges];
	add_trivial [] [tvd.tv_name] 
      end
  in

  let rule_xchg op1 op2 =
    let tvnm1 = tvNameGen funCFA op1 l in
    let tvnm2 = tvNameGen funCFA op2 l in
    if tvnm1 = tvnm2 then add_trivial [] []
    else (
      match tvnm1, tvnm2 with
      | (TV_imm _ , _) | (_, TV_imm _) | (TV_addr _, TV_addr _)-> 
      (** impossible and meaningless cases **)
	assert false
      | TV_addr _, TV_reg _ ->
	deref_assign op2 op1
      | TV_reg _, TV_addr _ ->
	deref_assign op1 op2
      | _ ->
	let tv11,edge11 = mktv tvnm1 l false in
	let tv12,edge12 = mktv tvnm1 (l+len) crossbb in
	let tv21,edge21 = mktv tvnm2 l false in
	let tv22,edge22 = mktv tvnm2 (l+len) crossbb in
	let tv11_tag = dbgtag tv11 in
	let tv12_tag = dbgtag tv12 in
	let tv21_tag = dbgtag tv21 in
	let tv22_tag = dbgtag tv22 in
	inner_eqcg_merge eqcg tv11 tv11_tag edge11 tv22 tv22_tag edge22;
	inner_eqcg_merge eqcg tv21 tv21_tag edge21 tv12 tv12_tag edge12;
	add_trivial [tvnm1; tvnm2] []
    )
  in

  let rule_call dis abs op slctr =
    (** assume return value is always stored in EAX **)
    (** TODO: read calling convention to make sure **)
    let tvnm = TV_reg EAX in
    add_trivial [tvnm] []
  in

  let rule_stos () =
    let tvnm = TV_reg EAX in
    add_trivial [tvnm] []
  in

  let handle_boolop opd =
    let tvnm = tvNameGen funCFA opd (l+len) in
    add_trivial [tvnm] []
  in  
  
  let rule_xor ops opd =
    match ops,opd with
    | Reg_op regs, Reg_op regd ->
      if regs = regd then
	handle_boolop opd
      else
	add_trivial [] []
    | _ -> 
      add_trivial [] []
  in

  let rule_and ops opd =
    handle_boolop opd
  in

  let rule_or ops opd =
    handle_boolop opd
  in

  let rule_neg opd =
    handle_boolop opd
  in

  let rule_sar ops opd =
    handle_boolop opd
  in

  let rule_shl ops opd =
    handle_boolop opd
  in
  
  let rule_shr ops opd =
    handle_boolop opd
  in

  let rule_setcc opd =
    let tvnmopd = tvNameGen funCFA opd (l+len) in
    add_trivial [tvnmopd] []
  in
  
  let rule_cdq () =
    let tvnm = TV_reg EDX in
    add_trivial [tvnm] []
  in

  let rule_bswap reg =
    let tvnm = TV_reg reg in
    add_trivial [tvnm] []
  in

  let rule_shld reg =
    let tvnm = TV_reg reg in
    add_trivial [tvnm] []
  in

  let rule_shrd reg =
    let tvnm = TV_reg reg in
    add_trivial [tvnm] []
  in

  if is_trivial ins then
    add_trivial [] []
  else if is_evil ins then
    ()
  else begin match ins with
  | PUSH(width,op) -> rule_push width op
  | MOV(width,opd,ops) -> rule_mov width ops opd
  | MOVZX(width,opd,ops) -> rule_mov width ops opd
  | MOVSX(width,opd,ops) -> rule_mov width ops opd
  | POP(op) -> rule_pop op
  | LEAVE -> rule_leave ()
  | LEA(opd,ops) -> (*rule_lea_consrv ops opd*) rule_lea ops opd
  | CMOVcc(cond,opd,ops) -> rule_cmov ops opd
  | CALL(dis,abs,op,slctr) -> rule_call dis abs op slctr
  | XOR(width,opd,ops) -> rule_xor ops opd
  | AND(width,opd,ops) -> rule_and ops opd
  | OR(width,opd,ops) -> rule_or ops opd
  | NEG(width,opd) -> rule_neg opd
  | NOT(width,opd) -> handle_boolop opd
  | SHL(width,opd,ops) -> rule_shl ops opd
  | SHLD(width,reg,regimm) -> rule_shld reg
  | SHR(width,opd,ops) -> rule_shr ops opd
  | SHRD(width,reg,regimm) -> rule_shrd reg
  | SAR(width,opd,ops) -> rule_sar ops opd
  | ROL(width,opd,ops) -> handle_boolop opd
  | ROR(width,opd,ops) -> handle_boolop opd
  | STOS(width) -> rule_stos ()
  | XCHG(width,opd,ops) -> rule_xchg ops opd
  | SETcc(cond,opd) -> rule_setcc opd
  | CDQ -> rule_cdq ()
  | BSWAP reg -> rule_bswap reg
  | BSR(opd,ops) -> handle_boolop opd
  | _ -> 
    if !report_unsupport then
      P.printf "Unsupported instruction at %x: %s\n" l (Instr.str_of_instr (pre,ins)); flush stdout;
    ()

(*| ADD(width,opd,ops) -> rule_add width ops opd
  | SUB(width,opd,ops) -> rule_sub width ops opd
  | CMP(width,opd,ops) -> rule_cmp width ops opd
  | CALL(dis,abs,op,slctr) -> rule_call dis abs op slctr; []
*)
  (*
    let new_eq = mkCTEQ l [SC_allTypeVar (l+len)] [SC_top] in
    add_new_eq_to_s new_eq preCond
  *)
  end
  
let handle_control_flows preds bb funCFA dbgTypeInfo eqcg track_worth struct_map =
  List.iter (fun tvnm ->
    let tv1_ndx = bb.bb_relAddr in
    let tv1,edge1 = mkTV tvnm tv1_ndx false in
    let tv1_tag = dbgtype_tag tv1 dbgTypeInfo in
    let tagged_tv_list = List.map (fun pred_bb ->
      let _,_,len = List.last pred_bb.bb_instrs in
      let end_addr = pred_bb.bb_relAddr + pred_bb.bb_size - len in
      let tv, edge = mkTV tvnm end_addr true in
      let tv_tag = dbgtype_tag tv dbgTypeInfo in
      tv, tv_tag, edge
    ) preds
    in
    eqcg_connect eqcg tv1 tv1_tag edge1 tagged_tv_list
  ) track_worth

let bb_eqcgGen bb funCFA dbgTypeInfo eqcg track_worth struct_map =
  let addrcount = ref bb.bb_relAddr in
  let l_instr_list = 
    List.map (fun (pre,ins,len) -> 
      let outaddr = !addrcount in
      addrcount := len + !addrcount;
      outaddr, (pre,ins,len)
    ) bb.bb_instrs 
  in
  List.iteri (fun ndx l_instr ->
    let crossbb = ((ndx + 1) = List.length l_instr_list) in
    instr_eqcgGen l_instr funCFA dbgTypeInfo eqcg track_worth crossbb struct_map
  ) l_instr_list

(*
  let config_dereference_nodes eqcg dbgTypeInfo =
  let _, nodes = eqcg in
  H.iter (fun repr eqc ->
    if List.is_empty eqc.eqc_preds then
      TypeVarSet.iter (fun tv ->
	let tv_tag = dbgtype_tag tv dbgTypeInfo in
	match tv.tv_name with
	| TV_addr addrnm ->
	  begin match addrnm with
	  | {disp = indisp; base = Some reg; index = None} ->
	    let new_tv = mkTV (TV_reg reg) tv.tv_index tv.tv_crossbb in
	    let new_tv_tag = dbgtype_tag new_tv dbgTypeInfo in
	    eqcg_connect eqcg tv tv_tag [(new_tv, new_tv_tag)] (-1)
	  | {disp = indisp; base = Some regbase; index = Some (scl,regindex)} ->
	    let new_tv = mkTV (TV_reg regbase) tv.tv_index tv.tv_crossbb in
	    let new_tv_tag = dbgtype_tag new_tv dbgTypeInfo in
	    eqcg_connect eqcg tv tv_tag [(new_tv, new_tv_tag)] (-1)
	  | _ -> ()
	  end
	| _ -> ()
      ) eqc.eqc_vars
  ) nodes
*)

let func_eqcgGen asm_fun funCFA dbgTypeInfo eqcg struct_map =
  let consrv = false in
  
  let track_worth =
    (*define_worth_tracking funCFA asm_fun*)
    if consrv then    
      consrv_worth_tracking funCFA asm_fun
    else 
      aggres_worth_tracking funCFA asm_fun
  in
  (*
  P.printf "track_worth has been collected!\n";
  List.iter (fun tvnm -> P.printf "%s\n" (str_of_tvnm tvnm)) track_worth;
  flush stdout;
  *)
  List.iter (fun bb ->
    let possible_preds = 
      List.filter_map (fun bb' ->
	if List.mem bb.bb_label (bb'.bb_inter_succs @ bb'.bb_succs) then
	  Some bb'
	else
	  None
      ) asm_fun.func_bbs
    in
    bb_eqcgGen bb funCFA dbgTypeInfo eqcg track_worth struct_map;
    handle_control_flows possible_preds bb funCFA dbgTypeInfo eqcg track_worth struct_map;
  ) asm_fun.func_bbs


(*config_dereference_nodes eqcg dbgTypeInfo*)

(*
    H.fold (fun repr node node_list ->
      if (List.is_empty (node.eqc_succs @ node.eqc_preds) && List.is_empty node.eqc_dbgtype) 
	|| List.exists (fun node' -> node'.eqc_name = node.eqc_name) node_list
      then
	node_list
      else
	node :: node_list
    ) varmap []
*)

(** from assembly basic blocks to equivalence class graph **)
(*let func_type_infer_graph funCFA asm_fun =
  let tibbl = 
    List.map (fun bb ->
    let inter_preds = 
      try
	List.filter_map (fun bb' ->
	  if List.mem bb.bb_label bb'.bb_inter_succs then
	    Some bb'.bb_label
	  else
	    None
	) asm_fun.func_bbs
      with Not_found -> []
    in
    let eqset = 
      bb_type_infer funCFA CtypeEqSet.empty bb
    in
    mkTIBB bb inter_preds eqset
  ) asm_fun.func_bbs
  in
*)


(** func_type_infer: cfa_repr -> sg_condition -> func -> (...) -> () **)
(*
let func_type_infer funCFA preCond asm_fun tvNameGen =
  let workq = Queue.create () in
  
  let entry_bb_label = (List.hd asm_fun.func_bbs).bb_label in
  
  Queue.add (preCond, entry_bb_label) workq;

  let add_new_task task_tibb post =
    if not (does_bb_ret task_tibb.ti_bb) then (
      List.iter (fun bb_label ->
	if List.exists (fun func_bb -> 
	  func_bb.bb_label = bb_label
	) asm_fun.func_bbs && bb_label <> asm_fun.func_low then (
	  Queue.add (post, bb_label) workq
	)
      ) (task_tibb.ti_bb.bb_succs @ task_tibb.ti_bb.bb_inter_succs)
    ) 
  in

  let tibbl = ref [] in
  
  while (not (Queue.is_empty workq)) do
    let task_pre, task_bb_label = Queue.take workq in
    try
      let task_tibb = 
	List.find (fun tibb ->
	  tibb.ti_bb.bb_label = task_bb_label
	) !tibbl 
      in
      if Option.is_none task_tibb.ti_pre then (
	P.printf "not handled properly\n"; flush stdout;
	task_tibb.ti_pre <- Some task_pre;
	let post = bb_type_infer funCFA task_pre task_tibb.ti_bb tvNameGen in
	task_tibb.ti_post <- Some post;
	add_new_task task_tibb post
      ) else (
	let old_pre = Option.get task_tibb.ti_pre in
	if not (eq_s old_pre task_pre) then (
	  let union_pre = union_s old_pre task_pre in
	  task_tibb.ti_pre <- Some union_pre;
	  let post = bb_type_infer funCFA union_pre task_tibb.ti_bb tvNameGen in
	  task_tibb.ti_post <- Some post;
	  add_new_task task_tibb post
	)
      )
    with Not_found ->
      try
	let task_bb = List.find (fun func_bb -> 
	  func_bb.bb_label = task_bb_label
	) asm_fun.func_bbs
	in
	let task_tibb = mkTIBB (Some task_pre) task_bb None in
	let post = bb_type_infer funCFA task_pre task_bb tvNameGen in
	task_tibb.ti_post <- Some post;
	tibbl := task_tibb :: !tibbl;
	add_new_task task_tibb post
      with Not_found ->
        P.printf "missing basic block: %s\n" task_bb_label; flush stdout;
	raise (Failure "basic block label not found in function")
  done;
  let sorted_tibbl =
    List.sort (fun tibb1 tibb2 ->
      tibb1.ti_bb.bb_relAddr - tibb2.ti_bb.bb_relAddr
    ) !tibbl
  in
  List.iter dump_tibb sorted_tibbl; flush stdout
*)
(*
let merge_point_finder bb_list =
  List.filter_map (fun bb ->
    let inter_pred_bool = 
      List.fold_right (fun bb' b ->
	if List.mem bb.bb_label bb'.bb_inter_succs then
	  true || b
	else
	  false || b
      ) bb_list false
    in
    if List.length bb.bb_preds > 1 || (List.length bb.bb_preds = 1 && inter_pred_bool) then
      let preds = 
	List.filter_map (fun bb' ->
	  if List.mem bb.bb_label bb'.bb_succs then
	    let _,last_ins,len = List.last bb'.bb_instrs in
	    Some (bb'.bb_size + bb'.bb_relAddr - len)
	  else
	    None
	) bb_list
      in
      if not (List.is_empty preds) then
	Some (bb.bb_label, preds)
      else
	None
    else
      None
  ) bb_list

(** no worklist algorithm is needed **)
let func_type_infer_linear funCFA preCond asm_fun tvNameGen =
  let base_eqset = 
    List.fold_left (fun pre bb ->
      bb_type_infer funCFA pre bb tvNameGen
    ) preCond asm_fun.func_bbs
  in
  let merge_point_list = merge_point_finder asm_fun.func_bbs in
  let new_eqs = 
    List.flatten (List.map (fun mrgpnt -> 
      let lbl,preds = mrgpnt in
      let mergebb = List.find (fun bb ->
	bb.bb_label = lbl
      ) asm_fun.func_bbs
      in
      let addrcount = ref mergebb.bb_relAddr in
      let concerned_varnames = 
	List.flatten (List.map (fun (pre,ins,len) ->
	  let eq_ndx = !addrcount in
	  let tyeq_list = List.filter (fun eq -> 
	    eq.index = eq_ndx
	  ) (CtypeEqSet.elements base_eqset) 
	  in
	  let concerned_vars = 
	    List.flatten (List.map (fun tyeq ->
	      snd tyeq.equation
	    ) tyeq_list) 
	  in
	  let varnames = 
	    List.filter_map (fun sctype ->
	      match sctype with
	      | SC_typeVar(tvnm,tvndx) -> Some tvnm
	      | _ -> None
	    ) concerned_vars
	  in
	  let varnames = 
	    elim_dup (fun str1 str2 -> str1 = str2) varnames
	  in
	  addrcount := len + !addrcount;
	  varnames
	) mergebb.bb_instrs)
      in
      List.map (fun varname ->
	let ectyper = List.map (fun pred ->
	  SC_typeVar (varname,pred)
	) preds
	in
	let ectypel = [SC_typeVar (varname,mergebb.bb_relAddr)] in
	mkCTEQ mergebb.bb_relAddr ectypel ectyper
      ) concerned_varnames
    ) merge_point_list)
  in
  let mrgInsert_eqset = List.fold_left (fun eqset new_eq ->
    add_new_eq_to_s new_eq eqset
  ) base_eqset new_eqs
  in
  print_cond mrgInsert_eqset
  *)
