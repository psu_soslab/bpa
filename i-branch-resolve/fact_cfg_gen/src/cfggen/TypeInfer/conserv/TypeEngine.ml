open Batteries
module P = Printf
module H = Hashtbl

open X86Syntax
open X86Semantics
open X86_MACHINE
open X86_RTL
open Config
(*open Abbrev*)

open BB
open Ibaux

open TypeEngineLib

(** EQCG generation **)
let instr_eqcgGen l_instr funCFA dbgTypeInfo eqcg track_worth crossbb struct_map =
  let l, (pre,ins,len) = l_instr in
  
  let dbgtag tv =
    dbgtype_tag tv dbgTypeInfo
  in
  
  let consrv = false in

  let inner_eqcg_merge eqcg tv1 tv1_tag edge1 tv2 tv2_tag edge2 =
    if consrv then
      eqcg_merge_consrv eqcg tv1 tv1_tag edge1 tv2 tv2_tag edge2
    else
      eqcg_merge eqcg tv1 tv1_tag edge1 tv2 tv2_tag edge2
  in
  
  let mktv tvnm l crossbb =
    if crossbb then
      mkTV tvnm (l-len) true
    else
      mkTV tvnm l false
  in

  let add_trivial donthandle doconnect =
    List.iter (fun tvnm ->
      if not (List.mem tvnm donthandle) then
	let tv1,edge1 = mktv tvnm (l+len) crossbb in
	let tv1_tag = dbgtag tv1 in
	let tv2,edge2 = mktv tvnm l false in
	let tv2_tag = dbgtag tv2 in
	if not (List.mem tvnm doconnect) then
	  inner_eqcg_merge eqcg tv1 tv1_tag edge1 tv2 tv2_tag edge2
	else
	  eqcg_connect eqcg tv1 tv1_tag edge1 [(tv2,tv2_tag,edge2)]
    ) track_worth
  in

  let deref_assign opd ops = (** op1:dest; op2:src **)
    let tvnmd = tvNameGen funCFA opd (l+len) in
    match tvnmd with
    | TV_imm _ | TV_addr _ -> (** impossible and meaningless cases **)
      add_trivial [] []
    | _ -> (** destination is meaningful **)
      let tvd,edged = mktv tvnmd (l+len) crossbb in (** edged should be 0**)
      assert(fst edged = 0);
      let tvd_tag = dbgtag tvd in
      let tvnms = tvNameGen funCFA ops l in
      let tvs,edges = mktv tvnms l false in
      begin match tvs.tv_name with
      (*| TV_cfa off when off < 0 ->
      	let tvs_tag = dbgtag tvs in
	eqcg_connect eqcg tvd tvd_tag edged [(tvs,tvs_tag,edges)];
	add_trivial [tvd.tv_name] [tvs.tv_name]*)
      | TV_imm _ 
      | TV_reg _ | TV_cfa _ ->
	let tvs_tag = dbgtag tvs in
	if fst edges = 0 then
	  eqcg_merge eqcg tvd tvd_tag edged tvs tvs_tag edges
	else 
	  eqcg_connect eqcg tvd tvd_tag edged [(tvs,tvs_tag,edges)];
	add_trivial [tvd.tv_name] []
      | TV_addr _ ->
	assert (fst edges = 1);
	let tvs_tag = dbgtag tvs in
	(*let evaluated_tag = reason_ectype tvs_tag [edges] struct_map in*)
	eqcg_connect eqcg tvd tvd_tag edged [tvs,tvs_tag,edges];
	add_trivial [tvd.tv_name] [] 
      end
  in
  
  let rule_lea_consrv ops opd =
    let tvnmopd = tvNameGen funCFA opd (l+len) in
    add_trivial [tvnmopd] []
  in

  let rule_lea ops opd =
    let tvnmd = tvNameGen funCFA opd (l+len) in
    match tvnmd with
    | TV_imm _ | TV_addr _ -> (** impossible and meaningless cases **)
      add_trivial [] []
    | _ ->
      let tvd,edged = mktv tvnmd (l+len) crossbb in (** edged should be 0**)
      let tvd_tag = dbgtag tvd in      
      assert((fst edged) = 0);
      let tvnms = tvNameGen funCFA ops l in
      begin match tvnms with
      | TV_imm _ | TV_reg _ -> (** impossible cases **)
        assert false
      | TV_cfa _ | TV_addr _ -> (** two kinds of addresses **)
	let tvs,edges = mktv tvnms l false in
	begin match tvs.tv_name with
	| TV_imm _ -> (** impossible case **)
	  assert false
	| TV_reg _ -> (** must from off(reg,_) address **)
	  assert ((fst edges) = 1);
	  let tvs_tag = dbgtag tvs in
	  eqcg_merge eqcg tvd tvd_tag edged tvs tvs_tag (0,snd edges);
	  (*eqcg_connect eqcg tvd tvd_tag edged [(tvs,tvs_tag,(0, snd edges))];*)
	  add_trivial [tvd.tv_name] []
	| TV_cfa _ ->
	  let tvs_tag = dbgtag tvs in
	  eqcg_connect eqcg tvd tvs_tag edged [(tvs,tvs_tag,(2,snd edges))];
	  add_trivial [tvd.tv_name] []
	| TV_addr addrnm -> (** only abs address **)
	  assert (addrnm.base = None && addrnm.index = None);
	  assert ((fst edges) = 1);
	  (*let dbgident,_ = mktv (TV_imm addrnm.disp) l false in*)
	  let tvs_tag = dbgtag tvs in
	  (*let evaluated_tag = reason_ectype tvs_tag [edges] struct_map in*)
	  eqcg_merge eqcg tvd tvd_tag edged tvs tvs_tag (0,snd edges); 
	  (*eqcg_connect eqcg tvd tvd_tag edged [tvs,tvs_tag,(0,snd edges)];*)
	  add_trivial [tvd.tv_name] []
	end
      end
  in

  let rule_push width op =
    let currstack = Address_op 
      {addrDisp=Big.zero; addrBase=Some ESP; addrIndex=None;}
    in
    deref_assign currstack op;
  in

  let rule_mov width ops opd =
    deref_assign opd ops
  in

  let rule_pop op =
    let currstack = Address_op
      {addrDisp = Big.zero; addrBase = Some ESP; addrIndex=None;}
    in
    deref_assign op currstack
  in
  
  let rule_leave () =
    let opEBP = Reg_op EBP in
    let opESP = Reg_op ESP in
    let tvEBP_name = tvNameGen funCFA opEBP l in
    let tvESP_name = tvNameGen funCFA opESP (l+len) in
    let tvEBP,edgeEBP = mktv tvEBP_name l false in
    let tvESP,edgeESP = mktv tvESP_name (l+len) crossbb in
    let tvEBP_tag = dbgtag tvEBP in
    let tvESP_tag = dbgtag tvESP in
    inner_eqcg_merge eqcg tvESP tvESP_tag edgeEBP tvEBP tvEBP_tag edgeESP;

    let currstack = Address_op
      {addrDisp=Big.zero; addrBase=Some ESP; addrIndex=None;}
    in
    let tv1_name = tvEBP_name in
    let tv1,edge1 = mktv tv1_name (l+len) crossbb in
    let tv2_name = tvNameGen funCFA currstack (l+len) in
    let tv2,edge2 = mktv tv2_name l false in
    let tv1_tag = dbgtag tv1 in
    let tv2_tag = dbgtag tv2 in
    assert ((fst edge1 = 0) && (fst edge2 = 0));
    eqcg_merge eqcg tv1 tv1_tag edge1 tv2 tv2_tag edge2;
    (*eqcg_connect eqcg tv1 tv1_tag edge1 [(tv2,tv2_tag,edge2)];*)
    add_trivial [tvESP_name;tvEBP_name] []
  in

  let rule_cmov ops opd =
    let tvnmd = tvNameGen funCFA opd (l+len) in
    match tvnmd with
    | TV_imm _ | TV_addr _ -> (** impossible and meaningless cases **)
      add_trivial [] []
    | _ -> (** destination is meaningful **)
      let tvd,edged = mktv tvnmd (l+len) crossbb in (** edged should be 0**)
      assert(fst edged = 0);
      let tvd_tag = dbgtag tvd in
      let tvnms = tvNameGen funCFA ops l in
      let tvs,edges = mktv tvnms l false in
      begin match tvs.tv_name with
      (*| TV_cfa off when off < 0 ->
	let tvs_tag = dbgtag tvs in
	eqcg_connect eqcg tvd tvd_tag edged [(tvs,tvs_tag,edges)];
	add_trivial [] [tvs.tv_name;tvd.tv_name]*)
      | TV_imm _
      | TV_reg _ | TV_cfa _->
	let tvs_tag = dbgtag tvs in
	eqcg_connect eqcg tvd tvd_tag edged [(tvs,tvs_tag,edges)];
	add_trivial [] [tvd.tv_name]
      | TV_addr _ ->
	let tvs_tag = dbgtag tvs in
	(*let evaluated_tag = reason_ectype tvs_tag [edges] struct_map in*)
	(*eqcg_connect eqcg tvd evaluated_tag edged [];*)
	eqcg_connect eqcg tvd tvd_tag edged [tvs,tvs_tag,edges];
	add_trivial [] [tvd.tv_name] 
      end
  in

  let rule_xchg op1 op2 =
    let tvnm1 = tvNameGen funCFA op1 l in
    let tvnm2 = tvNameGen funCFA op2 l in
    if tvnm1 = tvnm2 then add_trivial [] []
    else (
      match tvnm1, tvnm2 with
      | (TV_imm _ , _) | (_, TV_imm _) | (TV_addr _, TV_addr _)-> 
      (** impossible and meaningless cases **)
	assert false
      | TV_addr _, TV_reg _ ->
	deref_assign op2 op1
      | TV_reg _, TV_addr _ ->
	deref_assign op1 op2
      | _ ->
	let tv11,edge11 = mktv tvnm1 l false in
	let tv12,edge12 = mktv tvnm1 (l+len) crossbb in
	let tv21,edge21 = mktv tvnm2 l false in
	let tv22,edge22 = mktv tvnm2 (l+len) crossbb in
	let tv11_tag = dbgtag tv11 in
	let tv12_tag = dbgtag tv12 in
	let tv21_tag = dbgtag tv21 in
	let tv22_tag = dbgtag tv22 in
	inner_eqcg_merge eqcg tv11 tv11_tag edge11 tv22 tv22_tag edge22;
	inner_eqcg_merge eqcg tv21 tv21_tag edge21 tv12 tv12_tag edge12;
	add_trivial [tvnm1; tvnm2] []
    )
  in

  let rule_call dis abs op slctr =
    (** assume return value is always stored in EAX **)
    (** TODO: read calling convention to make sure **)
    let tvnm = TV_reg EAX in
    add_trivial [tvnm] []
  in

  let rule_stos () =
    let tvnm = TV_reg EAX in
    add_trivial [tvnm] []
  in

  let handle_boolop opd =
    let tvnm = tvNameGen funCFA opd (l+len) in
    add_trivial [tvnm] []
  in  
  
  let rule_xor ops opd =
    match ops,opd with
    | Reg_op regs, Reg_op regd ->
      if regs = regd then
	handle_boolop opd
      else
	add_trivial [] []
    | _ -> 
      add_trivial [] []
  in

  let rule_and ops opd =
    handle_boolop opd
  in

  let rule_or ops opd =
    handle_boolop opd
  in

  let rule_neg opd =
    handle_boolop opd
  in

  let rule_sar ops opd =
    handle_boolop opd
  in

  let rule_shl ops opd =
    handle_boolop opd
  in
  
  let rule_shr ops opd =
    handle_boolop opd
  in

  let rule_setcc opd =
    let tvnmopd = tvNameGen funCFA opd (l+len) in
    add_trivial [tvnmopd] []
  in
  
  let rule_cdq () =
    let tvnm = TV_reg EDX in
    add_trivial [tvnm] []
  in

  let rule_bswap reg =
    let tvnm = TV_reg reg in
    add_trivial [tvnm] []
  in

  let rule_shld reg =
    let tvnm = TV_reg reg in
    add_trivial [tvnm] []
  in

  let rule_shrd reg =
    let tvnm = TV_reg reg in
    add_trivial [tvnm] []
  in

  if is_trivial ins then
    add_trivial [] []
  else if is_evil ins then
    ()
  else begin match ins with
  | PUSH(width,op) -> rule_push width op
  | MOV(width,opd,ops) -> rule_mov width ops opd
  | MOVZX(width,opd,ops) -> rule_mov width ops opd
  | MOVSX(width,opd,ops) -> rule_mov width ops opd
  | POP(op) -> rule_pop op
  | LEAVE -> rule_leave ()
  | LEA(opd,ops) -> (*rule_lea_consrv ops opd*) rule_lea ops opd
  | CMOVcc(cond,opd,ops) -> rule_cmov ops opd
  | CALL(dis,abs,op,slctr) -> rule_call dis abs op slctr
  | XOR(width,opd,ops) -> rule_xor ops opd
  | AND(width,opd,ops) -> rule_and ops opd
  | OR(width,opd,ops) -> rule_or ops opd
  | NEG(width,opd) -> rule_neg opd
  | NOT(width,opd) -> handle_boolop opd
  | SHL(width,opd,ops) -> rule_shl ops opd
  | SHLD(width,reg,regimm) -> rule_shld reg
  | SHR(width,opd,ops) -> rule_shr ops opd
  | SHRD(width,reg,regimm) -> rule_shrd reg
  | SAR(width,opd,ops) -> rule_sar ops opd
  | ROL(width,opd,ops) -> handle_boolop opd
  | ROR(width,opd,ops) -> handle_boolop opd
  | STOS(width) -> rule_stos ()
  | XCHG(width,opd,ops) -> rule_xchg ops opd
  | SETcc(cond,opd) -> rule_setcc opd
  | CDQ -> rule_cdq ()
  | BSWAP reg -> rule_bswap reg
  | BSR(opd,ops) -> handle_boolop opd
  | _ -> 
    if !report_unsupport then
      P.printf "Unsupported instruction at %x: %s\n" l (Instr.str_of_instr (pre,ins)); flush stdout;
    ()
  end
  
let handle_control_flows preds bb funCFA dbgTypeInfo eqcg track_worth struct_map =
  List.iter (fun tvnm ->
    let tv1_ndx = bb.bb_relAddr in
    let tv1,edge1 = mkTV tvnm tv1_ndx false in
    let tv1_tag = dbgtype_tag tv1 dbgTypeInfo in
    let tagged_tv_list = List.map (fun pred_bb ->
      let _,_,len = List.last pred_bb.bb_instrs in
      let end_addr = pred_bb.bb_relAddr + pred_bb.bb_size - len in
      let tv, edge = mkTV tvnm end_addr true in
      let tv_tag = dbgtype_tag tv dbgTypeInfo in
      tv, tv_tag, edge
    ) preds
    in
    eqcg_connect eqcg tv1 tv1_tag edge1 tagged_tv_list
  ) track_worth

let bb_eqcgGen bb funCFA dbgTypeInfo eqcg track_worth struct_map =
  let addrcount = ref bb.bb_relAddr in
  let l_instr_list = 
    List.map (fun (pre,ins,len) -> 
      let outaddr = !addrcount in
      addrcount := len + !addrcount;
      outaddr, (pre,ins,len)
    ) bb.bb_instrs 
  in
  List.iteri (fun ndx l_instr ->
    let crossbb = ((ndx + 1) = List.length l_instr_list) in
    instr_eqcgGen l_instr funCFA dbgTypeInfo eqcg track_worth crossbb struct_map
  ) l_instr_list

let func_eqcgGen asm_fun funCFA dbgTypeInfo eqcg struct_map =
  let consrv = false in
  
  let track_worth =
    if consrv then    
      consrv_worth_tracking funCFA asm_fun
    else 
      aggres_worth_tracking funCFA asm_fun
  in
  List.iter (fun bb ->
    let possible_preds = 
      List.filter_map (fun bb' ->
	if List.mem bb.bb_label (bb'.bb_inter_succs @ bb'.bb_succs) then
	  Some bb'
	else
	  None
      ) asm_fun.func_bbs
    in
    bb_eqcgGen bb funCFA dbgTypeInfo eqcg track_worth struct_map;
    handle_control_flows possible_preds bb funCFA dbgTypeInfo eqcg track_worth struct_map;
  ) asm_fun.func_bbs


(** Solver **)
let visited_eqcs = ref []

let rec propagate_top_fast eqcg start_eqc start_ectype =
  let next_eqcs =  
    List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) start_eqc.eqc_succs)
  in
  visited_eqcs := start_eqc :: !visited_eqcs;
  List.iter (fun (eqc,edge) ->
    if not (List.mem eqc !visited_eqcs) then (
      if List.is_empty eqc.eqc_dbgtype then (
	let processed_ectype =
	  if !report_top_trace then
	    List.map (fun sctype ->
	      match sctype with
	      | SC_top pretrace -> 
		SC_top ((start_eqc.eqc_name,edge) :: pretrace)
	      | _ -> sctype
	    ) start_ectype
	  else start_ectype
	in
	if not (sub_ectype processed_ectype eqc.eqc_fflowtype)
	then (
	  eqc.eqc_fflowtype <- processed_ectype @ eqc.eqc_fflowtype;
	  propagate_top_fast eqcg eqc processed_ectype
	)
      )
    ) else ()
  ) next_eqcs

let type_solve_full_wl struct_map eqcg =
  let direction = true in
  let verbose = false in
  let next eqc = 
    if direction then 
      List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) eqc.eqc_succs)
    else
      List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) eqc.eqc_preds)
  in
  let _, nodes = eqcg in
  let workq = Queue.create () in
  H.iter (fun repr eqc ->
    if not (List.is_empty eqc.eqc_dbgtype) then
      Queue.add eqc workq
    else if List.is_empty eqc.eqc_preds then (
      eqc.eqc_fflowtype <- [SC_top[]];
      visited_eqcs := [];
      propagate_top_fast eqcg eqc [SC_top[]]
      (*Queue.add eqc workq*)
    )    
    else ()
  ) nodes;
  while (not (Queue.is_empty workq)) do
    if verbose then
      (P.printf "TASKS remaining: %d\n" (Queue.length workq); flush stdout);
    let task_eqc = Queue.take workq in
    let prop_type = 
      if List.is_empty task_eqc.eqc_dbgtype then
	task_eqc.eqc_fflowtype
      else
	task_eqc.eqc_dbgtype
    in
    assert (not (List.is_empty prop_type));
    List.iter (fun (eqc,edge) ->
      if List.is_empty eqc.eqc_dbgtype then
	let processed_ectype = 
      	  try
	    reason_ectype prop_type [edge] struct_map 
	  with Failure errmsg ->
	    P.printf "%s" errmsg;
	    P.printf "at eqc: %s\n" eqc.eqc_name;
	    P.printf "aiming with edge %s at %s\n" 
	      (str_of_edge edge) (str_of_ectype prop_type);
	    flush stdout;
	    []
	in
	if verbose then (
	  P.printf "comparing types: %d : %d\n" 
	    (List.length processed_ectype)
	    (List.length eqc.eqc_fflowtype);
	  flush stdout
	);
	if sub_ectype processed_ectype eqc.eqc_fflowtype then
	  ()
	else (
	  eqc.eqc_fflowtype <- 
	    List.fold_left (fun rettype sctype -> 
	      if List.exists (eq_sctype sctype) rettype then rettype
	      else sctype :: rettype
	    ) eqc.eqc_fflowtype processed_ectype;
	  Queue.add eqc workq
	)
      else
	()
    ) (next task_eqc)
  done
