open Batteries
module P = Printf
module H = Hashtbl

open TypeEngineLib

let visited_eqcs = ref []

let rec propagate_top_fast eqcg start_eqc start_ectype =
  let next_eqcs =  
    List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) start_eqc.eqc_succs)
  in
  visited_eqcs := start_eqc :: !visited_eqcs;
  List.iter (fun (eqc,edge) ->
    if not (List.mem eqc !visited_eqcs) then (
      if List.is_empty eqc.eqc_dbgtype then (
	let processed_ectype =
	  if !report_top_trace then
	    List.map (fun sctype ->
	      match sctype with
	      | SC_top pretrace -> 
		SC_top ((start_eqc.eqc_name,edge) :: pretrace)
	      | _ -> sctype
	    ) start_ectype
	  else start_ectype
	in
	if not (sub_ectype processed_ectype eqc.eqc_fflowtype)
	then (
	  eqc.eqc_fflowtype <- processed_ectype @ eqc.eqc_fflowtype;
	  propagate_top_fast eqcg eqc processed_ectype
	)
      )
    ) else ()
  ) next_eqcs

let type_solve_full_wl struct_map eqcg =
  let direction = true in
  let verbose = false in
  let next eqc = 
    if direction then 
      List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) eqc.eqc_succs)
    else
      List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) eqc.eqc_preds)
  in
  let _, nodes = eqcg in
  let workq = Queue.create () in
  H.iter (fun repr eqc ->
    if not (List.is_empty eqc.eqc_dbgtype) then
      Queue.add eqc workq
    else if List.is_empty eqc.eqc_preds then (
      eqc.eqc_fflowtype <- [SC_top[]];
      visited_eqcs := [];
      propagate_top_fast eqcg eqc [SC_top[]]
      (*Queue.add eqc workq*)
    )    
    else ()
  ) nodes;
  while (not (Queue.is_empty workq)) do
    if verbose then
      (P.printf "TASKS remaining: %d\n" (Queue.length workq); flush stdout);
    let task_eqc = Queue.take workq in
    let prop_type = 
      if List.is_empty task_eqc.eqc_dbgtype then
	task_eqc.eqc_fflowtype
      else
	task_eqc.eqc_dbgtype
    in
    assert (not (List.is_empty prop_type));
    List.iter (fun (eqc,edge) ->
      if List.is_empty eqc.eqc_dbgtype then
	let processed_ectype = 
      	  try
	    reason_ectype prop_type [edge] struct_map 
	  with Failure errmsg ->
	    P.printf "%s" errmsg;
	    P.printf "at eqc: %s\n" eqc.eqc_name;
	    P.printf "aiming with edge %s at %s\n" 
	      (str_of_edge edge) (str_of_ectype prop_type);
	    flush stdout;
	    []
	in
	if verbose then (
	  P.printf "comparing types: %d : %d\n" 
	    (List.length processed_ectype)
	    (List.length eqc.eqc_fflowtype);
	  flush stdout
	);
	if sub_ectype processed_ectype eqc.eqc_fflowtype then
	  ()
	else (
	  eqc.eqc_fflowtype <- 
	    List.fold_left (fun rettype sctype -> 
	      if List.exists (eq_sctype sctype) rettype then rettype
	      else sctype :: rettype
	    ) eqc.eqc_fflowtype processed_ectype;
	  Queue.add eqc workq
	)
      else
	()
    ) (next task_eqc)
  done
