open Batteries
open Bigarray
open Bits
open X86Syntax
open X86Semantics
open X86_MACHINE
open X86_RTL
open Config
open Abbrev
open Elf

open BB
open Ibaux
open RTL_opt
open Printer

open TyInfoRetrieval

module P = Printf
module H = Hashtbl

let int_of_32string str =
  Util.int_of_int32 (
    Int64.to_int32 (
      MWord.of_string ("0x"^str)
    )
  )

type addrName = {
  disp: string;
  base: register option;
  index: (scale * register) option;
}

let mkADNM indisp inbase inindex =
  {disp = indisp; base = inbase; index = inindex}

type tvName = 
| TV_imm of string
| TV_reg of register
| TV_cfa of int
| TV_addr of addrName    

let str_of_scale scl =
  match scl with
  | Scale1 -> 1
  | Scale2 -> 2
  | Scale4 -> 4
  | Scale8 -> 8

let str_of_tvnm tvnm =
  match tvnm with
  | TV_imm imm -> 
    P.sprintf "$0x%s" imm
  | TV_reg reg ->
    str_of_reg reg
  | TV_cfa off ->
    P.sprintf "%d<CFA>" off
  | TV_addr addr ->
    begin match addr with
    | {disp = strv; base = None; index = None} ->
      P.sprintf "(%s)" strv
    | {disp = strv; base = Some reg; index = None} ->
      P.sprintf "%s(%s)"
	strv
	(str_of_reg reg)
    | {disp = strv; base = None ; index = Some (scl,reg)} ->
      P.sprintf "%s(,%s,%d)"
	strv
	(str_of_reg reg)
	(str_of_scale scl)
    | {disp = strv; base = Some regbase ; index = Some (scl,regindex)} ->
      P.sprintf "%s(%s,%s,%d)"
	strv
	(str_of_reg regbase)
	(str_of_reg regindex)
	(str_of_scale scl)
    end

let print_tvname tvnm =
  P.printf "%s" (str_of_tvnm tvnm)

type edge = int * string

let str_of_edge edge =
  P.sprintf "(%d:%s)" (fst edge) (snd edge)

let split_at_eq re =
  match re with
  | Coq_test_rtl_exp(s,Coq_eq_op,re1,re2) ->
    begin match re1 with
    | Coq_get_loc_rtl_exp(s,Coq_reg_loc reg) ->
      reg, re2
    | _ ->
      raise (Failure "Should be no other expressions other than get_loc")
    end
  | _ ->
    raise (Failure "Should be no other expressions other than test")

let reverse_cfa_expr re =
  let cfa_ps_reg = Coq_get_ps_reg_rtl_exp(Big.of_int 31, Big.of_int (-1)) in
  match re with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    begin match re1 with
    | Coq_get_loc_rtl_exp(s, Coq_reg_loc reg) ->
      begin match bvop with
      | Coq_add_op ->
       	reg, Coq_arith_rtl_exp(s,Coq_sub_op,cfa_ps_reg,re2)
      | Coq_sub_op ->
	reg, Coq_arith_rtl_exp(s,Coq_add_op,cfa_ps_reg,re2)
      | _ ->
	raise (Failure "Should be no other operators")
      end
    | _ -> 
      raise (Failure "Should not exist things other than registers")
    end
  | Coq_get_loc_rtl_exp(s,Coq_reg_loc reg) ->
    reg, cfa_ps_reg
  | _ ->
    P.printf "%s\n" (str_of_rtl_exp_less_size re); flush stdout;
    raise (Failure "Should not exist expressions other than arith")

let rtl_add_big re bigv =
  simp_re simpre_all_methods (Coq_arith_rtl_exp(Big.of_int 31, Coq_add_op, re, Coq_imm_rtl_exp(Big.of_int 31, bigv)))

let transform_cfa_expr re addr_name =
  match re with
  | Coq_arith_rtl_exp(_,bvop,re1,re2) ->
    begin match re1 with
    | Coq_get_ps_reg_rtl_exp(_,ps_reg) when Big.to_int ps_reg = (-1) ->
      begin match bvop with
      | Coq_add_op ->
	begin match re2 with
	| Coq_imm_rtl_exp (_,v) -> 
	  TV_cfa (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int v)))
	| _ -> 
	  raise Not_found
	(*TV_addr addr_name*)
	end
      | Coq_sub_op ->
	begin match re2 with
	| Coq_imm_rtl_exp (_,v) -> 
	  TV_cfa (-(Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int v))))
	| _ -> 
	  raise Not_found
	  (*TV_addr addr_name*)
	end
      | _ -> (*TV_addr addr_name*) raise Not_found
      end
    | _ -> (*TV_addr addr_name*) raise Not_found
    end
  | Coq_get_ps_reg_rtl_exp(_,ps_reg) when Big.to_int ps_reg = (-1) ->
    TV_cfa 0
  | _ -> (*TV_addr addr_name*) raise Not_found

let tvNameGen funCFA op l =
  match op with
  | Imm_op imm -> 
    (TV_imm (str_of_mword_flex (MWord.of_big_int imm))),(0,"0")
  | Reg_op reg -> (TV_reg reg), (0,"0")
  | Offset_op off ->
    (TV_addr {disp = str_of_mword_flex (MWord.of_big_int off); base = None; index = None}), (1,"0")
  | Address_op addr ->
    begin match addr with
    | {addrDisp = big_v; addrBase = None; addrIndex = None} ->
      let nm = mkADNM
	(str_of_mword_flex (MWord.of_big_int big_v)) 
	None None
      in
      (TV_addr nm), (1,"0")
    | {addrDisp = big_v; addrBase = Some reg; addrIndex = index_option} ->
      let _, l_cfa =
	try
	  List.find (fun (lopc,cfaexp) ->
	    lopc = l
	  ) funCFA
	with Not_found ->
	  P.printf "not found cfaexp at %x\n" l; flush stdout;
	  List.last funCFA
	  (*raise (Failure "in tvNameGen")*)
      in
      let cfaoff = List.map (fun (lvl,re) ->
	if lvl > 0 then split_at_eq re
	else 
	  reverse_cfa_expr re
      ) l_cfa 
      in
      begin 
	try 
	  let _, reg_in_cfa = List.find (fun (register, re) ->
	    register = reg
	  ) cfaoff
	  in
	  let op_cfa_expr = rtl_add_big reg_in_cfa big_v in
	  let nm = mkADNM 
	    (str_of_mword_flex (MWord.of_big_int big_v)) 
	    (Some reg) index_option
	  in
	  if index_option = None then
	    (transform_cfa_expr op_cfa_expr nm), (0,"0")
	  else
	    (transform_cfa_expr op_cfa_expr nm), (0,"some")
	with Not_found ->
	  let off_str = 
	    str_of_mword_flex (MWord.of_big_int big_v)
	  in
	  let off_int = 
	    Util.int_of_int32 
	      (Int64.to_int32 (MWord.of_big_int big_v))
	  in
	  if index_option = None then
	    if off_int >= (Int32.to_int (Int32.of_string "0x8048000")) then
	      let nm = mkADNM off_str None None in
	      (TV_addr nm), (1,"some")
	    else
	      (TV_reg reg), (1,off_str)
	  else
	    if off_int >= (Int32.to_int (Int32.of_string "0x8048000")) then
	      let nm = mkADNM off_str None None in
	      (TV_addr nm), (1,"some")
	    else
	      (TV_reg reg), (1,"some")
      end
    | _ ->
      let nm = mkADNM 
	(str_of_mword_flex (MWord.of_big_int addr.addrDisp)) 
        None None
      in
      (TV_addr nm), (1,"some")
    end
      

type range = 
| R_all
| R_range of int * int

let str_of_range rng =
  match rng with
  | R_all -> "all"
  | R_range(lo,hi) ->
    P.sprintf "(%x,%x)" lo hi

let in_range addr range =
  match range with
  | R_all -> true
  | R_range(lo,hi) -> 
    (* WARNING: range may overlap from debugging information *)
    lo <= addr && addr < hi

let in_range_for_breg addr range =
  match range with
  | R_all -> true
  | R_range(lo,hi) ->
    lo <= addr && addr <= hi

type infer_t =
(** top: over-approximation with a reason**)
| IT_top of int
(** tblent: distinguished jmptbl entry **)
| IT_tblent
| IT_ptr of infer_t
(** off: a C type with an offset **)
| IT_off of c_type * int
(** elem: an element of a C type **)
| IT_elem of c_type * int

let str_of_reason reason_num =
  match reason_num with
  | 0 -> "Unknown"
  | 1 -> "Top-source"
  | 2 -> "Infer-fail: deref"
  | 3 -> "Infer-fail: trans"
  | 4 -> "Infer-fail: cite"
  | _ ->
    (** Should be impossible **)
    raise (Failure "Unsuppoted reason number")

let rec str_of_it it =
  match it with
  | IT_top reason_num ->
    P.sprintf "<T>:%s" (str_of_reason reason_num)
  | IT_tblent ->
    P.sprintf "Table-Entry"
  | IT_ptr it' ->
    P.sprintf "PTR(%s)" (str_of_it it')
  | IT_off (ctype,off) ->
    P.sprintf "OFF[%x](%s)" off (str_of_c_type ctype)
  | IT_elem (ctype,off) ->
    P.sprintf "ELEM[%x](%s)" off (str_of_c_type ctype)

let str_of_itlist itlist =
  List.fold_left (fun str it ->
    P.sprintf "%s%s " str (str_of_it it)
  ) "" itlist

let rec eq_ct ct1 ct2 =
  try
    ct1 = ct2
  with _ ->
    let errmsg =
      P.sprintf "EQProblem: Comparing two C types"
    in
    P.printf "%s" errmsg;
    flush stdout;
    true

let calc_array_bound dimlist =
  List.fold_right (fun dim mult ->
    dim * mult
  ) dimlist 1

let rec size_of_ctype typedef_map struct_map ctype =
  match ctype with
  | C_base (size,_) -> size
  | C_pointer (size,_) -> size
  | C_enumeration (size,_,_) -> size
  | C_union (size,_,_) -> size
  | C_array (ct, dimlist) ->
    let ctsize = size_of_ctype typedef_map struct_map ct in
    let bound = calc_array_bound dimlist in
    ctsize * bound
  | C_structure (size,nm,_) ->
    if size <> 0 then size
    else if H.mem struct_map nm then
      size_of_ctype typedef_map struct_map (H.find struct_map nm)
    else
      assert false
  | C_typedef (defnm, ct)->
    if H.mem typedef_map defnm then
      size_of_ctype typedef_map struct_map 
	(H.find typedef_map defnm)
    else
      size_of_ctype typedef_map struct_map ct
  | C_const ct | C_volatile ct ->
    size_of_ctype typedef_map struct_map ct
  | _ ->
    let errmsg =
      P.sprintf "No size attribute for type:%s" (str_of_c_type ctype)
    in
    raise (Failure errmsg)

let find_closest_field fdlist off =
  List.last (
    List.filter_map (fun (memoff,ft,fn) ->
      if off - memoff >=0 then
	Some (memoff,ft)
      else
	None
    ) fdlist
  )

let rec is_ctype_void ctype =
  match ctype with
  | C_void -> true
  | C_union _ -> false
  | C_typedef (_,ct) | C_const ct | C_volatile ct ->
    is_ctype_void ct
  | _ -> false

let rec is_ctype_base ctype name =
  match ctype with
  | C_base (size,basename) -> basename = name
  | C_typedef (_,ct) | C_const ct | C_volatile ct ->
    is_ctype_base ct name
  | _ -> false

let rec is_ctype_enum ctype name =
  match ctype with
  | C_enumeration (size,nm,etorlist) ->
    name = nm
  | C_typedef (_,ct) | C_const ct | C_volatile ct ->
    is_ctype_enum ct name
  | _ -> false

let rec is_ctype_top ctype =
  match ctype with
  | C_void -> true
  | C_union _ -> true
  | C_typedef (_,ct) | C_const ct | C_volatile ct ->
    is_ctype_top ct
  | _ -> false

let rec is_ctype_ptr ctype =
  match ctype with
  | C_pointer _ -> true
  | C_typedef (_,ct) | C_const ct | C_volatile ct ->
    is_ctype_ptr ct
  | _ -> false

let rec is_ctype_subroutine ctype =
  match ctype with
  | C_subroutine _ -> true
  | C_typedef (_,ct) | C_const ct | C_volatile ct ->
    is_ctype_subroutine ct
  | _ -> false

let rec mymod fst snd =
  if fst >= 0 && snd > 0 then
    fst mod snd
  else if fst < 0 && snd > 0 then
    mymod (fst+snd) snd
  else
    assert false

let rec find_ctype_off typedef_map struct_map init_ctype init_off =
  let rec aux ctype off = 
    if off < 0 then (
      let off = 
	mymod off (size_of_ctype typedef_map struct_map ctype)
      in
      aux ctype off
    )
    else
      match ctype with
      | C_array (ct',dimlist) ->
	let ctsize = size_of_ctype typedef_map struct_map ct' in
	let bound = calc_array_bound dimlist in
	if off >= (ctsize * bound) then
	  let errmsg = 
	    P.sprintf "(%d) out of bound in array\n%s"
	      off (str_of_c_type ctype)
	  in
	  raise (Failure errmsg)
	else 
	  let inner_off = off mod ctsize in
	  aux ct' inner_off
      | C_structure (size,nm,fdlist) ->
	if (not (List.is_empty fdlist)) && size <> 0 then 
	  if off >= size then
	    let errmsg = 
	      P.sprintf "(%d) out of bound in structure\n%s"
		off (str_of_c_type ctype)
	    in
	    P.printf "Warning: %s\n" errmsg;
	    flush stdout;
	    aux ctype (off mod size)
	  else
	    let inner_memoff,inner_ct = 
	      find_closest_field fdlist off
	    in
	    aux inner_ct (off-inner_memoff)
	else if H.mem struct_map nm then
	  aux (H.find struct_map nm) off
	else
	  assert false
      | C_union (size,nm,fdlist) ->
	if off < size then
	  ctype
	else
	  let errmsg = P.sprintf "(%d) Accessing UNION out of bound\n%s" 
	    off (str_of_c_type ctype)
	  in
	  P.printf "Warning: %s\n" errmsg;
	  flush stdout;
	  ctype
      | C_typedef (defnm,ct') ->
	if H.mem typedef_map defnm then (
	  aux (H.find typedef_map defnm) off
	)
	else
	  aux ct' off
      | C_const ct' ->
	C_const (aux ct' off)
      | C_volatile ct' ->
	C_volatile (aux ct' off)
      | C_pointer (size,ct') ->
	if off = 0 then
	  ctype
	else
	  raise (Failure "Non-zero offset for pointer in find_ctype_off")
      | C_void -> C_void
      | C_base (size,basename) ->
	if off = 0 then
	  ctype
	else if String.exists basename "long" then
	  ctype
	else
	  let errmsg = 
	    P.sprintf "(%d) mismatch for type\n%s"
	      off (str_of_c_type ctype)
	  in
	  raise (Failure errmsg);	  
      | _ -> 
	if off = 0 then
	  ctype
	else 
	  let errmsg = 
	    P.sprintf "(%d) mismatch for type\n%s"
	      off (str_of_c_type ctype)
	  in
	  raise (Failure errmsg);
  in
  match init_ctype with
  | C_array (ct',dimlist) ->
    aux init_ctype init_off
  | C_structure (size,nm,fdlist) ->
    aux init_ctype init_off
  | C_typedef (defnm,ct') ->
    if H.mem typedef_map defnm then (
      aux (H.find typedef_map defnm) init_off
    )
    else
      find_ctype_off typedef_map struct_map ct' init_off
  | C_const ct' ->
    find_ctype_off typedef_map struct_map ct' init_off
  | C_volatile ct' ->
    find_ctype_off typedef_map struct_map ct' init_off
  | C_void -> C_void
  | _ -> 
    if init_off < 0 then 
      let off = 
	mymod init_off (size_of_ctype typedef_map struct_map init_ctype)
      in
      find_ctype_off typedef_map struct_map init_ctype off
    else if init_off = 0 then
      init_ctype
    else
      let ctsize = size_of_ctype typedef_map struct_map init_ctype in
      let inner_off = init_off mod ctsize in
      if inner_off = 0 then
	init_ctype
      else
	init_ctype

let rec find_next_target_ctype ctype =
  match ctype with
  | C_pointer (_,ct) ->
    Some ct
  | C_typedef (_,ct) | C_const ct | C_volatile ct ->
    find_next_target_ctype ct
  | _ ->
    None

let rec interp_elem_memtype typedef_map struct_map init_ctype init_off =
  try
    Some (find_ctype_off typedef_map struct_map init_ctype init_off)
  with Failure errmsg ->
    P.printf "Abnormal memtype interpretation: %s\n" errmsg;
    flush stdout;
    None

let rec interp_elem typedef_map struct_map init_ctype init_off =
  let rec is_memchunk ctype =
    match ctype with
    | C_array _ -> true
    | C_structure _ -> true
    | C_const ct | C_typedef (_,ct) | C_volatile ct ->
      is_memchunk ct
    | _ -> false
  in
  try
    match init_ctype with
    | C_void
    | C_base _ -> Some init_ctype
    | C_const ctype | C_volatile ctype ->
      interp_elem typedef_map struct_map ctype init_off
    | C_subroutine _ ->
      if init_off <> 0 then None
      else Some init_ctype
    | C_typedef (defnm,ctype) ->
      if H.mem typedef_map defnm then
	let struct_type = H.find typedef_map defnm in
	interp_elem_memtype typedef_map struct_map struct_type init_off
      else
	interp_elem typedef_map struct_map ctype init_off
    | C_array (ctype,dimlist) ->
      interp_elem_memtype typedef_map struct_map init_ctype init_off
    | C_structure (size,nm,fdlist) ->
      if H.mem struct_map nm then
	interp_elem_memtype typedef_map struct_map (H.find struct_map nm) init_off
      else if not (List.is_empty fdlist) then
	interp_elem_memtype typedef_map struct_map init_ctype init_off
      else (
	P.printf "Unable to handle structure:\n%s\n" (str_of_c_type init_ctype);
	P.printf "with offset %d\n" init_off;
	flush stdout;
	Some C_void
      (*assert false*)
      )
    | C_enumeration (size,nm,etorlist) -> Some init_ctype
    | C_union (size,nm,fdlist) -> Some init_ctype
    | C_unspecified_param -> Some init_ctype
    | C_wrong_type errmsg ->
      let newerrmsg = P.sprintf "Found wrong type: %s" errmsg in
      raise (Failure newerrmsg)
    | C_pointer (size,ctype) ->
      if is_memchunk ctype then
	let interp_elem_type = interp_elem typedef_map struct_map ctype init_off in
	if interp_elem_type = None then
	  None
	else
	  Some (C_pointer (size, Option.get interp_elem_type))
      else
	Some init_ctype
  with Failure errmsg ->
    P.printf "Abnormal element type interpretation: %s\n" errmsg;
    flush stdout;
    None

let rec eq_it typedef_map struct_map it1 it2 =
  match it1, it2 with
  | (IT_top n1), (IT_top n2) -> 
    n1 = n2
  | IT_tblent, IT_tblent -> true
  | IT_ptr it1', IT_ptr it2' -> 
    eq_it typedef_map struct_map it1' it2'
  | (IT_off (ct,off)), _ -> 
    assert false
  | _, (IT_off (ct,off)) -> 
    assert false
  | IT_elem (ct1,off1), IT_elem (ct2,off2) ->
    if off1 = off2 && eq_ct ct1 ct2 then
      true
    else false
(*
      let real_ct1_option = interp_elem typedef_map struct_map ct1 off1 in
      let real_ct2_option = interp_elem typedef_map struct_map ct2 off2 in
      if real_ct1_option = None || real_ct2_option = None then
	true
      else 
	eq_ct (Option.get real_ct1_option) (Option.get real_ct2_option)
*)
  | _,_ -> false

let rec sub_it typedef_map struct_map it1 it2 =
  match it1, it2 with
  | (IT_top n1), (IT_top n2) -> 
    n1 = n2
  | IT_tblent, IT_tblent -> true
  | IT_ptr it1', IT_ptr it2' -> 
    sub_it typedef_map struct_map it1' it2'
  | (IT_off (ct,off)), _ -> 
    assert false
  | _, (IT_off (ct,off)) -> 
    assert false
  | IT_elem (ct1,off1), IT_elem (ct2,off2) ->
    if eq_ct ct1 ct2 then
      let distance = abs (off1-off2) in
      try
	if (distance mod (size_of_ctype typedef_map struct_map ct1)) = 0 then true
	else false
      with _ -> true
    else false
  | _,_ -> false
    
let eq_itlist typedef_map struct_map itlist1 itlist2 =
  if List.length itlist1 <> List.length itlist2 then
    false
  else
    List.fold_right (fun it1 b ->
      b && List.exists (fun it2 -> eq_it typedef_map struct_map it1 it2) itlist2
    ) itlist1 true

let sub_itlist typedef_map struct_map itlist1 itlist2 =
  List.for_all (fun it1 ->
    List.exists (fun it2 -> sub_it typedef_map struct_map it1 it2) itlist2
  ) itlist1

let size_of_it typedef_map struct_map itype =
  match itype with
  | IT_top _ -> 4
  | IT_tblent -> 4
  | IT_ptr _ -> 4
  | IT_off (ctype, _) -> assert false
  | IT_elem (ctype, off) ->
    if is_ctype_ptr ctype then
      size_of_ctype typedef_map struct_map ctype
    else
      let elem_ctype = find_ctype_off typedef_map struct_map ctype off in
      size_of_ctype typedef_map struct_map elem_ctype

type interp_type_info = (tvName * range * infer_t list) list

let dump_interp_tyinfo interp_tyinfo =
  List.iter (fun (tvnm,rng,ctlist) ->
    P.printf "%s in %s has:\n" (str_of_tvnm tvnm) (str_of_range rng);
    List.iter (fun ct ->
      P.printf "%s\n" (str_of_c_type ct)
    ) ctlist
  ) interp_tyinfo

let interp_locexprs frame_base funCFA locexprs range =
  if List.mem "DW_OP_stack_value" locexprs then []
  else if List.length locexprs <> 1 then [] (* do not support currently *)
  else 
    let locexpr = List.hd locexprs in
    if String.exists locexpr "DW_OP_reg" then
      let op = String.strip locexpr in
      match op with
      | "DW_OP_reg0" -> [TV_reg EAX]
      | "DW_OP_reg1" -> [TV_reg ECX]
      | "DW_OP_reg2" -> [TV_reg EDX]
      | "DW_OP_reg3" -> [TV_reg EBX]
      | "DW_OP_reg4" -> [TV_reg ESP]
      | "DW_OP_reg5" -> [TV_reg EBP]
      | "DW_OP_reg6" -> [TV_reg ESI]
      | "DW_OP_reg7" -> [TV_reg EDI]
      | _ -> []
    else if String.exists locexpr "DW_OP_fbreg" then
      let op,off_str = String.split locexpr " " in
      let off_big_v = Big.of_int (int_of_string off_str) in
      let change_frame_base fbreg funCFA =
	let lvre_list = List.flatten ( 
	  List.filter_map (fun (relAddr,lvre_list) ->
	    if in_range relAddr range then
	      Some lvre_list
	    else None
	  ) funCFA
	)
	in
	let cfa_name_list = List.filter_map (fun (lv,re) -> 
	  let reg, reg_in_cfa = if lv > 0 then split_at_eq re
	    else reverse_cfa_expr re
	  in
	  if reg = fbreg then
	    let op_cfa_expr = rtl_add_big reg_in_cfa off_big_v in
	    let addr_name = mkADNM off_str (Some fbreg) None in
	    Some (transform_cfa_expr op_cfa_expr addr_name)
	  else
	    None
	) lvre_list
	in
	let unique_name_list = List.unique cfa_name_list in
	if List.length unique_name_list = 1 then
	  [List.hd unique_name_list]
	else
	  let errmsg = P.sprintf "%s frame base is not uniformed" (str_of_reg fbreg) in 
	  raise (Failure errmsg)
      in
      match frame_base with
      | "DW_OP_call_frame_cfa" -> 
	let off = int_of_string off_str in
	[TV_cfa off]
      (** TODO: check correctness: interp different frame base into CFA **)
      (** (relAddr * ((level * rtl_exp) list)) list **)
      | "DW_OP_breg0" -> 
	(*let addrname = mkADNM off_str (Some EAX) None in*)
	change_frame_base EAX funCFA
      | "DW_OP_breg1" -> 
	change_frame_base ECX funCFA
      | "DW_OP_breg2" -> 
	change_frame_base EDX funCFA
      | "DW_OP_breg3" -> 
	change_frame_base EBX funCFA
      | "DW_OP_breg4" -> 
	change_frame_base ESP funCFA
      | "DW_OP_breg5" ->
	change_frame_base EBP funCFA
      | "DW_OP_breg6" ->
	change_frame_base ESI funCFA
      | "DW_OP_breg7" -> 
	change_frame_base EDI funCFA
      | _ -> []
    else if String.exists locexpr "DW_OP_breg" then
      (** a lot of useful information is hidden in this category **)
      let op,off_str = String.split locexpr " " in
      match op with
      | "DW_OP_breg0" -> 
      let addrname = mkADNM off_str (Some EAX) None in
      [TV_addr addrname]
      | "DW_OP_breg1" -> 
      let addrname = mkADNM off_str (Some ECX) None in
      [TV_addr addrname]
      | "DW_OP_breg2" -> 
      let addrname = mkADNM off_str (Some EDX) None in
      [TV_addr addrname]
      | "DW_OP_breg3" -> 
      let addrname = mkADNM off_str (Some EBX) None in
      [TV_addr addrname]
      | "DW_OP_breg4" -> 
      let addrname = mkADNM off_str (Some ESP) None in
      [TV_addr addrname]
      | "DW_OP_breg5" ->
      let addrname = mkADNM off_str (Some EBP) None in
      [TV_addr addrname]
      | "DW_OP_breg6" ->
      let addrname = mkADNM off_str (Some ESI) None in
      [TV_addr addrname]
      | "DW_OP_breg7" -> 
      let addrname = mkADNM off_str (Some EDI) None in
      [TV_addr addrname]
      | _ -> []
    else if String.exists locexpr "DW_OP_addr" then
      let op,addr = String.split locexpr " " in
      let abs_addr_str = str_of_mword_flex (MWord.of_string addr) in
      let addrname = mkADNM abs_addr_str None None in
      [TV_addr addrname]
    else 
      []

let process_raw_ti framebase funCFA raw_ti = 
  let ti = 
    List.flatten (
      List.map (fun (ctype,sinfo) ->
	List.map (fun (lostr, histr, locexprs) ->
	  locexprs, (lostr,histr), ctype
	) sinfo
      ) raw_ti
    )
  in
  let rec is_memchunk ctype =
    match ctype with
    | C_array _ -> true
    | C_structure _ -> true
    | C_const ct | C_typedef (_,ct) | C_volatile ct ->
      is_memchunk ct
    | _ -> false
  in
  let type_of_addr ctype =
    if is_memchunk ctype then
      ctype
    else
      (** we can only handle 32bit programs **)
      C_pointer (4,ctype)
  in
  let interp_ti = 
    List.flatten (
      List.map (fun (locexprs,rng,ctype) ->
	let lopc_int64 = Int64.of_string (fst rng) in
	let hipc_int64 = Int64.of_string (snd rng) in
	let lopc = Int64.to_int lopc_int64 in
	let hipc = Int64.to_int hipc_int64 in
	let range = 
	  if lopc = 0 && hipc = (-1) then R_all
	  else
	    let lopc_rel = lopc - (int_of_string "0x8048000") in
	    let hipc_rel = hipc - (int_of_string "0x8048000") in
	    R_range (lopc_rel, hipc_rel)
	in
	let locs = interp_locexprs framebase funCFA locexprs range in
	List.map (fun loc ->
	  match loc with
	  | TV_imm _ ->
	    (** Impossible **)
	    assert false
	  | TV_addr adnm ->
	    begin match adnm with
	    | {disp = offstr; base = Some reg; index = None} ->
	      loc, range, [ctype]
	    | {disp = addrstr; base = None; index = None} ->
	      loc, range, [ctype]
	    | _ ->
	      assert false
	    end
	  | TV_cfa _ ->
	    loc, range, [ctype]
	  | TV_reg _ ->
	    loc, range, [ctype]
	) locs
      ) ti
    )
  in
  interp_ti

let process_raw_gti raw_gti = 
  process_raw_ti "" [] raw_gti

let process_raw_pti framebase funCFA raw_pti =
  process_raw_ti framebase funCFA raw_pti

let map_libfun_addr raw_funsig_info elf start =
  let libfuns = collect_library_functions elf start in
  List.map (fun (sigtype, ident) ->
    try 
      let lopc_rel, _ = 
	List.find (fun (lopc,libfunnm) -> libfunnm = ident) libfuns
      in
      let lopc_abs = MWord.(+%) (MWord.of_int lopc_rel) start in
      (sigtype, "0x" ^ (str_of_mword_flex lopc_abs))
    with Not_found ->
      (sigtype,ident)
  ) raw_funsig_info
      

let cons_funsig_map funsig_info =
  let funsig_map = H.create 32 in
  P.printf "Construct general funsig_map...\n";
  List.iter (fun (sigtype, lopcstr) ->
    let abs_addr_str = str_of_mword_flex (MWord.of_string lopcstr) in
    if H.mem funsig_map abs_addr_str then
      ()
    else
      H.add funsig_map abs_addr_str (C_pointer (4,sigtype))
  ) funsig_info;
  funsig_map

let cons_taken_funsig_map elf funsig_info start =
  let funsig_map = H.create 32 in
  let all_functions = collect_all_functions elf start in
  let icTargets, _ = collect_indirect_targets elf all_functions in
  P.printf "Construct taken funsig_map...\n";
  List.iter (fun (sigtype, lopcstr) ->
    let abs_addr_str = str_of_mword_flex (MWord.of_string lopcstr) in
    if H.mem funsig_map abs_addr_str then
      ()
    else 
      if List.exists (fun (loc,lbl) -> 
	abs_addr_str = (str_of_mword_flex (MWord.(+%) (MWord.of_int loc) start))
      ) icTargets 
      then
	H.add funsig_map abs_addr_str (C_pointer (4,sigtype))
      else
	()
  ) funsig_info;
  flush stdout;
  funsig_map

let dump_funsig_map funsig_map =
  H.iter (fun lopcstr sigtypeptr ->
    P.printf "%s -->\n%s\n" lopcstr (str_of_c_type sigtypeptr)
  ) funsig_map

(*
  let upper_bound_ti typedef_map struct_map interp_ti =
  List.flatten (
  List.map (fun (tvnm,rng,itlist) ->
  List.map (fun it ->
  match tvnm with
  | TV_imm _ ->
  assert false
  | TV_reg _ ->
  (tvnm,tvnm), rng, it
  | TV_cfa off ->
  let new_off = 
  off - (size_of_it typedef_map struct_map it)
  in
  (tvnm, (TV_cfa new_off)), rng, it
  | TV_addr adnm ->
  assert (adnm.base = None && adnm.index = None);
  let off = int_of_32string adnm.disp in
  let new_offstr = P.sprintf "%x" (off + (size_of_it typedef_map struct_map it)) in
  (tvnm, (TV_addr (mkADNM new_offstr None None))), rng,it	
  ) itlist
  ) interp_ti
  )

  let add_tvnm_to_tvnm_map typedef_map struct_map tvnm tvnm_map it = 
  let rec gen_field_offset typedef_map struct_map ct init_off =
  match ct with
  | C_array (ct',dimlist) ->
  let ctsize = size_of_ctype typedef_map struct_map ct' in
  let bound = calc_array_bound dimlist in
  List.flatten (List.init bound (fun i ->
  gen_field_offset typedef_map struct_map ct' (init_off+i*ctsize)
  ))
  | C_structure (size,nm,fdlist) ->
  if (not (List.is_empty fdlist)) && size <> 0 then (
  List.flatten ( List.map (fun (memoff,ft,fn) ->
  gen_field_offset typedef_map struct_map ft (init_off+memoff)
  ) fdlist )
  )
  else if H.mem struct_map nm then
  gen_field_offset typedef_map struct_map (H.find struct_map nm) init_off
  else
  assert false
  | C_typedef (defnm,ct') ->
  if H.mem typedef_map defnm then (
  gen_field_offset typedef_map struct_map (H.find typedef_map defnm) init_off
  )
  else (
  gen_field_offset typedef_map struct_map ct' init_off
  )
  | C_const ct' | C_volatile ct' ->
  gen_field_offset typedef_map struct_map ct' init_off
  | _ -> [init_off]
  in
  match tvnm with
  | TV_imm _ -> assert false
  | TV_reg _ -> ()
  | TV_cfa off ->
  begin match it with
  | IT_ctype ctype ->
  let fields = gen_field_offset typedef_map struct_map ctype 0 in
  List.iter (fun fdoff ->
  let newtvnm = TV_cfa (off - fdoff) in
  if H.mem tvnm_map newtvnm then
  let new_target = tvnm :: (H.find tvnm_map newtvnm) in
  H.replace tvnm_map newtvnm new_target
  else
  H.add tvnm_map newtvnm [tvnm]
  ) fields
  | _ -> assert false
  end
  | TV_addr adnm ->
  begin match it with
  | IT_ctype ctype ->
  let fields = gen_field_offset typedef_map struct_map ctype 0 in
  List.iter (fun fdoff ->
  let addr = int_of_32string adnm.disp in
  let newtvnm = TV_addr (mkADNM (P.sprintf "%x" (addr + fdoff)) None None) in
  if H.mem tvnm_map newtvnm then
  let new_target = tvnm :: (H.find tvnm_map newtvnm) in
  H.replace tvnm_map newtvnm new_target
  else
  H.add tvnm_map newtvnm [tvnm]
  ) fields	
  | IT_ptr _ -> 
  if H.mem tvnm_map tvnm then
  let new_target = tvnm :: (H.find tvnm_map tvnm) in
  H.replace tvnm_map tvnm new_target
  else
  H.add tvnm_map tvnm [tvnm]
  | _ -> assert false
  end

  let add_rng_to_rng_map asm_fun rng rngmap =
  let all_indices = 
  List.flatten (
  List.map (fun bb ->
  let addrcount = ref bb.bb_relAddr in 
  List.map (fun (pre,ins,len) ->
  let outaddr = !addrcount in
  addrcount := len + !addrcount;
  outaddr
  ) bb.bb_instrs
  ) asm_fun.func_bbs
  )
  in
  let covered_indices =
  match rng with
  | R_all -> all_indices
  | R_range (lopc,hipc) ->
  List.filter (fun index -> 
  lopc <= index && index < hipc
  ) all_indices
  in
  List.iter (fun index ->
  if H.mem rngmap index then
  let new_rng = rng :: (H.find rngmap index) in
  H.replace rngmap index new_rng
  else
  H.add rngmap index [rng]
  ) covered_indices

  let cons_ubti_map asm_fun typedef_map struct_map interp_ti =
  let tvnm_map = H.create 32 in
  let rng_map = H.create 32 in
  let itype_map = H.create 32 in
  List.iter (fun (tvnm, rng, itlist) ->
  List.iter (fun it ->
  add_tvnm_to_tvnm_map typedef_map struct_map tvnm tvnm_map it;
  add_rng_to_rng_map asm_fun rng rng_map;
  if H.mem itype_map (tvnm,rng) then
  let new_itlist = it :: (H.find itype_map (tvnm,rng)) in
  H.replace itype_map (tvnm,rng) new_itlist
  else
  H.add itype_map (tvnm,rng) [it]
  ) itlist
  ) interp_ti;
  tvnm_map, rng_map, itype_map
*)
let add_to_effi_map typedef_map struct_map effi_map tvnm rng ctype =
  let rec gen_field_offset ct init_off =
    match ct with
    | C_array (ct',dimlist) ->
      let ctsize = size_of_ctype typedef_map struct_map ct' in
      let bound = calc_array_bound dimlist in
      List.flatten (List.init bound (fun i ->
	gen_field_offset ct' (init_off+i*ctsize)
      ))
    | C_structure (size,nm,fdlist) ->
      if (not (List.is_empty fdlist)) && size <> 0 then (
	List.flatten ( List.map (fun (memoff,ft,fn) ->
	  gen_field_offset ft (init_off+memoff)
	) fdlist )
      )
      else if H.mem struct_map nm then
	gen_field_offset (H.find struct_map nm) init_off
      else
	assert false
    | C_typedef (defnm,ct') ->
      if H.mem typedef_map defnm then (
	gen_field_offset (H.find typedef_map defnm) init_off
      )
      else (
	gen_field_offset ct' init_off
      )
    | C_const ct' | C_volatile ct' ->
      gen_field_offset ct' init_off
    | _ -> [init_off]
  in
  match tvnm with
  | TV_imm _ -> assert false
  | TV_reg _ -> 
    if H.mem effi_map tvnm then
      H.replace effi_map tvnm ((rng,IT_elem (ctype, 0)) :: (H.find effi_map tvnm))
    else
      H.add effi_map tvnm [rng, IT_elem (ctype, 0)]
  | TV_cfa off ->
    let fdoffs = gen_field_offset ctype 0 in
    List.iter (fun fdoff ->
      let newtvnm = TV_cfa (off + fdoff) in
      let newit =
	IT_elem (ctype,fdoff)
      in
      if H.mem effi_map newtvnm then
	let new_infolist =
	  (rng, newit) :: (H.find effi_map newtvnm)
	in
	H.replace effi_map newtvnm new_infolist
      else
	H.add effi_map newtvnm [rng,newit]
    ) fdoffs
  | TV_addr adnm ->
    begin match adnm with
    | {disp=offstr; base = Some reg; index = None} ->
      if H.mem effi_map tvnm then
	let new_infolist =
	  (rng, IT_elem (ctype, 0)) :: (H.find effi_map tvnm)
	in
	H.replace effi_map tvnm new_infolist
      else
	H.add effi_map tvnm [rng, IT_elem (ctype, 0)]
    | _ ->
      let addr = int_of_32string adnm.disp in
      let fdoffs = gen_field_offset ctype 0 in
      List.iter (fun fdoff ->
	let newtvnm = TV_addr 
	  (mkADNM (P.sprintf "%x" (addr + fdoff)) None None) 
	in
	let newit =
	  IT_elem ((C_pointer (4,ctype)), fdoff)
	in
	if H.mem effi_map newtvnm then
	  let new_infolist = 
	    (rng, newit) :: (H.find effi_map newtvnm)
	  in
	  H.replace effi_map newtvnm new_infolist
	else
	  H.add effi_map newtvnm [rng, newit]
      ) fdoffs
    end

let cons_effi_map typedef_map struct_map interp_ti =
  let effi_map = H.create 32 in
  List.iter (fun (tvnm,rng,ctlist) ->
    List.iter (fun ct ->
      add_to_effi_map typedef_map struct_map effi_map
	tvnm rng ct
    ) ctlist
  ) interp_ti;
  effi_map

let check_effi_map effi_map =
  H.iter (fun tvnm infolist ->
    List.iter (fun (rng,it) ->
      match it with
      | IT_off _ -> assert false
      | IT_elem (ctype,off) ->
	()
      | _ ->
	let errmsg = 
	  P.sprintf "Abnormal effi_map entry:\n%s:\n%s\n" 
	    (str_of_tvnm tvnm) (str_of_it it) 
	in
	raise (Failure errmsg)
    ) infolist
  ) effi_map

let cons_jmptbl_map elf start =
  let all_functions = Ibaux.collect_all_functions elf start in
  let _, ijT = 
    Ibaux.collect_indirect_targets elf all_functions 
  in
  let ijT_abs = List.map (fun addr_rel ->
    MWord.(+%) (MWord.of_int addr_rel) start
  ) ijT
  in
(*
  List.iter (fun addr -> P.printf "taken ijT: %s\n" (str_of_mword_flex addr)) ijT_abs;
  flush stdout;
*)
  let rodata_shdr = 
    List.find (fun shdr -> 
      shdr.sh_name = ".rodata"
    ) elf.shdr_tab 
  in
  let rodata_start = rodata_shdr.sh_offset in
  let rodata_size = rodata_shdr.sh_size in
  let rodata = file_read_region elf.fname
    (MWord.to_int rodata_start)
    (MWord.to_int rodata_size)
  in
  let data_shdr =
    List.find (fun shdr ->
      shdr.sh_name = ".data"
    ) elf.shdr_tab
  in
  let data_start = data_shdr.sh_offset in
  let data_size = data_shdr.sh_size in
  let data = file_read_region elf.fname
    (MWord.to_int data_start)
    (MWord.to_int data_size)
  in
  let entries_in_data = 
    List.filter_map (fun loc ->
      let abs_addr = form_abs_addr data loc in
      if List.exists (fun ijt_abs ->
	Int64.equal abs_addr ijt_abs) ijT_abs then
	let location = 
	  str_of_mword_flex (
	    MWord.(+%) 
	      (MWord.(+%) (MWord.of_int loc) data_start)
	      start
	  )
	in
	Some location
      else None
    ) (List.init ((MWord.to_int data_size)-3) (fun i -> i))
  in
  let entries_in_rodata =
    List.filter_map (fun loc ->
      let abs_addr = form_abs_addr rodata loc in
      if List.exists (fun ijt_abs ->
	Int64.equal abs_addr ijt_abs) ijT_abs then
	let location = 
	  str_of_mword_flex (
	    MWord.(+%) 
	      (MWord.(+%) (MWord.of_int loc) rodata_start)
	      start
	  )
	in
	Some location
      else None
    ) (List.init ((MWord.to_int rodata_size)-3) (fun i -> i))
  in    
  entries_in_data @ entries_in_rodata 

let dump_effi_map effi_map =
  H.iter (fun tvnm infolist ->
    P.printf "%s -->\n" (str_of_tvnm tvnm);
    List.iter (fun (rng,it) ->
      P.printf "%s:\n%s;;\n" (str_of_range rng) (str_of_it it)
    ) infolist
  ) effi_map

let dump_ub_interp_ti ub_interp_ti =
  List.iter (fun (tvnmrng, rng, it) ->
    P.printf "(%s ~ %s):\n" (str_of_tvnm (fst tvnmrng)) (str_of_tvnm (snd tvnmrng));
    P.printf "%s\n" (str_of_range rng);
    P.printf "%s\n" (str_of_it it)
  ) ub_interp_ti

(*
  let cons_global_dbgmap interp_gti struct_map =
  let global_dbgmap = H.create 32 in
  List.iter (fun (tvnm,rng,ctlist) ->
  match tvnm with
  | TV_imm imm ->
  let startAddr = MWord.of_string ("0x"^imm) in
  List.iter (fun sct ->
  let arr_option = find_arr sct struct_map in
  if Option.is_some arr_option then
  let arrsct, eltsize, arrbound = Option.get arr_option in
  let newsct = 
  if is_memchunk arrsct then arrsct
  else (SC_pointer arrsct)
  in
  let newtvnms = List.init arrbound (fun i ->
  TV_imm (str_of_mword_flex (MWord.(+%) startAddr (MWord.of_int (i*eltsize)))) 
  ) in
  List.iter (fun tvnm' ->
  if not (H.mem global_dbgmap (tvnm',(-1))) then
  H.add global_dbgmap (tvnm',(-1)) [newsct]
  else
  let newect = 
  List.unique ?eq:(Some eq_sctype)
  (newsct :: (H.find global_dbgmap (tvnm',(-1))))
	      (*((SC_pointer arrsct) :: (H.find global_dbgmap (tvnm',(-1))))*)
  in
  H.replace global_dbgmap (tvnm',(-1)) newect
  ) newtvnms
  else
  if not (H.mem global_dbgmap (tvnm,(-1))) then
  H.add global_dbgmap (tvnm,(-1)) [sct]
  else
  let newect = 
  List.unique ?eq:(Some eq_sctype) (sct :: (H.find global_dbgmap (tvnm,(-1))))
  in
  H.replace global_dbgmap (tvnm,(-1)) newect
  ) ect
  | TV_addr addrnm ->
  assert(addrnm.base = None && addrnm.index = None);
  let startAddr = MWord.of_string ("0x"^addrnm.disp) in
  List.iter (fun sct ->
  let arr_option = find_arr sct struct_map in
  if Option.is_some arr_option then
  let arrsct, eltsize, arrbound = Option.get arr_option in
  let newsct =
  if is_memchunk arrsct then arrsct
  else (SC_pointer arrsct)
  in
  let newtvnms = List.init arrbound (fun i ->
  TV_addr 
  (mkADNM (str_of_mword_flex (MWord.(+%) startAddr (MWord.of_int (i*eltsize)))) None None) 
  ) in
  List.iter (fun tvnm' ->
  if not (H.mem global_dbgmap (tvnm',(-1))) then
  H.add global_dbgmap (tvnm',(-1)) [newsct]
  else
  let newect = 
  List.unique ?eq:(Some eq_sctype) 
  (newsct :: (H.find global_dbgmap (tvnm',(-1))))
  in
  H.replace global_dbgmap (tvnm',(-1)) newect
  ) newtvnms
  else
  if not (H.mem global_dbgmap (tvnm,(-1))) then
  H.add global_dbgmap (tvnm,(-1)) [sct]
  else
  let newect = 
  List.unique ?eq:(Some eq_sctype) (sct :: (H.find global_dbgmap (tvnm,(-1))))
  in
  H.replace global_dbgmap (tvnm,(-1)) newect
  ) ect
  | _ ->
  if not (H.mem global_dbgmap (tvnm,(-1))) then
  H.add global_dbgmap (tvnm,(-1)) ect
  else
  let newect = 
  List.unique ?eq:(Some eq_sctype) (ect @ (H.find global_dbgmap (tvnm,(-1))))
  in
  H.replace global_dbgmap (tvnm,(-1)) newect
  ) globalDBG;
  global_dbgmap
*)
(*
  let cons_global_dbgmap_consrv struct_map interp_gti =
  let global_dbgmap = H.create 32 in
  List.iter (fun (tvnm,rng,itlist) ->
  if not (H.mem global_dbgmap (tvnm,(-1))) then
  H.add global_dbgmap (tvnm,(-1)) itlist
  else
  let newitlist = 
  List.unique ?eq:(Some eq_it) (itlist @ (H.find global_dbgmap (tvnm,(-1))))
  in
  H.replace global_dbgmap (tvnm,(-1)) newitlist
  ) interp_gti;
  global_dbgmap

  let cons_fun_dbgmap_consrv asm_fun struct_map interp_pti =
  let fun_dbgmap = H.create 32 in
  let all_indices = 
  List.flatten (
  List.map (fun bb ->
  let addrcount = ref bb.bb_relAddr in 
  List.map (fun (pre,ins,len) ->
  let outaddr = !addrcount in
  addrcount := len + !addrcount;
  outaddr
  ) bb.bb_instrs
  ) asm_fun.func_bbs
  )
  in
  List.iter (fun (tvnm,rng,itlist) ->
  let covered_indices =
  match rng with
  | R_all -> all_indices
  | R_range (lopc,hipc) ->
  List.filter (fun index -> 
  lopc <= index && index < hipc
  ) all_indices
  in
  List.iter (fun ndx ->
  if not (H.mem fun_dbgmap (tvnm,ndx)) then
  H.add fun_dbgmap (tvnm,ndx) itlist
  else
  let newitlist = 
  List.unique ?eq:(Some eq_it) (itlist @ (H.find fun_dbgmap (tvnm,ndx)))
  in
  H.replace fun_dbgmap (tvnm,ndx) newitlist
  ) covered_indices
  ) interp_pti;
  fun_dbgmap

  let dump_dbgmap dbg_map =
  H.iter (fun (tvnm,tvndx) itlist ->
  if tvndx <> (-1) then
  P.printf "%s_%d -->\n" (str_of_tvnm tvnm) tvndx
  else
  P.printf "%s -->\n" (str_of_tvnm tvnm);
  List.iter (fun it ->
  P.printf "%s;\n" (str_of_it it)
  ) itlist
  ) dbg_map
*)
let icall_exists asm_fun =
  List.fold_left (fun b bb ->
    let _,ins,_ = List.last bb.bb_instrs in
    let icall_bb = 
      match ins with
      | CALL (dis,abs,op,slctr) ->
	begin match op with
	| Imm_op _ -> false
	| _ -> true
	end
      | _ -> false
    in
    b || icall_bb
  ) false asm_fun.func_bbs

let ijump_exists asm_fun =
  List.fold_left (fun b bb ->
    let _,ins,_ = List.last bb.bb_instrs in
    let ijump_bb = 
      match ins with
      | JMP (dis,abs,op,slctr) ->
	begin match op with
	| Imm_op _ -> false
	| _ -> true
	end
      | _ -> false
    in
    b || ijump_bb
  ) false asm_fun.func_bbs

let trans_with_off typedef_map struct_map itype offset =
  match itype with
  | IT_top reason -> 
    if offset = "some" then
      [itype; IT_top 3]
    else
      [itype]
  | IT_tblent ->
    (** no matter what offset is **)
    [itype]
  | IT_ptr it ->
    begin match it with
    | IT_off _ -> assert false
    | IT_elem (ctype, off) ->
      if offset = "some" then
	[itype]
      else
	let new_off = off + (int_of_32string offset) in
	[IT_ptr (IT_elem (ctype, new_off))]
    | _ -> [itype]
    end
  | IT_off _ -> assert false
  | IT_elem (ctype,off) ->
    if offset = "some" then
      [itype]
    else
      if is_ctype_ptr ctype then
	let new_off = off + (int_of_32string offset) in
	[IT_elem (ctype, new_off)]
      else
	try
	  let newctype = find_ctype_off typedef_map struct_map ctype off in
	  [IT_elem (newctype, (int_of_32string offset))]
	with _ -> []

let cite_with_off typedef_map struct_map itype offset =
  (** no matter what offset is, because we use heuristics **)
  [IT_ptr itype]
    
let rec strict_deref_ctype typedef_map struct_map ctype off =
  match ctype with
  | C_void -> [IT_top 2]
  | C_base _ -> []
  | C_const ct | C_volatile ct -> 
    strict_deref_ctype typedef_map struct_map ct off
  | C_array _ -> []
  | C_pointer (size,ct) ->
    begin 
      try
	let target_ct = find_ctype_off typedef_map struct_map ct off in
	[IT_elem (target_ct,0)]
      with _ ->
	(*
	  P.printf "Strict deref error! Over-approximate!\n";
	  flush stdout;
	*)
	[]
    end
  | C_typedef (defnm,ct) ->
    strict_deref_ctype typedef_map struct_map ct off
  | C_subroutine _ -> []
  | C_enumeration _ -> []
  | C_union (size,nm,fdlist) ->
    [IT_top 2]
  | C_structure _ -> []
  | _ -> []

let rec deref_with_off typedef_map struct_map itype offset =
  let deref_off = 
    if offset = "some" then 0
    else int_of_32string offset
  in  
  match itype with
  | IT_top reason -> [itype]
  | IT_tblent -> []
  | IT_ptr it ->
    begin match it with
    | IT_off _ -> assert false
    | IT_elem (ctype, off) ->
      let newoff = off + deref_off in
      [IT_elem (ctype, newoff)]      
    | _ ->
      [it]
    end
  | IT_off _ -> assert false
  | IT_elem (ctype, off) ->
    if is_ctype_ptr ctype then
      let newoff = off + deref_off in
      strict_deref_ctype typedef_map struct_map ctype newoff
    else
      let elem_ctype = find_ctype_off typedef_map struct_map ctype off in
      strict_deref_ctype typedef_map struct_map elem_ctype deref_off
(*
  let opaque_deref infer_type =
    match infer_type with
    | IT_top _ -> [infer_type]
    | IT_const _ -> []
    | IT_tblent ->
      (** should be impossible **)
      assert false
    | IT_off (ctype,typeoff) ->
      if typeoff = 0 then
	[IT_top 2]
	(*opaque_deref_ctype typdef_map struct_map ctype offset*)
      else
      [IT_top 2]
    | IT_elem (ctype,memoff) ->
      [IT_top 2]
  in
  if offset = "some" then
    opaque_deref itype
  else
    let off = int_of_32string offset in
    match itype with
    | IT_top _ -> [itype]
    | IT_const _ -> []
    | IT_tblent ->
      (** should be impossible **)
      assert false
    | IT_off(ctype,typeoff) ->
      assert false
    | IT_elem(ctype,memoff) ->
      let target_ctype = 
	find_ctype_off typdef_map struct_map ctype memoff
      in
      strict_deref_ctype target_ctype
*)
let reason_itlist itlist edgestack typedef_map struct_map =
  if List.is_empty itlist || 
    sub_itlist typedef_map struct_map itlist 
    [IT_top 0; IT_top 1; IT_top 2; IT_top 3; IT_top 4]
    (*
    sub_itlist itlist [IT_private (IT_top 0); 
		       IT_private (IT_top 1); 
		       IT_private (IT_top 2); 
		       IT_private (IT_top 3); 
		       IT_private (IT_top 4)]
    *)
  then
    itlist
  else (
    List.fold_left (fun retitlist edge ->
      let edgetype, offset = edge in
      if edgetype = 0 then (
	List.unique ?eq:(Some (eq_it typedef_map struct_map)) (List.flatten (
	  List.map (fun it ->
	    trans_with_off typedef_map struct_map it offset
	  ) retitlist
	))	  
      )
      else if edgetype = 2 then
	(** without lea, no edgetype 2 **)
	(** can only happen for lea cfa **)
	List.unique ?eq:(Some (eq_it typedef_map struct_map)) (List.flatten (
	  List.map (fun it ->
	    cite_with_off typedef_map struct_map it offset
	  ) retitlist
	))
      else (** edgetype = 1 **)
	List.unique ?eq:(Some (eq_it typedef_map struct_map)) (List.flatten (
	  List.map (fun it ->
	    deref_with_off typedef_map struct_map it offset
	  ) retitlist
	))
    ) itlist edgestack					     
  )

let add_to_dbgmap typedef_map struct_map processed_dbgmap tvnm tvndx it =
  if H.mem processed_dbgmap (tvnm,tvndx) then
    let curr_itlist = 
      H.find processed_dbgmap (tvnm,tvndx)
    in
    let new_itlist =
      List.unique ?eq:(Some (eq_it typedef_map struct_map))
	(it :: curr_itlist)
    in
    H.replace processed_dbgmap (tvnm,tvndx) new_itlist
  else
    H.add processed_dbgmap (tvnm, tvndx) [it]
(*
let unfold_reg typedef_map struct_map processed_dbgmap tvnm tvndx itlist =
  List.iter (fun it ->
    match it with
    | IT_ctype ctype ->
      begin match ctype with
      | C_array _
      | C_structure _ ->
	P.printf "Unexpected type in register:\n%s\n" (str_of_it it);
	flush stdout;
	assert false
      | C_typedef (defnm, _) ->
	if H.mem typedef_map defnm then (
	  P.printf "Unexpected type in register:\n%s\n" (str_of_it it);
	  flush stdout;
	  assert false
	)
      | _ -> 
	add_to_dbgmap processed_dbgmap tvnm tvndx it
      end
    | IT_off (ctype,off) ->
      add_to_dbgmap processed_dbgmap tvnm tvndx it
    | IT_ptr _ ->
      add_to_dbgmap processed_dbgmap tvnm tvndx it
    | _ -> 
      P.printf "Unexpected type in register:\n%s\n" (str_of_it it);
      flush stdout;
      assert false
  ) itlist
      
let unfold_cfa_ct typedef_map struct_map dbgmap tvnm tvndx ctype =
  let curr_cfaoff = 
    match tvnm with
    | TV_cfa off -> off
    | _ ->
      assert false
  in

  let rec add_field_offset_cfa ct off =
    match ct with
    | C_array (ct',dimlist) ->
      let ctsize = size_of_ctype typedef_map struct_map ct' in
      let bound = calc_array_bound dimlist in
      let offs = List.init bound (fun i -> off+i*ctsize) in
      List.iter (fun off' ->
	add_field_offset_cfa ct' off'
      ) offs
    | C_structure (size,nm,fdlist) ->
      if (not (List.is_empty fdlist)) && size <> 0 then
	List.iter (fun (memoff,ft,fn) ->
	  add_field_offset_cfa ft (off+memoff)
	) fdlist
      else if H.mem struct_map nm then
	add_field_offset_cfa (H.find struct_map nm) off
      else
	assert false
    | C_typedef (defnm,ct') ->
      if H.mem typedef_map defnm then (
	add_field_offset_cfa (H.find typedef_map defnm) off
      )
      else (
	add_field_offset_cfa ct' off
      )
    | C_const ct' | C_volatile ct' ->
      add_field_offset_cfa ct' off
    | _ ->
      let newcfaoff = curr_cfaoff - off in
      let newtvnm = TV_cfa newcfaoff in
      let newit = IT_elem (ctype,off) in
      add_to_dbgmap dbgmap newtvnm tvndx newit
  in
  add_field_offset_cfa ctype 0

let unfold_cfa typedef_map struct_map processed_dbgmap tvnm tvndx itlist =
  List.iter (fun it ->
    match it with
    | IT_ctype ctype ->
      unfold_cfa_ct typedef_map struct_map processed_dbgmap tvnm tvndx ctype
    | _ -> assert false
  ) itlist
    
let unfold_addr_ct typedef_map struct_map dbgmap tvnm tvndx ctype =
  let curr_addr =
    match tvnm with
    | TV_addr adnm ->
      int_of_32string adnm.disp
    | _ -> assert false
  in
  let rec add_field_offset_addr ct off =
    match ct with
    | C_array (ct',dimlist) ->
      let ctsize = size_of_ctype typedef_map struct_map ct' in
      let bound = calc_array_bound dimlist in
      let offs = List.init bound (fun i -> off+i*ctsize) in
      List.iter (fun off' ->
	add_field_offset_addr ct' off'
      ) offs
    | C_structure (size,nm,fdlist) ->
      if (not (List.is_empty fdlist)) && size <> 0 then
	List.iter (fun (memoff,ft,fn) ->
	  add_field_offset_addr ft (off+memoff)
	) fdlist
      else if H.mem struct_map nm then
	add_field_offset_addr (H.find struct_map nm) off
      else
	assert false
    | C_typedef (defnm,ct') ->
      if H.mem typedef_map defnm then (
	add_field_offset_addr (H.find typedef_map defnm) off
      )
      else (
	add_field_offset_addr ct' off
      )
    | C_const ct' | C_volatile ct' ->
      add_field_offset_addr ct' off
    | _ ->
      let newaddr = curr_addr + off in
      let newadnm = mkADNM
	(P.sprintf "%x" newaddr)
	None None
      in
      let newtvnm1 = TV_addr newadnm in
      let newtvnm2 = TV_imm (P.sprintf "%x" newaddr) in
      let newit = IT_ptr (IT_elem (ctype,off)) in
      add_to_dbgmap dbgmap newtvnm1 tvndx newit;
      add_to_dbgmap dbgmap newtvnm2 tvndx newit
  in
  add_field_offset_addr ctype 0

let unfold_addr typedef_map struct_map processed_dbgmap tvnm tvndx itlist =
  let adnm =
    match tvnm with
    | TV_addr adnm -> adnm
    | _ -> assert false
  in
  List.iter (fun it ->
    match it with
    | IT_ctype ctype -> 
      unfold_addr_ct typedef_map struct_map processed_dbgmap tvnm tvndx ctype
    | IT_ptr _ ->
      add_to_dbgmap processed_dbgmap tvnm tvndx it;
      add_to_dbgmap processed_dbgmap (TV_imm adnm.disp) tvndx it
    | _ -> 
      assert false
  ) itlist

let process_dbgmap typedef_map struct_map dbgmap =
  let processed_dbgmap = H.create 32 in
  H.iter (fun (tvnm,tvndx) itlist ->
    match tvnm with
    | TV_imm _ ->
      assert false
    | TV_reg _ ->
      unfold_reg typedef_map struct_map processed_dbgmap tvnm tvndx itlist
    | TV_cfa _ ->
      unfold_cfa typedef_map struct_map processed_dbgmap tvnm tvndx itlist
    | TV_addr adnm -> 
      assert (adnm.base = None && adnm.index = None);
      unfold_addr typedef_map struct_map processed_dbgmap tvnm tvndx itlist
  ) dbgmap;
  processed_dbgmap

let dbgmap_check dbgmap =
  H.iter (fun (tvnm,tvndx) itlist ->
    match tvnm with
    | TV_imm immstr ->
      assert (not (String.exists immstr "0x"));
      assert (H.mem dbgmap (TV_addr {disp=immstr;base=None;index=None},tvndx));
      List.iter (fun it ->
	begin match it with
	| IT_ptr _ -> ()
	| _ -> assert false
	end
      ) itlist
    | TV_reg _ -> ()
    | TV_cfa _ ->
      List.iter (fun it ->
	begin match it with
	| IT_elem _ -> ()
	| _ -> assert false
	end
      ) itlist
    | TV_addr adnm ->
      assert (not (String.exists adnm.disp "0x"));
      assert (adnm.base = None && adnm.index = None);
      assert (H.mem dbgmap ((TV_imm adnm.disp),tvndx));
      List.iter (fun it ->
	begin match it with
	| IT_ptr _ -> ()
	| _ -> assert false
	end
      ) itlist
  ) dbgmap
*)
type typeVar = {
  (** a type variable contains information:
      1. storage location: tv_name
      2. program location: tv_index + tv_crossbb
  **)
  tv_name: tvName;
  tv_index: int;
  tv_crossbb: bool;
}

let mkTV tvnm tvndx tvcros = {
  tv_name = tvnm;
  tv_index = tvndx;
  tv_crossbb = tvcros;
}

let str_of_tv tv =
  if tv.tv_crossbb then
    P.sprintf "%s_%x+" (str_of_tvnm tv.tv_name) tv.tv_index
  else
    P.sprintf "%s_%x" (str_of_tvnm tv.tv_name) tv.tv_index

let print_tv tv =
  P.printf "%s" (str_of_tv tv)

let dbgtype_tag tv dbgTypeInfo =
  let fun_dbgmap, global_dbgmap = dbgTypeInfo in
  if tv.tv_crossbb then []
  else
    let tvnm = tv.tv_name in
    let index = tv.tv_index in
    match tvnm with
    | TV_imm imm_str ->
      begin 
	try
	  IT_elem (C_base (4,"int"), 0) :: (H.find global_dbgmap (tvnm,(-1)))
	with Not_found ->
	  [IT_elem (C_base (4,"int"), 0)]
      end
    | TV_addr adnm ->
      assert (adnm.base = None && adnm.index = None);
      begin 
	try
	  H.find global_dbgmap (tvnm,(-1))
	with Not_found ->
	  []
      end
    | TV_cfa _ | TV_reg _ ->
      begin 
	try
	  H.find fun_dbgmap (tvnm,index)
	with Not_found ->
	  []
      end

let in_namerng tvnm tvnmrng =
  let start_tvnm, end_tvnm = tvnmrng in
  match tvnm with
  | TV_imm _ ->
    assert false
  | TV_reg reg ->
    begin match start_tvnm with
    | TV_reg reg' ->
      reg = reg'
    | _ -> false
    end
  | TV_cfa off ->
    begin match start_tvnm with
    | TV_cfa start_off ->
      off >= start_off &&
	begin match end_tvnm with
	| TV_cfa end_off ->
	  off < end_off
	| _ -> assert false
	end
    | _ -> false
    end
  | TV_addr adnm ->
    begin match start_tvnm with
    | TV_addr start_adnm ->
      let addr = int_of_32string adnm.disp in
      let start_addr = int_of_32string start_adnm.disp in
      addr >= start_addr &&
	begin match end_tvnm with
	| TV_addr end_adnm ->
	  let end_addr = int_of_32string end_adnm.disp in
	  addr < end_addr
	| _ -> assert false
	end
    | _ -> false
    end
(*
let rec gen_elem tvnmrng tvnm it =
  let start_tvnm, _ = tvnmrng in
  match tvnm with
  | TV_imm _ -> assert false
  | TV_reg reg ->
    begin match it with
    | IT_ctype ctype ->
      IT_elem (ctype, 0)
    | _ -> assert false
    end
  | TV_cfa off ->
    begin match it with
    | IT_ctype ctype ->
      begin match start_tvnm with
      | TV_cfa start_off ->
	IT_elem (ctype, (start_off - off))
      | _ -> assert false
      end
    | _ -> assert false
    end
  | TV_addr adnm ->
    begin match it with
    | IT_ptr (IT_ctype ctype) ->
      begin match start_tvnm with
      | TV_addr start_adnm ->
	let addr = int_of_32string adnm.disp in
	let start_addr = int_of_32string start_adnm.disp in
	IT_ptr (IT_elem (ctype, (addr - start_addr)))
      | _ -> assert false
      end
    | IT_ctype ctype ->
      begin match start_tvnm with
      | TV_addr start_adnm ->
	let addr = int_of_32string adnm.disp in
	let start_addr = int_of_32string start_adnm.disp in
	IT_ptr (IT_elem (ctype, (addr - start_addr)))
      | _ -> assert false
      end
    | _ -> assert false
      
    end

let locate_in_ubti ub_interp_ti tvnm tvndx =
  List.filter_map (fun (tvnmrng, rng, it) ->
    if in_namerng tvnm tvnmrng && in_range tvndx rng then
      Some (gen_elem tvnmrng tvnm it)
    else
      None
  ) ub_interp_ti

let ubti_tag tv ubti =
  if tv.tv_crossbb then []
  else
    let tvnm = tv.tv_name in
    let tvndx = tv.tv_index in
    match tvnm with
    | TV_imm imm_str ->
      let pseudo_addr = TV_addr (mkADNM imm_str None None) in
      begin
	try
	  (IT_const imm_str) :: (locate_in_ubti ubti pseudo_addr tvndx)
	with Not_found ->
	  []
      end
    | TV_reg _ | TV_cfa _ ->
      begin 
	try
	  locate_in_ubti ubti tvnm tvndx
	with Not_found ->
	  []
      end
    | TV_addr adnm ->
      assert (adnm.base = None && adnm.index = None);
      begin 
	try
	  locate_in_ubti ubti tvnm tvndx
	with Not_found ->
	  []
      end
*)
let effi_tag tv effi_map =
  let effi_fun_map, effi_global_map = effi_map in
  if tv.tv_crossbb then []
  else
    let tvnm = tv.tv_name in
    let index = tv.tv_index in
    match tvnm with
    | TV_imm imm_str ->
      let pseudo_addr = TV_addr (mkADNM imm_str None None) in
      begin
	try
	  let itlist = 
	    List.filter_map (fun (rng, it) ->
	      if in_range index rng then
		Some it
	      else
		None
	    ) (H.find effi_global_map pseudo_addr)
	  in
	  (IT_elem (C_base (4,"int"), 0)) :: itlist
	with Not_found ->
	  begin 
	    try
	      let itlist = 
		List.filter_map (fun (rng, it) ->
		  if in_range index rng then
		    Some it
		  else
		    None
		) (H.find effi_fun_map pseudo_addr)
	      in
	      (IT_elem (C_base (4,"int"), 0)) :: itlist
	    with Not_found ->
	      [IT_elem (C_base (4,"int"), 0)]
	  end
      end
    | TV_addr adnm ->
      assert (adnm.index = None);
      let in_range =
	if adnm.base = None then in_range
	else in_range_for_breg
      in
      begin
	try
	  let itlist = 
	    List.filter_map (fun (rng, it) ->
	      if in_range index rng then
		Some it
	      else
		None
	    ) (H.find effi_global_map tvnm)
	  in
	  itlist
	with Not_found ->
	  begin 
	    try
	      let itlist = 
		List.filter_map (fun (rng, it) ->
		  if in_range index rng then
		    Some it
		  else
		    None
		) (H.find effi_fun_map tvnm)
	      in
	      itlist
	    with Not_found ->
	      []
	  end
      end
    | TV_cfa _ | TV_reg _ ->
      begin 
	try
	  let itlist =
	    List.filter_map (fun (rng, it) ->
	      if in_range index rng then
		Some it
	      else
		None
	    ) (H.find effi_fun_map tvnm)
	  in
	  itlist
	with Not_found ->
	  []
      end

let ultim_tag tv allInfo =
  let effi_fun_map, effi_global_map, jmptbl, funsig_map = allInfo in
  if tv.tv_crossbb then []
  else
    let tvnm = tv.tv_name in
    match tvnm with
    | TV_imm imm_str ->
      if List.mem imm_str jmptbl then
	[IT_ptr IT_tblent]
      else if H.mem funsig_map imm_str then
	[IT_elem (H.find funsig_map imm_str, 0)]
      else
	effi_tag tv (effi_fun_map, effi_global_map)
    | TV_addr adnm ->
      let addr_str = adnm.disp in
      if List.mem addr_str jmptbl then
	[IT_ptr IT_tblent]
      else if H.mem funsig_map addr_str then
	[IT_elem (H.find funsig_map addr_str, 0)]
      else
	effi_tag tv (effi_fun_map, effi_global_map)
    | _ ->
      effi_tag tv (effi_fun_map, effi_global_map)

module TypeVarOrder = struct
  type t = typeVar
  let compare (tv1:typeVar) (tv2:typeVar) = 
    Pervasives.compare tv1 tv2
end

module TypeVarSet = Set.Make (TypeVarOrder)

type eqcNode = 
  {
    mutable eqc_name: string;
    mutable eqc_vars: TypeVarSet.t;
    mutable eqc_succs: (typeVar*(int*string)) list;
    mutable eqc_preds: (typeVar*(int*string)) list;
    mutable eqc_dbgtype: infer_t list;
    mutable eqc_fflowtype: infer_t list;
    mutable eqc_bflowtype: infer_t list;
  }

let mkEQCNode tv = 
  {
    eqc_name = str_of_tv tv;
    eqc_vars = TypeVarSet.singleton tv;
    eqc_succs = []; eqc_preds = []; 
    eqc_dbgtype = []; 
    eqc_fflowtype = [];
    eqc_bflowtype = [];
  }

let dump_eqcnode eqcnode =
  P.printf "EQC_node %s contains variables:\n" 
    eqcnode.eqc_name;
  TypeVarSet.iter (fun var ->
    P.printf "%s " (str_of_tv var)
  ) eqcnode.eqc_vars;
  P.printf "\n";
  P.printf "SUCCS:\n";
  List.iter (fun (tv,edge) ->
    P.printf "(%s,%s) " (str_of_tv tv) (str_of_edge edge)
  ) eqcnode.eqc_succs;
  P.printf "\n";
  P.printf "PREDS:\n";
  List.iter (fun (tv,edge) ->
    P.printf "(%s,%s) " (str_of_tv tv) (str_of_edge edge)
  ) eqcnode.eqc_preds;
  P.printf "\n";
  P.printf "TypeInfo from DBG:\n";
  List.iter (fun it -> 
    P.printf "%s\n" (str_of_it it)
  ) eqcnode.eqc_dbgtype;
  P.printf "\n";
  P.printf "TypeInfo from Forward-Flow:\n";
  List.iter (fun it -> 
    P.printf "%s\n" (str_of_it it)
  ) eqcnode.eqc_fflowtype;
  P.printf "\n";
  P.printf "TypeInfo from Backward-Flow:\n";
  List.iter (fun it -> 
    P.printf "%s\n" (str_of_it it)
  ) eqcnode.eqc_bflowtype;
  P.printf "\n";
  flush stdout

type eqcGraph = (typeVar, string) H.t * (string, eqcNode) H.t

let dump_nodes eqcg =
  let _,nodes = eqcg in
  H.iter (fun repr node ->
    P.printf "%s -->\n" repr;
    dump_eqcnode node;
    P.printf "\n";
    flush stdout
  ) nodes

let dump_eqcg eqcg =
  let varmap, nodes = eqcg in
  H.iter (fun tv1 str ->
    print_tv tv1;
    P.printf " --> %s" str;
    P.printf "\n";
    flush stdout
  ) varmap;
  H.iter (fun repr node ->
    P.printf "%s -->\n" repr;
    dump_eqcnode node;
    P.printf "\n";
    flush stdout
  ) nodes

let eqcnode_merge node1 node2 =
  node1.eqc_vars <- TypeVarSet.union node1.eqc_vars node2.eqc_vars;
  node1.eqc_succs <- 
    List.unique (node1.eqc_succs @ node2.eqc_succs);
  node1.eqc_preds <- 
    List.unique (node1.eqc_preds @ node2.eqc_preds);
  node1.eqc_dbgtype <- 
    List.unique (node1.eqc_dbgtype @ node2.eqc_dbgtype);
  node1.eqc_fflowtype <- 
    List.unique (node1.eqc_fflowtype @ node2.eqc_fflowtype);
  node1.eqc_bflowtype <- 
    List.unique (node1.eqc_bflowtype @ node2.eqc_bflowtype)

let eqcg_find_repr eqcg tv =
  let varmap, _ = eqcg in
  try
    Some (H.find varmap tv)
  with Not_found ->
    None

let eqcg_find_node eqcg repr =
  let _, nodes = eqcg in
  try
    H.find nodes repr
  with Not_found ->
    raise (Failure "One repr does not have correspondent node!")

let eqcg_find eqcg tv =
  let varmap, nodes = eqcg in
  H.find nodes (H.find varmap tv)

let eqcg_add_repr eqcg tv repr =
  let varmap, _ = eqcg in
  H.add varmap tv repr

let eqcg_add_node eqcg repr node =
  let _, nodes = eqcg in
  H.add nodes repr node

let eqcg_update eqcg tv node =
  let varmap,nodes = eqcg in
  H.replace varmap tv node.eqc_name;
(*
  TypeVarSet.iter (fun tv ->
    H.replace varmap tv node.eqc_name;
  ) node.eqc_vars;*)
  H.replace nodes node.eqc_name node

let eqcg_replace_repr eqcg tv repr =
  let varmap, _ = eqcg in
  H.replace varmap tv repr

let eqcg_replace_node eqcg repr node =
  let _,nodes = eqcg in
  H.replace nodes repr node

let eqcg_remove_node eqcg repr =
  let _, nodes = eqcg in
  H.remove_all nodes repr

let eqcg_connect eqcg tvd tvd_tag tagged_tv_list =
  let tvd_repr = eqcg_find_repr eqcg tvd in
  let tvd_node =
    if Option.is_none tvd_repr then
      let tv_node = mkEQCNode tvd in
      tv_node.eqc_dbgtype <- tvd_tag;
      eqcg_add_repr eqcg tvd tv_node.eqc_name;
      eqcg_add_node eqcg tv_node.eqc_name tv_node;
      tv_node
    else
      let tv_node = eqcg_find_node eqcg (Option.get tvd_repr) in
      tv_node.eqc_dbgtype <- List.unique (tvd_tag @ tv_node.eqc_dbgtype);
      tv_node
  in
  let node_unique node_list = 
    List.unique ?eq:(Some (fun (nodename1,edge1) (nodename2,edge2) ->
      nodename1 = nodename2 && edge1 = edge2   
    )) node_list
  in
  List.iter (fun (tvs, tvs_tag, edges) ->
    let tvs_repr = eqcg_find_repr eqcg tvs in
    if Option.is_none tvs_repr then (
      let tvs_node = mkEQCNode tvs in
      tvs_node.eqc_dbgtype <- tvs_tag;
      tvs_node.eqc_succs <- node_unique ((tvd,edges) :: tvs_node.eqc_succs);
      tvd_node.eqc_preds <- node_unique ((tvs,edges) :: tvd_node.eqc_preds);
      eqcg_add_repr eqcg tvs tvs_node.eqc_name;
      eqcg_add_node eqcg tvs_node.eqc_name tvs_node;
      eqcg_update eqcg tvd tvd_node
    ) else (
      let tvs_node = eqcg_find_node eqcg (Option.get tvs_repr) in
      tvs_node.eqc_dbgtype <- List.unique (tvs_tag @ tvs_node.eqc_dbgtype);
      tvs_node.eqc_succs <- node_unique ((tvd,edges) :: tvs_node.eqc_succs);
      tvd_node.eqc_preds <- node_unique ((tvs,edges) :: tvd_node.eqc_preds);
      eqcg_update eqcg tvd tvd_node;
      eqcg_update eqcg tvs tvs_node
    )
  ) tagged_tv_list

let eqcg_merge eqcg tv1 tv1_tag tv2 tv2_tag = 
  let eq_tag tag1 tag2 =
    if List.length tag1 <> List.length tag2 then
      false
    else
      List.fold_right (fun it1 b ->
	b && List.mem it1 tag2
      ) tag1 true
  in
  if (List.is_empty tv1_tag && List.is_empty tv2_tag) || (eq_tag tv1_tag tv2_tag) then (
    let dbgtag = tv1_tag in
    let tv1_repr = eqcg_find_repr eqcg tv1 in
    let tv2_repr = eqcg_find_repr eqcg tv2 in
    if Option.is_none tv1_repr && Option.is_none tv2_repr then (
      let tv1_node = mkEQCNode tv1 in
      let tv2_node = mkEQCNode tv2 in
      eqcnode_merge tv2_node tv1_node;
      tv2_node.eqc_dbgtype <- dbgtag;
      eqcg_add_repr eqcg tv1 tv2_node.eqc_name;
      eqcg_add_repr eqcg tv2 tv2_node.eqc_name;
      eqcg_add_node eqcg tv2_node.eqc_name tv2_node
    ) else if Option.is_none tv1_repr then (
      let tv1_node = mkEQCNode tv1 in
      let tv2_node = eqcg_find_node eqcg (Option.get tv2_repr) in
      eqcnode_merge tv2_node tv1_node;
      tv2_node.eqc_dbgtype <- List.unique (dbgtag @ tv2_node.eqc_dbgtype);
      eqcg_add_repr eqcg tv1 (Option.get tv2_repr);
      eqcg_replace_node eqcg (Option.get tv2_repr) tv2_node
    ) else if Option.is_none tv2_repr then (
      let tv2_node = mkEQCNode tv1 in
      let tv1_node = eqcg_find_node eqcg (Option.get tv1_repr) in
      eqcnode_merge tv1_node tv2_node;
      tv1_node.eqc_dbgtype <- List.unique (dbgtag @ tv1_node.eqc_dbgtype);
      eqcg_add_repr eqcg tv2 (Option.get tv1_repr);
      eqcg_replace_node eqcg (Option.get tv1_repr) tv1_node
    ) else (
      let tv1_node = eqcg_find_node eqcg (Option.get tv1_repr) in
      let tv2_node = eqcg_find_node eqcg (Option.get tv2_repr) in
      eqcnode_merge tv2_node tv1_node;
      tv2_node.eqc_dbgtype <- List.unique (dbgtag @ tv2_node.eqc_dbgtype);
      eqcg_replace_repr eqcg tv1 (Option.get tv2_repr);
      eqcg_remove_node eqcg (Option.get tv1_repr);
      eqcg_replace_node eqcg (Option.get tv2_repr) tv2_node
    )
  ) else(
    eqcg_connect eqcg tv1 tv1_tag [tv2,tv2_tag,(0,"0")]
  )

let eqcg_merge_consrv eqcg tv1 tv1_tag tv2 tv2_tag = 
  if List.is_empty tv1_tag && List.is_empty tv2_tag then (
    let tv1_repr = eqcg_find_repr eqcg tv1 in
    let tv2_repr = eqcg_find_repr eqcg tv2 in
    if Option.is_none tv1_repr && Option.is_none tv2_repr then (
      let tv1_node = mkEQCNode tv1 in
      let tv2_node = mkEQCNode tv2 in
      eqcnode_merge tv2_node tv1_node;
      eqcg_add_repr eqcg tv1 tv2_node.eqc_name;
      eqcg_add_repr eqcg tv2 tv2_node.eqc_name;
      eqcg_add_node eqcg tv2_node.eqc_name tv2_node
    ) else if Option.is_none tv1_repr then (
      let tv1_node = mkEQCNode tv1 in
      let tv2_node = eqcg_find_node eqcg (Option.get tv2_repr) in
      eqcnode_merge tv2_node tv1_node;
      eqcg_add_repr eqcg tv1 (Option.get tv2_repr);
      eqcg_replace_node eqcg (Option.get tv2_repr) tv2_node
    ) else if Option.is_none tv2_repr then (
      let tv2_node = mkEQCNode tv1 in
      let tv1_node = eqcg_find_node eqcg (Option.get tv1_repr) in
      eqcnode_merge tv1_node tv2_node;
      eqcg_add_repr eqcg tv2 (Option.get tv1_repr);
      eqcg_replace_node eqcg (Option.get tv1_repr) tv1_node
    ) else (
      let tv1_node = eqcg_find_node eqcg (Option.get tv1_repr) in
      let tv2_node = eqcg_find_node eqcg (Option.get tv2_repr) in
      eqcnode_merge tv2_node tv1_node;
      eqcg_replace_repr eqcg tv1 (Option.get tv2_repr);
      eqcg_remove_node eqcg (Option.get tv1_repr);
      eqcg_replace_node eqcg (Option.get tv2_repr) tv2_node
    )
  ) else(
    eqcg_connect eqcg tv1 tv1_tag [tv2,tv2_tag,(0,"")]
  )

let is_trivial ins =
  match ins with
  | PUSH _ | POP _ | LEAVE -> true
  | NOP _ -> true
  | JMP(_,_,_,_) | Jcc(_,_) | JCXZ(_) -> true
  | CMP _ | TEST _ | CMPS _ -> true
  | MUL _ | IDIV _ | DIV _ | SBB _ | ADC _ -> true
  | AND _ | OR _ | NEG _ -> true
  | BT _ | BTC _ | BTR _ | BTS _ -> true
  | DEC _ | INC _ -> true
  | CWDE -> true
  | MOVS _ -> true
  | CLD -> true
  | SCAS _ -> true
  | SHL _ | SHR _ | SHLD _ | SHRD _ | SAR _ -> true
  (** float-point instructions **) 
  | F2XM1 
  | FABS 
  | FADD _ 
  | FADDP _ 
  | FBLD _
  | FBSTP _
  | FCHS
  | FCMOVcc _ 
  | FCOM _ 
  | FCOMP _ 
  | FCOMPP
  | FCOMIP _
  | FCOS
  | FDECSTP
  | FDIV _
  | FDIVP _
  | FDIVR _
  | FDIVRP _
  | FFREE _
  | FIADD _
  | FICOM _
  | FICOMP _
  | FIDIV _
  | FIDIVR _
  | FILD _
  | FIMUL _
  | FINCSTP
  | FIST _
  | FISTP _
  | FISUB _
  | FISUBR _
  | FLD _
  | FLD1
  | FLDCW _
  | FLDENV _
  | FLDL2E
  | FLDL2T
  | FLDLG2
  | FLDLN2
  | FLDPI
  | FLDZ
  | FMUL _
  | FMULP _
  | FNCLEX
  | FNINIT
  | FNOP
  | FNSAVE _
  | FNSTCW _
  | FNSTSW _
  | FPATAN
  | FPREM
  | FPREM1
  | FPTAN
  | FRNDINT
  | FRSTOR _
  | FSCALE
  | FSIN
  | FSINCOS
  | FSQRT
  | FST _
  | FSTENV _
  | FSTP _
  | FSUB _
  | FSUBP _
  | FSUBR _
  | FSUBRP _
  | FTST
  | FUCOM _
  | FUCOMP _
  | FUCOMPP
  | FUCOMI _
  | FUCOMIP _
  | FXAM
  | FXCH _
  | FXTRACT
  | FYL2X
  | FYL2XP1
  | FWAIT -> true
  | _ -> false

let is_evil ins =
  match ins with
  | RET(_,_) | IRET -> true
  | _ -> false

let consrv_worth_tracking funCFA asm_fun =
  let inner_tvNameGen funCFA op l =
    let tvnm, _ = tvNameGen funCFA op l in
    match tvnm with
    | TV_cfa off when off < (-4) ->
      [tvnm]
    | TV_reg reg when reg <> ESP -> 
      [tvnm]
    | _ -> []
  in
  let tvnm_of_ops op_list l =
    List.flatten (List.map (fun op ->
      inner_tvNameGen funCFA op l
    ) op_list)
  in
  List.unique (
    List.flatten (List.map (fun bb ->  
      let addrcount = ref bb.bb_relAddr in
      let l_instr_list = 
	List.map (fun (pre,ins,len) -> 
	  let outaddr = !addrcount in
	  addrcount := len + !addrcount;
	  outaddr, (pre,ins,len)
	) bb.bb_instrs 
      in
      List.flatten (List.map (fun (l, (pre,ins,len)) ->
	if is_trivial ins then []
	else match ins with
	| PUSH(_,op)
	| POP(op) 
	| NEG(_,op) 
	| NOT(_,op)
	| SHL(_,op,_) 
	| SHR(_,op,_)
	| SAR(_,op,_)
	| ROL(_,op,_)
	| ROR(_,op,_)
	| SETcc(_,op) -> tvnm_of_ops [op] l

	| MOV(_,opd,ops)
	| MOVZX(_,opd,ops)
	| MOVSX(_,opd,ops)
	| LEA(opd,ops)
	| CMOVcc(_,opd,ops)
	| XOR(_,opd,ops)
	| AND(_,opd,ops)
	| OR(_,opd,ops)

	| XCHG(_,opd,ops) -> tvnm_of_ops [opd;ops] l

	| SHLD(_,reg,_)
	| SHRD(_,reg,_)
   	| BSWAP reg -> tvnm_of_ops [Reg_op reg] l

	| ADD(width,opd,ops) -> 
	  let tvNameOpd = inner_tvNameGen funCFA opd l in
	  tvNameOpd
	| SUB(width,opd,ops) -> 
	  let tvNameOpd = inner_tvNameGen funCFA opd l in
	  tvNameOpd
	| CMP(width,ops1,ops2) -> 
	  let tvNameOps1 = inner_tvNameGen funCFA ops1 l in
	  let tvNameOps2 = inner_tvNameGen funCFA ops2 l in
	  tvNameOps1@tvNameOps2
	| CALL(dis,abs,op,slctr) 
	| JMP(dis,abs,op,slctr) ->
	  begin match op with
	  | Imm_op _ -> []
	  | _ ->
	    let tvNameOp = inner_tvNameGen funCFA op l in
	    tvNameOp
	  end
	| TEST(width,ops1,ops2) ->
	  let tvNameOps1 = inner_tvNameGen funCFA ops1 l in
	  let tvNameOps2 = inner_tvNameGen funCFA ops2 l in
	  tvNameOps1@tvNameOps2
	| _ -> []
      ) l_instr_list)
    ) asm_fun.func_bbs)
  )

let aggres_worth_tracking funCFA asm_fun =
  let inner_tvNameGen funCFA op l =
    let tvnm, _ = tvNameGen funCFA op l in
    match tvnm with
    | TV_cfa off when off < (-4) ->
      [tvnm]
    | TV_reg reg when reg <> ESP -> 
      [tvnm]
    | _ -> []
  in
  let tvnm_of_ops op_list l =
    List.flatten (List.map (fun op ->
      inner_tvNameGen funCFA op l
    ) op_list)
  in
  List.unique (
    List.flatten (List.map (fun bb ->  
      let addrcount = ref bb.bb_relAddr in
      let l_instr_list = 
	List.map (fun (pre,ins,len) -> 
	  let outaddr = !addrcount in
	  addrcount := len + !addrcount;
	  outaddr, (pre,ins,len)
	) bb.bb_instrs 
      in
      List.flatten (List.map (fun (l, (pre,ins,len)) ->
	if is_trivial ins then []
	else match ins with
	| MOV(_,opd,ops) -> tvnm_of_ops [opd;ops] l
	(*| MOVZX(_,opd,ops)
	| MOVSX(_,opd,ops)*)
	| LEA(opd,ops)
	| CMOVcc(_,opd,ops) -> tvnm_of_ops [opd;ops] l
	(*| XCHG(_,opd,ops) -> tvnm_of_ops [opd;ops] l*)

	| CALL(dis,abs,op,slctr) 
	| JMP(dis,abs,op,slctr) ->
	  begin match op with
	  | Imm_op _ -> []
	  | _ ->
	    let tvNameOp = inner_tvNameGen funCFA op l in
	    tvNameOp
	  end
	| _ -> []
      ) l_instr_list)
    ) asm_fun.func_bbs)
  )

let ic_checked = ref 0
let ij_checked = ref 0

let ic_over = ref 0
let ic_over_dbg = ref 0
let ic_over_inf = ref 0

let ij_over = ref 0
let ij_over_dbg = ref 0
let ij_over_inf = ref 0

let ic_wrong = ref 0
let ij_wrong = ref 0

let from_dbg = ref false

let ic_accept = ref 0
let ic_multi = ref 0
let ic_acpt_dbg = ref 0
let ic_acpt_inf = ref 0

let ij_accept = ref 0
let ij_multi = ref 0
let ij_tailcall = ref 0
let ij_tc_dbg = ref 0
let ij_tc_inf = ref 0
let ij_tablejmp = ref 0

let ic_decline = ref 0
let ic_dcln_dbg = ref 0
let ic_dcln_inf = ref 0

let ij_decline = ref 0
let ij_dcln_dbg = ref 0
let ij_dcln_inf = ref 0

let is_from_dbg tvnm edge =
  if fst edge = 0 then snd edge = "0"
  else if fst edge = 1 then
    begin match tvnm with
    | TV_reg _ -> false
    | TV_addr _ -> snd edge = "0"
    | _ -> assert false
    end
  else assert false

let check_funcptr_itlist typedef_map struct_map itlist =
  let acceptable = ref 0 in
  let over = ref 0 in

  let rec check_funcptr_ctype typedef_map struct_map ctype =
    match ctype with
    | C_void ->
      over := 1;
      P.printf "Function pointer over-approximated as void!\n";
    | C_pointer (size,ct) ->
      if is_ctype_subroutine ct then (
	acceptable := !acceptable + 1;
	P.printf "Correctly inferred Call-site type: \n";
	P.printf "%s\n" (str_of_c_type ctype);
      )
      else (
	ic_wrong := !ic_wrong + 1;
	P.printf "Wrong ctype for ICALL:\n%s\n" (str_of_c_type ct)
      )
    | C_typedef (_,ct) | C_const ct | C_volatile ct ->
      check_funcptr_ctype typedef_map struct_map ct
    | C_union (size,nm,memlist) ->
      List.iter (fun (ft,fn) ->
	check_funcptr_ctype typedef_map struct_map ft
      ) memlist
    | _ ->
      ic_wrong := !ic_wrong + 1;
      P.printf "Wrong ctype for ICALL:\n%s\n" (str_of_c_type ctype)
  in

  List.iter (fun itype ->
    match itype with
    | IT_top _ ->
      over := 1;
      P.printf "Function pointer over-approximated as TOP!\n";
    | IT_off (ctype,off) ->
      assert false
    | IT_elem (ctype,off) ->
      if off = 0 then 
	check_funcptr_ctype typedef_map struct_map ctype
      else
	if is_ctype_ptr ctype then (
	  ic_wrong := !ic_wrong + 1;
	  P.printf "Wrong itype for ICALL:\n%s\n" (str_of_it itype)
	)
	else (
	  try
	    let target_type = 
	      find_ctype_off typedef_map struct_map ctype off
	    in
	    check_funcptr_ctype typedef_map struct_map target_type
	  with Failure errmsg ->
	    ic_wrong := !ic_wrong + 1;
	    P.printf "Received error message:\n%s\n" errmsg;
	    P.printf "Wrong itype for ICALL:\n%s\n" (str_of_it itype)
	)
    | _ ->
      ic_wrong := !ic_wrong + 1;
      P.printf "Wrong itype for ICALL:\n%s\n" (str_of_it itype)
  ) itlist;
  if !over = 0 && !acceptable > 0 then (
    ic_accept := !ic_accept + 1;
    if !acceptable > 1 then ic_multi := !ic_multi + 1;
    if !from_dbg then ic_acpt_dbg := !ic_acpt_dbg + 1
    else ic_acpt_inf := !ic_acpt_inf + 1
  )
  else if !over = 1 then (
    ic_over := !ic_over + 1;
    if !from_dbg then ic_over_dbg := !ic_over_dbg + 1
    else ic_over_inf := !ic_over_inf + 1
  )
  else (
    P.printf "DECLINE type:\n%s\n" (str_of_itlist itlist);
    flush stdout;
    ic_decline := !ic_decline + 1;
    if !from_dbg then ic_dcln_dbg := !ic_dcln_dbg + 1
    else ic_dcln_inf := !ic_dcln_inf + 1
  )

let check_tblent_itlist typedef_map struct_map itlist =
  let acceptable = ref 0 in
  let tailcall = ref false in
  let tablejmp = ref false in
  let over = ref 0 in

  let rec check_funcptr_ctype typedef_map struct_map ctype =
    match ctype with
    | C_void ->
      over := 1;
      P.printf "Tail call over-approximated as void!\n";
    | C_pointer (size,ct) ->
      if is_ctype_subroutine ct then (
	acceptable := !acceptable + 1;
	tailcall := true;
	P.printf "Correctly inferred Indirect-Tail_Call-Site type: \n";
	P.printf "%s\n" (str_of_c_type ctype);
      )
      else (
	ij_wrong := !ij_wrong + 1;
	P.printf "Wrong ctype for IJUMP:\n%s\n" (str_of_c_type ct)
      )
    | C_typedef (_,ct) | C_const ct | C_volatile ct ->
      check_funcptr_ctype typedef_map struct_map ct
    | C_union (size,nm,memlist) ->
      List.iter (fun (ft,fn) ->
	check_funcptr_ctype typedef_map struct_map ft
      ) memlist
    | _ ->
      ij_wrong := !ij_wrong + 1;
      P.printf "Wrong ctype for IJUMP:\n%s\n" (str_of_c_type ctype)
  in

  List.iter (fun itype ->
    match itype with
    | IT_top _ ->
      over := 1;
      P.printf "Jump target over-approximated as TOP!\n";
    | IT_tblent ->
      acceptable := !acceptable + 1;
      tablejmp := true;
      P.printf "Correctly inferred Jump Entry!\n";
    | IT_elem (ctype,off) ->
      if off = 0 then 
	check_funcptr_ctype typedef_map struct_map ctype
      else
	if is_ctype_ptr ctype then (
	  ij_wrong := !ij_wrong + 1;
	  P.printf "Wrong itype for IJUMP:\n%s\n" (str_of_it itype)
	)
	else (
	  try
	    let target_type = 
	      find_ctype_off typedef_map struct_map ctype off
	    in
	    check_funcptr_ctype typedef_map struct_map target_type
	  with Failure errmsg ->
	    ic_wrong := !ic_wrong + 1;
	    P.printf "Received error message:\n%s\n" errmsg;
	    P.printf "Wrong itype for ICALL:\n%s\n" (str_of_it itype)
	)	    
    | _ ->
      ij_wrong := !ij_wrong + 1;
      P.printf "Wrong type for IJUMP:\n%s\n" (str_of_it itype)
  ) itlist;
  if !over = 0 && !acceptable > 0 then (
    ij_accept := !ij_accept + 1;
    if !tailcall && not !tablejmp then (
      ij_tailcall := !ij_tailcall + 1;
      if !acceptable > 1 then ij_multi := !ij_multi + 1;
      if !from_dbg then ij_tc_dbg := !ij_tc_dbg + 1
      else ij_tc_inf := !ij_tc_inf + 1
    ) 
    else if not !tailcall && !tablejmp then (
      ij_tablejmp := !ij_tablejmp + 1;
      if !acceptable > 1 then ij_multi := !ij_multi + 1;
    )
    else if !tailcall && !tablejmp then
      raise (Failure "IJUMP targets both local addresses and functions!")
    else
      assert false
  )
  else if !over = 1 then (
    ij_over := !ij_over + 1;
    if !from_dbg then ij_over_dbg := !ij_over_dbg + 1
    else ij_over_inf := !ij_over_inf + 1
  )
  else (
    P.printf "DECLINE type:\n%s\n" (str_of_itlist itlist);
    flush stdout;
    ij_decline := !ij_decline + 1;
    if !from_dbg then ij_dcln_dbg := !ij_dcln_dbg + 1
    else ij_dcln_inf := !ij_dcln_inf + 1
  )
    
let icij_checking typedef_map struct_map funCFA allInfo asm_fun eqcg =
  let ic_bb_list = 
    List.filter (fun bb ->
      does_bb_icall bb
    ) asm_fun.func_bbs
  in
  let ij_bb_list =
    List.filter (fun bb ->
      does_bb_ijump bb
    ) asm_fun.func_bbs
  in
  let ic_ops =
    List.map (fun bb ->
      let _,ins,len = List.last bb.bb_instrs in
      let branch_site = bb.bb_relAddr + bb.bb_size - len in
      let op = 
	match ins with
	| CALL (_,_,op,_) -> op
	| _ -> assert false
      in
      op, branch_site
    ) ic_bb_list
  in
  let ij_ops =
    List.map (fun bb ->
      let _,ins,len = List.last bb.bb_instrs in
      let branch_site = bb.bb_relAddr + bb.bb_size - len in
      let op = 
	match ins with
	| JMP (_,_,op,_) -> op
	| _ -> assert false
      in
      op, branch_site
    ) ij_bb_list
  in
  List.iter (fun (op,bsite) ->
    ic_checked := !ic_checked + 1;
    let optvnm, edge = tvNameGen funCFA op bsite in
    P.printf "Checking ICALL %s(%s) at %x...\n" 
      (str_of_tvnm optvnm) (str_of_edge edge) bsite;
    flush stdout;
    let auxtype = 
      if fst edge = 1 && snd edge <> "some" then
	begin match optvnm with
	| TV_reg reg ->
	  let off_dec = int_of_32string (snd edge) in
	  let off_dec_str = string_of_int off_dec in
	  let auxtvnm = TV_addr (mkADNM off_dec_str (Some reg) None) in
	  let auxtv = mkTV auxtvnm bsite false in
	  ultim_tag auxtv allInfo
	| _ -> []
	end
      else []
    in
    if List.is_empty auxtype then (
      let optv = mkTV optvnm bsite false in
      try
	let opnode = eqcg_find eqcg optv in
	if List.is_empty opnode.eqc_dbgtype && List.is_empty opnode.eqc_fflowtype then (
	  ic_wrong := !ic_wrong + 1;
	  ic_decline := !ic_decline + 1;
	  ic_dcln_inf := !ic_dcln_inf + 1;
	  P.printf "Type not inferred!\n"
	)
	else if List.is_empty opnode.eqc_fflowtype then (
	  from_dbg := is_from_dbg optvnm edge;
	  P.printf "Type from DBG field:\n";
	  let optype = reason_itlist opnode.eqc_dbgtype [edge] typedef_map struct_map in
	  check_funcptr_itlist typedef_map struct_map optype;
	  flush stdout
	)
	else (
	  from_dbg := false;
	  P.printf "Type inferred:\n";
	  let optype = reason_itlist opnode.eqc_fflowtype [edge] typedef_map struct_map in
	  (*P.printf "%s\n" (str_of_itlist optype)*)
	  check_funcptr_itlist typedef_map struct_map optype;
	  flush stdout
	);
	flush stdout;
      with Not_found ->
	from_dbg := is_from_dbg optvnm edge;
	let optype = ultim_tag optv allInfo in
	P.printf "Type from DBG map:\n";
	let optype = reason_itlist optype [edge] typedef_map struct_map in
	(*P.printf "%s\n" (str_of_itlist optype); flush stdout;*)
	check_funcptr_itlist typedef_map struct_map optype;
	flush stdout
    ) else (
      from_dbg := true;
      let optype = auxtype in
      check_funcptr_itlist typedef_map struct_map optype;
      flush stdout
    )
  ) ic_ops;
  List.iter (fun (op,bsite) ->
    ij_checked := !ij_checked + 1;
    let optvnm, edge = tvNameGen funCFA op bsite in
    P.printf "Checking IJUMP %s(%s) at %x...\n" 
      (str_of_tvnm optvnm) (str_of_edge edge) bsite;
    flush stdout;
    let auxtype = 
      if fst edge = 1 && snd edge <> "some" then
	begin match optvnm with
	| TV_reg reg ->
	  let off_dec = int_of_32string (snd edge) in
	  let off_dec_str = string_of_int off_dec in
	  let auxtvnm = TV_addr (mkADNM off_dec_str (Some reg) None) in
	  let auxtv = mkTV auxtvnm bsite false in
	  ultim_tag auxtv allInfo
	| _ -> []
	end
      else []
    in
    if List.is_empty auxtype then (
      let optv = mkTV optvnm bsite false in
      try
	let opnode = eqcg_find eqcg optv in
	if List.is_empty opnode.eqc_dbgtype && List.is_empty opnode.eqc_fflowtype then (
	  ij_wrong := !ij_wrong + 1;
	  ij_decline := !ij_decline + 1;
	  ij_dcln_inf := !ij_dcln_inf + 1;
	  P.printf "Type not inferred!\n"
	)
	else if List.is_empty opnode.eqc_fflowtype then (
	  from_dbg := is_from_dbg optvnm edge;
	  P.printf "Type from DBG field:\n";
	  let optype = reason_itlist opnode.eqc_dbgtype [edge] typedef_map struct_map in
	(*P.printf "%s\n" (str_of_itlist optype)*)
	  check_tblent_itlist typedef_map struct_map optype;
	  flush stdout
	)
	else (
	  from_dbg := false;
	  P.printf "Type inferred:\n";
	  let optype = reason_itlist opnode.eqc_fflowtype [edge] typedef_map struct_map in
	(*P.printf "%s\n" (str_of_itlist optype)*)
	  check_tblent_itlist typedef_map struct_map optype;
	  flush stdout
	);
	flush stdout;
      with Not_found ->
	from_dbg := is_from_dbg optvnm edge;
	P.printf "Type from DBG map:\n";
	let optype = ultim_tag optv allInfo in
	let optype = reason_itlist optype [edge] typedef_map struct_map in
      (*P.printf "%s\n" (str_of_itlist optype)*)
	check_tblent_itlist typedef_map struct_map optype;
	flush stdout
    ) else (
      from_dbg := true;
      let optype = auxtype in
      check_funcptr_itlist typedef_map struct_map optype;
      flush stdout
    )
  ) ij_ops
    
let dump_checking_stat () =
  P.printf "ICALL checked: %d\n" !ic_checked;
  P.printf "ICALL over-apprx: %d\n" !ic_over;
  P.printf "ICALL error: %d\n" !ic_wrong;
  P.printf "ICALL accept: %d\n" !ic_accept;
  P.printf " with multi: %d\n" !ic_multi;
  P.printf " from DBG: %d\n" !ic_acpt_dbg;
  P.printf " from INF: %d\n" !ic_acpt_inf;
  P.printf "ICALL decline: %d\n" !ic_decline;
  P.printf " due to DBG: %d\n" !ic_dcln_dbg;
  P.printf " due to INF: %d\n" !ic_dcln_inf;

  P.printf "IJUMP checked: %d\n" !ij_checked;
  P.printf "IJUMP over-apprx: %d\n" !ij_over;
  P.printf "IJUMP error: %d\n" !ij_wrong;
  P.printf "IJUMP accept: %d\n" !ij_accept;
  P.printf " with multi: %d\n" !ij_multi;
  P.printf " as TAIL-CALL: %d\n" !ij_tailcall;
  P.printf "  from DBG: %d\n" !ij_tc_dbg;
  P.printf "  from INF: %d\n" !ij_tc_inf; 
  P.printf " as TABLE-JMP: %d\n" !ij_tablejmp;
  P.printf "IJUMP decline: %d\n" !ij_decline;
  P.printf " due to DBG: %d\n" !ij_dcln_dbg;
  P.printf " due to INF: %d\n" !ij_dcln_inf

