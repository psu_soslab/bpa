open Batteries
module P = Printf
module H = Hashtbl

open X86Syntax
open X86Semantics
open X86_MACHINE
open X86_RTL
open Config
(*open Abbrev*)

open BB
open Ibaux
open TyInfoRetrieval
open TypeEngineLib

(** EQCG generation **)

let instr_eqcgGen l_instr funCFA dbgTypeInfo eqcg track_worth crossbb =
  let l, (pre,ins,len) = l_instr in
  
  let dbgtag tv =
    (*ubti_tag tv dbgTypeInfo*)
    (*effi_tag tv dbgTypeInfo*)
    ultim_tag tv dbgTypeInfo
    (*dbgtype_tag tv dbgTypeInfo*)
  in
  
  let consrv = false in

  let inner_eqcg_merge eqcg tv1 tv1_tag tv2 tv2_tag =
    if consrv then
      eqcg_merge_consrv eqcg tv1 tv1_tag tv2 tv2_tag
    else
      eqcg_merge eqcg tv1 tv1_tag tv2 tv2_tag
  in

  let mktv tvnm l crossbb =
    if crossbb then
      mkTV tvnm (l-len) true
    else
      mkTV tvnm l false
  in

  let add_trivial donthandle doconnect =
    List.iter (fun tvnm ->
      if not (List.mem tvnm donthandle) then
	let tv1 = mktv tvnm (l+len) crossbb in
	let tv1_tag = dbgtag tv1 in
	let tv2 = mktv tvnm l false in
	let tv2_tag = dbgtag tv2 in
	if not (List.mem tvnm doconnect) then
	  inner_eqcg_merge eqcg tv1 tv1_tag tv2 tv2_tag
	else
	  eqcg_connect eqcg tv1 tv1_tag [(tv2,tv2_tag,(0,"0"))]
    ) track_worth
  in

  let add_trivial_reg donthandle doconnect =
    List.iter (fun tvnm ->
      match tvnm with
      | TV_reg _ ->
	if not (List.mem tvnm donthandle) then
	  let tv1 = mktv tvnm (l+len) crossbb in
	  let tv1_tag = dbgtag tv1 in
	  let tv2 = mktv tvnm l false in
	  let tv2_tag = dbgtag tv2 in
	  if not (List.mem tvnm doconnect) then
	    inner_eqcg_merge eqcg tv1 tv1_tag tv2 tv2_tag
	  else
	    eqcg_connect eqcg tv1 tv1_tag [(tv2,tv2_tag,(0,"0"))]
      | _ ->
	()
    ) track_worth
  in

  let deref_assign opd ops = (** opd:dest; ops:src **)
    let tvnmd, edged = tvNameGen funCFA opd (l+len) in
    match tvnmd with
    | TV_imm _ ->
      (** Impossible **)
      assert false
    | TV_addr _ ->
      (** Untrackable **)
      add_trivial [] []
    | TV_reg _ 
    | TV_cfa _ -> (** destination is meaningful **)
      if fst edged <> 0 then
	add_trivial [] []
      else if snd edged = "some" then
	add_trivial [] []
      (*add_trivial_reg [] []*)
      else
	let tvd = mktv tvnmd (l+len) crossbb in
	let tvd_tag = dbgtag tvd in
	let tvnms, edges = tvNameGen funCFA ops l in
	let tvs = mktv tvnms l false in
	begin match tvnms with
	| TV_imm _ 
	| TV_reg _ ->
	  let tvs_tag = dbgtag tvs in
	  if fst edges = 0 then
	    eqcg_merge eqcg tvd tvd_tag tvs tvs_tag
	  else 
	    eqcg_connect eqcg tvd tvd_tag [(tvs,tvs_tag,edges)];
	  add_trivial [tvnmd] []
	| TV_cfa _ ->
	  assert (fst edges = 0);
	  let tvs_tag = dbgtag tvs in
	  if snd edges <> "some" then (
	    assert( snd edges = "0" );
	    eqcg_merge eqcg tvd tvd_tag tvs tvs_tag
	  )
	  else
	    eqcg_connect eqcg tvd tvd_tag [tvs,tvs_tag,edges];
	  add_trivial [tvnmd] []
	| TV_addr _ ->
	  assert (fst edges = 1);
	  let tvs_tag = dbgtag tvs in
	  eqcg_connect eqcg tvd tvd_tag [tvs,tvs_tag,edges];
	  add_trivial [tvnmd] [] 
	end
  in
  
  let rule_lea_consrv ops opd =
    let tvnmopd,_ = tvNameGen funCFA opd (l+len) in
    add_trivial [tvnmopd] []
  in

  let rule_lea ops opd =
    let tvnmd,edged = tvNameGen funCFA opd (l+len) in
    match tvnmd with
    | TV_imm _ ->
      (** Impossible **)
      assert false
    | TV_addr _ -> (** impossible and meaningless cases **)
      add_trivial [] []
    | TV_cfa _     
    | TV_reg _ ->
      if fst edged <> 0 then
	add_trivial [] []
      else if snd edged = "some" then
	add_trivial [] []
      (*add_trivial_reg [] []*)
      else
	let tvd = mktv tvnmd (l+len) crossbb in (** edged should be 0**)
	let tvd_tag = dbgtag tvd in
	let tvnms, edges = tvNameGen funCFA ops l in
	let tvs = mktv tvnms l false in
	begin match tvnms with
	| TV_imm _ ->
	  assert false      
	| TV_reg _ -> 
	  let tvs_tag = dbgtag tvs in
	  assert (fst edges = 1);
	  if snd edges = "0" then
	    eqcg_merge eqcg tvd tvd_tag tvs tvs_tag
	  else
	    eqcg_connect eqcg tvd tvd_tag [tvs,tvs_tag,(0,snd edges)];
	  add_trivial [tvnmd] []
	| TV_cfa _ ->
	  assert (fst edges = 0);
	  let tvs_tag = dbgtag tvs in
	  eqcg_connect eqcg tvd tvd_tag [tvs,tvs_tag,(2,snd edges)];
	  add_trivial [tvnmd] []
	| TV_addr _ ->
	  assert (fst edges = 1);
	  let tvs_tag = dbgtag tvs in
	  if snd edges = "0" then
	    eqcg_merge eqcg tvd tvd_tag tvs tvs_tag
	  else
	    eqcg_connect eqcg tvd tvd_tag [tvs,tvs_tag,(0,snd edges)];
	  add_trivial [tvnmd] []
	end
  in

  let rule_push width op =
    let currstack = Address_op 
      {addrDisp=Big.zero; addrBase=Some ESP; addrIndex=None;}
    in
    deref_assign currstack op;
  in

  let rule_mov width ops opd =
    deref_assign opd ops
  in

  let rule_pop op =
    let currstack = Address_op
      {addrDisp = Big.zero; addrBase = Some ESP; addrIndex=None;}
    in
    deref_assign op currstack
  in
  
  let rule_leave () =
    let opEBP = Reg_op EBP in
    let opESP = Reg_op ESP in
    let tvEBP_name, edgeEBP = tvNameGen funCFA opEBP l in
    let tvESP_name, edgeESP = tvNameGen funCFA opESP (l+len) in
    let tvEBP = mktv tvEBP_name l false in
    let tvESP = mktv tvESP_name (l+len) crossbb in
    let tvEBP_tag = dbgtag tvEBP in
    let tvESP_tag = dbgtag tvESP in
    inner_eqcg_merge eqcg tvESP tvESP_tag tvEBP tvEBP_tag;

    let currstack = Address_op
      {addrDisp=Big.zero; addrBase=Some ESP; addrIndex=None;}
    in
    let tv1_name = tvEBP_name in
    let tv1= mktv tv1_name (l+len) crossbb in
    let tv2_name,_ = tvNameGen funCFA currstack (l+len) in
    let tv2 = mktv tv2_name l false in
    let tv1_tag = dbgtag tv1 in
    let tv2_tag = dbgtag tv2 in
    eqcg_merge eqcg tv1 tv1_tag tv2 tv2_tag;
    add_trivial [tvESP_name;tvEBP_name] []
  in

  let rule_cmov ops opd =
    let tvnmd,edged = tvNameGen funCFA opd (l+len) in
    match tvnmd with
    | TV_imm _ ->
      (** Impossible **)
      assert false
    | TV_addr _ -> 
      (** Untrackable **)
      add_trivial [] []
    | TV_reg _ 
    | _ -> (** destination is meaningful **)
      if fst edged <> 0 then
	add_trivial [] []
      else if snd edged = "some" then
	add_trivial [] []
      (*add_trivial_reg [] []*)
      else
	let tvd = mktv tvnmd (l+len) crossbb in
	let tvd_tag = dbgtag tvd in
	let tvnms, edges = tvNameGen funCFA ops l in
	let tvs = mktv tvnms l false in
	begin match tvnms with
	| TV_imm _
	| TV_reg _ ->
	  let tvs_tag = dbgtag tvs in
	  eqcg_connect eqcg tvd tvd_tag [(tvs,tvs_tag,edges)];
	  add_trivial [] [tvnmd]
	| TV_cfa _->
	  assert (fst edges = 0);
	  let tvs_tag = dbgtag tvs in
	  eqcg_connect eqcg tvd tvd_tag [tvs,tvs_tag,edges];
	  add_trivial [] [tvnmd]
	| TV_addr _ ->
	  assert (fst edges = 1);
	  let tvs_tag = dbgtag tvs in
	  eqcg_connect eqcg tvd tvd_tag [tvs,tvs_tag,edges];
	  add_trivial [] [tvnmd] 
	end
  in

  let rule_arith width ops opd =
    rule_cmov ops opd
  in
  
  let rule_imul width ops_opt opv_opt opd =
    if ops_opt = None then
      add_trivial [] []
    else
      rule_cmov (Option.get ops_opt) opd
  in

  let rule_xchg op1 op2 =
    let tvnm1,edge1 = tvNameGen funCFA op1 l in
    let tvnm2,edge2 = tvNameGen funCFA op2 l in
    
    if tvnm1 = tvnm2 then add_trivial [] []      
    else (
      match tvnm1, tvnm2 with
      | (TV_imm _ , _) | (_, TV_imm _) | (TV_addr _, TV_addr _)-> 
	(** impossible and meaningless cases **)
	assert false
      | TV_addr _, TV_reg _ ->
	deref_assign op2 op1
      | TV_reg _, TV_addr _ ->
	deref_assign op1 op2
      | _ ->
	if fst edge1 <> 0 || snd edge1 = "some"then
	  deref_assign op2 op1
	else if fst edge2 <> 0 || snd edge2 = "some"then
	  deref_assign op1 op2
	else
	  let tv11 = mktv tvnm1 l false in
	  let tv12 = mktv tvnm1 (l+len) crossbb in
	  let tv21 = mktv tvnm2 l false in
	  let tv22 = mktv tvnm2 (l+len) crossbb in
	  let tv11_tag = dbgtag tv11 in
	  let tv12_tag = dbgtag tv12 in
	  let tv21_tag = dbgtag tv21 in
	  let tv22_tag = dbgtag tv22 in
	  inner_eqcg_merge eqcg tv11 tv11_tag tv22 tv22_tag;
	  inner_eqcg_merge eqcg tv21 tv21_tag tv12 tv12_tag;
	  add_trivial [tvnm1; tvnm2] []
    )
  in

  let rule_call dis abs op slctr =
    (** assume return value is always stored in EAX **)
    (** TODO: read calling convention to make sure **)
    let rec find_rettype typesig_ptr =
      match typesig_ptr with
      | C_subroutine (rettype, _) -> rettype
      | C_pointer (_,sigtype) -> find_rettype sigtype
      | _ -> assert false
    in
    let rec equiv_to_void ctype =
      match ctype with
      | C_void -> true
      | C_const ct | C_volatile ct | C_typedef (_,ct) ->
	equiv_to_void ct
      | _ -> false
    in
    let _,_,_,funsig_map = dbgTypeInfo in
    (** deal with function return **)
    match op with
    | Imm_op imm ->
      let abs_callee_addrstr = 
	if abs then
	  str_of_mword_flex (MWord.of_big_int (Abbrev.unsigned32 imm))
	else
	  let rel_addr = l + len + Big.to_int (Abbrev.signed32 imm) in
	  str_of_mword_flex (MWord.(+%) (MWord.of_int rel_addr) (MWord.of_string "0x8048000"))
      in
      if H.mem funsig_map abs_callee_addrstr then
	let callee_typesig_ptr = H.find funsig_map abs_callee_addrstr in
	let rettype = find_rettype callee_typesig_ptr in
	if equiv_to_void rettype then
	  add_trivial [] []
	else (
	  (** try to track return type; easy over-approximation **)
	  let tvnm = TV_reg EAX in
	  add_trivial [tvnm] [];
	  let rettv = mktv tvnm (l+len) true in
	  let rettv_tag = [IT_elem (rettype,0)] in
	  eqcg_connect eqcg rettv rettv_tag []
	)
      else (
	(*
	P.printf "calling no-type function %s at %x\n" abs_callee_addrstr l;
	flush stdout;*)
	let tvnm = TV_reg EAX in
	add_trivial [tvnm] []
      )
    | _ ->
      (** deliberately add a node for call operand **)
      let tvnmd, edged = tvNameGen funCFA op l in
      (*
      let tvnmd_aux = 
	begin match tvnmd with
	| TV_reg reg ->
	  if fst edged = 1 && snd edged <> "some" then (
	    TV_addr {disp=(snd edged); base=Some reg; index=None}
	  ) else tvnmd
	| _ -> tvnmd
	end
      in
      let tvdaux = mktv tvnmd_aux l false in
      let tvdaux_tag = dbgtag tvdaux in
      if not (List.is_empty tvdaux_tag) then (
	P.printf "Adding aux dbg-info to call operand...\n";
	P.printf "TV: %s has tag\n%s\n" (str_of_tv tvdaux) (str_of_itlist tvdaux_tag);
	flush stdout;
      ) else ();
      *)
      let tvd = mktv tvnmd l false in
      let tvd_tag = dbgtag tvd in
      (*
      P.printf "Adding dbg-info to call operand...\n";
      P.printf "TV: %s has tag\n%s\n" (str_of_tv tvd) (str_of_itlist tvd_tag);
      flush stdout;
      *)
      eqcg_connect eqcg tvd tvd_tag [];
      let tvnm = TV_reg EAX in
      add_trivial [tvnm] []	
  in

  let rule_stos () =
    let tvnm = TV_reg EAX in
    add_trivial [tvnm] []
  in

  let handle_boolop opd =
    let tvnm,_ = tvNameGen funCFA opd (l+len) in
    add_trivial [tvnm] []
  in  
  
  let rule_xor ops opd =
    match ops,opd with
    | Reg_op regs, Reg_op regd ->
      if regs = regd then
	handle_boolop opd
      else
	add_trivial [] []
    | _ -> 
      add_trivial [] []
  in

  let rule_and ops opd =
    handle_boolop opd
  in

  let rule_or ops opd =
    handle_boolop opd
  in

  let rule_neg opd =
    handle_boolop opd
  in

  let rule_sar ops opd =
    handle_boolop opd
  in

  let rule_shl ops opd =
    handle_boolop opd
  in
  
  let rule_shr ops opd =
    handle_boolop opd
  in

  let rule_setcc opd =
    let tvnmopd,_ = tvNameGen funCFA opd (l+len) in
    add_trivial [tvnmopd] []
  in
  
  let rule_cdq () =
    let tvnm = TV_reg EDX in
    add_trivial [tvnm] []
  in

  let rule_bswap reg =
    let tvnm = TV_reg reg in
    add_trivial [tvnm] []
  in

  let rule_shld reg =
    let tvnm = TV_reg reg in
    add_trivial [tvnm] []
  in

  let rule_shrd reg =
    let tvnm = TV_reg reg in
    add_trivial [tvnm] []
  in

  if is_trivial ins then
    add_trivial [] []
  else if is_evil ins then
    ()
  else begin match ins with
  | PUSH(width,op) -> rule_push width op
  | MOV(width,opd,ops) -> rule_mov width ops opd
  | MOVZX(width,opd,ops) -> rule_mov width ops opd
  | MOVSX(width,opd,ops) -> rule_mov width ops opd
  | ADD(width,opd,ops)
  | SUB(width,opd,ops)
  | SBB(width,opd,ops)
  | ADC(width,opd,ops) -> rule_arith width ops opd
  | IMUL(width,opd,ops_opt,opv_opt) -> rule_imul width ops_opt opv_opt opd
  | POP(op) -> rule_pop op
  | LEAVE -> rule_leave ()
  | LEA(opd,ops) -> 
    (*rule_lea_consrv ops opd*)
    rule_lea ops opd
  | CMOVcc(cond,opd,ops) -> rule_cmov ops opd
  | CALL(dis,abs,op,slctr) -> rule_call dis abs op slctr
  | XOR(width,opd,ops) -> rule_xor ops opd
  | AND(width,opd,ops) -> rule_and ops opd
  | OR(width,opd,ops) -> rule_or ops opd
  | NEG(width,opd) -> rule_neg opd
  | NOT(width,opd) -> handle_boolop opd
  | SHL(width,opd,ops) -> rule_shl ops opd
  | SHLD(width,reg,regimm) -> rule_shld reg
  | SHR(width,opd,ops) -> rule_shr ops opd
  | SHRD(width,reg,regimm) -> rule_shrd reg
  | SAR(width,opd,ops) -> rule_sar ops opd
  | ROL(width,opd,ops) -> handle_boolop opd
  | ROR(width,opd,ops) -> handle_boolop opd
  | STOS(width) -> rule_stos ()
  | XCHG(width,opd,ops) -> rule_xchg ops opd
  | SETcc(cond,opd) -> rule_setcc opd
  | CDQ -> rule_cdq ()
  | BSWAP reg -> rule_bswap reg
  | BSR(opd,ops) -> handle_boolop opd
  | _ -> 
    P.printf "Unsupported instruction at %x: %s\n" l (Instr.str_of_instr (pre,ins)); 
    flush stdout;
  end

let handle_control_flows preds bb funCFA dbgTypeInfo eqcg track_worth =
  List.iter (fun tvnm ->
    let tv1_ndx = bb.bb_relAddr in
    let tv1 = mkTV tvnm tv1_ndx false in
    let tv1_tag = ultim_tag tv1 dbgTypeInfo in
    (*let tv1_tag = dbgtype_tag tv1 dbgTypeInfo in*)
    let tagged_tv_list = List.map (fun pred_bb ->
      let _,_,len = List.last pred_bb.bb_instrs in
      let end_addr = pred_bb.bb_relAddr + pred_bb.bb_size - len in
      let tv = mkTV tvnm end_addr true in
      let tv_tag = ultim_tag tv dbgTypeInfo in
      (*let tv_tag = dbgtype_tag tv dbgTypeInfo in*)
      tv, tv_tag, (0,"0")
    ) preds
    in
    eqcg_connect eqcg tv1 tv1_tag tagged_tv_list
  ) track_worth

let bb_eqcgGen bb funCFA dbgTypeInfo eqcg track_worth =
  let addrcount = ref bb.bb_relAddr in
  let l_instr_list = 
    List.map (fun (pre,ins,len) -> 
      let outaddr = !addrcount in
      addrcount := len + !addrcount;
      outaddr, (pre,ins,len)
    ) bb.bb_instrs 
  in
  List.iteri (fun ndx l_instr ->
    let crossbb = ((ndx + 1) = List.length l_instr_list) in
    instr_eqcgGen l_instr funCFA dbgTypeInfo eqcg track_worth crossbb
  ) l_instr_list

let func_eqcgGen asm_fun funCFA dbgTypeInfo eqcg =
  let consrv = false in
  
  let track_worth =
    if consrv then    
      consrv_worth_tracking funCFA asm_fun
    else 
      aggres_worth_tracking funCFA asm_fun
  in
  List.iter (fun bb ->
    let possible_preds = 
      List.filter_map (fun bb' ->
	if does_bb_call bb' then
	  if List.mem bb.bb_label bb'.bb_inter_succs then
	    Some bb'
	  else
	    None
	else
	  if List.mem bb.bb_label bb'.bb_succs then
	    Some bb'
	  else
	    None
      ) asm_fun.func_bbs
    in
    bb_eqcgGen bb funCFA dbgTypeInfo eqcg track_worth;
    handle_control_flows possible_preds bb funCFA dbgTypeInfo eqcg track_worth;
  ) asm_fun.func_bbs


(** Solver **)
let visited_eqcs = ref []

let rec propagate_top_fast typedef_map struct_map eqcg start_eqc start_itlist =
  let next_eqcs =  
    List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) start_eqc.eqc_succs)
  in
  visited_eqcs := start_eqc :: !visited_eqcs;
  List.iter (fun (eqc,edge) ->
    if not (List.mem eqc !visited_eqcs) then (
      if List.is_empty eqc.eqc_dbgtype then (
	let processed_itlist =
	  start_itlist 
	in
	if not (sub_itlist typedef_map struct_map processed_itlist eqc.eqc_fflowtype)
	then (
	  eqc.eqc_fflowtype <- processed_itlist @ eqc.eqc_fflowtype;
	  propagate_top_fast typedef_map struct_map eqcg eqc processed_itlist
	)
      )
    ) else ()
  ) next_eqcs

let type_solve_full_wl typedef_map struct_map ultim_map eqcg =
  let direction = true in
  let next eqc = 
    if direction then 
      List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) eqc.eqc_succs)
    else
      List.unique (List.map (fun (tv,edge) -> (eqcg_find eqcg tv, edge)) eqc.eqc_preds)
  in
  let _, nodes = eqcg in
  let workq = Queue.create () in
  H.iter (fun repr eqc ->
    if not (List.is_empty eqc.eqc_dbgtype) then
      Queue.add eqc workq
    else if List.is_empty eqc.eqc_preds then (
      let trivial_b = List.exists (fun tv ->
	match tv.tv_name with
	| TV_imm _ -> true
	| TV_addr _ -> true
	| _ -> false
      ) (TypeVarSet.elements eqc.eqc_vars) 
      in 
      if trivial_b then (
	eqc.eqc_fflowtype <- [IT_elem (C_base (4,"int"), 0)];
	Queue.add eqc workq
      ) else (
	eqc.eqc_fflowtype <- [IT_top 1];
	visited_eqcs := [];
	propagate_top_fast typedef_map struct_map eqcg eqc [IT_top 1]
      (*Queue.add eqc workq*)
      )
    )    
    else ()
  ) nodes;
  while (not (Queue.is_empty workq)) do
    let verbose = false in
    if verbose then
      (P.printf "TASKS remaining: %d\n" (Queue.length workq); flush stdout);
    let task_eqc = Queue.take workq in
    let prop_type = 
      if List.is_empty task_eqc.eqc_dbgtype then
	task_eqc.eqc_fflowtype
      else
	task_eqc.eqc_dbgtype
    in
    assert (not (List.is_empty prop_type));
    List.iter (fun (eqc,edge) ->
      if List.is_empty eqc.eqc_dbgtype then
	let processed_itlist = 
	  if fst edge = 1 && snd edge <> "some" then
	    try 
	      List.find_map (fun tv ->
		begin match tv.tv_name with
		| TV_reg reg ->
		  let off_dec = int_of_32string (snd edge) in
		  let off_dec_str = string_of_int off_dec in
		  let auxtvnm = TV_addr (mkADNM off_dec_str (Some reg) None) in
		  let auxtv = mkTV auxtvnm tv.tv_index tv.tv_crossbb in
		  let auxtype = ultim_tag auxtv ultim_map in
		  if List.is_empty auxtype then None
		  else Some auxtype
		| _ -> None
		end
	      ) (TypeVarSet.elements task_eqc.eqc_vars) 
	    with Not_found ->
	      reason_itlist prop_type [edge] typedef_map struct_map 
	  else
	    reason_itlist prop_type [edge] typedef_map struct_map 
	in
	(*
	let processed_itlist = 
	  reason_itlist prop_type [edge] typedef_map struct_map
	in
	*)
	(*
	  try
	  reason_itlist prop_type [edge] typedef_map struct_map 
	  with Failure errmsg ->
	  P.printf "%s" errmsg;
	  P.printf "at eqc: %s\n" eqc.eqc_name;
	  P.printf "aiming with edge %s at %s\n" 
	  (str_of_edge edge) (str_of_itlist prop_type);
	  flush stdout;
	  []
	*)
	if List.is_empty processed_itlist ||
	  sub_itlist typedef_map struct_map processed_itlist eqc.eqc_fflowtype then
	  ()
	else (
	  if verbose then (
	    P.printf "comparing types:\n%s\n%s\n" 
	      (str_of_itlist processed_itlist)
	      (str_of_itlist eqc.eqc_fflowtype);
	    flush stdout
	  );
	  eqc.eqc_fflowtype <- 
	    List.fold_left (fun retit it -> 
	      if List.exists (sub_it typedef_map struct_map it) retit then retit
	      else it :: retit
	    ) eqc.eqc_fflowtype processed_itlist;
	  Queue.add eqc workq
	)
      else
	()
    ) (next task_eqc)
  done


    
