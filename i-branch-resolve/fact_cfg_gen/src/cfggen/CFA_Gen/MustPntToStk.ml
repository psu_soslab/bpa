open Batteries
module P = Printf
module H = Hashtbl

open X86Syntax
open X86Semantics
open X86_RTL
open Config
open Abbrev

open BB
open Ibaux
open RTL_opt

open PntToStkLib
    
type task_type = {
  t_curr: string;
  t_prev: string;
}
  
let mkTask curr prev = {
  t_curr = curr;
  t_prev = prev;
}
                     
let patch_for_must_analysis asm_fun bb_instrs =
  List.map (fun (pre,instr,len) -> 
    match instr with
    | CALL(_,_,_,_) -> 
      (pre, MOV (true, (Reg_op EAX), (Imm_op (Big.of_int 0))),len)
    | AND(_,op1,op2) ->
      if asm_fun.func_name = "main" then
	begin match op1, op2 with
	| Reg_op reg, Imm_op v 
	| Imm_op v, Reg_op reg ->
	  if reg = ESP && (unsigned32_to_hex v) = "0xfffffff0" then
	    (pre,NOP (Imm_op (Big.of_int 0)),len)
	  else
	    pre,instr,len
	| _, _ -> pre,instr,len
	end
      else
	(pre,instr,len)
    | _ -> (pre,instr,len)
  ) bb_instrs

(** Translate operands to alocs **)
let loc_to_aloc s loc =
  let size = (Big.to_int s) + 1 in
  match loc with
  | X86_MACHINE.Coq_reg_loc reg ->
    if size = 8 || size = 16 then (
      P.printf "Warning: partial access!\n";
      [mkAloc (AR_Reg reg) 0 None 4 0]
    )
    else if size = 32 then
      [mkAloc (AR_Reg reg) 0 None 4 0]
    else assert false
  | _ -> []

let psreg_to_aloc psreg ai fun_name read =
  let mem_size = mem_size_of_ai ai read in
  let psregid = Big.to_int psreg in
  let psreg_aloc = mkAloc (AR_Psreg (fun_name,psregid))
    0 None mem_size 0
  in
  [psreg_aloc]

let addr_to_aloc addr_re ai vsettbl fname read = 
  let mem_size = mem_size_of_ai ai read in
  let seg_opt = match (fst ai).seg_override with
    | None -> Some DS
    | Some segreg -> Some segreg
  in
  let addr_mode = addr_mode_from_addr_re addr_re ai in
  let alocs_generated = match addr_mode with
    | {addrDisp = disp; addrBase = base_opt; addrIndex = _} ->
      let int_of_disp = big32_to_int disp in
      let accessed_galoc =
	mkAloc (AR_Global seg_opt) int_of_disp None mem_size 0
      in
      let related_galocs = ref [] in
      H.iter (fun aloc _ ->
	if aloc.aloc_region = (AR_Global seg_opt) then
	  let aloc_start = aloc.aloc_offset in
	  let aloc_end = aloc_start + aloc.aloc_size in
	  let disjoint = 
	    aloc_start >= int_of_disp + mem_size 
	    || aloc_end <= int_of_disp
	  in
	  if not disjoint then
	    related_galocs := aloc :: !related_galocs
      ) vsettbl;
      let possible_global_aloc =
	if List.is_empty !related_galocs then [accessed_galoc]
	else if List.length !related_galocs = 1 then
	  if !related_galocs = [accessed_galoc] then [accessed_galoc]
	  else []
	else (
	  P.printf "Problematic instruction:\n%s\n"
	    (Instr.str_of_instr ai);
	  P.printf "RevAddrMode:\n%s\n" (Instr.str_of_addr addr_mode);
	  P.printf "Reverse RE:\n%s\n" 
	    (Printer.str_of_rtl_exp_less_size addr_re);
	  P.printf "Related global alocs:\n";
	  List.iter (fun laloc ->
	    P.printf "%s\n" (str_of_aloc laloc)
	  ) !related_galocs;
	  dump_vset_table vsettbl;
	  []
	)
      in
      if base_opt = None then possible_global_aloc
      else (
	let reg = Option.get base_opt in
	let reg_aloc = mkAloc (AR_Reg reg) 0 None 4 0 in
	if H.mem vsettbl reg_aloc then
	  let cfa_reprs_of_base_reg =
	    H.find vsettbl reg_aloc
	  in
	  let offsets =
	    List.filter_map offset_in_cfarepr cfa_reprs_of_base_reg
	  in
	  let new_offsets =
	    List.map (fun off ->
	      off - int_of_disp - mem_size
	    ) offsets
	  in
	  assert (List.length new_offsets <= 1);
	  if new_offsets = [] then []
	  else (
	    let new_offset = List.hd new_offsets in
	    let accessed_laloc =
	      mkAloc (AR_Local fname) new_offset None mem_size 0
	    in
	    let related_lalocs = ref [] in
	    H.iter (fun aloc _ ->
	      if aloc.aloc_region = (AR_Local fname) then
		let aloc_start = aloc.aloc_offset in
		let aloc_end = aloc_start + aloc.aloc_size in
		let disjoint = 
		  aloc_start >= new_offset + mem_size 
		  || aloc_end <= new_offset
		in
		if not disjoint then
		  related_lalocs := aloc :: !related_lalocs
	    ) vsettbl;
	    let possible_local_aloc =
	      if List.is_empty !related_lalocs then [accessed_laloc]
	      else if List.length !related_lalocs = 1 then
		if !related_lalocs = [accessed_laloc] then [accessed_laloc]
		else []
	      else (
		P.printf "Problematic instruction in function %s:\n%s\n" fname
		  (Instr.str_of_instr ai);
		P.printf "RevAddrMode:\n%s\n" (Instr.str_of_addr addr_mode);
		P.printf "Reverse RE:\n%s\n" 
		  (Printer.str_of_rtl_exp_less_size addr_re);
		P.printf "Accessed local aloc:\n%s\n"
		  (str_of_aloc accessed_laloc);
		P.printf "Related local alocs:\n";
		List.iter (fun laloc ->
		  P.printf "%s\n" (str_of_aloc laloc)
		) !related_lalocs;
		dump_vset_table vsettbl;
		[]
	      )
	    in
	    possible_local_aloc
	  ) else []
      )
  in
  if List.is_empty alocs_generated then
    (**Warning: Treat empty alocs as the same storage location**)
    [unknownAloc]
  else (
    assert (List.length alocs_generated = 1);
    alocs_generated
  )

let rec apply_vsettbl vsettbl re ai fname = 
  match re with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    let res1 = apply_vsettbl vsettbl re1 ai fname in
    let res2 = apply_vsettbl vsettbl re2 ai fname in
    List.flatten (List.map (fun new_re1 ->
      List.map (fun new_re2 ->
	Coq_arith_rtl_exp(s,bvop,new_re1,new_re2)	
      ) res2
    ) res1)
  | Coq_test_rtl_exp(s,top,re1,re2) ->
    let res1 = apply_vsettbl vsettbl re1 ai fname in
    let res2 = apply_vsettbl vsettbl re2 ai fname in
    List.flatten (List.map (fun new_re1 ->
      List.map (fun new_re2 ->
	Coq_test_rtl_exp(s,top,new_re1,new_re2)
      ) res2
    ) res1)
  | Coq_if_rtl_exp(s, ire, tre, ere) ->
    let new_ires = apply_vsettbl vsettbl ire ai fname in
    let new_tres = apply_vsettbl vsettbl tre ai fname in
    let new_eres = apply_vsettbl vsettbl ere ai fname in
    List.flatten (List.map (fun new_ire ->
      List.flatten (List.map (fun new_tre ->
	List.map (fun new_ere ->
	  Coq_if_rtl_exp(s, new_ire, new_tre, new_ere)
	) new_eres
      ) new_tres)
    ) new_ires)	      
  | Coq_cast_s_rtl_exp(s1,s2,re) ->
    List.map (fun new_re ->
      Coq_cast_s_rtl_exp(s1,s2,new_re)
    ) (apply_vsettbl vsettbl re ai fname)
  | Coq_cast_u_rtl_exp(s1,s2,re) ->
    List.map (fun new_re ->
      Coq_cast_u_rtl_exp(s1,s2,new_re)
    ) (apply_vsettbl vsettbl re ai fname)
  | Coq_imm_rtl_exp(s,v) ->
    [re]
  | Coq_get_loc_rtl_exp(s,loc) ->
    begin match loc with
    | X86_MACHINE.Coq_reg_loc(reg) ->
      assert((Big.to_int s) = 31);
      begin try H.find vsettbl (mkAloc (AR_Reg reg) 0 None 4 0)
	with Not_found -> [re]
      end
    | _ -> [re] 
    end
  | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    [re]
  | Coq_get_array_rtl_exp(l,s,arr,index_re) ->
    List.map (fun new_re ->
      Coq_get_array_rtl_exp(l,s,arr,new_re)
    ) (apply_vsettbl vsettbl index_re ai fname)
  | Coq_get_byte_rtl_exp(re') ->
    let target_alocs = addr_to_aloc re' ai vsettbl fname true in
    if target_alocs = [] then [re]
    else  
      List.flatten (List.map (fun target_aloc -> 
	try H.find vsettbl target_aloc
	with Not_found -> [re]
      ) target_alocs)
  | Coq_get_random_rtl_exp(s) ->
    [re]
  | _ ->
    assert false

let rec cfa_must_in_re re = 
  match re with
  | Coq_arith_rtl_exp(s,_,re1,re2)
  | Coq_test_rtl_exp(s,_,re1,re2) ->
    cfa_must_in_re re1 || cfa_must_in_re re2
  | Coq_if_rtl_exp(s, ire, tre, ere) ->
    false
  | Coq_cast_s_rtl_exp(s1,s2,re)    
  | Coq_cast_u_rtl_exp(s1,s2,re) ->
    cfa_must_in_re re
  | Coq_imm_rtl_exp(s,v) ->
    false
  | Coq_get_loc_rtl_exp(s,loc) ->
    false
  | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    Big.eq ps_reg (Big.of_int (-1))
  | Coq_get_array_rtl_exp(l,s,arr,re) ->
    false
  | Coq_get_byte_rtl_exp(re) ->
    false
  | Coq_get_random_rtl_exp(s) ->
    false
  | _ ->
    assert false

let cfa_repr_of_re re vsettbl ai fname =
  (*P.printf "Before transformation:\n%s\n" (Printer.str_of_rtl_exp_less_size re);*)
  let no_cast_re = prune_cast re in
  let transformed_res = apply_vsettbl vsettbl no_cast_re ai fname in
  let evaluated_res =
    List.map (fun transformed_re ->
      (**TODO: implement new expression folding**)
      (*P.printf "Before evaluation:\n%s\n" (Printer.str_of_rtl_exp_less_size transformed_re);*) 
      let evaluated_re = simp_re [constant_fold] transformed_re in
      (*P.printf "After evaluation:\n%s\n" (Printer.str_of_rtl_exp_less_size evaluated_re);*)
      evaluated_re
(*      (simp_re simpre_all_methods transformed_re)*)
    ) transformed_res
  in
  let cfa_in_res =
    List.filter cfa_must_in_re evaluated_res
  in
  List.unique ?eq:(Some eq_rtl_exp) cfa_in_res

let must_PntToStk asm_fun = 
  let pvs_tbl = H.create 32 in  

  let bb_map = H.create 32 in
  List.iter (fun bb ->
    H.add bb_map bb.bb_label bb;
  ) asm_fun.func_bbs;

  let in_vsettbl_map = H.create 32 in
  let out_vsettbl_map = H.create 32 in
  List.iter (fun bb ->
    let empty_in_vsettbl = H.create 32 in
    H.add in_vsettbl_map bb.bb_label empty_in_vsettbl;
    let empty_out_vsettbl = H.create 32 in
    H.add out_vsettbl_map bb.bb_label empty_out_vsettbl
  ) asm_fun.func_bbs;

  let cfa_pseudo_reg =
    Coq_get_ps_reg_rtl_exp (Big.of_int 31, Big.of_int (-1))
  in
  
  let esp_aloc = mkAloc (AR_Reg ESP) 0 None 4 0 in
  
  let initial_esp_vset =
    [rtl_sub 32 cfa_pseudo_reg (rtl_imm 32 4)]
  in

  let entry_bb_label =
    (List.hd asm_fun.func_bbs).bb_label
  in
  if entry_bb_label <> asm_fun.func_low then
    P.printf "EntBBLbl:%s v.s. FunLow:%s \n" entry_bb_label asm_fun.func_low;
  flush stdout;
  assert (entry_bb_label = asm_fun.func_low);

  H.add (H.find in_vsettbl_map entry_bb_label) esp_aloc initial_esp_vset;

  let rec analyze_ri ri ai vsettbl =
    match ri with
    | Coq_set_loc_rtl(s,re,loc) ->
      let target_alocs = loc_to_aloc s loc in
      let target_vset = 
	cfa_repr_of_re re vsettbl ai asm_fun.func_name 
      in
      if target_alocs <> [] then assert((Big.to_int s) = 31);
      List.iter (fun target_aloc ->
	H.replace vsettbl target_aloc target_vset
      ) target_alocs
    | Coq_set_ps_reg_rtl(s,re,psreg) -> 
      let target_alocs = 
	psreg_to_aloc psreg ai asm_fun.func_name false
      in
      let target_vset = 
	cfa_repr_of_re re vsettbl ai asm_fun.func_name 
      in
      List.iter (fun target_aloc ->
	H.replace vsettbl target_aloc target_vset
      ) target_alocs
    | Coq_set_array_rtl(s1,s2,arr,ire,vre) ->
      soundness_warning "Unsupported set_array...\n"
    | Coq_set_byte_rtl(vre, are) ->
      let target_alocs = 
	addr_to_aloc are ai vsettbl asm_fun.func_name false
      in
      if target_alocs = [] then ()
      else (
	let target_vset = 
	  cfa_repr_of_re vre vsettbl ai asm_fun.func_name 
	in
	List.iter (fun target_aloc ->
	  H.replace vsettbl target_aloc target_vset
	) target_alocs
      )
    | Coq_advance_oracle_rtl -> ()
    | Coq_if_rtl(guard,ri') -> (** Ignore if_rtl **)
      ()
    | Coq_error_rtl ->
       soundness_warning "RTL-Error during MayPntToStk\n"
    | Coq_trap_rtl ->
       soundness_warning "RTL-Trap during MayPntToStk\n"
  in
  
  let analyze_one_step ri_list ai vsettbl =
    let reduced_ri_list =
      reduce_rtl false ri_list []
    in
    List.iter (fun ri ->
      analyze_ri ri ai vsettbl
    ) reduced_ri_list
  in

  (** Use the instructions in taskbb and the in_vsettbl
      to compute a new out_vsettbl;
      if the new and the old are the same then return
      false; else update the out_vsettbl_map and return
      true **)
  let perform_vsa taskbb in_vsettbl out_vsettbl =
    let new_out_vsettbl = H.copy in_vsettbl in
    List.iter (fun (pre,instr,len) ->
      let rtl_instrs = X86_Compile.instr_to_rtl pre instr in
      analyze_one_step rtl_instrs (pre,instr) new_out_vsettbl
    ) (patch_for_must_analysis asm_fun taskbb.bb_instrs);

    let continue_sign = not (eq_vsettbl out_vsettbl new_out_vsettbl) in
    if continue_sign then
      H.replace out_vsettbl_map taskbb.bb_label new_out_vsettbl;
    continue_sign      
  in

  let analyze_bb taskbb task_in_vsettbl = 
    try
      let in_vsettbl = H.find in_vsettbl_map taskbb.bb_label in
      if H.length in_vsettbl = 0 then
	(* Should test if it is the first time of update*)
	H.iter (fun aloc vset ->
	  H.add in_vsettbl aloc vset
	) task_in_vsettbl
      else
	intersect_vsettbl_tofst in_vsettbl task_in_vsettbl;
      let out_vsettbl = 
	H.find out_vsettbl_map taskbb.bb_label 
      in
      let push_new_b =
	perform_vsa taskbb in_vsettbl out_vsettbl
      in
      push_new_b
    with Not_found ->
      assert false
  in

  let workq = Queue.create () in
  
  let add_new_tasks bb =
    if List.is_empty bb.bb_inter_succs then
      List.iter (fun succlbl ->
	if H.mem bb_map succlbl then
	  let newtask = mkTask
	    succlbl bb.bb_label
	  in
	  Queue.push newtask workq
      ) bb.bb_succs
    else 
      List.iter (fun succlbl ->
	if H.mem bb_map succlbl then
	  let newtask = mkTask
	    succlbl bb.bb_label
	  in
	  Queue.push newtask workq
	else (
	  P.printf "%s not found for %s!\n" succlbl bb.bb_label;
	  flush stdout;
	)
      ) bb.bb_inter_succs
  in

  let entry_bb = List.hd asm_fun.func_bbs in
  let push_task_b = 
    analyze_bb entry_bb (H.find in_vsettbl_map entry_bb_label) 
  in
  if push_task_b then add_new_tasks entry_bb;
  while not (Queue.is_empty workq) do
    let task = Queue.take workq in
    let tasklbl = task.t_curr in
    let task_prev = task.t_prev in
    try
      let task_in_vsettbl = H.find out_vsettbl_map task_prev in
      let task_bb = H.find bb_map tasklbl in
      let push_task_b = analyze_bb task_bb task_in_vsettbl in
      if push_task_b then add_new_tasks task_bb
    with Not_found ->
      assert false
  done;

  (** Translate in_vsettbl_map to PVS-Table **)
  H.iter (fun bblbl vsettbl ->
    let bb = H.find bb_map bblbl in
    let address = ref bb.bb_relAddr in
    let target_vsettbl = H.copy vsettbl in
    List.iter (fun (pre,ins,len) ->
      let ai = pre,ins in
      let exepnt = !address, false in
      H.add pvs_tbl exepnt (H.copy target_vsettbl);
      let rtl_instrs = X86_Compile.instr_to_rtl pre ins in
      let reduced_rilist = reduce_rtl false rtl_instrs [] in
      List.iter (fun ri ->
	match ri with
	| Coq_set_loc_rtl(s,re,loc) ->
	  let target_alocs = loc_to_aloc s loc in
	  if target_alocs = [] then ()
	  else (
	    let target_vset = 
	      cfa_repr_of_re re target_vsettbl ai asm_fun.func_name 
	    in
	    assert((Big.to_int s) = 31);
	    List.iter (fun target_aloc -> 
	      H.replace target_vsettbl target_aloc
		target_vset
	    ) target_alocs
	  )
	| Coq_set_byte_rtl(vre, are) ->
	  let target_alocs = addr_to_aloc are ai
	    target_vsettbl asm_fun.func_name false
	  in
	  if target_alocs = [] then (
	    soundness_warning "No alocs for set_byte...\n";
	    P.printf "0x%x: %s\n" !address (Instr.str_of_instr ai);
	    let addr_mode = addr_mode_from_addr_re are ai in
	    P.printf "RevAddrMode:\n%s\n" (Instr.str_of_addr addr_mode);
	    P.printf "Reverse RE:\n%s\n" (Printer.str_of_rtl_exp_less_size are);
	    dump_vset_table target_vsettbl;
	  ) else (
	    let target_vset = 
	      cfa_repr_of_re vre target_vsettbl ai asm_fun.func_name 
	    in
	    List.iter (fun target_aloc ->
	      H.replace target_vsettbl target_aloc
		target_vset
	    ) target_alocs
	  )
	| Coq_set_ps_reg_rtl(s,re,psreg) -> 
	  let target_alocs = 
	    psreg_to_aloc psreg ai asm_fun.func_name false
	  in
	  let target_vset = 
	    cfa_repr_of_re re vsettbl ai asm_fun.func_name 
	  in
	  List.iter (fun target_aloc ->
	    H.replace target_vsettbl target_aloc target_vset
	  ) target_alocs
	| _ -> ()
      ) reduced_rilist;
      address := !address + len
    ) (patch_for_must_analysis asm_fun bb.bb_instrs);
  ) in_vsettbl_map;
  pvs_tbl

let reverse_vsettbl vsettbl =
  H.fold (fun aloc re_list cfa_exprs ->
    match aloc.aloc_region with
    | AR_Reg reg ->
      let get_reg = Coq_get_loc_rtl_exp(Big.of_int 31, X86_MACHINE.Coq_reg_loc reg) in
      let new_cfaexprs =
	List.filter_map (fun re ->
	  let offset_opt = offset_in_cfarepr re in
	  if offset_opt = None then None
	  else
	    let offset = Option.get offset_opt in
	    if offset >= 0 then
	      Some (rtl_add 32 get_reg (rtl_imm 32 offset))
	    else
	      Some (rtl_sub 32 get_reg (rtl_imm 32 (-offset)))
	) re_list
      in
      (List.unique ?eq:(Some eq_rtl_exp) new_cfaexprs) :: cfa_exprs
    | _ -> cfa_exprs
  ) vsettbl []

let prog_vsettbl_to_CFA pvs_tbl =
  let func_cfa_repr = 
    H.fold (fun exepnt vsettbl l ->
      let reversed_vsettbl = 
	reverse_vsettbl vsettbl 
      in
      if snd exepnt then l else
	let lopc = fst exepnt in
	(lopc, reversed_vsettbl) :: l
    ) pvs_tbl []
  in
  let sorted_func_cfa_repr = 
    List.sort (fun (lopc1, cfa_exprs1) (lopc2, cfa_exprs2) ->
      if lopc1 < lopc2 then -1
      else if lopc1 > lopc2 then 1
      else 0
    ) func_cfa_repr
  in
  let backward_compatible_result =
    List.map (fun (lopc, cfa_exprs) ->
      let res = 
	List.filter_map (fun re_list ->
	  if List.is_empty re_list then
	    None
	  else if List.length re_list = 1 then
	    Some (0, (List.hd re_list))
	  else
	    None
	) cfa_exprs
      in
      (lopc, res)
    ) sorted_func_cfa_repr
  in
  backward_compatible_result
  
let gen_must_CFA asm_fun =
  let must_PntToStk = 
    Stats.time ("Must-CFA generation")
      must_PntToStk asm_fun
  in
	(*PntToStkLib.dump_prog_vset_tables must_PntToStk;*)
  let must_CFA =
    Stats.time ("Must-CFA translation") 
      prog_vsettbl_to_CFA must_PntToStk
  in
  must_CFA

let query_about_reg cfa_exprs reg =
  List.find_map (fun (lvl,rtle) ->
    if lvl = 0 then
      begin match rtle with
      | Coq_get_loc_rtl_exp(s,loc) ->
	begin match loc with
	| X86_MACHINE.Coq_reg_loc reg_loc ->
	  if reg_loc = reg then Some 0
	  else None
	| _ -> None
	end
      | Coq_arith_rtl_exp (s,bvop,re1,re2) ->
	let reg_loc =
	  begin match re1 with
	  | Coq_get_loc_rtl_exp(s,loc) ->
	    begin match loc with
	    | X86_MACHINE.Coq_reg_loc reg_loc ->
	      reg_loc
	    | _ -> assert false
	    end
	  | _ -> assert false
	  end
	in
	if reg_loc = reg then
	  begin match bvop with
	  | Coq_add_op ->
	    begin match re2 with
	    | Coq_imm_rtl_exp(s,v) ->
	      Some (-(Big.to_int v))
	    | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
	      None
	    | _ -> assert false
	    end
	  | Coq_sub_op ->
	    begin match re2 with
	    | Coq_imm_rtl_exp(s,v) ->
	      Some (Big.to_int v)
	    | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
	      None
	    | _ -> assert false
	    end
	  | _ -> assert false
	  end
	else None
      | _ -> assert false
      end
    else
	(** Complex representation requires extra effort **)
      None    
  ) cfa_exprs

let dump_CFA cfa =
  List.iter (fun (lopc,cfa_exprs) ->
    (*printf "%s:\t" (str_of_mword_flex lopc);*)
    P.printf "%x:\t" lopc;
    List.iter (fun (lvl,re) ->
      if lvl <= 0 then
	P.printf "%s\t" (Printer.str_of_rtl_exp_less_size re);
    ) cfa_exprs;
    P.printf "\n"
  ) cfa

(*
type cfa_pattern =
| PTN_const_off of bool
| PTN_cast_to_8 of int * bool
| PTN_unknown of rtl_exp

let sig_of_pattern ptn =
  let size32 = Big.of_int 31 in
  let size8 = Big.of_int 7 in
  
  let cfa_psreg =
    Coq_get_ps_reg_rtl_exp (size32, Big.of_int (-1))
  in

  let random_v size t =
    Coq_get_ps_reg_rtl_exp(size, Big.of_int t)
  in

  let rand32 t = random_v size32 t in

  let coff_true_rand32 = rand32 (-2) in

  let coff_false_rand32 = rand32 (-3) in

  let coq_imm size v =
    Coq_imm_rtl_exp(size, Big.of_int v)
  in

  let coq_add size re1 re2 =
    Coq_arith_rtl_exp(size,Coq_add_op,re1,re2)
  in

  let coq_sub size re1 re2 =
    Coq_arith_rtl_exp(size,Coq_sub_op,re1,re2)
  in

  let rec coq_multi_shru num bits size re =
    if num = 0 then
      re
    else
      let new_re = 
	coq_multi_shru (num-1) bits size re
      in 
      Coq_arith_rtl_exp(size, Coq_shru_op, new_re, coq_imm size bits)
  in
  
  let coq_castu from_size to_size re =
    Coq_cast_u_rtl_exp(from_size,to_size,re)
  in

  match ptn with
  | PTN_const_off sign ->
    if sign then
      (** cfa + positive_or_zero **)
      coq_add size32 cfa_psreg coff_true_rand32
    else
      (** cfa + negative **)
      coq_sub size32 cfa_psreg coff_false_rand32
  | PTN_cast_to_8 (shru_num, sign) ->
    assert (shru_num <= 3);
    let core_re = 
      if sign then
	coq_add size32 cfa_psreg (rand32 (-4-shru_num))
      else
	coq_sub size32 cfa_psreg (rand32 (-8-shru_num))
    in
    coq_castu size32 size8 
      (coq_multi_shru shru_num 8 size32 core_re)
  | PTN_unknown re -> re


let conclude_pattern re =
  let rec num_of_shru inre =
    match inre with
    | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
      begin match bvop with
      | Coq_shru_op ->
	begin match re2 with
	| Coq_imm_rtl_exp(s,v) -> 
	  if Big.to_int v = 8 then
	    let num_option = num_of_shru re1 in
	    if num_option = None then None
	    else
	      Some ((Option.get num_option) + 1)
	  else
	    None
	| _ -> None
	end
      | Coq_add_op | Coq_sub_op ->
	begin match re1 with
	| Coq_get_ps_reg_rtl_exp(s,psreg) when (Big.to_int psreg) = (-1) ->
	  begin match re2 with
	  | Coq_get_ps_reg_rtl_exp(s,psreg') when (Big.to_int psreg') <= (-2) ->
	    Some 0
	  | Coq_imm_rtl_exp(s,v) ->
	    Some 0
	  | _ -> None
	  end
	| _ -> None
	end
      | _ -> None
      end
    | _ -> None
  in
  
  let special_symbol = special_symbol_in_re re in
  match (-special_symbol) with
  | 2 -> PTN_const_off true
  | 3 -> PTN_const_off false
  | n -> 
    if n >= 4 && n < 8 then
      PTN_cast_to_8 ((n - 4),true)
    else if 
  match re with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    begin match re1 with
    | Coq_get_ps_reg_rtl_exp(s,psreg) when (Big.to_int psreg) = (-1) ->
      begin match re2 with
      | Coq_get_ps_reg_rtl_exp(s,psreg') when (Big.to_int psreg') = (-2) ->
	begin match bvop with
	| Coq_add_op ->	PTN_const_off true
	| Coq_sub_op -> PTN_const_off false
	| _ -> assert false	  
	end
      | Coq_imm_rtl_exp(s,v) ->
	let signed_native_int_of_v = 
	  (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int v)))
	in
	if signed_native_int_of_v >= 0 then
	  PTN_const_off true
	else
	  PTN_const_off false
      | _ -> PTN_unknown re
      end
    | _ ->
      PTN_unknown re
    end
  | Coq_cast_u_rtl_exp(from_s,to_s,re') ->
    let num_of_shru_option = num_of_shru re' in
    if num_of_shru_option = None then PTN_unknown re
    else
      PTN_cast_to_8 (Option.get num_of_shru_option)  
  | _ ->
    PTN_unknown re

let constant_widening tol vsettbl =
  let widening vset =
    let ptntbl = H.create 32 in
    List.iter (fun re ->
      let re_ptn = conclude_pattern re in
      if H.mem ptntbl re_ptn then
	H.replace ptntbl re_ptn (re::(H.find ptntbl re_ptn))
      else
	H.add ptntbl re_ptn [re]
    ) vset;
    let new_vset = ref [] in
    H.iter (fun ptn leaves ->
      if List.exists (eq_rtl_exp (sig_of_pattern ptn)) leaves then
	new_vset := (sig_of_pattern ptn) :: !new_vset
      else if List.length leaves > tol then
	new_vset := (sig_of_pattern ptn) :: !new_vset
      else
	new_vset := leaves @ !new_vset
    ) ptntbl;
    !new_vset
  in
  H.iter (fun aloc vset ->
    H.replace vsettbl aloc (widening vset)
  ) vsettbl
*)
(*
let gen_may_PntToStk asm_fun = 
  let prog_vsettbl = H.create 32 in

  let bb_map = H.create 32 in
  List.iter (fun bb ->
    H.add bb_map bb.bb_label bb;
  ) asm_fun.func_bbs;
  
  let in_vsettbl_map = H.create 32 in
  let out_vsettbl_map = H.create 32 in
  List.iter (fun bb ->
    let empty_in_vsettbl = H.create 32 in
    H.add in_vsettbl_map bb.bb_label empty_in_vsettbl;
    let empty_out_vsettbl = H.create 32 in
    H.add out_vsettbl_map bb.bb_label empty_out_vsettbl
  ) asm_fun.func_bbs;

  let cfa_pseudo_reg =
    Coq_get_ps_reg_rtl_exp (Big.of_int 31, Big.of_int (-1))
  in

  let initial_esp_cfa_expr =
    rtl_sub 32 cfa_pseudo_reg (rtl_imm 32 4)
  in

  let initial_esp_vset = 
    [initial_esp_cfa_expr]
  in

  let entry_bb_label =
    (List.hd asm_fun.func_bbs).bb_label
  in
  assert (entry_bb_label = asm_fun.func_low);
  
  H.add (H.find in_vsettbl_map entry_bb_label) (AL_Reg ESP) initial_esp_vset;
  
  (*
  H.iter (fun relAddr in_vsettbl ->
    P.printf "%x --> \n" relAddr;
    dump_vset_table in_vsettbl
  ) in_vsettbl_map;
  *)
  
  let analyze_ri ri ai vsettbl =
    match ri with
    | Coq_set_loc_rtl(s,re,loc) ->
      let target_aloc = loc_to_aloc loc in
      let target_vset = cfa_repr_of_re re vsettbl ai asm_fun.func_name in
      if target_aloc = None then ()
      else if target_vset = [] then
	H.remove vsettbl (Option.get target_aloc)
      else 
	H.replace vsettbl (Option.get target_aloc) target_vset
    | Coq_set_ps_reg_rtl(s,re,psreg) -> 
      ()
    | Coq_set_array_rtl(s1,s2,arr,ire,vre) ->
      ()
    | Coq_set_byte_rtl(vre, are) ->
      let target_aloc = addr_to_aloc are ai vsettbl asm_fun.func_name in
      let target_vset = cfa_repr_of_re vre vsettbl ai asm_fun.func_name in
      if target_aloc = None then ()
      else if target_vset = [] then
	H.remove vsettbl (Option.get target_aloc)
      else
	H.replace vsettbl (Option.get target_aloc) target_vset
    | Coq_advance_oracle_rtl ->
      ()
    | Coq_if_rtl(guard,ri') ->
      ()
    | Coq_error_rtl ->
      ()
    (*raise (Failure "Unexpected RTL Error Instruction in CFAGen")*)
    | Coq_trap_rtl ->
      ()
  (*raise (Failure "Unexpected RTL Trap Instruction in CFAGen")*)
  in
  
  let perform_vsa taskbb in_vsettbl out_vsettbl =
    (** Use the instructions in taskbb and the in_vsettbl
	to compute a new out_vsettbl;
	if the new and the old are the same then return
	false;
	else update the out_vsettbl_map and return true
    **)

    let new_out_vsettbl = H.copy in_vsettbl in
    List.iter (fun (pre,instr,len) ->
      let rtl_instrs = X86_Compile.instr_to_rtl pre instr in
      List.iter (fun ri ->
	analyze_ri ri (pre,instr) new_out_vsettbl
      ) rtl_instrs
    ) (patch_for_may_analysis asm_fun taskbb.bb_instrs);
    
    (*
    P.printf "New out vsettbl for %s\n" taskbb.bb_label;
    dump_vset_table new_out_vsettbl;
    P.printf "Old out vsettbl for %s\n" taskbb.bb_label;
    dump_vset_table out_vsettbl;
    flush stdout;
    *)

    let continue_sign = not (eq_vsettbl out_vsettbl new_out_vsettbl) in
    if continue_sign then
      H.replace out_vsettbl_map taskbb.bb_label new_out_vsettbl;
    continue_sign
  in

  let analyze_bb taskbb task_in_vsettbl = 
    try
      let in_vsettbl = H.find in_vsettbl_map taskbb.bb_label in
      if H.length in_vsettbl = 0 then
	(* Should test if it is the first time of update*)
	(* Empty vsettbl means not visited, because at least 
	   ESP should be in*)
	H.iter (fun aloc vset ->
	  H.add in_vsettbl aloc vset
	) task_in_vsettbl
      else
	union_vsettbl_tofst in_vsettbl task_in_vsettbl;
      let out_vsettbl = 
	H.find out_vsettbl_map taskbb.bb_label 
      in
      (*
      dump_vset_table in_vsettbl;
      dump_vset_table out_vsettbl;
      *)
      let push_new_b =
	perform_vsa taskbb in_vsettbl out_vsettbl
      in
      push_new_b
    with Not_found ->
      assert false
  in

  let workq = Queue.create () in
  
  let add_new_tasks bb =
    if List.is_empty bb.bb_inter_succs then
      List.iter (fun succlbl ->
	if H.mem bb_map succlbl then
	  let newtask = mkVT
	    succlbl bb.bb_label
	  in
	  Queue.push newtask workq
      ) bb.bb_succs
    else 
      List.iter (fun succlbl ->
	if H.mem bb_map succlbl then
	  let newtask = mkVT
	    succlbl bb.bb_label
	  in
	  Queue.push newtask workq
	else (
	  P.printf "%s not found for %s!\n" succlbl bb.bb_label;
	  flush stdout;
	)
      ) bb.bb_inter_succs
  in

  let entry_bb = List.hd asm_fun.func_bbs in
  let push_task_b = analyze_bb entry_bb (H.find in_vsettbl_map entry_bb_label) in
  if push_task_b then add_new_tasks entry_bb;
  while not (Queue.is_empty workq) do
    let task = Queue.take workq in
    let tasklbl = task.vt_curr in
    let task_prev = task.vt_prev in
    try
      let task_in_vsettbl = H.find out_vsettbl_map task_prev in
      let task_bb = H.find bb_map tasklbl in
      let push_task_b = analyze_bb task_bb task_in_vsettbl in
      if push_task_b then add_new_tasks task_bb
    with Not_found ->
      assert false
  done;
(*
  H.iter (fun bblbl in_vsettbl ->
    P.printf "%s --> \n" bblbl;
    dump_vset_table in_vsettbl
  ) in_vsettbl_map;
*)

  H.iter (fun bblbl vsettbl ->
    let bb = H.find bb_map bblbl in
    let address = ref bb.bb_relAddr in
    let target_vsettbl = H.copy vsettbl in
    List.iter (fun (pre,ins,len) ->
      let ai = pre,ins in
      let exepnt = !address, false in
      H.add prog_vsettbl exepnt (H.copy target_vsettbl);
      let rtl_instrs = X86_Compile.instr_to_rtl pre ins in
      List.iter (fun ri ->
	match ri with
	| Coq_set_loc_rtl(s,re,loc) ->
	  let target_aloc = loc_to_aloc loc in
	  let target_vset = 
	    cfa_repr_of_re re target_vsettbl ai asm_fun.func_name 
	  in
	  if target_aloc = None then ()
	  else if target_vset = [] then
	    H.remove target_vsettbl (Option.get target_aloc)
	  else (
	    (*P.printf "set_loc target vset:\n%s\n" (str_of_value_set target_vset);*)
	    H.replace target_vsettbl (Option.get target_aloc)
	      target_vset
	  )
	| Coq_set_byte_rtl(vre, are) ->
	  let target_aloc = addr_to_aloc are ai vsettbl asm_fun.func_name in
	  let target_vset = 
	    cfa_repr_of_re vre target_vsettbl ai asm_fun.func_name in
	  if target_aloc = None then ()
	  else if target_vset = [] then
	    H.remove target_vsettbl (Option.get target_aloc)
	  else
	    H.replace target_vsettbl (Option.get target_aloc)
	      target_vset
	| _ -> ()
      ) rtl_instrs;
      address := !address + len
    ) (patch_for_may_analysis asm_fun bb.bb_instrs);
  ) in_vsettbl_map;
  prog_vsettbl
*)
