(**
   This application is to generate for each assembly-level function a 
   complete list of CFA representations with registers and offsets: 
   
   Input: disassembled functions with CFGs
   Output: a list of CFA representations for each function
   range * (CFAExpr list) list

   Workflow:
   1. Initialize a hash table for storing values of registers in terms
   of CFA and offsets
   2. Split the function into address intervals per instruction; we have
   range * X86_instr list
   3. range * X86_instr list --> range * (rtl_instr list) list
   4. range * (rtl_instr list) list --> range * (CFAExpr list) list

**)

open Batteries
open Printf
module H = Hashtbl

open X86Syntax
open X86Semantics
open X86_MACHINE
open X86_RTL
open Config
open Abbrev
open BB
open Ibaux

open Printer
open RTL_opt

(** (relAddr * ((level * rtl_exp) list)) list **)
type cfaRepr = (int * ((int*rtl_exp) list)) list

type cfabb = {
  mutable cfa_bb : basicblock;
  mutable cfa_repr: cfaRepr;
  mutable cfa_in_regvals : ((register, rtl_exp) H.t) option;
  mutable cfa_out_regvals : ((register, rtl_exp) H.t) option;
}

let mkCFABB cfa_input bb cfa_output =
  {
    cfa_bb = bb;
    cfa_repr = [];
    cfa_in_regvals = cfa_input;
    cfa_out_regvals = cfa_output;
  }

let dump_regvals regvals =
  H.iter (fun reg re ->
    (*let re = simp_re [neg_opt] re in*) 
    printf "%s:\n%s\n" 
      (str_of_reg reg)
      (str_of_rtl_exp_less_size re);
    flush stdout
  ) regvals

let dump_cfa_repr level cfa_repr =
  List.iter (fun (lopc,cfa_exprs) ->
    (*printf "%s:\t" (str_of_mword_flex lopc);*)
    printf "%x:\t" lopc;
    List.iter (fun (lvl,re) ->
      if lvl <= level then
	printf "%s\t" (str_of_rtl_exp_less_size re);
    ) cfa_exprs;
    printf "\n"
  ) cfa_repr;
  flush stdout

let rec apply_regvals regvals rtl_expr =
  begin match rtl_expr with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    let new_re1 = apply_regvals regvals re1 in
    let new_re2 = apply_regvals regvals re2 in
    Coq_arith_rtl_exp(s,bvop,new_re1,new_re2)
  | Coq_test_rtl_exp(s,top,re1,re2) ->
    let new_re1 = apply_regvals regvals re1 in
    let new_re2 = apply_regvals regvals re2 in
    Coq_test_rtl_exp(s,top,new_re1,new_re2)
  | Coq_if_rtl_exp(s,re1,re2,re3) ->
    let new_re1 = apply_regvals regvals re1 in
    let new_re2 = apply_regvals regvals re2 in
    let new_re3 = apply_regvals regvals re3 in
    Coq_if_rtl_exp(s,new_re1,new_re2,new_re3)
  | Coq_cast_s_rtl_exp(s1,s2,re) ->
    let new_re = apply_regvals regvals re in
    Coq_cast_s_rtl_exp(s1,s2,new_re)
  | Coq_cast_u_rtl_exp(s1,s2,re) ->
    let new_re = apply_regvals regvals re in
    Coq_cast_u_rtl_exp(s1,s2,new_re)
  | Coq_get_loc_rtl_exp(s,loc) ->
    begin match loc with
    | Coq_reg_loc(reg) -> 
      (*if reg = ESP || reg = EBP then*)
      begin
	try
	  H.find regvals reg
	with Not_found -> 
	  Coq_get_loc_rtl_exp(s,loc)
      end
    | _ -> Coq_get_loc_rtl_exp(s,loc)
    end
  | Coq_get_array_rtl_exp(s,l,arr,re) ->
    let new_re = apply_regvals regvals re in
    Coq_get_array_rtl_exp(s,l,arr,new_re)
  | Coq_get_byte_rtl_exp(re) ->
    let new_re = apply_regvals regvals re in
    Coq_get_byte_rtl_exp(new_re)
  | _ -> (* ignore floating point arithmetic expressions *)
    rtl_expr
  end

let translate_regvals regvals =
  H.fold (fun reg re cfa_exprs ->
    let get_reg = Coq_get_loc_rtl_exp(Big.of_int 31, Coq_reg_loc reg) in
    let expr = Coq_test_rtl_exp(Big.of_int 0,Coq_eq_op,get_reg,re) in
    match re with
    | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
      begin match re1 with
      | Coq_get_ps_reg_rtl_exp(s, ps_reg) when Big.to_int ps_reg = (-1) ->
	begin match bvop with
	| Coq_add_op ->
	  begin match re2 with
	  | Coq_get_ps_reg_rtl_exp(s, ps_reg2) when Big.to_int ps_reg2 = (-2) ->
	    (0,Coq_arith_rtl_exp(s,Coq_sub_op,get_reg,re2)) :: cfa_exprs
	  | _ ->
	    (0,Coq_arith_rtl_exp(s,Coq_sub_op,get_reg,re2)) :: cfa_exprs
	  end
	| Coq_sub_op ->
	  begin match re2 with
	  | Coq_get_ps_reg_rtl_exp(s, ps_reg2) when Big.to_int ps_reg2 = (-2) ->
	    (0,Coq_arith_rtl_exp(s,Coq_add_op,get_reg,re2)) :: cfa_exprs
	  | _ ->
	    (0,Coq_arith_rtl_exp(s,Coq_add_op,get_reg,re2)) :: cfa_exprs
	  end
	| _ -> (1,expr) :: cfa_exprs
	end
      | _ -> (1,expr) :: cfa_exprs
      end
    | Coq_get_ps_reg_rtl_exp(s,ps_reg) when Big.to_int ps_reg = (-1) ->
      (0,get_reg) :: cfa_exprs
    | _ -> (1,expr) :: cfa_exprs
  ) regvals []

let transform_lopc start rel_low =
  let start_mword = MWord.of_string start in
  let rel_low_mword = MWord.of_int rel_low in
  let abs_low_mword = MWord.(+%) rel_low_mword start_mword in
  (*str_of_mword_flex abs_low_mword*)
  abs_low_mword

(** For simplicity, we assume stack layout modification only happens in
    prologue and we do not care about epilogue. **)

let rec eq_rtl_exp rtl_exp1 rtl_exp2 =
  match rtl_exp1, rtl_exp2 with
  | Coq_arith_rtl_exp(s,bvop,re1,re2), Coq_arith_rtl_exp(s',bvop',re1',re2') ->
    Big.eq s s' && bvop = bvop' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2'
  | Coq_test_rtl_exp(s,top,re1,re2), Coq_test_rtl_exp(s',top',re1',re2') ->
    Big.eq s s' && top = top' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2'
  | Coq_if_rtl_exp(s,re1,re2,re3), Coq_if_rtl_exp(s',re1',re2',re3') ->
    Big.eq s s' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2' && eq_rtl_exp re3 re3'
  | Coq_cast_s_rtl_exp(s1,s2,re), Coq_cast_s_rtl_exp(s1',s2',re') ->
    Big.eq s1 s1' && Big.eq s2 s2' && eq_rtl_exp re re'
  | Coq_cast_u_rtl_exp(s1, s2, re), Coq_cast_u_rtl_exp(s1', s2', re') ->
    Big.eq s1 s1' && Big.eq s2 s2' && eq_rtl_exp re re'
  | Coq_imm_rtl_exp(s, v), Coq_imm_rtl_exp(s', v') ->
    Big.eq s s' && Big.eq v v'
  | Coq_get_loc_rtl_exp(s, loc), Coq_get_loc_rtl_exp(s', loc') ->
    Big.eq s s' && loc = loc'
  | Coq_get_ps_reg_rtl_exp(s, ps_reg), Coq_get_ps_reg_rtl_exp(s',ps_reg') ->
    Big.eq s s' && Big.eq ps_reg ps_reg'
(*
  | Coq_get_ps_reg_rtl_exp(s,ps_reg), _ 
  | _, Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    (Big.to_string ps_reg) = "-2"
*)
  | Coq_get_array_rtl_exp(l, s, arr, re), Coq_get_array_rtl_exp(l',s',arr',re') ->
    Big.eq l l' && Big.eq s s' && arr = arr' && eq_rtl_exp re re'
  | Coq_get_byte_rtl_exp(re), Coq_get_byte_rtl_exp(re') ->
    eq_rtl_exp re re'
  | Coq_get_random_rtl_exp(s),Coq_get_random_rtl_exp(s') ->
    Big.eq s s'
  | Coq_farith_rtl_exp(ew,mw,faop,rtl_rm,re1,re2),
    Coq_farith_rtl_exp(ew',mw',faop',rtl_rm',re1',re2') ->
    Big.eq ew ew' && Big.eq mw mw' && faop = faop' && eq_rtl_exp rtl_rm rtl_rm' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2'
  | Coq_fcast_rtl_exp(s1,s2,s3,s4,re1,re2),Coq_fcast_rtl_exp(s1',s2',s3',s4',re1',re2') ->
    Big.eq s1 s1' && Big.eq s2 s2' && Big.eq s3 s3' && Big.eq s4 s4' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2'
  | _,_ -> false

let eq_regvals regvals1 regvals2 =
  H.fold (fun key dest b ->
    try
      b && eq_rtl_exp dest (H.find regvals2 key) (*(dest = (H.find regvals2 key))*)
    with Not_found ->
      false
  ) regvals1 true

let rec cfa_in_rtl_exp rtl_exp =
  match rtl_exp with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    cfa_in_rtl_exp re1 || cfa_in_rtl_exp re2
  | Coq_test_rtl_exp(s,top,re1,re2) ->
    cfa_in_rtl_exp re1 || cfa_in_rtl_exp re2
  | Coq_if_rtl_exp(s,re1,re2,re3) ->
    cfa_in_rtl_exp re1 || cfa_in_rtl_exp re2 || cfa_in_rtl_exp re3
  | Coq_cast_s_rtl_exp(s1,s2,re) ->
    cfa_in_rtl_exp re
  | Coq_cast_u_rtl_exp(s1,s2,re) ->
    cfa_in_rtl_exp re
  | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    Big.to_int ps_reg = (-1)
  | _ -> (* ignore floating point arithmetic expressions *)
    false


let rec random_in_re rtl_exp =
  match rtl_exp with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    random_in_re re1 || random_in_re re2
  | Coq_test_rtl_exp(s,top,re1,re2) ->
    random_in_re re1 || random_in_re re2
  | Coq_if_rtl_exp(s,re1,re2,re3) ->
    random_in_re re1 || random_in_re re2 || random_in_re re3
  | Coq_cast_s_rtl_exp(s1,s2,re) ->
    random_in_re re
  | Coq_cast_u_rtl_exp(s1,s2,re) ->
    random_in_re re
  | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    Big.to_int ps_reg = (-2)
  | _ -> (* ignore floating point arithmetic expressions *)
    false

exception Regvals_Conflict

let must_cfa_repr_gen asm_fun =
  (*printf "current function contains basic blocks:\n";
  List.iter (fun bb ->
    printf "%s\n" bb.bb_label;    
  ) asm_fun.func_bbs;
  flush stdout;*)
  let regvals = H.create 32 in
  
  let cfa_pseudo_reg = (* may consider other types for CFA *)
    Coq_get_ps_reg_rtl_exp (Big.of_int 31, Big.of_int (-1)) in
  
  let initial_esp = 
  Coq_arith_rtl_exp (Big.of_int 31, Coq_sub_op, cfa_pseudo_reg, Coq_imm_rtl_exp (Big.of_int 31, Big.of_int 4))
  in

  H.add regvals ESP initial_esp;
  
  let analyze_bb in_regvals bb =    
    let out_regvals = H.copy in_regvals in

    let bb_instrs = bb.bb_instrs in
    
    let bb_start = bb.bb_relAddr in
    
    let instr_start = ref bb_start in

    let patch_for_intra_analysis bb_instrs =
      List.map (fun (pre,instr,len) -> 
	match instr with
	| CALL(_,_,_,_) -> 
	  (pre, MOV (true, (Reg_op EAX), (Imm_op (Big.of_int 0))),len)
	| AND(_,op1,op2) ->
	  if asm_fun.func_name = "main" then
	    begin match op1, op2 with
	    | Reg_op reg, Imm_op v 
	    | Imm_op v, Reg_op reg ->
	      if reg = ESP && (unsigned32_to_hex v) = "0xfffffff0" then
		(pre,NOP (Imm_op (Big.of_int 0)),len)
	      else
		pre,instr,len
	    | _, _ -> pre,instr,len
	    end
	  else
	    (pre,instr,len)
	| _ -> (pre,instr,len)
      ) bb_instrs
    in

    let patch_call_with_nop bb_instrs =
      List.map (fun (pre,instr,len) -> 
	match instr with
	| CALL(_,_,_,_) -> (pre,NOP (Imm_op (Big.of_int 0)),len)
	| AND(_,op1,op2) ->
	  if asm_fun.func_name = "main" then
	    begin match op1, op2 with
	    | Reg_op reg, Imm_op v 
	    | Imm_op v, Reg_op reg ->
	      if reg = ESP && (unsigned32_to_hex v) = "0xfffffff0" then
		(pre,NOP (Imm_op (Big.of_int 0)),len)
	      else
		pre,instr,len
	    | _, _ -> pre,instr,len
	    end
	  else
	    (pre,instr,len)
	| _ -> (pre,instr,len)
      ) bb_instrs
    in

    let addressed_rtl_instrs = 
      List.map (fun (pre,instr,len) ->
	let rtl_instrs = X86_Compile.instr_to_rtl pre instr in
	let addressed_rtl_instr = 
	  !instr_start, rtl_instrs
	in
	instr_start := !instr_start + len;
	addressed_rtl_instr
      ) (patch_for_intra_analysis bb_instrs)
    in

    let addressed_patched_rtl_instrs =
      List.map (fun (pre,instr,len) ->
	let rtl_instrs = 
	  match instr with
	  | CALL(_,_,_,_) ->
	    let random_value = 
	      Coq_get_random_rtl_exp (Big.of_int 31)
	    in
	    [Coq_set_loc_rtl(Big.of_int 31, random_value, Coq_reg_loc EAX)]
	  | AND(_,op1,op2) ->
	    if asm_fun.func_name = "main" then
	      begin match op1, op2 with
	      | Reg_op reg, Imm_op v
	      | Imm_op v, Reg_op reg ->
		if reg = ESP && (unsigned32_to_hex v) = "0xfffffff0" then
		  X86_Compile.instr_to_rtl pre (NOP (Imm_op (Big.of_int 0)))
		else
		  X86_Compile.instr_to_rtl pre instr
	      | _, _ -> 
		X86_Compile.instr_to_rtl pre instr
	      end
	    else
	      X86_Compile.instr_to_rtl pre instr
	  | _ -> X86_Compile.instr_to_rtl pre instr
	in
	let addressed_rtl_instr = 
	  !instr_start, rtl_instrs
	in
	instr_start := !instr_start + len;
	addressed_rtl_instr
      ) bb_instrs
    in

    let cfa_repr =
      List.map (fun (lopc, rtl_instr_list) ->
	let cfa_expr_list = translate_regvals out_regvals in
	List.iter (fun rtl_instr ->
	  begin match rtl_instr with
	  | Coq_set_loc_rtl(s,rtl_e,loc) ->
	    begin match loc with
	    | X86_MACHINE.Coq_reg_loc(reg) ->
	      (*if reg = ESP || reg = EBP then*)
		let new_rhs = apply_regvals out_regvals rtl_e in
		let simplified_new_rhs = simp_re simpre_all_methods new_rhs in
		let simplified_new_rhs = neg_opt simplified_new_rhs in
		(*printf "simplified_new_rhs:%s\n" (str_of_rtl_exp_less_size simplified_new_rhs);*)
		if cfa_in_rtl_exp simplified_new_rhs then 
		  H.replace out_regvals reg simplified_new_rhs
		else
		  H.remove out_regvals reg
	    | _ -> ()
	    end
	  | _ -> ()
	  end
	) rtl_instr_list;
	(*let abs_lopc = transform_lopc "0x8048000" lopc in*)
	lopc, cfa_expr_list
      ) addressed_rtl_instrs
    in
    out_regvals, cfa_repr
  in
  
  (** To completely reconstruct stack layout, we need to 
      do a worklist algorithm from entry block. **)
  let workq = Queue.create () in
  
  let add_new_task task_cfabb out_regvals =
    if not (does_bb_ret task_cfabb.cfa_bb) then (
      List.iter (fun bb_label ->
	if List.exists (fun func_bb -> func_bb.bb_label = bb_label) asm_fun.func_bbs && bb_label <> asm_fun.func_low then
	  Queue.add (out_regvals, bb_label) workq
      ) (task_cfabb.cfa_bb.bb_succs @ task_cfabb.cfa_bb.bb_inter_succs)
    )
  in
  
  let entry_bb_label = (* because the bb list are sorted *)
    (List.hd asm_fun.func_bbs).bb_label 
  in

  (*let entry_cfabb = mkCFABB None entry_bb None in*)

  Queue.add (regvals, entry_bb_label) workq;

  let cfabbs = ref [] in

  while (not (Queue.is_empty workq)) do
    let task_in_regvals, task_bb_label = Queue.take workq in
    (*printf "task bb label %s with input regvals:\n" task_bb_label; 
    dump_regvals task_in_regvals;
    flush stdout;*)
    let task_in_regvals = H.copy task_in_regvals in
    try 
      (*printf "current cfabbs:\n";
      List.iter (fun cfabb -> printf "%s " cfabb.cfa_bb.bb_label) !cfabbs;
      printf "\ntask_bb: %s\n" task_bb_label;
      flush stdout;*)
      let task_cfabb = 
	List.find (fun cfabb -> 
	  cfabb.cfa_bb.bb_label = task_bb_label
	) !cfabbs in
      if Option.is_none task_cfabb.cfa_in_regvals then (
	printf "not handled properly\n"; flush stdout;
	task_cfabb.cfa_in_regvals <- Some task_in_regvals;
	let out_regvals, cfa_repr = analyze_bb task_in_regvals task_cfabb.cfa_bb in
	task_cfabb.cfa_repr <- cfa_repr;
	task_cfabb.cfa_out_regvals <- Some out_regvals;
	add_new_task task_cfabb out_regvals
      )
      else (
	let old_in_regvals = Option.get task_cfabb.cfa_in_regvals in
	if not (eq_regvals old_in_regvals task_in_regvals) then (
	  (*printf "For task bb %s\n" task_bb_label;
	  printf "old input regvals:\n";
	  dump_regvals old_in_regvals;
	  printf "\n";
	  printf "new input regvals:\n";
	  dump_regvals task_in_regvals;
	  printf "\n";
	  flush stdout;*)
	  let shared_regvals = H.create 32 in
	  H.iter (fun key dest ->
	    try 
	      if eq_rtl_exp (H.find old_in_regvals key) dest then
		H.add shared_regvals key dest
	      else (
		if not (random_in_re (H.find old_in_regvals key) 
			|| random_in_re dest)
		then (
		  (*
		  P.printf "Conflict at %s!\n" task_bb_label;
		  P.printf "New input regvals:\n";
		  dump_regvals task_in_regvals;
		  P.printf "Old input regvals:\n";
		  dump_regvals old_in_regvals;
		  P.printf "\n";*)
		  let newdest = 
		    Coq_arith_rtl_exp (
		      Big.of_int 31, Coq_sub_op, 
		      cfa_pseudo_reg,
		      Coq_get_ps_reg_rtl_exp (Big.of_int 31, Big.of_int (-2)))
		  in
		  H.add shared_regvals key newdest
		)
	      )
	    with Not_found -> ()
	      (*
	      P.printf "Conflict at %s!\n" task_bb_label;
	      P.printf "New input regvals:\n";
	      dump_regvals task_in_regvals;
	      P.printf "Old input regvals:\n";
	      dump_regvals old_in_regvals;
	      P.printf "\n";
	      *)
	  ) task_in_regvals;
	  (*printf "shared regvals:\n";
	  dump_regvals shared_regvals;
	  printf "\n";
	  flush stdout;*)
	  task_cfabb.cfa_in_regvals <- Some shared_regvals;
	  let out_regvals, cfa_repr = analyze_bb shared_regvals task_cfabb.cfa_bb in
	  task_cfabb.cfa_repr <- cfa_repr;
	  task_cfabb.cfa_out_regvals <- Some out_regvals;
	  add_new_task task_cfabb out_regvals
	)
      )
    with Not_found ->
      try 
	let task_bb = List.find (fun func_bb -> func_bb.bb_label = task_bb_label) asm_fun.func_bbs in
	let task_cfabb = mkCFABB (Some task_in_regvals) task_bb None in
	let out_regvals, cfa_repr = analyze_bb task_in_regvals task_cfabb.cfa_bb in
	task_cfabb.cfa_repr <- cfa_repr;
	task_cfabb.cfa_out_regvals <- Some out_regvals;
	cfabbs := task_cfabb :: !cfabbs;
	add_new_task task_cfabb out_regvals
      with Not_found ->
	printf "missing basic block: %s\n" task_bb_label; flush stdout;
	raise (Failure "basic block label not found in function")
  done;
  let func_cfa_repr =
    List.flatten (List.map (fun cfabb -> cfabb.cfa_repr) !cfabbs)
  in
  (*dump_cfa_repr func_cfa_repr;*)
  let sorted_func_cfa_repr = 
    List.sort (fun (lopc1, cfa_exprs1) (lopc2, cfa_exprs2) -> 
      if lopc1 < lopc2 then -1
      else if lopc1 > lopc2 then 1
      else 0
    ) func_cfa_repr
  in
  if (List.length sorted_func_cfa_repr) <> (List.length func_cfa_repr) then
    printf "%d : %d" (List.length sorted_func_cfa_repr) (List.length func_cfa_repr);
  flush stdout;
  (*let prev = ref (MWord.of_int 0) in
  printf "duplicated addresses:\n";
  List.iter (fun (addr,cfa_repr) ->
    if !prev = addr then printf "%s\n" (str_of_mword_flex addr);
    prev := addr
  ) func_cfa_repr;
  flush stdout;*)
  (*dump_cfa_repr sorted_func_cfa_repr;*)
  (*sorted_func_cfa_repr*)
  (*
  let standardize_func_cfa_repr sorted_cfarepr =
    let addr_list, cfarepr_list = List.split sorted_cfarepr in
    let get_esp = Coq_get_loc_rtl_exp (Big.of_int 31, X86_MACHINE.Coq_reg_loc(ESP)) in
    let initial_cfa = 
      (0,Coq_arith_rtl_exp (Big.of_int 31, Coq_add_op, get_esp, Coq_imm_rtl_exp (Big.of_int 31, Big.of_int 4)))
    in
    let new_cfarepr_list = [initial_cfa] :: (List.rev (List.tl (List.rev cfarepr_list))) in
    List.combine addr_list new_cfarepr_list
  in
  let standard_func_cfa_repr = standardize_func_cfa_repr sorted_func_cfa_repr in*)
  (*dump_cfa_repr 1 standard_func_cfa_repr;*)
  sorted_func_cfa_repr
    
  (** Here, we assume stack depth is not modified after prologue **)
  (*
  let prologue_bb = 
    (*try*) 
      List.hd asm_fun.func_bbs
  (* may cause trouble here: this may not be the prologue*)
    (*with Not_found -> exit 1*)
  in
    
  let prologue_instrs = prologue_bb.bb_instrs in

  let prologue_start = prologue_bb.bb_relAddr in
  
  let start = ref prologue_start in

  let ranged_prologue_instrs = List.map (fun (pre,instr,len) ->
    let rtl_instrs = X86_Compile.instr_to_rtl pre instr in
    let ranged_rtl_instr = 
      (!start, len + !start), rtl_instrs
    in
    start := !start + len;
    ranged_rtl_instr
  ) prologue_instrs
  in

  let cfa_repr =
    List.map (fun (range, rtl_instr_list) ->
      List.iter (fun rtl_instr ->
	begin match rtl_instr with
	| Coq_set_loc_rtl(s,rtl_e,loc) ->
	  begin match loc with
	  | X86_MACHINE.Coq_reg_loc(reg) ->
	    if reg = ESP || reg = EBP then
	      let new_rhs = apply_regvals regvals rtl_e in
	      let simplified_new_rhs = simp_re simpre_all_methods new_rhs in
	    (*printf "simplified_new_rhs:%s\n" (str_of_rtl_exp_less_size simplified_new_rhs);*)
	      H.replace regvals reg simplified_new_rhs
	  | _ -> ()
	  end
	| _ -> ()
	end
      ) rtl_instr_list;
      let cfa_expr_list = translate_regvals regvals in
      let abs_range = transform_rel_range "0x8048000" range in
      abs_range, cfa_expr_list
    ) ranged_prologue_instrs
  in
  (*dump_regvals regvals;*)
  dump_cfa_repr cfa_repr;
  cfa_repr*)

let may_cfa_repr_gen asm_fun =
  (*printf "current function contains basic blocks:\n";
  List.iter (fun bb ->
    printf "%s\n" bb.bb_label;    
  ) asm_fun.func_bbs;
  flush stdout;*)
  let regvals = H.create 32 in
  
  let cfa_pseudo_reg = (* may consider other types for CFA *)
    Coq_get_ps_reg_rtl_exp (Big.of_int 31, Big.of_int (-1)) in
  
  let initial_esp = 
  Coq_arith_rtl_exp (Big.of_int 31, Coq_sub_op, cfa_pseudo_reg, Coq_imm_rtl_exp (Big.of_int 31, Big.of_int 4))
  in

  H.add regvals ESP initial_esp;
  
  let analyze_bb in_regvals bb =    
    let out_regvals = H.copy in_regvals in

    let bb_instrs = bb.bb_instrs in
    
    let bb_start = bb.bb_relAddr in
    
    let instr_start = ref bb_start in

    let patch_for_intra_analysis bb_instrs =
      List.map (fun (pre,instr,len) -> 
	match instr with
	| CALL(_,_,_,_) -> 
	  (pre,NOP (Imm_op (Big.of_int 0)),len)
	(*(pre, MOV (true, (Reg_op EAX), (Imm_op (Big.of_int 0))),len)*)
	| AND(_,op1,op2) ->
	  if asm_fun.func_name = "main" then
	    begin match op1, op2 with
	    | Reg_op reg, Imm_op v 
	    | Imm_op v, Reg_op reg ->
	      if reg = ESP && (unsigned32_to_hex v) = "0xfffffff0" then
		(pre,NOP (Imm_op (Big.of_int 0)),len)
	      else
		pre,instr,len
	    | _, _ -> pre,instr,len
	    end
	  else
	    (pre,instr,len)
	| _ -> (pre,instr,len)
      ) bb_instrs
    in

    let patch_call_with_nop bb_instrs =
      List.map (fun (pre,instr,len) -> 
	match instr with
	| CALL(_,_,_,_) -> (pre,NOP (Imm_op (Big.of_int 0)),len)
	| AND(_,op1,op2) ->
	  if asm_fun.func_name = "main" then
	    begin match op1, op2 with
	    | Reg_op reg, Imm_op v 
	    | Imm_op v, Reg_op reg ->
	      if reg = ESP && (unsigned32_to_hex v) = "0xfffffff0" then
		(pre,NOP (Imm_op (Big.of_int 0)),len)
	      else
		pre,instr,len
	    | _, _ -> pre,instr,len
	    end
	  else
	    (pre,instr,len)
	| _ -> (pre,instr,len)
      ) bb_instrs
    in

    let addressed_rtl_instrs = 
      List.map (fun (pre,instr,len) ->
	let rtl_instrs = X86_Compile.instr_to_rtl pre instr in
	let addressed_rtl_instr = 
	  !instr_start, rtl_instrs
	in
	instr_start := !instr_start + len;
	addressed_rtl_instr
      ) (patch_for_intra_analysis bb_instrs)
    in

    let addressed_patched_rtl_instrs =
      List.map (fun (pre,instr,len) ->
	let rtl_instrs = 
	  match instr with
	  | CALL(_,_,_,_) ->
	    let random_value = 
	      Coq_get_random_rtl_exp (Big.of_int 31)
	    in
	    [Coq_set_loc_rtl(Big.of_int 31, random_value, Coq_reg_loc EAX)]
	  | AND(_,op1,op2) ->
	    if asm_fun.func_name = "main" then
	      begin match op1, op2 with
	      | Reg_op reg, Imm_op v
	      | Imm_op v, Reg_op reg ->
		if reg = ESP && (unsigned32_to_hex v) = "0xfffffff0" then
		  X86_Compile.instr_to_rtl pre (NOP (Imm_op (Big.of_int 0)))
		else
		  X86_Compile.instr_to_rtl pre instr
	      | _, _ -> 
		X86_Compile.instr_to_rtl pre instr
	      end
	    else
	      X86_Compile.instr_to_rtl pre instr
	  | _ -> X86_Compile.instr_to_rtl pre instr
	in
	let addressed_rtl_instr = 
	  !instr_start, rtl_instrs
	in
	instr_start := !instr_start + len;
	addressed_rtl_instr
      ) bb_instrs
    in

    let cfa_repr =
      List.map (fun (lopc, rtl_instr_list) ->
	let cfa_expr_list = translate_regvals out_regvals in
	List.iter (fun rtl_instr ->
	  begin match rtl_instr with
	  | Coq_set_loc_rtl(s,rtl_e,loc) ->
	    begin match loc with
	    | X86_MACHINE.Coq_reg_loc(reg) ->
	      (*if reg = ESP || reg = EBP then*)
		let new_rhs = apply_regvals out_regvals rtl_e in
		let simplified_new_rhs = simp_re simpre_all_methods new_rhs in
		let simplified_new_rhs = neg_opt simplified_new_rhs in
		(*printf "simplified_new_rhs:%s\n" (str_of_rtl_exp_less_size simplified_new_rhs);*)
		if cfa_in_rtl_exp simplified_new_rhs then 
		  H.replace out_regvals reg simplified_new_rhs
		else
		  H.remove out_regvals reg
	    | _ -> ()
	    end
	  | _ -> ()
	  end
	) rtl_instr_list;
	(*let abs_lopc = transform_lopc "0x8048000" lopc in*)
	lopc, cfa_expr_list
      ) addressed_rtl_instrs
    in
    out_regvals, cfa_repr
  in
  
  (** To completely reconstruct stack layout, we need to 
      do a worklist algorithm from entry block. **)
  let workq = Queue.create () in
  
  let add_new_task task_cfabb out_regvals =
    if not (does_bb_ret task_cfabb.cfa_bb) then (
      List.iter (fun bb_label ->
	if List.exists (fun func_bb -> func_bb.bb_label = bb_label) asm_fun.func_bbs && bb_label <> asm_fun.func_low then
	  Queue.add (out_regvals, bb_label) workq
      ) (task_cfabb.cfa_bb.bb_succs @ task_cfabb.cfa_bb.bb_inter_succs)
    )
  in
  
  let entry_bb_label = (* because the bb list are sorted *)
    (List.hd asm_fun.func_bbs).bb_label 
  in

  (*let entry_cfabb = mkCFABB None entry_bb None in*)

  Queue.add (regvals, entry_bb_label) workq;

  let cfabbs = ref [] in

  while (not (Queue.is_empty workq)) do
    let task_in_regvals, task_bb_label = Queue.take workq in
    (*printf "task bb label %s with input regvals:\n" task_bb_label; 
    dump_regvals task_in_regvals;
    flush stdout;*)
    let task_in_regvals = H.copy task_in_regvals in
    try 
      (*printf "current cfabbs:\n";
      List.iter (fun cfabb -> printf "%s " cfabb.cfa_bb.bb_label) !cfabbs;
      printf "\ntask_bb: %s\n" task_bb_label;
      flush stdout;*)
      let task_cfabb = 
	List.find (fun cfabb -> 
	  cfabb.cfa_bb.bb_label = task_bb_label
	) !cfabbs in
      if Option.is_none task_cfabb.cfa_in_regvals then (
	assert false
	(*
	printf "not handled properly\n"; flush stdout;
	task_cfabb.cfa_in_regvals <- Some task_in_regvals;
	let out_regvals, cfa_repr = analyze_bb task_in_regvals task_cfabb.cfa_bb in
	task_cfabb.cfa_repr <- cfa_repr;
	task_cfabb.cfa_out_regvals <- Some out_regvals;
	add_new_task task_cfabb out_regvals*)
      )
      else (
	let old_in_regvals = Option.get task_cfabb.cfa_in_regvals in
	if not (eq_regvals old_in_regvals task_in_regvals) then (
	  printf "For task bb %s\n" task_bb_label;
	  printf "old input regvals:\n";
	  dump_regvals old_in_regvals;
	  printf "\n";
	  printf "new input regvals:\n";
	  dump_regvals task_in_regvals;
	  printf "\n";
	  flush stdout;
	  let unioned_regvals = H.copy old_in_regvals in
	  H.iter (fun key dest ->
	    if H.mem unioned_regvals key then (
	      if eq_rtl_exp (H.find unioned_regvals key) dest then ()
	      else (
		if not (random_in_re (H.find unioned_regvals key)
			|| random_in_re dest) 
		then
		  let newdest = 
		    Coq_arith_rtl_exp (
		      Big.of_int 31, Coq_sub_op, 
		      cfa_pseudo_reg,
		      Coq_get_ps_reg_rtl_exp (Big.of_int 31, Big.of_int (-2)))
		  in
		  H.add unioned_regvals key newdest
	      )
	    )
	    else
	      H.add unioned_regvals key dest
	  ) task_in_regvals;
	  task_cfabb.cfa_in_regvals <- Some unioned_regvals;
	  if not (eq_regvals old_in_regvals unioned_regvals) then
	  let out_regvals, cfa_repr = analyze_bb unioned_regvals task_cfabb.cfa_bb in
	  task_cfabb.cfa_repr <- cfa_repr;
	  task_cfabb.cfa_out_regvals <- Some out_regvals;
	  add_new_task task_cfabb out_regvals
	)
      )
    with Not_found ->
      try 
	let task_bb = List.find (fun func_bb -> func_bb.bb_label = task_bb_label) asm_fun.func_bbs in
	let task_cfabb = mkCFABB (Some task_in_regvals) task_bb None in
	let out_regvals, cfa_repr = analyze_bb task_in_regvals task_cfabb.cfa_bb in
	task_cfabb.cfa_repr <- cfa_repr;
	task_cfabb.cfa_out_regvals <- Some out_regvals;
	cfabbs := task_cfabb :: !cfabbs;
	add_new_task task_cfabb out_regvals
      with Not_found ->
	printf "missing basic block: %s\n" task_bb_label; flush stdout;
	raise (Failure "basic block label not found in function")
  done;
  let func_cfa_repr =
    List.flatten (List.map (fun cfabb -> cfabb.cfa_repr) !cfabbs)
  in
  (*dump_cfa_repr func_cfa_repr;*)
  let sorted_func_cfa_repr = 
    List.sort (fun (lopc1, cfa_exprs1) (lopc2, cfa_exprs2) -> 
      if lopc1 < lopc2 then -1
      else if lopc1 > lopc2 then 1
      else 0
    ) func_cfa_repr
  in
  if (List.length sorted_func_cfa_repr) <> (List.length func_cfa_repr) then
    printf "%d : %d" (List.length sorted_func_cfa_repr) (List.length func_cfa_repr);
  flush stdout;
  (*let prev = ref (MWord.of_int 0) in
  printf "duplicated addresses:\n";
  List.iter (fun (addr,cfa_repr) ->
    if !prev = addr then printf "%s\n" (str_of_mword_flex addr);
    prev := addr
  ) func_cfa_repr;
  flush stdout;*)
  (*dump_cfa_repr sorted_func_cfa_repr;*)
  (*sorted_func_cfa_repr*)
  (*
  let standardize_func_cfa_repr sorted_cfarepr =
    let addr_list, cfarepr_list = List.split sorted_cfarepr in
    let get_esp = Coq_get_loc_rtl_exp (Big.of_int 31, X86_MACHINE.Coq_reg_loc(ESP)) in
    let initial_cfa = 
      (0,Coq_arith_rtl_exp (Big.of_int 31, Coq_add_op, get_esp, Coq_imm_rtl_exp (Big.of_int 31, Big.of_int 4)))
    in
    let new_cfarepr_list = [initial_cfa] :: (List.rev (List.tl (List.rev cfarepr_list))) in
    List.combine addr_list new_cfarepr_list
  in
  let standard_func_cfa_repr = standardize_func_cfa_repr sorted_func_cfa_repr in*)
  (*dump_cfa_repr 1 standard_func_cfa_repr;*)
  sorted_func_cfa_repr

let mustcfaGen asm_fun_list =
  List.map (fun asm_fun ->
    (*printf "Dealing with function %s...\n" asm_fun.func_name; flush stdout;*)
    asm_fun, must_cfa_repr_gen asm_fun
  ) asm_fun_list

let maycfaGen asm_fun_list =
  List.map (fun asm_fun ->
    (*printf "Dealing with function %s...\n" asm_fun.func_name; flush stdout;*)
    asm_fun, may_cfa_repr_gen asm_fun
  ) asm_fun_list

let dump_cfaInfo cfaInfo =
  List.iter (fun (asm_fun, cfarepr)-> 
    printf "Function %s:\n" asm_fun.func_name;
    (*List.iter (fun bb -> Ibaux.print_bb stdout bb (MWord.of_string "0x8048000")) asm_fun.func_bbs;
    printf "\n";*)
    dump_cfa_repr 1 cfarepr
  ) cfaInfo

let query_about_reg cfa_exprs reg =
  List.find_map (fun (lvl,rtle) ->
    if lvl = 0 then
      begin match rtle with
      | Coq_get_loc_rtl_exp(s,loc) ->
	begin match loc with
	| X86_MACHINE.Coq_reg_loc reg_loc ->
	  if reg_loc = reg then Some 0
	  else None
	| _ -> None
	end
      | Coq_arith_rtl_exp (s,bvop,re1,re2) ->
	let reg_loc =
	  begin match re1 with
	  | Coq_get_loc_rtl_exp(s,loc) ->
	    begin match loc with
	    | X86_MACHINE.Coq_reg_loc reg_loc ->
	      reg_loc
	    | _ -> assert false
	    end
	  | _ -> assert false
	  end
	in
	if reg_loc = reg then
	  begin match bvop with
	  | Coq_add_op ->
	    begin match re2 with
	    | Coq_imm_rtl_exp(s,v) ->
	      Some (-(Big.to_int v))
	    | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
	      None
	    | _ -> assert false
	    end
	  | Coq_sub_op ->
	    begin match re2 with
	    | Coq_imm_rtl_exp(s,v) ->
	      Some (Big.to_int v)
	    | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
	      None
	    | _ -> assert false
	    end
	  | _ -> assert false
	  end
	else None
      | _ -> assert false
      end
    else
	(** Complex representation requires extra effort **)
      None    
  ) cfa_exprs
