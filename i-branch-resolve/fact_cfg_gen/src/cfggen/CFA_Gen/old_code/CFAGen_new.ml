open Batteries
module P = Printf
module H = Hashtbl

open X86Syntax
open X86Semantics
open X86_RTL
open Config
open Abbrev

open BB
open Ibaux
open RTL_opt

let big32_to_int v = 
  Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int v))
    
type offset = int

type size = int

type func_name = string

type abs_addr = string

type malloc_site = string

type abstract_loc =
| AL_Reg of register
| AL_Local of func_name * int * int
| AL_Global of segment_register * abs_addr * int
| AL_Heap of malloc_site * int * int

let str_of_aloc aloc =
  match aloc with
  | AL_Reg reg ->
    Printer.str_of_reg reg
  | AL_Local (fname, off, size) ->
    if off >= 0 then
      P.sprintf "%s:(0x%x+%x)" fname off size
    else
      P.sprintf "%s:(-0x%x+%x)" fname (-off) size
  | AL_Global (seg_reg, abs_addr, size) ->
    P.sprintf "%s:%s+%x" (Instr.str_of_segreg seg_reg) abs_addr size
  | AL_Heap (msite, off, size) ->
    if off >= 0 then
      P.sprintf "Malloc@%s:(0x%x+%x)" msite off size
    else
      P.sprintf "Malloc@%s:(-0x%x+%x)" msite off size

type absloc_table = (abstract_loc, unit) H.t

let dump_stk_aloctbl aloctbl =
  H.iter (fun (off,size) _ ->
    if off >= 0 then
      P.printf "(0x%x+%x)\n" off size
    else
      P.printf "(-0x%x+%x)\n" (-off) size
  ) aloctbl

type execution_point = int * bool
  
let str_of_exepnt exepnt =
  if snd exepnt then
    P.sprintf "%x+" (fst exepnt)
  else
    P.sprintf "%x" (fst exepnt)
      
type value_set_type = rtl_exp list

let str_of_value_set vset =
  String.concat "" (List.map (fun re ->
    P.sprintf "%s\n" (Printer.str_of_rtl_exp_less_size re)
  ) vset)
  
let rec eq_rtl_exp rtl_exp1 rtl_exp2 =
  match rtl_exp1, rtl_exp2 with
  | Coq_arith_rtl_exp(s,bvop,re1,re2), Coq_arith_rtl_exp(s',bvop',re1',re2') ->
    Big.eq s s' && bvop = bvop' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2'
  | Coq_test_rtl_exp(s,top,re1,re2), Coq_test_rtl_exp(s',top',re1',re2') ->
    Big.eq s s' && top = top' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2'
  | Coq_if_rtl_exp(s,re1,re2,re3), Coq_if_rtl_exp(s',re1',re2',re3') ->
    Big.eq s s' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2' && eq_rtl_exp re3 re3'
  | Coq_cast_s_rtl_exp(s1,s2,re), Coq_cast_s_rtl_exp(s1',s2',re') ->
    Big.eq s1 s1' && Big.eq s2 s2' && eq_rtl_exp re re'
  | Coq_cast_u_rtl_exp(s1, s2, re), Coq_cast_u_rtl_exp(s1', s2', re') ->
    Big.eq s1 s1' && Big.eq s2 s2' && eq_rtl_exp re re'
  | Coq_imm_rtl_exp(s, v), Coq_imm_rtl_exp(s', v') ->
    Big.eq s s' && Big.eq v v'
  | Coq_get_loc_rtl_exp(s, loc), Coq_get_loc_rtl_exp(s', loc') ->
    Big.eq s s' && loc = loc'
  | Coq_get_ps_reg_rtl_exp(s, ps_reg), Coq_get_ps_reg_rtl_exp(s',ps_reg') ->
    Big.eq s s' && Big.eq ps_reg ps_reg'
(*
  | Coq_get_ps_reg_rtl_exp(s,ps_reg), _ 
  | _, Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    (Big.to_string ps_reg) = "-2"
*)
  | Coq_get_array_rtl_exp(l, s, arr, re), Coq_get_array_rtl_exp(l',s',arr',re') ->
    Big.eq l l' && Big.eq s s' && arr = arr' && eq_rtl_exp re re'
  | Coq_get_byte_rtl_exp(re), Coq_get_byte_rtl_exp(re') ->
    eq_rtl_exp re re'
  | Coq_get_random_rtl_exp(s),Coq_get_random_rtl_exp(s') ->
    Big.eq s s'
  | Coq_farith_rtl_exp(ew,mw,faop,rtl_rm,re1,re2),
    Coq_farith_rtl_exp(ew',mw',faop',rtl_rm',re1',re2') ->
    Big.eq ew ew' && Big.eq mw mw' && faop = faop' && eq_rtl_exp rtl_rm rtl_rm' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2'
  | Coq_fcast_rtl_exp(s1,s2,s3,s4,re1,re2),Coq_fcast_rtl_exp(s1',s2',s3',s4',re1',re2') ->
    Big.eq s1 s1' && Big.eq s2 s2' && Big.eq s3 s3' && Big.eq s4 s4' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2'
  | _,_ -> false

let sub_vset vset1 vset2 =
  List.for_all (fun re ->
    List.exists (eq_rtl_exp re) vset2
  ) vset1 

let eq_vset vset1 vset2 =
  sub_vset vset1 vset2 &&
    sub_vset vset2 vset1
    
let union_vset vset1 vset2 =
  (*P.printf "%s U %s = \n" (str_of_value_set vset1)
    (str_of_value_set vset2);*)
  let res = List.unique ?eq:(Some eq_rtl_exp) (vset1 @ vset2) in
  (*P.printf "%s\n" (str_of_value_set res);*)
  res

type value_set_table = (abstract_loc, value_set_type) H.t
  
let intersect_vsettbl_tofst dest_vsettbl src_vsettbl =
  H.iter (fun aloc vset ->
    if not (H.mem src_vsettbl aloc) then
      H.remove dest_vsettbl aloc
    else if not (eq_vset (H.find src_vsettbl aloc) vset) then
      H.remove dest_vsettbl aloc
    else ()
  ) dest_vsettbl

let union_vsettbl_tofst dest_vsettbl src_vsettbl =
  H.iter (fun aloc vset ->
    if not (H.mem dest_vsettbl aloc) then
      H.add dest_vsettbl aloc vset
    else 
      H.replace dest_vsettbl aloc (union_vset vset (H.find dest_vsettbl aloc))
  ) src_vsettbl

let sub_vsettbl vsettbl1 vsettbl2 =
  let sub_b = ref true in
  H.iter (fun aloc vset ->
    if H.mem vsettbl2 aloc then
      sub_b := eq_vset (H.find vsettbl2 aloc) vset
    else
      sub_b := false
  ) vsettbl1;
  !sub_b

let eq_vsettbl vsettbl1 vsettbl2 =
  sub_vsettbl vsettbl1 vsettbl2 &&
    sub_vsettbl vsettbl2 vsettbl1

type program_vset_tables = (execution_point, value_set_table) H.t

let dump_vset_table vsettbl =
  H.iter (fun aloc vset ->
    P.printf "%s == \n%s\n" (str_of_aloc aloc) (str_of_value_set vset)
  ) vsettbl
    
let dump_prog_vset_tables prog_vsettbls =
  P.printf "Program value set table:\n";
  H.iter (fun exepnt vsettbl ->
    P.printf "%s --> \n" (str_of_exepnt exepnt);
    dump_vset_table vsettbl
  ) prog_vsettbls

let rtl_imm size v =
  Coq_imm_rtl_exp (Big.of_int (size-1), Big.of_int v)

let rtl_sub size operand1 operand2 =
  Coq_arith_rtl_exp (Big.of_int (size-1), Coq_sub_op, operand1, operand2)

let rtl_add size operand1 operand2 =
  Coq_arith_rtl_exp (Big.of_int (size-1), Coq_add_op, operand1, operand2)

let patch_for_may_analysis asm_fun bb_instrs =
  List.map (fun (pre,instr,len) -> 
    match instr with
    | CALL(_,_,_,_) -> 
      (pre,NOP (Imm_op (Big.of_int 0)),len)
    | AND(_,op1,op2) ->
      if asm_fun.func_name = "main" then
	begin match op1, op2 with
	| Reg_op reg, Imm_op v 
	| Imm_op v, Reg_op reg ->
	  if reg = ESP && (unsigned32_to_hex v) = "0xfffffff0" then
	    (pre,NOP (Imm_op (Big.of_int 0)),len)
	  else
	    pre,instr,len
	| _, _ -> pre,instr,len
	end
      else
	(pre,instr,len)
    | _ -> (pre,instr,len)
  ) bb_instrs

let patch_for_must_analysis asm_fun bb_instrs =
  List.map (fun (pre,instr,len) -> 
    match instr with
    | CALL(_,_,_,_) -> 
      (pre, MOV (true, (Reg_op EAX), (Imm_op (Big.of_int 0))),len)
    | AND(_,op1,op2) ->
      if asm_fun.func_name = "main" then
	begin match op1, op2 with
	| Reg_op reg, Imm_op v 
	| Imm_op v, Reg_op reg ->
	  if reg = ESP && (unsigned32_to_hex v) = "0xfffffff0" then
	    (pre,NOP (Imm_op (Big.of_int 0)),len)
	  else
	    pre,instr,len
	| _, _ -> pre,instr,len
	end
      else
	(pre,instr,len)
    | _ -> (pre,instr,len)
  ) bb_instrs

type vsa_task = {
  vt_curr: string;
  vt_prev: string;
}
  
let mkVT curr prev = {
  vt_curr = curr;
  vt_prev = prev;
}

let loc_to_aloc loc = 
  match loc with
  | X86_MACHINE.Coq_reg_loc reg ->
    Some (AL_Reg reg)
  | _ -> None

let size_of_re re =
  match re with
  | Coq_arith_rtl_exp(s,_,_,_)
  | Coq_test_rtl_exp(s,_,_,_) ->
    ((Big.to_int s) + 1) / 8
  | Coq_if_rtl_exp(s, _,_,_) ->
    raise (Failure "Conditional expression met!")
  | Coq_cast_s_rtl_exp(s,_,_)    
  | Coq_cast_u_rtl_exp(s,_,_)
  | Coq_imm_rtl_exp(s,_)
  | Coq_get_loc_rtl_exp(s,_)
  | Coq_get_ps_reg_rtl_exp(s,_) 
  | Coq_get_array_rtl_exp(_,s,_,_) ->
    ((Big.to_int s) + 1) / 8
  | Coq_get_byte_rtl_exp(re) ->
    1
  | Coq_get_random_rtl_exp(s) ->
    ((Big.to_int s) + 1) / 8
  | _ ->
    assert false

let rec regs_in_re re =
  match re with
  | Coq_arith_rtl_exp(s,_,re1,re2)
  | Coq_test_rtl_exp(s,_,re1,re2) ->
    regs_in_re re1 @ regs_in_re re2
  | Coq_if_rtl_exp(s, ire, tre, ere) ->
    raise (Failure "Conditional expression met!")
  | Coq_cast_s_rtl_exp(s1,s2,re)    
  | Coq_cast_u_rtl_exp(s1,s2,re) ->
    regs_in_re re
  | Coq_imm_rtl_exp(s,v) ->
    []
  | Coq_get_loc_rtl_exp(s,loc) ->
    begin match loc with
    | X86_MACHINE.Coq_reg_loc(reg) -> [reg]
    | _ -> []
    end
  | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    []
  | Coq_get_array_rtl_exp(l,s,arr,re) ->
    []
  | Coq_get_byte_rtl_exp(re) ->
    regs_in_re re
  | Coq_get_random_rtl_exp(s) ->
    []
  | _ ->
    assert false


let ad_hoc_simpre re =
  match re with
  | X86_RTL.Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    if bvop = X86_RTL.Coq_or_op then
      begin match re1 with
      | X86_RTL.Coq_cast_u_rtl_exp(s1,s2,re') ->
	if Big.eq s1 (Big.of_int 7) &&
	  Big.eq s2 (Big.of_int 31) then
	  begin match re' with
	  | X86_RTL.Coq_cast_u_rtl_exp(s1',s2',re'') ->
	    if Big.eq s1' (Big.of_int 31) &&
	      Big.eq s2' (Big.of_int 7) then
	      re''
	    else re
	  | _ -> re
	  end
	else re
      | _ -> re
      end
    else re
  | _ -> re

let rec addr_re_to_global addr_re ai vsettbl seg_reg_option =
  (** 
      To do so, we must assume some patterns of RTL
      expressions for representing global memory
      addresses:
      1. There is no registers
      2. Addresses are represented by imm_rtl_exp only

      Might be easier to directly look at assembly
      instruction to determine the destination.
  **)
  let check_instr () =
    P.printf "%s\n" (Instr.str_of_instr ai);
    List.iter (fun ri ->
      Printer.print_rtl_instr ri
    ) (X86_Compile.instr_to_rtl (fst ai) (snd ai));
    P.printf "%s\n" (Printer.str_of_rtl_exp_less_size addr_re);
    flush stdout
  in

  (*P.printf "Original:\n%s\n" (Printer.str_of_rtl_exp_less_size addr_re);*)
  let simp_addr_re = 
    simp_re [constant_fold] addr_re
  in
  (*P.printf "Simplified:\n%s\n" (Printer.str_of_rtl_exp_less_size simp_addr_re);*)
  let pre, ins = ai in
  match ins with
  | PUSH _ | JMP _ -> 
    raise (Failure "Don't care")
  | _ ->
    begin match simp_addr_re with
    | Coq_arith_rtl_exp(s,_,re1,re2) ->
      begin match re1 with
      | Coq_get_loc_rtl_exp(s,loc) ->
	begin match loc with
	| X86_MACHINE.Coq_seg_reg_start_loc seg_reg when seg_reg = DS || seg_reg = ES || seg_reg = GS ->
	  addr_re_to_global re2 ai vsettbl (Some seg_reg)
	| _ ->
	  check_instr ();
	  assert false
	end
      | Coq_imm_rtl_exp _
      | Coq_arith_rtl_exp _ ->
	raise (Failure "Not global")
      | _ ->
	check_instr ();
	assert false
      end
    | Coq_test_rtl_exp(s,_,re1,re2) ->
      assert false
    | Coq_if_rtl_exp(s, ire, tre, ere) ->
      raise (Failure "Conditional expression met!")
    | Coq_cast_s_rtl_exp(s1,s2,re)    
    | Coq_cast_u_rtl_exp(s1,s2,re) ->
      addr_re_to_global re ai vsettbl seg_reg_option
    | Coq_imm_rtl_exp(s,v) ->
      AL_Global ((Option.get seg_reg_option), (str_of_mword_flex (MWord.of_big_int v)), 1)
    | Coq_get_loc_rtl_exp(s,loc) ->
      assert false
    | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
      assert false
    | Coq_get_array_rtl_exp(l,s,arr,re) ->
      assert false
    | Coq_get_byte_rtl_exp(re) ->
      assert false
    | Coq_get_random_rtl_exp(s) ->
      assert false
    | _ ->
      assert false
    end      

let off_in_simpre simpre =
  match simpre with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    begin match re2 with
    | Coq_imm_rtl_exp(s,v) ->
      v
    | _ -> 
      assert false
    end
  | Coq_get_loc_rtl_exp(s,ps_reg) ->
    Big.of_int 0
  | _ ->
    assert false

let addr_re_to_local re ai vsettbl fname =
  (**TODO: this has a bug of doing integer computation**)


  assert (List.length (regs_in_re re) = 1);
  let simp_addr_re = simp_re [constant_fold] re in
  let base_reg = List.hd (regs_in_re simp_addr_re) in
  let off = off_in_simpre simp_addr_re in
  let cfa_repr_of_base_reg = 
    List.hd (H.find vsettbl (AL_Reg base_reg))
  in
  let new_cfa_repr =
    Coq_arith_rtl_exp(Big.of_int 31, Coq_add_op,
		      cfa_repr_of_base_reg, Coq_imm_rtl_exp(Big.of_int 31, off))
  in
  let newoff = 
    off_in_simpre (simp_re [constant_fold] new_cfa_repr)
  in
  AL_Local(fname,Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int newoff)),1)


let addr_re_to_heap re ai vsettbl =
  raise (Failure "Implementing...")
      
let addr_re_to_mem addr_re ai vsettbl fname =
  (** In this case, we know the target is to
      transform an address expression to an
      abstract location of memory;
      also, we know that there is only one
      register in the expression
  **)
  let check_instr () =
    P.printf "%s\n" (Instr.str_of_instr ai);
    List.iter (fun ri ->
      Printer.print_rtl_instr ri
    ) (X86_Compile.instr_to_rtl (fst ai) (snd ai));
    P.printf "%s\n" (Printer.str_of_rtl_exp_less_size addr_re);
    flush stdout
  in
  match addr_re with
  | Coq_arith_rtl_exp(s,_,re1,re2) ->
    begin match re1 with
    | Coq_get_loc_rtl_exp(s,loc) ->
      begin match loc with
      | X86_MACHINE.Coq_seg_reg_start_loc seg_reg -> 
	if seg_reg = SS then
	  addr_re_to_local re2 ai vsettbl fname
	else if seg_reg = DS || seg_reg = ES then (
	  (*P.printf "Address RE:\n%s\n" (Printer.str_of_rtl_exp_less_size addr_re);*)
	  addr_re_to_heap re2 ai vsettbl
	)
	else (
	  (*P.printf "Address RE:\n%s\n" (Printer.str_of_rtl_exp_less_size addr_re);*)
	  check_instr ();
	  assert false
	)
      | _ ->
	check_instr ();
	assert false
      end
    | Coq_imm_rtl_exp _ ->
      raise (Failure "Not global")
    | _ ->
      check_instr ();
      assert false
    end
  | _ ->
    raise (Failure "Implementing...")
      
let addr_to_aloc addr_re ai vsettbl fname =
  try
    let addr_re_regs = regs_in_re addr_re in
    if List.length addr_re_regs = 0 then
    (** Should be AL_Global **)
      let global_aloc = addr_re_to_global addr_re ai vsettbl None in
      Some global_aloc
    else if List.length addr_re_regs = 1 then
    (** Should be AL_Heap or AL_Local **)
      if H.mem vsettbl (AL_Reg (List.hd addr_re_regs)) then
	let mem_aloc = addr_re_to_mem addr_re ai vsettbl fname in
	Some mem_aloc
      else None
    else if List.length addr_re_regs = 2 then
    (** Should be complex AL_Heap or AL_Local **)
      None
    else
      assert false
  with Failure _ ->
    (* A failure case whne if expression is met *)
    None
	
let rec apply_vsettbl vsettbl re ai fname = 
  match re with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    let res1 = apply_vsettbl vsettbl re1 ai fname in
    let res2 = apply_vsettbl vsettbl re2 ai fname in
    List.flatten (List.map (fun new_re1 ->
      List.map (fun new_re2 ->
	Coq_arith_rtl_exp(s,bvop,new_re1,new_re2)	
      ) res2
    ) res1)
  | Coq_test_rtl_exp(s,top,re1,re2) ->
    let res1 = apply_vsettbl vsettbl re1 ai fname in
    let res2 = apply_vsettbl vsettbl re2 ai fname in
    List.flatten (List.map (fun new_re1 ->
      List.map (fun new_re2 ->
	Coq_test_rtl_exp(s,top,new_re1,new_re2)
      ) res2
    ) res1)
  | Coq_if_rtl_exp(s, ire, tre, ere) ->
    let new_ires = apply_vsettbl vsettbl ire ai fname in
    let new_tres = apply_vsettbl vsettbl tre ai fname in
    let new_eres = apply_vsettbl vsettbl ere ai fname in
    List.flatten (List.map (fun new_ire ->
      List.flatten (List.map (fun new_tre ->
	List.map (fun new_ere ->
	  Coq_if_rtl_exp(s, new_ire, new_tre, new_ere)
	) new_eres
      ) new_tres)
    ) new_ires)	      
  | Coq_cast_s_rtl_exp(s1,s2,re) ->
    List.map (fun new_re ->
      Coq_cast_s_rtl_exp(s1,s2,new_re)
    ) (apply_vsettbl vsettbl re ai fname)
  | Coq_cast_u_rtl_exp(s1,s2,re) ->
    List.map (fun new_re ->
      Coq_cast_u_rtl_exp(s1,s2,new_re)
    ) (apply_vsettbl vsettbl re ai fname)
  | Coq_imm_rtl_exp(s,v) ->
    [re]
  | Coq_get_loc_rtl_exp(s,loc) ->
    begin match loc with
    | X86_MACHINE.Coq_reg_loc(reg) ->
      begin try H.find vsettbl (AL_Reg reg)
	with Not_found -> [re]
      end
    | _ -> [re] 
    end
  | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    [re]
  | Coq_get_array_rtl_exp(l,s,arr,index_re) ->
    List.map (fun new_re ->
      Coq_get_array_rtl_exp(l,s,arr,new_re)
    ) (apply_vsettbl vsettbl index_re ai fname)
  | Coq_get_byte_rtl_exp(re') ->
    let target_aloc = addr_to_aloc re' ai vsettbl fname in
    if target_aloc = None then [re]
    else begin try H.find vsettbl (Option.get target_aloc)
      with Not_found -> [re]
    end
  | Coq_get_random_rtl_exp(s) ->
    [re]
  | _ ->
    assert false

let rec cfa_must_in_re re = 
  match re with
  | Coq_arith_rtl_exp(s,_,re1,re2)
  | Coq_test_rtl_exp(s,_,re1,re2) ->
    cfa_must_in_re re1 || cfa_must_in_re re2
  | Coq_if_rtl_exp(s, ire, tre, ere) ->
    false
  | Coq_cast_s_rtl_exp(s1,s2,re)    
  | Coq_cast_u_rtl_exp(s1,s2,re) ->
    cfa_must_in_re re
  | Coq_imm_rtl_exp(s,v) ->
    false
  | Coq_get_loc_rtl_exp(s,loc) ->
    false
  | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    Big.eq ps_reg (Big.of_int (-1))
  | Coq_get_array_rtl_exp(l,s,arr,re) ->
    false
  | Coq_get_byte_rtl_exp(re) ->
    false
    (*cfa_must_in_re re*)
  | Coq_get_random_rtl_exp(s) ->
    false
  | _ ->
    assert false

let cfa_repr_of_re re vsettbl ai fname =
  (*P.printf "Before transformation:\n%s\n" (Printer.str_of_rtl_exp_less_size re);*)
  let transformed_res = apply_vsettbl vsettbl re ai fname in
  let evaluated_res =
    List.map (fun transformed_re ->
      (**TODO: implement new expression folding**)
      (*P.printf "Before evaluation:\n%s\n" (Printer.str_of_rtl_exp_less_size transformed_re);*) 
      let evaluated_re = simp_re [constant_fold; ad_hoc_simpre] transformed_re in
      (*P.printf "After evaluation:\n%s\n" (Printer.str_of_rtl_exp_less_size evaluated_re);*)
      evaluated_re
(*      (simp_re simpre_all_methods transformed_re)*)
    ) transformed_res
  in
  let cfa_in_res =
    List.filter cfa_must_in_re evaluated_res
  in
  List.unique ?eq:(Some eq_rtl_exp) cfa_in_res

let reverse_vsettbl vsettbl =
  H.fold (fun aloc re_list cfa_exprs ->
    match aloc with
    | AL_Reg reg ->
      let get_reg = Coq_get_loc_rtl_exp(Big.of_int 31, X86_MACHINE.Coq_reg_loc reg) in
      let exprs = 
	List.map (fun re ->
	  Coq_test_rtl_exp(Big.of_int 0,Coq_eq_op,get_reg,re)
	) re_list
      in
      let new_cfaexprs =
	List.filter_map (fun re ->
	  begin match re with
	  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
	    begin match re1 with
	    | Coq_get_ps_reg_rtl_exp(s, ps_reg) when Big.to_int ps_reg = (-1) ->
	      begin match bvop with
	      | Coq_add_op ->
		begin match re2 with
		| Coq_get_ps_reg_rtl_exp(s, ps_reg2) when Big.to_int ps_reg2 = (-2) ->
		  Some (Coq_arith_rtl_exp(s,Coq_sub_op,get_reg,re2))
		| _ ->
		  Some (Coq_arith_rtl_exp(s,Coq_sub_op,get_reg,re2))
		end
	      | Coq_sub_op ->
		begin match re2 with
		| Coq_get_ps_reg_rtl_exp(s, ps_reg2) when Big.to_int ps_reg2 = (-2) ->
		  Some (Coq_arith_rtl_exp(s,Coq_add_op,get_reg,re2))
		| _ ->
		  Some (Coq_arith_rtl_exp(s,Coq_add_op,get_reg,re2))
		end
	      | _ -> None
	      end
	    | _ -> None
	    end
	  | Coq_get_ps_reg_rtl_exp(s,ps_reg) when Big.to_int ps_reg = (-1) ->
	    Some get_reg
	  | _ -> None
	  end
	) re_list
      in
      new_cfaexprs :: cfa_exprs
    | _ -> cfa_exprs
  ) vsettbl []

let backward_compatible prog_vsettbl =
  let func_cfa_repr = 
    H.fold (fun exepnt vsettbl l ->
      let reversed_vsettbl = reverse_vsettbl vsettbl in
      if snd exepnt then l
      else
	let lopc = fst exepnt in
	(lopc, reversed_vsettbl) :: l
    ) prog_vsettbl []
  in
  let sorted_func_cfa_repr = 
    List.sort (fun (lopc1, cfa_exprs1) (lopc2, cfa_exprs2) ->
      if lopc1 < lopc2 then -1
      else if lopc1 > lopc2 then 1
      else 0
    ) func_cfa_repr
  in
  let backward_compatible_result =
    List.map (fun (lopc, cfa_exprs) ->
      let res = 
	List.filter_map (fun re_list ->
	  if List.is_empty re_list then
	    None
	  else if List.length re_list = 1 then
	    Some (0, (List.hd re_list))
	  else
	    None
	) cfa_exprs
      in
      (lopc, res)
    ) sorted_func_cfa_repr
  in
  backward_compatible_result

let gen_aloctbls asm_fun = 
  let stk_aloctbl = H.create 32 in
  let glb_aloctbl = H.create 32 in
  stk_aloctbl, glb_aloctbl

let gen_must_PntToStk asm_fun = 
  (**TODO: this function is buggy; 
     fix it with Nginx_o1's ngx_signal_handler **)
  let stk_aloctbl = H.create 32 in
  let glb_aloctbl = H.create 32 in
  let prog_vsettbl = H.create 32 in  
  
  let combine_sorted_ranges sorted_ranges =
    if List.is_empty sorted_ranges then []
    else if List.length sorted_ranges = 1 then
      [List.hd sorted_ranges]
    else
      let hd = List.hd sorted_ranges in
      let last = List.last sorted_ranges in
      [fst hd, fst last]
  in
  
  let update_aloctbl alocs =
    List.iter (fun aloc ->
      begin match aloc with
      | AL_Local (fname, off, size) ->
	H.replace stk_aloctbl (off,size) ()
      | AL_Global (segreg, absaddr, size) ->
	H.replace glb_aloctbl (absaddr,size) ()
      | _ -> ()
      end
    ) alocs
  in      

  let bb_map = H.create 32 in
  List.iter (fun bb ->
    H.add bb_map bb.bb_label bb;
  ) asm_fun.func_bbs;

  let in_vsettbl_map = H.create 32 in
  let out_vsettbl_map = H.create 32 in
  List.iter (fun bb ->
    let empty_in_vsettbl = H.create 32 in
    H.add in_vsettbl_map bb.bb_label empty_in_vsettbl;
    let empty_out_vsettbl = H.create 32 in
    H.add out_vsettbl_map bb.bb_label empty_out_vsettbl
  ) asm_fun.func_bbs;

  let cfa_pseudo_reg =
    Coq_get_ps_reg_rtl_exp (Big.of_int 31, Big.of_int (-1))
  in

  let initial_esp_cfa_expr =
    rtl_sub 32 cfa_pseudo_reg (rtl_imm 32 4)
  in

  let initial_esp_vset = 
    [initial_esp_cfa_expr]
  in

  let entry_bb_label =
    (List.hd asm_fun.func_bbs).bb_label
  in
  assert (entry_bb_label = asm_fun.func_low);

  H.add (H.find in_vsettbl_map entry_bb_label) (AL_Reg ESP) initial_esp_vset;

  H.add stk_aloctbl (0,4) (); (** CFA itself **)
  H.add stk_aloctbl ((-4),4) (); (** Return address **)
  
  (*
  H.iter (fun relAddr in_vsettbl ->
    P.printf "%x --> \n" relAddr;
    dump_vset_table in_vsettbl
    ) in_vsettbl_map;
  *)

  let dest_alocs_in_ri ri ai vsettbl =
    match ri with
    | Coq_set_loc_rtl(s,re,loc) ->
      let target_aloc = loc_to_aloc loc in
      if target_aloc = None then []
      else [Option.get target_aloc]
    | Coq_set_byte_rtl(vre, are) ->
      let target_aloc = addr_to_aloc are ai vsettbl asm_fun.func_name in
      if target_aloc = None then []
      else [Option.get target_aloc]
    | _ -> []
  in

  let analyze_ri ri ai vsettbl =
    let dest_alocs = 
      List.flatten (List.map (fun ri -> 
	dest_alocs_in_ri ri ai vsettbl
      ) [ri])
    in
    (*P.printf "%s has dest alocs:\n" (Instr.str_of_instr ai);    
    List.iter (fun aloc ->
      P.printf "%s\t" (str_of_aloc aloc);
    ) dest_alocs;
    P.printf "\n";
    *)

    let local_ranges = List.filter_map (fun aloc ->
      match aloc with
      | AL_Local (fname, off, size) ->
	Some (off,off+size)
      | _ -> None
    ) dest_alocs
    in
    let sorted_local_ranges = List.sort (fun (off1,_) (off2,_) ->
      off1 - off2 
    ) local_ranges
    in
    let combined_ranges = combine_sorted_ranges sorted_local_ranges in
    List.iter (fun range ->
      H.replace stk_aloctbl (fst range, ((snd range) - (fst range))) ()
    ) combined_ranges;


    match ri with
    | Coq_set_loc_rtl(s,re,loc) ->
      let target_aloc = loc_to_aloc loc in
      let target_vset = cfa_repr_of_re re vsettbl ai asm_fun.func_name in
      if target_aloc = None then ()
      else if target_vset = [] then (
	H.remove vsettbl (Option.get target_aloc)
      (** TODO: conflicted alocs should be considered also **)
      )
      else ( 
	H.replace vsettbl (Option.get target_aloc) target_vset
      )
    | Coq_set_ps_reg_rtl(s,re,psreg) -> 
      ()
    | Coq_set_array_rtl(s1,s2,arr,ire,vre) ->
      ()
    | Coq_set_byte_rtl(vre, are) ->
      let target_aloc = addr_to_aloc are ai vsettbl asm_fun.func_name in
      let target_vset = cfa_repr_of_re vre vsettbl ai asm_fun.func_name in
      if target_aloc = None then ()
      else if target_vset = [] then
	H.remove vsettbl (Option.get target_aloc)
      else
	H.replace vsettbl (Option.get target_aloc)
	  target_vset
    | Coq_advance_oracle_rtl ->
      ()
    | Coq_if_rtl(guard,ri') ->
      ()
    | Coq_error_rtl ->
      ()
    (*raise (Failure "Unexpected RTL Error Instruction in CFAGen")*)
    | Coq_trap_rtl ->
      ()
  (*raise (Failure "Unexpected RTL Trap Instruction in CFAGen")*)
  in
  
  let perform_vsa taskbb in_vsettbl out_vsettbl =
    (** Use the instructions in taskbb and the in_vsettbl
	to compute a new out_vsettbl;
	if the new and the old are the same then return
	false;
	else update the out_vsettbl_map and return true
    **)

    let new_out_vsettbl = H.copy in_vsettbl in
    List.iter (fun (pre,instr,len) ->
      let rtl_instrs = X86_Compile.instr_to_rtl pre instr in
      List.iter (fun ri ->
	analyze_ri ri (pre,instr) new_out_vsettbl
      ) rtl_instrs
    ) (patch_for_must_analysis asm_fun taskbb.bb_instrs);
    (*dump_vset_table new_out_vsettbl;
    dump_vset_table out_vsettbl;*)

    let continue_sign = not (eq_vsettbl out_vsettbl new_out_vsettbl) in
    if continue_sign then
      H.replace out_vsettbl_map taskbb.bb_label new_out_vsettbl;
    continue_sign      
  in

  let analyze_bb taskbb task_in_vsettbl = 
    try
      let in_vsettbl = H.find in_vsettbl_map taskbb.bb_label in
      if H.length in_vsettbl = 0 then
	(* Should test if it is the first time of update*)
	H.iter (fun aloc vset ->
	  H.add in_vsettbl aloc vset
	) task_in_vsettbl
      else
	intersect_vsettbl_tofst in_vsettbl task_in_vsettbl;
      let out_vsettbl = 
	H.find out_vsettbl_map taskbb.bb_label 
      in
      (*
      dump_vset_table in_vsettbl;
      dump_vset_table out_vsettbl;
      *)
      let push_new_b =
	perform_vsa taskbb in_vsettbl out_vsettbl
      in
      push_new_b
    with Not_found ->
      assert false
  in

  let workq = Queue.create () in
  
  let add_new_tasks bb =
    if List.is_empty bb.bb_inter_succs then
      List.iter (fun succlbl ->
	if H.mem bb_map succlbl then
	  let newtask = mkVT
	    succlbl bb.bb_label
	  in
	  Queue.push newtask workq
      ) bb.bb_succs
    else 
      List.iter (fun succlbl ->
	if H.mem bb_map succlbl then
	  let newtask = mkVT
	    succlbl bb.bb_label
	  in
	  Queue.push newtask workq
	else (
	  P.printf "%s not found for %s!\n" succlbl bb.bb_label;
	  flush stdout;
	)
      ) bb.bb_inter_succs
  in

  let entry_bb = List.hd asm_fun.func_bbs in
  let push_task_b = analyze_bb entry_bb (H.find in_vsettbl_map entry_bb_label) in
  if push_task_b then add_new_tasks entry_bb;
  while not (Queue.is_empty workq) do
    let task = Queue.take workq in
    let tasklbl = task.vt_curr in
    let task_prev = task.vt_prev in
    try
      let task_in_vsettbl = H.find out_vsettbl_map task_prev in
      let task_bb = H.find bb_map tasklbl in
      let push_task_b = analyze_bb task_bb task_in_vsettbl in
      if push_task_b then add_new_tasks task_bb
    with Not_found ->
      assert false
  done;
  H.iter (fun bblbl vsettbl ->
    let bb = H.find bb_map bblbl in
    let address = ref bb.bb_relAddr in
    let target_vsettbl = H.copy vsettbl in
    List.iter (fun (pre,ins,len) ->
      let ai = pre,ins in
      let exepnt = !address, false in
      H.add prog_vsettbl exepnt (H.copy target_vsettbl);
      let rtl_instrs = X86_Compile.instr_to_rtl pre ins in
      List.iter (fun ri ->
	match ri with
	| Coq_set_loc_rtl(s,re,loc) ->
	  let target_aloc = loc_to_aloc loc in
	  let target_vset = 
	    cfa_repr_of_re re target_vsettbl ai asm_fun.func_name 
	  in
	  if target_aloc = None then ()
	  else if target_vset = [] then
	    H.remove target_vsettbl (Option.get target_aloc)
	  else 
	    H.replace target_vsettbl (Option.get target_aloc)
	      target_vset
	| Coq_set_byte_rtl(vre, are) ->
	  let target_aloc = addr_to_aloc are ai vsettbl asm_fun.func_name in
	  let target_vset = 
	    cfa_repr_of_re vre target_vsettbl ai asm_fun.func_name in
	  if target_aloc = None then ()
	  else if target_vset = [] then
	    H.remove target_vsettbl (Option.get target_aloc)
	  else
	    H.replace target_vsettbl (Option.get target_aloc)
	      target_vset
	| _ -> ()
      ) rtl_instrs;
      address := !address + len
    ) (patch_for_must_analysis asm_fun bb.bb_instrs);
  ) in_vsettbl_map;
  stk_aloctbl, glb_aloctbl, prog_vsettbl

(*
type cfa_pattern =
| PTN_const_off of bool
| PTN_cast_to_8 of int * bool
| PTN_unknown of rtl_exp

let sig_of_pattern ptn =
  let size32 = Big.of_int 31 in
  let size8 = Big.of_int 7 in
  
  let cfa_psreg =
    Coq_get_ps_reg_rtl_exp (size32, Big.of_int (-1))
  in

  let random_v size t =
    Coq_get_ps_reg_rtl_exp(size, Big.of_int t)
  in

  let rand32 t = random_v size32 t in

  let coff_true_rand32 = rand32 (-2) in

  let coff_false_rand32 = rand32 (-3) in

  let coq_imm size v =
    Coq_imm_rtl_exp(size, Big.of_int v)
  in

  let coq_add size re1 re2 =
    Coq_arith_rtl_exp(size,Coq_add_op,re1,re2)
  in

  let coq_sub size re1 re2 =
    Coq_arith_rtl_exp(size,Coq_sub_op,re1,re2)
  in

  let rec coq_multi_shru num bits size re =
    if num = 0 then
      re
    else
      let new_re = 
	coq_multi_shru (num-1) bits size re
      in 
      Coq_arith_rtl_exp(size, Coq_shru_op, new_re, coq_imm size bits)
  in
  
  let coq_castu from_size to_size re =
    Coq_cast_u_rtl_exp(from_size,to_size,re)
  in

  match ptn with
  | PTN_const_off sign ->
    if sign then
      (** cfa + positive_or_zero **)
      coq_add size32 cfa_psreg coff_true_rand32
    else
      (** cfa + negative **)
      coq_sub size32 cfa_psreg coff_false_rand32
  | PTN_cast_to_8 (shru_num, sign) ->
    assert (shru_num <= 3);
    let core_re = 
      if sign then
	coq_add size32 cfa_psreg (rand32 (-4-shru_num))
      else
	coq_sub size32 cfa_psreg (rand32 (-8-shru_num))
    in
    coq_castu size32 size8 
      (coq_multi_shru shru_num 8 size32 core_re)
  | PTN_unknown re -> re


let conclude_pattern re =
  let rec num_of_shru inre =
    match inre with
    | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
      begin match bvop with
      | Coq_shru_op ->
	begin match re2 with
	| Coq_imm_rtl_exp(s,v) -> 
	  if Big.to_int v = 8 then
	    let num_option = num_of_shru re1 in
	    if num_option = None then None
	    else
	      Some ((Option.get num_option) + 1)
	  else
	    None
	| _ -> None
	end
      | Coq_add_op | Coq_sub_op ->
	begin match re1 with
	| Coq_get_ps_reg_rtl_exp(s,psreg) when (Big.to_int psreg) = (-1) ->
	  begin match re2 with
	  | Coq_get_ps_reg_rtl_exp(s,psreg') when (Big.to_int psreg') <= (-2) ->
	    Some 0
	  | Coq_imm_rtl_exp(s,v) ->
	    Some 0
	  | _ -> None
	  end
	| _ -> None
	end
      | _ -> None
      end
    | _ -> None
  in
  
  let special_symbol = special_symbol_in_re re in
  match (-special_symbol) with
  | 2 -> PTN_const_off true
  | 3 -> PTN_const_off false
  | n -> 
    if n >= 4 && n < 8 then
      PTN_cast_to_8 ((n - 4),true)
    else if 
  match re with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    begin match re1 with
    | Coq_get_ps_reg_rtl_exp(s,psreg) when (Big.to_int psreg) = (-1) ->
      begin match re2 with
      | Coq_get_ps_reg_rtl_exp(s,psreg') when (Big.to_int psreg') = (-2) ->
	begin match bvop with
	| Coq_add_op ->	PTN_const_off true
	| Coq_sub_op -> PTN_const_off false
	| _ -> assert false	  
	end
      | Coq_imm_rtl_exp(s,v) ->
	let signed_native_int_of_v = 
	  (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int v)))
	in
	if signed_native_int_of_v >= 0 then
	  PTN_const_off true
	else
	  PTN_const_off false
      | _ -> PTN_unknown re
      end
    | _ ->
      PTN_unknown re
    end
  | Coq_cast_u_rtl_exp(from_s,to_s,re') ->
    let num_of_shru_option = num_of_shru re' in
    if num_of_shru_option = None then PTN_unknown re
    else
      PTN_cast_to_8 (Option.get num_of_shru_option)  
  | _ ->
    PTN_unknown re

let constant_widening tol vsettbl =
  let widening vset =
    let ptntbl = H.create 32 in
    List.iter (fun re ->
      let re_ptn = conclude_pattern re in
      if H.mem ptntbl re_ptn then
	H.replace ptntbl re_ptn (re::(H.find ptntbl re_ptn))
      else
	H.add ptntbl re_ptn [re]
    ) vset;
    let new_vset = ref [] in
    H.iter (fun ptn leaves ->
      if List.exists (eq_rtl_exp (sig_of_pattern ptn)) leaves then
	new_vset := (sig_of_pattern ptn) :: !new_vset
      else if List.length leaves > tol then
	new_vset := (sig_of_pattern ptn) :: !new_vset
      else
	new_vset := leaves @ !new_vset
    ) ptntbl;
    !new_vset
  in
  H.iter (fun aloc vset ->
    H.replace vsettbl aloc (widening vset)
  ) vsettbl
*)

let gen_old_CFAInfo asm_fun =
  let _, _, must_PntToStk =
    Stats.time ("Intra-procedural must-CFA generation")
      gen_must_PntToStk asm_fun
  in
  let old_CFAInfo =
    backward_compatible must_PntToStk
  in
  old_CFAInfo

let dump_old_CFAInfo old_cfa =
  List.iter (fun (lopc,cfa_exprs) ->
    (*printf "%s:\t" (str_of_mword_flex lopc);*)
    P.printf "%x:\t" lopc;
    List.iter (fun (lvl,re) ->
      if lvl <= 0 then
	P.printf "%s\t" (Printer.str_of_rtl_exp_less_size re);
    ) cfa_exprs;
    P.printf "\n"
  ) old_cfa

let gen_may_PntToStk asm_fun = 
  let prog_vsettbl = H.create 32 in

  let bb_map = H.create 32 in
  List.iter (fun bb ->
    H.add bb_map bb.bb_label bb;
  ) asm_fun.func_bbs;
  
  let in_vsettbl_map = H.create 32 in
  let out_vsettbl_map = H.create 32 in
  List.iter (fun bb ->
    let empty_in_vsettbl = H.create 32 in
    H.add in_vsettbl_map bb.bb_label empty_in_vsettbl;
    let empty_out_vsettbl = H.create 32 in
    H.add out_vsettbl_map bb.bb_label empty_out_vsettbl
  ) asm_fun.func_bbs;

  let cfa_pseudo_reg =
    Coq_get_ps_reg_rtl_exp (Big.of_int 31, Big.of_int (-1))
  in

  let initial_esp_cfa_expr =
    rtl_sub 32 cfa_pseudo_reg (rtl_imm 32 4)
  in

  let initial_esp_vset = 
    [initial_esp_cfa_expr]
  in

  let entry_bb_label =
    (List.hd asm_fun.func_bbs).bb_label
  in
  assert (entry_bb_label = asm_fun.func_low);
  
  H.add (H.find in_vsettbl_map entry_bb_label) (AL_Reg ESP) initial_esp_vset;
  
  (*
  H.iter (fun relAddr in_vsettbl ->
    P.printf "%x --> \n" relAddr;
    dump_vset_table in_vsettbl
  ) in_vsettbl_map;
  *)
  
  let analyze_ri ri ai vsettbl =
    match ri with
    | Coq_set_loc_rtl(s,re,loc) ->
      let target_aloc = loc_to_aloc loc in
      let target_vset = cfa_repr_of_re re vsettbl ai asm_fun.func_name in
      if target_aloc = None then ()
      else if target_vset = [] then
	H.remove vsettbl (Option.get target_aloc)
      else 
	H.replace vsettbl (Option.get target_aloc) target_vset
    | Coq_set_ps_reg_rtl(s,re,psreg) -> 
      ()
    | Coq_set_array_rtl(s1,s2,arr,ire,vre) ->
      ()
    | Coq_set_byte_rtl(vre, are) ->
      let target_aloc = addr_to_aloc are ai vsettbl asm_fun.func_name in
      let target_vset = cfa_repr_of_re vre vsettbl ai asm_fun.func_name in
      if target_aloc = None then ()
      else if target_vset = [] then
	H.remove vsettbl (Option.get target_aloc)
      else
	H.replace vsettbl (Option.get target_aloc) target_vset
    | Coq_advance_oracle_rtl ->
      ()
    | Coq_if_rtl(guard,ri') ->
      ()
    | Coq_error_rtl ->
      ()
    (*raise (Failure "Unexpected RTL Error Instruction in CFAGen")*)
    | Coq_trap_rtl ->
      ()
  (*raise (Failure "Unexpected RTL Trap Instruction in CFAGen")*)
  in
  
  let perform_vsa taskbb in_vsettbl out_vsettbl =
    (** Use the instructions in taskbb and the in_vsettbl
	to compute a new out_vsettbl;
	if the new and the old are the same then return
	false;
	else update the out_vsettbl_map and return true
    **)

    let new_out_vsettbl = H.copy in_vsettbl in
    List.iter (fun (pre,instr,len) ->
      let rtl_instrs = X86_Compile.instr_to_rtl pre instr in
      List.iter (fun ri ->
	analyze_ri ri (pre,instr) new_out_vsettbl
      ) rtl_instrs
    ) (patch_for_may_analysis asm_fun taskbb.bb_instrs);
    
    (*
    P.printf "New out vsettbl for %s\n" taskbb.bb_label;
    dump_vset_table new_out_vsettbl;
    P.printf "Old out vsettbl for %s\n" taskbb.bb_label;
    dump_vset_table out_vsettbl;
    flush stdout;
    *)

    let continue_sign = not (eq_vsettbl out_vsettbl new_out_vsettbl) in
    if continue_sign then
      H.replace out_vsettbl_map taskbb.bb_label new_out_vsettbl;
    continue_sign
  in

  let analyze_bb taskbb task_in_vsettbl = 
    try
      let in_vsettbl = H.find in_vsettbl_map taskbb.bb_label in
      if H.length in_vsettbl = 0 then
	(* Should test if it is the first time of update*)
	(* Empty vsettbl means not visited, because at least 
	   ESP should be in*)
	H.iter (fun aloc vset ->
	  H.add in_vsettbl aloc vset
	) task_in_vsettbl
      else
	union_vsettbl_tofst in_vsettbl task_in_vsettbl;
      let out_vsettbl = 
	H.find out_vsettbl_map taskbb.bb_label 
      in
      (*
      dump_vset_table in_vsettbl;
      dump_vset_table out_vsettbl;
      *)
      let push_new_b =
	perform_vsa taskbb in_vsettbl out_vsettbl
      in
      push_new_b
    with Not_found ->
      assert false
  in

  let workq = Queue.create () in
  
  let add_new_tasks bb =
    if List.is_empty bb.bb_inter_succs then
      List.iter (fun succlbl ->
	if H.mem bb_map succlbl then
	  let newtask = mkVT
	    succlbl bb.bb_label
	  in
	  Queue.push newtask workq
      ) bb.bb_succs
    else 
      List.iter (fun succlbl ->
	if H.mem bb_map succlbl then
	  let newtask = mkVT
	    succlbl bb.bb_label
	  in
	  Queue.push newtask workq
	else (
	  P.printf "%s not found for %s!\n" succlbl bb.bb_label;
	  flush stdout;
	)
      ) bb.bb_inter_succs
  in

  let entry_bb = List.hd asm_fun.func_bbs in
  let push_task_b = analyze_bb entry_bb (H.find in_vsettbl_map entry_bb_label) in
  if push_task_b then add_new_tasks entry_bb;
  while not (Queue.is_empty workq) do
    let task = Queue.take workq in
    let tasklbl = task.vt_curr in
    let task_prev = task.vt_prev in
    try
      let task_in_vsettbl = H.find out_vsettbl_map task_prev in
      let task_bb = H.find bb_map tasklbl in
      let push_task_b = analyze_bb task_bb task_in_vsettbl in
      if push_task_b then add_new_tasks task_bb
    with Not_found ->
      assert false
  done;
(*
  H.iter (fun bblbl in_vsettbl ->
    P.printf "%s --> \n" bblbl;
    dump_vset_table in_vsettbl
  ) in_vsettbl_map;
*)

  H.iter (fun bblbl vsettbl ->
    let bb = H.find bb_map bblbl in
    let address = ref bb.bb_relAddr in
    let target_vsettbl = H.copy vsettbl in
    List.iter (fun (pre,ins,len) ->
      let ai = pre,ins in
      let exepnt = !address, false in
      H.add prog_vsettbl exepnt (H.copy target_vsettbl);
      let rtl_instrs = X86_Compile.instr_to_rtl pre ins in
      List.iter (fun ri ->
	match ri with
	| Coq_set_loc_rtl(s,re,loc) ->
	  let target_aloc = loc_to_aloc loc in
	  let target_vset = 
	    cfa_repr_of_re re target_vsettbl ai asm_fun.func_name 
	  in
	  if target_aloc = None then ()
	  else if target_vset = [] then
	    H.remove target_vsettbl (Option.get target_aloc)
	  else (
	    (*P.printf "set_loc target vset:\n%s\n" (str_of_value_set target_vset);*)
	    H.replace target_vsettbl (Option.get target_aloc)
	      target_vset
	  )
	| Coq_set_byte_rtl(vre, are) ->
	  let target_aloc = addr_to_aloc are ai vsettbl asm_fun.func_name in
	  let target_vset = 
	    cfa_repr_of_re vre target_vsettbl ai asm_fun.func_name in
	  if target_aloc = None then ()
	  else if target_vset = [] then
	    H.remove target_vsettbl (Option.get target_aloc)
	  else
	    H.replace target_vsettbl (Option.get target_aloc)
	      target_vset
	| _ -> ()
      ) rtl_instrs;
      address := !address + len
    ) (patch_for_may_analysis asm_fun bb.bb_instrs);
  ) in_vsettbl_map;
  prog_vsettbl
