open Batteries
module P = Printf
module H = Hashtbl

open X86Syntax
open X86Semantics
open X86_RTL
open Elf
open Instr
open Config
open Abbrev

open BB
open Ibaux
open RTL_opt
open Printer

(**=============== Utility Functions  ===============**)
let soundness_warning msg =
  if !Interface.soundness_warn then
    P.printf "Soundness Warning: %s" msg
   
let big32_to_int v = 
  Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int v))
 
let get_byte_size pre w =
  match pre.op_override, w with
  | true, true -> 2
  | true, false -> 1
  | false, true -> 4
  | false, false -> 1

let bss_range elf =
  let bss_shdr = List.find (fun shdr ->
    shdr.sh_name = ".bss"
  ) elf.shdr_tab
  in
  let bss_start = MWord.to_int bss_shdr.sh_addr in
  let bss_size = MWord.to_int bss_shdr.sh_size in
  bss_start, bss_start + bss_size

let rodata_range elf =
  let rodata_shdr = List.find (fun shdr ->
    shdr.sh_name = ".rodata"
  ) elf.shdr_tab
  in
  let rodata_start = MWord.to_int rodata_shdr.sh_addr in
  let rodata_size = MWord.to_int rodata_shdr.sh_size in
  rodata_start, rodata_start + rodata_size

let data_range elf =
  let data_shdr = List.find (fun shdr ->
    shdr.sh_name = ".data"
  ) elf.shdr_tab
  in
  let data_start = MWord.to_int data_shdr.sh_addr in
  let data_size = MWord.to_int data_shdr.sh_size in
  data_start, data_start + data_size

let rtl_imm size v =
  Coq_imm_rtl_exp (Big.of_int (size-1), Big.of_int v)

let rtl_sub size operand1 operand2 =
  Coq_arith_rtl_exp (Big.of_int (size-1), Coq_sub_op, operand1, operand2)

let rtl_add size operand1 operand2 =
  Coq_arith_rtl_exp (Big.of_int (size-1), Coq_add_op, operand1, operand2)

let rec prune_cast re =
  match re with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    begin match bvop with
    | Coq_shl_op 
    | Coq_shr_op 
    | Coq_shru_op
    | Coq_or_op -> prune_cast re1
    | _ ->
      Coq_arith_rtl_exp(s,bvop,prune_cast re1,prune_cast re2)
    end
  | Coq_test_rtl_exp(s,top,re1,re2) ->
    Coq_test_rtl_exp(s,top,prune_cast re1,prune_cast re2)
  | Coq_if_rtl_exp(s, ge,te,ee) ->
    Coq_if_rtl_exp(s, prune_cast ge, prune_cast te, prune_cast ee)
  | Coq_cast_s_rtl_exp(s,_,re') -> 
    prune_cast re' 
  | Coq_cast_u_rtl_exp(s,_,re') -> 
    prune_cast re'
  | Coq_imm_rtl_exp(s,_)
  | Coq_get_loc_rtl_exp(s,_)
  | Coq_get_ps_reg_rtl_exp(s,_) 
  | Coq_get_array_rtl_exp(_,s,_,_) ->
    re
  | Coq_get_byte_rtl_exp(re') ->
    Coq_get_byte_rtl_exp(prune_cast re')
  | Coq_get_random_rtl_exp(s) ->
    re
  | _ ->
    assert false

let rec reduce_rtl skip ris res =
  match ris with
  | [] -> res
  | h :: t ->
    begin match h with
    | Coq_set_byte_rtl _ ->
      if skip then
	reduce_rtl true t res
      else
	reduce_rtl true t (res @ [h])
    | Coq_set_loc_rtl(s,_,loc) ->
      begin match loc with
      | X86_MACHINE.Coq_flag_loc _ ->
	reduce_rtl false t res
      | _ ->
	reduce_rtl false t (res @ [h])
      end
    | Coq_if_rtl(_,ri) ->
      begin match ri with
      | Coq_trap_rtl | Coq_error_rtl -> 
	reduce_rtl skip t res
      | Coq_set_loc_rtl(s,_,loc) ->
	begin match loc with
	| X86_MACHINE.Coq_pc_loc ->
	  reduce_rtl false t res
	| _ ->
	  reduce_rtl false t (res @ [h])
	end
      | _ ->
	reduce_rtl false t (res @ [h])
      end
    | _ ->
      reduce_rtl false t (res @ [h])
    end

let is_arith re =
  match re with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) -> true
  | _ -> false

let is_mul re =
  match re with
  | Coq_arith_rtl_exp(s,bvop,_,_) ->
    begin match bvop with
    | Coq_mul_op -> true
    | _ -> false
    end
  | _ -> false

let is_get_reg_loc re =
  match re with
  | Coq_get_loc_rtl_exp(s,X86_MACHINE.Coq_reg_loc _) -> true
  | _ -> false

let is_imm re =
  match re with
  | Coq_imm_rtl_exp(s,v) -> true
  | _ -> false

let is_terminate re =
  is_imm re || is_mul re || is_get_reg_loc re
    
let reverse_sign_to_imm re =
  match re with
  | Coq_imm_rtl_exp(s,v) ->
    Coq_imm_rtl_exp(s, Big.opp v)
  | _ -> re

let rec key_component re =
  match re with
  | Coq_imm_rtl_exp(s,v) -> 
    [re]
  | Coq_get_loc_rtl_exp(_, X86_MACHINE.Coq_reg_loc _) ->
    [re]
  | Coq_arith_rtl_exp(_,bvop,re1,re2) ->
    let re2 =
      match bvop with
      | Coq_sub_op ->
	reverse_sign_to_imm re2
      | _ -> re2
    in
    begin match re1 with
    | Coq_arith_rtl_exp(_,bvop',re1',re2') ->
      let re2' =
	match bvop with
	| Coq_sub_op ->
	  reverse_sign_to_imm re2'
	| _ -> re2'
      in
      if is_mul re1 then 
	List.filter is_terminate [re1;re2]
      else if (is_terminate re1' && is_terminate re2') then
	List.filter is_terminate [re1';re2';re2]
      else
	key_component re1
    | _ -> 
      List.filter is_terminate [re1;re2]
    end
  | _ -> assert false

let addr_mode_from_addr_re addr_re ai =
  (** addr_re are operands of set_byte or get_byte;
      such expressions are all from address-mode
      operands at assembly level. Here, we reverse
      addr_re to its address mode for easier analysis
  **)
  let disp = ref (Big.of_int 0) in
  let sign = ref true in
  let base = ref None in
  let index = ref None in
  (** Three known cases: computed, register-dereference, immediate **)
  (*P.printf "Reverse RE:\n%s\n" (Printer.str_of_rtl_exp_less_size addr_re);*)

  let traverse_arith_re arith_re =
    let re1, re2 = 
      match arith_re with
      | Coq_arith_rtl_exp(s,bvop,re1,re2) -> re1,re2
      | _ -> assert false
    in
    let re1_is_seg_start =
      match re1 with
      | Coq_get_loc_rtl_exp(_, X86_MACHINE.Coq_seg_reg_start_loc _) ->
	true
      | _ -> false
    in
    if not re1_is_seg_start then (
      P.printf "Address RTL Expression not started with seg_reg!\n";
      assert false
    );
        
    let extract_essence terminate_re =
      match terminate_re with
      | Coq_imm_rtl_exp(_,v) ->
	disp := v
      | Coq_get_loc_rtl_exp(_,X86_MACHINE.Coq_reg_loc reg) ->
	base := Some reg
      | Coq_arith_rtl_exp(_,Coq_mul_op,re1,re2) ->
	begin match re1, re2 with
	| Coq_get_loc_rtl_exp(_,X86_MACHINE.Coq_reg_loc reg), Coq_imm_rtl_exp(_,v)
	| Coq_imm_rtl_exp(_,v), Coq_get_loc_rtl_exp(_,X86_MACHINE.Coq_reg_loc reg) ->
	  let scale = 
	    if Big.to_int v = 1 then Scale1
	    else if Big.to_int v = 2 then Scale2
	    else if Big.to_int v = 4 then Scale4
	    else if Big.to_int v = 8 then Scale8
	    else assert false
	  in
	  index := Some (scale,reg)
	| _ -> assert false
	end
      | _ -> assert false
    in
    List.iter extract_essence (key_component re2)
  in
  try (
    if is_get_reg_loc addr_re then
      begin match addr_re with
      | Coq_get_loc_rtl_exp(s, X86_MACHINE.Coq_reg_loc reg) ->
	Instr.mkAddress Big.zero (Some reg) None
      | _ -> assert false
      end
    else if is_imm addr_re then
      begin match addr_re with
      | Coq_imm_rtl_exp(s,v) ->
	Instr.mkAddress v None None
      | _ -> assert false
      end
    else if is_arith addr_re then (
      traverse_arith_re addr_re;
      Instr.mkAddress !disp !base !index
    ) else assert false
  ) with _ ->
    P.printf "%s\n" (Instr.str_of_instr ai);
    P.printf "%s\n" (Printer.str_of_rtl_exp_less_size addr_re);
    assert false

let mem_size_of_ai ai read =
  let movx_sizes prefix w = 
    match prefix.op_override, w with
    | true, true -> (2, 2)
    | true, false -> (1, 2)
    | false, true -> (2, 4)
    | false, false -> (1, 4)
  in

  let pre, ins = ai in
  match ins with
  | PUSH (w,_)
  | ADD (w,_,_)
  | ADC (w,_,_)
  | AND (w,_,_)
  | OR (w,_,_)
  | NEG (w,_)
  | NOT (w,_)
  | SUB (w,_,_)
  | SHL (w,_,_)
  | SHR (w,_,_)
  | ROR (w,_,_)
  | SBB (w,_,_)
  | SAR (w,_,_)
  | MUL (w,_)
  | DIV (w,_)
  | IMUL (w,_,_,_)
  | IDIV (w,_)
  | XADD (w,_,_)
  | XCHG (w,_,_)
  | XOR (w,_,_)
  | MOV (w,_,_) 
  | MOVS w
  | CMP (w,_,_)
  | CMPS w
  | CMPXCHG (w,_,_) -> 
    get_byte_size pre w

  | MOVSX (w,_,_)
  | MOVZX (w,_,_) ->
    let sz_from, sz_to = movx_sizes pre w in
    if read then sz_from else sz_to
  | POP _ 
  | JMP _
  | CALL _
  | LEAVE
  | CMOVcc _
  | RET _ -> 4
  | _ -> 
    P.printf 
      "Unsupported instruction in mem_size_of_ai\n%s\n"
      (Instr.str_of_instr ai);
    0

(**============= Abstract Location Declaration  =============**)
(* x64 native integer should be enough for x86 memory address *)
type abstract_region =
| AR_Reg of register
| AR_Psreg of string*int
| AR_Local of string
| AR_Global of segment_register option
| AR_Heap of int
| AR_Unknown

let str_of_aloc_region alocregion =
  match alocregion with
  | AR_Reg reg -> 
    "R:" ^ (str_of_reg reg)
  | AR_Psreg (func_name, regid) ->
    (** Hide function name for pretty printing **)
    P.sprintf "P:%d" regid
  | AR_Local func_name ->
    (** Hide function name for pretty printing **)
    "F:" (*^ func_name*)
  | AR_Global segreg_option ->
    if segreg_option = None then
      "G:NS" 
    else
      "G:"^ (str_of_segreg (Option.get segreg_option))
  | AR_Heap malloc_site ->
    "H:" ^ (P.sprintf "0x%x" malloc_site)
  | AR_Unknown ->
    "Unknown"

type abstract_loc = {
  aloc_region: abstract_region;
  aloc_offset: int;
  aloc_unit: int option;
  aloc_size: int;
  aloc_level: int;
}

let mkAloc rgn off unit_opt size lv = {
  aloc_region = rgn;
  aloc_offset = off;
  aloc_unit = unit_opt;
  aloc_size = size;
  aloc_level = lv;
}

let unknownAloc = {
  aloc_region = AR_Unknown;
  aloc_offset = -256;
  aloc_unit = None;
  aloc_size = 0;
  aloc_level = 0;
}

let str_of_signed_hex i =
  if i >= 0 then
    P.sprintf "%x" i
  else
    P.sprintf "-%x" (-i)

let str_of_aloc aloc =
  if aloc.aloc_region = AR_Unknown then
    P.sprintf "Unknown"
  else if aloc.aloc_unit = None then
    P.sprintf "%s[%s, %s]_%d" 
      (str_of_aloc_region aloc.aloc_region)
      (str_of_signed_hex aloc.aloc_offset)
      (str_of_signed_hex (aloc.aloc_offset + aloc.aloc_size))
      (aloc.aloc_level)
  else
    P.sprintf "%s[%s, %s](%x)_%d" 
      (str_of_aloc_region aloc.aloc_region)
      (str_of_signed_hex aloc.aloc_offset)
      (str_of_signed_hex (aloc.aloc_offset + aloc.aloc_size))
      (Option.get aloc.aloc_unit)
      (aloc.aloc_level)

type aloc_set_type = (abstract_loc, unit) H.t
  
type aloc_set = {
  reg_alocs: aloc_set_type;
  local_alocs: (string, aloc_set_type) H.t;
  global_alocs: aloc_set_type;
  heap_alocs: aloc_set_type;
}
  
let mkAlocSet r l g h = {
  reg_alocs = r;
  local_alocs = l;
  global_alocs = g;
  heap_alocs = h;
}
      
let dump_aloc_set alocset =
  let count = ref 0 in
  P.printf "Register alocs:\n";
  H.iter (fun reg_aloc _ ->
    count := !count + 1;
    if !count mod 4 = 0
    || H.length alocset.reg_alocs = !count
    then
      P.printf "%s\n" (str_of_aloc reg_aloc)
    else
      P.printf "%s\t" (str_of_aloc reg_aloc)
  ) alocset.reg_alocs;
  P.printf "\n";
  P.printf "Local alocs:\n";
  H.iter (fun fun_name local_alocs ->
    P.printf "\n%s:\n" fun_name;
    count := 0;
    H.iter (fun local_aloc _ ->
      count := !count + 1;      
      if !count mod 4 = 0 
      || H.length local_alocs = !count
      then
	P.printf "%s\n" (str_of_aloc local_aloc)
      else
	P.printf "%s\t" (str_of_aloc local_aloc)
    ) local_alocs;
  ) alocset.local_alocs;
  P.printf "\n";
  P.printf "Global alocs:\n";
  count := 0;
  H.iter (fun global_aloc _ ->
    count := !count + 1;
    if !count mod 4 = 0 
    || H.length alocset.global_alocs = !count
    then
      P.printf "%s\n" (str_of_aloc global_aloc)
    else
      P.printf "%s\t" (str_of_aloc global_aloc)
  ) alocset.global_alocs;
  P.printf "\n";
  P.printf "Heap alocs:\n";
  count := 0;
  H.iter (fun heap_aloc _ ->
    count := !count + 1;
    if !count mod 4 = 0 
    || H.length alocset.heap_alocs = !count
    then
      P.printf "%s\n" (str_of_aloc heap_aloc)
    else
      P.printf "%s\t" (str_of_aloc heap_aloc)
  ) alocset.heap_alocs;
  P.printf "\n"

(**=============== Value Set Declaration  ===============**)
type execution_point = int * bool
  
let str_of_exepnt exepnt =
  if snd exepnt then
    P.sprintf "%x+" (fst exepnt)
  else
    P.sprintf "%x" (fst exepnt)

(** May consider to design VSA's expression to represent values **)
type value_set_type = rtl_exp list

let str_of_value_set vset =
  String.concat "" (List.map (fun re ->
    P.sprintf "%s\n" 
      (Printer.str_of_rtl_exp_less_size re)
  ) vset)

let rec eq_rtl_exp rtl_exp1 rtl_exp2 =
  match rtl_exp1, rtl_exp2 with
  | Coq_arith_rtl_exp(s,bvop,re1,re2), Coq_arith_rtl_exp(s',bvop',re1',re2') ->
    Big.eq s s' && bvop = bvop' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2'
  | Coq_test_rtl_exp(s,top,re1,re2), Coq_test_rtl_exp(s',top',re1',re2') ->
    Big.eq s s' && top = top' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2'
  | Coq_if_rtl_exp(s,re1,re2,re3), Coq_if_rtl_exp(s',re1',re2',re3') ->
    Big.eq s s' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2' && eq_rtl_exp re3 re3'
  | Coq_cast_s_rtl_exp(s1,s2,re), Coq_cast_s_rtl_exp(s1',s2',re') ->
    Big.eq s1 s1' && Big.eq s2 s2' && eq_rtl_exp re re'
  | Coq_cast_u_rtl_exp(s1, s2, re), Coq_cast_u_rtl_exp(s1', s2', re') ->
    Big.eq s1 s1' && Big.eq s2 s2' && eq_rtl_exp re re'
  | Coq_imm_rtl_exp(s, v), Coq_imm_rtl_exp(s', v') ->
    Big.eq s s' && Big.eq v v'
  | Coq_get_loc_rtl_exp(s, loc), Coq_get_loc_rtl_exp(s', loc') ->
    Big.eq s s' && loc = loc'
  | Coq_get_ps_reg_rtl_exp(s, ps_reg), Coq_get_ps_reg_rtl_exp(s',ps_reg') ->
    Big.eq s s' && Big.eq ps_reg ps_reg'
(*
  | Coq_get_ps_reg_rtl_exp(s,ps_reg), _ 
  | _, Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    (Big.to_string ps_reg) = "-2"
*)
  | Coq_get_array_rtl_exp(l, s, arr, re), Coq_get_array_rtl_exp(l',s',arr',re') ->
    Big.eq l l' && Big.eq s s' && arr = arr' && eq_rtl_exp re re'
  | Coq_get_byte_rtl_exp(re), Coq_get_byte_rtl_exp(re') ->
    eq_rtl_exp re re'
  | Coq_get_random_rtl_exp(s),Coq_get_random_rtl_exp(s') ->
    Big.eq s s'
  | Coq_farith_rtl_exp(ew,mw,faop,rtl_rm,re1,re2),
    Coq_farith_rtl_exp(ew',mw',faop',rtl_rm',re1',re2') ->
    Big.eq ew ew' && Big.eq mw mw' && faop = faop' && eq_rtl_exp rtl_rm rtl_rm' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2'
  | Coq_fcast_rtl_exp(s1,s2,s3,s4,re1,re2),Coq_fcast_rtl_exp(s1',s2',s3',s4',re1',re2') ->
    Big.eq s1 s1' && Big.eq s2 s2' && Big.eq s3 s3' && Big.eq s4 s4' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2'
  | _,_ -> false

let sub_vset vset1 vset2 =
  List.for_all (fun re ->
    List.exists (eq_rtl_exp re) vset2
  ) vset1 

let eq_vset vset1 vset2 =
  sub_vset vset1 vset2 &&
    sub_vset vset2 vset1
    
let union_vset vset1 vset2 =
  (*P.printf "%s U %s = \n" (str_of_value_set vset1)
    (str_of_value_set vset2);*)
  let res = List.unique ?eq:(Some eq_rtl_exp) (vset1 @ vset2) in
  (*P.printf "%s\n" (str_of_value_set res);*)
  res

type value_set_table = (abstract_loc, value_set_type) H.t

let dump_vset_table vsettbl =
  H.iter (fun aloc vset ->
    P.printf "%s == \n%s\n" (str_of_aloc aloc) (str_of_value_set vset)
  ) vsettbl;
  flush stdout

let intersect_vsettbl_tofst dest_vsettbl src_vsettbl =
  H.iter (fun aloc vset ->
    if not (H.mem src_vsettbl aloc) then
      H.replace dest_vsettbl aloc []
    else if not (eq_vset (H.find src_vsettbl aloc) vset) then
      H.replace dest_vsettbl aloc []
    else ()
  ) dest_vsettbl

let union_vsettbl_tofst dest_vsettbl src_vsettbl =
  H.iter (fun aloc vset ->
    if not (H.mem dest_vsettbl aloc) then
      H.add dest_vsettbl aloc vset
    else 
      H.replace dest_vsettbl aloc (union_vset vset (H.find dest_vsettbl aloc))
  ) src_vsettbl

let sub_vsettbl vsettbl1 vsettbl2 =
  let sub_b = ref true in
  H.iter (fun aloc vset ->
    if H.mem vsettbl2 aloc then
      sub_b := eq_vset (H.find vsettbl2 aloc) vset
    else
      sub_b := false
  ) vsettbl1;
  !sub_b

let eq_vsettbl vsettbl1 vsettbl2 =
  sub_vsettbl vsettbl1 vsettbl2 &&
    sub_vsettbl vsettbl2 vsettbl1

let offset_in_cfarepr cfa_repr =
  match cfa_repr with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    begin match re2 with
    | Coq_imm_rtl_exp(s,v) ->
      let int_of_v =
	big32_to_int v
      in
      begin match bvop with
      | Coq_add_op ->
	 Some (-int_of_v)
      | Coq_sub_op ->
	 Some int_of_v
      | _ ->
         let warn_msg = 
	   P.sprintf "Soundness Warning! Ignoring cfa repr:\n%s\n" 
	     (Printer.str_of_rtl_exp_less_size cfa_repr)
         in
         soundness_warning warn_msg;
	 None
      end
    | _ -> 
       let warn_msg = 
	 P.sprintf "Soundness Warning! Ignoring cfa repr:\n%s\n" 
	   (Printer.str_of_rtl_exp_less_size cfa_repr)
       in
       soundness_warning warn_msg;
       None
    end
  | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    Some 0
  | _ ->
     let warn_msg = 
       P.sprintf "Soundness Warning! Ignoring cfa repr:\n%s\n" 
	 (Printer.str_of_rtl_exp_less_size cfa_repr)
     in
     soundness_warning warn_msg;
     None

type program_vset_tables = (execution_point, value_set_table) H.t

let dump_prog_vset_tables prog_vsettbls =
  P.printf "Program value set table:\n";
  H.iter (fun exepnt vsettbl ->
    P.printf "%s --> \n" (str_of_exepnt exepnt);
    dump_vset_table vsettbl
  ) prog_vsettbls

(** Some deprecated code **) 
(*
let rec compact_ri_list ri_list ai =
  let effective_set_locs = List.filter (fun ri ->
    match ri with
    | Coq_set_loc_rtl(_,_,X86_MACHINE.Coq_reg_loc _) ->
      true
    | _ -> false
  ) ri_list
  in
  let effective_set_bytes = List.filter (fun ri ->
    match ri with
    | Coq_set_byte_rtl _ ->
      true
    | _ -> false
  ) ri_list
  in
  let effective_if_set_locs = List.filter (fun ri ->
    match ri with
    | Coq_if_rtl (_,ri') ->
      begin match ri' with
      | Coq_set_loc_rtl(_,_,X86_MACHINE.Coq_reg_loc _) ->
	true
      | _ -> false
      end
    | _ -> false
  ) ri_list
  in
  let effective_if_set_bytes = List.filter (fun ri ->
    match ri with
    | Coq_if_rtl (_,ri') ->
      begin match ri' with
      | Coq_set_byte_rtl _ ->
	true
      | _ -> false
      end
    | _ -> false
  ) ri_list
  in
  let effective_ri_list =
    effective_set_locs @ effective_set_bytes 
    @ effective_if_set_locs @ effective_if_set_bytes
  in
  try (
    assert(List.length effective_set_locs <= 3);
    let meta_set_locs =
      List.map (fun ri ->
	(1,ri)
      ) effective_set_locs
    in
    assert(List.length effective_set_bytes <= 4);
    let meta_set_bytes =
      if List.length effective_set_bytes > 0 then
	[List.length effective_set_bytes, List.hd effective_set_bytes]
      else []
    in
    assert(List.length effective_if_set_locs <= 3);
    let meta_if_set_locs = 
      List.map (fun ri -> 1,ri) effective_if_set_locs
    in
    assert(List.length effective_if_set_locs <= 4);
    let meta_if_set_bytes =
      if List.length effective_if_set_bytes > 0 then
	[List.length effective_if_set_locs, List.hd effective_if_set_bytes]
      else []
    in
    meta_set_locs @ meta_set_bytes @ meta_if_set_locs @ meta_if_set_bytes
  )
  with _ ->
    P.printf "Error RTL instructions:\n\n";
    List.iter (fun ri ->
      P.printf "%s\n" (Printer.str_of_rtl_instr_less_size ri)
    ) effective_ri_list;
    P.printf "of assembly instruction:\n%s\n" (Instr.str_of_instr ai);
    assert false
*)      
