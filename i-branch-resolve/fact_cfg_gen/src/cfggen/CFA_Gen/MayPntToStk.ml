open Batteries
module P = Printf
module H = Hashtbl

open X86Syntax
open X86Semantics
open X86_RTL
open Config
open Abbrev

open BB
open Ibaux
open RTL_opt

open PntToStkLib

type task_type = {
  t_curr: string;
  t_prev: string;
}
  
let mkTask curr prev = {
  t_curr = curr;
  t_prev = prev;
}

let patched_call = ref 0

let patch_for_may_analysis asm_fun bb_instrs =
  List.map (fun (pre,instr,len) -> 
    match instr with
    | CALL(_,_,_,_) -> 
      patched_call := !patched_call + 1;
      (** TODO: need more advanced way to deal with call instructions **)
      (pre, MOV (true, (Reg_op EAX), (Imm_op (Big.of_int 0))),len)
    | AND(_,op1,op2) ->
      if asm_fun.func_name = "main" then
	begin match op1, op2 with
	| Reg_op reg, Imm_op v 
	| Imm_op v, Reg_op reg ->
	  if reg = ESP && (unsigned32_to_hex v) = "0xfffffff0" then
	    (pre,NOP (Imm_op (Big.of_int 0)),len)
	  else
	    pre,instr,len
	| _, _ -> pre,instr,len
	end
      else
	(pre,instr,len)
    | _ -> (pre,instr,len)
  ) bb_instrs

(** Translate operands to alocs according to the input-alocs **)
let loc_to_aloc alocs s loc =
  let size = (Big.to_int s) + 1 in
  match loc with
  | X86_MACHINE.Coq_reg_loc reg ->
    if size = 8 || size = 16 then (
      P.printf "Warning: partial access!\n";
      [mkAloc (AR_Reg reg) 0 None 4 0]
    )
    else if size = 32 then
      [mkAloc (AR_Reg reg) 0 None 4 0]
    else assert false
  | _ -> []

let psreg_to_aloc alocs psreg ai fun_name read =
  let mem_size = mem_size_of_ai ai read in
  let psregid = Big.to_int psreg in
  let psreg_aloc = mkAloc (AR_Psreg (fun_name,psregid))
    0 None mem_size 0
  in
  let generated_local_alocs =
    H.find alocs.local_alocs fun_name
  in
  let count = ref 0 in
  (*
  H.iter (fun local_aloc _ ->
      count := !count + 1;      
      if !count mod 4 = 0 
         || H.length generated_local_alocs = !count
      then
	P.printf "%s\n" (str_of_aloc local_aloc)
      else
	P.printf "%s\t" (str_of_aloc local_aloc)
    ) generated_local_alocs;
 *)
  if (H.mem generated_local_alocs psreg_aloc) then
    [psreg_aloc]
  else (
    P.printf "Wrong psreg_aloc: %s--%s\n" fun_name (str_of_aloc psreg_aloc);
    assert false
  )

let addr_to_aloc alocs addr_re ai vsettbl fname read = 
  let mem_size = mem_size_of_ai ai read in
  let addr_mode = addr_mode_from_addr_re addr_re ai in
  (*
  P.printf "RevAddrMode:\n%s\n" (Instr.str_of_addr addr_mode);
  P.printf "Reverse RE:\n%s\n" (Printer.str_of_rtl_exp_less_size addr_re);*)
  (*P.printf "RevAddrMode:\n%s\n" (Instr.str_of_addr addr_mode);*)
  let alocs_generated =
    match addr_mode with
    | {addrDisp = disp; addrBase = None; addrIndex = _} ->
      let int_of_disp = big32_to_int disp in
      let approximate = ref [] in
      H.iter (fun aloc _ ->
	let aloc_start = aloc.aloc_offset in
	let aloc_size = aloc.aloc_size in
	if int_of_disp >= aloc_start && int_of_disp < aloc_start + aloc_size then
	  approximate := aloc :: !approximate
      ) alocs.global_alocs;
      let available_alocs = !approximate in

      if List.is_empty available_alocs then (
	P.printf "Problematic instruction:\n%s\n"
	  (Instr.str_of_instr ai);
	P.printf "RevAddrMode:\n%s\n" (Instr.str_of_addr addr_mode);
	P.printf "Reverse RE:\n%s\n" (Printer.str_of_rtl_exp_less_size addr_re);
      );
      assert(List.length available_alocs >= 1);
      available_alocs
    | {addrDisp = disp; addrBase = Some reg; addrIndex = _} ->
      let int_of_disp = big32_to_int disp in
      let approximate = ref [] in
      H.iter (fun aloc _ ->
	let aloc_start = aloc.aloc_offset in
	let aloc_size = aloc.aloc_size in
	if int_of_disp >= aloc_start && int_of_disp < aloc_start + aloc_size then
	  approximate := aloc :: !approximate
      ) alocs.global_alocs;
      let available_global_alocs = !approximate in
      if available_global_alocs <> [] then (
	try
	  assert (List.length available_global_alocs = 1);
	  available_global_alocs
	with _ ->
	  List.iter (fun galoc ->
	      P.printf "%s\n" (str_of_aloc galoc)
	    ) available_global_alocs;
          let exact_galocs =
            List.filter (fun galoc ->
                let galoc_start = galoc.aloc_offset in
	        let galoc_size = galoc.aloc_size in
                galoc_start = int_of_disp && galoc_size = mem_size
              ) available_global_alocs
          in
          assert (List.length exact_galocs = 1);
          exact_galocs
      (*assert false*)
      ) else (
	let reg_aloc = mkAloc (AR_Reg reg) 0 None 4 0 in
	if H.mem vsettbl reg_aloc then
	  let cfa_reprs_of_base_reg =
	    H.find vsettbl reg_aloc
	  in
	  let offsets =
	    List.filter_map offset_in_cfarepr cfa_reprs_of_base_reg
	  in
	  let new_offsets =
	    List.map (fun off ->
	      off - int_of_disp - mem_size
	    ) offsets
	  in
	  let func_local_alocs =
	    H.find alocs.local_alocs fname
	  in
	  (*
	  P.printf "ADDR_RE:\n%s\n" (Printer.str_of_rtl_exp_less_size addr_re);
	  P.printf "Addr_Mode:\n%s\n" (Instr.str_of_addr addr_mode);
	  
	  List.iter (fun off ->
	    P.printf "%x\n" off
	  ) new_offsets;
	  *)
	  let available_local_alocs =
	    List.filter_map (fun offset ->
	      let approximate = ref None in
	      H.iter (fun aloc _ ->
		let aloc_start = aloc.aloc_offset in
		let aloc_size = aloc.aloc_size in
		if offset >= aloc_start && offset < aloc_start + aloc_size then
		  approximate := Some aloc
	      ) func_local_alocs;
	      !approximate
	    ) new_offsets
	  in
	  available_local_alocs
	else []
      )
  in
  (*
  if List.is_empty alocs_generated then (
    P.printf "Soundness Warning! No alocs for set_byte...\n";
    P.printf "Reversed Address Mode:\n%s\n" (Instr.str_of_addr addr_mode);
    P.printf "of assembly instruction:\n%s\n" (Instr.str_of_instr ai)
  );*)
  if List.is_empty alocs_generated then
    (**Warning: Treat empty alocs as the same storage location**)
    [unknownAloc]
  else alocs_generated

let rec apply_vsettbl vsettbl alocs re ai fname = 
  match re with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    let res1 = apply_vsettbl vsettbl alocs re1 ai fname in
    let res2 = apply_vsettbl vsettbl alocs re2 ai fname in
    List.flatten (List.map (fun new_re1 ->
      List.map (fun new_re2 ->
	Coq_arith_rtl_exp(s,bvop,new_re1,new_re2)	
      ) res2
    ) res1)
  | Coq_test_rtl_exp(s,top,re1,re2) ->
    let res1 = apply_vsettbl vsettbl alocs re1 ai fname in
    let res2 = apply_vsettbl vsettbl alocs re2 ai fname in
    List.flatten (List.map (fun new_re1 ->
      List.map (fun new_re2 ->
	Coq_test_rtl_exp(s,top,new_re1,new_re2)
      ) res2
    ) res1)
  | Coq_if_rtl_exp(s, ire, tre, ere) ->
    let new_ires = apply_vsettbl vsettbl alocs ire ai fname in
    let new_tres = apply_vsettbl vsettbl alocs tre ai fname in
    let new_eres = apply_vsettbl vsettbl alocs ere ai fname in
    List.flatten (List.map (fun new_ire ->
      List.flatten (List.map (fun new_tre ->
	List.map (fun new_ere ->
	  Coq_if_rtl_exp(s, new_ire, new_tre, new_ere)
	) new_eres
      ) new_tres)
    ) new_ires)	      
  | Coq_cast_s_rtl_exp(s1,s2,re) ->
    List.map (fun new_re ->
      Coq_cast_s_rtl_exp(s1,s2,new_re)
    ) (apply_vsettbl vsettbl alocs re ai fname)
  | Coq_cast_u_rtl_exp(s1,s2,re) ->
    List.map (fun new_re ->
      Coq_cast_u_rtl_exp(s1,s2,new_re)
    ) (apply_vsettbl vsettbl alocs re ai fname)
  | Coq_imm_rtl_exp(s,v) ->
    [re]
  | Coq_get_loc_rtl_exp(s,loc) ->
    begin match loc with
    | X86_MACHINE.Coq_reg_loc(reg) ->
      assert((Big.to_int s) = 31);
      begin try H.find vsettbl (mkAloc (AR_Reg reg) 0 None 4 0)
	with Not_found -> [re]
      end
    | _ -> [re] 
    end
  | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    [re]
  | Coq_get_array_rtl_exp(l,s,arr,index_re) ->
    List.map (fun new_re ->
      Coq_get_array_rtl_exp(l,s,arr,new_re)
    ) (apply_vsettbl vsettbl alocs index_re ai fname)
  | Coq_get_byte_rtl_exp(re') ->
    let target_alocs = addr_to_aloc alocs re' ai vsettbl fname true in
    if target_alocs = [] then [re]
    else  
      List.flatten (List.map (fun target_aloc -> 
	try H.find vsettbl target_aloc
	with Not_found -> [re]
      ) target_alocs)
  | Coq_get_random_rtl_exp(s) ->
    [re]
  | _ ->
    assert false

let rec cfa_must_in_re re = 
  match re with
  | Coq_arith_rtl_exp(s,_,re1,re2)
  | Coq_test_rtl_exp(s,_,re1,re2) ->
    cfa_must_in_re re1 || cfa_must_in_re re2
  | Coq_if_rtl_exp(s, ire, tre, ere) ->
    false
  | Coq_cast_s_rtl_exp(s1,s2,re)    
  | Coq_cast_u_rtl_exp(s1,s2,re) ->
    cfa_must_in_re re
  | Coq_imm_rtl_exp(s,v) ->
    false
  | Coq_get_loc_rtl_exp(s,loc) ->
    false
  | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    Big.eq ps_reg (Big.of_int (-1))
  | Coq_get_array_rtl_exp(l,s,arr,re) ->
    false
  | Coq_get_byte_rtl_exp(re) ->
    false
  | Coq_get_random_rtl_exp(s) ->
    false
  | _ ->
    assert false

let available_offset_in_alocs alocs re fname =
  if not (cfa_must_in_re re) then false
  else
    let offset_opt = 
      match re with
      | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
	begin match re1 with
	| Coq_get_ps_reg_rtl_exp(s,ps_reg) when Big.to_int ps_reg = (-1) ->
	  begin match re2 with
	  | Coq_imm_rtl_exp(s,v) ->
	    let int_of_v =
	      big32_to_int v
	    in
	    begin match bvop with
	    | Coq_add_op ->
	      Some (-int_of_v)
	    | Coq_sub_op ->
	      Some int_of_v
	    | _ -> 
	      P.printf "Soundness Warning! Ignoring cfa repr:\n%s\n" 
		(Printer.str_of_rtl_exp_less_size re);
	      None
	    end
	  | _ -> 
	    P.printf "Soundness Warning! Ignoring cfa repr:\n%s\n" 
	      (Printer.str_of_rtl_exp_less_size re);
	    None
	  end
	| _ -> 
	  P.printf "Soundness Warning! Ignoring cfa repr:\n%s\n" 
	    (Printer.str_of_rtl_exp_less_size re);
	  None
	end
      | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
	Some 0
      | _ ->
	P.printf "Soundness Warning! Ignoring cfa repr:\n%s\n" 
	  (Printer.str_of_rtl_exp_less_size re);
	None
    in
    if offset_opt = None then false
    else
      let offset = Option.get offset_opt in
      let off_lbound = ref 0 in
      let off_ubound = ref 0 in
      H.iter (fun aloc _ ->
	let aloc_start = aloc.aloc_offset in
	let aloc_size = aloc.aloc_size in
	let real_offset =
	  aloc_start + aloc_size
	in
	if real_offset < !off_lbound then
	  off_lbound := real_offset;
	if real_offset > !off_ubound then
	  off_ubound := real_offset
      ) (H.find alocs.local_alocs fname);
      offset <> 0 && offset >= !off_lbound && offset <= !off_ubound

let cfa_repr_of_re alocs re vsettbl ai fname =
  (*P.printf "Before transformation:\n%s\n" (Printer.str_of_rtl_exp_less_size re);*)
  let no_cast_re = prune_cast re in
  let transformed_res = apply_vsettbl vsettbl alocs no_cast_re ai fname in
  let evaluated_res =
    List.map (fun transformed_re ->
      (**TODO: implement new expression folding**)
      (*P.printf "Before evaluation:\n%s\n" (Printer.str_of_rtl_exp_less_size transformed_re);*) 
      let evaluated_re = simp_re [constant_fold] transformed_re in
      (*P.printf "After evaluation:\n%s\n" (Printer.str_of_rtl_exp_less_size evaluated_re);*)
      evaluated_re
    ) transformed_res
  in
  let cfa_in_res =
    List.filter (fun re ->
      available_offset_in_alocs alocs re fname
    ) evaluated_res
  in
  List.unique ?eq:(Some eq_rtl_exp) cfa_in_res
    
(** Based on the May-Analysis, we should be able to determine
    each operand in the function to be an aloc with approximation.
    The key is to figure out each register's value at a program
    execution point. The analysis must be sound. **)

let may_PntToStk alocs asm_fun =
  (** A Program-Value-Set (PVS) Table maps each program point to 
      a value-set table, which maps an aloc to a list of RTL
      expressions for representing values and alocs. **)
  let pvs_tbl = H.create 32 in
  
  (** bb_map for quick finding **)
  let bb_map = H.create 32 in
  List.iter (fun bb ->
    H.add bb_map bb.bb_label bb
  ) asm_fun.func_bbs;

  (** We compute fix-point for value-set (VS) table around each basic
      block. We use hashtbls to map each basic block to its VS-tables
      at the entry and exit. **)
  let in_vsettbl_map = H.create 32 in
  let out_vsettbl_map = H.create 32 in
  
  (** A May-Analysis should initialize the two tables to be TOP.
      Alocs that are not registered in the hashtbl are treated as
      having TOP value-set; on the other hand, we use empty list to 
      represent BOTTOM. **)
  List.iter (fun bb ->
    let empty_in_vsettbl = H.create 32 in
    H.add in_vsettbl_map bb.bb_label empty_in_vsettbl;
    let empty_out_vsettbl = H.create 32 in
    H.add out_vsettbl_map bb.bb_label empty_out_vsettbl
  ) asm_fun.func_bbs;
  
  (** Introduce initial knowledge about value sets **)
  let cfa_pseudo_reg =
    Coq_get_ps_reg_rtl_exp (Big.of_int 31, Big.of_int (-1))
  in
  
  let esp_aloc = mkAloc (AR_Reg ESP) 0 None 4 0 in
  assert (H.mem alocs.reg_alocs esp_aloc);
  
  let initial_esp_vset =
    [rtl_sub 32 cfa_pseudo_reg (rtl_imm 32 4)]
  in
  
  let entry_bb_label =
    (List.hd asm_fun.func_bbs).bb_label
  in
  assert (entry_bb_label = asm_fun.func_low);
  
  H.add (H.find in_vsettbl_map entry_bb_label) esp_aloc initial_esp_vset;
  
  (** For each RTL instruction ``ri'' of Assembly instruction 
      ``ai'' with respect to a value-set table ``vsettbl'', we
      analyze the semantics to update ``vsettbl''. **)
  
  let rec analyze_ri ri ai vsettbl =
    match ri with
    | Coq_set_loc_rtl(s,re,loc) ->
      let target_alocs = loc_to_aloc alocs s loc in
      if target_alocs = [] then ()
      else (
	let target_vset = 
	  cfa_repr_of_re alocs re vsettbl ai asm_fun.func_low 
	in
	assert((Big.to_int s) = 31);
	List.iter (fun target_aloc ->
	  H.replace vsettbl target_aloc target_vset
	) target_alocs
      )
    | Coq_set_ps_reg_rtl(s,re,psreg) -> 
      let target_alocs = psreg_to_aloc alocs
	psreg ai asm_fun.func_low false
      in
      if target_alocs = [] then ()
      else (
	let target_vset = 
	  cfa_repr_of_re alocs re vsettbl 
	    ai asm_fun.func_low
	in
	List.iter (fun target_aloc ->
	  H.replace vsettbl target_aloc target_vset
	) target_alocs
      )
    | Coq_set_array_rtl(s1,s2,arr,ire,vre) ->
      P.printf "Soundness Warning! Unsupported set_array...\n"
    | Coq_set_byte_rtl(vre, are) ->
      let target_alocs = addr_to_aloc alocs 
	are ai vsettbl asm_fun.func_low false 
      in
      if target_alocs = [] then ()
      else (
	let target_vset = 
	  cfa_repr_of_re alocs vre vsettbl ai asm_fun.func_low 
	in
	List.iter (fun target_aloc ->
	  H.replace vsettbl target_aloc target_vset
	) target_alocs
      )
    | Coq_advance_oracle_rtl -> ()
    | Coq_if_rtl(guard,ri') ->
      (** Approximate if_rtl **)
      analyze_ri ri' ai vsettbl
    | Coq_error_rtl ->
      P.printf "Soundness Warning! Unexpected RTL-Error during MayPntToStk\n"
    | Coq_trap_rtl ->
      P.printf "Soundness Warning! Unexpected RTL-Trap during MayPntToStk\n"
  in

  let analyze_one_step ri_list ai vsettbl =
    let reduced_ri_list =
      reduce_rtl false ri_list []
    in
    List.iter (fun ri ->
      analyze_ri ri ai vsettbl
    ) reduced_ri_list
  in
  
  (** The function performs VSA and returns a boolean to the 
      caller to decide if control-flow successors should be 
      propagated. In detail, it uses the instructions in taskbb 
      and the in_vstbl to compute a new out_vstbl;
      if the new and the old are the same then return false;
      else update the out_vsettbl_map and return true. **)

  let perform_vsa taskbb in_vsettbl out_vsettbl = 
    let new_out_vsettbl = H.copy in_vsettbl in
    List.iter (fun (pre,instr,len) ->
      let rtl_instrs = X86_Compile.instr_to_rtl pre instr in
      analyze_one_step rtl_instrs (pre,instr) new_out_vsettbl
    ) (patch_for_may_analysis asm_fun taskbb.bb_instrs);

    let continue_sign = not (eq_vsettbl out_vsettbl new_out_vsettbl) in
    if continue_sign then
      H.replace out_vsettbl_map taskbb.bb_label new_out_vsettbl;
    continue_sign
  in

  let analyze_bb taskbb task_in_vsettbl =
    try
      let in_vsettbl = H.find in_vsettbl_map taskbb.bb_label in
      if H.length in_vsettbl = 0 then
	(** No alocs are registered: all TOP **)
	H.iter (fun aloc vset ->
	  H.add in_vsettbl aloc vset
	) task_in_vsettbl
      else
        union_vsettbl_tofst in_vsettbl task_in_vsettbl;
      let out_vsettbl = 
	H.find out_vsettbl_map taskbb.bb_label 
      in
      let push_new_b =
	perform_vsa taskbb in_vsettbl out_vsettbl
      in
      push_new_b
    with Not_found ->
      assert false
  in

  let workq = Queue.create () in

  let add_new_tasks bb =
    if List.is_empty bb.bb_inter_succs then
      List.iter (fun succlbl ->
	if H.mem bb_map succlbl then
	  let newtask = mkTask
	    succlbl bb.bb_label
	  in
	  Queue.push newtask workq
      ) bb.bb_succs
    else 
      List.iter (fun succlbl ->
	if H.mem bb_map succlbl then
	  let newtask = mkTask
	    succlbl bb.bb_label
	  in
	  Queue.push newtask workq
	else (
	  P.printf "%s not found for %s!\n" succlbl bb.bb_label;
	  flush stdout;
	)
      ) bb.bb_inter_succs
  in
  
  (** Analyze the entry basic block and initialize the work-queue **)
  let entry_bb = List.hd asm_fun.func_bbs in
  let push_task_b = 
    analyze_bb entry_bb (H.find in_vsettbl_map entry_bb_label) 
  in
  if push_task_b then add_new_tasks entry_bb;
  while not (Queue.is_empty workq) do
    let task = Queue.take workq in
    let tasklbl = task.t_curr in
    let task_prev = task.t_prev in
    try
      let task_in_vsettbl = H.find out_vsettbl_map task_prev in
      let task_bb = H.find bb_map tasklbl in
      let push_task_b = analyze_bb task_bb task_in_vsettbl in
      if push_task_b then add_new_tasks task_bb
    with Not_found ->
      assert false    
  done;
  
  (** Translate in_vsettbl_map to PVS-Table **)
  H.iter (fun bblbl vsettbl ->
    (*P.printf "%s:\n" bblbl;
    dump_vset_table vsettbl;*)
    let bb = H.find bb_map bblbl in
    let address = ref bb.bb_relAddr in
    let target_vsettbl = H.copy vsettbl in
    List.iter (fun (pre,ins,len) ->
      let ai = pre,ins in
      let exepnt = !address, false in
      H.add pvs_tbl exepnt (H.copy target_vsettbl);
      let rtl_instrs = X86_Compile.instr_to_rtl pre ins in
      let reduced_rilist = reduce_rtl false rtl_instrs [] in
      List.iter (fun ri ->
	match ri with
	| Coq_set_loc_rtl(s,re,loc) ->
	  let target_alocs = loc_to_aloc alocs s loc in
	  if target_alocs = [] then ()
	  else (
	    let target_vset = 
	      cfa_repr_of_re alocs re target_vsettbl ai asm_fun.func_low 
	    in
	    assert((Big.to_int s) = 31);
	    List.iter (fun target_aloc -> 
	      H.replace target_vsettbl target_aloc
		target_vset
	    ) target_alocs
	  )
	| Coq_set_byte_rtl(vre, are) ->
	  let target_alocs = addr_to_aloc alocs 
	    are ai target_vsettbl asm_fun.func_low false 
	  in
	  if target_alocs = [] then (
	    P.printf "Soundness Warning! No alocs for set_byte...\n";
	    P.printf "0x%x: %s\n" !address (Instr.str_of_instr ai);
	    let addr_mode = addr_mode_from_addr_re are ai in
	    P.printf "RevAddrMode:\n%s\n" (Instr.str_of_addr addr_mode);
	    P.printf "Reverse RE:\n%s\n" (Printer.str_of_rtl_exp_less_size are);
	    dump_vset_table target_vsettbl;
	  ) else (
	    let target_vset = 
	      cfa_repr_of_re alocs vre target_vsettbl ai asm_fun.func_low 
	    in
	    List.iter (fun target_aloc ->
	      H.replace target_vsettbl target_aloc
		target_vset
	    ) target_alocs
	  )
	| Coq_set_ps_reg_rtl(s,re,psreg) -> 
	  let target_alocs = psreg_to_aloc alocs
	    psreg ai asm_fun.func_low false
	  in
	  if target_alocs = [] then ()
	  else (
	    let target_vset = 
	      cfa_repr_of_re alocs re vsettbl 
		ai asm_fun.func_low 
	    in
	    List.iter (fun target_aloc ->
	      H.replace target_vsettbl target_aloc target_vset
	    ) target_alocs
	  )
	| _ -> ()
      ) reduced_rilist;
      address := !address + len
    ) (patch_for_may_analysis asm_fun bb.bb_instrs);
  ) in_vsettbl_map;
  pvs_tbl
    
let reverse_vsettbl alocs fname vsettbl =
  H.fold (fun aloc re_list cfa_exprs ->
    match aloc.aloc_region with
    | AR_Reg reg ->
      let get_reg = Coq_get_loc_rtl_exp(Big.of_int 31, X86_MACHINE.Coq_reg_loc reg) in
      (*
      let exprs = 
	List.map (fun re ->
	  Coq_test_rtl_exp(Big.of_int 0,Coq_eq_op,get_reg,re)
	) re_list
      in
      *)
      let new_cfaexprs =
	List.filter_map (fun re ->
	  let offset_opt = offset_in_cfarepr re in
	  if offset_opt = None then None
	  else if true then (
	    let offset = Option.get offset_opt in
	    if offset >= 0 then
	      Some (rtl_add 32 get_reg (rtl_imm 32 offset))
	    else
	      Some (rtl_sub 32 get_reg (rtl_imm 32 (-offset)))
	  ) else (
	    let offset = Option.get offset_opt in
	    let approximate = ref None in
	    H.iter (fun aloc _ ->
	      let aloc_start = aloc.aloc_offset in
	      let aloc_size = aloc.aloc_size in
	      if offset > aloc_start && offset <= aloc_start + aloc_size then
		approximate := Some (aloc_start+aloc_size)
	    ) (H.find alocs.local_alocs fname);
	    if !approximate = None then None
	    else Some (
	      let appr_offset = Option.get !approximate in
	      if appr_offset >= 0 then
	        rtl_add 32 get_reg (rtl_imm 32 appr_offset)
	      else
	        rtl_sub 32 get_reg (rtl_imm 32 (-appr_offset))
	    )
	  )
	) re_list
      in
      (List.unique ?eq:(Some eq_rtl_exp) new_cfaexprs) :: cfa_exprs
  | _ -> cfa_exprs
  ) vsettbl []

let prog_vsettbl_to_CFA alocs fname pvs_tbl =
  let func_cfa_repr = 
    H.fold (fun exepnt vsettbl l ->
      let reversed_vsettbl = 
	reverse_vsettbl alocs fname vsettbl in
      if snd exepnt then l
      else
	let lopc = fst exepnt in
	(lopc, reversed_vsettbl) :: l
    ) pvs_tbl []
  in
  let sorted_func_cfa_repr = 
    List.sort (fun (lopc1, cfa_exprs1) (lopc2, cfa_exprs2) ->
      if lopc1 < lopc2 then -1
      else if lopc1 > lopc2 then 1
      else 0
    ) func_cfa_repr
  in
  let cfa_result =
    sorted_func_cfa_repr
  in
  cfa_result

let dump_CFA cfa =
  List.iter (fun (lopc,cfa_exprs) ->
    P.printf "%x: " lopc;
    let printed_one = ref false in
    List.iteri (fun ndx re_list ->
      if List.is_empty re_list then ()
      else (
	if !printed_one then P.printf " & ";
	P.printf "{";
	List.iteri (fun i re ->
	  if i <> 0 then P.printf " | ";
	  P.printf "%s" (Printer.str_of_rtl_exp_less_size re);
	) re_list;
	P.printf "}";
	printed_one := true
      )
    ) cfa_exprs;
    P.printf "\n"
  ) cfa

let lookup_cfaexprs cfaexprs reg =
  List.flatten (List.map (fun rtl_exprs ->
    List.filter_map (fun rtl_exp ->
      match rtl_exp with
      | Coq_get_loc_rtl_exp(s,loc) ->
	begin match loc with
	| X86_MACHINE.Coq_reg_loc reg_loc ->
	  if reg_loc = reg then Some 0
	  else None
	| _ -> None
	end
      | Coq_arith_rtl_exp (s,bvop,re1,re2) ->
	let reg_loc =
	  begin match re1 with
	  | Coq_get_loc_rtl_exp(s,loc) ->
	    begin match loc with
	    | X86_MACHINE.Coq_reg_loc reg_loc ->
	      reg_loc
	    | _ -> assert false
	    end
	  | _ -> assert false
	  end
	in
	if reg_loc = reg then
	  begin match bvop with
	  | Coq_add_op ->
	    begin match re2 with
	    | Coq_imm_rtl_exp(s,v) ->
	      Some (-(Big.to_int v))
	    | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
	      None
	    | _ -> assert false
	    end
	  | Coq_sub_op ->
	    begin match re2 with
	    | Coq_imm_rtl_exp(s,v) ->
	      Some (Big.to_int v)
	    | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
	      None
	    | _ -> assert false
	    end
	  | _ -> assert false
	  end
	else None
      | _ -> assert false
    ) rtl_exprs
  ) cfaexprs)
		  
(*
let reverse_vsettbl alocs fname vsettbl =
  H.fold (fun aloc re_list cfa_exprs ->
    match aloc.aloc_region with
    | AR_Reg reg ->
      let get_reg = Coq_get_loc_rtl_exp(Big.of_int 31, X86_MACHINE.Coq_reg_loc reg) in
      let exprs = 
	List.map (fun re ->
          Coq_test_rtl_exp(Big.of_int 0,Coq_eq_op,get_reg,re)
	) re_list
      in
      let new_cfaexprs =
	List.filter_map (fun re ->
	  let offset_opt = 
	    match re with
	    | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
	      begin match re1 with
	      | Coq_get_ps_reg_rtl_exp(s,ps_reg) when Big.to_int ps_reg = (-1) ->
		begin match re2 with
		| Coq_imm_rtl_exp(s,v) ->
		  let int_of_v =
		    big32_to_int v
		  in
		  begin match bvop with
		  | Coq_add_op ->
		    Some (-int_of_v)
		  | Coq_sub_op ->
		    Some int_of_v
		  | _ -> 
		    P.printf "Soundness Warning! Ignoring cfa repr:\n%s\n" (Printer.str_of_rtl_exp_less_size re);
		    None
		  end
		| _ -> 
		  P.printf "Soundness Warning! Ignoring cfa repr:\n%s\n" (Printer.str_of_rtl_exp_less_size re);
		  None
		end
	      | _ -> assert false
	      end
	    | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
	      Some 0
	    | _ ->
	      P.printf "Soundness Warning! Ignoring cfa repr:\n%s\n" (Printer.str_of_rtl_exp_less_size re);
	      None
	  in
	  if offset_opt = None then None
	  else if reg = ESP then (
	    let offset = Option.get offset_opt in
	    if offset >= 0 then
	      Some (rtl_add 32 get_reg (rtl_imm 32 offset))
	    else
	      Some (rtl_sub 32 get_reg (rtl_imm 32 (-offset)))
	  ) else (
	    let offset = Option.get offset_opt in
	    let approximate = ref None in
	    H.iter (fun aloc _ ->
	      let aloc_start = aloc.aloc_offset in
	      let aloc_size = aloc.aloc_size in
	      if offset >= aloc_start && offset < aloc_start + aloc_size then
		approximate := Some aloc_start
	    ) (H.find alocs.local_alocs fname);
	    if !approximate = None then None
	    else Some (
	      let appr_offset = Option.get !approximate in
	      if appr_offset >= 0 then
	        rtl_add 32 get_reg (rtl_imm 32 appr_offset)
	      else
	        rtl_sub 32 get_reg (rtl_imm 32 (-appr_offset))
	    )
	  )
	) re_list
      in
      (List.unique ?eq:(Some eq_rtl_exp) new_cfaexprs) :: cfa_exprs
  | _ -> cfa_exprs
  ) vsettbl []

let prog_vsettbl_to_CFAInfo alocs fname pvs_tbl =
  let func_cfa_repr = 
    H.fold (fun exepnt vsettbl l ->
      let reversed_vsettbl = 
	reverse_vsettbl alocs fname vsettbl in
      if snd exepnt then l
      else
	let lopc = fst exepnt in
	(lopc, reversed_vsettbl) :: l
    ) pvs_tbl []
  in
  let sorted_func_cfa_repr = 
    List.sort (fun (lopc1, cfa_exprs1) (lopc2, cfa_exprs2) ->
      if lopc1 < lopc2 then -1
      else if lopc1 > lopc2 then 1
      else 0
    ) func_cfa_repr
  in
  let mayCFA_result =
    sorted_func_cfa_repr
  in
  mayCFA_result
    
let dump_mayCFA mayCFA =
  List.iter (fun (lopc,cfa_exprs) ->
    P.printf "%x: " lopc;
    let printed_one = ref false in
    List.iteri (fun ndx re_list ->
      if List.is_empty re_list then ()
      else (
	if !printed_one then P.printf " & ";
	P.printf "{";
	List.iteri (fun i re ->
	  if i <> 0 then P.printf " | ";
	  P.printf "%s" (Printer.str_of_rtl_exp_less_size re);
	) re_list;
	P.printf "}";
	printed_one := true
      )
    ) cfa_exprs;
    P.printf "\n"
  ) mayCFA
*)

(** Some deprecated code **) 
(*
let size_of_re re =
  match re with
  | Coq_arith_rtl_exp(s,_,_,_)
  | Coq_test_rtl_exp(s,_,_,_) ->
    ((Big.to_int s) + 1) / 8
  | Coq_if_rtl_exp(s, _,_,_) ->
    raise (Failure "Conditional expression met!")
  | Coq_cast_s_rtl_exp(s,_,_)    
  | Coq_cast_u_rtl_exp(s,_,_)
  | Coq_imm_rtl_exp(s,_)
  | Coq_get_loc_rtl_exp(s,_)
  | Coq_get_ps_reg_rtl_exp(s,_) 
  | Coq_get_array_rtl_exp(_,s,_,_) ->
    ((Big.to_int s) + 1) / 8
  | Coq_get_byte_rtl_exp(re) ->
    1
  | Coq_get_random_rtl_exp(s) ->
    ((Big.to_int s) + 1) / 8
  | _ ->
    assert false

let rec regs_in_re re =
  match re with
  | Coq_arith_rtl_exp(s,_,re1,re2)
  | Coq_test_rtl_exp(s,_,re1,re2) ->
    regs_in_re re1 @ regs_in_re re2
  | Coq_if_rtl_exp(s, ire, tre, ere) ->
    raise (Failure "Conditional expression met!")
  | Coq_cast_s_rtl_exp(s1,s2,re')    
  | Coq_cast_u_rtl_exp(s1,s2,re') ->
    regs_in_re re'
  | Coq_imm_rtl_exp(s,v) ->
    []
  | Coq_get_loc_rtl_exp(s,loc) ->
    begin match loc with
    | X86_MACHINE.Coq_reg_loc(reg) -> [reg]
    | _ -> []
    end
  | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    []
  | Coq_get_array_rtl_exp(l,s,arr,re) ->
    []
  | Coq_get_byte_rtl_exp(re) ->
    regs_in_re re
  | Coq_get_random_rtl_exp(s) ->
    []
  | _ ->
    assert false

let rec imms_in_re re =
  match re with
  | Coq_arith_rtl_exp(s,_,re1,re2)
  | Coq_test_rtl_exp(s,_,re1,re2) ->
    imms_in_re re1 @ imms_in_re re2
  | Coq_if_rtl_exp(s, ire, tre, ere) ->
    raise (Failure "Conditional expression met!")
  | Coq_cast_s_rtl_exp(s1,s2,re')    
  | Coq_cast_u_rtl_exp(s1,s2,re') ->
    imms_in_re re'
  | Coq_imm_rtl_exp(s,v) ->
    [v]
  | Coq_get_loc_rtl_exp(s,loc) ->
    []
  | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    []
  | Coq_get_array_rtl_exp(l,s,arr,re) ->
    []
  | Coq_get_byte_rtl_exp(re) ->
    imms_in_re re
  | Coq_get_random_rtl_exp(s) ->
    []
  | _ ->
    assert false

let rec ndxs_in_re re =
  match re with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    begin match bvop with
    | Coq_mul_op ->
      begin match re1, re2 with
      | Coq_get_loc_rtl_exp(_,X86_MACHINE.Coq_reg_loc reg), Coq_imm_rtl_exp(_,v)
      | Coq_imm_rtl_exp(_,v), Coq_get_loc_rtl_exp(_,X86_MACHINE.Coq_reg_loc reg) ->
	let scale = 
	  if Big.to_int v = 1 then Scale1
	  else if Big.to_int v = 2 then Scale2
	  else if Big.to_int v = 4 then Scale4
	  else if Big.to_int v = 8 then Scale8
	  else assert false
	in
	[scale,reg]
      | _,_ -> []
      end
    | _ ->
      ndxs_in_re re1 @ ndxs_in_re re2
    end
  | Coq_test_rtl_exp(s,_,re1,re2) ->
    ndxs_in_re re1 @ ndxs_in_re re2
  | Coq_if_rtl_exp(s, ire, tre, ere) ->
    raise (Failure "Conditional expression met!")
  | Coq_cast_s_rtl_exp(s1,s2,re)    
  | Coq_cast_u_rtl_exp(s1,s2,re) ->
    ndxs_in_re re
  | Coq_imm_rtl_exp(s,v) ->
    []
  | Coq_get_loc_rtl_exp(s,loc) ->
    []
  | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    []
  | Coq_get_array_rtl_exp(l,s,arr,re) ->
    []
  | Coq_get_byte_rtl_exp(re) ->
    ndxs_in_re re
  | Coq_get_random_rtl_exp(s) ->
    []
  | _ ->
    assert false

let ad_hoc_simpre re =
  match re with
  | X86_RTL.Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    if bvop = X86_RTL.Coq_or_op then
      begin match re1 with
      | X86_RTL.Coq_cast_u_rtl_exp(s1,s2,re') ->
	if Big.eq s1 (Big.of_int 7) &&
	  Big.eq s2 (Big.of_int 31) then
	  begin match re' with
	  | X86_RTL.Coq_cast_u_rtl_exp(s1',s2',re'') ->
	    if Big.eq s1' (Big.of_int 31) &&
	      Big.eq s2' (Big.of_int 7) then
	      re''
	    else re
	  | _ -> re
	  end
	else re
      | _ -> re
      end
    else re
  | _ -> re

let offset_available_in_alocs alocs re fname =
  if not (cfa_must_in_re re) then false
  else
    let offset_opt = 
      match re with
      | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
	begin match re2 with
	| Coq_imm_rtl_exp(s,v) ->
	  let int_of_v =
	    big32_to_int v
	  in
	  begin match bvop with
	  | Coq_add_op ->
	    Some (-int_of_v)
	  | Coq_sub_op ->
	    Some int_of_v
	  | _ -> 
	    P.printf "Ignoring cfa repr:\n%s\n" 
	      (Printer.str_of_rtl_exp_less_size re);
	    None
	  end
	| _ -> 
	  P.printf "Ignoring cfa repr:\n%s\n" 
	    (Printer.str_of_rtl_exp_less_size re);
	  None
	end
      | Coq_get_loc_rtl_exp(s,ps_reg) ->
	Some 0
      | _ ->
	P.printf "Ignoring cfa repr:\n%s\n" 
	  (Printer.str_of_rtl_exp_less_size re);
	None
    in
    if offset_opt = None then false
    else
      let offset = Option.get offset_opt in
      let frame_lower_bound = ref 0 in
      H.iter (fun laloc _ ->
	let real_offset =
	  laloc.aloc_offset + laloc.aloc_size
	in
	if real_offset < !frame_lower_bound then
	  frame_lower_bound := real_offset
      )	(H.find alocs.local_alocs fname);
      offset >= !frame_lower_bound

(*
  if (Big.to_int s) = 31 then (
    let no_cast_re = prune_cast re in
    let transformed_res = apply_vsettbl vsettbl alocs no_cast_re ai fname in
    let evaluated_res =
      List.map (fun transformed_re ->
      (**TODO: implement new expression folding**)
      (*P.printf "Before evaluation:\n%s\n" (Printer.str_of_rtl_exp_less_size transformed_re);*) 
	let evaluated_re = simp_re [constant_fold; ad_hoc_simpre] transformed_re in
      (*P.printf "After evaluation:\n%s\n" (Printer.str_of_rtl_exp_less_size evaluated_re);*)
	evaluated_re
    (*      (simp_re simpre_all_methods transformed_re)*)
      ) transformed_res
    in
    let cfa_in_res =
      List.filter_map (fun re ->
        available_offset_in_alocs alocs re fname
      ) evaluated_res
    in
    List.unique ?eq:(Some eq_rtl_exp) cfa_in_res
  ) else if (Big.to_int s) = 7 then (
    let no_cast_re = prune_cast re in
    let transformed_res = apply_vsettbl vsettbl alocs no_cast_re ai fname in
    let evaluated_res =
      List.map (fun transformed_re ->
	(**TODO: implement new expression folding**)
	(*P.printf "Before evaluation:\n%s\n" (Printer.str_of_rtl_exp_less_size transformed_re);*) 
	let evaluated_re = simp_re [constant_fold; ad_hoc_simpre] transformed_re in
	(*P.printf "After evaluation:\n%s\n" (Printer.str_of_rtl_exp_less_size evaluated_re);*)
	evaluated_re
      (*      (simp_re simpre_all_methods transformed_re)*)
      ) transformed_res
    in
    let cfa_in_res =
      List.filter (fun re ->
        offset_available_in_alocs alocs re fname
      ) evaluated_res
    in
    List.unique ?eq:(Some eq_rtl_exp) cfa_in_res
  ) else []
*)

*)


