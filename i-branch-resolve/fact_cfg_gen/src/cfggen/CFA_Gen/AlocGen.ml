open Batteries
module P = Printf
module H = Hashtbl

open X86Syntax
open X86Semantics
open X86_RTL
open Elf
open Instr
open Config
open Abbrev

open BB
open Ibaux
open Printer
open RTL_opt

open PntToStkLib

let all_reg_alocs r_set = 
  let aloc_of_reg opsize reg =
    match opsize,reg with
    | OpSize8, EAX
    | OpSize8, ECX
    | OpSize8, EDX
    | OpSize8, EBX ->
      mkAloc (AR_Reg reg) 0 None 1 0
    | OpSize8, ESP ->
      mkAloc (AR_Reg EAX) 1 None 1 0
    | OpSize8, EBP ->
      mkAloc (AR_Reg ECX) 1 None 1 0
    | OpSize8, ESI ->
      mkAloc (AR_Reg EDX) 1 None 1 0
    | OpSize8, EDI ->
      mkAloc (AR_Reg EBX) 1 None 1 0
    | _,_ ->
      let size = match opsize with
	| OpSize16 -> 2
	| OpSize32 -> 4
	| OpSize8 -> assert false
      in
      mkAloc (AR_Reg reg) 0 None size 0
  in
  let reg_alocs =
    List.flatten (List.map (fun opsize ->
      List.map (aloc_of_reg opsize) 
	[EAX;ECX;EDX;EBX;ESP;EBP;ESI;EDI]
    ) [OpSize8; OpSize16; OpSize32])
  in
  List.iter (fun raloc ->
    H.replace r_set raloc ()
  ) reg_alocs

let partition_region rgn rgn_start rgn_end boundaries =
  (** Boundaries should be sorted **)
  (** Accessed alocs are generated in another function;
      this function generates partitioned alocs. **)
  let boundaries_in_rgn =
    List.filter (fun (off,size) ->
      off >= rgn_start && off < rgn_end
    ) boundaries
  in
  let aloc_start = ref rgn_start in
  let aloc_end = ref rgn_start in
  let partitioned_alocs = ref [] in
  
  List.iter (fun (off,size) ->
    if off = !aloc_start then (
      if (off+size) <= !aloc_end then (
	(*
	P.printf "Ignoring sub-aloc or the same aloc:\n";
	P.printf "[%x,%x) VS [%x,%x)\n"
	  off (off+size) !aloc_start !aloc_end*) 
      ) else (
	aloc_start := off;
	aloc_end := off+size;
      ) 
    ) else if off > !aloc_start && off < !aloc_end then (
      if (off+size) <= !aloc_end then (
	(*
	P.printf "Ignoring sub-aloc...\n";
	P.printf "[%x,%x) VS [%x,%x)\n" 
	  off (off+size) !aloc_start !aloc_end*)
      ) else (
	aloc_start := off;
	aloc_end := off+size
      )
    ) else if off = !aloc_end then (
      aloc_start := off;
      aloc_end := off+size
    ) else (
      let newaloc = mkAloc rgn
	!aloc_end None (off - !aloc_end) 0
      in
      partitioned_alocs := newaloc :: !partitioned_alocs;
      aloc_start := off;
      aloc_end := off+size
    )
  ) boundaries_in_rgn;
  
  if !aloc_end < rgn_end then (
    let tailaloc = mkAloc rgn !aloc_end None 
      (rgn_end - !aloc_end) 0
    in
    partitioned_alocs := tailaloc :: !partitioned_alocs
  );
  !partitioned_alocs

let rec boundaries_in_re re ai b_in_are =
  let no_cast_re = prune_cast re in
  match no_cast_re with
  | Coq_arith_rtl_exp(_,_,re1,re2)
  | Coq_test_rtl_exp(_,_,re1,re2) ->
    boundaries_in_re re1 ai b_in_are
    @ boundaries_in_re re2 ai b_in_are
  | Coq_if_rtl_exp(_,ge,te,ee) ->
    boundaries_in_re ge ai b_in_are
    @ boundaries_in_re te ai b_in_are
    @ boundaries_in_re ee ai b_in_are
  | Coq_cast_s_rtl_exp(_,_,re') 
  | Coq_cast_u_rtl_exp(_,_,re') ->
    boundaries_in_re re' ai b_in_are
  | Coq_get_byte_rtl_exp(are) ->
    b_in_are are ai true
  | _ -> [] 
    
let rec boundaries_in_ri ri ai b_in_are =
  match ri with
  | Coq_set_loc_rtl(s,re,_)
  | Coq_set_ps_reg_rtl(s,re,_) ->
    boundaries_in_re re ai b_in_are
  | Coq_set_array_rtl _ ->
    assert false
  | Coq_set_byte_rtl(vre,are) ->
    boundaries_in_re vre ai b_in_are
    @ b_in_are are ai false
  | Coq_if_rtl(guard,ri') ->
    boundaries_in_re guard ai b_in_are
    @ boundaries_in_ri ri' ai b_in_are
  | Coq_error_rtl 
  | Coq_trap_rtl
  | Coq_advance_oracle_rtl -> []

(*************************************************)
(** Deal with global alocs appearred in asm_fun **)
(*************************************************)
let global_boundaries_in_rilist elf rtl_instrs =
  let bss_start = fst (bss_range elf) in
  let bss_end = snd (bss_range elf) in
  let data_start = fst (data_range elf) in
  let data_end = snd (data_range elf) in
  let rodata_start = fst (rodata_range elf) in
  let rodata_end = snd (rodata_range elf) in
  let int_in_range i l h =
    i >= l && i < h
  in
  let global_boundaries_in_addr_re addr_re (relAddr,ai) read =
    let pre, ins = ai in
    let addr_mode = addr_mode_from_addr_re addr_re ai in
    let mem_size = mem_size_of_ai ai read in
    match addr_mode with
    | {addrDisp=disp; addrBase=_; addrIndex=_} ->
      if pre.seg_override = None then
	if int_in_range (Big.to_int disp) bss_start bss_end ||
	  int_in_range (Big.to_int disp) data_start data_end ||
	  int_in_range (Big.to_int disp) rodata_start rodata_end 
	then
	  [None, (Big.to_int disp), mem_size]	
	else []
      else
	[pre.seg_override, (Big.to_int disp),mem_size]
  in
  let global_boundaries =
    List.flatten (List.map (fun (relAddr, ai, rilist) ->
      let reduced_rilist =
	reduce_rtl false rilist []
      in
      List.flatten (List.map (fun ri ->
	boundaries_in_ri ri (relAddr,ai) 
	  global_boundaries_in_addr_re
      ) reduced_rilist)
    ) rtl_instrs)
  in
  let ds_boundaries =
    List.unique (List.filter_map (fun (segreg_op, off, size) ->
      if segreg_op = None then
	Some (off,size)
      else if Option.get segreg_op = DS then
	Some (off,size)
      else None
    ) global_boundaries)
  in
  let nds_boundaries =
    List.unique (List.filter_map (fun (segreg_op, off, size) ->
      if segreg_op = None then None
      else if Option.get segreg_op = DS then None
      else
	Some (Option.get segreg_op, off, size)
    ) global_boundaries)
  in
  ds_boundaries, nds_boundaries

let partition_global_regions g_set elf ds_boundaries nds_boundaries =
  let bss_start = fst (bss_range elf) in
  let bss_end = snd (bss_range elf) in
  let data_start = fst (data_range elf) in
  let data_end = snd (data_range elf) in
  let rodata_start = fst (rodata_range elf) in
  let rodata_end = snd (rodata_range elf) in
  
  let sorted_ds_boundaries =
    List.sort_unique (fun (addr1,off1) (addr2,off2) ->
      if addr1 = addr2 then
	off1 - off2
      else addr1 - addr2
    ) ds_boundaries
  in
  let accessed_ds_alocs =
    List.filter_map (fun (off,size) ->
      if size = 0 then None
      else Some 
	(mkAloc (AR_Global (Some DS)) off None size 0)
    ) sorted_ds_boundaries
  in
  let bss_partitioned_ds_alocs =
    partition_region (AR_Global (Some DS)) 
      bss_start bss_end sorted_ds_boundaries
  in
  let data_partitioned_ds_alocs =
    partition_region (AR_Global (Some DS)) 
      data_start data_end sorted_ds_boundaries
  in
  let rodata_partitioned_ds_alocs =
    partition_region (AR_Global (Some DS)) 
      rodata_start rodata_end sorted_ds_boundaries
  in
  let accessed_nds_alocs = 
    List.unique (List.map (fun (segreg, off, size) ->
      mkAloc (AR_Global (Some segreg)) off None size 0
    ) nds_boundaries)
  in
  let all_global_alocs =
    accessed_ds_alocs 
    @ bss_partitioned_ds_alocs 
    @ data_partitioned_ds_alocs 
    @ rodata_partitioned_ds_alocs
    @ accessed_nds_alocs
  in
  List.iter (fun galoc ->
    H.replace g_set galoc ()
  ) all_global_alocs


(***********************************************)
(** Deal with local and heap alocs in asm_fun **)
(***********************************************)

let rec psregs_in_re re ai =
  let no_cast_re = prune_cast re in
  match no_cast_re with
  | Coq_arith_rtl_exp(_,_,re1,re2)
  | Coq_test_rtl_exp(_,_,re1,re2) ->
    psregs_in_re re1 ai
    @ psregs_in_re re2 ai
  | Coq_if_rtl_exp(_,ge,te,ee) ->
    psregs_in_re ge ai
    @ psregs_in_re te ai
    @ psregs_in_re ee ai
  | Coq_cast_s_rtl_exp(_,_,re') 
  | Coq_cast_u_rtl_exp(_,_,re') ->
    psregs_in_re re' ai
  | Coq_get_byte_rtl_exp(are) ->
    psregs_in_re are ai
  | Coq_get_ps_reg_rtl_exp(s,psreg) ->
    let mem_size = mem_size_of_ai ai true in
    [(Big.to_int psreg), mem_size]
  | _ -> [] 

let rec psregs_in_ri ri ai =
  match ri with
  | Coq_set_loc_rtl(s,re,_) ->
    psregs_in_re re ai
  | Coq_set_ps_reg_rtl(s,re,psreg) ->
    let mem_size = mem_size_of_ai ai false in
    (Big.to_int psreg, mem_size) :: (psregs_in_re re ai)
  | Coq_set_array_rtl _ ->
    assert false
  | Coq_set_byte_rtl(vre,are) ->
    psregs_in_re vre ai 
    @ psregs_in_re are ai 
  | Coq_if_rtl(guard,ri') ->
    psregs_in_re guard ai 
    @ psregs_in_ri ri' ai
  | Coq_error_rtl 
  | Coq_trap_rtl
  | Coq_advance_oracle_rtl -> []

let alocs_of_rtl_fun l_tbl h_set elf asm_fun rtl_instrs =
  let asm_fun_name = asm_fun.func_low in
  let deal_with_local () =
    let old_CFAInfo =
	MustPntToStk.gen_must_CFA asm_fun
    in
    let frame_upper_bound =
      try    
	List.max (List.filter_map (fun (lopc, cfaexprs) ->
	  try
	    Some (-(MustPntToStk.query_about_reg cfaexprs ESP))
	  with Not_found -> None
	) old_CFAInfo)
      with _ -> 
	assert false
    in
    let local_boundaries_in_addr_re addr_re (relAddr,ai) read = 
      let _,cfaexprs =
	try List.find (fun (lopc, cfaexprs) ->
	  lopc = relAddr
	) old_CFAInfo
	with Not_found ->
	  assert false
      in
      let addr_mode = addr_mode_from_addr_re addr_re ai in
      let mem_size = mem_size_of_ai ai read in
      match addr_mode with
      | {addrDisp=disp; addrBase=(Some reg); addrIndex=_} ->
	begin match (snd ai) with
	| PUSH _ -> (**reverse addr_mode does not work for this**)
	  begin
	    try
	      let reg_off =
		MustPntToStk.query_about_reg cfaexprs reg
	      in
	      let disp = Big.opp disp in
	      let signed_native_int_of_disp = 
		try
		  (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int disp)))
		with _ ->
		  Util.int_of_int64 (MWord.of_big_int disp)
	      in
	      [None,
	       (-(reg_off+signed_native_int_of_disp)), 
	       mem_size]
	    with Not_found -> []
	  end
	| _ ->
	  begin
	    try
	      let reg_off =
		MustPntToStk.query_about_reg cfaexprs reg
	      in
	      let signed_native_int_of_disp = 
		try
		  (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int disp)))
		with _ ->
		  Util.int_of_int64 (MWord.of_big_int disp)
	      in
	      [None,
	       (-(reg_off+signed_native_int_of_disp)), 
	       mem_size]
	    with Not_found -> []
	  end
	end
      | _ -> []
    in
      
    let local_boundaries =
      List.flatten (List.map (fun (relAddr, ai, rilist) ->
	let reduced_rilist =
	  reduce_rtl false rilist []
	in
	List.flatten (List.map (fun ri ->
	  boundaries_in_ri ri (relAddr,ai) 
	    local_boundaries_in_addr_re
	) reduced_rilist)
      ) rtl_instrs)
    in
    let all_local_boundaries =
      (0,4) :: (List.map (fun (_,off,size) -> 
	off-size,size
       ) local_boundaries)
    in
    let frame_lower_bound = 
      try List.min (List.map fst all_local_boundaries) with _ -> 0
    in
    let sorted_unique_boundaries =
      List.sort_unique (fun (off1,size1) (off2,size2) ->
	if off1 = off2 then size1 - size2
	else off1 - off2
      ) all_local_boundaries
    in
    let accessed_alocs =
      List.filter_map (fun (off,size) ->
	if size = 0 then None
	else Some 
	  (mkAloc (AR_Local asm_fun_name) off None size 0)
      ) sorted_unique_boundaries
    in
    let partitioned_alocs =
      partition_region (AR_Local asm_fun_name) 
	frame_lower_bound frame_upper_bound 
	sorted_unique_boundaries
    in
    let sized_psregs = 
      List.unique (List.flatten (List.map (fun (relAddr, ai, rilist) ->
	let reduced_rilist = 
	  reduce_rtl false rilist []
	in
	List.flatten (List.map (fun ri ->
	  psregs_in_ri ri ai
	) reduced_rilist)
      ) rtl_instrs))
    in
    let psreg_alocs =
      List.map (fun (psreg,mem_size) ->
	mkAloc (AR_Psreg (asm_fun_name,psreg)) 0 None mem_size 0
      ) sized_psregs
    in
    let all_local_alocs = 
      unknownAloc :: psreg_alocs @ accessed_alocs @ partitioned_alocs
    in
    let local_aloc_set = H.create 32 in
    List.iter (fun l_aloc ->
      H.replace local_aloc_set l_aloc ()
    ) all_local_alocs;
    H.add l_tbl asm_fun_name local_aloc_set
  in

  (***********************************************)
  (** Deal with heap alocs appearred in asm_fun **)
  (***********************************************)
  let deal_with_heap () = () in  
  Stats.time "Deal with local" deal_with_local ();
  deal_with_heap ()

let alocs_of_rtl_crs elf crs =
  let r_aloc_set = H.create 32 in
  let l_aloc_table = H.create 32 in
  let ds_bounds = ref [] in
  let nds_bounds = ref [] in
  let g_aloc_set = H.create 32 in
  let h_aloc_set = H.create 32 in

  all_reg_alocs r_aloc_set;
  let asm_fun_list = 
    List.flatten (List.map (Ibaux.collect_asm_func elf) crs)
  in
  List.iter (fun asm_fun ->
    let asm_instrs =
      List.flatten (List.map (fun bb ->
	let instr_addr = ref bb.bb_relAddr in
	List.map (fun (pre,ins,len) ->
	  let res = (!instr_addr, pre, ins) in
	  instr_addr := !instr_addr + len;
	  res
	) bb.bb_instrs
      ) asm_fun.func_bbs)
    in
    let rtl_instrs =
      List.map (fun (addr,pre,ins) ->
	let rtl_instrs = 
	  Stats.time "RTL Compilation"
	    (X86_Compile.instr_to_rtl pre) ins in
	(addr, (pre,ins), rtl_instrs)
      ) asm_instrs
    in
    let new_ds_bounds, new_nds_bounds =
      Stats.time "Global alocs" 
	(global_boundaries_in_rilist elf)
	rtl_instrs
    in
    ds_bounds := new_ds_bounds @ !ds_bounds;
    nds_bounds := new_nds_bounds @ !nds_bounds;
    Stats.time "Call-frame alocs" 
      (alocs_of_rtl_fun l_aloc_table h_aloc_set elf asm_fun) 
      rtl_instrs
  ) asm_fun_list;
  Stats.time "Global alocs" 
    (partition_global_regions g_aloc_set elf !ds_bounds) 
    !nds_bounds;
  mkAlocSet r_aloc_set l_aloc_table g_aloc_set h_aloc_set


(** ASM-Constant based Approach **)
(*
 let reg_alocs_of_instr_list instr_list = 
  let aloc_of_reg opsize reg =
    match opsize,reg with
    | OpSize8, EAX
    | OpSize8, ECX
    | OpSize8, EDX
    | OpSize8, EBX ->
      mkAloc (AR_Reg reg) 0 None 1 0
    | OpSize8, ESP ->
      mkAloc (AR_Reg EAX) 1 None 1 0
    | OpSize8, EBP ->
      mkAloc (AR_Reg ECX) 1 None 1 0
    | OpSize8, ESI ->
      mkAloc (AR_Reg EDX) 1 None 1 0
    | OpSize8, EDI ->
      mkAloc (AR_Reg EBX) 1 None 1 0
    | _,_ ->
      let size = match opsize with
	| OpSize16 -> 2
	| OpSize32 -> 4
	| OpSize8 -> assert false
      in
      mkAloc (AR_Reg reg) 0 None size 0
  in
  let all_regs =
    List.flatten (List.map (fun opsize ->
      List.map (aloc_of_reg opsize) 
	[EAX;ECX;EDX;EBX;ESP;EBP;ESI;EDI]
    ) [OpSize8; OpSize16; OpSize32])
  in
  all_regs

let local_alocs_of_asm_fun local_aloc_table asm_fun =
  let partition_region rgn_start rgn_end boundaries =
    let boundaries_in_rgn =
      List.filter (fun (off,size) ->
	off >= rgn_start && off < rgn_end
      ) boundaries
    in
    let aloc_start = ref rgn_start in
    let aloc_end = ref rgn_start in
    let partitioned_alocs = ref [] in
    
    List.iter (fun (off,size) ->
      if off = !aloc_start then (
	if (off+size) <= !aloc_end then (
	  P.printf "Ignoring sub-aloc or the same aloc...\n";
	) else (
	  aloc_start := off;
	  aloc_end := off+size;
	) 
      ) else if off > !aloc_start && off < !aloc_end then (
	if (off+size) <= !aloc_end then
	  P.printf "Ignoring sub-aloc...\n"
	else (
	  aloc_start := off;
	  aloc_end := off+size
	)
      ) else if off = !aloc_end then (
	aloc_start := off;
	aloc_end := off+size
      ) else (
	let newaloc = mkAloc (AR_Local asm_fun.func_name)
	  !aloc_end None (off - !aloc_end) 0
	in
	partitioned_alocs := newaloc :: !partitioned_alocs;
	aloc_start := off;
	aloc_end := off+size
      )
    ) boundaries_in_rgn;
    
  if !aloc_end < rgn_end then (
    let tailaloc = mkAloc (AR_Local asm_fun.func_name) 
      !aloc_end None (rgn_end - !aloc_end) 0
    in
    partitioned_alocs := tailaloc :: !partitioned_alocs
  );
  !partitioned_alocs
  in
  
  let old_CFAInfo =
    (*CFAGen_new.gen_old_CFAInfo asm_fun*)
    CFAGen.must_cfa_repr_gen asm_fun
  in
  (*
  P.printf "%s:\n" asm_fun.func_name;
  CFAGen_new.dump_old_CFAInfo old_CFAInfo;
  *)
  let ebp_on_stack = 
    List.exists (fun (lopc,cfaexprs) ->
      try
	let _ = (CFAGen.query_about_reg cfaexprs EBP) in
	true
      with Not_found -> false
    ) old_CFAInfo
  in
  
  let frame_upper_bound =
    try    
      List.max (List.filter_map (fun (lopc, cfaexprs) ->
	try
	  Some (-(CFAGen.query_about_reg cfaexprs ESP))
	with Not_found -> None
      ) old_CFAInfo)
    with _ -> 
      assert false
  in

  let all_instrs =
    List.flatten (List.map (fun bb ->
      let instr_addr = ref bb.bb_relAddr in
      List.map (fun (pre,ins,len) ->
	let res = (!instr_addr, pre, ins) in
	instr_addr := !instr_addr + len;
	res
      ) bb.bb_instrs
    ) asm_fun.func_bbs)
  in

  let all_boundaries =
   List.flatten (
      List.map (fun (relAddr,pre,ins) ->
	let ops = conservative_ops_of_ins ins in
	let _, cfaexprs = 
	  try
	    List.find (fun (lopc, cfaexprs) ->
	      lopc = relAddr
	    ) old_CFAInfo
	  with Not_found ->
	    assert false
	in	
	List.filter_map (fun (w,op) ->
	  let mem_size = 
	    match ins with
	    | LEA _ -> 0
	    | _ -> get_byte_size pre w
	  in
	  match op with
	  | Imm_op _ -> None
	  | Reg_op _ -> None
	  | Address_op address ->
	    begin match address with
	    | {addrDisp=disp; addrBase=(Some reg); addrIndex=_} ->
	      if reg = ESP then (
		try
		  let esp_off = 
		    CFAGen.query_about_reg cfaexprs ESP 
		  in
		  let signed_native_int_of_disp = 
		    try
		      (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int disp)))
		    with _ ->
		      Util.int_of_int64 (MWord.of_big_int disp)
		  in
		  Some ((-(esp_off+signed_native_int_of_disp)), 
		        mem_size)
		with _ -> None
	      ) else if reg = EBP then (
		try
		  let ebp_off = 
		    CFAGen.query_about_reg cfaexprs EBP 
		  in
		  let signed_native_int_of_disp = 
		    try
		      (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int disp)))
		    with _ ->
		      Util.int_of_int64 (MWord.of_big_int disp)
		  in
		  Some ((-(ebp_off+signed_native_int_of_disp)),
			mem_size)
		with _ -> None
	      ) else None		  
	    | _ -> None
	    end
	  | Offset_op offset ->
	    None
	) ops

      ) all_instrs
    )   
  in

  let all_boundaries =
    if ebp_on_stack then
      (0,4) :: (4,4) :: (List.map (fun (off,size) -> off-size, size) all_boundaries)
    else
      (0,4) :: (List.map (fun (off,size) -> off-size, size) all_boundaries)
  in
  (*
    List.iter (fun (off,size) ->
    if off >= 0 then
    P.printf "Var_%x:%x\n" off size
    else
    P.printf "Var_-%x:%x\n" (-off) size
    ) all_boundaries;
  *)

  let frame_lower_bound = 
    try
      List.min (List.map fst all_boundaries)
    with _ -> 
      assert false
  in

  let sorted_unique_boundaries =
    List.sort_unique (fun (off1,size1) (off2,size2) ->
      if off1 = off2 then
	size1 - size2
      else off1 - off2
    ) all_boundaries
  in
  (*
  List.iter (fun (off,size) ->
    if off >= 0 then
      P.printf "Var_%x:%x\n" off size
    else
      P.printf "Var_-%x:%x\n" (-off) size
  ) sorted_unique_boundaries;
  *)

  let accessed_alocs =
    List.filter_map (fun (off,size) ->
      if size = 0 then None
      else
	Some (mkAloc (AR_Local asm_fun.func_name) off None size 0)
    ) sorted_unique_boundaries
  in
  
  let partitioned_alocs =
    partition_region frame_lower_bound frame_upper_bound 
      sorted_unique_boundaries
  in
  let local_aloc_set = H.create 32 in
  List.iter (fun l_aloc ->
    H.add local_aloc_set l_aloc ()
  ) (unknownAloc :: (accessed_alocs @ partitioned_alocs));
  H.add local_aloc_table asm_fun.func_name local_aloc_set


let global_alocs_of_instr_list elf instr_list =
  let partition_region seg_opt rgn_start rgn_end boundaries =
    let boundaries_in_rgn =
      List.filter (fun (off,size) ->
	off >= rgn_start && off < rgn_end
      ) boundaries
    in
    let aloc_start = ref rgn_start in
    let aloc_end = ref rgn_start in
    let partitioned_alocs = ref [] in
    
    List.iter (fun (off,size) ->
      if off = !aloc_start then (
	if (off+size) <= !aloc_end then (
	  P.printf "Ignoring sub-aloc or the same aloc...\n";
	) else (
	  aloc_start := off;
	  aloc_end := off+size;
	) 
      ) else if off > !aloc_start && off < !aloc_end then (
	if (off+size) <= !aloc_end then
	  P.printf "Ignoring sub-aloc...\n"
	else (
	  aloc_start := off;
	  aloc_end := off+size
	)
      ) else if off = !aloc_end then (
	aloc_start := off;
	aloc_end := off+size
      ) else (
	let newaloc = mkAloc (AR_Global seg_opt)
	  !aloc_end None (off - !aloc_end) 0
	in
	partitioned_alocs := newaloc :: !partitioned_alocs;
	aloc_start := off;
	aloc_end := off+size
      )
    ) boundaries_in_rgn;
    
  if !aloc_end < rgn_end then (
    let tailaloc = mkAloc (AR_Global seg_opt) 
      !aloc_end None (rgn_end - !aloc_end) 0
    in
    partitioned_alocs := tailaloc :: !partitioned_alocs
  );
  !partitioned_alocs
  in

  let bss_start = fst (bss_range elf) in
  let bss_end = snd (bss_range elf) in
  let data_start = fst (data_range elf) in
  let data_end = snd (data_range elf) in
  let rodata_start = fst (rodata_range elf) in
  let rodata_end = snd (rodata_range elf) in

  let int_in_range i l h =
    i >= l && i < h
  in

  let all_boundaries =
    List.flatten (
      List.map (fun (relAddr,pre,ins) ->
	let ops = conservative_ops_of_ins ins in
	List.filter_map (fun (w,op) ->
	  let mem_size = 
	    match ins with
	    | LEA _ -> 0
	    | _ -> get_byte_size pre w
	  in
	  match op with
	  | Imm_op _ -> None
	  | Reg_op _ -> None
	  | Address_op address ->
	    begin match address with
	    | {addrDisp=disp; addrBase=_; addrIndex=_} ->
	      if pre.seg_override = None then
		if int_in_range (Big.to_int disp) bss_start bss_end ||
		  int_in_range (Big.to_int disp) data_start data_end ||
		  int_in_range (Big.to_int disp) rodata_start rodata_end 
		then
		  Some (None, (Big.to_int disp), mem_size)	
		else (
		  (*
		    P.printf "Error instruction at 0x%x:%s\n" 
		    relAddr
		    (str_of_instr (pre,ins));*)
							None
		)
	      else
		Some (pre.seg_override, 
		      (Big.to_int disp),
		      mem_size)
	    (*| _ -> None*)
	    end
	  | Offset_op offset ->
	    if pre.seg_override = None then 
	      Some (Some DS, 
		    (Big.to_int offset), 
		    mem_size)
	    else
	      Some (pre.seg_override, 
		    (Big.to_int offset), 
		    mem_size)
	) ops
      ) instr_list
    )
  in

  let ds_boundaries =
    List.unique (List.filter_map (fun (segreg_op, off, size) ->
      if segreg_op = None then
	Some (off,size)
      else if Option.get segreg_op = DS then
	Some (off,size)
      else None
    ) all_boundaries)
  in
  let nds_boundaries =
    List.unique (List.filter_map (fun (segreg_op, off, size) ->
      if segreg_op = None then None
      else if Option.get segreg_op = DS then None
      else
	Some (Option.get segreg_op, off, size)
    ) all_boundaries)
  in
  let sorted_ds_boundaries =
    List.sort (fun (addr1,off1) (addr2,off2) ->
      if addr1 = addr2 then
	off1 - off2
      else addr1 - addr2
    ) ds_boundaries
  in
  let accessed_ds_alocs =
    List.filter_map (fun (off,size) ->
      if size = 0 then None
      else
	Some (mkAloc (AR_Global (Some DS)) off None size 0)
    ) sorted_ds_boundaries
  in
  
  let bss_partitioned_ds_alocs =
    partition_region (Some DS) bss_start bss_end sorted_ds_boundaries
  in


  let data_partitioned_ds_alocs =
    partition_region (Some DS) data_start data_end sorted_ds_boundaries
  in

  let rodata_partitioned_ds_alocs =
    partition_region (Some DS) rodata_start rodata_end sorted_ds_boundaries
  in
  (*
  P.printf "bss_start:%x, bss_end:%x\n" bss_start bss_end;
  P.printf "data_start:%x, data_end:%x\n" data_start data_end;
  P.printf "rodata_start:%x, rodata_end:%x\n" rodata_start rodata_end;
  *)
  (** nds_alocs are abstract locations that are accessed by an
      offset to a segment register other than DS; in theory, a 
      segment register's value may change; we assume they do not 
      and use them to represent several Abstract Regions.
  **)
  
  let accessed_nds_alocs = 
    List.map (fun (segreg, off, size) ->
      mkAloc (AR_Global (Some segreg)) off None size 0
    ) nds_boundaries
  in
  
  let ds_alocs =
    accessed_ds_alocs @ bss_partitioned_ds_alocs 
    @ data_partitioned_ds_alocs @ rodata_partitioned_ds_alocs
  in
  let nds_alocs =
    accessed_nds_alocs
  in
  ds_alocs @ nds_alocs
  
let heap_alocs_of_instr_list instr_list = []

(*
let alocs_of_asm_fun elf asm_fun =
  let all_instrs = 
    List.flatten (List.map (fun bb ->
      List.map (fun (pre,ins,len) ->
	(pre,ins)
      ) bb.bb_instrs
    ) asm_fun.func_bbs)
  in
  alocs_of_instr_list elf all_instrs
*)

let alocs_of_crs elf crs =
  let all_instrs =
    List.flatten (List.map (fun cr ->
      List.flatten (List.map (fun bb ->
	let instr_addr = ref bb.bb_relAddr in
	List.map (fun (pre,ins,len) ->
	  let res = (!instr_addr, pre, ins) in
	  instr_addr := !instr_addr + len;
	  res
	) bb.bb_instrs
      ) (BBSet.elements cr.bbs))
    ) crs)
  in

  let r_alocs =
    reg_alocs_of_instr_list all_instrs
  in
  let r_aloc_set = H.create 32 in
  List.iter (fun raloc ->
    H.add r_aloc_set raloc ()
  ) r_alocs;

  let g_alocs = 
    global_alocs_of_instr_list elf all_instrs
  in
  let g_aloc_set = H.create 32 in
  List.iter (fun galoc ->
    H.add g_aloc_set galoc ()
  ) g_alocs;

  let asm_fun_list = 
    List.flatten (List.map (Ibaux.collect_asm_func elf) crs)
  in
  
  let local_aloc_table = H.create 32 in

  List.iter (fun asm_fun ->
    local_alocs_of_asm_fun local_aloc_table asm_fun
  ) asm_fun_list;
  
  let l_aloc_set = local_aloc_table in
  
  let h_alocs =
    heap_alocs_of_instr_list all_instrs
  in
  let h_aloc_set = H.create 32 in
  List.iter (fun haloc ->
    H.add h_aloc_set haloc ()
  ) h_alocs;
  mkAlocSet r_aloc_set l_aloc_set g_aloc_set h_aloc_set

let alocs_of_locals elf crs fname =
  let asm_fun_list = 
    List.flatten (List.map (Ibaux.collect_asm_func elf) crs)
  in
  
  let target_asm_fun =
    List.find (fun asm_fun ->
      asm_fun.func_name = fname
    ) asm_fun_list
  in
  
  let local_aloc_table = H.create 32 in

  local_alocs_of_asm_fun local_aloc_table target_asm_fun;
  
  let l_aloc_set = local_aloc_table in
  l_aloc_set

let alocs_of_globals elf crs =
  let all_instrs =
    List.flatten (List.map (fun cr ->
      List.flatten (List.map (fun bb ->
	let instr_addr = ref bb.bb_relAddr in
	List.map (fun (pre,ins,len) ->
	  let res = (!instr_addr, pre, ins) in
	  instr_addr := !instr_addr + len;
	  res
	) bb.bb_instrs
      ) (BBSet.elements cr.bbs))
    ) crs)
  in

  let r_alocs =
    reg_alocs_of_instr_list all_instrs
  in
  let r_aloc_set = H.create 32 in
  List.iter (fun raloc ->
    H.add r_aloc_set raloc ()
  ) r_alocs;

  let g_alocs = 
    global_alocs_of_instr_list elf all_instrs
  in
  let g_aloc_set = H.create 32 in
  List.iter (fun galoc ->
    H.add g_aloc_set galoc ()
  ) g_alocs;
  let h_alocs =
    heap_alocs_of_instr_list all_instrs
  in
  let h_aloc_set = H.create 32 in
  List.iter (fun haloc ->
    H.add h_aloc_set haloc ()
  ) h_alocs;
  r_aloc_set, g_aloc_set, h_aloc_set

let alocs_of_fun elf crs fname =
  let asm_fun_list = 
    List.flatten (List.map (Ibaux.collect_asm_func elf) crs)
  in
 
  let target_asm_fun =
    List.find (fun asm_fun ->
      asm_fun.func_name = fname
    ) asm_fun_list
  in
  
  let fun_instrs =
    List.flatten (List.map (fun bb ->
      let instr_addr = ref bb.bb_relAddr in
      List.map (fun (pre,ins,len) ->
	let res = (!instr_addr, pre, ins) in
	instr_addr := !instr_addr + len;
	res
      ) bb.bb_instrs
    ) target_asm_fun.func_bbs)
  in

  let local_aloc_table = H.create 32 in

  local_alocs_of_asm_fun local_aloc_table target_asm_fun;
  
  let l_aloc_set = local_aloc_table in

  let r_alocs =
    reg_alocs_of_instr_list fun_instrs
  in
  let r_aloc_set = H.create 32 in
  List.iter (fun raloc ->
    H.add r_aloc_set raloc ()
  ) r_alocs;

  let g_alocs = 
    global_alocs_of_instr_list elf fun_instrs
  in
  let g_aloc_set = H.create 32 in
  List.iter (fun galoc ->
    H.add g_aloc_set galoc ()
  ) g_alocs;

  let h_alocs =
    heap_alocs_of_instr_list fun_instrs
  in
  let h_aloc_set = H.create 32 in
  List.iter (fun haloc ->
    H.add h_aloc_set haloc ()
  ) h_alocs;
  mkAlocSet r_aloc_set l_aloc_set g_aloc_set h_aloc_set

*)

(** Some deprecated code **) 
(*
let ops_of_ins ins =
  match ins with
  | PUSH (w,op) -> [(w,op)]
  | ADD (w,op1,op2)
  | SUB (w,op1,op2)
  | MOV (w,op1,op2) -> [(w,op1);(w,op2)]
  | LEA (op1,op2) -> [(true,op1);(true,op2)] 
  | _ -> []

let conservative_ops ops =
  List.filter (fun op ->
    match op with
    | _, Address_op {addrDisp = _; addrBase = _; addrIndex = Some ndx} -> false
    | _ -> true    
  ) ops

let conservative_ops_of_ins ins =
  let ops = ops_of_ins ins in
  conservative_ops ops

let local_alocs_of_rtl_fun local_aloc_table asm_fun =
  let partition_region rgn_start rgn_end boundaries =
    let boundaries_in_rgn =
      List.filter (fun (off,size) ->
	off >= rgn_start && off < rgn_end
      ) boundaries
    in
    let aloc_start = ref rgn_start in
    let aloc_end = ref rgn_start in
    let partitioned_alocs = ref [] in
    
    List.iter (fun (off,size) ->
      if off = !aloc_start then (
	if (off+size) <= !aloc_end then (
	  P.printf "Ignoring sub-aloc or the same aloc...\n";
	) else (
	  aloc_start := off;
	  aloc_end := off+size;
	) 
      ) else if off > !aloc_start && off < !aloc_end then (
	if (off+size) <= !aloc_end then
	  P.printf "Ignoring sub-aloc...\n"
	else (
	  aloc_start := off;
	  aloc_end := off+size
	)
      ) else if off = !aloc_end then (
	aloc_start := off;
	aloc_end := off+size
      ) else (
	let newaloc = mkAloc (AR_Local asm_fun.func_name)
	  !aloc_end None (off - !aloc_end) 0
	in
	partitioned_alocs := newaloc :: !partitioned_alocs;
	aloc_start := off;
	aloc_end := off+size
      )
    ) boundaries_in_rgn;
    
  if !aloc_end < rgn_end then (
    let tailaloc = mkAloc (AR_Local asm_fun.func_name) 
      !aloc_end None (rgn_end - !aloc_end) 0
    in
    partitioned_alocs := tailaloc :: !partitioned_alocs
  );
  !partitioned_alocs
  in
  let old_CFAInfo =
    (*CFAGen_new.gen_old_CFAInfo asm_fun*)
    CFAGen.must_cfa_repr_gen asm_fun
  in

  let frame_upper_bound =
    try    
      List.max (List.filter_map (fun (lopc, cfaexprs) ->
	try
	  Some (-(CFAGen.query_about_reg cfaexprs ESP))
	with Not_found -> None
      ) old_CFAInfo)
    with _ -> 
      assert false
  in

  let all_instrs =
    List.flatten (List.map (fun bb ->
      let instr_addr = ref bb.bb_relAddr in
      List.map (fun (pre,ins,len) ->
	let res = (!instr_addr, pre, ins) in
	instr_addr := !instr_addr + len;
	res
      ) bb.bb_instrs
    ) asm_fun.func_bbs)
  in

  let all_rtl_instrs =
    List.map (fun (addr,pre,ins) ->
      let rtl_instrs = 
	Stats.time "RTL Compilation" 
	  (X86_Compile.instr_to_rtl pre) ins in
      (addr, (pre,ins), rtl_instrs)
    ) all_instrs
  in

  let local_boundaries_in_addr_re addr_re ai cfaexprs = 
    let addr_mode = addr_mode_from_addr_re addr_re ai in
    let mem_size = mem_size_of_ai ai in
    match addr_mode with
    | {addrDisp=disp; addrBase=(Some reg); addrIndex=_} ->
      begin try
	let reg_off =
	  CFAGen.query_about_reg cfaexprs reg
	in
	let signed_native_int_of_disp = 
	  try
	    (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int disp)))
	  with _ ->
	    Util.int_of_int64 (MWord.of_big_int disp)
	in
	[((-(reg_off+signed_native_int_of_disp)), 
	  mem_size)]
	with Not_found -> []
      end
    | _ -> []
  in
  
  let rec local_boundaries_in_re re ai cfaexprs =
    let no_cast_re = prune_cast re in
    match no_cast_re with
    | Coq_arith_rtl_exp(_,_,re1,re2)
    | Coq_test_rtl_exp(_,_,re1,re2) ->
      local_boundaries_in_re re1 ai cfaexprs
      @ local_boundaries_in_re re2 ai cfaexprs
    | Coq_if_rtl_exp(_,ge,te,ee) ->
      local_boundaries_in_re ge ai cfaexprs
      @ local_boundaries_in_re te ai cfaexprs
      @ local_boundaries_in_re ee ai cfaexprs
    | Coq_cast_s_rtl_exp(_,_,re') 
    | Coq_cast_u_rtl_exp(_,_,re') ->
      local_boundaries_in_re re' ai cfaexprs
    | Coq_get_byte_rtl_exp(are) ->
      local_boundaries_in_addr_re are ai cfaexprs
    | _ -> [] 
  in
  
  let rec local_boundaries_in_ri ri ai cfaexprs =
    match ri with
    | Coq_set_loc_rtl(s,re,_)
    | Coq_set_ps_reg_rtl(s,re,_) ->
      local_boundaries_in_re re ai cfaexprs
    | Coq_set_array_rtl _ ->
      assert false
    | Coq_set_byte_rtl(vre,are) ->
      local_boundaries_in_re vre ai cfaexprs
      @	local_boundaries_in_addr_re are ai cfaexprs
    | Coq_if_rtl(guard,ri') ->
      local_boundaries_in_re guard ai cfaexprs
      @ local_boundaries_in_ri ri' ai cfaexprs
    | Coq_error_rtl 
    | Coq_trap_rtl
    | Coq_advance_oracle_rtl -> []
  in

  let all_boundaries =
    List.flatten (List.map (fun (relAddr, ai, rilist) ->
      let reduced_rilist =
	reduce_rtl false rilist []
      in
      List.flatten (List.map (fun ri ->
	let _,cfaexprs =
	  try
	    List.find (fun (lopc, cfaexprs) ->
	      lopc = relAddr
	    ) old_CFAInfo
	  with Not_found ->
	    assert false
	in
	local_boundaries_in_ri ri ai cfaexprs
      ) reduced_rilist)
    ) all_rtl_instrs)
  in

  let all_boundaries =
    (0,4) :: (List.map (fun (off,size) -> 
      off-size,size
     ) all_boundaries)
  in
      
  let frame_lower_bound = 
    try
      List.min (List.map fst all_boundaries)
    with _ -> 0
  in

  let sorted_unique_boundaries =
    List.sort_unique (fun (off1,size1) (off2,size2) ->
      if off1 = off2 then
	size1 - size2
      else off1 - off2
    ) all_boundaries
  in
  let accessed_alocs =
    List.filter_map (fun (off,size) ->
      if size = 0 then None
      else
	Some (mkAloc (AR_Local asm_fun.func_name) off None size 0)
    ) sorted_unique_boundaries
  in
  
  let partitioned_alocs =
    partition_region frame_lower_bound frame_upper_bound 
      sorted_unique_boundaries
  in

  let psreg_alocs = [] in
  
  let all_alocs =
    unknownAloc :: psreg_alocs @ accessed_alocs @ partitioned_alocs
  in
  
  let local_aloc_set = H.create 32 in
  List.iter (fun l_aloc ->
    H.add local_aloc_set l_aloc ()
  ) all_alocs;
  H.add local_aloc_table asm_fun.func_name local_aloc_set

let global_alocs_of_rtl_instrs elf all_rtl_instrs =
  let partition_region seg_opt rgn_start rgn_end boundaries =
    let boundaries_in_rgn =
      List.filter (fun (off,size) ->
	off >= rgn_start && off < rgn_end
      ) boundaries
    in
    let aloc_start = ref rgn_start in
    let aloc_end = ref rgn_start in
    let partitioned_alocs = ref [] in
    
    List.iter (fun (off,size) ->
      if off = !aloc_start then (
	if (off+size) <= !aloc_end then (
	  P.printf "Ignoring sub-aloc or the same aloc...\n";
	) else (
	  aloc_start := off;
	  aloc_end := off+size;
	) 
      ) else if off > !aloc_start && off < !aloc_end then (
	if (off+size) <= !aloc_end then
	  P.printf "Ignoring sub-aloc...\n"
	else (
	  aloc_start := off;
	  aloc_end := off+size
	)
      ) else if off = !aloc_end then (
	aloc_start := off;
	aloc_end := off+size
      ) else (
	let newaloc = mkAloc (AR_Global seg_opt)
	  !aloc_end None (off - !aloc_end) 0
	in
	partitioned_alocs := newaloc :: !partitioned_alocs;
	aloc_start := off;
	aloc_end := off+size
      )
    ) boundaries_in_rgn;
    
    if !aloc_end < rgn_end then (
      let tailaloc = mkAloc (AR_Global seg_opt) 
	!aloc_end None (rgn_end - !aloc_end) 0
      in
      partitioned_alocs := tailaloc :: !partitioned_alocs
    );
    !partitioned_alocs
  in
  let bss_start = fst (bss_range elf) in
  let bss_end = snd (bss_range elf) in
  let data_start = fst (data_range elf) in
  let data_end = snd (data_range elf) in
  let rodata_start = fst (rodata_range elf) in
  let rodata_end = snd (rodata_range elf) in

  let int_in_range i l h =
    i >= l && i < h
  in
  
  let global_boundaries_in_addr_re addr_re ai =
    let pre, ins = ai in
    let addr_mode = addr_mode_from_addr_re addr_re ai in
    let mem_size = mem_size_of_ai ai in
    match addr_mode with
    | {addrDisp=disp; addrBase=_; addrIndex=_} ->
      if pre.seg_override = None then
	if int_in_range (Big.to_int disp) bss_start bss_end ||
	  int_in_range (Big.to_int disp) data_start data_end ||
	  int_in_range (Big.to_int disp) rodata_start rodata_end 
	then
	  [None, (Big.to_int disp), mem_size]	
	else []
      else
	[pre.seg_override, (Big.to_int disp),mem_size]
  in

  let rec global_boundaries_in_re re ai =
    let no_cast_re = prune_cast re in
    match no_cast_re with
    | Coq_arith_rtl_exp(_,_,re1,re2)
    | Coq_test_rtl_exp(_,_,re1,re2) ->
      global_boundaries_in_re re1 ai
      @ global_boundaries_in_re re2 ai
    | Coq_if_rtl_exp(_,ge,te,ee) ->
      global_boundaries_in_re ge ai
      @ global_boundaries_in_re te ai
      @ global_boundaries_in_re ee ai
    | Coq_cast_s_rtl_exp(_,_,re') 
    | Coq_cast_u_rtl_exp(_,_,re') ->
      global_boundaries_in_re re' ai
    | Coq_get_byte_rtl_exp(are) ->
      global_boundaries_in_addr_re are ai
    | _ -> []
  in

  let rec global_boundaries_in_ri ri ai =
    match ri with
    | Coq_set_loc_rtl(s,re,_)
    | Coq_set_ps_reg_rtl(s,re,_) ->
      global_boundaries_in_re re ai
    | Coq_set_array_rtl _ ->
      assert false
    | Coq_set_byte_rtl(vre,are) ->
      global_boundaries_in_re vre ai
      @	global_boundaries_in_addr_re are ai
    | Coq_if_rtl(guard,ri') ->
      global_boundaries_in_re guard ai
      @ global_boundaries_in_ri ri' ai
    | Coq_error_rtl 
    | Coq_trap_rtl
    | Coq_advance_oracle_rtl -> []
  in

  let all_boundaries =
    List.flatten (List.map (fun (relAddr, ai, rilist) ->
      let reduced_rilist =
	reduce_rtl false rilist []
      in
      List.flatten (List.map (fun ri ->
	global_boundaries_in_ri ri ai
      ) reduced_rilist)
    ) all_rtl_instrs)
  in
  let ds_boundaries =
    List.unique (List.filter_map (fun (segreg_op, off, size) ->
      if segreg_op = None then
	Some (off,size)
      else if Option.get segreg_op = DS then
	Some (off,size)
      else None
    ) all_boundaries)
  in
  let nds_boundaries =
    List.unique (List.filter_map (fun (segreg_op, off, size) ->
      if segreg_op = None then None
      else if Option.get segreg_op = DS then None
      else
	Some (Option.get segreg_op, off, size)
    ) all_boundaries)
  in
  let sorted_ds_boundaries =
    List.sort (fun (addr1,off1) (addr2,off2) ->
      if addr1 = addr2 then
	off1 - off2
      else addr1 - addr2
    ) ds_boundaries
  in
  let accessed_ds_alocs =
    List.filter_map (fun (off,size) ->
      if size = 0 then None
      else
	Some (mkAloc (AR_Global (Some DS)) off None size 0)
    ) sorted_ds_boundaries
  in
  
  let bss_partitioned_ds_alocs =
    partition_region (Some DS) bss_start bss_end sorted_ds_boundaries
  in

  let data_partitioned_ds_alocs =
    partition_region (Some DS) data_start data_end sorted_ds_boundaries
  in

  let rodata_partitioned_ds_alocs =
    partition_region (Some DS) rodata_start rodata_end sorted_ds_boundaries
  in
  let accessed_nds_alocs = 
    List.map (fun (segreg, off, size) ->
      mkAloc (AR_Global (Some segreg)) off None size 0
    ) nds_boundaries
  in
  
  let ds_alocs =
    accessed_ds_alocs @ bss_partitioned_ds_alocs 
    @ data_partitioned_ds_alocs @ rodata_partitioned_ds_alocs
  in
  let nds_alocs =
    accessed_nds_alocs
  in
  ds_alocs @ nds_alocs

*)
