open Batteries
open Bigarray
module H = Hashtbl
module P = Printf

open IO

open Util
open Config

open Bits
open X86Syntax
open Abbrev
open Instr
open Elf

module AddrOrder = struct
  type t = int
  let compare (a1: int) (a2: int) =
    Pervasives.compare a1 a2
end

module AddrSet = Set.Make (AddrOrder)

(** type for basic blocks *)
type basicblock = {
  (** a string labeling the basic block, maybe empty *)
  mutable bb_label : string;

  mutable bb_symbol_label : string;

  (** list of instructions; should end with a control-transfer instruction *)
  mutable bb_instrs : (prefix * instr * int) list;

  (** the relative address of the block beginning;
      relative to the beginning address of the segment *)
  mutable bb_relAddr : int;

  (** size of the basic blocks; can be computed from instrs *)
  mutable bb_size : int;

  (** The set of known successor basic blocks **)
  mutable bb_succs: string list;

  (** The set of known interprocedural successor basic blocks *)
  mutable bb_inter_succs : string list;

  (** The set of known predecessor basic blocks *)
  mutable bb_preds : string list; 

  mutable bb_inter_preds : string list;

  (* possible more fields: direct jmp, direct call, indirect jump, indirect call, hlt, conditional jumps *)

}

module BasicBlockOrder = struct
  type t = basicblock
  let compare (bb1:basicblock) (bb2:basicblock) = 
    Pervasives.compare bb1.bb_relAddr bb2.bb_relAddr
end

(* the type for a set of basic blocks *)
module BBSet = Set.Make (BasicBlockOrder)

(** A region of code *)
type code_region = {
  (** the absolute start address of the region *)
  mutable startAddr : mword;

  (** the set of basic blocks *)
  mutable bbs : BBSet.t;
}

(** get the size of an instruction list *)
let get_il_size instrs = 
  List.fold_left (fun t (ins,pre,sz) -> t+sz) 0 instrs

let mkBB label instrs offset = 
  {bb_label=label; bb_symbol_label=""; bb_instrs=instrs; bb_relAddr=offset; 
   bb_size=get_il_size instrs; bb_succs=[]; bb_inter_succs=[]; bb_preds=[];bb_inter_preds=[]}

let mkCR start bbs = {startAddr=start; bbs=bbs}

let rec instrs_with_starts start instrs = match instrs with
  | [] -> []
  | (pre,ins,len) :: instrs' -> 
    (start,pre,ins,len) :: 
      instrs_with_starts (start+len) instrs'

let print_bb oc bb =
  P.fprintf oc "{\n";
  P.fprintf oc "bb_label = %s\n" bb.bb_label;
  P.fprintf oc "bb_symbol_label = %s\n" bb.bb_symbol_label;
  P.fprintf oc "bb_instrs = \n";
  (*List.iter print_rtl_instr rb.rb_instrs;*)
  List.iter 
    (fun (loc, pre, ins, len) -> 
      P.fprintf oc " %s %d\t%s\t%s\n" (str_of_mword_flex (MWord.of_int loc)) len (str_of_prefix (pre,ins)) (str_of_instr (pre,ins));
      )  (instrs_with_starts (bb.bb_relAddr) bb.bb_instrs);	
  P.fprintf oc "bb_relAddr = %s\n" (str_of_mword_flex (MWord.of_int (bb.bb_relAddr)));
  P.fprintf oc "bb_succs = \n";
  List.iter (fun id -> P.fprintf oc "%s " id) bb.bb_succs;
  P.fprintf oc "\n";
  P.fprintf oc "bb_inter_succs = \n";
  List.iter (fun id -> P.fprintf oc "%s " id) bb.bb_inter_succs;
  P.fprintf oc "\n";
  P.fprintf oc "bb_preds = \n";
  List.iter (fun id -> P.fprintf oc "%s " id) bb.bb_preds;
  P.fprintf oc "\n";
  P.fprintf oc "bb_inter_preds = \n";
  List.iter (fun id -> P.fprintf oc "%s " id) bb.bb_inter_preds;
  P.fprintf oc "\n";
  P.fprintf oc "}\n\n";
  flush oc

let dump_bbs oc bbs start =
  BBSet.iter (fun bb -> print_bb oc bb) bbs

let dump_cr_list oc cr_list =
  List.iter (fun cr -> dump_bbs oc cr.bbs cr.startAddr) cr_list 

type func = {
  func_name: string;
  func_low: string;
  func_high: string;
  func_bbs: basicblock list;
}

let mkFUNC name low high bbs =
  {
    func_name = name;
    func_low = low;
    func_high = high;
    func_bbs = bbs;
  }

let print_func (func:func) =
  print_string "{\n";
  P.printf " function name: %s\n" func.func_name;
  P.printf " low pc:%s\n" func.func_low;
  P.printf " high pc:%s\n" func.func_high;
  print_string " Containing blocks:\n";
  List.iter (fun bb -> P.printf " %s" bb.bb_label) func.func_bbs;
  print_string "\n} \n"

let does_bb_ret bb =
  let (pre,ins,len) = (List.last bb.bb_instrs) in
  match ins with
    | RET (_,_) -> true
    | _ -> false

let does_bb_call bb =
  let (pre,ins,len) = (List.last bb.bb_instrs) in
  match ins with
    | CALL (_,_,_,_) -> true
    | _ -> false

let does_bb_jump bb =
  let (pre,ins,len) = (List.last bb.bb_instrs) in
  match ins with
    | JMP (_,_,_,_) -> true
    | _ -> false

let does_bb_jcc bb =
  let (pre,ins,len) = (List.last bb.bb_instrs) in
  match ins with
    | Jcc (_,_) | JCXZ _ -> true
    | _ -> false

let does_bb_icall bb =
  let (pre,ins,len) = (List.last bb.bb_instrs) in
  match ins with
    | CALL (true,abs,op,None) -> (
      match op with
      | Imm_op imm -> false
      | _ -> true
    )
    | _ -> false

let does_bb_ijump bb =
  let (pre,ins,len) = (List.last bb.bb_instrs) in
  match ins with
    | JMP (true,abs,op,None) -> (
      match op with
      | Imm_op imm -> false
      | _ -> true 
    )
    | _ -> false

let does_bb_ibranch bb =
  does_bb_ret bb || does_bb_icall bb || does_bb_ijump bb

let rec num_of_blocks cr_lst =
  match cr_lst with
  | [] -> 0
  | h_cr :: t_cr_lst -> (BBSet.cardinal h_cr.bbs) + (num_of_blocks t_cr_lst)


let rec sum_of_int_list int_lst =
  match int_lst with
  | [] -> 0
  | h :: t -> h + (sum_of_int_list t)

let rec max_of_int_list int_lst =
  match int_lst with
  | [] -> (-1,0)
  | (h',h) :: [] -> (h',h)
  | (h',h) :: tl ->
    let max',max = max_of_int_list tl in
    if h > max then (h',h)
    else (max',max)

let rec num_of_out_edges cr_lst =
  let num_of_out_edges_aux bbs =
    let bb_lst = BBSet.elements bbs in
    let edge_num_lst = List.map (fun bb -> List.length bb.bb_succs) bb_lst in
    sum_of_int_list edge_num_lst
  in
  match cr_lst with
  | [] -> 0
  | h_cr :: t_cr_lst -> (num_of_out_edges_aux h_cr.bbs) + (num_of_out_edges t_cr_lst)  

let rec num_of_in_edges cr_lst =
  let num_of_out_edges_aux bbs =
    let bb_lst = BBSet.elements bbs in
    let edge_num_lst = List.map (fun bb -> List.length bb.bb_preds) bb_lst in
    sum_of_int_list edge_num_lst
  in
  match cr_lst with
  | [] -> 0
  | h_cr :: t_cr_lst -> (num_of_out_edges_aux h_cr.bbs) + (num_of_out_edges t_cr_lst)  

let average_out_edges cr_lst =
  (float_of_int (num_of_out_edges cr_lst)) /. (float_of_int (num_of_blocks cr_lst))

let average_in_edges cr_lst =
  (float_of_int (num_of_in_edges cr_lst)) /. (float_of_int (num_of_blocks cr_lst))

let max_out_edges cr_lst =
  let bbs_lst = List.map (fun cr -> cr.bbs) cr_lst in
  let bb_lst = List.flatten (List.map BBSet.elements bbs_lst) in
  let edge_num_lst = List.map (fun bb -> bb.bb_relAddr, (List.length bb.bb_succs)) bb_lst in
  if List.length edge_num_lst = 0 then None
  else Some (max_of_int_list edge_num_lst)

let max_in_edges cr_lst =
  let bbs_lst = List.map (fun cr -> cr.bbs) cr_lst in
  let bb_lst = List.flatten (List.map BBSet.elements bbs_lst) in
  let edge_num_lst = List.map (fun bb -> bb.bb_relAddr, (List.length bb.bb_preds)) bb_lst in
  if List.length edge_num_lst = 0 then None
  else Some (max_of_int_list edge_num_lst)

let rec num_of_IB cr_lst =
  let num_of_IB_aux bbs =
    (*let bb_lst = BBSet.elements bbs in*)
    let ibset = BBSet.filter (fun bb -> does_bb_ibranch bb && List.length bb.bb_succs <> 0) bbs in
    BBSet.cardinal ibset
  in
  match cr_lst with
  | [] -> 0
  | h :: t -> (num_of_IB_aux h.bbs) + (num_of_IB t)

let rec num_of_IBT cr_lst =
  let num_of_IBT_aux bbs =
    let bb_lst = BBSet.elements bbs in
    let ib_lst = List.filter (fun bb -> does_bb_ibranch bb && List.length bb.bb_succs <> 0) bb_lst in
    let ibt_lst = List.map (fun bb -> List.length bb.bb_succs) ib_lst in
    sum_of_int_list ibt_lst
  in
  match cr_lst with
  | [] -> 0
  | h_cr :: t_cr_lst -> (num_of_IBT_aux h_cr.bbs) + (num_of_IBT t_cr_lst)

let rec num_of_IC cr_lst =
  let num_of_IC_aux bbs =
    (*let bb_lst = BBSet.elements bbs in*)
    let icset = BBSet.filter (fun bb -> does_bb_icall bb && List.length bb.bb_succs <> 0) bbs in
    BBSet.cardinal icset
  in
  match cr_lst with
  | [] -> 0
  | h :: t -> (num_of_IC_aux h.bbs) + (num_of_IC t)

let rec num_of_ICT cr_lst =
  let num_of_ICT_aux bbs =
    let bb_lst = BBSet.elements bbs in
    let ic_lst = List.filter (fun bb -> does_bb_icall bb && List.length bb.bb_succs <> 0) bb_lst in
    let ict_lst = List.map (fun bb -> List.length bb.bb_succs) ic_lst in
    sum_of_int_list ict_lst
  in
  match cr_lst with
  | [] -> 0
  | h_cr :: t_cr_lst -> (num_of_ICT_aux h_cr.bbs) + (num_of_ICT t_cr_lst)

let rec num_of_IJ cr_lst =
  let num_of_IJ_aux bbs =
    (*let bb_lst = BBSet.elements bbs in*)
    let ibset = BBSet.filter (fun bb -> does_bb_ijump bb && List.length bb.bb_succs <> 0) bbs in
    BBSet.cardinal ibset
  in
  match cr_lst with
  | [] -> 0
  | h :: t -> (num_of_IJ_aux h.bbs) + (num_of_IJ t)

let rec num_of_IJT cr_lst =
  let num_of_IJT_aux bbs =
    let bb_lst = BBSet.elements bbs in
    let ij_lst = List.filter (fun bb -> does_bb_ijump bb && List.length bb.bb_succs <> 0) bb_lst in
    let ijt_lst = List.map (fun bb -> List.length bb.bb_succs) ij_lst in
    sum_of_int_list ijt_lst
  in
  match cr_lst with
  | [] -> 0
  | h_cr :: t_cr_lst -> (num_of_IJT_aux h_cr.bbs) + (num_of_IJT t_cr_lst)

let rec num_of_mcfi_IB cr_lst =
  let num_of_IB_aux bbs =
    (*let bb_lst = BBSet.elements bbs in*)
    let ibset = BBSet.filter (fun bb -> (does_bb_icall bb || does_bb_ret bb) && List.length bb.bb_succs <> 0) bbs in
    BBSet.cardinal ibset
  in
  match cr_lst with
  | [] -> 0
  | h :: t -> (num_of_IB_aux h.bbs) + (num_of_IB t)

let rec num_of_mcfi_IBT cr_lst =
  let num_of_IBT_aux bbs =
    let bb_lst = BBSet.elements bbs in
    let ib_lst = List.filter (fun bb -> (does_bb_icall bb || does_bb_ret bb) && List.length bb.bb_succs <> 0) bb_lst in
    let ibt_lst = List.map (fun bb -> List.length bb.bb_succs) ib_lst in
    sum_of_int_list ibt_lst
  in
  match cr_lst with
  | [] -> 0
  | h_cr :: t_cr_lst -> (num_of_IBT_aux h_cr.bbs) + (num_of_IBT t_cr_lst)

let avg_ibt_after_mcfi_eqc_merge cr_lst =
  let rec succs_interleave succ_list1 succ_list2 =
    match succ_list1 with
    | [] -> false
    | hd_succ :: tl_succ_list -> 
      List.mem hd_succ succ_list2 || succs_interleave tl_succ_list succ_list2
  in
  let rec eliminate_dup list =
    match list with
    | [] -> []
    | h :: t ->
      if List.mem h t then eliminate_dup t
      else h :: (eliminate_dup t)
  in
  let all_blocks = List.flatten (List.map (fun cr -> BBSet.elements cr.bbs) cr_lst) in
  let ret_blocks = List.filter does_bb_ret all_blocks in
  let rec merge_aux2 blocks res_blocks =
    match blocks with
    | [] -> res_blocks
    | hd_bb :: tl_bb_list ->
      let interleaved_bb_list =
	List.filter (fun bb -> succs_interleave bb.bb_succs hd_bb.bb_succs) tl_bb_list
      in
      if List.is_empty interleaved_bb_list then merge_aux2 tl_bb_list ([hd_bb] :: res_blocks)
      else
	let not_interleaved_bb_list =
	  List.filter (fun bb -> not (succs_interleave bb.bb_succs hd_bb.bb_succs)) tl_bb_list
	in
	merge_aux2 not_interleaved_bb_list ((hd_bb :: interleaved_bb_list) :: res_blocks)
  in
  let work_func bb_list =
    let fst = List.length bb_list in
    let succs_list = List.map (fun bb -> bb.bb_succs) bb_list in
    let all_succs = eliminate_dup (List.flatten succs_list) in
    let snd = List.length all_succs in
    fst, snd
  in
  let eqc = merge_aux2 ret_blocks [] in
  let data_set = List.map work_func eqc in
  let ibnum = num_of_mcfi_IB cr_lst in
  let icalltargetnum = 
    List.fold_right (fun bb ictnum -> 
      if does_bb_icall bb then ictnum + (List.length bb.bb_succs) else ictnum
    ) all_blocks 0
  in
  let rettargetnum = 
    List.fold_right (fun (ret_num, targets_num) rtnum ->
      ret_num * targets_num + rtnum
    ) data_set 0
  in
  let avg_ibt = ((float_of_int (icalltargetnum + rettargetnum)) /. (float_of_int ibnum)) in
  avg_ibt

let iCFG bbtbl fname =
  let iCFGFile = P.sprintf "%s.icfg" fname in
  let oc = open_out iCFGFile in
  let output_cflow oc bb =
    let succ_labels = 
      List.fold_right (fun succ succ_str ->
	let succ_out = 
	  if succ = "ffffffff" then "-1"
	  else
	    str_of_mword_flex (MWord.(-%) (MWord.of_string ("0x"^succ)) (MWord.of_string "0x8048000"))
	in
	succ_str ^ succ_out ^ " "
      ) bb.bb_succs ""
    in
    let _,_,len = List.last bb.bb_instrs in
    let ibsite = 
      bb.bb_relAddr + bb.bb_size - len
    in
    let ibranch_lbl = P.sprintf "%x" ibsite in
    let cf =
      P.sprintf "%s:%s" ibranch_lbl succ_labels
    in
    P.fprintf oc "%s\n" cf
  in
  H.iter (fun lbl bb ->
    if does_bb_ibranch bb then
      output_cflow oc bb
    else
      ()
  ) bbtbl;
  flush oc;
  close_out oc

let rec index_of_instr instrs off ndx =
  if off = 0 then ndx
  else if off > 0 then
    match instrs with
    | [] -> raise (Failure "No instruction left when off is non-zero")
    | h :: t ->
      let _,_,len = h in
      index_of_instr t (off-len) (ndx+1)
  else
    raise (Failure "Wrong offset into a basic block")

let locate_sensi_pnt bbtbl sensi_pnt =
  let target_lbl = ref "" in
  let index = ref 0 in
  H.iter (fun lbl bb ->
    let bbaddr = MWord.of_string ("0x"^lbl) in
    let sensiaddr = MWord.of_string ("0x"^sensi_pnt) in
    let off = MWord.to_int (MWord.(-%) sensiaddr bbaddr) in
    if off >= 0 && off < bb.bb_size then (
      target_lbl := lbl;
      index := index_of_instr bb.bb_instrs off 1;
    )
  ) bbtbl;
  !target_lbl,!index
