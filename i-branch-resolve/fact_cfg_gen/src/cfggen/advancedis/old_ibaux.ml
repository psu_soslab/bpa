(* Auxiliary functions for dealing indirect branches *)

open Batteries
open Bigarray

open IO

module P=Printf
module F=Format
open Formatutil
module E=Errormsg
module L=Logger
open Printf

open Util
open Config
open BB

open Bits
open X86Syntax
open Abbrev
open Instr
open Elf

open TyInfoRetrieval

(** used for stat in disassembler **)
let icallsite_num = ref 0
let tailcall_num = ref 0
let icall_afp_num = ref 0
let icall_vfp_num = ref 0
let tcall_afp_num = ref 0
let tcall_vfp_num = ref 0

let allfp_num = ref 0

let ic_total = ref 0

let ij_total = ref 0
let ij_plt = ref 0


type ijump_class =
| IJ_plt_t
| IJ_local_t
| IJ_function_t
| IJ_unclassified_t

let rec instrs_with_starts start instrs = match instrs with
  | [] -> []
  | (pre,ins,len) :: instrs' -> 
    (start,pre,ins,len) :: instrs_with_starts (start+len) instrs'

(* old utilities
type goto_label = {
  label_addr: int;
  label_lowpc: int;
  label_highpc: int;
}

type func_type_sig = {
  mutable func_name: string;
  mutable func_rel_lowpc: int;
  mutable func_ret: string;
  mutable func_param: string list;
}

type equivalent_class = {
  mutable eqc_ret_t: string;
  mutable eqc_param_num: int;
  mutable eqc_param_t: string list;
  mutable eqc_member: (int * string) list;
}

type t_func_type_sig = {
  mutable t_func_name: string;
  mutable t_func_rel_lowpc: int;
  mutable t_func_ret: c_type;
  mutable t_func_param: c_type list;
}

type t_equivalent_class = {
  mutable t_eqc_ret_t: c_type;
  mutable t_eqc_param_num: int;
  mutable t_eqc_param_t: c_type list;
  mutable t_eqc_member: (int * string) list;
}

type visisrc = 
| VS_param
| VS_global
| VS_local
| VS_unknown

type locexpr = {
  mutable le_atom: string;
  mutable le_opd1: string;
  mutable le_opd2: string;
  mutable le_opd3: string;
  mutable le_offset: string;
}

type locdesc = {
  mutable ld_lopc: int;
  mutable ld_hipc: int;
  mutable ld_cents: int;
  mutable ld_locexprs: string list;
}

type vfp = {
  mutable vfp_tsig: c_type;
  mutable vfp_visisrc: visisrc;
  mutable vfp_snum: int;
  mutable vfp_sinfo: locdesc list;
}

let mkdesc lopc hipc exprs =
  {
    ld_lopc = lopc;
    ld_hipc = hipc;
    ld_cents = List.length exprs;
    ld_locexprs = exprs;
  }

let mkvfp tsig vs sinfo =
  {
    vfp_tsig = tsig;
    vfp_visisrc = vs;
    vfp_snum = List.length sinfo;
    vfp_sinfo = sinfo;
  }
(** encoding functions in C and decoding functions in OCAML based on
    encoding-decoding rules:
    *************************
    Encoding: (in C)
    *************************
    Done!C_base str: 0(str);
    Done!C_const t: 1t;
    Done!C_array t dimlst: 2t(dim)(dimlst[0])...(dimlst[dim-1]);
    Done!C_pointer t: 3t;
    Done!C_typedef str t: 4(str)t;
    Done!C_subroutine t tlst: 5t(num)tlst[0]...tlst[num-1];
    Done!C_enumeration str etorlst: 6(str)(num)(etorlst[0].name)(etorlst[0].val)...(etorlst[num-1].name)(etorlst[num-1].val);
    Done!C_union str umemlst: 7(str)(num)umemlst[0].t(umemlst[0].field)...umemlst[num-1].t(umemlst[num-1].field);
    Done!C_structure str1 str2 smemlst: 8(str1)(num)(str2)smemlst[0].t(smemlst[0].field)...smemlst[num-1].t(smemlst[num-1].field);
    Done!C_unspecified_param: 9
    Done!C_wrong_type errinfo: a(errinfo)  
 **)

(** Choice 1: reading in one line of type encoding then decode **)

(** read_sb: string -> string * int; 
    in the output, 
    the first string is the content of the small box;
    the second string is the starting index of the remaining undecoded string;
**)
(* annotate a list of instrs with their (relative) start addresses *)
let rec instrs_with_starts start instrs = match instrs with
  | [] -> []
  | (pre,ins,len) :: instrs' -> 
    (start,pre,ins,len) :: instrs_with_starts (start+len) instrs'

let print_bb oc bb start =
  fprintf oc "{\n";
  fprintf oc "bb_label = %s\n" bb.bb_label;
  fprintf oc "bb_symbol_label = %s\n" bb.bb_symbol_label;
  fprintf oc "bb_instrs = \n";
  (*List.iter print_rtl_instr rb.rb_instrs;*)
  List.iter 
    (fun (loc, pre, ins, len) -> 
      fprintf oc " %s %d\t%s\t%s\n" (str_of_mword_flex (MWord.of_int loc)) len (str_of_prefix (pre,ins)) (str_of_instr (pre,ins));
      )  (instrs_with_starts (bb.bb_relAddr) bb.bb_instrs);	
  fprintf oc "bb_relAddr = %s\n" (str_of_mword_flex (MWord.of_int (bb.bb_relAddr)));
  fprintf oc "bb_succs = \n";
  List.iter (fun id -> fprintf oc "%s " id) bb.bb_succs;
  fprintf oc "\n";
  fprintf oc "bb_inter_succs = \n";
  List.iter (fun id -> fprintf oc "%s " id) bb.bb_inter_succs;
  fprintf oc "\n";
  fprintf oc "bb_preds = \n";
  List.iter (fun id -> fprintf oc "%s " id) bb.bb_preds;
  fprintf oc "\n";
  fprintf oc "}\n\n";
  flush oc

let dump_bbs oc bbs start =
  BBSet.iter (fun bb -> print_bb oc bb start) bbs

let dump_cr_list oc cr_list =
  List.iter (fun cr -> dump_bbs oc cr.bbs cr.startAddr) cr_list 

(** global flags that control the CFG precision level **)

let rel_on = ref true
let tmatch_on = ref true
let ijclass_on = ref true

let progress_logger = L.make_log "Progress"
let progress_event progress = L.log progress_logger L.INFO (fun _ -> (progress, []))
let progress_formatter log lvl event timestamp =
  let ename, elist = event in
  print_endline ename;
  flush stdout
  (*List.iter (fun (key,des) -> P.printf "%s %s\n" key des) elist *)
  
let general_logger = L.make_log "General"
let general_info_event event_name event_list = L.log general_logger L.INFO (fun _ -> event_name, event_list)
let general_debug_event event_name event_list = L.log general_logger L.DEBUG (fun _ -> event_name, event_list)
(*will use L.dbg_formatter as formatter*)

let simple_warn str =
  print_string str;
  flush stdout

let find_target_name fname =
  if String.exists fname "/" then 
    let _, target_name = String.rsplit fname "/" in
    target_name
  else
    fname

let decode_visisrc str =
  match str with
  | "ge" | "gs" -> VS_global
  | "p" -> VS_param
  | "l" -> VS_local
  | _ -> VS_unknown

exception WrongTypeEncoding

(** decode_c_type: string -> c_type * int **)
let decode_c_type str =
  let read_sb start =
    try
      let small_box_start = String.find_from str start "(" in
      let small_box_end = String.find_from str start ")" in
      let content_len = small_box_end - small_box_start - 1 in
      (String.sub str (small_box_start+1) content_len), (small_box_end+1)
    with Not_found ->
      let errmsg = P.sprintf "error happens at %d in str %s\n" start str
      in
      raise (Failure errmsg)
  in
  let rec form_dimlst dimension start =
    if dimension = 0 then [],start
    else
      let content, next_start = read_sb start in
      let subrange = 
	if content <> "NaN" then
	  String.to_int content 
	else
	  0
      in
      let subrange_list, next_start = form_dimlst (dimension-1) next_start in
      (subrange :: subrange_list), next_start 
  in
  let rec form_etorlst num start =
    if num = 0 then [],start
    else
      let etorname, next_start = read_sb start in
      let etorval_str, next_start = read_sb next_start in
      let etorval = String.to_int etorval_str in
      let etorlst, next_start = form_etorlst (num-1) next_start in
      ((etorname,etorval) :: etorlst), next_start
  in
  let rec form_typelst num start =
    if num = 0 then [],start
    else
      let param_type, next_start = decode_c_type_aux start in
      let param_typelst, next_start = form_typelst (num-1) next_start in
      (param_type :: param_typelst), next_start
  and form_umemlst num start =
    if num = 0 then [],start
    else
      let umemtype, next_start = decode_c_type_aux start in
      let umemname, next_start = read_sb next_start in
      let umemlst, next_start = form_umemlst (num-1) next_start in
      ((umemtype,umemname) :: umemlst), next_start
  and form_smemlst num start =
    if num = 0 then [],start
    else
      let smemtype, next_start = decode_c_type_aux start in
      let smemname, next_start = read_sb next_start in
      let smemlst, next_start = form_smemlst (num-1) next_start in
      ((smemtype,smemname) :: smemlst), next_start
  and decode_c_type_aux start =
      begin match String.get str start with
      | '0' -> 
	let next_start = start + 1 in
	let base_type_name, next_start = read_sb next_start in
	(C_base base_type_name), next_start
      | '1' ->
	let next_start = start + 1 in
	let ct, next_start = decode_c_type_aux next_start in
	(C_const ct), next_start
      | '2' ->
	let next_start = start + 1 in
	let ct, next_start = decode_c_type_aux next_start in
	let dim_str, next_start = read_sb next_start in
	let dim = String.to_int dim_str in
	let dimlst, next_start = form_dimlst dim next_start in
	(C_array (ct, dimlst)), next_start      
      | '3' ->
	let next_start = start + 1 in
	let ct, next_start = decode_c_type_aux next_start in
	(C_pointer ct), next_start
      | '4' ->
	let next_start = start + 1 in
	let typedef_name, next_start = read_sb next_start in
	let ct, next_start = decode_c_type_aux next_start in
	(C_typedef (typedef_name, ct)), next_start
      | '5' ->
	let next_start = start + 1 in
	let ret_type, next_start = decode_c_type_aux next_start in
	let num_str, next_start = read_sb next_start in
	let num = String.to_int num_str in
	let paramlst, next_start = form_typelst num next_start in
	(C_subroutine (ret_type, paramlst)), next_start
      | '6' ->
	let next_start = start + 1 in
	let enum_name, next_start = read_sb next_start in
	let etornum_str, next_start = read_sb next_start in
	let etornum = String.to_int etornum_str in
	let etorlst, next_start = form_etorlst etornum next_start in
	(C_enumeration (enum_name, etorlst)), next_start
      | '7' -> 
	let next_start = start + 1 in
	let union_name, next_start = read_sb next_start in
	let umemnum_str, next_start = read_sb next_start in
	let umemnum = String.to_int umemnum_str in
	let umemlst, next_start = form_umemlst umemnum next_start in
	(C_union (union_name, umemlst)), next_start
      | '8' ->
	let next_start = start + 1 in
	let original_name, next_start = read_sb next_start in
	let smemnum_str, next_start = read_sb next_start in
	let smemnum = String.to_int smemnum_str in
	(*let level_name, next_start = read_sb next_start in*)
	let smemlst, next_start = form_smemlst smemnum next_start in
	(C_structure (original_name, "", smemlst)), next_start
      | '9' ->
	C_unspecified_param, (start+1)
      | 'a' -> 
	let next_start = start + 1 in
	let errinfo, next_start = read_sb next_start in
	let () = P.printf "Wrong c type: %s\n" errinfo in
	(C_wrong_type errinfo), next_start
      | 'b' ->
	let next_start = start + 1 in
	let ct, next_start = decode_c_type_aux next_start in
	(C_volatile ct), next_start
      | _ -> 
	(*let () = P.printf "Unrecognized type initials %s\n" str in*)
	raise WrongTypeEncoding
      end
	(*
    with _ ->
      raise (Failure "Not enough");
      (*(C_wrong_type "Unrecognized type initials!"), start*)
	*)
  in
  let c_type, next_start = decode_c_type_aux 0 in
  if next_start = String.length str then c_type
  else
    let () = P.printf "Type decoding is not finished but returns before %d\nin type encoding %s\n" next_start str in
    C_wrong_type (P.sprintf "Type decoding is not finished but returns before %d" next_start)

(*
let mkEQC func_ret func_param (func_rel_lowpc, func_name) =
  {eqc_ret_t=func_ret;eqc_param_num=List.length func_param;
   eqc_param_t=func_param;eqc_member=[func_rel_lowpc,func_name]}
*)
(* converting from an absolute address to a relative address 
   relative to start *)
let get_relative_addr (addr:mword) (start:mword) : int = 
  MWord.to_int (MWord.(-%) addr start)

let file_read_region (fname:string) (offset:int) (size:int) =
  (* alternative: use memory mapped files *)
  (* let fdesc = Unix.openfile fname [Unix.O_RDONLY] 0 in *)
  (* let prog = Array1.map_file fdesc ?pos:(Some (Int64.of_int32 shdr.sh_offset)) *)
  (*   int8_unsigned c_layout false (Int32.to_int shdr.sh_size) in *)
  (* Unix.close fdesc; *)
  let fch = Pervasives.open_in fname in
  seek_in fch offset;
  let ip = input_channel fch in
  (* limitation: only handle files whose size is less than max_int *)
  let prog = Array1.create int8_unsigned c_layout size in
  Array1.modify (fun _ -> (IO.read_byte ip)) prog;
  Pervasives.close_in fch;
  prog

let form_abs_addr (data:codeBytes) (loc:int) =
  let a = str_of_mword_one_byte (MWord.of_int data.{loc}) in
  let b = str_of_mword_one_byte (MWord.of_int data.{loc+1}) in
  let c = str_of_mword_one_byte (MWord.of_int data.{loc+2}) in
  let d = str_of_mword_flex (MWord.of_int data.{loc+3}) in
  (*let () = P.printf "Before of_string: 0x%s%s%s%s\n" d c b a in*)
  MWord.of_string (P.sprintf "0x%s%s%s%s" d c b a) 

let rec range_n_step num start stepsize =
  if num <= 0 then [] 
  else if num = 1 then [start]
  else start :: (range_n_step (num-1) (start+stepsize) stepsize)

let rec form_lib_func_lst plt_shdr rel_plt_shdr num =
  let plt_start = MWord.to_int plt_shdr.sh_addr in
  let n = MWord.to_int num in
  let lowpc_list = range_n_step n (16+plt_start) 16 in
  lowpc_list
    

let rec eliminate_dup_a' list =
  match list with
  | [] -> []
  | h :: t ->
    if List.mem h t then eliminate_dup_a' t
    else h :: (eliminate_dup_a' t)

let rec eliminate_duplication bs_lst =
  match bs_lst with
  | [] -> []
  | h :: [] -> bs_lst
  | (h_loc,h_lbl) :: t -> 
    if List.mem h_loc (List.map (fun (l,ll) -> l) t) then eliminate_duplication t else (h_loc,h_lbl) :: (eliminate_duplication t)

let collect_dynsym_functions (elf:elf_file) (start:mword) =
  (* collect all function names and order in .dynsym *)
  let process_symtab_entry se =
    match se.st_type with
    | STT_FUNC -> Some (se.st_value, se.st_name)
    | _ ->
      if se.st_name = "__gmon_start__" then
	Some (se.st_value, se.st_name)
      else None
  in
  let process_dynsym st =
    if st.symtab_name = ".dynsym" then 
      List.filter_map process_symtab_entry st.symtab_entries
    else []
  in 
  let func_lowpc_names = List.concat (List.map process_dynsym elf.sym_tabs) in
  let ret = List.map (fun (loc,lbl) -> (get_relative_addr loc start,lbl)) func_lowpc_names in
  ret

let collect_library_functions (elf:elf_file) (start:mword) =
  let form_symndx_lst bytes num =
    let rec aux bytes index =
      if index < num then
	let symndx_index = index * 8 + 5 in
	let a = str_of_mword_one_byte (MWord.of_int bytes.{symndx_index}) in
	let b = str_of_mword_one_byte (MWord.of_int bytes.{symndx_index+1}) in
	let c = str_of_mword_one_byte (MWord.of_int bytes.{symndx_index+2}) in
	let symndx = MWord.to_int (MWord.of_string (P.sprintf "0x%s%s%s" c b a)) in
	symndx :: (aux bytes (index+1))
      else []
    in
    aux bytes 0
  in
  let is_dynsym st = 
    st.symtab_name = ".dynsym" 
  in 
  (* Use relocation table to find the addresses and names for library functions *)
  let rel_plt_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".rel.plt") elf.shdr_tab) in
  let relplt_bytes = file_read_region elf.fname (MWord.to_int rel_plt_shdr.sh_offset) (MWord.to_int rel_plt_shdr.sh_size) in
  let num = MWord.(/%) rel_plt_shdr.sh_size (MWord.of_int 8) in
  let plt_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".plt") elf.shdr_tab) in
  let lib_func_lst = form_lib_func_lst plt_shdr rel_plt_shdr num in
  let abs_target_lst = List.map (MWord.of_int) lib_func_lst in
  let rel_target_lst = List.map (fun addr -> get_relative_addr addr start) abs_target_lst in
  let symndx_lst = form_symndx_lst relplt_bytes (MWord.to_int num) in
  let rel_addr_with_symndx_lst =
    try
      let lib_func_with_name = List.combine rel_target_lst symndx_lst in
      lib_func_with_name
    with Invalid_argument _ ->
      let () = P.printf "Different length while combining in collect_library_functions!" in
      let lib_func_with_name = List.map (fun loc -> (loc, (-1))) rel_target_lst in
      lib_func_with_name
  in
  let plt_start = plt_shdr.sh_addr in
  let plt_rel_start = get_relative_addr plt_start start in
  let plt_1st_func_name = "__plt_dispatcher" in
  let dynsym = List.find is_dynsym elf.sym_tabs in
  (plt_rel_start, plt_1st_func_name) :: 
    List.map (fun (rel_addr, symndx) ->
      let entry = List.nth dynsym.symtab_entries symndx in
      let funcname = entry.st_name in
      (rel_addr,funcname)
    ) rel_addr_with_symndx_lst

  (*let () = List.iter (fun addr -> P.printf "library function address: %s\n" (str_of_mword_flex (MWord.of_int addr))) rel_target_lst in*)

let collect_symtab_functions (elf:elf_file) (start:mword) = 
  (* collect all function starts in symbol tables *)
  let process_symtab_entry se = 
    match se.st_type, se.st_shndx with
    | STT_FUNC, SHN_REGULAR _ -> (* NOTE: no .dynsym entry can be matched here *)
      Some (se.st_value, se.st_name) 
    | _ -> None 
  in
  let process_symtab st =
    List.filter_map process_symtab_entry st.symtab_entries 
  in 
  let func_starts = 
    List.concat (List.map process_symtab elf.sym_tabs) in
  let abs_addr_list = 
    (elf.ehdr.e_entry, "_start") :: func_starts in
  let ret = List.map (fun (abs,lbl) -> (get_relative_addr abs start,lbl)) abs_addr_list in
  eliminate_duplication ret

let collect_all_functions elf start =
  let func_in_symtab = collect_symtab_functions elf start in
  let lib_func = collect_library_functions elf start in
  func_in_symtab @ lib_func

let collect_indirect_targets elf all_functions =
  let datafile = 
    P.sprintf "%s.its" elf.fname
  in
  let ic = open_in datafile in
  let icTargets = ref [] in
  let ijTargets = ref [] in
  let () = 
    try
      while true do
	let instring = input_line ic in
	let funcName, offset = String.split instring " 0x" in
	if offset = "0" then
	  icTargets := funcName :: !icTargets
	else if offset <> "0" then
	  try
	    let funclopc,_ = 
	      List.find (fun (loc,lbl) -> 
		lbl = funcName 
	      ) all_functions in
	    ijTargets := (funclopc + (MWord.to_int (MWord.of_string ("0x" ^ offset)))) :: !ijTargets
	  with Not_found -> ()
	    (*
	    begin 
	      try
		let funclopc,_ = 
		  List.find (fun (loc,lbl) -> 
		    String.starts_with lbl funcName 
		  ) all_functions in
		ijTargets := (funclopc + (MWord.to_int (MWord.of_string ("0x" ^ offset)))) :: !ijTargets
	      with Not_found ->
		()
	    end
	    *)
      done
    with End_of_file ->
      close_in ic
  in
  !icTargets, !ijTargets
  
(*
let collect_possible_entries elf =
  (*let elf_name = find_target_name elf.fname in*)
  let datafile = 
    P.sprintf "%s_posent.txt" elf.fname
  in
  (* open goto_labels.txt *)
  let ic = open_in datafile in
  let targets = ref [] in
  let () = 
    try 
      while true do
	let posent = input_line ic in
	targets := posent :: !targets
      done
    with End_of_file ->
      close_in ic
  in
  !targets

let collect_static_funcs elf =
  (*let elf_name = find_target_name elf.fname in*)
  let datafile = 
    P.sprintf "%s_static.txt" elf.fname
  in
  (* open goto_labels.txt *)
  let ic = open_in datafile in
  let targets = ref [] in
  let () = 
    try 
      while true do
	let posent = input_line ic in
	targets := posent :: !targets
      done
    with End_of_file ->
      close_in ic
  in
  !targets

let select_taken_funcs posents all_funcs =
  List.filter (fun func -> List.mem (snd func) posents) all_funcs
*)
let collect_func_ranges_from_symtab elf start =
  let process_symtab_entry se = 
    match se.st_type, se.st_shndx with
    | STT_FUNC, SHN_REGULAR _ -> Some (get_relative_addr se.st_value start, get_relative_addr (MWord.(+%) se.st_value se.st_size) start)
    | _ -> None
  in
  let process_symtab st =
    List.filter_map process_symtab_entry st.symtab_entries 
  in
  let func_ranges =
    List.concat (List.map process_symtab elf.sym_tabs) 
  in  
  (*let () = List.iter (fun fs,fe -> P.printf "Function starts at %s; ends at %s\n" (str_of_mword_flex (MWord.of_int fs)) (str_of_mword_flex (MWord.of_int fe))) func_ranges
  in*)
  func_ranges

let decide_compiler_and_olevel elf =
    let elf_name = find_target_name elf.fname in
    let datafile = 
      P.sprintf "%s_compiler_info.txt" elf_name
    in
    (* open tail_call.txt *)
    let ic = open_in datafile in 
    let compiler_info = input_line ic in
    let () = close_in ic in
    let compiler = 
    if String.exists compiler_info "GNU C " then
      "gcc"
    else if String.exists compiler_info "clang" then
      "clang"
    else if String.exists compiler_info "GNU C++ " then
      "g++"
    else 
      "Unsupported"
    in
    let olevel = 
      if String.exists compiler_info "-O3" then 3
      else if String.exists compiler_info "-O2" then 2
      else if String.exists compiler_info "-O1" then 1
      else if String.exists compiler_info "-O0" then 0
      else 0
    in
    compiler, olevel

let collect_tail_call_site elf start =
    let elf_name = find_target_name elf.fname in
    let datafile = 
      P.sprintf "%s_tcs.txt" elf_name
    in
    (* open tail_call.txt *)
    let ic = open_in datafile in
    let targets = ref [] in
    let () = 
      try
	while true do
	  let tail_call_site = input_line ic in
	  (*let () = P.printf "function_label: %s\n\n" function_label in*)
	  let rel_Addr = get_relative_addr (MWord.of_string tail_call_site) start in
	  targets := rel_Addr :: !targets 
	done
      with End_of_file ->
	(*P.printf "End of File\n\n";*)
	close_in ic
    in
    !targets

let collect_labels elf start = 
  let elf_name = find_target_name elf.fname in
  let datafile = 
    P.sprintf "%s_lbl.txt" elf_name
  in
  (* open goto_labels.txt *)
  let ic = open_in datafile in
  let targets = ref [] in
  let () = 
    try
      while true do
	let label_addr_str = input_line ic in
	let label_lowpc_str = input_line ic in
	let label_highpc_str = input_line ic in
	(*let _ = input_line ic in*)
	(*let () = P.printf "function_label: %s\n\n" function_label in*)
	if (MWord.of_string label_addr_str) != (MWord.of_int 0) &&  
	  (MWord.of_string label_lowpc_str) != (MWord.of_int (-1)) && 
	  (MWord.of_string label_highpc_str) != (MWord.of_int (-1))  
	then
	  let addr = get_relative_addr (MWord.of_string label_addr_str) start in
	  let lowpc = get_relative_addr (MWord.of_string label_lowpc_str) start in
	  let highpc = get_relative_addr (MWord.of_string label_highpc_str) start in
	  let label = {label_addr = addr; label_lowpc = lowpc; label_highpc = highpc} in
	  targets := label :: !targets 
	else ()
      done
    with End_of_file ->
	(*P.printf "End of File\n\n";*)
      close_in ic
  in
  !targets

let collect_wanted_label_addr elf bb start label_list = 
  let (pre,ins,len) = List.last bb.bb_instrs in
  let indirect_jump_site = bb.bb_relAddr+bb.bb_size-len in
  (*let indirect_jump_site_str = str_of_mword (MWord.of_int indirect_jump_site) in*)
  let wanted_labels = List.filter (fun lbl -> (indirect_jump_site >= lbl.label_lowpc) && (indirect_jump_site <= lbl.label_highpc)) label_list in
  let ret = List.map (fun lbl -> lbl.label_addr) wanted_labels in
  ret

let collect_funcsig_list elf start =
    (*let elf_name = find_target_name elf.fname in*)
    let resultdir = P.sprintf "%s_type" elf.fname in
    let datafile = 
      P.sprintf "%s/fsig.txt" resultdir
    in
    let cmd = 
      P.sprintf "./dbg/collect_func_type_sig %s > %s" elf.fname datafile in
    let _ = if not (Sys.file_exists datafile) then Sys.command cmd else -1 in
    (* open func_type_sig.txt *)
    let ic = open_in datafile in
    let funcsig = {func_name="";func_rel_lowpc=0;func_ret="";func_param=[]} in
    let funcsig_list = ref [] in
    let () = 
      try
	while true do
	  let indicator = input_line ic in
	  match indicator with
	  | "Function_name:" -> 
	    let content = input_line ic in
	    let func_name = content in
	    funcsig.func_name <- func_name
	  | "Function_no_name:" -> 
	    let _ = input_line ic in
	    let func_name = "" in
	    funcsig.func_name <- func_name
	  | "Function_lowpc:" ->
	    let content = input_line ic in
	    let low_pc = content in
	    funcsig.func_rel_lowpc <- get_relative_addr (MWord.of_string low_pc) start
	  | "Return type:" ->
	    let content = input_line ic in
	    let ret_t = content in
	    funcsig.func_ret <- ret_t
	  | "Parameter type:" ->
	    let content = input_line ic in
	    let par_t = content in
	    funcsig.func_param <- funcsig.func_param @ [par_t]
	  | "Unspecified parameters" ->
	    let par_t = "unspecified" in
	    funcsig.func_param <- funcsig.func_param @ [par_t]
	  | "Param_end" ->
	    if funcsig.func_name = "" || funcsig.func_rel_lowpc = get_relative_addr (MWord.of_int (-1)) start then (
	      funcsig.func_name <- "";
	      funcsig.func_ret <- "";
	      funcsig.func_param <- []
	    )
	    else (
	      let newfuncsig = {func_name = funcsig.func_name;
				func_rel_lowpc = funcsig.func_rel_lowpc;
				func_ret = funcsig.func_ret;
				func_param = funcsig.func_param} in
	      funcsig_list := newfuncsig :: !funcsig_list;
	      funcsig.func_name <- "";
	      funcsig.func_ret <- "";
	      funcsig.func_param <- []
	    )
	  | _ ->
	    ()
	done
      with End_of_file ->
	(*P.printf "End of File\n\n";*)
	close_in ic
    in
    !funcsig_list

let print_funcsig funcsig =
  P.printf "{\nfunc_name:\n %s\nfunc_rel_num:\n %s\nfunc_ret:%s\n" funcsig.func_name (str_of_mword_flex (MWord.of_int funcsig.func_rel_lowpc)) funcsig.func_ret;
  List.iter (fun str -> P.printf "func_parameter:%s\n" str) funcsig.func_param;
  P.printf "}\n"

let dump_funcsig_list funcsig_list =
  List.iter print_funcsig funcsig_list

let rec str_of_c_type ctype =
  match ctype with
  | C_base name -> name 
  | C_const ct -> P.sprintf "const %s" (str_of_c_type ct)
  | C_array (ct,dimlst) -> 
    let dimension_str = String.concat "" (List.map (fun dim -> P.sprintf "[%d]" dim) dimlst) in
    P.sprintf "(%s%s)" (str_of_c_type ct) dimension_str
  | C_pointer ct -> P.sprintf "%s *" (str_of_c_type ct)
  | C_typedef (typedef_name, ct) ->
    P.sprintf "typedef(%s:%s)" typedef_name (str_of_c_type ct)
  | C_subroutine (ret_ct, param_ct_list) ->
    let ret_ct_str = str_of_c_type ret_ct in
    let param_ct_str = String.concat "," (List.map str_of_c_type param_ct_list) in
    P.sprintf "(%s)(%s)" ret_ct_str param_ct_str 
  | C_enumeration (enumname, etor_list) -> 
    let etor_list_str = String.concat ";" (List.map (fun (etorname,etorval) -> P.sprintf "%s(%d)" etorname etorval) etor_list) in
    P.sprintf "ENUM %s{%s}" enumname etor_list_str
  | C_union (unionname, umem_list) -> 
    let umem_list_str = String.concat ";" (List.map (fun (umemtype,umemname) -> P.sprintf "%s %s" (str_of_c_type umemtype) umemname) umem_list) in
    P.sprintf "UNION %s{%s}" unionname umem_list_str
  | C_structure (original_name, level_name, smem_list) ->
    let smem_list_str = String.concat ";" (List.map (fun (smemtype,smemname) -> P.sprintf "%s %s" (str_of_c_type smemtype) smemname) smem_list) in
    P.sprintf "STRUCT %s(%s){%s}" original_name level_name smem_list_str
  | C_unspecified_param ->
    "..."    
  | C_wrong_type errmsg ->
    let () = P.printf "Wrong type: %s" errmsg in
    P.sprintf "wrong_type(%s)" errmsg   
  | C_volatile ct ->
    P.sprintf "volatile %s" (str_of_c_type ct)

let print_teqc teqc =
  P.printf "{\nret_t:\n %s\nparam_num:\n %d\nparam_t:\n" (str_of_c_type teqc.t_eqc_ret_t) teqc.t_eqc_param_num;
  List.iter (fun ctype -> P.printf "%s\n" (str_of_c_type ctype)) teqc.t_eqc_param_t;
  P.printf "\n";
  List.iter (fun (i,str) -> P.printf "member_name:%s, member_lowpc:%s\n" str (str_of_mword_flex (MWord.of_int i))) teqc.t_eqc_member;
  P.printf "}\n"
    
let dump_teqcs teqcs =
  List.iter print_teqc teqcs

let rec compare_type type1 type2 = 
  type1 = type2
  
let mkTEQC t_funcsig =
  {t_eqc_ret_t = t_funcsig.t_func_ret;
   t_eqc_param_num = List.length t_funcsig.t_func_param;
   t_eqc_param_t = t_funcsig.t_func_param;
   t_eqc_member = [(t_funcsig.t_func_rel_lowpc, t_funcsig.t_func_name)]}

let mkTEQC_or_addMem t_funcsig_list =
  let compare_type_sig t_funcsig teqc =
    let i_compare_type type1 type2 b =
      (compare_type type1 type2) && b
    in
    (compare_type teqc.t_eqc_ret_t t_funcsig.t_func_ret) 
    &&
      if List.length teqc.t_eqc_param_t = List.length t_funcsig.t_func_param 
      then (List.fold_right2 i_compare_type teqc.t_eqc_param_t t_funcsig.t_func_param true)
      else false
  in
  let rec aux t_eqc_list t_funcsig_list =
    match t_funcsig_list with
    | [] -> t_eqc_list
    | hd_t_funcsig :: tl_t_funcsig ->
      try 
	let teqc = List.find (compare_type_sig hd_t_funcsig) t_eqc_list in
	if List.mem (hd_t_funcsig.t_func_rel_lowpc, hd_t_funcsig.t_func_name) teqc.t_eqc_member 
	then aux t_eqc_list tl_t_funcsig
	else 
	  let () = teqc.t_eqc_member <- 
	    (hd_t_funcsig.t_func_rel_lowpc, hd_t_funcsig.t_func_name) :: teqc.t_eqc_member 
	  in aux t_eqc_list tl_t_funcsig
      with Not_found ->
	let new_teqcs = (mkTEQC hd_t_funcsig) :: t_eqc_list in
	aux new_teqcs tl_t_funcsig
  in
  aux [] t_funcsig_list
  
  
let mkTEQCs funcsig_list =
  let t_funcsig_list = List.map 
    (fun funcsig -> 
      {t_func_name=funcsig.func_name;
       t_func_rel_lowpc=funcsig.func_rel_lowpc;
       t_func_ret=decode_c_type funcsig.func_ret;
       t_func_param=List.map decode_c_type funcsig.func_param}) funcsig_list 
  in
  let teqcs = mkTEQC_or_addMem t_funcsig_list in
  (*let () = dump_teqcs teqcs in*)
  teqcs

let find_lowpc_in_func_list func_name funcl =
  try 
    let lowpc, _  = List.find (fun (loc,name) -> name = func_name) funcl in
    lowpc
  with Not_found ->
    (-1)

let configure_funcsig_list_lowpc funcsiglr funcl =
  let tmp = 
    List.map (fun funcsig -> {func_name = funcsig.func_name;
			      func_rel_lowpc = find_lowpc_in_func_list funcsig.func_name funcl;
			      func_ret = funcsig.func_ret;
			      func_param = funcsig.func_param;}) funcsiglr
  in
  List.filter (fun funcsig -> funcsig.func_rel_lowpc <> (-1)) tmp


let collect_teqcs elf func_list start fsig =
  let funcsig_list_raw = fsig in
  let funcsig_list = configure_funcsig_list_lowpc funcsig_list_raw func_list in
  let teqcs = mkTEQCs funcsig_list in
  teqcs

let in_teqcs func teqcs =
  List.fold_right (fun teqc b -> b || (List.mem func teqc.t_eqc_member) ) teqcs false

let collect_func_name_range_from_symtab elf start =
  let process_symtab_entry se = 
    match se.st_type, se.st_shndx with
    | STT_FUNC, SHN_REGULAR _ -> Some (se.st_name, get_relative_addr se.st_value start, get_relative_addr (MWord.(+%) se.st_value se.st_size) start)
    | _ -> None
  in
  let process_symtab st =
    List.filter_map process_symtab_entry st.symtab_entries 
  in
  let func_ranges =
    List.concat (List.map process_symtab elf.sym_tabs) 
  in  
  (*let () = List.iter (fun (name,fs,fe) -> P.printf "Function %s starts at %s; ends at %s\n" name (str_of_mword_flex (MWord.of_int fs)) (str_of_mword_flex (MWord.of_int fe))) func_ranges
  in*)
  func_ranges

let func_bb_in elf bb start =
  let find_min_offset func_name_off_list =
    let rec aux lst =
      match lst with
      | [] -> ("",-1)
      | (hname,hoff) :: [] -> (hname, hoff)
      | (hname,hoff) :: t -> 
	let min_name,min_off = aux t in
	if hoff <= min_off then (hname,hoff)
	else (min_name,min_off)
    in
    let ret_name, ret_off = aux func_name_off_list in
    ret_name
  in
  let (pre,ins,len) = List.last bb.bb_instrs in
  let call_site = bb.bb_relAddr + bb.bb_size - len in
  let func_name_range = collect_func_name_range_from_symtab elf start in
  try
    let target_name, _, _ = List.find (fun (name,lp,hp) -> call_site > lp && call_site < hp) func_name_range in
    target_name
  with Not_found ->
    let modified_func_name_range = List.map (fun (name,lp,hp) -> (name, call_site - lp)) func_name_range in
    let func_name_off_list = List.filter (fun (name, off) -> off >= 0) modified_func_name_range in
    find_min_offset func_name_off_list

let rec search_for_subroutines_t ctype curr_fp_list =   
  match ctype with
  | C_const ct -> search_for_subroutines_t ct curr_fp_list 
  | C_array (ct,_) -> search_for_subroutines_t ct curr_fp_list 
  | C_pointer ct -> search_for_subroutines_t ct curr_fp_list 
  | C_typedef (typedef_name, ct) -> search_for_subroutines_t ct curr_fp_list 
  | C_subroutine (ret,param_list) -> 
    List.fold_right search_for_subroutines_t (ret :: param_list) (ctype :: curr_fp_list)
  | C_union (_,umem_list) -> 
    List.fold_right search_for_subroutines_t (List.map (fun (ct,str) -> ct) umem_list) curr_fp_list
  | C_structure (_,_,smem_list) ->
    List.fold_right search_for_subroutines_t (List.map (fun (ct,str) -> ct) smem_list) curr_fp_list
  | _ -> curr_fp_list

let str_of_vs vs =
  match vs with
  | VS_param -> "param"
  | VS_global -> "global"
  | VS_local -> "local"
  | VS_unknown -> "unknown"

let print_locdesc ld =
  printf "ld_lopc: %d\n" ld.ld_lopc;
  printf "ld_hipc: %d\n" ld.ld_hipc;
  printf "ld_cents: %d\n" ld.ld_cents;
  printf "ld_locexprs:\n";
  List.iter print_endline ld.ld_locexprs

let print_vfp vfp =
  print_endline "{";
  printf "vfp type signature: %s\n" (str_of_c_type vfp.vfp_tsig);
  printf "vfp visibility source: %s\n" (str_of_vs vfp.vfp_visisrc);
  printf "vfp storage description number: %d\n" vfp.vfp_snum;
  printf "vfp storage information:\n";
  List.iter print_locdesc vfp.vfp_sinfo;
  print_endline "}"

let collect_visible_static_funcs elf bb start =
  let elf_name = find_target_name elf.fname in
  let target_name = func_bb_in elf bb start in
  let resultfile = 
    P.sprintf "./vstc_%s_%s.txt" elf_name target_name
  in
  if Sys.file_exists elf.fname then 
    let cmd = 
      P.sprintf "./dbg/collect_visible_static_funcs %s %s > %s" elf.fname target_name resultfile
    in
    let _ = if not (Sys.file_exists resultfile) then Sys.command cmd else 0 in
    let ic = open_in resultfile in
    let targets = ref [] in
    let () = 
      try 
	while true do
	  let posent = input_line ic in
	  targets := posent :: !targets
	done
      with End_of_file ->
	close_in ic
    in
    !targets
  else 
    let () = P.printf "No file %s exists!" resultfile in 
    []

let collect_vfp_with_sinfo elf bb start : vfp list =
  let elf_name = find_target_name elf.fname in
  let target_name = func_bb_in elf bb start in
  let resultfilename = P.sprintf "./vfp_sinfo_%s_%s.txt" elf_name target_name in
  let void = C_base "void" in
  let size_t = C_typedef ("size_t", C_base "unsinged int") in
  let ptr ct = C_pointer ct in
  let fp ret paramlst= C_subroutine (ret, paramlst) in
  if target_name = "deregister_tm_clones" then
    [mkvfp (fp void [ptr void]) VS_unknown []]
  else if target_name = "register_tm_clones" then
    [mkvfp (fp void [ptr void;size_t ]) VS_unknown []]
  else if target_name = "frame_dummy" then
    let fstvfp = mkvfp (fp void [void]) VS_unknown [] in
    let sndvfp = mkvfp (fp void [ptr void]) VS_unknown [] in
    [fstvfp; sndvfp]
  else if Sys.file_exists elf.fname then (
    let cmd = 
      P.sprintf "./dbg/collect_vfp_with_sinfo %s %s > %s" elf.fname target_name resultfilename 
    in
    let _ = if not (Sys.file_exists resultfilename) then Sys.command cmd else 0 in
    let ic = open_in resultfilename in
    let vfps = ref [] in
    let sinfo_count = ref 0 in
    let () = 
      try
	while true do 
	  sinfo_count := !sinfo_count + 1;
	  let visisrc_e = input_line ic in
	  let vs = decode_visisrc visisrc_e in
	  let indicator = input_line ic in
	  let ctype = 
	    try decode_c_type indicator 
	    with WrongTypeEncoding ->
	      let () = 
		P.printf "Error! Now in file %s, decoding number %d sinfo entry!\n" resultfilename !sinfo_count; 
		flush stdout
	      in 
	      C_wrong_type "debugging"
	  in
	  let sinfo_start = input_line ic in
	  if sinfo_start <> "{" then (
	    print_endline "Error: unknown encoding after type encoding\n"; 
	    P.printf "in file %s at entry %d\n" resultfilename !sinfo_count;
	    flush stdout; 
	    exit 1)
	  else
	    let first_line = ref (input_line ic) in
	    let locdesc_list = ref [] in
	    while !first_line <> "}" do
	      if !first_line = "NULL" then (
		let vfp_entry = mkvfp ctype vs [] in
		vfps := !vfps @ [vfp_entry];
		first_line := input_line ic
	      )
	      else (
		let lopc_e = !first_line in
		let hipc_e = input_line ic in
		let lopc = MWord.of_string lopc_e in
		let hipc = MWord.of_string hipc_e in
		let lopc_rel =
		  if MWord.(>=%) lopc start then get_relative_addr lopc start 
		  else MWord.to_int lopc
		in
		let hipc_rel =
		  if MWord.(>=%) hipc start then get_relative_addr hipc start 
		  else MWord.to_int hipc
		in
		(* 
		   let le_start = input_line ic in
		   if le_start <> "Le:" then ( simple_warn "Error: unknown things after location expression start string, le"; exit 1 )
		 else
	      *)
		let locexpr_list = ref [] in
		let curr_expr = ref (input_line ic) in
		while !curr_expr <> "" do
		  locexpr_list := !locexpr_list @ [!curr_expr];
		  curr_expr := input_line ic
		done;
		let locdesc_entry = mkdesc lopc_rel hipc_rel !locexpr_list in
		locdesc_list := !locdesc_list @ [locdesc_entry];
		first_line := input_line ic
	      )
	    done;
	    let vfp_entry = mkvfp ctype vs !locdesc_list in
	    vfps := !vfps @ [vfp_entry]
	done
      with End_of_file ->
	close_in ic
    in
    (*let () = List.iter print_vfp !vfps in*)
    !vfps
  )
  else
    let () = P.printf "No file %s exists!" resultfilename in
    []

let collect_tsig_from_vfps vfps =
  let subroutine_tsig_list_list = List.map (fun vfp -> search_for_subroutines_t vfp.vfp_tsig []) vfps in
  let subroutine_tsig_list = List.flatten subroutine_tsig_list_list in
  eliminate_dup_a' subroutine_tsig_list

let is_vfp_static_local vfp =
  let local_b = (vfp.vfp_visisrc = VS_local) in
  let locdesc_list = vfp.vfp_sinfo in
  let locexpr_list_list = List.map (fun locdesc -> locdesc.ld_locexprs) locdesc_list in
  let locexpr_list = List.flatten locexpr_list_list in
  let static_b = 
    List.fold_right (fun expr b -> b || (String.exists expr "addr" && (not (String.exists expr "stack_value")))) locexpr_list false
  in
  local_b && static_b

let stored_in_addr vfp addr_big_int branch_site =
  let locdesc_list = List.filter (fun locdesc -> branch_site >= locdesc.ld_lopc && (locdesc.ld_hipc = (-1) || branch_site < locdesc.ld_hipc)) vfp.vfp_sinfo in
  let locexpr_list_list = List.map (fun locdesc -> locdesc.ld_locexprs) locdesc_list in
  let addr_mword = MWord.of_big_int addr_big_int in
  let dw_addr_str = "DW_OP_addr 0x" ^ (str_of_mword addr_mword) in
  List.fold_right (fun expr_list b -> b || (not (List.mem "DW_OP_stack_value" expr_list)) && (List.mem dw_addr_str expr_list)) locexpr_list_list false

let dw_encoding_of_reg reg =
  match reg with
  | EAX -> "reg0"
  | ECX -> "reg1"
  | EDX -> "reg2"
  | EBX -> "reg3"
  | ESP -> "reg4"
  | EBP -> "reg5"
  | ESI -> "reg6"
  | EDI -> "reg7"

let dw_encoding_of_breg reg =
  match reg with
  | EAX -> "breg0"
  | ECX -> "breg1"
  | EDX -> "breg2"
  | EBX -> "breg3"
  | ESP -> "breg4"
  | EBP -> "breg5"
  | ESI -> "breg6"
  | EDI -> "breg7"

let stored_in_reg vfp reg branch_site =
  let reg_str = dw_encoding_of_reg reg in
  let dw_reg_str = "DW_OP_" ^ reg_str in
  let locdesc_list = List.filter (fun locdesc -> branch_site >= locdesc.ld_lopc && (locdesc.ld_hipc = (-1) || branch_site < locdesc.ld_hipc)) vfp.vfp_sinfo in
  let locexpr_list_list = List.map (fun locdesc -> locdesc.ld_locexprs) locdesc_list in
  List.fold_right (fun expr_list b -> b || (not (List.mem "DW_OP_stack_value" expr_list)) && (List.mem dw_reg_str expr_list)) locexpr_list_list false

let stored_in_breg vfp reg offset branch_site =
  let dw_breg_str = "DW_OP_" ^ (dw_encoding_of_breg reg) in
  let offset_str = Big.to_string offset in
  let target_str = 
    if Big.gt offset Big.zero then dw_breg_str ^ " +" ^ offset_str
    else if Big.lt offset Big.zero then dw_breg_str ^ " -" ^ offset_str
    else dw_breg_str ^ " " ^ offset_str
  in
  let locdesc_list = List.filter (fun locdesc -> branch_site >= locdesc.ld_lopc && (locdesc.ld_hipc = (-1) || branch_site < locdesc.ld_hipc)) vfp.vfp_sinfo in
  let locexpr_list_list = List.map (fun locdesc -> locdesc.ld_locexprs) locdesc_list in
  List.fold_right (fun expr_list b -> b || (not (List.mem "DW_OP_stack_value" expr_list)) && (List.mem target_str expr_list)) locexpr_list_list false

let collect_visible_function_pointers_t elf prog bb start : c_type list =
  let elf_name = find_target_name elf.fname in
  let target_name = func_bb_in elf bb start in
  let resultfilename = P.sprintf "./vfp_%s_%s.txt" elf_name target_name in
  let void = C_base "void" in
  let size_t = C_typedef ("size_t", C_base "unsinged int") in
  let ptr ct = C_pointer ct in
  let fp ret paramlst= C_subroutine (ret, paramlst) in
  if target_name = "deregister_tm_clones" then
    [fp void [ptr void]]
  else if target_name = "register_tm_clones" then
    [fp void [ptr void;size_t ]]
  else if target_name = "frame_dummy" then
    [(fp void [void]);(fp void [ptr void])]
  else if Sys.file_exists elf.fname then (
    let cmd = 
      P.sprintf "./dbg/collect_visible_fp %s %s > %s" elf.fname target_name resultfilename
    in
    let _ = Sys.command cmd in
    (* open visible_fp.txt to construct visible functio pointers *)
    let ic = open_in resultfilename in
    let possible_targets_t_typesig = ref [] in
    let () = 
      try
	while true do
	  let indicator = input_line ic in
	  let ctype = decode_c_type indicator in
	  possible_targets_t_typesig := search_for_subroutines_t ctype !possible_targets_t_typesig
	done
      with End_of_file ->
	(*P.printf "End of File\n\n";*)
	close_in ic
    in
    let unique_possible_targets = eliminate_dup_a' !possible_targets_t_typesig in
    (*let cmd' = P.sprintf "echo %d >> vfp_%s.count" (List.length unique_possible_targets) elf_name in
    let _ = Sys.command cmd' in*)
    unique_possible_targets
  )
  else 
    let () = P.printf "No file %s exists!" resultfilename in
    []

let collect_all_visible_function_pointers elf start : c_type list =
  let elf_name = find_target_name elf.fname in
  let resultfilename = P.sprintf "./%s_allvfp.txt" elf_name in
  if Sys.file_exists elf.fname then (
    let cmd = 
      P.sprintf "./dbg/collect_subroutine_types %s > %s" elf.fname resultfilename
    in
    let _ = if not (Sys.file_exists resultfilename) then Sys.command cmd else 0 in
    (* open visible_fp.txt to construct visible functio pointers *)
    let ic = open_in resultfilename in
    let possible_targets_t_typesig = ref [] in
    let () = 
      try
	while true do
	  let indicator = input_line ic in
	  let ctype = decode_c_type indicator in
	  possible_targets_t_typesig := ctype :: !possible_targets_t_typesig
	done
      with End_of_file ->
	(*P.printf "End of File\n\n";*)
	close_in ic
    in
    let unique_possible_targets = eliminate_dup_a' !possible_targets_t_typesig in
    (*let cmd' = P.sprintf "echo %d >> vfp_%s.count" (List.length unique_possible_targets) elf_name in
    let _ = Sys.command cmd' in*)
    unique_possible_targets
  )
  else 
    let () = P.printf "No file %s exists!" resultfilename in
    []


type func = {
  func_name: string;
  func_low: string;
  func_high: string;
  func_bbs: basicblock list;
}

let mkFUNC name low high bbs =
  {
    func_name = name;
    func_low = low;
    func_high = high;
    func_bbs = bbs;
  }

let print_func (func:func) =
  print_string " {\n";
  P.printf " function name: %s\n" func.func_name;
  P.printf " low pc:%s\n" func.func_low;
  P.printf " high pc:%s\n" func.func_high;
  print_string " Containing blocks:\n";
  List.iter (fun bb -> P.printf " %s" bb.bb_label) func.func_bbs;
  print_string "\n \n"

let collect_asm_func elf cr =
  let generate_highpc func_list topAddr =
    let compare func1 func2 = (fst func1) - (fst func2) in
    let sorted_func_list = List.sort compare func_list in
    let length = List.length sorted_func_list in
    List.mapi (fun ndx (low,lbl) -> 
      let high = 
	if (ndx+1) < length then 
	  (fst (List.nth sorted_func_list (ndx+1))) - 1 
	else
	  topAddr
      in
      (low,high,lbl)) sorted_func_list
  in

  let bbs_in_range low high bbs =
    List.filter (fun bb -> low <= bb.bb_relAddr && (bb.bb_relAddr+bb.bb_size-1) <= high) (BBSet.elements bbs)
  in

  let bbs = cr.bbs in
  let start = cr.startAddr in
  let top_bb = BBSet.max_elt bbs in
  let (_,_,len) = List.last top_bb.bb_instrs in
  let top_addr = top_bb.bb_relAddr + top_bb.bb_size - len in
  let all_functions = collect_all_functions elf start in
  (*let lib_func =
    {
      func_name = "libfunc";
      func_low = -1;
      func_high = 0;
      func_bbs = [locate_label_in_bbs "ffffffff" bbs]
    }
  in*)
  let functions_with_ranges = generate_highpc all_functions top_addr in
  List.map (fun (lopc,hipc,name) -> 
    let abs_lopc_str = str_of_mword_flex (MWord.(+%) (MWord.of_int lopc) start) in 
    let abs_hipc_str = str_of_mword_flex (MWord.(+%) (MWord.of_int hipc) start) in
    mkFUNC name abs_lopc_str abs_hipc_str (bbs_in_range lopc hipc bbs)
  ) functions_with_ranges

(*let collect_all_visible_function_pointers elf bb start : c_type list =
  let elf_name = find_target_name elf.fname in
  let target_name = func_bb_in elf bb start in
  let resultfilename = P.sprintf "./allvfp_%s_%s.txt" elf_name target_name in
  let void = C_base "void" in
  let size_t = C_typedef ("size_t", C_base "unsinged int") in
  let ptr ct = C_pointer ct in
  let fp ret paramlst= C_subroutine (ret, paramlst) in
  if target_name = "deregister_tm_clones" then
    [fp void [ptr void]]
  else if target_name = "register_tm_clones" then
    [fp void [ptr void;size_t ]]
  else if target_name = "frame_dummy" then
    [(fp void [void]);(fp void [ptr void])]
  else if Sys.file_exists elf.fname then (
    let cmd = 
      P.sprintf "./dbg/collect_subroutine_types %s > %s" elf.fname resultfilename
    in
    let _ = if not (Sys.file_exists resultfilename) then Sys.command cmd else 0  in
    (* open visible_fp.txt to construct visible functio pointers *)
    let ic = open_in resultfilename in
    let possible_targets_t_typesig = ref [] in
    let () = 
      try
	while true do
	  let indicator = input_line ic in
	  let ctype = decode_c_type indicator in
	  possible_targets_t_typesig := search_for_subroutines_t ctype !possible_targets_t_typesig
	done
      with End_of_file ->
	(*P.printf "End of File\n\n";*)
	close_in ic
    in
    let unique_possible_targets = eliminate_dup_a' !possible_targets_t_typesig in
    (*let cmd' = P.sprintf "echo %d >> vfp_%s.count" (List.length unique_possible_targets) elf_name in
    let _ = Sys.command cmd' in*)
    unique_possible_targets
  )
  else 
    let () = P.printf "No file %s exists!" resultfilename in
    []
*)

(*let typesig_match (call_typesig: (string*string) list) (eqcs:equivalent_class list) =
  let match_typesig_with_eqc typesig_list eqc =
    let ret_str = eqc.eqc_ret_t in
    let param_str = String.concat "," eqc.eqc_param_t in
    (*let () = P.printf "eqc:(%s),(%s)\n" ret_str param_str in*)
    List.exists (fun (ret, param) -> (ret = ret_str && param = param_str)) typesig_list
  in
  let combine_members teqcs =
    let tmp1 = List.map (fun eqc -> eqc.eqc_member) teqcs in
    let tmp2 = List.flatten tmp1 in
    List.map (fun (lowpc,name) -> lowpc) tmp2
  in
  let target_eqcs = List.filter (fun eqc -> (match_typesig_with_eqc call_typesig eqc)) eqcs in
  combine_members target_eqcs
*)
(** 
    type consistency has direction: ctype1 is a type in function pointers and ctype2 is a type in eqcs **)
let rec ctype_consistent ctype1 ctype2 =
  match ctype1,ctype2 with
  | C_base name1, C_base name2 -> name1 = name2
  | C_const ct1, C_const ct2 -> ctype_consistent ct1 ct2
  | C_array (ct1,dimlist1), C_array (ct2,dimlist2) -> 
    let type_consis = ctype_consistent ct1 ct2 in 
    let dim_consis = 
      if List.length dimlist1 = List.length dimlist2 then 
	List.fold_right2 
	  (fun dim1 dim2 b -> 
	    if dim1 = 0 || dim2 = 0 then b 
	    else (dim1 = dim2) && b) dimlist1 dimlist2 true
      else false
    in
    type_consis && dim_consis
  | C_array (ct1,_), C_pointer ct2 -> ctype_consistent ct1 ct2
  | C_pointer ct1, C_pointer ct2 -> ctype_consistent ct1 ct2
  | C_pointer ct1, C_array (ct2,_) -> ctype_consistent ct1 ct2
  | C_typedef (defnm1,_), C_typedef (defnm2,_) -> true
  | C_typedef (_,ct1), _ -> ctype_consistent ct1 ctype2
  | _, C_typedef (_,ct2) -> ctype_consistent ctype1 ct2
  | C_subroutine (ret1,param1), C_subroutine (ret2,param2) ->
    let ret_consis = ctype_consistent ret1 ret2 in
    let param_consis = param_consistent param1 param2 in
    ret_consis && param_consis
  | C_enumeration (enumname1,etorlist1), C_enumeration (enumname2,etorlist2) ->
    enumname1 = enumname2
  | C_union (unionname1,umemlist1), C_union (unionname2,umemlist2) ->
    (** when one has name but no type and the other does not have name but have type, they may
	still be the same **)
    if unionname1 = unionname2 then true
    else if List.length umemlist1 = List.length umemlist2 then
      List.fold_right2 (fun (ct1,str1) (ct2,str2) b -> b && (ctype_consistent ct1 ct2) && (str1 = str2)) umemlist1 umemlist2 true
    else false
  | C_structure (structname1,levelname1,smemlist1), C_structure (structname2,levelname2,smemlist2) -> 
    if structname1 = structname2 then true
    else if levelname1 = levelname2 && (List.length smemlist1) = (List.length smemlist2) then
      if (List.length smemlist1) = 0 then true
      else
	List.fold_right2 (fun (ct1,str1) (ct2,str2) b -> b && ctype_consistent ct1 ct2 && str1 = str2) smemlist1 smemlist2 true
    else false
  | C_unspecified_param, _ ->  
    (** for example: fp (int)(...) should be able to call function int foo(int) **)
    true
  | _, C_unspecified_param ->
    (** for example: fp (int)(int) should never call function int foo(...) **)    
    false
  | C_wrong_type errmsg1, _ -> 
    let () = P.printf "In ctype_consistent, the first type is wrong type: %s" errmsg1 in
    false
  | _, C_wrong_type errmsg2 ->
    let () = P.printf "In ctype_consistent, the second type is wrong type: %s" errmsg2 in
    false
  | C_volatile ct1, C_volatile ct2 -> ctype_consistent ct1 ct2
  | _ -> false
    
and param_consistent fp_param eqc_param =
  match fp_param,eqc_param with
  | [], [] -> true
  | (hd_fp_param :: tl_fp_param), [] ->
    if hd_fp_param = C_unspecified_param && 0 = List.length tl_fp_param then true
    else false
  | hd_fp_param :: tl_fp_param, hd_eqc_param :: tl_eqc_param ->
    if hd_fp_param = C_unspecified_param then
      let hd_consis = ctype_consistent hd_fp_param hd_eqc_param in
      let tl_consis = param_consistent fp_param tl_eqc_param in
      hd_consis && tl_consis
    else 
      let hd_consis = ctype_consistent hd_fp_param hd_eqc_param in
      let tl_consis = param_consistent tl_fp_param tl_eqc_param in
      hd_consis && tl_consis
  | _ -> false

let rec ctype_equal ctype1 ctype2 =
  match ctype1,ctype2 with
  | C_base name1, C_base name2 -> name1 = name2
  | C_const ct1, C_const ct2 -> ctype_equal ct1 ct2
  | C_array (ct1,dimlist1), C_array (ct2,dimlist2) -> 
    let type_consis = ctype_equal ct1 ct2 in 
    let dim_consis = 
      if List.length dimlist1 = List.length dimlist2 then 
	List.fold_right2 
	  (fun dim1 dim2 b -> 
	    if dim1 = 0 || dim2 = 0 then b 
	    else (dim1 = dim2) && b) dimlist1 dimlist2 true
      else false
    in
    type_consis && dim_consis
  | C_array (ct1,_), C_pointer ct2 -> ctype_equal ct1 ct2
  | C_pointer ct1, C_pointer ct2 -> ctype_equal ct1 ct2
  | C_pointer ct1, C_array (ct2,_) -> ctype_equal ct1 ct2
  | C_typedef (_,ct1), _ -> ctype_equal ct1 ctype2
  | _, C_typedef (_,ct2) -> ctype_equal ctype1 ct2
  | C_subroutine (ret1,param1), C_subroutine (ret2,param2) ->
    let ret_consis = ctype_equal ret1 ret2 in
    let param_consis = param_equal param1 param2 in
    ret_consis && param_consis
  | C_enumeration (enumname1,etorlist1), C_enumeration (enumname2,etorlist2) ->
    enumname1 = enumname2
  | C_union (unionname1,umemlist1), C_union (unionname2,umemlist2) ->
    (** when one has name but no type and the other does not have name but have type, they may
	still be the same **)
    if unionname1 = unionname2 then true
    else if List.length umemlist1 = List.length umemlist2 then
      List.fold_right2 (fun (ct1,str1) (ct2,str2) b -> b && (ctype_equal ct1 ct2) && (str1 = str2)) umemlist1 umemlist2 true
    else false
  | C_structure (structname1,levelname1,smemlist1), C_structure (structname2,levelname2,smemlist2) -> 
    if structname1 = structname2 then true
    else if levelname1 = levelname2 && (List.length smemlist1) = (List.length smemlist2) then
      if (List.length smemlist1) = 0 then true
      else
	List.fold_right2 (fun (ct1,str1) (ct2,str2) b -> b && ctype_equal ct1 ct2 && str1 = str2) smemlist1 smemlist2 true
    else false
  | C_unspecified_param, C_unspecified_param ->  
    true
  | C_wrong_type errmsg1, _ -> 
    let () = P.printf "In ctype_equal, the first type is wrong type: %s" errmsg1 in
    false
  | _, C_wrong_type errmsg2 ->
    let () = P.printf "In ctype_equal, the second type is wrong type: %s" errmsg2 in
    false
  | C_volatile ct1, C_volatile ct2 -> ctype_equal ct1 ct2
  | _ -> false
    
and param_equal param1 param2 =
  match param1,param2 with
  | [], [] -> true
  | hd_param1 :: tl_param1, hd_param2 :: tl_param2 ->
    ctype_equal hd_param1 hd_param2 && param_equal tl_param1 tl_param2
  | _ -> false

let rec eliminate_dup_ctype ctype_list =
  match ctype_list with
  | [] -> []
  | h :: t ->
    if List.exists (fun e -> ctype_equal h e) t then eliminate_dup_ctype t
    else h :: (eliminate_dup_ctype t)

let t_typesig_match (call_t_typesig: c_type list) (teqcs:t_equivalent_class list) =
  let match_t_typesig_with_teqc t_typesig_list teqc =
    let i_match teqc_ret teqc_param subroutine_type =
      match subroutine_type with
      | C_subroutine (ret, param) ->
	ctype_consistent ret teqc_ret && (param_consistent param teqc_param)
      | _ -> false
    in
    let teqc_ret = teqc.t_eqc_ret_t in
    let teqc_param_list = teqc.t_eqc_param_t in
    (*let () = P.printf "eqc:(%s),(%s)\n" ret_str param_str in*)
    List.exists (i_match teqc_ret teqc_param_list) t_typesig_list
  in
  let combine_members teqcs =
    let tmp1 = List.map (fun teqc -> teqc.t_eqc_member) teqcs in
    let tmp2 = List.flatten tmp1 in
    List.map (fun (lowpc,name) -> lowpc) tmp2
  in
  let target_eqcs = List.filter (fun teqc -> (match_t_typesig_with_teqc call_t_typesig teqc)) teqcs in
  combine_members target_eqcs

let read_dataseg_for_target (elf:elf_file) (addr:address) (start:mword) =
  let addrDisp = addr.addrDisp in
  let read_data_seg phdr = 
    if is_data_seg phdr then
      let data = file_read_region elf.fname (MWord.to_int phdr.p_offset)
	(MWord.to_int phdr.p_filesz) in
      Some (data, phdr.p_vaddr)
    else None
  in
  let op_addr_mword = MWord.of_big_int (unsigned32 addrDisp) in
  let data,data_start = List.hd (List.filter_map read_data_seg elf.phdr_tab) in
  let op_rel_addr = get_relative_addr op_addr_mword data_start in
  let data_size = Array1.dim data in
  if op_rel_addr < 0 || op_rel_addr > data_size then
    let () = 
      print_endline "Error: operand address is illegal!";
      P.printf "Illegal address is %s\n" (str_of_mword_flex op_addr_mword);
    in
    raise (Failure "read_dataseg: Operand is illegeal!")
  else
    (* read 4 bytes from op_rel_addr to form an absolute address
       then get_relative address *)
    let target_address = form_abs_addr data op_rel_addr in
    (*if MWord.to_int target_address = 0 then [] (* first entry in .plt *)
    else [get_relative_addr target_address start]*)
    target_address
      
let read_codeseg_for_target (elf:elf_file) (addr:address) (start:mword) =
  let addrDisp = addr.addrDisp in
  let read_code_seg phdr = 
    if is_code_seg phdr then
      let code = file_read_region elf.fname (MWord.to_int phdr.p_offset)
	(MWord.to_int phdr.p_filesz) in
      Some (code, phdr.p_vaddr)
    else None
  in
  let op_addr_mword = MWord.of_big_int (unsigned32 addrDisp) in
  let code,code_start = List.hd (List.filter_map read_code_seg elf.phdr_tab) in
  let op_rel_addr = get_relative_addr op_addr_mword code_start in
  let code_size = Array1.dim code in
  if op_rel_addr < 0 || op_rel_addr > code_size then
    let () = 
      print_endline "Error: operand address is illegal!";
      P.printf "Illegal address is %s\n" (str_of_mword_flex op_addr_mword);
    in
    raise (Failure "read_codeseg: Operand is illegal ")
  else
    (* read 4 bytes from op_rel_addr to form an absolute address
       then get_relative address *)
    let target_address = form_abs_addr code op_rel_addr in
    target_address
    (*if MWord.to_int target_address = 0 then []
    else [get_relative_addr target_address start]*)

let addr_in_got_plt elf addr =
  let addrDisp = addr.addrDisp in
  let got_plt_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".got.plt") elf.shdr_tab) in
  let got_plt_start = got_plt_shdr.sh_addr in
  let got_plt_size = got_plt_shdr.sh_size in
  let abs_addr_mword = MWord.of_big_int (unsigned32 addrDisp) in
  (MWord.(<=%) got_plt_start abs_addr_mword) && (MWord.(<=%) abs_addr_mword (MWord.(+%) got_plt_start got_plt_size))

let addrop_in_rodata elf addr =
  let addrDisp = addr.addrDisp in
  let rodata_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".rodata") elf.shdr_tab) in
  let rodata_start = rodata_shdr.sh_addr in
  let rodata_size = rodata_shdr.sh_size in
  let abs_addr_mword = MWord.of_big_int (unsigned32 addrDisp) in
  (MWord.(<=%) rodata_start abs_addr_mword) && (MWord.(<=%) abs_addr_mword (MWord.(+%) rodata_start rodata_size))

let addrop_in_data elf addr =
  let addrDisp = addr.addrDisp in
  let data_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".data") elf.shdr_tab) in
  let data_start = data_shdr.sh_addr in
  let data_size = data_shdr.sh_size in
  let abs_addr_mword = MWord.of_big_int (unsigned32 addrDisp) in
  (MWord.(<=%) data_start abs_addr_mword) && (MWord.(<=%) abs_addr_mword (MWord.(+%) data_start data_size))

let reladdr_in_plt elf rel_addr start =
  let data_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".plt") elf.shdr_tab) in
  let data_start = data_shdr.sh_addr in
  let data_size = data_shdr.sh_size in
  let abs_addr_mword = MWord.(+%) (MWord.of_int rel_addr) start in
  (MWord.(<=%) data_start abs_addr_mword) && (MWord.(<=%) abs_addr_mword (MWord.(+%) data_start data_size))

let is_jmp_tail_call ijump_site tcs =
  List.mem ijump_site tcs

let is_jmp_plt elf addr =
  (Option.is_none addr.addrBase) && (Option.is_none addr.addrIndex) && (addr_in_got_plt elf addr)

let is_read_only_data elf addr =
  addrop_in_rodata elf addr

let is_data_sec elf addr =
  addrop_in_data elf addr

let is_label (elf:elf_file) (addr:int) (start:mword)=
  let labels = collect_labels elf start in
  List.exists (fun lbl -> addr = lbl.label_addr) labels

let is_visible_label elf bb addr start lbl =
  let visible_labels = collect_wanted_label_addr elf bb start lbl in
  List.mem addr visible_labels

(** search_back_for_bound: bb -> bbs -> start -> int option **)
    
let search_back_for_bound bb bbs start=
  let find_bound op =
    match op with
    | Imm_op i -> [Big.to_int (signed32 i)]
    | _ -> []
  in
  let search_bound instr =
    let (pre, ins, len) = instr in
    match ins with
    | CMP (_,op1,op2) -> find_bound op1 @ find_bound op2
    | CMPXCHG (_,op1,op2) -> find_bound op1 @ find_bound op2
    | _ -> []
  in
  let rec search_instrs instrs =
    match instrs with
    | [] -> []
    | h :: t -> search_bound h @ search_instrs t
  in
  let rec search_back_for_bound_aux start_bb =
    let possible_bounds_in_bb = search_instrs start_bb.bb_instrs in
    if start_bb.bb_symbol_label <> "" || not (List.is_empty possible_bounds_in_bb) 
    then possible_bounds_in_bb
    else 
      let pred_bb_list =
	List.filter_map (fun label -> 
	  try
	    Some (List.find (fun bb -> bb.bb_label = label) (BBSet.elements bbs)) 
	  with Not_found -> None
	) start_bb.bb_preds
      in
      let possible_bounds_in_preds = 
	List.flatten (List.map (fun pred_bb -> search_back_for_bound_aux pred_bb) pred_bb_list)
      in
      possible_bounds_in_preds
  in
  let possible_bounds = search_back_for_bound_aux bb in
  let () =
    P.printf "Find possible bounds for %s:\n " bb.bb_label;
    List.iter (fun bound -> P.printf "Found bound %d\n" bound) possible_bounds;
    flush stdout
  in
  if List.is_empty possible_bounds then None
  else Some (List.max possible_bounds)
    

let search_preds_for_bound bb bbs start =
  let rec naive_find_bounds instrs =
    let find_imm op =
      match op with
      | Imm_op i -> [Big.to_int i]
      | _ -> []
    in
    let naive_find_bounds_aux instr =
      let (pre,ins,len) = instr in
      match ins with
      | CMP (_,op1,op2) -> find_imm op1 @ find_imm op2
      | CMPXCHG (_,op1,op2) -> find_imm op1 @ find_imm op2
      | _ -> []
    in
    match instrs with
    | [] -> []
    | h :: t -> (naive_find_bounds_aux h) @ (naive_find_bounds t)
  in
  let pred_bb_list =
    List.filter_map (fun label -> 
      try
	Some (List.find (fun bb -> bb.bb_label = label) (BBSet.elements bbs)) 
      with Not_found -> None
    ) bb.bb_preds
  in
  let possible_bounds = 
    List.flatten (List.map (fun bb -> naive_find_bounds bb.bb_instrs) pred_bb_list)
  in
  if List.is_empty possible_bounds then None
  else Some (List.max possible_bounds)  (* in terms of number of entries *)

let search_max_bound bb =
  let find_bound op =
    match op with
    | Imm_op i -> [Big.to_int i]
    | _ -> []
  in
  let search_bound instr =
    let (pre, ins, len) = instr in
    match ins with
    | CMP (_,op1,op2) -> find_bound op1 @ find_bound op2
    | CMPXCHG (_,op1,op2) -> find_bound op1 @ find_bound op2
    | _ -> []
  in
  let rec search_instrs instrs =
    match instrs with
    | [] -> []
    | h :: t -> search_bound h @ search_instrs t
  in
  let possible_bounds = search_instrs bb.bb_instrs in
  (*let () =
    P.printf "Find possible bounds for %s:\n " bb.bb_label;
    List.iter (fun bound -> P.printf "Found bound %d\n" bound) possible_bounds;
    flush stdout
  in  *)
  if List.is_empty possible_bounds then None
  else Some (List.max possible_bounds)

let search_possible_bases bb lopc hipc =
  let rec inner_max mword_list =
    match mword_list with
    | [] -> raise (Invalid_argument "empty list")
    | h :: [] -> h
    | h :: t -> 
      let bigtail = inner_max t in
      if MWord.(>%) h bigtail then h else bigtail
  in
  let rec inner_min mword_list =
    match mword_list with
    | [] -> raise (Invalid_argument "empty list")
    | h :: [] -> h
    | h :: t -> 
      let smalltail = inner_min t in
      if MWord.(<%) h smalltail then h else smalltail
  in
  let find_base_mword op =
    match op with
    | Imm_op i -> [MWord.of_big_int i]
    | Address_op addr -> (
      match addr with
      | {addrDisp = addr'; addrBase = _; addrIndex = _} -> [MWord.of_big_int addr']
    )
    | _ -> []
  in
  let search_base instr =
    let (pre, ins, len) = instr in
    match ins with
    | CMOVcc (_,op1,op2) -> find_base_mword op1 @ find_base_mword op2
    | MOV (_,op1,op2) -> find_base_mword op1 @ find_base_mword op2
    | MOVBE (op1,op2) -> find_base_mword op1 @ find_base_mword op2
    | MOVSX (_,op1,op2) -> find_base_mword op1 @ find_base_mword op2
    | MOVZX (_,op1,op2) -> find_base_mword op1 @ find_base_mword op2
    | ADD (_,op1,op2) -> find_base_mword op1 @ find_base_mword op2
    | _ -> []
  in
  let rec search_instrs instrs =
    match instrs with
    | [] -> []
    | h :: t -> search_base h @ search_instrs t
  in
  let possible_bases = List.filter (fun base -> MWord.(>=%) base lopc && MWord.(<%) base hipc) (search_instrs bb.bb_instrs) in
  (*let () =
    P.printf "Find possible bases for %s:\n " bb.bb_label;
    List.iter (fun base -> P.printf "Found base %s\n" (str_of_mword_flex base)) possible_bases;
    flush stdout
  in  *)
  if List.is_empty possible_bases then None
  else Some (inner_min possible_bases, inner_max possible_bases)

let subst_reg instr orig_op =
  let (pre,ins,len) = instr in
  match ins with
  | MOV (_, dest_op, src_op) -> 
    if dest_op = orig_op then [src_op]
    else [orig_op]
  | CMOVcc (_,dest_op, src_op) ->
    if dest_op = orig_op then [src_op; orig_op]
    else [orig_op]
  | _ -> [orig_op]

exception TailCall

let safe_pattern_match bb reg =
  let str_of_reg reg =
    match reg with
    | EAX -> "EAX"
    | ECX -> "ECX"
    | EDX -> "EDX"
    | EBX -> "EBX"
    | ESP -> "ESP"
    | EBP -> "EBP"
    | ESI -> "ESI"
    | EDI -> "EDI"
  in
  match List.map (fun (pre,ins,len) -> ins) (List.tl (List.rev bb.bb_instrs)) with
  | [] -> P.printf "Nothing to match at BB %s! Reg_op %s\n" bb.bb_label (str_of_reg reg); flush stdout; Reg_op reg
  | (MOV (_,Reg_op reg, Offset_op addr)) :: _ -> 
    Address_op {addrDisp = addr; addrBase = None; addrIndex = None}
  | (MOV (_,Reg_op reg, Address_op addr)) :: tl -> (
    match tl with
    | ADD (_,_,base) :: SHL (_,_,_) :: MOV(_,_,_) :: _ -> (
      match base with 
      | Imm_op i -> 
	Address_op {addrDisp = i; addrBase = None; addrIndex = Some (Scale4,reg)}
      | _ -> P.printf "Base not recognized in Pattern at BB %s! Reg_op %s\n" bb.bb_label (str_of_reg reg); flush stdout; 
	Reg_op reg
    )
    | _ -> Address_op addr
  )
  | _ :: _ -> 
    P.printf "Pattern not matched at BB %s! Reg_op %s\n" bb.bb_label (str_of_reg reg); flush stdout; 
    print_bb stdout bb (MWord.of_string "0x8048000");
    Reg_op reg

let pattern_match bb reg =
  let str_of_reg reg =
    match reg with
    | EAX -> "EAX"
    | ECX -> "ECX"
    | EDX -> "EDX"
    | EBX -> "EBX"
    | ESP -> "ESP"
    | EBP -> "EBP"
    | ESI -> "ESI"
    | EDI -> "EDI"
  in
  match List.map (fun (pre,ins,len) -> ins) (List.tl (List.rev bb.bb_instrs)) with
  | [] -> P.printf "Nothing to match at BB %s! Reg_op %s\n" bb.bb_label (str_of_reg reg); flush stdout; Reg_op reg
  | (MOV (_,Reg_op reg, Offset_op addr)) :: _ -> 
    Address_op {addrDisp = addr; addrBase = None; addrIndex = None}
  | (MOV (_,Reg_op reg, Address_op addr)) :: tl -> (
    match tl with
    | ADD (_,_,base) :: SHL (_,_,_) :: MOV(_,_,_) :: _ -> (
      match base with 
      | Imm_op i -> 
	Address_op {addrDisp = i; addrBase = None; addrIndex = Some (Scale4,reg)}
      | _ -> P.printf "Base not recognized in Pattern at BB %s! Reg_op %s\n" bb.bb_label (str_of_reg reg); flush stdout; 
	Reg_op reg
    )
    | _ -> Address_op addr
  )
  | MOV (_,Address_op addr,_) :: _ -> (
    match addr with
    | {addrDisp=_;addrBase = Some ESP; addrIndex=None} -> raise TailCall
    | _ -> Reg_op reg
  )
  | POP op :: _ -> raise TailCall
  | ADD (_,Reg_op ESP,_) :: _-> raise TailCall
  | _ :: _ -> 
    P.printf "Pattern not matched at BB %s! Reg_op %s\n" bb.bb_label (str_of_reg reg); flush stdout; 
    print_bb stdout bb (MWord.of_string "0x8048000");
    Reg_op reg

let rec search_operand instrs orig_op =
  match instrs with
  | [] -> [orig_op]
  | h :: t ->
    let curr_ops = subst_reg h orig_op in
    List.flatten (
      List.map (fun op ->
	match op with
	| Reg_op reg' -> search_operand t op
	| _ -> [op]
      ) curr_ops
    )

let validAddrIn elf bb start addr_list =
  let (pre,ins,len) = List.last bb.bb_instrs in
  let indirect_jump_site = bb.bb_relAddr+bb.bb_size-len in
  let func_ranges = collect_func_ranges_from_symtab elf start in
  let visible_fs,visible_fe = List.find (fun (fs,fe) -> (fs <= indirect_jump_site) && (indirect_jump_site <= fe)) func_ranges in
  List.filter (fun addr -> addr >= visible_fs && addr <= visible_fe) addr_list

let read_aligned_data_for_all elf code bb start =
  let read_data_seg phdr = 
    if is_data_seg phdr then
      let data = file_read_region elf.fname (MWord.to_int phdr.p_offset)
	(MWord.to_int phdr.p_filesz) in
      Some (data, phdr.p_vaddr)
    else None
  in
  let dataseg,dataseg_start = List.hd (List.filter_map read_data_seg elf.phdr_tab) in
  let (pre,ins,len) = List.last bb.bb_instrs in
  let indirect_jump_site = bb.bb_relAddr+bb.bb_size-len in
  let func_ranges = collect_func_ranges_from_symtab elf start in
  let visible_fs,visible_fe = List.find (fun (fs,fe) -> (fs <= indirect_jump_site) && (indirect_jump_site <= fe)) func_ranges in
  let data_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".data") elf.shdr_tab) in
  let data_start = get_relative_addr data_shdr.sh_addr dataseg_start in
  let data_size = MWord.to_int data_shdr.sh_size in
  let init_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".init") elf.shdr_tab) in
  let init_start = init_shdr.sh_addr in
  let fini_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".fini") elf.shdr_tab) in
  let fini_start = fini_shdr.sh_addr in
  let fini_size = fini_shdr.sh_size in
  let code_sec_lopc = init_start in
  let code_sec_hipc = MWord.(+%) fini_start fini_size in
  let rec read_aux read_start read_end =
    let op_rel_addr = read_start in
    let dataseg_size = Array1.dim dataseg in
    if (read_start + 4) > read_end then []
    else if op_rel_addr < 0 || op_rel_addr >= dataseg_size then
      let () = 
	print_endline "Error: operand address is illegal! (in read_data_all)";
	P.printf "Illegal address is %s\n" (str_of_mword_flex (MWord.(+%) dataseg_start (MWord.of_int op_rel_addr)));
      in
      []
    else
      let target_address = form_abs_addr dataseg op_rel_addr in
      let next_start_addr = read_start + 4 in
      if (MWord.(>=%) target_address code_sec_lopc) && (MWord.(<=%) target_address code_sec_hipc)  then (
	let rel_target_address = get_relative_addr target_address start in
	if (visible_fs <= rel_target_address) && (rel_target_address <= visible_fe) then
	  rel_target_address :: (read_aux next_start_addr read_end) 
	else
	  read_aux next_start_addr read_end
      )
      else 
	read_aux next_start_addr read_end
  in
  let startAddr = data_start in
  let endAddr = data_start + data_size in
  let possible_jump_targets = read_aux startAddr endAddr in
  possible_jump_targets

let read_aligned_data_for_labels elf code bb start bound_option bbs =
  let read_data_seg phdr = 
    if is_data_seg phdr then
      let data = file_read_region elf.fname (MWord.to_int phdr.p_offset)
	(MWord.to_int phdr.p_filesz) in
      Some (data, phdr.p_vaddr)
    else None
  in
  let dataseg,dataseg_start = List.hd (List.filter_map read_data_seg elf.phdr_tab) in
  let (pre,ins,len) = List.last bb.bb_instrs in
  let indirect_jump_site = bb.bb_relAddr+bb.bb_size-len in
  let func_ranges = collect_func_ranges_from_symtab elf start in
  let visible_fs,visible_fe = List.find (fun (fs,fe) -> (fs <= indirect_jump_site) && (indirect_jump_site <= fe)) func_ranges in
  let data_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".data") elf.shdr_tab) in
  let data_start = get_relative_addr data_shdr.sh_addr dataseg_start in
  let data_size = MWord.to_int data_shdr.sh_size in
  let init_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".init") elf.shdr_tab) in
  let init_start = init_shdr.sh_addr in
  let fini_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".fini") elf.shdr_tab) in
  let fini_start = fini_shdr.sh_addr in
  let fini_size = fini_shdr.sh_size in
  let code_sec_lopc = init_start in
  let code_sec_hipc = MWord.(+%) fini_start fini_size in
  let rec read_aux read_start read_end =
    let op_rel_addr = read_start in
    let dataseg_size = Array1.dim dataseg in
    if (read_start + 4) > read_end then []
    else if op_rel_addr < 0 || op_rel_addr >= dataseg_size then
      let () = 
	print_endline "Error: operand address is illegal! (in read_data)";
	P.printf "Illegal address is %s\n" (str_of_mword_flex (MWord.(+%) dataseg_start (MWord.of_int op_rel_addr)));
      in
      []
    else
      let target_address = form_abs_addr dataseg op_rel_addr in
      let next_start_addr = read_start + 4 in
      if (MWord.(>=%) target_address code_sec_lopc) && (MWord.(<=%) target_address code_sec_hipc)  then (
	let rel_target_address = get_relative_addr target_address start in
	if (visible_fs <= rel_target_address) && (rel_target_address <= visible_fe) (*&& (not (is_label elf (get_relative_addr target_address start) start))*) then
	  rel_target_address :: (read_aux next_start_addr read_end) 
	else
	  read_aux next_start_addr read_end
      )
      else 
	read_aux next_start_addr read_end
  in
  let base_option = search_possible_bases bb data_shdr.sh_addr (MWord.(+%) data_shdr.sh_addr data_shdr.sh_size) in
  let startAddr, endAddr =
    if Option.is_none base_option then (data_start, data_start+data_size)
    else 
      let base_min, base_max = Option.get base_option in
      if Option.is_none bound_option then 
	let new_bound_option = search_back_for_bound bb bbs start in
	if Option.is_none new_bound_option then
	  (get_relative_addr base_min dataseg_start, data_start+data_size)
	else
	  let bound = Option.get new_bound_option in
	  (get_relative_addr base_min dataseg_start, (get_relative_addr base_max dataseg_start) + 4 * bound - 1)
      else
	let bound = Option.get bound_option in
	(get_relative_addr base_min dataseg_start, (get_relative_addr base_max dataseg_start) + 4 * bound - 1)
  in
  let possible_jump_targets = read_aux startAddr endAddr in
  possible_jump_targets

let read_aligned_data_for_labels_from_addr elf code bb start addr bound_option bbs =
  let read_data_seg phdr = 
    if is_data_seg phdr then
      let data = file_read_region elf.fname (MWord.to_int phdr.p_offset)
	(MWord.to_int phdr.p_filesz) in
      Some (data, phdr.p_vaddr)
    else None
  in
  let dataseg,dataseg_start = List.hd (List.filter_map read_data_seg elf.phdr_tab) in
  let (pre,ins,len) = List.last bb.bb_instrs in
  let indirect_jump_site = bb.bb_relAddr+bb.bb_size-len in
  let func_ranges = collect_func_ranges_from_symtab elf start in
  let visible_fs,visible_fe = List.find (fun (fs,fe) -> (fs <= indirect_jump_site) && (indirect_jump_site <= fe)) func_ranges in
  let data_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".data") elf.shdr_tab) in
  let data_start = get_relative_addr data_shdr.sh_addr dataseg_start in
  let data_size = MWord.to_int data_shdr.sh_size in
  let init_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".init") elf.shdr_tab) in
  let init_start = init_shdr.sh_addr in
  let fini_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".fini") elf.shdr_tab) in
  let fini_start = fini_shdr.sh_addr in
  let fini_size = fini_shdr.sh_size in
  let code_sec_lopc = init_start in
  let code_sec_hipc = MWord.(+%) fini_start fini_size in
  let rec read_aux read_start read_end =
    let op_rel_addr = read_start in
    let dataseg_size = Array1.dim dataseg in
    if (read_start + 4) > read_end then []
    else if op_rel_addr < 0 || op_rel_addr >= dataseg_size then
      let () = 
	print_endline "Error: operand address is illegal! (in read_data_addr)";
	P.printf "Illegal address is %s\n" (str_of_mword_flex (MWord.(+%) dataseg_start (MWord.of_int op_rel_addr)));
      in
      []
    else
      let target_address = form_abs_addr dataseg op_rel_addr in
      let next_start_addr = read_start + 4 in
      if (MWord.(>=%) target_address code_sec_lopc) && (MWord.(<=%) target_address code_sec_hipc)  then (
	let rel_target_address = get_relative_addr target_address start in
	if (visible_fs <= rel_target_address) && (rel_target_address <= visible_fe) (*&& (not (is_label elf (get_relative_addr target_address start) start))*) then
	  rel_target_address :: (read_aux next_start_addr read_end) 
	else
	  [] (*read_aux next_start_addr read_end*)
      )
      else []
	(*read_aux next_start_addr read_end*)
  in
  let startAddr = get_relative_addr (MWord.of_big_int (unsigned32 addr)) dataseg_start in
  (*let endAddr = 
    if Option.is_none bound_option then 
      let new_bound_option = search_back_for_bound bb bbs start in
      if Option.is_none new_bound_option then (data_start+data_size)
      else startAddr + 4 * (Option.get new_bound_option) - 1 
    else startAddr + 4 * (Option.get bound_option) - 1 
      
  in*)
  let endAddr = (data_start+data_size) in 
  let possible_jump_targets = read_aux startAddr endAddr in
  possible_jump_targets

let read_aligned_rodata_for_all elf code bb start =
  let (pre,ins,len) = List.last bb.bb_instrs in
  let indirect_jump_site = bb.bb_relAddr+bb.bb_size-len in
  let func_ranges = collect_func_ranges_from_symtab elf start in
  let visible_fs,visible_fe = List.find (fun (fs,fe) -> (fs <= indirect_jump_site) && (indirect_jump_site <= fe)) func_ranges in
  let rodata_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".rodata") elf.shdr_tab) in
  let rodata_start = get_relative_addr rodata_shdr.sh_addr start in
  let rodata_size = MWord.to_int rodata_shdr.sh_size in
  let init_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".init") elf.shdr_tab) in
  let init_start = init_shdr.sh_addr in
  let fini_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".fini") elf.shdr_tab) in
  let fini_start = fini_shdr.sh_addr in
  let fini_size = fini_shdr.sh_size in
  let code_sec_lopc = init_start in
  let code_sec_hipc = MWord.(+%) fini_start fini_size in
  let rec read_aux read_start read_end =
    let op_rel_addr = read_start in
    let code_size = Array1.dim code in
    if (read_start + 4) > read_end then []
    else if op_rel_addr < 0 || op_rel_addr >= code_size then
      let () = 
	print_endline "Error: operand address is illegal! (in read_rodata)";
	P.printf "Illegal address is %s\n" (str_of_mword_flex (MWord.(+%) start (MWord.of_int op_rel_addr)));
      in
      []
    else
      let target_address = form_abs_addr code op_rel_addr in
      let next_start_addr = read_start + 4 in
      if (MWord.(>=%) target_address code_sec_lopc) && (MWord.(<=%) target_address code_sec_hipc)  then (
	let rel_target_address = get_relative_addr target_address start in
	if (visible_fs <= rel_target_address) && (rel_target_address <= visible_fe) (*&& (not (is_label elf (get_relative_addr target_address start) start))*) then
	  rel_target_address :: (read_aux next_start_addr read_end) 
	else
	  read_aux next_start_addr read_end
      )
      else 
	read_aux next_start_addr read_end
  in
  let startAddr = rodata_start in
  let endAddr = rodata_start + rodata_size in
  let possible_jump_targets = read_aux startAddr endAddr in
  possible_jump_targets

let read_aligned_rodata_for_targets elf code bb start bound_option bbs =
  let (pre,ins,len) = List.last bb.bb_instrs in
  let indirect_jump_site = bb.bb_relAddr+bb.bb_size-len in
  let func_ranges = collect_func_ranges_from_symtab elf start in
  let visible_fs,visible_fe = List.find (fun (fs,fe) -> (fs <= indirect_jump_site) && (indirect_jump_site <= fe)) func_ranges in
  let rodata_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".rodata") elf.shdr_tab) in
  let rodata_start = get_relative_addr rodata_shdr.sh_addr start in
  let rodata_size = MWord.to_int rodata_shdr.sh_size in
  let init_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".init") elf.shdr_tab) in
  let init_start = init_shdr.sh_addr in
  let fini_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".fini") elf.shdr_tab) in
  let fini_start = fini_shdr.sh_addr in
  let fini_size = fini_shdr.sh_size in
  let code_sec_lopc = init_start in
  let code_sec_hipc = MWord.(+%) fini_start fini_size in
  let rec read_aux read_start read_end =
    let op_rel_addr = read_start in
    let code_size = Array1.dim code in
    if (read_start + 4) > read_end then []
    else if op_rel_addr < 0 || op_rel_addr >= code_size then
      let () = 
	print_endline "Error: operand address is illegal! (in read_rodata)";
	P.printf "Illegal address is %s\n" (str_of_mword_flex (MWord.(+%) start (MWord.of_int op_rel_addr)));
      in
      []
    else
      let target_address = form_abs_addr code op_rel_addr in
      let next_start_addr = read_start + 4 in
      if (MWord.(>=%) target_address code_sec_lopc) && (MWord.(<=%) target_address code_sec_hipc)  then (
	let rel_target_address = get_relative_addr target_address start in
	if (visible_fs <= rel_target_address) && (rel_target_address <= visible_fe) (*&& (not (is_label elf (get_relative_addr target_address start) start))*) then
	  rel_target_address :: (read_aux next_start_addr read_end) 
	else
	  read_aux next_start_addr read_end
      )
      else 
	read_aux next_start_addr read_end
  in
  let base_option = search_possible_bases bb rodata_shdr.sh_addr (MWord.(+%) rodata_shdr.sh_addr rodata_shdr.sh_size) in
  let startAddr, endAddr =
    if Option.is_none base_option then (rodata_start, rodata_start+rodata_size)
    else 
      let base_min, base_max = Option.get base_option in
      if Option.is_none bound_option then 
	let new_bound_option = search_back_for_bound bb bbs start in
	if Option.is_none new_bound_option then
	  (get_relative_addr base_min start, rodata_start+rodata_size)
	else
	  let bound = Option.get new_bound_option in
	  (get_relative_addr base_min start, (get_relative_addr base_max start) + 4 * bound - 1)
      else
	let bound = Option.get bound_option in
	(get_relative_addr base_min start, (get_relative_addr base_max start) + 4 * bound - 1)
  in
  let possible_jump_targets = read_aux startAddr endAddr in
  possible_jump_targets

let read_aligned_rodata_for_targets_from_addr elf code bb start addr bound_option bbs =
  let (pre,ins,len) = List.last bb.bb_instrs in
  let indirect_jump_site = bb.bb_relAddr+bb.bb_size-len in
  let func_ranges = collect_func_ranges_from_symtab elf start in
  let visible_fs,visible_fe = List.find (fun (fs,fe) -> (fs <= indirect_jump_site) && (indirect_jump_site <= fe)) func_ranges in
  let rodata_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".rodata") elf.shdr_tab) in
  let rodata_start = get_relative_addr rodata_shdr.sh_addr start in
  let rodata_size = MWord.to_int rodata_shdr.sh_size in
  let init_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".init") elf.shdr_tab) in
  let init_start = init_shdr.sh_addr in
  let fini_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".fini") elf.shdr_tab) in
  let fini_start = fini_shdr.sh_addr in
  let fini_size = fini_shdr.sh_size in
  let code_sec_lopc = init_start in
  let code_sec_hipc = MWord.(+%) fini_start fini_size in
  let rec read_aux read_start read_end =
    let op_rel_addr = read_start in
    let code_size = Array1.dim code in
    if (read_start + 4) > read_end then []
    else if op_rel_addr < 0 || op_rel_addr >= code_size then
      let () = 
	print_endline "Error: operand address is illegal! (in read_rodata_addr)";
	P.printf "Illegal address is %s\n" (str_of_mword_flex (MWord.(+%) start (MWord.of_int op_rel_addr)));
      in
      []
    else
      let target_address = form_abs_addr code op_rel_addr in
      let next_start_addr = read_start + 4 in
      if (MWord.(>=%) target_address code_sec_lopc) && (MWord.(<=%) target_address code_sec_hipc)  then (
	let rel_target_address = get_relative_addr target_address start in
	if (visible_fs <= rel_target_address) && (rel_target_address <= visible_fe) (*&& (not (is_label elf (get_relative_addr target_address start) start))*) then
	  rel_target_address :: (read_aux next_start_addr read_end) 
	else
	  []
	  (*read_aux next_start_addr read_end*)
      )
      else []
	(*read_aux next_start_addr read_end*)
  in
  let startAddr = get_relative_addr (MWord.of_big_int (unsigned32 addr)) start in
  (*let endAddr = 
    if Option.is_none bound_option then 
      let new_bound_option = search_back_for_bound bb bbs start in
      if Option.is_none new_bound_option then (rodata_start+rodata_size)
      else startAddr + 4 * (Option.get new_bound_option) - 1 
    else startAddr + 4 * (Option.get bound_option) - 1 
  in*)
  let endAddr = rodata_start + rodata_size in
  let possible_jump_targets = read_aux startAddr endAddr in
  possible_jump_targets

let read_init_array_for_targets elf code bb start =
  let read_data_seg phdr = 
    if is_data_seg phdr then
      let data = file_read_region elf.fname (MWord.to_int phdr.p_offset)
	(MWord.to_int phdr.p_filesz) in
      Some (data, phdr.p_vaddr)
    else None
  in
  let data,data_start = List.hd (List.filter_map read_data_seg elf.phdr_tab) in
  let data_size = Array1.dim data in
  let init_array_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".init_array") elf.shdr_tab) in
  (*let () = P.printf "start:%s; size:%s\n" (str_of_mword_flex init_array_shdr.sh_addr) (str_of_mword_flex init_array_shdr.sh_size) in*)
  let init_array_start = get_relative_addr init_array_shdr.sh_addr data_start in
  let init_array_size = MWord.to_int init_array_shdr.sh_size in
  (*let () = P.printf "start:%d; size:%d\n" init_array_start init_array_size in*)
  let init_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".init") elf.shdr_tab) in
  (*let () = P.printf "start:%s; size:%s\n" (str_of_mword_flex data_shdr.sh_addr) (str_of_mword_flex data_shdr.sh_size) in*)
  let init_start = init_shdr.sh_addr in
  let fini_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".fini") elf.shdr_tab) in
  (*let () = P.printf "start:%s; size:%s\n" (str_of_mword_flex data_shdr.sh_addr) (str_of_mword_flex data_shdr.sh_size) in*)
  let fini_start = fini_shdr.sh_addr in
  let fini_size = fini_shdr.sh_size in
  let code_sec_lopc = init_start in
  let code_sec_hipc = MWord.(+%) fini_start fini_size in
  let rec read_aux read_start read_end =
    let op_rel_addr = read_start in
    if op_rel_addr < 0 || op_rel_addr >= data_size then
      let () = 
	print_endline "Error: operand address is illegal! (in read_init_array)";
	P.printf "Illegal address is %s\n" (str_of_mword_flex (MWord.(+%) data_start (MWord.of_int op_rel_addr)));
      in
      []
    else
      (* read 4 bytes from op_rel_addr to form an absolute address
	 then get_relative address *)
      let target_address = form_abs_addr data op_rel_addr in
      (*let () = P.printf "target_address: %s\n" (str_of_mword_flex target_address) in*)
      let next_start_addr = read_start + 4 in
      (*let () = P.printf "next_start_addr = %s\n" (str_of_mword_flex (MWord.of_int next_start_addr)) in*)
      if (read_start + 4) > read_end then []
      else if MWord.(>=%) target_address code_sec_lopc && MWord.(<=%) target_address code_sec_hipc then (
	let rel_target_address = get_relative_addr target_address start in
	(*let () = P.printf "rel_target_address: %s\n" (str_of_mword_flex (MWord.of_int rel_target_address)) in*) 
	rel_target_address :: (read_aux next_start_addr read_end) 
      )
      else 
	read_aux next_start_addr read_end
  in
  read_aux init_array_start (init_array_start+init_array_size)

    
let precollect_visible_typeinfo elf =
  let start = MWord.of_string "0x8048000" in
  let all_functions = collect_all_functions elf start in
  List.iter (fun (lopc,target_name) ->
    let resultdir = P.sprintf "%s_type" elf.fname in
    if not (Sys.file_exists resultdir) then (
      let cmd = P.sprintf "mkdir %s" resultdir in
      let _ = Sys.command cmd in
      ()
    );
    let resultfilename = P.sprintf "%s/visible_typeinfo_%s.txt" resultdir target_name in
    let cmd = 
      P.sprintf "./dbg/collect_visible_typeinfo %s %s > %s" elf.fname target_name resultfilename 
    in
    if not (Sys.file_exists resultfilename) then
      if Sys.file_exists elf.fname then (
	P.printf "collect elf.fname %s: %s\n" elf.fname target_name; 
	let exit_code = Sys.command cmd in
	P.printf "exit_code: %d\n" exit_code; 
	flush stdout
      )
  ) all_functions

let preprocess elf =
  if Sys.file_exists elf.fname then
    let resultdir = P.sprintf "%s_type" elf.fname in
    if not (Sys.file_exists resultdir) then (
      let cmd = P.sprintf "mkdir %s" resultdir in
      let _ = Sys.command cmd in
      ()
    );
    (*let elf_name = find_target_name elf.fname in*)
    (*let tcs_file = P.sprintf "%s_tcs.txt" elf_name in*)
    (*let lbl_file = P.sprintf "%s_lbl.txt" elf_name in*)
    let fsig_file = P.sprintf "%s/fsig.txt" resultdir in
    (*let stc_file = P.sprintf "%s_static.txt" elf_name in*)
    let cmd1 = 
      P.sprintf "./dbg/collect_func_type_sig %s > %s" elf.fname fsig_file
    in
    (*
    let cmd2 =
      P.sprintf "./dbg/collect_label %s > %s" elf.fname lbl_file 
    in
    let cmd3 = 
      P.sprintf "./dbg/collect_tail_call_site %s > %s" elf.fname tcs_file 
    in
    let cmd4 = 
      P.sprintf "./grep_possible_entries.py %s" elf.fname
    in
    let cmd5 = 
      P.sprintf "./dbg/collect_static_funcs %s > %s" elf.fname stc_file
    in
    *)
    let () = progress_event "Collecting function type signatures..." in
    if not (Sys.file_exists fsig_file) then (
      let _ = Sys.command cmd1 in
      ()
    );
    let memmap_file = P.sprintf "%s/struct_memmap.txt" resultdir in
    let cmd2 = 
      P.sprintf "./dbg/collect_struct_memmap %s > %s" elf.fname memmap_file
    in
    if not (Sys.file_exists memmap_file) then (
      let _ = Sys.command cmd2 in
      ()
    );
    let globaltype_file = P.sprintf "%s/global_typeinfo.txt" resultdir in
    let cmd3 = 
      P.sprintf "./dbg/collect_global_typeinfo %s > %s" elf.fname globaltype_file
    in
    if not (Sys.file_exists globaltype_file) then (
      let _ = Sys.command cmd3 in
      ()
    );
    (*
    let () = progress_event "Collecting labels..." in
    let _ = Sys.command cmd2 in
    let () = progress_event "Collecting tail call sites..." in
    let _ = Sys.command cmd3 in
    *)
    precollect_visible_typeinfo elf;
    ()

(*
    if !rel_on then (
      let () = progress_event "Collecting relocation information..." in
      let _ = Sys.command cmd4 in
      let _ = Sys.command cmd5 in
      ()
    )*)

(*
let print_bit_vector_op bvop = match bvop with
    X86_RTL.Coq_add_op -> print_string "Add";
  | X86_RTL.Coq_sub_op -> print_string "Sub";
  | X86_RTL.Coq_mul_op -> print_string "Mul";
  | X86_RTL.Coq_divs_op -> print_string "Divs";
  | X86_RTL.Coq_divu_op -> print_string "Divu";
  | X86_RTL.Coq_modu_op -> print_string "Modu";
  | X86_RTL.Coq_mods_op -> print_string "Mods";
  | X86_RTL.Coq_and_op -> print_string "And";
  | X86_RTL.Coq_or_op -> print_string "Or";
  | X86_RTL.Coq_xor_op -> print_string "Xor";
  | X86_RTL.Coq_shl_op -> print_string "Shl";
  | X86_RTL.Coq_shr_op -> print_string "Shr";
  | X86_RTL.Coq_shru_op -> print_string "Shru";
  | X86_RTL.Coq_ror_op -> print_string "Ror";
  | X86_RTL.Coq_rol_op -> print_string "Rol"

let print_test_op top = match top with
    X86_RTL.Coq_eq_op -> print_string "Eq";
  | X86_RTL.Coq_lt_op -> print_string "Lt";
  | X86_RTL.Coq_ltu_op -> print_string "Ltu"

let print_value v = 
    let str = to_string v in
    print_string str

let print_register reg = match reg with 
    EAX -> print_string "EAX";
  | ECX -> print_string "ECX";
  | EDX -> print_string "EDX";
  | EBX -> print_string "EBX";
  | ESP -> print_string "ESP";
  | EBP -> print_string "EBP";
  | ESI -> print_string "ESI";
  | EDI -> print_string "EDI"

let print_segment_register segreg= match segreg with
    ES -> print_string "ES";
  | CS -> print_string "CS";
  | SS -> print_string "SS";
  | DS -> print_string "DS";
  | FS -> print_string "FS";
  | GS -> print_string "GS"

let print_flag f = match f with
    X86_MACHINE.ID -> print_string "ID";
  | X86_MACHINE.VIP -> print_string "VIP";
  | X86_MACHINE.VIF -> print_string "VIF";
  | X86_MACHINE.AC -> print_string "AC";
  | X86_MACHINE.VM -> print_string "VM";
  | X86_MACHINE.RF -> print_string "RF";
  | X86_MACHINE.NT -> print_string "NT";
  | X86_MACHINE.IOPL -> print_string "IOPL";
  | X86_MACHINE.OF -> print_string "OF";
  | X86_MACHINE.DF -> print_string "DF";
  | X86_MACHINE.IF_flag -> print_string "IF_flag";
  | X86_MACHINE.TF -> print_string "TF";
  | X86_MACHINE.SF -> print_string "SF";
  | X86_MACHINE.ZF -> print_string "ZF";
  | X86_MACHINE.AF -> print_string "AF";
  | X86_MACHINE.PF -> print_string "PF";
  | X86_MACHINE.CF -> print_string "CF"


let print_control_register ctrlseg = match ctrlseg with
    CR0 -> print_string "CR0";
  | CR2 -> print_string "CR2";
  | CR3 -> print_string "CR3";
  | CR4 -> print_string "CR4"

let print_debug_register dbgreg = match dbgreg with
    DR0 -> print_string "DR0";
  | DR1 -> print_string "DR1";
  | DR2 -> print_string "DR2";
  | DR3 -> print_string "DR3";
  | DR6 -> print_string "DR6";
  | DR7 -> print_string "DR7"

let print_fpu_flag ff = match ff with
    X86_MACHINE.F_Busy -> print_string "F_Busy";
  | X86_MACHINE.F_C3 -> print_string "F_C3";
  | X86_MACHINE.F_C2 -> print_string "F_C2";
  | X86_MACHINE.F_C1 -> print_string "F_C1";
  | X86_MACHINE.F_C0 -> print_string "F_C0";
  | X86_MACHINE.F_ES -> print_string "F_ES";
  | X86_MACHINE.F_SF -> print_string "F_SF";
  | X86_MACHINE.F_PE -> print_string "F_PE";
  | X86_MACHINE.F_UE -> print_string "F_UE";
  | X86_MACHINE.F_OE -> print_string "F_OE";
  | X86_MACHINE.F_ZE -> print_string "F_ZE";
  | X86_MACHINE.F_DE -> print_string "F_DE";
  | X86_MACHINE.F_IE -> print_string "F_IE"

let print_fpu_ctrl_flag fcf = match fcf with
    X86_MACHINE.F_Res15 -> print_string "F_Res15";
  | X86_MACHINE.F_Res14 -> print_string "F_Res14";
  | X86_MACHINE.F_Res13 -> print_string "F_Res13";
  | X86_MACHINE.F_Res7 -> print_string "F_Res7";
  | X86_MACHINE.F_Res6 -> print_string "F_Res6";
  | X86_MACHINE.F_IC -> print_string "F_IC";
  | X86_MACHINE.F_PM -> print_string "F_PM";
  | X86_MACHINE.F_UM -> print_string "F_UM";
  | X86_MACHINE.F_OM -> print_string "F_OM";
  | X86_MACHINE.F_ZM -> print_string "F_ZM";
  | X86_MACHINE.F_DM -> print_string "F_DM";
  | X86_MACHINE.F_IM -> print_string "F_IM"

let print_location loc = match loc with
    X86_MACHINE.Coq_reg_loc(reg) -> print_register reg;
  | X86_MACHINE.Coq_seg_reg_start_loc(segreg) -> print_string "Start"; print_segment_register segreg;
  | X86_MACHINE.Coq_seg_reg_limit_loc(segreg) -> print_string "Limit"; print_segment_register segreg;
  | X86_MACHINE.Coq_flag_loc(f) -> print_flag f;
  | X86_MACHINE.Coq_control_register_loc(ctrlreg) -> print_control_register ctrlreg;
  | X86_MACHINE.Coq_debug_register_loc (dbgreg) -> print_debug_register dbgreg;
  | X86_MACHINE.Coq_pc_loc -> print_string "Pc";
  | X86_MACHINE.Coq_fpu_stktop_loc -> print_string "Fpu_stktop";
  | X86_MACHINE.Coq_fpu_flag_loc(ff) -> print_fpu_flag ff;
  | X86_MACHINE.Coq_fpu_rctrl_loc -> print_string "Fpu_rctrl";
  | X86_MACHINE.Coq_fpu_pctrl_loc -> print_string "Fpu_pctrl";
  | X86_MACHINE.Coq_fpu_ctrl_flag_loc(fcf) -> print_fpu_ctrl_flag fcf;
  | X86_MACHINE.Coq_fpu_lastInstrPtr_loc -> print_string "Fpu_lastInstrPtr";
  | X86_MACHINE.Coq_fpu_lastDataPtr_loc -> print_string "Fpu_lastDataPtr";
  | X86_MACHINE.Coq_fpu_lastOpcode_loc -> print_string "Fpu_lastOpcode"

let print_array arr = match arr with
  | X86_MACHINE.Coq_fpu_datareg -> print_string "Fpu_datareg";
  | X86_MACHINE.Coq_fpu_tag -> print_string "Fpu_tag"

let print_float_arith_op faop = match faop with
    X86_RTL.Coq_fadd_op -> print_string "Fadd";
  | X86_RTL.Coq_fsub_op -> print_string "Fsub";
  | X86_RTL.Coq_fmul_op -> print_string "Fmul";
  | X86_RTL.Coq_fdiv_op -> print_string "Fdiv"


let rec print_rtl_exp rtl_e = match rtl_e with
    X86_RTL.Coq_arith_rtl_exp(s, bvop, rtl_e1, rtl_e2) ->
      print_string "Arith(";
      print_size s;
      print_string ", ";
      print_bit_vector_op bvop;
      print_string ", ";
      print_rtl_exp rtl_e1;
      print_string ", ";
      print_rtl_exp rtl_e2;
      print_string ")";
  | X86_RTL.Coq_test_rtl_exp(s, top, rtl_e1, rtl_e2) ->
      print_string "Test(";
      print_size s;
      print_string ", ";
      print_test_op top;
      print_string ", ";
      print_rtl_exp rtl_e1;
      print_string ", ";
      print_rtl_exp rtl_e2;
      print_string ")";
  | X86_RTL.Coq_if_rtl_exp(s, rtl_e1, rtl_e2, rtl_e3) -> 
      print_string "If(";
      print_size s;
      print_string ", ";
      print_rtl_exp rtl_e1;
      print_string ", ";
      print_rtl_exp rtl_e2;
      print_string ", ";
      print_rtl_exp rtl_e3;
      print_string ")";
  | X86_RTL.Coq_cast_s_rtl_exp(s1, s2, rtl_e') ->
      print_string "Cast_s(";
      print_size s1;
      print_string ", ";
      print_size s2;
      print_string ", ";
      print_rtl_exp rtl_e';
      print_string ")";
  | X86_RTL.Coq_cast_u_rtl_exp(s1, s2, rtl_e') ->
      print_string "Cast_u(";
      print_size s1;
      print_string ", ";
      print_size s2;
      print_string ", ";
      print_rtl_exp rtl_e';
      print_string ")";
  | X86_RTL.Coq_imm_rtl_exp(s, v) ->
      print_string "Imm(";
      print_size s;
      print_string ", ";
      print_value v;
      print_string ")";
  | X86_RTL.Coq_get_loc_rtl_exp(s, loc) ->
      print_string "Get_loc(";
      print_size s;
      print_string ", ";
      print_location loc;
      print_string ")";
  | X86_RTL.Coq_get_array_rtl_exp(s1, s2, arr, rtl_e') -> 
      print_string "Get_array(";
      print_size s1;
      print_string ", ";
      print_size s2;
      print_string ", ";
      print_array arr;
      print_string ", ";
      print_rtl_exp rtl_e';
      print_string ")";
  | X86_RTL.Coq_get_byte_rtl_exp(rtl_e') -> 
      print_string "Get_byte(";
      print_rtl_exp rtl_e';
      print_string ")";
  | X86_RTL.Coq_get_random_rtl_exp(s) -> 
      print_string "Get_random(";
      print_size s;
      print_string ")";
  | X86_RTL.Coq_farith_rtl_exp(s1, s2, faop, rtl_e1, rtl_e2, rtl_e3) -> 
      print_string "Farith(";
      print_size s1;
      print_string ", ";
      print_size s2;
      print_string ", ";
      print_float_arith_op faop;
      print_string ", ";
      print_rtl_exp rtl_e1;
      print_string ", ";
      print_rtl_exp rtl_e2;
      print_string ", ";
      print_rtl_exp rtl_e3;
      print_string ")";
  | X86_RTL.Coq_fcast_rtl_exp(s1, s2, s3, s4, rtl_e1, rtl_e2) -> 
      print_string "Fcast(";
      print_size s1;
      print_string ", ";
      print_size s2;
      print_string ", ";
      print_size s3;
      print_string ", ";
      print_size s4;
      print_string ", ";
      print_rtl_exp rtl_e1;
      print_string ", ";
      print_rtl_exp rtl_e2;
      print_string ")"



let rec print_rtl_instr rtl_i = match rtl_i with
    X86_RTL.Coq_set_loc_rtl(s, rtl_e, loc) ->
      print_string "Set_loc \t";
      print_size s;
      print_string ", ";
      print_rtl_exp rtl_e;
      print_string ", ";
      print_location loc;
      print_string "\n\n";
  | X86_RTL.Coq_set_array_rtl(s1, s2, arr, rtl_e1, rtl_e2) -> 
      print_string "Set_array \t";
      print_size s1;
      print_string ", ";
      print_size s2;
      print_string ", ";
      print_array arr;
      print_string ", ";
      print_rtl_exp rtl_e1;
      print_string ", ";
      print_rtl_exp rtl_e2;
      print_string "\n\n";
  | X86_RTL.Coq_set_byte_rtl(rtl_e1, rtl_e2) ->
      print_string "Set_byte \t";
      print_rtl_exp rtl_e1;
      print_string ", ";
      print_rtl_exp rtl_e2;
      print_string "\n\n";
  | X86_RTL.Coq_advance_oracle_rtl ->
      print_string "Advance_oracle \t";
      print_string "\n\n";
  | X86_RTL.Coq_if_rtl(rtl_e, rtl_i') -> 
      print_string "If \t";
      print_rtl_exp rtl_e;
      print_string ", ";
      print_rtl_instr rtl_i';
  | X86_RTL.Coq_error_rtl ->
      print_string "Error \t";
      print_string "\n\n";
  | X86_RTL.Coq_trap_rtl ->
      print_string "Trap \t";
      print_string "\n\n"


let print_rb rb = 
  print_string "{\n";
  print_string "rb_ID = ";
  print_int rb.rb_ID;
  print_string "\n";
  print_string "rb_OID = ";
  print_string rb.rb_OID;
  print_string "\n";
  print_string "rb_symbol_label = ";
  print_string rb.rb_symbol_label;
  print_string "\n";
  print_string "rb_instrs = \n";
  List.iter print_rtl_instr rb.rb_instrs;
  print_string "rb_relAddr = ";
  print_string (to_string rb.rb_relAddr);
  print_string "\n";
  print_string "rb_pred = \n";
  List.iter (fun id -> print_int id; print_string " ") rb.rb_pred;
  print_string "\n";
  print_string "rb_instrmap = \n";
  List.iter (fun index -> print_int index; print_string " ") rb.rb_instrmap;
  print_string "\n";
  print_string "}\n\n"

let dump_rcfg cfg = 
  IDMap.iter (fun id rb -> print_rb rb) cfg

let print_ab oc ab =
  let abi_to_bbi (pre, ins, l) =
    (pre, ins, Big.to_int l)
  in
  fprintf oc "{\n";
  fprintf oc "ab_ID = %d\n" ab.ab_ID;
  fprintf oc "ab_OID = %s\n" ab.ab_OID;
  fprintf oc "ab_symbol_label = %s\n" ab.ab_symbol_label;
  fprintf oc "ab_instrs = \n";
  (*List.iter print_rtl_instr rb.rb_instrs;*)
  List.iter 
    (fun (loc, pre, ins, len) -> 
      fprintf oc " %s %d\t%s\t%s\n" (str_of_mword_flex (MWord.of_int loc)) len (str_of_prefix (pre,ins)) (str_of_instr (pre,ins));
      )  (instrs_with_starts (Big.to_int ab.ab_relAddr) (List.map abi_to_bbi ab.ab_instrs));	
  fprintf oc "ab_relAddr = %s\n" (str_of_mword_flex (MWord.of_int (Big.to_int ab.ab_relAddr)));
  fprintf oc "ab_succ = \n";
  List.iter (fun id -> fprintf oc "%d " id) ab.ab_succ;
  fprintf oc "\n";
  fprintf oc "ab_inter_succ = \n";
  List.iter (fun id -> fprintf oc "%d " id) ab.ab_inter_succ;
  fprintf oc "\n";
  fprintf oc "ab_pred = \n";
  List.iter (fun id -> fprintf oc "%d " id) ab.ab_pred;
  fprintf oc "\n";
  fprintf oc "}\n\n";
  flush oc

let dump_acfg oc acfg =
  IDMap.iter (fun id ab -> print_ab oc ab ) acfg
*)

let multi_layer_dump cr_lst =
  let icnum = num_of_IC cr_lst in
  let ictnum = num_of_ICT cr_lst in
  P.printf "Number of Indirect Calls: %d\n" icnum;
  P.printf "Number of Indirect Call Targets: %d\n" ictnum;
  P.printf "Average of Indirect Call Targets: %f\n" ((float_of_int ictnum) /. (float_of_int icnum))

let dump_statistics cr_lst =
(*
  P.printf "Number of Blocks: %d\n" (num_of_blocks cr_lst);
  P.printf "Average Number of Out Edges: %f\n" (average_out_edges cr_lst);
  P.printf "Average Number of In Edges: %f\n" (average_in_edges cr_lst);
  (match (max_out_edges cr_lst) with
  | None -> 
    P.printf "No block in code region!\n";
  | Some (i',i) -> 
    P.printf "Maximum Number of Out Edges: %d\n" i;
    P.printf "Maximum Out Edge Basic Block Starts at %x\n" i'
  );
  (match (max_in_edges cr_lst) with
  | None -> 
    P.printf "No block in code region!\n";
  | Some (i',i) ->
    P.printf "Maximum Number of In Edges: %d\n" i;
    P.printf "Maximum In Edge Basic Block Starts at %x\n" i'
  );*)
  let ibnum = num_of_IB cr_lst in
  let ibtnum = num_of_IBT cr_lst in
  P.printf "Number of Indirect Branches: %d\n" ibnum;
  P.printf "Number of Indirect Branch Targets: %d\n" ibtnum;
  P.printf "Average of Indirect Branch Targets: %f\n" ((float_of_int ibtnum) /. (float_of_int ibnum));
  let icnum = num_of_IC cr_lst in
  let ictnum = num_of_ICT cr_lst in
  P.printf "Number of Indirect Calls: %d\n" icnum;
  P.printf "Number of Indirect Call Targets: %d\n" ictnum;
  P.printf "Average of Indirect Call Targets: %f\n" ((float_of_int ictnum) /. (float_of_int icnum));
  let ijnum = num_of_IJ cr_lst in
  let ijtnum = num_of_IJT cr_lst in
  P.printf "Number of Indirect Jumps: %d\n" ijnum;
  P.printf "Number of Indirect Jump Targets: %d\n" ijtnum;
  P.printf "Average of Indirect Jump Targets: %f\n" ((float_of_int ijtnum) /. (float_of_int ijnum))

let dump_mcfi_statistics cr_lst =
  let ibnum = num_of_mcfi_IB cr_lst in
  let ibtnum = num_of_mcfi_IBT cr_lst in
  P.printf "Number of Instrumentable Indirect Branches: %d\n" ibnum;
  P.printf "Number of Instrumentable Indirect Branch Targets: %d\n" ibtnum;
  P.printf "Average of Instrumentable Indirect Branch Targets: %f\n" ((float_of_int ibtnum) /. (float_of_int ibnum));
  P.printf "Average of EQC-merged Indirect Branch Targets: %f\n" (avg_ibt_after_mcfi_eqc_merge cr_lst)


let rec range a b =
  if a > b then []
  else a :: range (a+1) b

let rec ijclassify_global elf op branch_site start all_functions = 
  let local_mword_addr mword_addr =
    let func_ranges = collect_func_ranges_from_symtab elf start in
    let visible_fs, visible_fe = 
      List.find (fun (fs, fe) -> fs <= branch_site && branch_site <= fe) func_ranges
    in
    let fs_mword = MWord.(+%) (MWord.of_int visible_fs) start in
    let fe_mword = MWord.(+%) (MWord.of_int visible_fe) start in
    MWord.(<=%) fs_mword mword_addr && MWord.(<=%) mword_addr fe_mword
  in
  let func_mword_addr mword_addr =
    List.exists (fun (faddr, fname) -> 
      mword_addr = (MWord.(+%) (MWord.of_int faddr) start)
    ) all_functions 
  in
  if reladdr_in_plt elf branch_site start then IJ_plt_t
  else match op with
  | Imm_op _ -> (** Impossible Case **)
    raise (Failure "Direct Jump found during IJClassification!")
  | Offset_op off ->
    let sim_addr = {addrDisp = off; addrBase = None; addrIndex = None} in
    ijclassify_global elf (Address_op sim_addr) branch_site start all_functions
  | Address_op addr -> (
	(** Note that we assume plt, tail call, jump table, and label are the 
	    only source of indirect jump
	**)
    match addr with
    | {addrDisp = baseAddr; addrBase = _; addrIndex = _} -> 
      if addrop_in_rodata elf addr then ( 
	let target_mword = read_codeseg_for_target elf addr start in
	if local_mword_addr target_mword then IJ_local_t
	else if func_mword_addr target_mword then IJ_function_t
	else IJ_unclassified_t
      ) 
      else if addrop_in_data elf addr then (
	let target_mword = read_dataseg_for_target elf addr start in
	if local_mword_addr target_mword then IJ_local_t
	else if func_mword_addr target_mword then IJ_function_t
	else IJ_unclassified_t
      )
      else IJ_unclassified_t
  )
  | _ -> IJ_unclassified_t











(*
***************
Code Trashbin:
***************

let rec read_code_for_target elf addr start = 
  let addrDisp = addr.addrDisp in
  let rodata_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".rodata") elf.shdr_tab) in
  let rodata_start = rodata_shdr.sh_addr in
  let rodata_size = rodata_shdr.sh_size in
  let rodata_end = MWord.(+%) rodata_start rodata_size in
  let read_code_seg phdr = 
    if is_code_seg phdr then
      let code = file_read_region elf.fname (MWord.to_int phdr.p_offset)
	(MWord.to_int phdr.p_filesz) in
      Some (code, phdr.p_vaddr)
    else None
  in
  let op_addr_mword = MWord.of_big_int (unsigned32 addrDisp) in
  let code,code_start = List.hd (List.filter_map read_code_seg elf.phdr_tab) in
  let op_rel_addr = get_relative_addr op_addr_mword code_start in
  let code_size = Array1.dim code in
  if op_rel_addr < 0 || op_rel_addr > code_size then
    let () = 
      print_endline "Error: operand address is illegal!";
      P.printf "Illegal address is %s\n" (str_of_mword_flex op_addr_mword);
    in
    []
  else
    (* read 4 bytes from op_rel_addr to form an absolute address
       then get_relative address *)
    let target_address = form_abs_addr code op_rel_addr in
    let rel_target_address = get_relative_addr target_address start in
    let next_addr = {addrDisp = (Big.add_int 4 addr.addrDisp); 
		     addrBase = None;
		     addrIndex = None;} 
    in
    if op_rel_addr > get_relative_addr (MWord.(-%) rodata_end (MWord.of_int 4)) start then []
    else if MWord.to_int target_address = 0 then []
    else if is_label elf rel_target_address start then []
    else rel_target_address :: (read_code_for_target elf next_addr start)
      
let read_code_for_targets elf op start =
  match op with
  | Address_op addr -> 
    read_code_for_target elf addr start
  | _ -> []

let read_got_plt_for_target elf op start =
  match op with
  | Address_op addr ->
    read_dataseg_for_target elf addr start
  | _ -> 
    let () = E.warn "Detected .plt indirect jump operand is not an address! Give up!\n" in
    []


let read_code_to_classify elf op start = 
  match op with
  | Address_op addr ->
    let addrDisp = addr.addrDisp in
    let read_code_seg phdr = 
      if is_code_seg phdr then
	let code = file_read_region elf.fname (MWord.to_int phdr.p_offset)
	  (MWord.to_int phdr.p_filesz) in
	Some (code, phdr.p_vaddr)
      else None
    in
    let op_addr_mword = MWord.of_big_int (unsigned32 addrDisp) in
    let code,code_start = List.hd (List.filter_map read_code_seg elf.phdr_tab) in
    let op_rel_addr = get_relative_addr op_addr_mword code_start in
    let code_size = Array1.dim code in
    if op_rel_addr < 0 || op_rel_addr > code_size then
    let () = 
      print_endline "Error: operand address is illegal!";
      P.printf "Illegal address is %s\n" (str_of_mword_flex op_addr_mword);
    in
    []
  else
    (* read 4 bytes from op_rel_addr to form an absolute address
       then get_relative address *)
    let target_address = form_abs_addr code op_rel_addr in
    if MWord.to_int target_address = 0 then []
    else [get_relative_addr target_address start]
  | _ -> 
    let () = E.warn "Switch indirect jump operand is not an address! Give up!\n" in
    []


*)

type c_type = 
| (*0*) C_base of string (* like int, float including void *) 
| (*1*) C_const of c_type
| (*2*) C_array of c_type * (int list) (* int list for dimension *)
| (*3*) C_pointer of c_type
| (*4*) C_typedef of string * c_type (* string for typedef name *)
| (*5*) C_subroutine of c_type * (c_type list) (* first: return type, second: parameter type list *)
| (*6*) C_enumeration of string * ((string * int) list) (* enum_name, {enumerator_name, enumerator_value} *)
| (*7*) C_union of string * ((c_type * string) list) (* union_name, {field_type, field_name} *)
| (*8*) C_structure of string * string * ((c_type * string) list) (* struct_name, new_struct_name, {field_type, field_name} *)
| (*9*) C_unspecified_param
| (*a*) C_wrong_type of string (* including an error message *)
| (*b*) C_volatile of c_type
*)
