open Batteries
module P = Printf
module H = Hashtbl

open X86Syntax
open X86Semantics
open Bits
open Elf
open Config
open BB
   
let validate_cfg elf bbtbl =
  let legitimate_icalls = H.create 32 in
  H.iter (fun lbl bb ->
      if BB.does_bb_icall bb then
        let _,_,len = List.last bb.bb_instrs in
        let caller_str =
          P.sprintf "0x%x" 
            (bb.bb_size + (int_of_string ("0x"^bb.bb_label)) - len)
        in
        List.iter (fun succ ->
            let callee_str = "0x"^succ in
            H.add legitimate_icalls (caller_str, callee_str) ()
          ) bb.bb_succs;
    ) bbtbl;
  
  let runtime_patterns = H.create 32 in
  let target = elf.fname ^ ".pinout" in
  let ic = open_in target in
  let violations = ref [] in
  begin try
      while true do
        let line = input_line ic in
        let caller, callee = String.split line " " in
        if not (H.mem legitimate_icalls (caller, callee)) then
          violations := (caller,callee) :: !violations
      done
    with End_of_file ->
      close_in ic
  end;
  if !violations = [] then
    P.printf "CFG is validated with runtime data!\n"
  else (
    P.printf "CFG is unsound; unsound cases are:\n";
    List.iter (fun (caller, callee) ->
        P.printf "%s %s\n" caller callee
      ) !violations;
    assert false
  )
    
