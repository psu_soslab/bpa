open Batteries
open Printf
module H = Hashtbl

open Elf
open Config
open X86Semantics

open BB
open Ibaux
open Config
open Advdis
open Config_cfg
open PostStat

open TyInfoRetrieval
open TypeEngineLib
open TypeEngine
open CFGrefine

let cfg_m_on = ref false

let init_eqcg = ref false
 
let fini_eqcg = ref false

let find_registers relist =
  let rec search_reg re =
    match re with
    | X86_RTL.Coq_arith_rtl_exp(_,_,re1,re2)
    | X86_RTL.Coq_test_rtl_exp(_,_,re1,re2) ->
      search_reg re1 @ search_reg re2
    | X86_RTL.Coq_if_rtl_exp(_,ge,te,fe) ->
      search_reg te @ search_reg fe
    | X86_RTL.Coq_cast_s_rtl_exp(_,_,re)
    | X86_RTL.Coq_cast_u_rtl_exp(_,_,re) ->
      search_reg re
    | X86_RTL.Coq_get_loc_rtl_exp(_,loc) ->
      begin match loc with
      | X86_MACHINE.Coq_reg_loc reg ->
	[reg]
      | _ -> []
      end
    | X86_RTL.Coq_imm_rtl_exp _ 
    | X86_RTL.Coq_get_array_rtl_exp _ 
    | X86_RTL.Coq_get_byte_rtl_exp _
    | X86_RTL.Coq_get_random_rtl_exp _ -> []
    | _ -> []
  in
  List.unique (List.flatten (List.map (fun (lv,re) ->
    if lv = 0 then
      search_reg re
    else []
  ) relist ))

let cons_stkptr_map stkptr_map cr bbtbl funCFA =
  List.iter (fun (reladdr, exprlist) ->
    let absaddr = MWord.(+%) (MWord.of_int reladdr) cr.startAddr in
    let absaddr_str = str_of_mword_flex absaddr in
    (*let exepnt = locate_sensi_pnt bbtbl absaddr_str in*)
    let stkptrs = find_registers exprlist in
    H.add stkptr_map absaddr_str stkptrs
  ) funCFA
  
let base_cfggen elf =
  (** Base CFG generation **)
  let cfainfo = H.create 32 in
  let cr_list = Stats.time ("Recursive disassembly") Dis.rt_dis_cfg elf in
  let bbtbl = bbset_to_bbtbl cr_list in
  List.iter (fun cr ->
    let asm_fun_list = collect_asm_func elf cr in
    List.iter (fun asm_fun ->
      let funcfa = Stats.time ("CFA generation")
	MustPntToStk.gen_must_CFA asm_fun
      in
      MustPntToStk.dump_CFA funcfa;
      Stats.time ("STKPTR_Map Construction") (cons_stkptr_map cfainfo cr bbtbl) funcfa
    ) asm_fun_list;
    Stats.time ("Configure ret") (config_ret bbtbl) asm_fun_list;
  ) cr_list;
  cfainfo,bbtbl

let enhanced_cfggen elf =
  (** Enhanced CFG generation **)

  (** retrieve and interpret DBG info **)
  let structs = retrieve_namemap elf.fname in
  let raw_gti = retrieve_gti elf.fname in
  let union_map,struct_map,typedef_map = cons_namety_hashtbl structs in
  let interp_gti = process_raw_gti raw_gti in
  let effi_global_map = cons_effi_map typedef_map struct_map interp_gti in
  check_effi_map effi_global_map;
  let raw_funsig_info = retrieve_funsig_info elf.fname in
  (*************************************)

  let cr_list =
    Stats.time ("Recursive disassembly") Dis.rt_dis_cfg elf
  in

  let cfainfo = H.create 32 in
  let bbtbl = bbset_to_bbtbl cr_list in
  let target_cr_list = List.filter (fun cr ->
    not (BBSet.is_empty cr.bbs)
  ) cr_list
  in
  List.iter (fun cr -> 
    (** Possibly multiple code regions with 
	different starting address **)
    let start = cr.startAddr in
    let compiler_funcs = (** compiler-generated functions **)
      [
	"deregister_tm_clones"; 
	"register_tm_clones"; 
	"frame_dummy"; 
	"_start";
	"__x86.get_pc_thunk.bx";   
	"_init";
	"_fini";
	"__do_global_dtors_aux";
	"__libc_csu_init";
	"__libc_csu_fini";
      ]
    in
    let dynamic_linkers = (** dynamic library linkers **)
      List.map snd (Ibaux.collect_library_functions elf start)
    in
    let ignored_functions = (** functions we do not consider **)
      dynamic_linkers @ compiler_funcs 
    in
    let funsig_info = map_libfun_addr raw_funsig_info elf start in 
    let funsig_map = cons_funsig_map funsig_info in
    let taken_funsig_map = cons_taken_funsig_map elf funsig_info start in
    let jmptbl_addrs = Stats.time ("Jump Table Construction") 
      (cons_jmptbl_map elf) start 
    in
    let asm_fun_list = collect_asm_func elf cr in
    List.iter (fun asm_fun ->
      let funCFA = Stats.time ("CFA generation")
	MustPntToStk.gen_must_CFA asm_fun
      in
      Stats.time ("STKPTR_Map Construction") (cons_stkptr_map cfainfo cr bbtbl) funCFA; 
    ) asm_fun_list;
    let target_functions = 
      List.filter (fun asm_fun -> 
	not (List.mem asm_fun.func_name ignored_functions)
	&& (
	  (icall_exists asm_fun) || (ijump_exists asm_fun)
	)
      ) asm_fun_list
    in
    List.iter (fun asm_fun ->
      let funCFA = Stats.time ("CFA generation")
	MustPntToStk.gen_must_CFA asm_fun
      in
      (*dump_cfa_repr 1 funCFA;*)
      let framebase, raw_pti = Stats.time ("Procedural info retrieval") 
	(retrieve_pinfo elf.fname) asm_fun.func_low in
      let interp_pti = process_raw_pti framebase funCFA raw_pti in
      let effi_fun_map =
	Stats.time ("Efficient procedural info")
	  (cons_effi_map typedef_map struct_map) 
	  interp_pti
      in
      check_effi_map effi_fun_map;
      let ultim_map = 
	(effi_fun_map, effi_global_map, jmptbl_addrs, funsig_map) 
      in
      (** EQuivalent Class Graph: (varmap, nodes)
	  varmap: (Type variable, Node label) Hashtbl
	  nodes: (Node label, Node) Hashtbl
	  
	  Detailed definition can be found in 
	  src/cfggen/TypeInfer/TypeEngineLib.ml **)
      let varmap = H.create 32 in
      let nodes = H.create 32 in
      let eqcg = (varmap,nodes) in
      Stats.time ("Graph generation") (func_eqcgGen asm_fun funCFA ultim_map) eqcg;
      if !init_eqcg then (
	P.printf "Printing out initial EQCG...\n";
	dump_eqcg eqcg
      );
      Stats.time ("Solving process") (type_solve_full_wl typedef_map struct_map ultim_map) eqcg;
      if !fini_eqcg then (
	P.printf "Printing out solved EQCG...\n";
	dump_eqcg eqcg
      );
      flush stdout;
      (*icij_checking typedef_map struct_map funCFA ultim_map asm_fun eqcg;
      dump_checking_stat ();*)
      let ultim_map = (effi_fun_map, effi_global_map, jmptbl_addrs, taken_funsig_map) in
      Stats.time ("Refining CFG") 
	(refine_cfg_within_fun typedef_map struct_map union_map funCFA ultim_map asm_fun start) 
	eqcg;
      ()
    ) target_functions;
    Stats.time ("Configure ret") (config_ret bbtbl) asm_fun_list;
  ) target_cr_list;
  cfainfo, bbtbl

let marshal_content content filename =
  let moc = open_out_bin filename in
  Marshal.output moc content;
  close_out moc

type metacfg = 
  (string, (X86Syntax.register list)) H.t *
    (string, basicblock) H.t

let demarshal_cfg filename =
  let mic = open_in_bin filename in
  let cfg = (Marshal.input mic : metacfg) in
  close_in mic;
  cfg

let cfggen baseCFG_on elf =
  let cfgfile = 
    if baseCFG_on then elf.fname^"_base.cfg"
    else elf.fname^".cfg"
  in
  if Sys.file_exists cfgfile then
    demarshal_cfg cfgfile
  else
    let gened_cfg =
      if baseCFG_on then (
	base_cfggen elf
      ) else (
	enhanced_cfggen elf
      )
    in
    Stats.time ("CFG predecessor configuration") (config_preds_in_bbtbl (snd gened_cfg)) [];
    if !cfg_m_on then 
      marshal_content gened_cfg cfgfile;
    gened_cfg

  
