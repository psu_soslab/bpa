open X86Syntax
open X86Semantics
open RTL
open RTL_opt
module R=X86_RTL

let no_if_trap ri =
  match ri with
  | R.Coq_if_rtl(_,ri') ->
    ri' = R.Coq_trap_rtl
  | _ -> false

let rec trap_in ri =
  match ri with
  | X86_RTL.Coq_if_rtl(_, ri') ->
    trap_in ri'
  | X86_RTL.Coq_trap_rtl -> true
  | _ -> false

let rec error_in ri =
  match ri with
  | X86_RTL.Coq_if_rtl(_, ri') ->
    error_in ri'
  | X86_RTL.Coq_error_rtl -> true
  | _ -> false

let no_advance_oracle ri =
  match ri with
  | R.Coq_advance_oracle_rtl ->
    true
  | _ -> false
  
let ssa_filters =
  [no_if_trap; no_advance_oracle]

let filter_rtl_ssa filters rtl_list =
  let throw_away ri =
    List.fold_right (fun flt b -> flt ri || b) filters false
  in
  List.filter (fun rtl_instr -> 
    not (throw_away rtl_instr)
  ) rtl_list

let optimize_rtl_ssa filters simpri_methods simpre_methods rtl_list =
  let simplified_rtl_ssa = simplify_rtl simpri_methods simpre_methods rtl_list in
  let filtered_rtl_ssa = filter_rtl_ssa filters simplified_rtl_ssa in
  filtered_rtl_ssa

let optimize_rtl_all rtl_list =
  let simplified_rtl_ssa = simplify_rtl simpri_all_methods simpre_all_methods rtl_list in
  let filtered_rtl_ssa = filter_rtl_ssa ssa_filters simplified_rtl_ssa in
  filtered_rtl_ssa
