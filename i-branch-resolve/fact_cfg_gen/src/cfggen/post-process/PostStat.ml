open Batteries
module P = Printf
module H = Hashtbl

open BB
open Ibaux

open TypeEngineLib

(*open Config_cfg*)

let bbtbl_stat_report bbtbl =
  let ibnum = ref 0 in
  let ibtnum = ref 0 in
  let icnum = ref 0 in
  let ictnum = ref 0 in
  let ijnum = ref 0 in
  let ijtnum = ref 0 in
  H.iter (fun lbl bb ->
    if does_bb_icall bb then (
      ibnum := !ibnum + 1;
      ibtnum := !ibtnum + (List.length bb.bb_succs);
      icnum := !icnum + 1;
      ictnum := !ictnum + (List.length bb.bb_succs);
    ) else if does_bb_ijump bb then (
      ibnum := !ibnum + 1;
      ibtnum := !ibtnum + (List.length bb.bb_succs);
      ijnum := !ijnum + 1;
      ijtnum := !ijtnum + (List.length bb.bb_succs);
    ) else if does_bb_ret bb then (
      ibnum := !ibnum + 1;
      ibtnum := !ibtnum + (List.length bb.bb_succs);
    ) else ()
  ) bbtbl;
  P.printf "Number of Indirect Branches: %d\n" !ibnum;
  P.printf "Number of Indirect Branch Targets: %d\n" !ibtnum;
  P.printf "Average of Indirect Branch Targets: %f\n" ((float_of_int !ibtnum) /. (float_of_int !ibnum));
  P.printf "Number of Indirect Calls: %d\n" !icnum;
  P.printf "Number of Indirect Call Targets: %d\n" !ictnum;
  P.printf "Average of Indirect Call Targets: %f\n" ((float_of_int !ictnum) /. (float_of_int !icnum));
  P.printf "Number of Indirect Jumps: %d\n" !ijnum;
  P.printf "Number of Indirect Jump Targets: %d\n" !ijtnum;
  P.printf "Average of Indirect Jump Targets: %f\n" ((float_of_int !ijtnum) /. (float_of_int !ijnum))

(*
let inference_report () =
  let percent i1 i2 =
    100.0 *. (float_of_int i1) /. (float_of_int i2)
  in
  let ic_aprx = !ic_handled - !ic_from_dbg - !ic_from_inf in
  let ic_cmplr = !ic_total - !ic_handled in
  P.printf "# of ICALL: %d\n" !ic_total;
  P.printf "- # of ICALL from DBG: %d(%f%%)\n" !ic_from_dbg (percent !ic_from_dbg !ic_handled);
  P.printf "- # of ICALL from INF: %d(%f%%)\n" !ic_from_inf (percent !ic_from_inf !ic_handled);
  P.printf "- # of ICALL guess: %d(%f%%)\n" !ic_guess (percent !ic_guess !ic_handled);
  P.printf "- # of ICALL over-aprx: %d(%f%%)\n" ic_aprx (percent ic_aprx !ic_handled);
  P.printf "- # of ICALL cmplr: %d(%f%%)\n" ic_cmplr (percent ic_cmplr !ic_total);
  (*P.printf "--- # of ICALL cmplr from DBG: %d(%f%%)\n" 
    !ic_cmplr_dbg (percent !ic_cmplr_dbg !ic_cmplr);
  P.printf "--- # of ICALL cmplr from INF: %d(%f%%)\n" 
    !ic_cmplr_inf (percent !ic_cmplr_inf !ic_cmplr);*)
  P.printf "- AVG of ICALL type sigs: %f\n" 
    ((percent !ic_types (!ic_from_dbg + !ic_from_inf)) /. 100.0) ;
  let ij_aprx = !ij_handled - !ij_class - !ij_tcall in
  let ij_cmplr = !ij_total - !ij_handled - !ij_plt in
  P.printf "# of IJUMP: %d\n" !ij_total;
  P.printf "- # of IJUMP plt: %d(%f%%)\n" !ij_plt (percent !ij_plt !ij_handled);
  P.printf "- # of IJUMP classified: %d(%f%%)\n" !ij_class (percent !ij_class !ij_handled);
  P.printf "- # of IJUMP tail-call: %d(%f%%)\n" !ij_tcall (percent !ij_tcall !ij_handled);
  P.printf "--- # of TCALL from DBG: %d(%f%%)\n" !tc_from_dbg (percent !tc_from_dbg !ij_tcall);
  P.printf "--- # of TCALL from INF: %d(%f%%)\n" !tc_from_inf (percent !tc_from_inf !ij_tcall);
  P.printf "- # of IJUMP over-aprx: %d(%f%%)\n" ij_aprx (percent ij_aprx !ij_handled);
  P.printf "- # of IJUMP cmplr: %d(%f%%)\n" ij_cmplr (percent ij_cmplr !ij_total);
  (*P.printf "--- # of IJUMP cmplr classified: %d(%f%%)\n" 
    !ij_cmplr_class (percent !ij_cmplr_class !ij_cmplr);
  P.printf "--- # of IJUMP cmplr tail-call: %d(%f%%)\n" 
    !ij_cmplr_tcall (percent !ij_cmplr_tcall !ij_cmplr);*)
  P.printf "- # of IJUMP conflict: %d\n" !ij_conflict;
  let tv_aprx = !tv_total - !tv_from_dbg - !tv_from_inf in
  P.printf "# of TypeVar: %d\n" !tv_total;
  P.printf "- # of TypeVar from DBG: %d(%f%%)\n" !tv_from_dbg (percent !tv_from_dbg !tv_total);
  P.printf "- # of TypeVar from INF: %d(%f%%)\n" !tv_from_inf (percent !tv_from_inf !tv_total);
  P.printf "- # of TypeVar over-aprx: %d(%f%%)\n" tv_aprx (percent tv_aprx !tv_total)
*)
