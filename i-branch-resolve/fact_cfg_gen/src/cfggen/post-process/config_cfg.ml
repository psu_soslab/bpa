open Batteries

open Printf
module P = Printf
module H = Hashtbl

open X86Syntax
open Elf
open Config
open BB
open Ibaux

let bbset_to_bbtbl cr_list =
  let bbtbl = H.create 32 in
  List.iter (fun cr ->
    BBSet.iter (fun bb ->
      H.add bbtbl bb.bb_label bb
    ) cr.bbs
  ) cr_list;
  bbtbl

  
let config_preds_in_bbtbl bbtbl exception_list =
  (*
  let exception_tbl = H.create 32 in
  List.iter (fun lbl ->
    H.add exception_tbl lbl ()
  ) exception_list;
  *)
  H.iter (fun lbl bb ->
    List.iter (fun succ_lbl ->
      if succ_lbl <> "ffffffff" then
	try
	  let succ_bb = H.find bbtbl succ_lbl in
	  succ_bb.bb_preds <- bb.bb_label :: succ_bb.bb_preds
	with Not_found -> ()
	  (*
	  let errmsg = P.sprintf "%s, SUCC of %s, does not exists!" succ_lbl lbl in
	  raise (Failure errmsg)
	  *)
    ) bb.bb_succs
  ) bbtbl

let config_ret bbtbl asm_fun_list =
  let fun_tbl = H.create 32 in  
  List.iter (fun asm_fun ->
    let ret_to_lib_funcs = 
      [
	"deregister_tm_clones"; 
	"register_tm_clones"; 
	"frame_dummy";
	"__libc_csu_init";
	"__libc_csu_fini";
	"main";
	"__do_global_dtors_aux";
	"_fini";
      ]
    in
    if List.mem asm_fun.func_name ret_to_lib_funcs then
      List.iter (fun bb ->
	if does_bb_ret bb then
	  bb.bb_succs <- ["ffffffff"]
      ) asm_fun.func_bbs;
    H.add fun_tbl asm_fun.func_low asm_fun;
  ) asm_fun_list;

  H.iter (fun lbl bb ->
    if does_bb_call bb then (
      assert (List.length bb.bb_inter_succs = 1);
      let visited_fun = ref [] in

      let rec rets_in_fun callsite target_fun =
	if target_fun.func_name <> "exit" && 
	  not (List.mem target_fun.func_name !visited_fun) 
	then (
	  visited_fun := target_fun.func_name :: !visited_fun;
	  List.iter (fun bb_in_target ->
	    if does_bb_ret bb_in_target then
	      bb_in_target.bb_succs <- callsite :: bb_in_target.bb_succs
	    else if does_bb_jump bb_in_target then
	      List.iter (fun jmp_target ->
		if jmp_target = "ffffffff" then (
		  (** jump into library **)
		  assert (H.mem bbtbl callsite);
		  let ret_to_bb = H.find bbtbl callsite in
		  ret_to_bb.bb_preds <- List.unique (jmp_target :: ret_to_bb.bb_preds)
		)
		else if H.mem fun_tbl jmp_target then
		  rets_in_fun callsite (H.find fun_tbl jmp_target)
		else ()
	      ) bb_in_target.bb_succs
	    else ()
	  ) target_fun.func_bbs
	)
      in
      
      let callsite_label = List.hd bb.bb_inter_succs in
      let callsite_bb = 
	try
	  H.find bbtbl callsite_label 
	with Not_found ->
	  raise (Failure "callsite bb not found!")
      in
      callsite_bb.bb_inter_preds <- [bb.bb_label];
      List.iter (fun succ_lbl ->
	(** found special cases in musl's static library
	    functions: calling to middle of functions **)
	if succ_lbl <> "ffffffff" then
	  let target_fun = 
	    try
	      H.find fun_tbl succ_lbl
	    with Not_found ->
	      let succ_pc = MWord.of_string ("0x"^succ_lbl) in
	      let old_fun = 
		try
		  List.find (fun asm_fun ->
		    let asm_fun_lopc = MWord.of_string ("0x"^asm_fun.func_low) in
		    let asm_fun_hipc = MWord.of_string ("0x"^asm_fun.func_high) in
		    MWord.(<%) asm_fun_lopc succ_pc && MWord.(<%) succ_pc asm_fun_hipc
		  ) asm_fun_list
		with Not_found ->
		 let errmsg = 
		   P.sprintf "CALL_BB %s targets at %s" lbl succ_lbl 
		 in
		 (** In this case, the target belongs to no predefined function **)
		 raise (Failure errmsg)
	      in
	      let new_fun_bbs =
		List.filter (fun bb ->
		  let abs_addr = MWord.of_string ("0x"^bb.bb_label) in
		  MWord.(<=%) succ_pc abs_addr
		) old_fun.func_bbs
	      in
	      let new_fun =
		mkFUNC (old_fun.func_name^": "^succ_lbl) succ_lbl old_fun.func_high new_fun_bbs
	      in
	      print_func new_fun; flush stdout;
	      new_fun
	  in
	  if target_fun.func_name <> "exit" then
	    rets_in_fun callsite_label target_fun;
      ) bb.bb_succs
    ) else ()
  ) bbtbl

let clean_ret_and_preds cr_list =
  List.iter (fun cr ->
    BBSet.iter (fun bb ->
      bb.bb_preds <- [];
      if does_bb_ret bb then
	bb.bb_succs <- []
    ) cr.bbs
  ) cr_list
(*
let handle_exit_call cr_list = 
  let func_map = H.create 32 in
  List.iter (fun cr ->
    BBSet.iter (fun bb ->
      if bb.bb_symbol_label <> "" then
	H.add func_map bb.bb_label bb.bb_symbol_label
    ) cr.bbs
  ) cr_list;
  List.iter (fun cr ->
    BBSet.iter (fun bb ->
      let _,last_instr,_ = List.last bb.bb_instrs in
      match last_instr with
      | CALL(_,_,_,_) ->
	List.iter (fun succ_label ->
	  if H.mem func_map succ_label then
	    let func_name = H.find func_map succ_label in
	    if func_name = "exit" then
	      bb.bb_inter_succs <- []
	    else ()
	  else 
	    raise (Failure "call instruction targets at non-function address")
	) bb.bb_succs;
	List.iter (fun inter_succ_label ->
	  if H.mem func_map inter_succ_label then
	    bb.bb_inter_succs <- []	    
	) bb.bb_inter_succs
      | _ -> ()
    ) cr.bbs
  ) cr_list
*)
