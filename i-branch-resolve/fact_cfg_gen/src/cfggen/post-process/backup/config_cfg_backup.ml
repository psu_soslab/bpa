open Batteries
open BB
open Big

open Printf
module P = Printf

module H = Hashtbl

(*open Core
open Smt_syntax
open Logical_formula
open Smt2_generation_for_Z3
open Block
open Search*)

open X86Syntax
open Config
open BB
open Ibaux
open Elf
open Abbrev

(* The basic block set uses order based on relAddr *)
let locate_label_in_bbs label bbs =
  try
    List.find (fun bb -> bb.bb_label = label) (BBSet.elements bbs)
  with Not_found ->
    P.printf "cannot find bb: %s\n" label;
    flush stdout;
    raise Not_found

let does_bb_itcall bb tcs =
  let (pre,ins,len) = (List.last bb.bb_instrs) in
  match ins with
    | JMP (true,abs,op,None) -> (
      match op with
      | Imm_op imm -> false
      | _ -> is_jmp_tail_call bb tcs 
    )
    | _ -> false

(*
let does_funcs_ret_naive succs functions_in_bb_form =
  let does_func_ret succ_func =
    let bool_list = List.map does_bb_ret succ_func.func_bbs in
    disjunct bool_list
  in
  let map_succ_to_func succ =
    try
      Some (List.find (fun func -> List.mem succ (List.map (fun bb -> bb.bb_label) func.func_bbs)) functions_in_bb_form)
    with
      Not_found -> None
  in
  let succ_funcs = List.filter_map map_succ_to_func succs in
  let bool_list' = List.map does_func_ret succ_funcs in
  disjunct bool_list
*)
(*
type func = {
  func_name: string;
  func_low: string;
  func_high: string;
  func_bbs: basicblock list;
}
*)
type call_graph_entry = {
  cge_ret_addr: string;
  cge_functions: func list;
  cge_ret_blocks: basicblock list;
}
 
let does_funcs_ret succs functions_in_bb_form =
  let map_succ_to_func succ =
    try
      Some (List.find (fun func -> List.mem succ (List.map (fun bb -> bb.bb_label) func.func_bbs)) functions_in_bb_form)
    with
      Not_found -> None
  in
  let rec disjunct bools =
    match bools with
    | [] -> false
    | h :: t ->
      h || (disjunct t)
  in
  let succ_funcs = List.filter_map map_succ_to_func succs in
  let all_bb = List.flatten (List.map (fun func -> func.func_bbs) succ_funcs) in
  let bool_list = List.map does_bb_ret all_bb in
  disjunct bool_list

let mkCGE ret_addr functions =
  let ret_blocks = List.map (fun func -> List.filter does_bb_ret func.func_bbs) functions in
  {
    cge_ret_addr = ret_addr;
    cge_functions = functions;
    cge_ret_blocks = List.flatten ret_blocks;
  }

let mkFUNC name low high bbs =
  {
    func_name = name;
    func_low = low;
    func_high = high;
    func_bbs = bbs;
  }

let print_func (func:func) =
  print_string " {\n";
  P.printf " function name: %s\n" func.func_name;
  P.printf " low pc:%s\n" func.func_low;
  P.printf " high pc:%s\n" func.func_high;
  print_string " Containing blocks:\n";
  List.iter (fun bb -> P.printf " %s" bb.bb_label) func.func_bbs;
  print_string "\n \n"

let print_func_to oc (func:func) =
  fprintf oc " {\n";
  fprintf oc " function name: %s\n" func.func_name;
  fprintf oc " low pc:%s\n" func.func_low;
  fprintf oc " high pc:%s\n" func.func_high;
  fprintf oc " Containing blocks:\n";
  List.iter (fun bb -> fprintf oc " %s" bb.bb_label) func.func_bbs;
  fprintf oc "\n \n";
  flush oc

let dump_call_graph cg =
  let print_cge cge =
    print_string "{\n";
    P.printf "return address: %s\n" cge.cge_ret_addr;
    print_string "functions that can return to this address:\n";
    List.iter print_func cge.cge_functions;
    print_string "corresponding return blocks:\n";
    List.iter (fun bb -> P.printf "%s " bb.bb_label) cge.cge_ret_blocks;
    print_string "\n}\n\n"
  in
  List.iter print_cge cg

let output_functions_in_bb_form oc functions_in_bb_form =
  List.iter (print_func_to oc) functions_in_bb_form
    
let output_sensitivity oc functions_in_bb_form tcs =
  let print_sensitive_blocks func =
    fprintf oc "{ %s\n" func.func_name;
    let sensitive_order = ref 0 in
    let print_sensitive_block bb =
      if does_bb_ret bb then (
	sensitive_order := !sensitive_order + 1;
	fprintf oc "%dR: " !sensitive_order;
	List.iter (fun succ_lbl -> fprintf oc "%s " succ_lbl) bb.bb_succs;
	fprintf oc "\n"
      )
      else if does_bb_icall bb then (
	sensitive_order := !sensitive_order + 1;
	fprintf oc "%dI: " !sensitive_order;
	List.iter (fun succ_lbl -> fprintf oc "%s " succ_lbl) bb.bb_succs;
	fprintf oc "\n"
      )
      else if does_bb_itcall bb tcs then (
	sensitive_order := !sensitive_order + 1;
	fprintf oc "%dU: " !sensitive_order;
	List.iter (fun succ_lbl -> fprintf oc "%s " succ_lbl) bb.bb_succs;
	fprintf oc "\n"
      )
    in
    List.iter print_sensitive_block func.func_bbs;
    fprintf oc "}\n";
    flush oc;
  in
  List.iter print_sensitive_blocks functions_in_bb_form

let output_funcname_and_RA oc functions_in_bb_form =
  let print_funcname_and_RA func =
    let curr_index = ref 0 in
    let work_func bb =
      if does_bb_call bb then (
	curr_index := !curr_index + 1;
	fprintf oc "%s %s %d\n" (List.hd bb.bb_inter_succs) func.func_name !curr_index
      )
    in
    fprintf oc "%s %s %d\n" (List.hd func.func_bbs).bb_label func.func_name 0; 
    List.iter work_func func.func_bbs;
    flush oc
  in
  List.iter print_funcname_and_RA functions_in_bb_form

let output_indirect_branches oc functions_in_bb_form start =
  let print_indirect_branches func =
    let work_func bb =
      let branch_site = bb.bb_relAddr + bb.bb_size in
      if (does_bb_icall bb || does_bb_ijump bb) then (
	fprintf oc "%s: " (str_of_mword_flex (MWord.(+%) (MWord.of_int branch_site) start));
	List.iter (fprintf oc "%s ") bb.bb_succs;
	fprintf oc "\n"
      )
    in
    List.iter work_func func.func_bbs;
    flush oc
  in
  List.iter print_indirect_branches functions_in_bb_form

let output_branches oc bbs start =
  let print_branches oc bb =
    let (pre,ins,len) = List.last bb.bb_instrs in
    let branch_site = bb.bb_size + bb.bb_relAddr - len in
    let abs_branch_site_mword = MWord.(+%) start (MWord.of_int branch_site) in
    fprintf oc "0x%s:" (str_of_mword_flex abs_branch_site_mword);
    List.iter (fprintf oc "0x%s ") bb.bb_succs;
    fprintf oc "\n";
    flush oc
  in
  BBSet.iter (print_branches oc) bbs

let output_cfg_encoding elf cr_list =
  let elf_name = find_target_name elf.fname in
  let res_file = P.sprintf "%s.branch" elf_name in
  let oc = open_out res_file in
  List.iter (fun cr -> output_branches oc cr.bbs cr.startAddr) cr_list;
  close_out oc

(*
let output_sensitive_cfg elf crs =
  let elf_name = find_target_name elf.fname in
  (*let res_file = P.sprintf "%s.scfg" elf_name in
  let oc = open_out res_file in
  let res' = P.sprintf "%s.fnra" elf_name in
  let oc' = open_out res' in*)
  (*let res'' = P.sprintf "%s.ibs" elf_name in
  let oc'' = open_out res'' in*)
  let res''' = P.sprintf "%s.branch" elf_name in
  let oc''' = open_out res''' in
  let bbs_in_range low high bbs =
    List.filter (fun bb -> low <= bb.bb_relAddr && (bb.bb_relAddr+bb.bb_size-1) <= high) (BBSet.elements bbs)
  in
  let generate_highpc func_list topAddr =
    let compare func1 func2 = (fst func1) - (fst func2) in
    let sorted_func_list = List.sort compare func_list in
    let length = List.length sorted_func_list in
    List.mapi (fun ndx (low,lbl) -> let high = 
				      if (ndx+1) < length then 
					(fst (List.nth sorted_func_list (ndx+1))) - 1 
				      else
					topAddr
				    in
				    (low,high,lbl)) sorted_func_list
  in
  let work_func cr =
    let bbs = cr.bbs in
    let start = cr.startAddr in
    (*let tcs = collect_tail_call_site elf start in*)
    let top_bb = BBSet.max_elt bbs in
    let (_,_,len) = List.last top_bb.bb_instrs in
    let top_addr = top_bb.bb_relAddr + top_bb.bb_size - len in
    let all_functions = collect_all_functions elf start in
    let functions_with_ranges = generate_highpc all_functions top_addr in
    (*let functions_in_bb_form = List.map (fun (lopc,hipc,name) -> mkFUNC name lopc hipc (bbs_in_range lopc hipc bbs)) functions_with_ranges in*)
    (*output_sensitivity oc functions_in_bb_form tcs;
    output_funcname_and_RA oc' functions_in_bb_form;*)
    (*output_indirect_branches oc'' functions_in_bb_form start*)
    output_branches oc''' bbs start
  in
  List.iter work_func crs;
  (*close_out oc;
  close_out oc';*)
  (*close_out oc'';*)
  close_out oc'''*)

let find_and_set_ret bb bbs progress =
  let workload = List.length bb.bb_succs in
  let bb_count = ref 0 in
  let first_progress_percent = ref 0 in
  let rec aux visited_list (succ:string) =
    P.printf "Now dealing with successor %s...%d.%d" succ progress !first_progress_percent;
    print_string "%\n";
    flush stdout;
    if List.mem succ visited_list then (
      P.printf "Current successor is VISITED...%d.%d" progress !first_progress_percent;
      print_string "%\n";
      flush stdout
    )
    else ( 
      let new_visited_list = succ :: visited_list in
      try 
	let succ_bb = List.hd (BBSet.elements (BBSet.filter (fun bb -> bb.bb_label = succ) bbs))
	in
	if does_bb_ret succ_bb then (
	  P.printf "Current successor RETURNs! Configuring the return targets...%d.%d" progress !first_progress_percent;
	  print_string "%\n";
	  flush stdout;
	  if List.mem (List.hd bb.bb_inter_succs) succ_bb.bb_succs then  ()
	  else (
	    succ_bb.bb_succs <- bb.bb_inter_succs @ succ_bb.bb_succs
	  )
	)
	else (
	  if does_bb_call succ_bb then (
	    P.printf "Current successor CALLs! Following the interprocedural successor...%d.%d" progress !first_progress_percent;
	    print_string "%\n";
	    flush stdout;
	    aux new_visited_list (List.hd succ_bb.bb_inter_succs)
	  )
	  else (
	    P.printf "Current successor JUMPs! Following the successor...%d.%d" progress !first_progress_percent;
	    print_string "%\n";
	    flush stdout;
	    List.iter (aux new_visited_list) succ_bb.bb_succs
	  )      
	)
      with Failure _ -> 
	P.printf "Empty list, so stop...%d.%d" progress !first_progress_percent;
	print_string "%\n";
	flush stdout;
	()
    )
  in
  let work_func bb =
    let old_progress = !first_progress_percent in
    first_progress_percent := 100 * !bb_count / workload;
    if !first_progress_percent <> old_progress then 
      progress_event ((P.sprintf "...%d.%d" progress !first_progress_percent) ^ "%");
    aux [] bb;
    bb_count := !bb_count + 1
  in
  List.iter work_func bb.bb_succs

let start_from_call_bb bb bbs progress =
  let (pre,ins,len) = List.last bb.bb_instrs in
  match ins with
  | CALL (_,_,_,_) ->
    (*P.printf "Now start from call basic block %s...\n" bb.bb_label; flush stdout;*) 
    find_and_set_ret bb bbs progress
  | _ -> ()

let get_absAddr (relAddr:int) (start:mword) =
  let rel_mword = MWord.of_int relAddr in
  let abs_mword = MWord.(+%) rel_mword start in
  str_of_mword_flex abs_mword

let config_ret_in_bbs bbs start = 
  (*let refer_bbs = ref bbs in*)
  let workload = BBSet.cardinal bbs in
  let bb_count = ref 0 in
  let progress_percent = ref 0 in
  let work_func bbs bb =
    let old_progress = !progress_percent in
    progress_percent := 100 * !bb_count / workload;
    if !progress_percent <> old_progress then 
      progress_event ((P.sprintf "Return targets configuring...%d" !progress_percent) ^ "%");
    start_from_call_bb bb bbs !progress_percent;
    bb_count := !bb_count + 1
  in
  BBSet.iter (work_func bbs) bbs
  (*BBSet.iter (fun bb -> start_from_ret_bb bb !refer_bbs) bbs*)
    
let config_ret_in_crs crs =
  print_string "Now iterating through code regions...\n";
  flush stdout;
  List.iter (fun cr -> progress_event "Dealing with one basic block set..."; config_ret_in_bbs cr.bbs cr.startAddr) crs

let collect_asm_func elf cr =
  let generate_highpc func_list topAddr =
    let compare func1 func2 = (fst func1) - (fst func2) in
    let sorted_func_list = List.sort compare func_list in
    let length = List.length sorted_func_list in
    List.mapi (fun ndx (low,lbl) -> 
      let high = 
	if (ndx+1) < length then 
	  (fst (List.nth sorted_func_list (ndx+1))) - 1 
	else
	  topAddr
      in
      (low,high,lbl)) sorted_func_list
  in

  let bbs_in_range low high bbs =
    List.filter (fun bb -> low <= bb.bb_relAddr && (bb.bb_relAddr+bb.bb_size-1) <= high) (BBSet.elements bbs)
  in

  let bbs = cr.bbs in
  let start = cr.startAddr in
  let top_bb = BBSet.max_elt bbs in
  let (_,_,len) = List.last top_bb.bb_instrs in
  let top_addr = top_bb.bb_relAddr + top_bb.bb_size - len in
  let all_functions = collect_all_functions elf start in
  (*let lib_func =
    {
      func_name = "libfunc";
      func_low = -1;
      func_high = 0;
      func_bbs = [locate_label_in_bbs "ffffffff" bbs]
    }
  in*)
  let functions_with_ranges = generate_highpc all_functions top_addr in
  List.map (fun (lopc,hipc,name) -> 
    let abs_lopc_str = str_of_mword_flex (MWord.(+%) (MWord.of_int lopc) start) in 
    let abs_hipc_str = str_of_mword_flex (MWord.(+%) (MWord.of_int hipc) start) in
    mkFUNC name abs_lopc_str abs_hipc_str (bbs_in_range lopc hipc bbs)
  ) functions_with_ranges

  
let generate_call_graph elf crs =
  let map_to_functions succs functions start =
    List.filter_map (fun funcstart -> try Some (List.find (fun func -> func.func_low = funcstart) functions) with Not_found -> None) succs
  in
  let find_tail_call_targets func functions start =
    let func_lopcs = 
      List.map (fun func -> func.func_low) functions 
    in
    let jump_blocks = List.filter (fun bb -> does_bb_jump bb || does_bb_jcc bb) func.func_bbs in
    let jump_targets = List.flatten (List.map (fun bb -> bb.bb_succs) jump_blocks) in
    let tail_call_targets = List.filter (fun succ -> List.mem succ func_lopcs) (eliminate_dup_a' jump_targets) in
    (*List.iter (P.printf "tail call targets:%s\n") tail_call_targets;*)
    map_to_functions tail_call_targets functions start
  in

  let collect_all_call_blocks bbs =
    List.filter does_bb_call (BBSet.elements bbs)
  in

  let reachable_functions bbs succs functions start =
    let rec work_func func func_list =
      if List.mem func func_list then func_list
      else
	let func_list = func :: func_list in
	let tail_call_targets = find_tail_call_targets func functions start in
	if List.is_empty tail_call_targets then func_list
	else List.fold_right work_func tail_call_targets func_list
    in
    let succ_function_list = map_to_functions succs functions start in
    let reachable_functions = List.fold_right work_func succ_function_list [] in
    reachable_functions
  in

  let generate_cg_from_cr cr =
    let bbs = cr.bbs in
    let start = cr.startAddr in
    let functions_in_bb_form = collect_asm_func elf cr in
    (*let lib_func = 
      {
	func_name = "library";
	func_low = "ffffffff";
	func_high = "ffffffff";
	func_bbs = [List.find (fun bb -> bb.bb_label = "ffffffff") (BBSet.elements bbs)]
      }
    in*)
    let all_functions = (*lib_func ::*) functions_in_bb_form in
    let all_call_blocks = collect_all_call_blocks bbs in
    let workload = List.length all_call_blocks in
    let bb_count = ref 0 in
    let progress_percent = ref 0 in
    let work_func bb =
      let old_progress = !progress_percent in
      let () =
	progress_percent := 100 * !bb_count / workload; 
	if !progress_percent <> old_progress then 
	  progress_event ((P.sprintf "Generating call graph...%d" !progress_percent) ^ "%");
      in
      let result = mkCGE (List.hd bb.bb_inter_succs) (reachable_functions bbs bb.bb_succs all_functions start) in
      let () = bb_count := !bb_count + 1 in
      result
    in
    List.map work_func all_call_blocks
  in
  List.flatten (List.map generate_cg_from_cr crs)

let config_ret_by_call_graph crs call_graph =
  let config_ret_blocks bbs ret_addr ret_blocks =
    List.iter (fun ret_bb -> ret_bb.bb_succs <- ret_addr :: ret_bb.bb_succs;) ret_blocks
  in
  let config_ret_addr_block bbs ret_addr ret_blocks =
    let ret_addr_bb = locate_label_in_bbs ret_addr bbs in
    let new_preds = List.map (fun bb -> bb.bb_label) ret_blocks in
    ret_addr_bb.bb_preds <- new_preds @ ret_addr_bb.bb_preds
  in
  let work_func bbs start call_graph =
    let workload = List.length call_graph in
    let cge_count = ref 0 in
    let progress_percent = ref 0 in
    List.iter (fun cge -> 
      let old_progress = !progress_percent in
      progress_percent := 100 * !cge_count / workload;
      if !progress_percent <> old_progress then 
	progress_event ((P.sprintf "Configuring return targets...%d" !progress_percent) ^ "%");
      config_ret_addr_block bbs cge.cge_ret_addr cge.cge_ret_blocks;
      config_ret_blocks bbs cge.cge_ret_addr cge.cge_ret_blocks;
      cge_count := !cge_count + 1) call_graph
  in
  progress_event "Now iterating through code regions...";
  List.iter (fun cr -> progress_event "Dealing with one basic block set..."; work_func cr.bbs cr.startAddr call_graph) crs
  

let bbset_to_bbtbl cr_list =
  let bbtbl = H.create 32 in
  List.iter (fun cr ->
    BBSet.iter (fun bb ->
      H.add bbtbl bb.bb_label bb
    ) cr.bbs
  ) cr_list;
  bbtbl


let bbtbl_stat_report bbtbl =
  let ibnum = ref 0 in
  let ibtnum = ref 0 in
  let icnum = ref 0 in
  let ictnum = ref 0 in
  let ijnum = ref 0 in
  let ijtnum = ref 0 in
  Hashtbl.iter (fun lbl bb ->
    if does_bb_icall bb then (
      ibnum := !ibnum + 1;
      ibtnum := !ibtnum + (List.length bb.bb_succs);
      icnum := !icnum + 1;
      ictnum := !ictnum + (List.length bb.bb_succs);
    ) else if does_bb_ijump bb then (
      ibnum := !ibnum + 1;
      ibtnum := !ibtnum + (List.length bb.bb_succs);
      ijnum := !ijnum + 1;
      ijtnum := !ijtnum + (List.length bb.bb_succs);
    ) else if does_bb_ret bb then (
      ibnum := !ibnum + 1;
      ibtnum := !ibtnum + (List.length bb.bb_succs);
    ) else ()
  ) bbtbl;
  P.printf "Number of Indirect Branches: %d\n" !ibnum;
  P.printf "Number of Indirect Branch Targets: %d\n" !ibtnum;
  P.printf "Average of Indirect Branch Targets: %f\n" ((float_of_int !ibtnum) /. (float_of_int !ibnum));
  P.printf "Number of Indirect Calls: %d\n" !icnum;
  P.printf "Number of Indirect Call Targets: %d\n" !ictnum;
  P.printf "Average of Indirect Call Targets: %f\n" ((float_of_int !ictnum) /. (float_of_int !icnum));
  P.printf "Number of Indirect Jumps: %d\n" !ijnum;
  P.printf "Number of Indirect Jump Targets: %d\n" !ijtnum;
  P.printf "Average of Indirect Jump Targets: %f\n" ((float_of_int !ijtnum) /. (float_of_int !ijnum))

let config_preds cr_list =
  List.iter (fun cr ->
    BBSet.iter (fun bb ->
      List.iter (fun succ_label ->
	if succ_label <> "ffffffff" then
          let succ_bb = locate_label_in_bbs succ_label cr.bbs in
	  if List.mem bb.bb_label succ_bb.bb_preds then ()
	  else
	    succ_bb.bb_preds <- bb.bb_label :: succ_bb.bb_preds
      ) bb.bb_succs
    ) cr.bbs
  ) cr_list

let config_preds_in_bbtbl bbtbl =
  (** TODO: Check whether the change is reflected in cr_list **)
  H.iter (fun lbl bb ->
    List.iter (fun succ_lbl ->
      if succ_lbl <> "ffffffff" then
	try
	  let succ_bb = H.find bbtbl succ_lbl in
	  succ_bb.bb_preds <- List.unique (bb.bb_label :: succ_bb.bb_preds)
	with Not_found ->
	  let errmsg = P.sprintf "%s, SUCC of %s, does not exists!" succ_lbl lbl in
	  raise (Failure errmsg)
    ) bb.bb_succs
  ) bbtbl

let config_ret bbtbl asm_fun_list =
  let fun_tbl = H.create 32 in
  List.iter (fun asm_fun ->
    let ret_to_lib_funcs = 
      [
	"deregister_tm_clones"; 
	"register_tm_clones"; 
	"frame_dummy";
	"__libc_csu_init";
	"__libc_csu_fini";
	"main";
	"__do_global_dtors_aux";
	"_fini";
      ]
    in
    if List.mem asm_fun.func_name ret_to_lib_funcs then
      List.iter (fun bb ->
	if does_bb_ret bb then
	  bb.bb_succs <- ["ffffffff"]
      ) asm_fun.func_bbs;
    H.add fun_tbl asm_fun.func_low asm_fun;
  ) asm_fun_list;

  H.iter (fun lbl bb ->
    if does_bb_call bb then (
      assert (List.length bb.bb_inter_succs = 1);
      let visited_fun = ref [] in

      let rec rets_in_fun callsite target_fun =
	if target_fun.func_name <> "exit" && 
	  not (List.mem target_fun.func_name !visited_fun) 
	then (
	  visited_fun := target_fun.func_name :: !visited_fun;
	  List.iter (fun bb_in_target ->
	    if does_bb_ret bb_in_target then
	      bb_in_target.bb_succs <- callsite :: bb_in_target.bb_succs
	    else if does_bb_jump bb_in_target then
	      List.iter (fun jmp_target ->
		if jmp_target = "ffffffff" then (
		  (** jump into library **)
		  assert (H.mem bbtbl callsite);
		  let ret_to_bb = H.find bbtbl callsite in
		  ret_to_bb.bb_preds <- List.unique (jmp_target :: ret_to_bb.bb_preds)
		)
		else if H.mem fun_tbl jmp_target then
		  rets_in_fun callsite (H.find fun_tbl jmp_target)
		else ()
	      ) bb_in_target.bb_succs
	    else ()
	  ) target_fun.func_bbs
	)
      in
      
      let callsite_label = List.hd bb.bb_inter_succs in
      List.iter (fun succ_lbl ->
	if succ_lbl <> "ffffffff" then
	  let target_fun = 
	    try
	      H.find fun_tbl succ_lbl
	    with Not_found ->
	      let errmsg = P.sprintf "CALL_BB %s targets at %s" lbl succ_lbl in
	      raise (Failure errmsg)
	  in
	  if target_fun.func_name <> "exit" then
	    rets_in_fun callsite_label target_fun;
      ) bb.bb_succs
    ) else ()
  ) bbtbl

let clean_ret_and_preds cr_list =
  List.iter (fun cr ->
    BBSet.iter (fun bb ->
      bb.bb_preds <- [];
      if does_bb_ret bb then
	bb.bb_succs <- []
    ) cr.bbs
  ) cr_list

let handle_exit_call cr_list = 
  let func_map = H.create 32 in
  List.iter (fun cr ->
    BBSet.iter (fun bb ->
      if bb.bb_symbol_label <> "" then
	H.add func_map bb.bb_label bb.bb_symbol_label
    ) cr.bbs
  ) cr_list;
  List.iter (fun cr ->
    BBSet.iter (fun bb ->
      let _,last_instr,_ = List.last bb.bb_instrs in
      match last_instr with
      | CALL(_,_,_,_) ->
	List.iter (fun succ_label ->
	  if H.mem func_map succ_label then
	    let func_name = H.find func_map succ_label in
	    if func_name = "exit" then
	      bb.bb_inter_succs <- []
	    else ()
	  else 
	    raise (Failure "call instruction targets at non-function address")
	) bb.bb_succs;
	List.iter (fun inter_succ_label ->
	  if H.mem func_map inter_succ_label then
	    bb.bb_inter_succs <- []	    
	) bb.bb_inter_succs
      | _ -> ()
    ) cr.bbs
  ) cr_list
    
