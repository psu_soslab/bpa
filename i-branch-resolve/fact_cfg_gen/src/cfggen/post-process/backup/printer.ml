open Batteries
open X86Syntax
open X86Semantics
open RTL
open Big
open Printf
open Config
open Abbrev

let str_of_size s = to_string (succ s)

let str_of_bvop bvop = 
  match bvop with
  | X86_RTL.Coq_add_op ->  "+"
  | X86_RTL.Coq_sub_op ->  "-"
  | X86_RTL.Coq_mul_op ->  "*"
  | X86_RTL.Coq_divs_op ->  "/s"
  | X86_RTL.Coq_divu_op ->  "/u"
  | X86_RTL.Coq_modu_op ->  "%u"
  | X86_RTL.Coq_mods_op ->  "%s"
  | X86_RTL.Coq_and_op ->  "&"
  | X86_RTL.Coq_or_op ->  "|"
  | X86_RTL.Coq_xor_op ->  "^"
  | X86_RTL.Coq_shl_op ->  "<<"
  | X86_RTL.Coq_shr_op ->  ">>"
  | X86_RTL.Coq_shru_op ->  ">>u"
  | X86_RTL.Coq_ror_op ->  "ROR"
  | X86_RTL.Coq_rol_op ->  "ROL"

let str_of_top top = 
  match top with
  | X86_RTL.Coq_eq_op ->  "=="
  | X86_RTL.Coq_lt_op ->  "<"
  | X86_RTL.Coq_ltu_op ->  "<u"

let str_of_value v = to_string v

let str_of_reg reg =
  match reg with
  | EAX ->  "EAX"
  | ECX ->  "ECX"
  | EDX ->  "EDX"
  | EBX ->  "EBX"
  | ESP ->  "ESP"
  | EBP ->  "EBP"
  | ESI ->  "ESI"
  | EDI ->  "EDI"

let str_of_ps_reg ps_reg =
  to_string ps_reg

let str_of_segreg segreg= match segreg with
    ES ->  "ES"
  | CS ->  "CS"
  | SS ->  "SS"
  | DS ->  "DS"
  | FS ->  "FS"
  | GS ->  "GS"

let str_of_flag f = 
  match f with
  | X86_MACHINE.ID ->  "ID"
  | X86_MACHINE.VIP ->  "VIP"
  | X86_MACHINE.VIF ->  "VIF"
  | X86_MACHINE.AC ->  "AC"
  | X86_MACHINE.VM ->  "VM"
  | X86_MACHINE.RF ->  "RF"
  | X86_MACHINE.NT ->  "NT"
  | X86_MACHINE.IOPL ->  "IOPL"
  | X86_MACHINE.OF ->  "OF"
  | X86_MACHINE.DF ->  "DF"
  | X86_MACHINE.IF_flag ->  "IF_flag"
  | X86_MACHINE.TF ->  "TF"
  | X86_MACHINE.SF ->  "SF"
  | X86_MACHINE.ZF ->  "ZF"
  | X86_MACHINE.AF ->  "AF"
  | X86_MACHINE.PF ->  "PF"
  | X86_MACHINE.CF ->  "CF"

let str_of_ctrlseg ctrlseg = 
  match ctrlseg with
  | CR0 ->  "CR0"
  | CR2 ->  "CR2"
  | CR3 ->  "CR3"
  | CR4 ->  "CR4"

let str_of_dbgreg dbgreg = 
  match dbgreg with
  | DR0 ->  "DR0"
  | DR1 ->  "DR1"
  | DR2 ->  "DR2"
  | DR3 ->  "DR3"
  | DR6 ->  "DR6"
  | DR7 ->  "DR7"

let str_of_fpu_flag ff = match ff with
    X86_MACHINE.F_Busy ->  "F_Busy"
  | X86_MACHINE.F_C3 ->  "F_C3"
  | X86_MACHINE.F_C2 ->  "F_C2"
  | X86_MACHINE.F_C1 ->  "F_C1"
  | X86_MACHINE.F_C0 ->  "F_C0"
  | X86_MACHINE.F_ES ->  "F_ES"
  | X86_MACHINE.F_SF ->  "F_SF"
  | X86_MACHINE.F_PE ->  "F_PE"
  | X86_MACHINE.F_UE ->  "F_UE"
  | X86_MACHINE.F_OE ->  "F_OE"
  | X86_MACHINE.F_ZE ->  "F_ZE"
  | X86_MACHINE.F_DE ->  "F_DE"
  | X86_MACHINE.F_IE ->  "F_IE"

let str_of_fpu_ctrl_flag fcf = match fcf with
    X86_MACHINE.F_Res15 ->  "F_Res15"
  | X86_MACHINE.F_Res14 ->  "F_Res14"
  | X86_MACHINE.F_Res13 ->  "F_Res13"
  | X86_MACHINE.F_Res7 ->  "F_Res7"
  | X86_MACHINE.F_Res6 ->  "F_Res6"
  | X86_MACHINE.F_IC ->  "F_IC"
  | X86_MACHINE.F_PM ->  "F_PM"
  | X86_MACHINE.F_UM ->  "F_UM"
  | X86_MACHINE.F_OM ->  "F_OM"
  | X86_MACHINE.F_ZM ->  "F_ZM"
  | X86_MACHINE.F_DM ->  "F_DM"
  | X86_MACHINE.F_IM ->  "F_IM"

let str_of_loc loc = 
  match loc with
  | X86_MACHINE.Coq_reg_loc(reg) -> 
    str_of_reg reg
  | X86_MACHINE.Coq_seg_reg_start_loc(segreg) -> 
    "Start" ^ (str_of_segreg segreg)
  | X86_MACHINE.Coq_seg_reg_limit_loc(segreg) -> 
    "Limit" ^ (str_of_segreg segreg)
  | X86_MACHINE.Coq_flag_loc(f) -> 
    str_of_flag f
  | X86_MACHINE.Coq_control_register_loc(ctrlreg) -> 
    str_of_ctrlseg ctrlreg
  | X86_MACHINE.Coq_debug_register_loc (dbgreg) ->
    str_of_dbgreg dbgreg
  | X86_MACHINE.Coq_pc_loc -> "Pc"
  | X86_MACHINE.Coq_fpu_stktop_loc -> "Fpu_stktop"
  | X86_MACHINE.Coq_fpu_flag_loc(ff) -> 
    str_of_fpu_flag ff
  | X86_MACHINE.Coq_fpu_rctrl_loc -> "Fpu_rctrl"
  | X86_MACHINE.Coq_fpu_pctrl_loc -> "Fpu_pctrl"
  | X86_MACHINE.Coq_fpu_ctrl_flag_loc(fcf) -> 
    str_of_fpu_ctrl_flag fcf
  | X86_MACHINE.Coq_fpu_lastInstrPtr_loc -> "Fpu_lastInstrPtr"
  | X86_MACHINE.Coq_fpu_lastDataPtr_loc -> "Fpu_lastDataPtr"
  | X86_MACHINE.Coq_fpu_lastOpcode_loc -> "Fpu_lastOpcode"

let str_of_array arr = match arr with
  | X86_MACHINE.Coq_fpu_datareg -> "Fpu_datareg"
  | X86_MACHINE.Coq_fpu_tag -> "Fpu_tag"

let str_of_faop faop = match faop with
    X86_RTL.Coq_fadd_op -> "+."
  | X86_RTL.Coq_fsub_op -> "-."
  | X86_RTL.Coq_fmul_op -> "*."
  | X86_RTL.Coq_fdiv_op -> "/."

let rec str_of_rtl_exp_less_size rtl_e = 
  match rtl_e with
  | X86_RTL.Coq_arith_rtl_exp(s, bvop, rtl_e1, rtl_e2) ->
    sprintf "(%s %s %s)" 
      (str_of_rtl_exp_less_size rtl_e1)
      (str_of_bvop bvop)
      (str_of_rtl_exp_less_size rtl_e2)
  | X86_RTL.Coq_test_rtl_exp(s, top, rtl_e1, rtl_e2) ->
    sprintf "(%s %s %s)"
      (str_of_rtl_exp_less_size rtl_e1)
      (str_of_top top)
      (str_of_rtl_exp_less_size rtl_e2)
  | X86_RTL.Coq_if_rtl_exp(s, rtl_e1, rtl_e2, rtl_e3) -> 
    sprintf "(%s ? %s : %s)"
      (str_of_rtl_exp_less_size rtl_e1)
      (str_of_rtl_exp_less_size rtl_e2)
      (str_of_rtl_exp_less_size rtl_e3)
  | X86_RTL.Coq_cast_s_rtl_exp(s1, s2, rtl_e') -> (* from s1 to s2 *)
    sprintf "(CastS<%s,%s> %s)"
      (str_of_size s1)
      (str_of_size s2)
      (str_of_rtl_exp_less_size rtl_e')
  | X86_RTL.Coq_cast_u_rtl_exp(s1, s2, rtl_e') -> (* from s1 to s2 *)
    sprintf "(CastU<%s,%s> %s)"
      (str_of_size s1)
      (str_of_size s2)
      (str_of_rtl_exp_less_size rtl_e')
  | X86_RTL.Coq_imm_rtl_exp(s, v) ->
    sprintf "%d"
      (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int v)))
      (*(str_of_size s)*)
  | X86_RTL.Coq_get_loc_rtl_exp(s, loc) ->
    sprintf "*%s"
      (str_of_loc loc)
  | X86_RTL.Coq_get_ps_reg_rtl_exp(s, ps_reg) ->
    sprintf "*%s"
      (str_of_ps_reg ps_reg)
  | X86_RTL.Coq_get_array_rtl_exp(l, s, arr, rtl_e') ->
    sprintf "%s(%s)[%s]"
      (str_of_array arr)
      (to_string l)
      (str_of_rtl_exp_less_size rtl_e')
  | X86_RTL.Coq_get_byte_rtl_exp(rtl_e') ->
    sprintf "*%s" (str_of_rtl_exp_less_size rtl_e')
  | X86_RTL.Coq_get_random_rtl_exp(s) ->
    "random"
  | X86_RTL.Coq_farith_rtl_exp(ew,mw,faop,rtl_rm,rtl_e1,rtl_e2) ->
    sprintf "Farith(%s,%s,%s,%s,%s,%s)"
      (str_of_size ew)
      (str_of_size mw)
      (str_of_faop faop)
      (str_of_rtl_exp_less_size rtl_rm)
      (str_of_rtl_exp_less_size rtl_e1)
      (str_of_rtl_exp_less_size rtl_e2)
  | X86_RTL.Coq_fcast_rtl_exp(s1,s2,s3,s4,rtl_e1,rtl_e2) ->
    sprintf "FCast(%s,%s,%s,%s,%s,%s)"
      (str_of_size s1)
      (str_of_size s2)
      (str_of_size s3)
      (str_of_size s4)
      (str_of_rtl_exp_less_size rtl_e1)
      (str_of_rtl_exp_less_size rtl_e2)

let rec str_of_rtl_exp rtl_e = 
  match rtl_e with
  | X86_RTL.Coq_arith_rtl_exp(s, bvop, rtl_e1, rtl_e2) ->
    sprintf "(%s %s %s)@%s" 
      (str_of_rtl_exp rtl_e1)
      (str_of_bvop bvop)
      (str_of_rtl_exp rtl_e2)
      (str_of_size s)
  | X86_RTL.Coq_test_rtl_exp(s, top, rtl_e1, rtl_e2) ->
    sprintf "(%s %s %s)@%s"
      (str_of_rtl_exp rtl_e1)
      (str_of_top top)
      (str_of_rtl_exp rtl_e2)
      (str_of_size s)
  | X86_RTL.Coq_if_rtl_exp(s, rtl_e1, rtl_e2, rtl_e3) -> 
    sprintf "(%s ? %s : %s)@%s"
      (str_of_rtl_exp rtl_e1)
      (str_of_rtl_exp rtl_e2)
      (str_of_rtl_exp rtl_e3)
      (str_of_size s)
  | X86_RTL.Coq_cast_s_rtl_exp(s1, s2, rtl_e') -> (* from s1 to s2 *)
    sprintf "(CastS %s?%s)@%s"
      (str_of_rtl_exp rtl_e')
      (str_of_size s1)
      (str_of_size s2)
  | X86_RTL.Coq_cast_u_rtl_exp(s1, s2, rtl_e') -> (* from s1 to s2 *)
    sprintf "(CastU %s?%s)@%s"
      (str_of_rtl_exp rtl_e')
      (str_of_size s1)
      (str_of_size s2)
  | X86_RTL.Coq_imm_rtl_exp(s, v) ->
    sprintf "%x@%s"
      (to_int (signed32 v))
      (str_of_size s)
  | X86_RTL.Coq_get_loc_rtl_exp(s, loc) ->
    sprintf "(%s)@%s"
      (str_of_loc loc)
      (str_of_size s)
  | X86_RTL.Coq_get_ps_reg_rtl_exp(s, ps_reg) ->
    sprintf "(%s)@%s"
      (str_of_ps_reg ps_reg)
      (str_of_size s)
  | X86_RTL.Coq_get_array_rtl_exp(l, s, arr, rtl_e') ->
    sprintf "(%s(%s)[%s])@%s"
      (str_of_array arr)
      (to_string l)
      (str_of_rtl_exp rtl_e')
      (str_of_size s)
  | X86_RTL.Coq_get_byte_rtl_exp(rtl_e') ->
    sprintf "(%s)@8" (str_of_rtl_exp rtl_e')
  | X86_RTL.Coq_get_random_rtl_exp(s) ->
    sprintf "rand@%s" (str_of_size s)
  | X86_RTL.Coq_farith_rtl_exp(ew,mw,faop,rtl_rm,rtl_e1,rtl_e2) ->
    sprintf "Farith(%s,%s,%s,%s,%s,%s)"
      (str_of_size ew)
      (str_of_size mw)
      (str_of_faop faop)
      (str_of_rtl_exp rtl_rm)
      (str_of_rtl_exp rtl_e1)
      (str_of_rtl_exp rtl_e2)
  | X86_RTL.Coq_fcast_rtl_exp(s1,s2,s3,s4,rtl_e1,rtl_e2) ->
    sprintf "FCast(%s,%s,%s,%s,%s,%s)"
      (str_of_size s1)
      (str_of_size s2)
      (str_of_size s3)
      (str_of_size s4)
      (str_of_rtl_exp rtl_e1)
      (str_of_rtl_exp rtl_e2)

let rec str_of_rtl_exp rtl_e = 
  match rtl_e with
  | X86_RTL.Coq_arith_rtl_exp(s, bvop, rtl_e1, rtl_e2) ->
    sprintf "(%s %s %s)@%s" 
      (str_of_rtl_exp rtl_e1)
      (str_of_bvop bvop)
      (str_of_rtl_exp rtl_e2)
      (str_of_size s)
  | X86_RTL.Coq_test_rtl_exp(s, top, rtl_e1, rtl_e2) ->
    sprintf "(%s %s %s)@%s"
      (str_of_rtl_exp rtl_e1)
      (str_of_top top)
      (str_of_rtl_exp rtl_e2)
      (str_of_size s)
  | X86_RTL.Coq_if_rtl_exp(s, rtl_e1, rtl_e2, rtl_e3) -> 
    sprintf "(%s ? %s : %s)@%s"
      (str_of_rtl_exp rtl_e1)
      (str_of_rtl_exp rtl_e2)
      (str_of_rtl_exp rtl_e3)
      (str_of_size s)
  | X86_RTL.Coq_cast_s_rtl_exp(s1, s2, rtl_e') -> (* from s1 to s2 *)
    sprintf "(CastS %s?%s)@%s"
      (str_of_rtl_exp rtl_e')
      (str_of_size s1)
      (str_of_size s2)
  | X86_RTL.Coq_cast_u_rtl_exp(s1, s2, rtl_e') -> (* from s1 to s2 *)
    sprintf "(CastU %s?%s)@%s"
      (str_of_rtl_exp rtl_e')
      (str_of_size s1)
      (str_of_size s2)
  | X86_RTL.Coq_imm_rtl_exp(s, v) ->
    sprintf "%s@%s"
      (to_string v)
      (str_of_size s)
  | X86_RTL.Coq_get_loc_rtl_exp(s, loc) ->
    sprintf "(%s)@%s"
      (str_of_loc loc)
      (str_of_size s)
  | X86_RTL.Coq_get_ps_reg_rtl_exp(s, ps_reg) ->
    sprintf "(%s)@%s"
      (str_of_ps_reg ps_reg)
      (str_of_size s)
  | X86_RTL.Coq_get_array_rtl_exp(l, s, arr, rtl_e') ->
    sprintf "(%s(%s)[%s])@%s"
      (str_of_array arr)
      (to_string l)
      (str_of_rtl_exp rtl_e')
      (str_of_size s)
  | X86_RTL.Coq_get_byte_rtl_exp(rtl_e') ->
    sprintf "(%s)@8" (str_of_rtl_exp rtl_e')
  | X86_RTL.Coq_get_random_rtl_exp(s) ->
    sprintf "rand@%s" (str_of_size s)
  | X86_RTL.Coq_farith_rtl_exp(ew,mw,faop,rtl_rm,rtl_e1,rtl_e2) ->
    sprintf "Farith(%s,%s,%s,%s,%s,%s)"
      (str_of_size ew)
      (str_of_size mw)
      (str_of_faop faop)
      (str_of_rtl_exp rtl_rm)
      (str_of_rtl_exp rtl_e1)
      (str_of_rtl_exp rtl_e2)
  | X86_RTL.Coq_fcast_rtl_exp(s1,s2,s3,s4,rtl_e1,rtl_e2) ->
    sprintf "FCast(%s,%s,%s,%s,%s,%s)"
      (str_of_size s1)
      (str_of_size s2)
      (str_of_size s3)
      (str_of_size s4)
      (str_of_rtl_exp rtl_e1)
      (str_of_rtl_exp rtl_e2)

let rec str_of_rtl_instr_less_size rtl_i =
  match rtl_i with
  | X86_RTL.Coq_set_loc_rtl(s, rtl_e, loc) ->
    sprintf "%s <-- %s"
      (str_of_loc loc)
      (str_of_rtl_exp_less_size rtl_e)
  | X86_RTL.Coq_set_ps_reg_rtl(s, rtl_e, ps_reg) ->
    sprintf "%s <-- %s"
      (str_of_ps_reg ps_reg)
      (str_of_rtl_exp_less_size rtl_e)
  | X86_RTL.Coq_set_array_rtl(l,s,arr,rtl_e1,rtl_e2) ->
    sprintf "%s(%s)[%s] <-- %s"
      (str_of_array arr)
      (to_string l)
      (str_of_rtl_exp_less_size rtl_e1)
      (str_of_rtl_exp_less_size rtl_e2)
  | X86_RTL.Coq_set_byte_rtl(rtl_e1, rtl_e2) ->
    sprintf "%s <-- %s"
      (str_of_rtl_exp_less_size rtl_e2)
      (str_of_rtl_exp_less_size rtl_e1)
  | X86_RTL.Coq_advance_oracle_rtl ->
    "Advance_oracle"
  | X86_RTL.Coq_if_rtl(rtl_e, rtl_i') ->
    sprintf "IF %s DO\n\t%s\nFI"
      (str_of_rtl_exp_less_size rtl_e)
      (str_of_rtl_instr_less_size rtl_i')
  | X86_RTL.Coq_error_rtl ->
    "Error"
  | X86_RTL.Coq_trap_rtl ->
    "Trap"

let print_rtl_instr rtl_i =
  printf "%s\n\n" (str_of_rtl_instr_less_size rtl_i)
