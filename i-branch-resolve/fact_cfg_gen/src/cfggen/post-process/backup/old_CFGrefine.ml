open Batteries

open Elf
open X86Syntax

open Config
open BB
open Ibaux

open TypeEngineLib

(*
let rec sctype_equiv sctype1 sctype2 struct_map =
  match sctype1,sctype2 with
  | SC_base name1, SC_base name2 -> name1 = name2
  (*| SC_const sct1, SC_const sct2 -> sctype_equiv ct1 ct2*)
  | SC_const sct1, _ -> sctype_equiv sct1 sctype2
  | _, SC_const sct2 -> sctype_equiv sctype1 sct2
  | SC_array (sct1,dimlist1), SC_array (sct2,dimlist2) -> 
    let type_consis = sctype_equiv sct1 sct2 in 
    let dim_consis = 
      if List.length dimlist1 = List.length dimlist2 then 
	List.fold_right2 
	  (fun dim1 dim2 b -> 
	    if dim1 = 0 || dim2 = 0 then b 
	    else (dim1 = dim2) && b) dimlist1 dimlist2 true
      else false
    in
    type_consis && dim_consis
  | SC_array (sct1,_), SC_pointer sct2 -> sctype_equiv sct1 sct2
  | SC_pointer sct1, SC_pointer sct2 -> sctype_equiv sct1 sct2
  | SC_pointer sct1, SC_array (sct2,_) -> sctype_equiv sct1 sct2
  | SC_typedef (defnm1,sct1), SC_typedef (defnm2,sct2) -> 
    if defnm1 = defnm2 then true
    else
  | C_typedef (_,sct1), _ -> sctype_equiv sct1 sctype2
  | _, C_typedef (_,sct2) -> sctype_equiv sctype1 sct2
  | C_subroutine (ret1,param1), C_subroutine (ret2,param2) ->
    let ret_consis = sctype_equiv ret1 ret2 in
    let param_consis = param_consistent param1 param2 in
    ret_consis && param_consis
  | C_enumeration (enumname1,etorlist1), C_enumeration (enumname2,etorlist2) ->
    enumname1 = enumname2
  | C_union (unionname1,umemlist1), C_union (unionname2,umemlist2) ->
    (** when one has name but no type and the other does not have name but have type, they may
	still be the same **)
    if unionname1 = unionname2 then true
    else if List.length umemlist1 = List.length umemlist2 then
      List.fold_right2 (fun (sct1,str1) (sct2,str2) b -> b && (sctype_equiv sct1 sct2) && (str1 = str2)) umemlist1 umemlist2 true
    else false
  | SC_structure (structname1,_,smemlist1), SC_structure (structname2,_,smemlist2) -> 
    if structname1 = structname2 then true
    else if levelname1 = levelname2 && (List.length smemlist1) = (List.length smemlist2) then
      if (List.length smemlist1) = 0 then true
      else
	List.fold_right2 (fun (ct1,str1) (ct2,str2) b -> b && ctype_consistent ct1 ct2 && str1 = str2) smemlist1 smemlist2 true
    else false
  | C_unspecified_param, _ ->  
    (** for example: fp (int)(...) should be able to call function int foo(int) **)
    true
  | _, C_unspecified_param ->
    (** for example: fp (int)(int) should never call function int foo(...) **)    
    false
  | C_wrong_type errmsg1, _ -> 
    let () = P.printf "In ctype_consistent, the first type is wrong type: %s" errmsg1 in
    false
  | _, C_wrong_type errmsg2 ->
    let () = P.printf "In ctype_consistent, the second type is wrong type: %s" errmsg2 in
    false
  | C_volatile ct1, C_volatile ct2 -> ctype_consistent ct1 ct2
  | _ -> false
    
and param_consistent fp_param eqc_param =
  match fp_param,eqc_param with
  | [], [] -> true
  | (hd_fp_param :: tl_fp_param), [] ->
    if hd_fp_param = C_unspecified_param && 0 = List.length tl_fp_param then true
    else false
  | hd_fp_param :: tl_fp_param, hd_eqc_param :: tl_eqc_param ->
    if hd_fp_param = C_unspecified_param then
      let hd_consis = ctype_consistent hd_fp_param hd_eqc_param in
      let tl_consis = param_consistent fp_param tl_eqc_param in
      hd_consis && tl_consis
    else 
      let hd_consis = ctype_consistent hd_fp_param hd_eqc_param in
      let tl_consis = param_consistent tl_fp_param tl_eqc_param in
      hd_consis && tl_consis
  | _ -> false

let rec fp_cover_tsig fp_sct tsig_sct struct_map =
  let fp_ret, fp_param =
    match fp_sct with
    | SC_pointer (SC_subroutine (ret_sct,param_sct)) ->
      ret_sct, param_sct
    | _ -> assert false
  in
  let tsig_ret, tsig_param = 
    match tsig_sct with
    | SC_pointer (SC_subroutine (ret_sct,param_sct)) ->
      ret_sct, param_sct
    | _ -> assert false
  in
  let ret_consistent = 
    sctype_equiv fp_ret (SC_pointer (SC_base "void")) 
    (*|| sctype_equiv tsig_ret (SC_pointer (SC_base "void"))*) 
    || sctype_equiv fp_ret tsig_ret	
  in
  let param_consistent = 
    List.fold_right2 (fun fp_pa tsig_pa b
      
    ) fp_param tsig_param true
      
*)
let sctype_of_teqc teqc =
  let rettype = teqc.t_eqc_ret_t in
  let param_types = teqc.t_eqc_param_t in
  let ctype_subroutine = C_subroutine (rettype,param_types) in
  let sctype_subroutine = ctype_to_sctype ctype_subroutine in
  SC_pointer sctype_subroutine
    
let ctype_of_teqc teqc =
  let rettype = teqc.t_eqc_ret_t in
  let param_types = teqc.t_eqc_param_t in
  let ctype_subroutine = C_subroutine (rettype,param_types) in
  C_pointer ctype_subroutine
  
let rec eq_to_fp sctype =
  match sctype with
  | SC_pointer (SC_subroutine _) -> true
  | SC_typedef (defnm,sct) -> eq_to_fp sct
  | SC_const sct | SC_volatile sct -> eq_to_fp sct
  | SC_union (_,umemlist) ->
    List.exists (fun (ft,fn) -> 
      eq_to_fp ft
    ) umemlist
  | _ -> false
    
let rec collect_fps sctype struct_map =
  match sctype with
  | SC_base "void" -> 
    [SC_pointer (SC_subroutine ((SC_base "void"),[SC_unspecified_param]))]
  | SC_pointer (SC_base "void") -> 
    [SC_pointer (SC_subroutine ((SC_base "void"),[SC_unspecified_param]))]
  | SC_pointer (SC_subroutine _) -> [sctype]
  | SC_typedef (defnm,sct) ->
    if H.mem struct_map defnm then (
      let struct_entry = H.find struct_map defnm in
      match struct_entry.struct_type with
      | SC_structure (_,_,fdlist) ->
	collect_fps (fst (List.hd fdlist)) struct_map
      | _ -> []
    ) else collect_fps sct struct_map
  | SC_const sct | SC_volatile sct ->
    collect_fps sct struct_map
  | SC_union (_,umemlist) ->
    List.unique ?eq:(Some eq_sctype) (List.flatten (List.map (fun (ft,fn) ->
      collect_fps ft struct_map
    ) umemlist))
  | SC_structure (nm,_,fdlist) -> 
    if not (List.is_empty fdlist) then
      collect_fps (fst (List.hd fdlist)) struct_map
    else if H.mem struct_map nm then (
      let struct_entry = H.find struct_map nm in
      match struct_entry.struct_type with
      | SC_structure (_,_,fdlist) ->
	collect_fps (fst (List.hd fdlist)) struct_map
      | _ -> []
    ) else []
  | _ -> []
(*
let ic_handled = ref 0
let ic_from_dbg = ref 0
let ic_from_inf = ref 0
let ic_guess = ref 0
let ic_cmplr = ref 0
let ic_cmplr_dbg = ref 0
let ic_cmplr_inf = ref 0
let ic_types = ref 0

let ij_handled = ref 0
let ij_class = ref 0
let ij_tcall = ref 0
let ij_cmplr = ref 0
let ij_cmplr_class = ref 0
let ij_cmplr_tcall = ref 0
let tc_from_dbg = ref 0
let tc_from_inf = ref 0

let ij_conflict = ref 0
*)
let decide_type dbgmap eqcg struct_map target =
  let is_over_aprx ect =
    let noinfo = List.is_empty ect in
    let top =
      List.exists (fun sct ->
	match sct with
	| SC_top _ -> true
	| _ -> false
      ) ect
    in
    noinfo || top
  in
  let tv, edge = target in
  match tv.tv_name with
  | TV_imm imm -> (** Impossible for ICALL operand **)
    assert false
  | TV_addr addrnm ->
    let type_from_dbg = dbgtype_tag tv dbgmap in
    let ic_type = reason_ectype type_from_dbg [edge] struct_map in
    if not (is_over_aprx ic_type) then       
      (*ic_from_dbg := !ic_from_dbg + 1;*)
      0,ic_type
    else
      (-1),ic_type
  | _ ->
    try
      let tv_eqc = eqcg_find eqcg tv in
      let src, tv_type = 
	if List.is_empty tv_eqc.eqc_dbgtype then (
	  1, (reason_ectype tv_eqc.eqc_fflowtype [edge] struct_map)
	) 
	else (
	  0, (reason_ectype tv_eqc.eqc_dbgtype [edge] struct_map)
	)
      in
      if is_over_aprx tv_type then
        (-1), tv_type
      else if src = 1 || (fst edge) = 1 then
	1, tv_type
	(*ic_from_inf := !ic_from_inf + 1*)
      else
	0,tv_type
    (*ic_from_dbg := !ic_from_dbg + 1;
      tv_type*)
    with Not_found ->
      begin match tv.tv_name with
      | TV_addr _ | TV_imm _ -> 
	assert false
      | TV_cfa _ | TV_reg _ ->
	let type_from_dbg = dbgtype_tag tv dbgmap in
	let ic_type = reason_ectype type_from_dbg [edge] struct_map in
	if is_over_aprx ic_type then 
	  (-1),ic_type
	else if fst edge = 0 then
	  0, ic_type
	  (*ic_from_dbg := !ic_from_dbg + 1*)
	else
	  1, ic_type
	  (*ic_from_inf := !ic_from_inf + 1;*)
      end

let new_refine_cfg_within_fun typedef_map struct_map funCFA ultim_map asm_fun eqcg =
  P.printf "Start CFG refinement...\n"; flush stdout

(** TODO: more clever reasoning for inferred type  **)
let refine_cfg_within_fun elf asm_fun eqcg funCFA dbgmap struct_map teqcs default all_functions start =
  (** further configure targets of indirect calls and jumps **)
    List.iter (fun bb ->
      let _,ins,len = List.last bb.bb_instrs in
      let ibsite = bb.bb_relAddr + bb.bb_size - len in
      if does_bb_icall bb then (
	ic_handled := !ic_handled + 1;
	(*if String.exists asm_fun.func_name "." then 
	  ic_cmplr := !ic_cmplr + 1;*)
	let icall_target =
	  match ins with
	  | CALL(dis,abs,op,slctr) ->
	    begin match op with
	    | Imm_op _ -> assert false
	    | _ ->
	      let tvNameOp = 
		tvNameGen funCFA op ibsite 
	      in
	      mkTV tvNameOp ibsite false
	    end
	  | _ -> assert false
	in
	let src, target_ectype = 
	  decide_type dbgmap eqcg struct_map icall_target
	in
	let fp_types =
	  List.unique ?eq:(Some eq_sctype) (List.flatten (List.map (fun sct ->
	    collect_fps sct struct_map
       	  ) target_ectype))
	in
	if not (List.is_empty fp_types) then (
	  if src = 0 then (
	    (*if String.exists asm_fun.func_name "." then 
	      ic_cmplr_dbg := !ic_cmplr_dbg + 1;*)
	    ic_types := !ic_types + (List.length fp_types);
	    ic_from_dbg := !ic_from_dbg + 1
	  )
	  else if src = 1 then (
	    (*if String.exists asm_fun.func_name "." then 
	      ic_cmplr_inf := !ic_cmplr_inf + 1;*)
	    ic_types := !ic_types + (List.length fp_types);
	    ic_from_inf := !ic_from_inf + 1
	  )
	  else (
	    ic_guess := !ic_guess + 1;
	  );
	  if List.length fp_types > 1 then (
	    P.printf "One call site %x has multi-fp:\n" ibsite;
	    P.printf "%s\n" (str_of_ectype fp_types);
	    flush stdout;
	  );
	  let matched_funcs =
	    List.flatten (
	      List.filter_map (fun teqc -> 
		let teqc_type = ctype_of_teqc teqc in
		if List.exists (fun fp_sct -> 
		  let fp_ct = sctype_to_ctype fp_sct in
		  ctype_consistent fp_ct teqc_type
		) fp_types 
		then
		  Some teqc.t_eqc_member
		else
		  None
	      ) teqcs
	    )
	  in
	  let refined_icTargets = 
	    List.map (fun (relAddr, func_name) ->
	      str_of_mword_flex (MWord.(+%) (MWord.of_int relAddr) start)  
	    ) matched_funcs
	  in
	  bb.bb_succs <- refined_icTargets
	) else (
	  (*ic_types := !ic_types + default*)
	)
      )
      else if does_bb_ijump bb then (
	(*if String.exists asm_fun.func_name "." then 
	  ij_cmplr := !ij_cmplr + 1;*)
	ij_handled := !ij_handled + 1;
	let ijump_type, ijump_tv =
	  match ins with
	  | JMP(dis,abs,op,slctr) ->
	    begin match op with
	    | Imm_op _ -> assert false
	    | _ ->
	      let ijump_type = 
		ijclassify_global elf op ibsite start all_functions 
	      in
	      let tvNameOp = 
		tvNameGen funCFA op ibsite
	      in
	      ijump_type, (mkTV tvNameOp ibsite false)
	    end
	  | _ -> assert false
	in
	let src, target_ectype = 
	  decide_type dbgmap eqcg struct_map ijump_tv
	in
	
	let fp_types =
	  List.unique ?eq:(Some eq_sctype) (List.flatten (List.map (fun sct ->
	    collect_fps sct struct_map
	  ) target_ectype))
	in
	let ij_truetype =
	  match ijump_type with
	  | IJ_plt_t -> IJ_plt_t
	  | IJ_function_t ->
	    if List.is_empty fp_types then (
	      ij_conflict := !ij_conflict + 1;
	      IJ_unclassified_t
	    )
	    else (
	      (*ij_tcall := !ij_tcall + 1;*)
	      IJ_function_t
	    )
	  | IJ_local_t ->
	    if List.is_empty fp_types then (
	      (*if String.exists asm_fun.func_name "." then 
		ij_cmplr_class := !ij_cmplr_class + 1;*)
	      ij_class := !ij_class + 1;
	      IJ_local_t
	    )
	    else (
	      ij_conflict := !ij_conflict + 1;
	      IJ_unclassified_t
	    )
	  | IJ_unclassified_t ->
	    if List.is_empty fp_types then
	      IJ_unclassified_t
	    else (
	      (*ij_tcall := !ij_tcall + 1;*)
	      IJ_function_t
	    )
	in

	if not (List.is_empty fp_types) && (ij_truetype = IJ_function_t) then (
	  (*if String.exists asm_fun.func_name "." then 
	    ij_cmplr_tcall := !ij_cmplr_tcall + 1;*)
	  ij_tcall := !ij_tcall + 1;
	  if src = 0 then tc_from_dbg := !tc_from_dbg + 1
	  else if src = 1 then tc_from_inf := !tc_from_inf + 1;
	  let matched_funcs =
	    List.flatten (
	      List.filter_map (fun teqc -> 
		let teqc_type = ctype_of_teqc teqc in
		if List.exists (fun fp_sct ->
		  let fp_ct = sctype_to_ctype fp_sct in
		  ctype_consistent fp_ct teqc_type
		  ) fp_types then
		  Some teqc.t_eqc_member
		else
		  None
	      ) teqcs
	    )
	  in
	  let refined_ijTargets = 
	    List.map (fun (relAddr, func_name) ->
	      str_of_mword_flex (MWord.(+%) (MWord.of_int relAddr) start)  
	    ) matched_funcs
	  in
	  bb.bb_succs <- refined_ijTargets
	) else if ij_truetype = IJ_local_t then (
	  (*
	  bb.bb_succs <- 
	    List.filter (fun str ->
	      let rel_succ = get_relative_addr (MWord.of_string ("0x"^str)) start in
	      let fun_lopc = get_relative_addr (MWord.of_string ("0x"^asm_fun.func_low)) start in
	      let fun_hipc = get_relative_addr (MWord.of_string ("0x"^asm_fun.func_high)) start in
	      fun_lopc <= rel_succ && rel_succ < fun_hipc
	    (*
	      not (List.exists (fun (funclopc, funcname) ->
		str = (str_of_mword_flex (MWord.(+%) (MWord.of_int funclopc) start))
	      ) all_functions)*)
	    ) bb.bb_succs
	  *)
	) else (
	(** Conservative: do nothing; Aggressive: remove functions **)
			  
			  
	)
      )
      else
	()
    ) asm_fun.func_bbs


(*
let refine_cfg elf cr_list eqcg funCFA funDBG struct_map =
  (** further configure targets of indirect calls and jumps **)
  List.iter (fun cr ->
    let start = cr.startAddr in
    let all_functions = collect_all_functions elf start in
    let icTargets, ijTargets = collect_indirect_targets elf all_functions in
    let icTargets = List.filter (fun (loc,lbl) -> List.mem lbl icTargets) all_functions in
    let fsig = collect_funcsig_list elf start in
    let teqcs = collect_teqcs elf icTargets start fsig in 
    BBSet.iter (fun bb ->
      let _,ins,len = List.last bb.bb_instrs in
      let ibsite = bb.bb_relAddr + bb.bb_size - len in
      if does_bb_icall bb then (
	let icall_tv =
	  match ins with
	  | CALL(dis,abs,op,slctr) ->
	    begin match op with
	    | Imm_op _ -> assert false
	    | _ ->
	      let tvNameOp = 
		tvNameGen funCFA op ibsite 
	      in
	      mkTV tvNameOp ibsite false
	    end
	  | _ -> assert false
	in
	let target_types = 
	  eqcg_search funDBG eqcg struct_map [icall_tv]
	in
	let fp_types =
	  List.flatten (
	    List.map (fun (tv,ect) ->
	      List.filter_map (fun sct ->
		if eq_to_fp sct then
		  Some sct
		else
		  None
	      ) ect
	    ) target_types
	  )
	in
	if not (List.is_empty fp_types) then (
	  let matched_funcs =
	    List.flatten (
	      List.filter_map (fun teqc -> 
		let teqc_type = sctype_of_teqc teqc in
		if List.exists (eq_sctype teqc_type) fp_types then
		  Some teqc.t_eqc_member
		else
		  None
	      ) teqcs
	    )
	  in
	  let refined_icTargets = 
	    List.map (fun (relAddr, func_name) ->
	      str_of_mword_flex (MWord.(+%) (MWord.of_int relAddr) start)  
	    ) matched_funcs
	  in
	  bb.bb_succs <- refined_icTargets
	) else ()
      )
      else if does_bb_ijump bb then (
	let ijump_tv =
	  match ins with
	  | JMP(dis,abs,op,slctr) ->
	    begin match op with
	    | Imm_op _ -> assert false
	    | _ ->
	      let tvNameOp = 
		tvNameGen funCFA op ibsite
	      in
	      mkTV tvNameOp ibsite false
	    end
	  | _ -> assert false
	in
	let target_types = 
	  eqcg_search funDBG eqcg struct_map [ijump_tv]
	in
	let fp_types =
	  List.flatten (
	    List.map (fun (tv,ect) ->
	      List.filter_map (fun sct ->
		if eq_to_fp sct then
		  Some sct
		else
		  None
	      ) ect
	    ) target_types
	  )
	in
	if not (List.is_empty fp_types) then (
	  let matched_funcs =
	    List.flatten (
	      List.filter_map (fun teqc -> 
		let teqc_type = sctype_of_teqc teqc in
		if List.exists (eq_sctype teqc_type) fp_types then
		  Some teqc.t_eqc_member
		else
		  None
	      ) teqcs
	    )
	  in
	  let refined_icTargets = 
	    List.map (fun (relAddr, func_name) ->
	      str_of_mword_flex (MWord.(+%) (MWord.of_int relAddr) start)  
	    ) matched_funcs
	  in
	  bb.bb_succs <- refined_icTargets
	) else (
	(** Conservative: do nothing; Aggressive: remove functions **)
	(*
	  let local_ijtargets =
	  List.filter_map (fun succ_label ->
	  let succ_rel = get_relative_addr 
	  ) bb.bb_succs
	*)
	)
      )
      else
	()
    ) cr.bbs
  ) cr_list
*)
