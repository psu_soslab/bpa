open Batteries

open Elf
open X86Syntax

open Config
open BB
open Ibaux
open TyInfoRetrieval
open TypeEngineLib

let get_icij_op ins =
  match ins with
  | CALL(_,_,op,_)
  | JMP(_,_,op,_) ->
    begin match op with
    | Imm_op _ -> assert false
    | _ -> op
    end
  | _ -> assert false

let look_for_type typedef_map struct_map ultim_map eqcg target_tv target_edge =
  let from_inf = ref false in
  let from_dbg = ref false in
  let none = ref false in
  let optvnm = target_tv.tv_name in
  let auxtype = 
    if fst target_edge = 1 && snd target_edge <> "some" then
      begin match optvnm with
      | TV_reg reg ->
	let off_dec = int_of_32string (snd target_edge) in
	let off_dec_str = string_of_int off_dec in
	let auxtvnm = TV_addr (mkADNM off_dec_str (Some reg) None) in
	let auxtv = mkTV auxtvnm target_tv.tv_index false in
	ultim_tag auxtv ultim_map
      | _ -> []
      end
    else []
  in
  if List.is_empty auxtype then (
    try
    (** search from EQC Graph **)
      let tv_eqc = eqcg_find eqcg target_tv in
      if not (List.is_empty tv_eqc.eqc_dbgtype) then (
	from_dbg := true;
	reason_itlist tv_eqc.eqc_dbgtype [target_edge] typedef_map struct_map
      )
      else if not (List.is_empty tv_eqc.eqc_fflowtype) then (
	from_inf := true;
	reason_itlist tv_eqc.eqc_fflowtype [target_edge] typedef_map struct_map
      )
      else (
	none := true;
	[IT_top 0]
      )
    with Not_found ->
      (** search from DBG Map **)
      let type_from_dbg = ultim_tag target_tv ultim_map in
      let brancher_type = reason_itlist type_from_dbg [target_edge] typedef_map struct_map in
      if not (List.is_empty brancher_type) then
	from_dbg := true
      else
	none := true;
      brancher_type
  ) else auxtype

let refine_cfg_within_fun typedef_map struct_map union_map funCFA ultim_map asm_fun start eqcg =
  let verbose = false in
  let _,_,_,funsig_map = ultim_map in
  let rec find_typesig ctype =
    match ctype with
    | C_subroutine (rettype, paramlist) -> rettype, paramlist
    | C_pointer (_,sigtype) -> find_typesig sigtype
    | _ -> assert false
  in

  let rec find_pointee ctype =
    match ctype with
    | C_pointer (_,ct) ->
      ct
    | C_typedef (_,ct) | C_const ct | C_volatile ct ->
      find_pointee ct
    | _ ->
      assert false
  in

  let is_itype_subroutine itype =
    match itype with
    | IT_top _ -> false
    | IT_tblent -> false
    | IT_ptr it -> false
    | IT_off _ -> assert false
    | IT_elem (ctype, off) ->
      assert (off = 0);
      is_ctype_subroutine ctype
  in

  let is_itype_funptr itype =
    match itype with
    | IT_top reason_num -> reason_num <> 1
    | IT_ptr it -> is_itype_subroutine it
    | IT_tblent -> false
    | IT_off _ -> assert false
    | IT_elem (ctype, off) -> 
      if off <> 0 then (
	P.printf "%s with offset %d\n" 
	  (str_of_c_type ctype) off;
	flush stdout;
	assert false;
      ) else
      if is_ctype_ptr ctype then
	if is_ctype_subroutine (find_pointee ctype) 
	then true
	else is_ctype_top (find_pointee ctype)
      else is_ctype_top ctype
  in

  let is_itype_tblent itype =
    match itype with
    | IT_top _ -> false
    | IT_ptr it -> false
    | IT_tblent -> true
    | _ -> false
  in

  let check_brancher_type brancher_itlist =
    if List.exists is_itype_funptr brancher_itlist && 
      List.exists is_itype_tblent brancher_itlist
    then
      let errmsg = 
	P.sprintf "Not consistent brancher type list:\n%s\n" (str_of_itlist brancher_itlist)
      in
      raise (Failure errmsg)
  in

  let rec mcfi_consis_ret init_caller_ret init_callee_ret =
    let rec aux caller_ret callee_ret =
      match caller_ret, callee_ret with
      | C_const ctype, _ 
      | C_volatile ctype, _ -> aux ctype callee_ret
      | _, C_const ctype
      | _, C_volatile ctype -> aux caller_ret ctype
      | C_typedef (defnm,ctype), _ -> 
	if H.mem typedef_map defnm then
	  aux (H.find typedef_map defnm) callee_ret
	else if H.mem union_map defnm then
	  aux (H.find union_map defnm) callee_ret
	else
	  aux ctype callee_ret
      | _, C_typedef (defnm,ctype) -> 
	if H.mem typedef_map defnm then
	  aux caller_ret (H.find typedef_map defnm)
	else if H.mem union_map defnm then
	  aux caller_ret (H.find union_map defnm)
	else
	  aux caller_ret ctype
      | C_void, _
      | _, C_void -> true
      | C_base (size1,_), C_base (size2,_) ->
	size1 = size2
      | C_array _, _ | _, C_array _ -> assert false
      | C_pointer (_,ctype1), C_pointer (_,ctype2) ->
	aux ctype1 ctype2
      | C_subroutine (ret1, params1), C_subroutine (ret2, params2) ->
	mcfi_consis_ret ret1 ret2 && mcfi_consis_params params1 params2
      | C_enumeration (_,nm1,etors1), C_enumeration (_,nm2,etors2) ->
	List.length etors1 = List.length etors2
      | C_union (size1,nm1,fdlist1), C_union (size2,nm2,fdlist2) ->
	if nm1 = nm2 && nm1 <> "" then true
	else size1 = size2
      | C_union (size,nm,fdlist), _ ->
	if not (List.is_empty fdlist) then
	  List.exists (fun (fd,fn) -> aux fd callee_ret) fdlist
	else if H.mem union_map nm then
	  aux (H.find union_map nm) callee_ret
	else (
	  try
	    size <= (size_of_ctype typedef_map struct_map callee_ret)
	  with _ -> false
	)
      |_, C_union (size,nm,fdlist) ->
	if not (List.is_empty fdlist) then
          List.exists (fun (fd,fn) -> aux caller_ret fd) fdlist
        else if H.mem union_map nm then
          aux caller_ret (H.find union_map nm)
        else (
          try
            size >= (size_of_ctype typedef_map struct_map caller_ret)
          with _ -> false
        )
      | C_structure (size1,nm1,fdlist1), C_structure (size2,nm2,fdlist2) ->
	if nm1 = nm2 && nm1 <> "" then true
	else if List.is_empty fdlist1 then
	  if nm1 = "" then assert false
	  else aux (H.find struct_map nm1) callee_ret
	else if List.is_empty fdlist2 then
	  if nm2 = "" then assert false
	  else aux caller_ret (H.find struct_map nm2)
	else if List.length fdlist1 = List.length fdlist2 then
	  let consis = ref false in
	  List.iteri (fun index (off,ft,fn) ->
	    let _,ft2,_ = List.nth fdlist2 index in
	    consis := !consis && aux ft ft2
	  ) fdlist1;
	  !consis
	else false
      | C_unspecified_param, _ 
      | _, C_unspecified_param -> assert false
      | C_wrong_type _, _
      | _, C_wrong_type _ -> assert false
      | _, _ -> false
    in
    if is_ctype_void init_caller_ret && is_ctype_void init_callee_ret then true
    else if is_ctype_void init_caller_ret || is_ctype_void init_callee_ret then false
    else aux init_caller_ret init_callee_ret

  and mcfi_consis_params init_caller_params init_callee_params =
    let rec consis_param caller_param callee_param =
      match caller_param, callee_param with
      | C_const ctype, _ 
      | C_volatile ctype, _ -> consis_param ctype callee_param
      | C_typedef (defnm,ctype), _ ->
	if H.mem typedef_map defnm then
	  consis_param (H.find typedef_map defnm) callee_param
	else if H.mem union_map defnm then
	  consis_param (H.find union_map defnm) callee_param
	else
	  consis_param ctype callee_param
      | _, C_const ctype
      | _, C_volatile ctype -> consis_param caller_param ctype
      | _, C_typedef (defnm,ctype) ->
	if H.mem typedef_map defnm then
	  consis_param caller_param (H.find typedef_map defnm)
	else if H.mem union_map defnm then
	  consis_param caller_param (H.find union_map defnm)
	else
	  consis_param caller_param ctype
      | C_void, _
      | _, C_void -> true
      | C_base (size1,nm1), C_base (size2,nm2) ->
	size1 = size2
      | C_array (ctype1,dimlist1), C_array (ctype2,dimlist2) ->
	consis_param ctype1 ctype2 && 
	  (calc_array_bound dimlist1) >= (calc_array_bound dimlist2)
      | C_array (ctype1,_), C_pointer (_,ctype2)
      | C_pointer (_,ctype1), C_array (ctype2,_) ->
	consis_param ctype1 ctype2
      | C_pointer (_,ctype1), C_pointer (_,ctype2) ->
	consis_param ctype1 ctype2
      | C_subroutine (ret1, params1), C_subroutine (ret2, params2) ->
	mcfi_consis_ret ret1 ret2 && 
	  mcfi_consis_params params1 params2
      | C_enumeration (_,nm1,etors1), C_enumeration (_,nm2,etors2) -> 
	(List.length etors1 = List.length etors2)
      | C_union (size1,nm1,fdlist1), C_union (size2,nm2,fdlist2) ->
	if nm1 = nm2 && nm1 <> "" then true
	else size1 = size2
      | C_union (size,nm,fdlist), _ ->
	if not (List.is_empty fdlist) then
	  List.exists (fun (fd,fn) -> consis_param fd callee_param) fdlist
	else if H.mem union_map nm then
	  consis_param (H.find union_map nm) callee_param
	else (
	  try
	    size <= (size_of_ctype typedef_map struct_map callee_param)
	  with _ -> false
	)
      | _, C_union (size,nm,fdlist) ->
	if not (List.is_empty fdlist) then
          List.exists (fun (fd,fn) -> consis_param caller_param fd) fdlist
        else if H.mem union_map nm then
          consis_param caller_param (H.find union_map nm)
        else (
          try
            size >= (size_of_ctype typedef_map struct_map caller_param)
          with _ -> false
        )
      | C_structure (size1,nm1,fdlist1), C_structure (size2,nm2,fdlist2) ->
	if nm1 = nm2 && nm1 <> "" then true
	else if List.is_empty fdlist1 then
	  if nm1 = "" then assert false
	  else consis_param (H.find struct_map nm1) callee_param
	else if List.is_empty fdlist2 then
	  if nm2 = "" then assert false
	  else consis_param caller_param (H.find struct_map nm2)
	else if List.length fdlist1 = List.length fdlist2 then
	  let consis = ref true in
	  List.iteri (fun index (off,ft,fn) ->
	    let _,ft2,_ = List.nth fdlist2 index in
	    consis := !consis && consis_param ft ft2
	  ) fdlist1;
	  !consis
	else false
      | C_unspecified_param, _ -> true
      | _, C_unspecified_param -> true
      | C_wrong_type _, _
      | _, C_wrong_type _ -> assert false
      | _, _ -> false
    in

    let rec aux caller_params callee_params=
      match caller_params with
      | [] ->
	begin match callee_params with
	| [] -> true
	| _ -> false
	end
      | hd :: tl ->
	if hd = C_unspecified_param then true
	else
	  begin match callee_params with
	  | [] -> false
	  | h :: t ->
	    consis_param hd h && aux tl t
	  end
    in
    aux init_caller_params init_callee_params
  in

  let rec consis_ret typedef_map struct_map init_caller_ret init_callee_ret =
    let rec aux caller_ret callee_ret =
      match caller_ret, callee_ret with
      | C_const ctype, _ 
      | C_volatile ctype, _ -> aux ctype callee_ret
      | C_typedef (defnm,ctype), _ -> 
	if H.mem union_map defnm then
	  aux (H.find union_map defnm) callee_ret
	else if H.mem typedef_map defnm then
	  aux (H.find typedef_map defnm) callee_ret
	else
	  aux ctype callee_ret
      | _, C_const ctype
      | _, C_volatile ctype -> aux caller_ret ctype
      | _, C_typedef (defnm,ctype) -> 
	if H.mem union_map defnm then
	  aux caller_ret (H.find union_map defnm)
	else if H.mem typedef_map defnm then
	  aux caller_ret (H.find typedef_map defnm)
	else
	  aux caller_ret ctype
      | C_void, _ 
      | _, C_void -> true
      | C_base (size1,nm1), C_base (size2,nm2) ->
	size1 = size2 && nm1 = nm2
      | C_array _, _ | _, C_array _ -> assert false
      | C_pointer (_,ctype1), C_pointer (_,ctype2) ->
	aux ctype1 ctype2
      | C_subroutine (ret1, param1), C_subroutine (ret2, param2) ->
	aux ret1 ret2 && consis_params typedef_map struct_map param1 param2
      | C_enumeration (_,nm1,etors1), C_enumeration (_,nm2,etors2) -> 
	nm1 = nm2 
	&& (List.hd etors1 = List.hd etors2) 
	&& (List.length etors1 = List.length etors2)
      | C_union (size1,nm1,fdlist1), C_union(size2,nm2,fdlist2) ->
	if size1 <> size2 then false
	else
	  if nm1 <> "" && nm2 <> "" then
	    nm1 = nm2
	  else
	    if not (List.is_empty fdlist1) && not (List.is_empty fdlist2) then
	      if List.length fdlist1 = List.length fdlist2 then
		List.fold_left2 (fun b fd1 fd2 ->
		  b && eq_ct fd1 fd2
		) true fdlist1 fdlist2
	      else false
	    else false
      | C_union (size,nm,fdlist), _ ->
	if List.is_empty fdlist then
	  if nm <> "" then
	    if H.mem union_map nm then
	      aux (H.find union_map nm) callee_ret
	    else true
	  else true
	else
	  List.exists (fun (fd,fn) -> eq_ct fd callee_ret) fdlist
      | _, C_union (size,nm,fdlist) ->
	if List.is_empty fdlist then
	  if nm <> "" then
	    if H.mem union_map nm then
	      aux caller_ret (H.find union_map nm)
	    else true
	  else true
	else
	  List.exists (fun (fd,fn) -> eq_ct fd caller_ret) fdlist
      | C_structure (size1,nm1,fdlist1), C_structure (size2,nm2,fdlist2) ->
	size1 >= size2
      | C_unspecified_param, _ 
      | _, C_unspecified_param -> assert false
      | C_wrong_type _, _
      | _, C_wrong_type _ -> assert false
      | _, _ -> false
    in
    if is_ctype_void init_caller_ret && is_ctype_void init_callee_ret then true
    else if is_ctype_void init_caller_ret || is_ctype_void init_callee_ret then false
    else aux init_caller_ret init_callee_ret

  and consis_params typedef_map struct_map init_caller_params init_callee_params =
    let rec consis_param caller_param callee_param =
      match caller_param, callee_param with
      | C_const ctype, _ 
      | C_volatile ctype, _
      | C_typedef (_,ctype), _ -> consis_param ctype callee_param
      | _, C_const ctype
      | _, C_volatile ctype
      | _, C_typedef (_,ctype) -> consis_param caller_param ctype
      | C_void, _ -> true
      | C_base (size1,nm1), C_base (size2,nm2) ->
	size1 = size2 && nm1 = nm2
      | C_array (ctype1,dimlist1), C_array (ctype2,dimlist2) ->
	consis_param ctype1 ctype2 && 
	  (calc_array_bound dimlist1) <= (calc_array_bound dimlist2)
      | C_pointer (_,ctype1), C_array (ctype2, _) ->
	consis_param ctype1 ctype2
      | C_pointer (_,ctype1), C_pointer (_,ctype2) ->
	consis_param ctype1 ctype2
      | C_subroutine (ret1, param1), C_subroutine (ret2, param2) ->
	consis_ret typedef_map struct_map ret1 ret2 && 
	  consis_params typedef_map struct_map param1 param2
      | C_enumeration (_,nm1,etors1), C_enumeration (_,nm2,etors2) -> 
	nm1 = nm2 
	&& (List.hd etors1 = List.hd etors2) 
	&& (List.length etors1 = List.length etors2)
      | C_union (size1,nm1,fdlist1), C_union(size2,nm2,fdlist2) ->
	if size1 <> size2 then false
	else
	  if nm1 <> "" && nm2 <> "" then
	    nm1 = nm2
	  else
	    if not (List.is_empty fdlist1) && not (List.is_empty fdlist2) then
	      if List.length fdlist1 = List.length fdlist2 then
		List.fold_left2 (fun b fd1 fd2 ->
		  b && eq_ct fd1 fd2
		) true fdlist1 fdlist2
	      else false
	    else false
      | C_union (size,nm,fdlist), _ ->
	if List.is_empty fdlist then
	  if nm <> "" then
	    if H.mem union_map nm then
	      consis_param (H.find union_map nm) callee_param
	    else true
	  else true
	else
	  List.exists (fun (fd,fn) -> eq_ct fd callee_param) fdlist
      | _, C_union (size,nm,fdlist) ->
	if List.is_empty fdlist then
	  if nm <> "" then
	    if H.mem union_map nm then
	      consis_param caller_param (H.find union_map nm)
	    else true
	  else true
	else
	  List.exists (fun (fd,fn) -> eq_ct fd caller_param) fdlist
      | C_structure (size1,nm1,fdlist1), C_structure (size2,nm2,fdlist2) ->
        size1 >= size2
      | C_unspecified_param, _ -> true
      | _, C_unspecified_param -> false
      | C_wrong_type _, _
      | _, C_wrong_type _ -> assert false
      | _, _ -> false
    in

    let rec aux caller_params callee_params=
      match caller_params with
      | [] ->
	begin match callee_params with
	| [] -> true
	| _ -> false
	end
      | hd :: tl ->
	if hd = C_unspecified_param then true
	else
	  begin match callee_params with
	  | [] -> false
	  | h :: t ->
	    consis_param hd h && aux tl t
	  end
    in
    aux init_caller_params init_callee_params
  in

  let consis_caller_callee caller_itype callee_ctype =
    if verbose then (
      P.printf "matching signatures:\n"; 
      P.printf "CALLER:\n%s\n" (str_of_it caller_itype);
      P.printf "CALLEE:\n%s\n" (str_of_c_type callee_ctype);
      flush stdout
    );
    match caller_itype with
    | IT_top _ -> true
    | IT_tblent -> false
    | IT_ptr it -> 
      begin match it with
      | IT_elem (ctype,off) ->
        if is_ctype_subroutine ctype then
	  let caller_ret, caller_params = find_typesig ctype in
	  let callee_ret, callee_params = find_typesig callee_ctype in
	  mcfi_consis_ret caller_ret callee_ret &&
	    mcfi_consis_params caller_params callee_params
	else is_ctype_top ctype
      | _ -> false
      end
    | IT_off _ -> assert false
    | IT_elem (ctype,off) ->
      if is_ctype_ptr ctype then
	let pointee = find_pointee ctype in
	if is_ctype_subroutine pointee then
	  let caller_ret, caller_params = find_typesig pointee in
	  let callee_ret, callee_params = find_typesig callee_ctype in
	  if verbose then 
	    P.printf "Comparing return types...\nCALLER_RET:\n%s\nCALLEE_RET:\n%s\n"
	      (str_of_c_type caller_ret) (str_of_c_type callee_ret);
	  if mcfi_consis_ret caller_ret callee_ret then (
	    if verbose then P.printf "RETURN MATCHED!\n";
	    if mcfi_consis_params caller_params callee_params then (
	      if verbose then P.printf "PARAMS MATCHED!\n";
	      true
	    )
	    else (
	      if verbose then P.printf "PARAMS NOT MATCHED!\n";
	      false
	    )	      
	  ) else (
	    if verbose then P.printf "RETURN NOT MATCHED!\n";
	    false
	  )
	else is_ctype_top pointee
      else is_ctype_top ctype   
  in

  let match_funcs caller_itlist =
    List.unique (
      List.flatten (
	List.map (fun itype ->
	  let matched_funcs = ref [] in
	  H.iter (fun lopcstr sigctype ->
	    (*P.printf "Matching function %s...\n" lopcstr;*)
	    if consis_caller_callee itype sigctype then (
	      if verbose then P.printf "MATCHED!\n";
	      matched_funcs := lopcstr :: !matched_funcs
	    )
	    else 
	      if verbose then P.printf "NOT MATCHED!\n"
	  ) funsig_map;
	  flush stdout;
	  !matched_funcs
	) caller_itlist
      )
    )
  in

  let match_ijTargets jumper_itlist = 
    if List.exists (fun itype -> 
      match itype with
      | IT_tblent -> true
      | _ -> false
    ) jumper_itlist then
      []
    else match_funcs jumper_itlist
  in

  let work_on_ibranch ins ibsite match_proc =
    (*P.printf "refining on ibranch at %x\n" ibsite;*)
    let ibop = get_icij_op ins in
    let ibopnm,ibopedge = tvNameGen funCFA ibop ibsite in
    let iboptv = mkTV ibopnm ibsite false in
    let brancher_type = look_for_type typedef_map struct_map ultim_map eqcg iboptv ibopedge in
    check_brancher_type brancher_type;
    let matched_ibTargets = match_proc brancher_type in
    matched_ibTargets
  in

  (*P.printf "Start CFG refinement...\n"; flush stdout;*)
  List.iter (fun bb ->
    (*P.printf "Working on BB: %s in FUN: %s\n" bb.bb_label asm_fun.func_name; flush stdout;*)
    let _,ins,len = List.last bb.bb_instrs in
    let ibsite = bb.bb_relAddr + bb.bb_size - len in
    if does_bb_icall bb then
      let refined_icTargets = work_on_ibranch ins ibsite match_funcs in
      if List.is_empty refined_icTargets then ()
      else bb.bb_succs <- refined_icTargets
    else if does_bb_ijump bb then
      let refined_ijTargets = work_on_ibranch ins ibsite match_ijTargets in
      if List.is_empty refined_ijTargets then
	(** remove functions **)
	bb.bb_succs <- List.filter (fun label -> 
	  not (H.mem funsig_map label)
	) bb.bb_succs
      else bb.bb_succs <- refined_ijTargets
    else ()
  ) asm_fun.func_bbs
