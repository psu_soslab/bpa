open Batteries
open Bigarray
module H = Hashtbl
module P = Printf

open IO

open Util
open Config

open Bits
open X86Syntax
open Abbrev
open Instr
open Elf
open BB


let print_edge flow_fact edge_fact enter_node_fact bb =
  if bb.bb_symbol_label = "main" then
  P.fprintf enter_node_fact "%s\tmain" (str_of_mword_dec (MWord.of_int (bb.bb_relAddr))) else ();

  let instrs = instrs_with_starts (bb.bb_relAddr) bb.bb_instrs in
  let last_instr = List.hd (List.rev instrs) in

  let edge_facts = MakeFacts.gen_edge_fact instrs in



  let succ_edge_facts = MakeFacts.gen_succ_edge_fact last_instr bb.bb_succs in
  List.iter (fun flow -> P.fprintf flow_fact "%s" flow) edge_facts;
  List.iter (fun succ_edge -> P.fprintf edge_fact "%s" succ_edge) succ_edge_facts


let print_edge_main bbtbl =
  let flow_fact = open_out "flow.facts" in
  let edge_fact = open_out "edge.facts" in
  let enter_node_fact = open_out "enter_node.facts" in
  let bb_addr_bound = open_out "bb_addr_bound.facts" in
  let bb_addr_bound_instr = open_out "bb_addr_bound_instr.facts" in

  H.iter (fun lbl bb ->
    print_edge flow_fact edge_fact enter_node_fact bb;
	let (_, _, _, len) = List.hd (List.rev (instrs_with_starts (bb.bb_relAddr) bb.bb_instrs)) in
	let higher_bound = bb.bb_relAddr + bb.bb_size - len in 
    P.fprintf bb_addr_bound "%s" (string_of_int(bb.bb_relAddr) ^ "\t" ^ string_of_int(higher_bound) ^ "\n");
  
    List.iter (fun (loc, pre, ins, len) -> 
       P.fprintf bb_addr_bound_instr "%s" (string_of_int(bb.bb_relAddr) ^ "\t" ^ (str_of_mword_dec (MWord.of_int loc)) ^ "\n");
    )  (instrs_with_starts (bb.bb_relAddr) bb.bb_instrs);	

  ) bbtbl;

  close_out flow_fact;
  close_out edge_fact;
  close_out enter_node_fact;
  close_out bb_addr_bound;
  close_out bb_addr_bound_instr





