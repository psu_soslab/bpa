open Batteries
open X86Syntax
open X86Semantics
open RTL
open Big
open Printf
open Config
open Abbrev

open BB

module P = Printf
module H = Hashtbl




let rec prune_cast re =
  match re with
  | X86_RTL.Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    begin match bvop with
    | X86_RTL.Coq_shl_op 
    | X86_RTL.Coq_shr_op 
    | X86_RTL.Coq_shru_op
    | X86_RTL.Coq_or_op -> prune_cast re1
    | _ ->
      X86_RTL.Coq_arith_rtl_exp(s,bvop,prune_cast re1,prune_cast re2)
    end
  | X86_RTL.Coq_test_rtl_exp(s,top,re1,re2) ->
    X86_RTL.Coq_test_rtl_exp(s,top,prune_cast re1,prune_cast re2)
  | X86_RTL.Coq_if_rtl_exp(s, ge,te,ee) ->
    X86_RTL.Coq_if_rtl_exp(s, prune_cast ge, prune_cast te, prune_cast ee)
  | X86_RTL.Coq_cast_s_rtl_exp(s,_,re') -> 
    prune_cast re' 
  | X86_RTL.Coq_cast_u_rtl_exp(s,_,re') -> 
    prune_cast re'
  | X86_RTL.Coq_imm_rtl_exp(s,_)
  | X86_RTL.Coq_get_loc_rtl_exp(s,_)
  | X86_RTL.Coq_get_ps_reg_rtl_exp(s,_) 
  | X86_RTL.Coq_get_array_rtl_exp(_,s,_,_) ->
    re
  | X86_RTL.Coq_get_byte_rtl_exp(re') ->
    X86_RTL.Coq_get_byte_rtl_exp(prune_cast re')
  | X86_RTL.Coq_get_random_rtl_exp(s) ->
    re
  | _ ->
    assert false



let rec get_byte_prune_cast re =
  match re with
  | X86_RTL.Coq_arith_rtl_exp(s,bvop,re1,re2) ->
      X86_RTL.Coq_arith_rtl_exp(s,bvop,get_byte_prune_cast re1, get_byte_prune_cast re2)
  | X86_RTL.Coq_test_rtl_exp(s,top,re1,re2) ->
    X86_RTL.Coq_test_rtl_exp(s,top,get_byte_prune_cast re1, get_byte_prune_cast re2)
  | X86_RTL.Coq_if_rtl_exp(s, ge,te,ee) ->
    X86_RTL.Coq_if_rtl_exp(s, get_byte_prune_cast ge, get_byte_prune_cast te, get_byte_prune_cast ee)
  | X86_RTL.Coq_cast_s_rtl_exp(s,_,re') -> 
	get_byte_prune_cast re'
  | X86_RTL.Coq_cast_u_rtl_exp(s,_,re') -> 
	get_byte_prune_cast re'

  | X86_RTL.Coq_imm_rtl_exp(s,_)
  | X86_RTL.Coq_get_loc_rtl_exp(s,_)
  | X86_RTL.Coq_get_ps_reg_rtl_exp(s,_) 
  | X86_RTL.Coq_get_array_rtl_exp(_,s,_,_) ->
    re

  | X86_RTL.Coq_get_byte_rtl_exp(re') ->
    X86_RTL.Coq_get_byte_rtl_exp(prune_cast re')
  | X86_RTL.Coq_get_random_rtl_exp(s) ->
    re
  | _ ->
    assert false







let remove_instrs lst =
  let ret_lst = [] in
  let rec iterate counter lst ret_lst =
    match lst with
	|[] -> ret_lst
	|instr::l ->
	  begin match instr with
	   | X86_RTL.Coq_if_rtl(rtl_e, rtl_i') ->
	      begin match rtl_i' with
		  | X86_RTL.Coq_trap_rtl ->
		    iterate counter l ret_lst
		  | _ -> iterate counter l (ret_lst@[instr])
		  end
	   | X86_RTL.Coq_set_byte_rtl(rtl_e1, _) ->
	 	 begin match rtl_e1 with
 		  | X86_RTL.Coq_cast_u_rtl_exp(_,_,_)->
		  	if counter = 0 then iterate 1 l (ret_lst@[instr]) (*** add first one from 4 different rtl_instr**)
			else iterate counter l ret_lst
	      | _ -> iterate counter l (ret_lst@[instr])
		 end
	   | _ -> iterate counter l (ret_lst@[instr])
	  end
  in iterate 0 lst ret_lst




let size_bit_to_byte_str size_bit = 
  let size_byte = (((Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int size_bit))) + 1) / 8) in
  (string_of_int size_byte)


let simplify_rtl_exp rtl_e = 
  match rtl_e with
   | X86_RTL.Coq_arith_rtl_exp(_, _, rtl_e1, rtl_e2) ->		 
     begin match rtl_e1, rtl_e2 with
	 | X86_RTL.Coq_cast_u_rtl_exp(_,cast_size,rtl_e'), _ ->
	   begin match rtl_e' with
	     | X86_RTL.Coq_get_byte_rtl_exp(rtl_e'') ->
		   let str_cast_size = size_bit_to_byte_str cast_size in
		   (rtl_e'',1, str_cast_size)  (** EBP <-- ((CastU<8,32> (ESP + 32)) | (((CastU<8,32> (ESP + 33)) | (((CastU<8,32> (ESP + 34)) | ((CastU<8,32> (ESP + 35)) << 8)) << 8)) << 8)) **)

	   	 | _ -> (rtl_e,0, "0")
	   end
	 | _, X86_RTL.Coq_arith_rtl_exp(_,_,rtl_e',_) ->
	   begin match rtl_e' with
	   | X86_RTL.Coq_cast_u_rtl_exp(_,cast_size,rtl_e'') -> 
		 begin match rtl_e'' with
	     | X86_RTL.Coq_get_byte_rtl_exp(rtl_e''') ->
		   let str_cast_size = size_bit_to_byte_str cast_size in

		   (rtl_e''',4, str_cast_size)  (**(EAX - ((CastU<8,32> (EBP + 12)) | (((CastU<8,32> (EBP + 13)) | (((CastU<8,32> (EBP + 14)) | ((CastU<8,32> (EBP + 15)) << 8)) << 8)) << 8)))**)
					     (**only return *(ebp+ 12) **)
		 | _ -> (rtl_e,0, "0")
		 end
	   | _ -> (rtl_e,0, "0")
	   end
     | _ -> (rtl_e,0, "0")
	 end




   | X86_RTL.Coq_cast_u_rtl_exp(_,_,rtl_exp) ->
	 begin match rtl_exp with
	 | X86_RTL.Coq_arith_rtl_exp(_, _, rtl_exp1, _) ->
	   begin match rtl_exp1 with
	   | X86_RTL.Coq_cast_u_rtl_exp(_,cast_size,rtl_e') ->
		 begin match rtl_e' with
	      | X86_RTL.Coq_get_byte_rtl_exp(rtl_e'') ->
			let str_cast_size = size_bit_to_byte_str cast_size in
			(rtl_e'', 3, str_cast_size)  
 	 	
						(***(ESP - 4) <-- (CastU<32,8> ((CastU<8,32> *(ESP + 44)) | (((CastU<8,32> (ESP + 45)) | (((CastU<8,32> (ESP + 46)) | ((CastU<8,32> (ESP + 47)) << 8)) << 8)) << 8)))**)
						(**(32)EAX <-- (CastU<16,32> ((CastU<8,16> EAX) | ((CastU<8,16> (EAX + 1)) << 8))) **)


		  | _ -> (rtl_e,0, "0")
		 end
	   | X86_RTL.Coq_arith_rtl_exp(_, _, _, _) ->
		 (rtl_exp, 2, "4")

						(**
						*(EBP + -8) <-- (CastU<32,8> (((CastU<8,32> *(EBP + -8)) | (((CastU<8,32> (EBP + -7)) | (((CastU<8,32> (EBP + -6)) | ((CastU<8,32> (EBP + -5)) << 8)) << 8)) << 8)) + 8))

						**)




		 (**(rtl_e,0, "0")**)

	   | _ -> (rtl_e,0, "0")
	   end
	 | X86_RTL.Coq_cast_u_rtl_exp(cast_size, _,rtl_exp11) ->
	  let str_cast_size = size_bit_to_byte_str cast_size in
	  if str_cast_size = "8" then 
		(rtl_exp11,1, str_cast_size)   (**    (32)EAX <-- (CastU<8,32> (CastU<32,8> ECX))             **)
  									   (**    (32)EAX <-- (CastU<8,32> (CastU<8,8> (EBP + -124)))   *)
	  else (rtl_e, 0, "0")





	 | _ -> (rtl_exp,1, "4") (*** e.g)  *(EBP + -12) <-- (CastU<32,8> EAX) ==> *(EBP + -12) <-- EAX  **)
	 end
   | _ -> (rtl_e,0, "0")


(**
EAX <-- (CastU<8,32> (CastU<32,8> EAX))

________________________________
 90590 3	   	movzx eax, al
(32)EAX <-- (CastU<8,32> (CastU<32,8> EAX))
________________________________

 7986 3	   	movzx eax, BYTE PTR  [eax]
IF (DS <u EAX) DO
	TrapInstr 
(32)EAX <-- (CastU<8,32> (CastU<8,8> EAX))
***)


(*********************************************
let simplify_rtl_exp rtl_e = 
  match rtl_e with
   | X86_RTL.Coq_arith_rtl_exp(_, _, rtl_e1, rtl_e2) ->		 
     begin match rtl_e1, rtl_e2 with
	 | X86_RTL.Coq_cast_u_rtl_exp(_,_,rtl_e'), _ ->
	   begin match rtl_e' with
	     | X86_RTL.Coq_get_byte_rtl_exp(rtl_e'') ->
		   (rtl_e'',1)  (** EBP <-- ((CastU<8,32> (ESP + 32)) | (((CastU<8,32> (ESP + 33)) | (((CastU<8,32> (ESP + 34)) | ((CastU<8,32> (ESP + 35)) << 8)) << 8)) << 8)) **)

	   	 | _ -> (rtl_e,0)
	   end
	 | _, X86_RTL.Coq_arith_rtl_exp(_,_,rtl_e',_) ->
	   begin match rtl_e' with
	   | X86_RTL.Coq_cast_u_rtl_exp(_,_,rtl_e'') -> 
		 begin match rtl_e'' with
	     | X86_RTL.Coq_get_byte_rtl_exp(rtl_e''') ->
		   (rtl_e''',4)  (**(EAX - ((CastU<8,32> (EBP + 12)) | (((CastU<8,32> (EBP + 13)) | (((CastU<8,32> (EBP + 14)) | ((CastU<8,32> (EBP + 15)) << 8)) << 8)) << 8)))**)
					     (**only return *(ebp+ 12) **)
		 | _ -> (rtl_e,0)
		 end
	   | _ -> (rtl_e,0)
	   end
     | _ -> (rtl_e,0)
	 end




   | X86_RTL.Coq_cast_u_rtl_exp(_,_,rtl_exp) ->
	 begin match rtl_exp with
	 | X86_RTL.Coq_arith_rtl_exp(_, _, rtl_exp1, _) ->
	   begin match rtl_exp1 with
	   | X86_RTL.Coq_cast_u_rtl_exp(_,cast_size2,rtl_e') ->
		 begin match rtl_e' with
	      | X86_RTL.Coq_get_byte_rtl_exp(rtl_e'') ->
	        if (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int cast_size2))) = 15 then
			(rtl_e'', 5) (********* Sun:  worry about get_mem_rtl_exp size!!!!!!!!!!!!!!!!!!!!!!! ***********)
		    else
			(rtl_e'',3) (***(ESP - 4) <-- (CastU<32,8> ((CastU<8,32> *(ESP + 44)) | (((CastU<8,32> (ESP + 45)) | (((CastU<8,32> (ESP + 46)) | ((CastU<8,32> (ESP + 47)) << 8)) << 8)) << 8)))**)
						

						(**
						*(EBP + -8) <-- (CastU<32,8> (((CastU<8,32> *(EBP + -8)) | (((CastU<8,32> (EBP + -7)) | (((CastU<8,32> (EBP + -6)) | ((CastU<8,32> (EBP + -5)) << 8)) << 8)) << 8)) + 8))

						**)

		  | _ -> (rtl_e,0)
		 end
	   | X86_RTL.Coq_arith_rtl_exp(_, _, _, _) ->
		 (rtl_exp, 2)

	   | _ -> (rtl_e,0)
	   end
	 | X86_RTL.Coq_cast_u_rtl_exp(cast_size, cast_size2 ,rtl_exp11) ->
	   (*(rtl_exp1,1)*)
	  if (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int cast_size))) = 31 then
  
		(rtl_exp11,1)   (**    (32)EAX <-- (CastU<8,32> (CastU<32,8> ECX))             **)
	  (*else if (Util.int_of_int32 (Int64.to_int32 (MWord.of_big_int cast_size2))) = 15 then*)

      else if (str_of_size cast_size2) = "16" then
		(rtl_exp11,5)   (**    (32)EAX <-- (CastU<16,32> ((CastU<8,16> EAX) )   **)
	

	  else 
		(rtl_exp11,0)  (**    (32)EAX <-- (CastU<8,32> (CastU<8,8> (EBP + -124)))   *)
		(*(rtl_e,0)*)

	 | _ -> (rtl_exp,1) (*** e.g)  *(EBP + -12) <-- (CastU<32,8> EAX) ==> *(EBP + -12) <-- EAX  **)
	 end
   | _ -> (rtl_e,0)

******************************************************)





