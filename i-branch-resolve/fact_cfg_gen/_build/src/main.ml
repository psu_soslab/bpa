open Batteries
open BatOptParse
open Printf
module H = Hashtbl
module F = Format

open X86Syntax
open X86Semantics
open Elf
open Config

open BB
open Advdis
open CFGGen
open Config_cfg
open PostStat

open Bits



(** / Sun: **)

open GenFact
open Gen_edge

(** :Sun / **)



(** The main function *)
let theMain () = 
  let usagemsg = 
    "main.native [options] target\nrun with -h or --help to see the list of options"
  in
  let optpsr = OptParser.make ?usage:(Some usagemsg) () in
  let ls_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "Use linear-sweep method") ?long_name:(Some "ls") ls_opt;
  
  let aibt_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "Report AIBT") ?long_name:(Some "aibt") aibt_opt;

  let time_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "Report timing") ?long_name:(Some "time") time_opt;

  let baseCFG_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "Generate base-CFG only") ?long_name:(Some "base") baseCFG_opt;

  let dcfg_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "Generate D-CFG only") ?long_name:(Some "dcfg") dcfg_opt;
  
  let staticlib_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "Deal with statically-linked binary") ?long_name:(Some "static") staticlib_opt;

  let dfam_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "Marshal coq_DFA") ?long_name:(Some "dfam") dfam_opt;

  let cfgm_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "Marshal generated CFG") ?long_name:(Some "cfgm") cfgm_opt;

  let rtl_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "Output RTL instructions") ?long_name:(Some "rtl") rtl_opt;

  let swarn_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "Warn possible Soundness violations") ?long_name:(Some "swarn") swarn_opt;

  let edge_opt = StdOpt.store_true () in  (** Sun **)
  OptParser.add optpsr ?help:(Some "report Datalog edge facts") ?long_name:(Some "edge") edge_opt;  

  let fact_opt = StdOpt.store_true () in  (** Sun **)
  OptParser.add optpsr ?help:(Some "report Datalog facts") ?long_name:(Some "fact") fact_opt;  

  let meta_opt = StdOpt.store_true () in  (** Sun **)
  OptParser.add optpsr ?help:(Some "report Datalog facts") ?long_name:(Some "meta") meta_opt;  

  let bb_opt = StdOpt.store_true () in  (** Sun **)
  OptParser.add optpsr ?help:(Some "report basic blocks") ?long_name:(Some "bb") bb_opt;  

  

  let argvs = OptParser.parse_argv optpsr in


  let target_path = 
    if argvs = [] then 
      raise (Failure "No target file specified!")
    else
      List.hd argvs
  in
  let ls_on = Opt.get ls_opt in
  cfg_m_on := Opt.get cfgm_opt;
  Interface.m_on := Opt.get dfam_opt;
  let aibt_on = Opt.get aibt_opt in
  let time_on = Opt.get time_opt in
  let baseCFG_on = Opt.get baseCFG_opt in
  let dcfg_on = Opt.get dcfg_opt in
  Interface.static_lib := Opt.get staticlib_opt;
  Interface.no_optimization := false;
  let output_rtl = Opt.get rtl_opt in
  Interface.soundness_warn := Opt.get swarn_opt;
  

  let edge_on = Opt.get edge_opt in (** Sun **)
  let fact_on = Opt.get fact_opt in (** Sun **)
  let meta_on = Opt.get meta_opt in (** Sun **)
  let bb_on = Opt.get bb_opt in (** Sun **) 

  let elf = read_elf_file target_path in

  (*F.printf "%a" pp_elf_file elf;*)
  let elf_crsfile =
    elf.fname ^ ".crs"
  in
  let elf_dcfgfile =
    elf.fname ^ ".dcfg"
  in
  let crs = 
    if Sys.file_exists elf_crsfile then
      let mic = open_in_bin elf_crsfile in
      let tmp_crs = 
	(Marshal.input mic : code_region list) 
      in
      close_in mic;
      tmp_crs
    else (
      let tmp_crs =
        Stats.time ("Recursive disassembly") 
	  Dis.rt_dis_cfg elf
      in
      marshal_content tmp_crs elf_crsfile;
      tmp_crs
    )
  in
  (*
  let dcrs =
    Disassembler.Dis.ls_dis_file elf
  in
  List.iter (F.printf "%a" Disassembler.pp_code_region) dcrs;
   *)

(*************
  let dcfg =
    if not dcfg_on then H.create 32
    else if false (*Sys.file_exists elf_dcfgfile*) then
      let mic = open_in_bin elf_dcfgfile in
      let tmp_dcfg = 
	(Marshal.input mic : (string, basicblock) H.t) 
      in
      close_in mic;
      tmp_dcfg
    else if ls_on then (
      let tmp_dcfg =
        Stats.time ("LS-DCFG generation") 
	  bbset_to_bbtbl (Dis.ls_dis_cfg elf)
      in
      marshal_content tmp_dcfg elf_dcfgfile;
      tmp_dcfg
    ) else (
      let tmp_dcfg =
        Stats.time ("RT-DCFG generation") 
	  bbset_to_bbtbl (Dis.rt_dis_dcfg elf)
      in
      marshal_content tmp_dcfg elf_dcfgfile;
      tmp_dcfg
    )
  in
  H.iter (fun lbl bb ->
    P.printf "{\n";
    P.printf "bb_label = %s\n" bb.bb_label;
    P.printf "bb_symbol_label = %s\n" bb.bb_symbol_label;
    P.printf "bb_instrs = \n";
  (*List.iter print_rtl_instr rb.rb_instrs;*)
    List.iter 
      (fun (loc, pre, ins, len) -> 
	let rtl_instrs = X86_Compile.instr_to_rtl pre ins in
	P.printf " %s %d\t%s\t%s\n" (str_of_mword_flex (MWord.of_int loc)) 
	  (List.length rtl_instrs)
	  (Instr.str_of_prefix (pre,ins)) (Instr.str_of_instr (pre,ins));
      )  (instrs_with_starts (bb.bb_relAddr) bb.bb_instrs);	
    P.printf "bb_relAddr = %s\n" (str_of_mword_flex (MWord.of_int (bb.bb_relAddr)));
    P.printf "bb_succs = \n";
    List.iter (fun id -> P.printf "%s " id) bb.bb_succs;
    P.printf "\n";
    P.printf "bb_inter_succs = \n";
    List.iter (fun id -> P.printf "%s " id) bb.bb_inter_succs;
    P.printf "\n";
    P.printf "bb_preds = \n";
    List.iter (fun id -> P.printf "%s " id) bb.bb_preds;
    P.printf "\n";
    P.printf "bb_inter_preds = \n";
    List.iter (fun id -> P.printf "%s " id) bb.bb_inter_preds;
    P.printf "\n";
    P.printf "}\n\n";
    (*
    List.iter (fun (pre,ins,len) ->
      P.printf "RTL{\n";
      let rtl_instrs = X86_Compile.instr_to_rtl pre ins in
      List.iteri (fun ndx ri ->
	P.printf "%d:" (ndx+1);
	Printer.print_rtl_instr ri
      ) rtl_instrs;
      P.printf "}\n"
    ) bb.bb_instrs
    *)
  ) dcfg;
**************)



(*
  let dcfg =
    let tmp_dcfg =
      Stats.time ("RT-DCFG generation") 
	bbset_to_bbtbl (Dis.rt_dis_dcfg elf)
    in
    (*marshal_content tmp_dcfg elf_dcfgfile;*)
    tmp_dcfg
  in

  let (_, bbtbl) = Stats.time ("CFG generation")
	CFGGen.enhanced_cfggen elf    
  in 
 *)



  let target_cfg =
    if meta_on then
      let (_,tmpcfg) =
        Stats.time ("Meta-CFG generation")
          CFGGen.enhanced_cfggen elf
      in
      tmpcfg
    else
      let tmp_dcfg = 
        Stats.time ("RT-DCFG generation")
        bbset_to_bbtbl (Dis.rt_dis_dcfg elf) 
        (*  bbset_to_bbtbl (Dis.rt_dis_cfg elf)  *)
      in 
      if !cfg_m_on then marshal_content tmp_dcfg elf_dcfgfile;
      tmp_dcfg
  
  in

  if fact_on then
    GenFact.gen_datalog_fact target_cfg;

  if edge_on then
    Gen_edge.print_edge_main target_cfg;

  if bb_on then
   H.iter (fun lbl bb ->
     BB.print_bb stdout bb;
   ) target_cfg;


  (*
    if meta_on then (
      Gen_edge.print_edge_main bbtbl )
	else (
	  Gen_edge.print_edge_main dcfg)
   *)

(*  if edge_on then BB.print_edge_main dcfg else ()
*)


  flush stdout;

  if time_on then
    Stats.print Pervasives.stdout ("Timings:\n")
 
let _ = 
  Stats.reset Stats.SoftwareTimer;
  theMain ()
