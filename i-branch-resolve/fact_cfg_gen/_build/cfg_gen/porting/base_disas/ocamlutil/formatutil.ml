(** Pretty printing utilitity functions *)
(** Gang Tan *)

open Format

let pp_tab fmt = pp_print_tab fmt ()

(** Pretty print a list *)
let rec pp_list ?(sep="") pp_element fmt = function
  | [h] -> Format.fprintf fmt "%a" pp_element h
  | h::t ->
      Format.fprintf fmt "%a%s@,%a"
      pp_element h sep (pp_list ~sep pp_element) t
  | [] -> ()

(* Pretty print a table header; 
   it takes a list of pairs of header widths and texts *)
let pp_tab_header fmt (widthAndHeaderLst:(int*string) list) =
  List.iter 
    (fun (wd,s) -> 
      let len = String.length s in
      let sp = if (len < wd) then String.make (wd - len) ' ' else "" in
        pp_set_tab fmt ();
        fprintf fmt "%s%s" s sp)
    widthAndHeaderLst;
  fprintf fmt "@,"

