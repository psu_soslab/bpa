(** Constants for configuration of the system *)
open Batteries
open Bigarray

(** The elf state; need to set this right for the framework to work.
    If elf32 is true, then mword should be interpreted as a 32-bit word,
    even though its representation is int64 *)
type elf_state = 
  {mutable elf32:bool; mutable small_endian:bool}

(* The default elf state *)
let es = {elf32=true; small_endian=true}

let set_es_elf32 b = es.elf32 <- b
let set_es_endian b = es.small_endian <- b

(** a machine word, representing an address or values in a register.  We
    use int64 for both 32-bit and 64-bit machines. In the case of 32 bits,
    only the lower 32 bits should be used. It wastes some memory, but
    enables us to write a single function for both 32-bits and 64 bits. *)
module MWord = Util.Int64Op
type mword = int64

let str_of_mword mw = 
  if es.elf32 then Printf.sprintf "%08lx" (Util.int32_of_int64 mw)
  else Printf.sprintf "%16Lx" mw

let str_of_mword_one_byte mw = 
  if es.elf32 then Printf.sprintf "%02lx" (Util.int32_of_int64 mw)
  else Printf.sprintf "%2Lx" mw

(* in flexible width *)
let str_of_mword_flex mw = 
  if es.elf32 then Printf.sprintf "%lx" (Util.int32_of_int64 mw)
  else Printf.sprintf "%Lx" mw
  
(* in decimal *)
let str_of_mword_dec mw = 
  if es.elf32 then Printf.sprintf "%lu" (Util.int32_of_int64 mw)
  else Printf.sprintf "%Lu" mw

let pp_mword fmt mw = Format.fprintf fmt "%s" (str_of_mword mw)
let pp_mword_flex fmt mw = Format.fprintf fmt "%s" (str_of_mword_flex mw)
let pp_mword_dec fmt mw = Format.fprintf fmt "%s" (str_of_mword_dec mw)

(** The type used to store bytes in a code section *)
type codeBytes = (int, int8_unsigned_elt, c_layout) Array1.t



