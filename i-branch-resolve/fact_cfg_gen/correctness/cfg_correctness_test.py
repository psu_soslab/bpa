#!/usr/bin/python

import sys

LegalBranch=dict()

def main(Prog):
    violation = 0
    with open(Prog+'.cseg', 'r') as f:
        for line in f:
            if 'R E' in line:
                code_start = int(line.strip().split()[3], 16)
                code_size = int(line.strip().split()[5], 16)
                code_end = code_start + code_size
    with open(Prog+'.icfg', 'r') as f:
        for line in f:
            legal_branch_site = int(line.strip().split(':')[0], 16)
            legal_target_string = line.strip().split(':')[1]
            legal_target_list = [int(t,16) for t in legal_target_string.strip().split()]
            LegalBranch[legal_branch_site] = legal_target_list
    print "Start Checking trace..."
    with open(Prog+'.trace', 'r') as f:
        for line in f:
            if not 'eof' in line:
                branch_site = int(line.strip().split(':')[0], 16)# + code_start
                if len(line.strip().split(':')) == 1:
                    print "strange trace point " + line
                    continue
                elif line.strip().split(':') == '':
                    print "strange trace point " + line
                    continue
                else:
                    target = int(line.strip().split(':')[1], 16)# + code_start
                    if not branch_site in LegalBranch:
                        print '?' + hex(branch_site) + "-->" + hex(target)
                        violation = 1
                    elif target > code_size or target < 0:
                        target_list = LegalBranch[branch_site]
                        if -1 in target_list or len(target_list) == 0:
                            continue
                        else:
                            print hex(branch_site) + "~/>" + hex(target)
                            violation = 1
                    else:
                        if target in LegalBranch[branch_site]:
                            continue
                        else:
                            print hex(branch_site) + "-/>" + hex(target)
                            violation = 1
    if violation == 0:
        print Prog + " successfully passed the correctness test!"

if __name__=="__main__":
    main(sys.argv[1])
