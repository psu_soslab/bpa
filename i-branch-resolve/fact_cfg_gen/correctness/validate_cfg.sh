
if [ $# != 1 ]; then
    exit 1
fi

./main.native --icfg $1

readelf -l $1 | grep LOAD > $1.cseg

#run benchmark with pin
mv ibtrace32.out $1.trace

./cfg_correctness_test.py $1

rm $1.cseg
rm $1.icfg
rm $1.trace
