

if [ $# != 1 ]; then
    echo "ITRetrieve.sh [target]"
    exit 1
fi
    
readelf -s $1.o > $1.o.symtbl
readelf -S $1.o > $1.o.section
readelf -r $1.o > $1.o.reloca

./reloc_based_targets.py $1.o > $1.its

rm -rf $1.o.symtbl $1.o.section $1.o.reloca
