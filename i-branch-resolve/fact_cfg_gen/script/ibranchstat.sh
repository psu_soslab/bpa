BENCHNAME="tyinfer lbm libquantum mcf milc bzip2 hmmer sjeng h264ref perlbench gobmk gcc"

for FILE in lbm libquantum mcf ; do
    echo "working on "$FILE
    ./ibranchstat --rt --all ./benchmark/$FILE/$FILE | tee ./ibranchstatout/$FILE.check
done

for FILE in milc bzip2 hmmer sjeng sphinx3 h264ref gobmk perlbench gcc ; do
    echo "working on "$FILE
    ./ibranchstat --rt --unsup ./benchmark/$FILE/$FILE | tee ./ibranchstatout/$FILE.check
done


#gcc gobmk perlbench
