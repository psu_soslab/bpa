# read command line for target name
# do "objdump -d", "objdump -D", "readelf -w", "main.native" on the target

BENCHDIR=decfi_benchmarks

if [ $# = 0 ]; then
    echo "build.sh [option] [TARGET_PATH]: Please specify the option and the path to the target.
option:
  rcfg <path>
  acfg <path>
  cr <path>
  stat
  clean"
    exit 1
fi

if [ $1 = 'clean' ]; then
    rm *.asm *.txt *.debug *.stat *.scfg *.fnra vfp* *.branch *.bbs
    exit 0
fi

if [ $1 = 'stat' ]; then
    ./main.native stat ./benchmarks/dart_example > dart_example.stat
    echo "Source Line number:" >> dart_example.stat
    wc -l ./benchmarks/src/dart_example/*.c >> dart_example.stat
    ./main.native stat ./benchmarks/sort > sort.stat
    echo "Source Line number:" >> sort.stat
    wc -l ./benchmarks/src/sort/*.c >> sort.stat
    ./main.native stat ./benchmarks/lbm > lbm.stat
    echo "Source Line number:" >> lbm.stat
    wc -l ./benchmarks/src/lbm/*.c ./benchmarks/src/lbm/*.h >> lbm.stat
    ./main.native stat ./benchmarks/libquantum > libquantum.stat
    echo "Source Line number:" >> libquantum.stat
    wc -l ./benchmarks/src/libquantum/*.c ./benchmarks/src/libquantum/*.h >> libquantum.stat
    exit 0
fi

if [ $1 = 'rcfg' ]; then
    objdump -d $2 > $2.asm
    objdump -D $2 > $2.txt
    readelf -w $2 > $2.debug
    ./main.native rcfg $2 | tee $2.rcfg
    exit 0
fi

if [ $1 = 'acfg' ]; then
    objdump -d $2 > $2.asm
    objdump -D $2 > $2.txt
    readelf -w $2 > $2.debug
    ./main.native acfg $2 | tee $2.acfg
    exit 0
fi

if [ $1 = 'cr' ]; then
    objdump -d $2 > $2.asm
    objdump -D $2 > $2.txt
    readelf -w $2 > $2.debug
    ./main.native cr $2 | tee $2.cr
    exit 0
fi

if [ $1 = 'all' ]; then
    for FILE in dart_example sort goto bzip2 mcf gobmk hmmer sjeng libquantum h264ref specrand perlbench gcc; do
	echo Dealing with $FILE...
	mkdir ./bench_result/$FILE
	objdump -d ./C_int_benchmarks/$FILE > ./bench_result/$FILE/$FILE.asm
	readelf -w ./C_int_benchmarks/$FILE > ./bench_result/$FILE/$FILE.debug
	{ time ./main.native bbs ./C_int_benchmarks/$FILE ./bench_result | tee ./bench_result/$FILE/$FILE.log 1 ; } 2> ./bench_result/$FILE/$FILE.time
    done
    exit 0
fi

if [ $1 = 'decfi' ]; then
	rm -rf decfi_bench_result
	mkdir decfi_bench_result
    for FILE in bzip2 lbm mcf gobmk hmmer sjeng libquantum h264ref sphinx milc gcc; do
	echo Dealing with $FILE...
	mkdir ./decfi_bench_result/$FILE
	objdump -d ./decfi_benchmarks/$FILE > ./decfi_bench_result/$FILE/$FILE.asm
	readelf -w ./decfi_benchmarks/$FILE > ./decfi_bench_result/$FILE/$FILE.debug
	{ time ./main.native bbs ./decfi_benchmarks/$FILE ./decfi_bench_result | tee ./decfi_bench_result/$FILE/$FILE.log 1 ; } 2> ./decfi_bench_result/$FILE/$FILE.time
    done
    exit 0
fi
