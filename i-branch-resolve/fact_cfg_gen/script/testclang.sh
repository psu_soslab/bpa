EXPDIR=~/work/experiment_all_c_no
GCCBENCHDIR=benchmarks/clang

cd $EXPDIR

# sort bzip2 mcf milc gobmk hmmer sjeng lbm libquantum h264ref perlbench sphinx3 specrandfp specrandint gcc

# lbm mcf libquantum bzip2 sjeng milc sphinx3 hmmer h264ref perlbench gobmk gcc

for FILE in lbm mcf libquantum bzip2 sjeng milc sphinx3 hmmer h264ref perlbench gobmk gcc ; do
    echo Dealing with $FILE...
    rm -rf ./output/clang/$FILE
    mkdir ./output/clang/$FILE
    cp ./$GCCBENCHDIR/$FILE/* .

    ./CFGctor $FILE > ${FILE}_best.out
    mv ${FILE}_best.out ./output/clang/$FILE/${FILE}_best.out
    mv $FILE.bbs ./output/clang/$FILE/${FILE}_best.bbs
    mv $FILE.branch ./output/clang/$FILE/${FILE}_best.branch

    ./CFGctor --noRel --noType $FILE > ${FILE}_worst.out
    mv ${FILE}_worst.out ./output/clang/$FILE/${FILE}_worst.out
    mv $FILE.bbs ./output/clang/$FILE/${FILE}_worst.bbs
    mv $FILE.branch ./output/clang/$FILE/${FILE}_worst.branch

    rm -rf $FILE* vfp*
done
