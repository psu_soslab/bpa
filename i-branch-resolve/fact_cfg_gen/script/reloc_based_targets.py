#!/usr/bin/python

import sys
import struct

SymTbl = dict()
UserFuncs = dict()

Sections = dict()
SectionOffs = dict()
ExeSections = set()

RelocaInfo = dict()

FuncRelaTargets = set()

''' .section, .symtbl, and .reloca must be ready''' 

def process_symtbl_and_sections(ObjFile):
    with open(ObjFile+'.section', 'r') as f:
        for line in f:
            if '[' in line and not 'Nr' in line:
                SecNdx = int(line.strip().strip('[ ').split(']')[0], 10)
                SecName = line.strip().strip('[ ').split(']')[1].split()[0]
                SecOff = int(line.strip().strip('[ ').split(']')[1].split()[3],16)
                SecFlag = line.strip().strip('[ ').split(']')[1].split()[6]
                if SecNdx <> 0:
                    Sections[SecNdx] = SecName
                    SectionOffs[SecName] = SecOff
                if 'X' in SecFlag:
                    ExeSections.add(SecName)
    with open(ObjFile+'.symtbl', 'r') as f:
        for line in f:
            #if not '.symtab' in line and not 'Num' in line:
            #    SymNum = int(line.strip().split()[0].strip(':'),10)
            #    SymNdx = line.strip().split()[6]
            #    SymName = line.strip().split()[7]
            if 'FUNC' in line:
                FuncName = line.strip().split()[7]
                FuncSecNdx = int(line.strip().split()[6],10)
                FuncSec = Sections[FuncSecNdx]
                FuncLopc = int(line.strip().split()[1], 16)
                FuncSize = int(line.strip().split()[2], 10)
                UserFuncs[FuncLopc,FuncSec] = (FuncName, FuncLopc+FuncSize)

def process_reloca(ObjFile):
    RelocaSection = ''
    recording_on = 0
    with open(ObjFile+'.reloca', 'r') as f:
        for line in f:
            if '.rel' in line:
                RelocaSection = line.strip().split()[2].strip('\'')[4:]
                recording_on = 1
                continue
            if 'Offset' in line:
                continue
            if line[0] == '\n':
                recording_on = 0
                continue
            if recording_on == 1:
                Offset = int(line.strip().split()[0],16)
                Info = line.strip().split()[1]
                SymValue = int(line.strip().split()[3],16)
                SymName = line.strip().split()[4]
                RelocaInfo[RelocaSection,Offset] = (Info, SymValue, SymName)

def readWord(ObjFile, RelSecOff, Offset):
    start = RelSecOff + Offset
    with open(ObjFile, 'rb') as f:
        f.seek(start)
        byteSeq = f.read(4)
        off = struct.unpack('i', byteSeq)[0]
        #print hex(off)
        return off
        
def findRelFunc(off, SymName):
    DesiredKey = (0,0)
    for (FuncLopc, FuncSec), (FuncName, FuncHipc) in UserFuncs.iteritems():
        if off >= FuncLopc and FuncLopc >= DesiredKey[0] and FuncSec == SymName:
            DesiredKey = FuncLopc, FuncSec
    #print UserFuncs[DesiredKey], (off-DesiredKey)
    return UserFuncs[DesiredKey][0], (off - DesiredKey[0])

if __name__=="__main__":
    process_symtbl_and_sections(sys.argv[1])
    process_reloca(sys.argv[1])
    #print Sections
    #print ExeSections
    #print UserFuncs
    #print RelocaInfo

    FuncNames = [FuncName for (FuncLopc,FuncSec),(FuncName,FuncHipc) in UserFuncs.iteritems()]

    #Deal with relocation in executable sections
    for (FuncLopc,FuncSec), (FuncName, FuncHipc) in UserFuncs.iteritems():
        #print Property[1] + ':'
        for (RelSec, Offset), (Info, SymValue, SymName) in RelocaInfo.iteritems():
            if RelSec == FuncSec and FuncLopc <= Offset and FuncHipc >= Offset:
                if Info[len(Info)-1] == '1':                    
                    if SymName in ExeSections:
                        #offsets into executable sections
                        off = readWord(sys.argv[1], SectionOffs[RelSec], Offset) + SymValue
                        (relFunc, relFuncOff) = findRelFunc(off, SymName)
                        FuncRelaTargets.add((relFunc, relFuncOff))
                    elif SymName[0] <> '.':
                        #functions
                        #off = readWord(sys.argv[1], SectionOffs[RelSec], Offset) + SymValue
                        #relFuncOff = 0
                        FuncRelaTargets.add((SymName, 0))

    #Deal with relocation in .rodata and .data
    for (RelSec, Offset), (Info, SymValue, SymName) in RelocaInfo.iteritems():
        if RelSec == '.rodata' or RelSec == '.data':
            if Info[len(Info)-1] == '1':
                if SymName in ExeSections:
                    #offsets into executable sections
                    off = readWord(sys.argv[1], SectionOffs[RelSec], Offset) + SymValue
                    (relFunc, relFuncOff) = findRelFunc(off, SymName)
                    FuncRelaTargets.add((relFunc, relFuncOff))
                elif SymName[0] <> '.':
                    #functions
                    #off = readWord(sys.argv[1], SectionOffs[RelSec], Offset) + SymValue
                    #relFunc = UserFuncs[off][1]
                    ## ??? Modification here ???
                    FuncRelaTargets.add((SymName, 0))
    for item in FuncRelaTargets:
        print item[0] + " " + hex(item[1])
    #print FuncRelaTargets
