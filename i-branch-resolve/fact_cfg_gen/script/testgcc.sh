EXPDIR=~/work/experiment_all_c_no
GCCBENCHDIR=benchmarks/gcc

cd $EXPDIR

# sort bzip2 mcf milc gobmk hmmer sjeng lbm libquantum h264ref perlbench sphinx3 specrandfp specrandint gcc

# lbm mcf libquantum bzip2 sjeng milc sphinx3 hmmer h264ref perlbench gobmk gcc

for FILE in lbm mcf libquantum bzip2 sjeng milc sphinx3 hmmer h264ref perlbench gobmk gcc ; do
    echo Dealing with $FILE...
    rm -rf ./output/gcc/$FILE
    mkdir ./output/gcc/$FILE
    cp ./$GCCBENCHDIR/$FILE/* .

    ./CFGctor $FILE > ${FILE}_best.out
    mv ${FILE}_best.out ./output/gcc/$FILE/${FILE}_best.out
    mv $FILE.bbs ./output/gcc/$FILE/${FILE}_best.bbs
    mv $FILE.branch ./output/gcc/$FILE/${FILE}_best.branch

    ./CFGctor --noRel --noType $FILE > ${FILE}_worst.out
    mv ${FILE}_worst.out ./output/gcc/$FILE/${FILE}_worst.out
    mv $FILE.bbs ./output/gcc/$FILE/${FILE}_worst.bbs
    mv $FILE.branch ./output/gcc/$FILE/${FILE}_worst.branch

    rm -rf $FILE* vfp*
done

