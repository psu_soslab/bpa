TARGETS=$1

for FILE in $TARGETS 
do
    echo Now patching $FILE...
    readelf -S $FILE | grep ".text" > $FILE.tsec
    ./nop_patcher.py $FILE
    mv $FILE $FILE.old
    mv $FILE.new $FILE
    chmod +x $FILE
    rm $FILE.tsec 
done
