for FILE in bzip2 gcc gobmk h264ref hmmer lbm libquantum mcf milc perlbench sjeng ; do
    mkdir ./benchmark/$FILE
    mv ${FILE}_base* ./benchmark/$FILE/$FILE
    mv ${FILE}*.its ./benchmark/$FILE/$FILE.its
done

mkdir ./benchmark/sphinx3
mv sphinx_livepretend_base* ./benchmark/sphinx3/sphinx3
mv sphinx_livepretend*.its ./benchmark/sphinx3/sphinx3.its
