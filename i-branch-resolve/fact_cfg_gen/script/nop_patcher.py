#!/usr/bin/python

import sys


patch_patterns = [(bytearray([0xf3, 0x90]), bytearray([0x90, 0x90]))] 


def main(Prog):
    with open(Prog+'.tsec', 'r') as f:
        line = f.readline()
        tsec_start = int(line.strip().split()[5], 16)
        tsec_size = int(line.strip().split()[6], 16)
        #print "%x" % tsec_start
        #print "%x" % tsec_size
    with open(Prog, 'r+b') as f:
        data1 = f.read(tsec_start)
        data2 = f.read(tsec_size)
        data3 = f.read()
        #print data2
        for pattern in patch_patterns:
            data2 = data2.replace(pattern[0], pattern[1])
    with open(Prog+'.new', 'w+b') as f:
        f.write(data1+data2+data3)

if __name__=="__main__":
    main(sys.argv[1])
