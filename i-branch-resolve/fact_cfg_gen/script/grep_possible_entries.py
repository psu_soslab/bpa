#!/usr/bin/python

import sys

posent_set = set()

def main(Prog):
    recording_on = 0
    with open(Prog+'.rel', 'r') as f:
        for line in f:
            if '.rel.text' in line or '.rel.data' in line or '.rel.rodata' in line:
                recording_on = 1
                continue
            if 'Offset' in line:
                continue
            if line[0] == '\n':
                recording_on = 0
                continue
            if recording_on == 1:
                posent = line.strip().split()[4]
                info = line.strip().split()[1]
                if posent[0] != '.' and info[len(info)-1] == '1':
                    posent_set.add(posent)
    with open(Prog+'_posent.txt', 'w') as f:
        for posent in posent_set:
            f.write(posent+'\n')
                

if __name__=="__main__":
    main(sys.argv[1])
