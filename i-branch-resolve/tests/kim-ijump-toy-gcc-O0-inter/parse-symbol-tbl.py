
import os
import sys

def process_txt(txt_lst):
	#order: glb_address, section, size
	processed_lst = []
	for txt in txt_lst:
		type_size = txt[3].split("\t")
		processed_fact = str(int(txt[0], 16) ) + "\t" + type_size[0] + "\t" + str(int(type_size[1], 16)) + "\n"
		processed_lst.append(processed_fact)
	return processed_lst

def main(dir):
	command_line = "objdump -t " + str(dir) + "/src/" + str(dir) + "_gcc"

	txt_lst  = os.popen(command_line).readlines()
	word_lst = []

	bss_lst = []
	data_lst = []
	rodata_lst = []
	for txt in txt_lst:
		txt_splitted = txt.split(" ")
		if len(txt_splitted) > 5:
			#txt_splitted.remove('')
			txt_splitted = filter(lambda a: a != '', txt_splitted)

			txt_sec = txt_splitted[3]
			if (".bss" in txt_sec):
				bss_lst.append( txt_splitted)
			elif (".data" in txt_sec):
				data_lst.append( txt_splitted)
			elif (".rodata" in txt_sec):
				rodata_lst.append( txt_splitted)

	bss_lst = process_txt(bss_lst)
	data_lst = process_txt(data_lst)
	rodata_lst = process_txt(rodata_lst)
	lst_all = []
	lst_all.extend(bss_lst)
	lst_all.extend(data_lst)
	lst_all.extend(rodata_lst)

	file_write = str(dir) + "/facts/global_section_symbol.facts"
	file1 = open( file_write ,"w")
	for line in lst_all:
		file1.write(line)
	file1.close()



if __name__ == "__main__":
    dir = sys.argv[1] #should be a dir name
    #option = sys.argv[2]
    #init_edge_f = dir + "/facts/edge.facts"
    main(dir)


#
#  [16] .rodata           PROGBITS        08052b60 00ab60 001214 00   A  0   0 32

