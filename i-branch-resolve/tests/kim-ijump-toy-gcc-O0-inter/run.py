import os
import time
import argparse

def main(args):
    dir_var = args.file_dir
    if dir_var[-1] == "/":
        dir_var = dir_var[:-1]
    thread_num = str(args.thread)

    os.system("./preprocess-gcc-facts.sh -b 1 -s " + dir_var)

    os.system("cp -f " + dir_var + "/facts/inter_succ.facts " + dir_var + "/facts/inter_succ_save.facts")
    os.system("./preprocess-gcc-facts-temp.sh -b 1 -s " + dir_var + " -j " + thread_num)

    os.system("./preprocess-gcc.sh -b 1 -s " + dir_var)

    # in case you need it 
    os.system("python parse-symbol-tbl.py " + dir_var)

    #run BPA
    os.system("python run-BPA.py -d " + dir_var + " -t " + thread_num)



def arg_parsing():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t','--thread', help='# of threads (cores)', default = 4)
    parser.add_argument('-d','--file_dir', help='input directory file', default = "malloc_free")
    return parser.parse_args()

if __name__ == "__main__":
    main(arg_parsing())


