#! /bin/sh

dirs=$(ls -l ${testcase_root} |awk '/^d/ {print $NF}')

# default bound value is 64
bound=64
syntex_check=0
entry_name="main"

while [ -n "$1" ]
do
  case "$1" in
  -b) 
     bound=$2
     shift
     ;;
  -s) 
     dirs=$(ls -d $2)
     shift
     ;;
  --print)
     syntex_check=1
     ;;
  -e)
     entry_name=$2
     shift
     ;;
  esac
  shift
done

#obtain the absolute path of root directory
work_path=$(dirname $0)
cd ./${work_path}
cd ../../
root_dir=$(pwd)

testcase_root=${root_dir}"/tests/kim-ijump-toy-gcc-O0-inter/"

cfg_gen_dir=${root_dir}"/fact_cfg_gen/"

checker_dir=${root_dir}"/checker/"

logic_pre_process_dir=${root_dir}"/logic/souffle/pre-process/"

for dir in $dirs
do
    echo "\n\nCompiling ..."
    if [ ! -f ${testcase_root}${dir}"/src/Makefile" ]
    then
        cd ${testcase_root}
        #make clean T=${dir}"/src/"${dir} 
        #make T=${dir}"/src/"${dir}
    else
        cd ${testcase_root}${dir}"/src/"
        #make clean
        #make
    fi

    echo "\n\nRTL instruction facts generation ..."
    #cd ${cfg_gen_dir}
    #./main.native --fact --edge ${testcase_root}${dir}"/src/"${dir}"_gcc"
    #mv *.facts ${testcase_root}${dir}"/facts/"

    cd ${root_dir}

    python abstract_offset.py --b ${bound}
    mv *.facts ${testcase_root}${dir}"/facts/"

    echo "\n\nPreprocessing values in global sections ..."
    #java -jar ${checker_dir}"AlocGenHelper.jar" ${testcase_root}${dir}"/src/"${dir}"_gcc" ${testcase_root}${dir}"/facts/"

    echo ${entry_name} > ${testcase_root}${dir}"/facts/entry_input.facts"

    if [ $syntex_check -eq 1 ]
    then
        echo "\n\nRTL syntax checking and plain-text RTL instruction generation ..."
        #java -jar ${checker_dir}"SyntaxChecker.jar" ${testcase_root}${dir}"/facts/" > ${testcase_root}${dir}"/"${dir}".txt"
        sleep 5 #wait 5 seconds to avoid failure of the following procedures
    fi

start_tm=`date +%s%N`; 

    cd ${logic_pre_process_dir}

    #echo "\n\nDeriving aloc boundary ..."
    #souffle -F ${testcase_root}${dir}"/facts" -D ${testcase_root}${dir}"/facts" boundary_gen.dl

    echo "\n\nSearching for indirect jump/call addresses and functions ..."
    souffle -F ${testcase_root}${dir}"/facts" -D ${testcase_root}${dir}"/facts" find_indirect_jmp_funcs.dl

    #####echo "\n\nGenerating Sub-section header for functions ..."
    #####souffle -F ${testcase_root}${dir}"/facts" -D ${testcase_root}${dir}"/facts" rgn_sub_section_header.dl

    #echo "\n\nGenerating global alocs ..."
    #souffle -F ${testcase_root}${dir}"/facts" -D ${testcase_root}${dir}"/facts" glb_mem_rgn.dl






    #echo "\n\nGenerating local alocs ..."
    #souffle -F ${testcase_root}${dir}"/facts" -D ${testcase_root}${dir}"/facts" aloc-gen-local-static.dl

    #echo "\n\nFiltering registers used ..."
    #souffle -F ${testcase_root}${dir}"/facts" -D ${testcase_root}${dir}"/facts" loc_filter.dl

end_tm=`date +%s%N`; 
use_tm=`echo $end_tm $start_tm | awk '{ print ($1 - $2) / 1000000000}'` 
echo "Preprocessing time used: $use_tm (s)"

done

