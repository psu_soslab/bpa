import os
import sys

def main(dir):
    command_line = "readelf -s " + str(dir) + "/src/" + str(dir) + "_gcc"
    txt_lst  = os.popen(command_line).readlines()
    #txt_lst = open("test21.symtbl").readlines()
    line_lst = []
    line_lst_temp = []
    for txt in txt_lst:
        if (("OBJECT" in txt) and ("DEFAULT" in txt)) :
            if (("LOCAL" in txt) or ("GLOBAL" in txt)):
                for each_word in txt.split(" "):
                    if each_word != '':
                        line_lst_temp.append(each_word)
                line_lst.append(line_lst_temp)
                line_lst_temp = []

    if len(line_lst) == 0:
        print("error!!!!!!!!!!!!!!")

    global_lst = []
    for line in line_lst:
        if line[2] != 0:
            hex_to_int = int(line[1], 16)
            global_lst.append(hex_to_int)

    file_write = str(dir) + "/facts/glb_symtbl_init.facts" #"temp.facts"
    file1 = open(file_write, "w")

    for glb in global_lst:
       file1.write(str(glb) + "\n")

    file1.close()
    os.system("./run-pts-analysis-gcc-increment.sh -b 1 -g -j 32 -s " + str(dir)) 


if __name__ == "__main__":
    dir = sys.argv[1] #should be a dir name
    main(dir)



#

