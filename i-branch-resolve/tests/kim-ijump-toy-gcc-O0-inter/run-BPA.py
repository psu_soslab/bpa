import os
import argparse
import time



def main(args):
    dir = args.file_dir
    thread_num = str(args.thread)

    start_time = time.time()

    os.system("python parse-symtbl.py " + dir)
    os.system("./run-pts-analysis-gcc-increment.sh -b 1 -j " + thread_num + " -ij -s " + dir) #ijump-resolve

    os.system("souffle -w -F " + dir + "/facts/" + " -D " + dir + "/facts/ i-jmp-merge-pts.dl")


    os.system("./run-pts-analysis-gcc-increment.sh -b 1 -j " + thread_num + " -ret_inter_succ -s " + dir) #remove inter_succ that calls functions without any return


    start_time2 = time.time()


    os.system("./run-pts-analysis-gcc-increment.sh -b 1 -j " + thread_num + " -sl -s " + dir) #stk layout analysis
    end_time2 = time.time()
    #quit()

    os.system("./run-pts-analysis-gcc-increment.sh -b 1 -j " + thread_num + " -c -s " + dir) #preprocessing CFG (dead-code filtering purpose)

    points_to_start_time = time.time()
    os.system("./run-pts-analysis-gcc-increment.sh -b 1 -j " + thread_num + " -s " + dir) #main points-to analysis
    
    # general version for BPA.
    #os.system("./run-pts-analysis-gcc-increment.sh -b 1 -gp -j " + thread_num + " -s " + dir) #main points-to analysis

    os.system("./run-pts-analysis-gcc-increment.sh -b 1 -aict -j " + thread_num + " -s " + dir)
    points_to_end_time = time.time()
    print("Points-to, it took %s seconds\n" % (points_to_end_time - points_to_start_time))


def arg_parsing():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t','--thread', help='# of threads (cores)', default = 4)
    parser.add_argument('-d','--file_dir', help='input directory file', default = "bzip2_gcc54-32bit-O0")
    return parser.parse_args()

if __name__ == "__main__":
    main(arg_parsing())

#e.g) python run-bpa.py -d testcase1/ -t 16




#

