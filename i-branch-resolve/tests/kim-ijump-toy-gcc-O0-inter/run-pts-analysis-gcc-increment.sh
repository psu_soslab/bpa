#! /bin/sh

dirs=$(ls -l ${testcase_root} |awk '/^d/ {print $NF}')

# default bound value is 64
bound=64

threads=1

entry_name="main"

# global_symbol_tbl
macro="global_original"

has_val_name="has_val.csv"
timestamp=$(date "+%Y%m%d-%H%M%S")
has_val_ordered_name="has_val_ordered"${timestamp}".csv"

algorithm="points-to-baseline.dl"
logic_dir_name="block-pointsto-interproc-toTrack-increment"

while [ -n "$1" ]
do
  case "$1" in
  -b) 
     bound=$2
     shift
     ;;
  -s) 
     dirs=$(ls -d $2)
     shift
     ;;
  -j) 
     threads=$2
     shift
     ;;
  -sl)
     algorithm="stack-layout-analysis-main.dl"
     ;;
  -c)
     algorithm="cfg-preprocess.dl"
     ;;
  -gp)
     algorithm="general-points-to-baseline.dl"
     ;;

  -gstk)
     algorithm="glb-mem-addr-preprocess-stk-arg.dl"
     ;;
  -aict)
     algorithm="comp-AICT.dl"
     ;;
  -g)
     algorithm="glb_mem_addr_preprocessing.dl"
     ;;

  -global_symbol_tbl)
    macro="global_symbol_tbl"
     ;;

  -ret_inter_succ)
     algorithm="cfg-ret-inter-succ-remove.dl"
     ;;

  -ij)
     algorithm="ijump-resolve.dl"
     ;;

  -e)
     entry_name=$2
     shift
     ;;
  esac
  shift
done

#obtain the absolute path of root directory
work_path=$(dirname $0)
cd ./${work_path}
cd ../../
root_dir=$(pwd)

testcase_root=${root_dir}"/tests/kim-ijump-toy-gcc-O0-inter/"

cfg_gen_dir=${root_dir}"/cfg_gen/"

checker_dir=${root_dir}"/checker/"

logic_dir=${root_dir}"/logic/souffle/"${logic_dir_name}
logic_pre_process_dir=${root_dir}"/logic/souffle/pre-process/"

for dir in $dirs
do

    cd ${root_dir}
    python abstract_offset.py --b ${bound}
    mv *.facts ${testcase_root}${dir}"/facts/"

    #java -jar ${checker_dir}"AlocGenHelper.jar" ${testcase_root}${dir}"/src/"${dir}"_gcc" ${testcase_root}${dir}"/facts/"

start_tm=`date +%s%N`; 
    cd ${logic_pre_process_dir}

    #souffle -F ${testcase_root}${dir}"/facts" -D ${testcase_root}${dir}"/facts" affine_indirect_branch.dl
end_tm=`date +%s%N`; 
use_tm=`echo $end_tm $start_tm | awk '{ print ($1 - $2) / 1000000000}'` 
echo "Affine build time used: $use_tm (s)"

echo ${entry_name} > ${testcase_root}${dir}"/facts/entry_input.facts"

start_tm=`date +%s%N`; 
    cd ${logic_dir}

     #souffle -F ${testcase_root}${dir}"/facts" -D ${testcase_root}${dir}"/facts" -j ${threads} ${algorithm}
	 
     #./souffle -F ${testcase_root}${dir}"/facts" -D ${testcase_root}${dir}"/facts" ${algorithm}
     souffle -w -F ${testcase_root}${dir}"/facts" -M ${macro} -D ${testcase_root}${dir}"/facts" -j ${threads} ${algorithm}


end_tm=`date +%s%N`; 
use_tm=`echo $end_tm $start_tm | awk '{ print ($1 - $2) / 1000000000}'` 
echo "Points-to analysis time used: $use_tm (s)"

#    java -jar ${checker_dir}"HasValSorting.jar" ${testcase_root}${dir}"/facts/"${has_val_name} ${testcase_root}${dir}"/facts/"${has_val_ordered_name}

done



