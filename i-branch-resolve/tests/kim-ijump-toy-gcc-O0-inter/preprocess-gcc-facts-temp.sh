#! /bin/sh

dirs=$(ls -l ${testcase_root} |awk '/^d/ {print $NF}')

# default bound value is 64
bound=64
threads=4


while [ -n "$1" ]
do
  case "$1" in
  -b) 
     bound=$2
     shift
     ;;
  -s) 
     dirs=$(ls -d $2)
     shift
     ;;
  -j)
     threads=$2
     shift
     ;;

  esac
  shift
done

#obtain the absolute path of root directory
work_path=$(dirname $0)
cd ./${work_path}
cd ../../
root_dir=$(pwd)

testcase_root=${root_dir}"/tests/kim-ijump-toy-gcc-O0-inter/"

cfg_gen_dir=${root_dir}"/fact_cfg_gen/"

checker_dir=${root_dir}"/checker/"

logic_pre_process_dir=${root_dir}"/logic/souffle/pre-process/"

for dir in $dirs
do
    if [ ! -f ${testcase_root}${dir}"/src/Makefile" ]
    then
        cd ${testcase_root}
        #make clean T=${dir}"/src/"${dir} 
        #make T=${dir}"/src/"${dir}
    else
        cd ${testcase_root}${dir}"/src/"
        #make clean
        #make
    fi

     #cd ${cfg_gen_dir}
     #./main.native --fact --edge ${testcase_root}${dir}"/src/"${dir}"_gcc"
     #mv *.facts ${testcase_root}${dir}"/facts/"


    cd ${testcase_root}
    #python readelf-txt-section.py ${dir}


    cd ${root_dir}

    #python abstract_offset.py --b ${bound}
    #mv *.facts ${testcase_root}${dir}"/facts/"


    #java -jar ${checker_dir}"AlocGenHelper.jar" ${testcase_root}${dir}"/src/"${dir}"_gcc" ${testcase_root}${dir}"/facts/"

#    sleep 5

    #java -jar ${checker_dir}"SyntaxChecker.jar" ${testcase_root}${dir}"/facts/" > ${testcase_root}${dir}"/"${dir}".txt"

start_tm=`date +%s%N`; 

    cd ${logic_pre_process_dir}
    souffle -F ${testcase_root}${dir}"/facts" -D ${testcase_root}${dir}"/facts" filter-noop.dl -j 16
 
    souffle -F ${testcase_root}${dir}"/facts" -D ${testcase_root}${dir}"/facts" remove-exit-inter-succ.dl -j 16


    #souffle -F ${testcase_root}${dir}"/facts" -D ${testcase_root}${dir}"/facts" boundary_gen.dl

    #souffle -F ${testcase_root}${dir}"/facts" -D ${testcase_root}${dir}"/facts" find_indirect_jmp_funcs.dl

    #souffle -F ${testcase_root}${dir}"/facts" -D ${testcase_root}${dir}"/facts" rgn_sub_section_header.dl

end_tm=`date +%s%N`; 
use_tm=`echo $end_tm $start_tm | awk '{ print ($1 - $2) / 1000000000}'` 
echo "Preprocess (boundary decision, indirect jump functions generation) time used: $use_tm (s)"

done

