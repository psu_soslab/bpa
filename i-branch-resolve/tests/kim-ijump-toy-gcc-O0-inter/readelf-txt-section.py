import os
import sys

def main(dir):
    command_line = "readelf -S " + str(dir) + "/src/" + str(dir) + "_gcc"

    txt_lst  = os.popen(command_line).readlines()
    word_lst = []
    for txt in txt_lst:
        if ".text" in txt:
            word_lst = txt.split(" ")
    if len(word_lst) == 0:
        print("error!!!!!!!!!!!!!!")
    
    word_lst_filter = []    
    for word in word_lst:
        if word != '':
            word_lst_filter.append(word)
    txt_sec_addr_start = (int (word_lst_filter[3], 16))
    txt_sec_size = (int (word_lst_filter[5], 16))
    txt_sec_addr_end = txt_sec_addr_start + txt_sec_size
    
    txt_sec_addr_start_simp = txt_sec_addr_start - 134512640
    txt_sec_addr_end_simp = txt_sec_addr_end - 134512640

    file_write = str(dir) + "/facts/txt_section.facts"
    file1 = open( file_write ,"w") 
    file1.write( str(txt_sec_addr_start_simp) + "\t" + str(txt_sec_addr_end_simp) + "\n")
    file1.close() 



if __name__ == "__main__":
    dir = sys.argv[1] #should be a dir name
    #option = sys.argv[2]
    #init_edge_f = dir + "/facts/edge.facts"
    main(dir)



#
#  [16] .rodata           PROGBITS        08052b60 00ab60 001214 00   A  0   0 32

