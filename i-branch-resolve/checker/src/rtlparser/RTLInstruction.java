package rtlparser;

import rtlparser.ast.*;

public class RTLInstruction{
	private String content; //The RTL string, leave for further usage
	
	private int order;
	private long addr;
	
	private RTLNode instAST; //Abstract syntax tree for current RTL instruction
	
	public RTLInstruction(String rtlstr, long addr) {
		this.addr=addr;
		int colonPos=rtlstr.indexOf(":");
		if(colonPos<0) { //abnormal
			this.order=-1;
			this.content=rtlstr;
		} else {
			this.order=Integer.parseInt(rtlstr.substring(0, colonPos));
			this.content=rtlstr.substring(colonPos+1, rtlstr.length());
		}
		instAST=RTLNode.getRtlAST(this.content, this.addr, this.order, RType.INST);
	}
	
	public long getAddr() {
		return this.addr;
	}
	public int getOrder() {
		return this.order;
	}
	public RTLNode getAST() {
		return this.instAST;
	}
	
	public void print() {
		System.out.println("   <"+this.addr+","+this.order+">:"+this.content);
		this.instAST.printInorder();
	}
}