package rtlparser;

import java.util.*;

public class Operand{
	enum Type{REG, MEM, CONST, INVALID};
	
	private static Set<String> regName32=new HashSet<String>(
				Arrays.asList(new String[] {"eax", "ebx", "ecx", "edx", "esp", "ebp", "edi", "esi"}));
	private static Set<String> regName16=new HashSet<String>(
				Arrays.asList(new String[] {"ax", "bx", "cx", "dx", "sp", "bp", "di", "si"}));
	private static Set<String> regName8=new HashSet<String>(
				Arrays.asList(new String[] {"ah", "bh", "ch", "dh", "al", "bl", "cl", "dl"}));
	
	private int size; //size in byte
	private Type type=Type.INVALID;
	private String name=""; //for reg name
	private long value=-1; //for immediate
	private Address address=null; //for address
	
	public Operand(String opstr) {
		opstr=opstr.toLowerCase();
		
		if(regName32.contains(opstr) ) {
			type=Type.REG;
			name=opstr;
			size=4;
		} else if(regName16.contains(opstr)) {
			type=Type.REG;
			name=opstr;
			size=2;
		} else if(regName8.contains(opstr)){ 
			type=Type.REG;
			name=opstr;
			size=1;
		} else if(opstr.startsWith("qword ptr")){//address of quadword-size mem-data "qword ptr [exp]", set size=8, type=MEM
			type=Type.MEM;
			size=8;
			int beginpos=opstr.indexOf("[");
			int endpos=opstr.indexOf("]");
			if(endpos>beginpos && beginpos>=0)
				address= new Address(opstr.substring(beginpos+1, endpos), Address.AddrType1.ABS);
		}else if(opstr.startsWith("dword ptr")){//address of dword-size mem-data "dword ptr [exp]", set size=4, type=MEM
			type=Type.MEM;
			size=4;
			int beginpos=opstr.indexOf("[");
			int endpos=opstr.indexOf("]");
			if(endpos>beginpos && beginpos>=0)
				address= new Address(opstr.substring(beginpos+1, endpos), Address.AddrType1.ABS);
		} else if(opstr.startsWith("word ptr")){//address of word-size mem-data "word ptr [exp]", set size=2, type=MEM
			type=Type.MEM;
			size=2;
			int beginpos=opstr.indexOf("[");
			int endpos=opstr.indexOf("]");
			if(endpos>beginpos && beginpos>=0)
				address= new Address(opstr.substring(beginpos+1, endpos), Address.AddrType1.ABS);
		} else if(opstr.startsWith("byte ptr")){//address of byte-size mem-data "byte ptr [exp]", set size=1, type=MEM
			type=Type.MEM;
			size=1;
			int beginpos=opstr.indexOf("[");
			int endpos=opstr.indexOf("]");
			if(endpos>beginpos && beginpos>=0)
				address= new Address(opstr.substring(beginpos+1, endpos), Address.AddrType1.ABS);
		} else if(opstr.matches("\\[[\\w\\s\\+\\-\\*]+\\]")) {//address of word-size mem-data "[exp]", set size=2, type=MEM
			type=Type.MEM;
			size=2;				//here is not definitely correct.
			int beginpos=opstr.indexOf("[");
			int endpos=opstr.indexOf("]");
			if(endpos>beginpos && beginpos>=0)
				address= new Address(opstr.substring(beginpos+1, endpos), Address.AddrType1.ABS);
		} else if(opstr.startsWith("0x0x")) { //immediate, to fix a problem from the CFG data
			type=Type.CONST;
			size=4;
			value=Long.parseLong(opstr.substring(4),16);
		} else if(opstr.startsWith("0x")) { //immediate
			type=Type.CONST;
			size=4;
			value=Long.parseLong(opstr.substring(2),16);
		} else if(opstr.startsWith("pc-rel")) { //relative address of the callee's start point
			type=Type.MEM;
			size=4;
			int beginpos=opstr.indexOf("0x");
			if(beginpos>=0)
				address=new Address(opstr.substring(beginpos),Address.AddrType1.REL);
		}

	}
	
	public void Print() {
		switch(type) {
		case REG:
			System.out.print(type.name()+"("+ size + "):"+ name);
			break;
		case MEM:
			System.out.print(type.name()+"("+ size + "):");
			address.Print();
			break;
		case CONST:
			System.out.print(type.name()+"("+size+"):");
			System.out.printf("%x<%d>", value, value);
			break;
		case INVALID:
			System.out.print(type.name());
			break;
		}
	}
}