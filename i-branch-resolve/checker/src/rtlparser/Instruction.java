package rtlparser;

import java.util.*;

import rtlparser.asmelem.*;

/* The assembly instruction at a specific address, attached with its RTL instructions. */
public class Instruction{
	private long address;
	private Opcode operator;
	private Prefix prefix;
	
	/*only support at most 3-operand instructions*/
	private Operand p1;
	private Operand p2;
	private Operand p3;
	
	private int numOfRTLInsts;
	private List<RTLInstruction> rtlinsts=new ArrayList<RTLInstruction>();
	
	public Instruction(long addr, Opcode op, Prefix pre, Operand op1, Operand op2, Operand op3,
			int numRTLs) {
		this.address=addr;
		this.operator=op;
		this.prefix=pre;
		this.p1=op1;
		this.p2=op2;
		this.p3=op3;
		this.numOfRTLInsts=numRTLs;
	}
	
	public List<RTLInstruction> getRTLInstructions(){
		return this.rtlinsts;
	}
	
	public Opcode getOpcode() {
		return this.operator;
	}
	
	public long getAddress() {
		return this.address;
	}
	
	public void addRTLInstruction(RTLInstruction rins) {
		this.rtlinsts.add(rins);
	}

	public void Print() {
		System.out.printf("%x<%d>(%d):",address,address,numOfRTLInsts);
		if(this.prefix!=Prefix.NONE) {
			System.out.print(this.prefix.name()+" ");
		}
		System.out.print(operator.name()+" ");
		if(p1!=null) {
			p1.Print();
			if(p2!=null) {
				System.out.print(", ");
				p2.Print();
				if(p3!=null) {
					System.out.print(", ");
					p3.Print();
				}
			}
		}
		System.out.println();
		for(int i=0;i<this.rtlinsts.size();i++) {
			RTLInstruction rins=this.rtlinsts.get(i);
			if(rins!=null) {
				rins.print();
				System.out.println();
			}
		}
		//System.out.println();
	}
	



}