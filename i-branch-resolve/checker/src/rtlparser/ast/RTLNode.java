package rtlparser.ast;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author suncong
 * The data structure of abstract syntax tree of RTL instruction
 * 
 */
public class RTLNode{
	private static final String[] BVOps= {"+","-","*","/s","/u","%u","%s","&","|","^","<<",">>",">>u","ROR","ROL"};
	private static final String[] TestOps= {"==", "<", "<u"};
	private static final String[] FloatArithOps= {"+.", "-.", "*.", "/."};
	
	private static final String ifPattern="IF([\\s\\w\\+,\\-\\*\\^\\&\\|=<>\\(\\)\\$\\:]*?)DO([\\s\\w\\+,\\-\\*\\^\\&\\|=<>\\(\\)\\$\\:]*?)";
	private static final String assignPattern="([\\s\\w\\+,\\-\\*\\^\\&\\|=<>\\(\\)\\$\\:]*?)<\\-\\-([\\s\\w\\+,\\-\\*\\^\\&\\|=<>\\(\\)\\$\\:]*?)";
	
	private long addr;
	private int order;
	private RType rt; //expression or instruction
	private RInstType type=RInstType.nop_rtl; //if instruction, storing the concrete instruction type
	private RExprType etype=RExprType.nop_expr; // if expression, stroing the concrete expression type
	private long value; //Only for storing const value, or the first cast field.
	private int cast_field2; //Only for storing the second cast field.
	
	private int cntChildren; //The number of children of the RTL instruction/expression.
	private RTLNode[] children=new RTLNode[4]; // Allow for at most four children.
	
	/*Recursively generate the abstract syntax tree for the RTL instruction/expression string*/
	public static RTLNode getRtlAST(String s, long addr, int order, RType rt) {
		switch(rt) {
		case INST:
			Pattern p=Pattern.compile(ifPattern);
			Matcher m=p.matcher(s);
			if(m.matches()) {
				RTLNode node=new RTLNode(addr, order, rt);
				node.cntChildren=2;
				node.type=RInstType.if_rtl;
				node.children[0]=RTLNode.getRtlAST(m.group(1).trim(), addr, order, RType.EXPR);
				node.children[1]=RTLNode.getRtlAST(m.group(2).trim(), addr, order, RType.INST);
				return node;
			} else {
				p=Pattern.compile(assignPattern);
				m=p.matcher(s);
				if(m.matches()) {
					RTLNode node=new RTLNode(addr, order, rt);
					node.cntChildren=2;
					node.type=RInstType.assign_rtl;
					node.children[0]=RTLNode.getRtlAST(m.group(1).trim(), addr, order, RType.EXPR);
					node.children[1]=RTLNode.getRtlAST(m.group(2).trim(), addr, order, RType.EXPR);
					return node;
				} else if(s.toLowerCase().equals("trapinstr")) {
					RTLNode node=new RTLNode(addr, order, rt);
					node.cntChildren=0;
					node.type=RInstType.trap_rtl;
					return node;
				} else if(s.toLowerCase().equals("errorinstr")) {
					RTLNode node=new RTLNode(addr, order, rt);
					node.cntChildren=0;
					node.type=RInstType.error_rtl;
					return node;
				} else if(s.toLowerCase().equals("advance_oracle")) {
					RTLNode node=new RTLNode(addr, order, rt);
					node.cntChildren=0;
					node.type=RInstType.advance_oracle_rtl;
					return node;
				}
			} 
			break;
		case EXPR:
			List<String> tokens=RTLNode.getTokens(s);

			return RTLNode.getRtlExpAST(addr, order, tokens);
		default:
			break;
		}
		return null;
	}
	
	/* Derive the AST for an RTL expression whose tokens are delivered by the list */
	private static RTLNode getRtlExpAST(long addr, int order, List<String> tokens) {
		//Stack<RTLNode> operands, Stack<String> ops,
		Stack<RTLNode> operands=new Stack<RTLNode>();
		Stack<String> ops=new Stack<String>();
		
		for(int i=0;i<tokens.size();i++) {
			String symbol=tokens.get(i);
			if (RTLNode.isOp(symbol) || symbol.equals("(")) { // operations, use # to represent deref on building AST.
				ops.push(symbol);
				continue;
			}  else if(!symbol.equals(")")) { //not op, not "()", plain operand, push on operands;
				RTLNode node=new RTLNode(symbol,addr,order);
				operands.push(node);
				continue;
			} else if(symbol.equals(")")) {
				while(RTLNode.isOp(ops.peek())){
					String tmpOp=ops.pop();
					if(RTLNode.isUnaryOp(tmpOp)) {
						RTLNode opd=operands.pop();
						RTLNode node=new RTLNode(tmpOp, addr, order, opd);
						operands.push(node);
						continue;
					} else {// binary op
						RTLNode right=operands.pop();
						RTLNode left=operands.pop();
						RTLNode node=new RTLNode(tmpOp, addr, order, left, right);
						operands.push(node);
						continue;
					}
				}
				if(!ops.isEmpty() && ops.peek().equals("(")) {
					ops.pop();
				}
			}
		}
		if(!ops.isEmpty()) {// the last outmost *
			if(ops.peek().equals("#")) {
				RTLNode node=new RTLNode(ops.pop(), addr, order, operands.pop());
				return node;
			}
		}
		return operands.pop();
	}
	
	/* Construct the AST node for a single token of RTL expression */
	public RTLNode(String single, long addr, int order) {
		this.addr=addr;
		this.order=order;
		this.rt=RType.EXPR;
		
		this.etype=RExprType.getExprType(single);
		if(single.startsWith("$-")) {
			this.etype=RExprType.CONST;
			this.value=-Long.parseLong(single.substring(4), 16);
		} else if(single.startsWith("$")) {
			this.etype=RExprType.CONST;
			this.value=Long.parseLong(single.substring(3), 16);
		}
		this.cntChildren=0;
	}
	
	public RTLNode(long addr, int order, RType rtype) {
		this.addr=addr;
		this.order=order;
		this.rt=rtype;
	}
	
	/* Build AST for the unary operations, with 'child' as its child. */
	public RTLNode(String unaryOp, long addr, int order, RTLNode child) {
		if(unaryOp.equals("#")) {
			this.etype=RExprType.DEREF;
		} else {
			Pattern castPattern=Pattern.compile("CastU<([0-9]+),([0-9]+)>");
			Matcher castMatch=castPattern.matcher(unaryOp);
			if(castMatch.matches()) {
				this.etype=RExprType.CASTU;
				this.value=Long.parseLong(castMatch.group(1));
				this.cast_field2=Integer.parseInt(castMatch.group(2));
			} else {
				castPattern=Pattern.compile("CastS<([0-9]+),([0-9]+)>");
				castMatch=castPattern.matcher(unaryOp);
				if(castMatch.matches()) {
					this.etype=RExprType.CASTS;
					this.value=Long.parseLong(castMatch.group(1));
					this.cast_field2=Integer.parseInt(castMatch.group(2));
				}
			}
		}
		this.addr=addr;
		this.order=order;
		this.rt=RType.EXPR;
		this.cntChildren=1;
		this.children[0]=child;
	}
	
	/* Build AST for the binary operations, with left and right as its children */
	public RTLNode(String binaryOp, long addr, int order, RTLNode left, RTLNode right) {
		this.addr=addr;
		this.order=order;
		this.rt=RType.EXPR;
		this.etype=RExprType.getBinOpType(binaryOp);
		this.cntChildren=2;
		this.children[0]=left;
		this.children[1]=right;
	}
	
	//Build AST for the if_rtl_exp, i.e. three children
	public RTLNode(long addr, int order, RTLNode op, RTLNode left, RTLNode right) {
		this.addr=addr;
		this.order=order;
		this.rt=RType.EXPR;
		this.cntChildren=3;
		this.children[0]=op;
		this.children[1]=left;
		this.children[2]=right;
	}
	
	/* Separate the RTL expression string into a list of tokens for parsing */
	private static List<String> getTokens(String s) {
		List<String> tokens=new ArrayList<String>();
		
		StringTokenizer st=new StringTokenizer(s,"() \t",true);
		while(st.hasMoreTokens()) {
			String tmp=st.nextToken();
			if(!tmp.equals(" ") && !tmp.equals("\t"))
				tokens.add(tmp);
		}
		/*introduce heuristic to make dereference different from multiplication*/
		if(tokens.get(0).equals("*"))
			tokens.set(0, "#");
		for(int i=1;i<tokens.size();i++) {
			if (tokens.get(i).equals("*") && 
				(tokens.get(i-1).matches("CastU<[0-9]+,[0-9]+>") 
				 || tokens.get(i-1).matches("CastS<[0-9]+,[0-9]+>")) ) {
				tokens.set(i, "#");
			}
		}
		return tokens;
	}

/*	public void print() {
		System.out.println("<"+this.type.name()+","+this.etype.name()+">");
		for(int i=0;i<this.cntChildren;i++) {
			System.out.print("  (");
			this.children[i].print();
			System.out.print("  )");
		}
	}*/
	
	/* To print like the original RTL instruction text */
	public void printInorder() {
		if(this.rt==RType.INST) {
			if(this.type==RInstType.if_rtl) {
				System.out.print("IF ");
				this.children[0].printInorder();
				System.out.println(" DO");
				System.out.print("\t");
				this.children[1].printInorder();
			} else if(this.type==RInstType.assign_rtl) {
				this.children[0].printInorder();
				System.out.print(" <-- ");
				this.children[1].printInorder();
			} else if(this.type==RInstType.trap_rtl) {
				System.out.print("TrapInstr");
			} else if(this.type==RInstType.error_rtl) {
				System.out.print("ErrorInstr");
			} else if(this.type==RInstType.advance_oracle_rtl) {
				System.out.print("Advance_Oracle");
			}
			System.out.println();
		} else if(this.rt==RType.EXPR) {
			if(this.cntChildren==3) { // for the if_rtl_expr
				System.out.print("IF (");
				this.children[0].printInorder();
				System.out.print(") THEN ");
				this.children[1].printInorder();
				System.out.print(" ELSE ");
				this.children[2].printInorder();
			} else if(this.cntChildren==2) {
				System.out.print("(");
				this.children[0].printInorder();
				System.out.print(" "+RExprType.symbolForPrint(this.etype)+" ");
				this.children[1].printInorder();
				System.out.print(")");
			} else if(this.cntChildren==1) {
				if(this.etype==RExprType.DEREF) {
					System.out.print(RExprType.symbolForPrint(this.etype));
					System.out.print(" ");
					this.children[0].printInorder();
				} else {
					System.out.print("(");
					System.out.print(RExprType.symbolForPrint(this.etype));
					if(this.etype==RExprType.CASTS || this.etype==RExprType.CASTU) {
						System.out.print("<"+this.value+","+this.cast_field2+">");
					}
					System.out.print(" ");
					this.children[0].printInorder();
					System.out.print(")");
				}
			} else if(this.cntChildren==0) {
				if(this.etype==RExprType.CONST) {
					System.out.print("$"+
					(this.value>=0?
							"0x"+Long.toHexString(this.value)+"("+this.value+")":
								"-0x"+Long.toHexString(-this.value)+"("+this.value+")" ));
				} else {
					System.out.print(RExprType.symbolForPrint(this.etype));
				}
			}
		}
	}
	
	public void setCntChildren(int cnt) {
		this.cntChildren=cnt;
	}
	public void setChild(int index, RTLNode node) {
		if(index<0 || index>3)
			return;
		this.children[index]=node;
	}
	public void setRInstType(RInstType rit) {
		this.type=rit;
	}
	public void setRExprType(RExprType ret) {
		this.etype=ret;
	}
	public void setValue(Long val) {
		this.value=val;
	}
	public void setCastField2(int f) {
		this.cast_field2=f;
	}
	
	private static boolean inBVOps(String s) { //actually this procedure accept unary deref "*"
		for(String ss:BVOps) {
			if(ss.equals(s))
				return true;
		}
		return false;
	}
	
	private static boolean inTestOps(String s) {
		for(String ss:TestOps) {
			if(ss.equals(s))
				return true;
		}
		return false;
	}
	
	private static boolean inFloatArithOps(String s) {
		for(String ss:FloatArithOps) {
			if(ss.equals(s))
				return true;
		}
		return false;
	}
	
	private static boolean isOp(String s) {
		return (RTLNode.inBVOps(s) || RTLNode.inTestOps(s) || 
				RTLNode.inFloatArithOps(s) || s.matches("CastU<[0-9]+,[0-9]+>") ||
				s.matches("CastS<[0-9]+,[0-9]+>") || s.equals("#"));
	}
	
/*	private static boolean isUnaryOpForPush(String s, Stack<String> ops) {
		if(!RTLNode.isOp(s))
			return false;
		if (s.matches("CastU<[0-9]+,[0-9]+>") || s.matches("CastS<[0-9]+,[0-9]+>"))
			return true;
		else if(s.equals("*")) {
			if(ops.isEmpty())
				return true;
			else {
				String tmp=ops.peek();
				if(tmp.matches("CastU<[0-9]+,[0-9]+>") || tmp.matches("CastS<[0-9]+,[0-9]+>"))
					return true;
			}
		}
		return false;
	}*/
	
	private static boolean isUnaryOp(String s) {
		if(!RTLNode.isOp(s))
			return false;
		else if (s.matches("CastU<[0-9]+,[0-9]+>") || s.matches("CastS<[0-9]+,[0-9]+>")
				|| s.equals("#"))
			return true;
		return false;
	}
}