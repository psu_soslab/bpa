package rtlparser.ast;

/**
 * 
 * @author suncong
 * The type of RTL instruction.
 * We do not distinguish different value assignments
 * 
 */
public enum RInstType{
	//set_loc_rtl,
	//set_ps_reg_rtl,
	//set_array_rtl,
	//set_byte_rtl,
	assign_rtl,
	advance_oracle_rtl,
	if_rtl,
	error_rtl,
	trap_rtl,

	nop_rtl
}