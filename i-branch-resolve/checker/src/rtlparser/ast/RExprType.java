package rtlparser.ast;

/**
 * 
 * @author suncong
 * The type of RTL expressions
 * The size is consistent with the datalog size definition
 * 
 */
public enum RExprType{
	BV_PLUS(-1), BV_MINUS(-1), BV_MULT(-1), BV_DIVS(-1), BV_DIVU(-1),
	BV_REMU(-1), BV_REMS(-1), BV_AND(-1), BV_OR(-1), BV_XOR(-1),
	BV_SHIFTL(-1), BV_SHIFTR(-1), BV_USHIFTR(-1), BV_ROR(-1), BV_ROL(-1),
	
	TEST_EQ(-1), TEST_LESS(-1), TEST_ULESS(-1),
	
	FLOAT_PLUS(-1), FLOAT_MINUS(-1), FLOAT_MULT(-1), FLOAT_DIV(-1),
	
	DEREF(-1),
	CASTS(-1),
	CASTU(-1),
	
	nop_expr(-1),
	
	CONST(-1),
	RANDOM(-1),
	
	EAX(32), ECX(32), EDX(32), EBX(32),
	ESP(32), EBP(32), ESI(32), EDI(32),
	
	ES_S(32), CS_S(32), SS_S(32), DS_S(32), FS_S(32), GS_S(32),	
	ES_L(32), CS_L(32), SS_L(32), DS_L(32), FS_L(32), GS_L(32),
	
	PC(32),
	
	ID(1), VIP(1), VIF(1), AC(1), VM(1), RF(1), NT(1), IOPL(1),
	OF(1), DF(1), IF_flag(1), TF(1), SF(1), ZF(1), AF(1), PF(1), CF(1),
	//control_register_loc
	CR0(32), CR2(32), CR3(32), CR4(32),
	//debug_register_loc
	DR0(32), DR1(32), DR2(32), DR3(32), DR6(32), DR7(32),
	
	FPU_stktop(3),
	//fpu_flag
	F_Busy(1), F_C3(1), F_C2(1), F_C1(1), F_C0(1), 
	F_ES(1), F_SF(1), F_PE(1), F_UE(1), F_OE(1), F_ZE(1), F_DE(1), F_IE(1),

	FPU_RCtrl(2),
	FPU_PCtrl(2),

	F_Res15(1), F_Res14(1), F_Res13(1), F_Res7(1), F_Res6(1), F_IC(1), F_PM(1), 
	F_UM(1), F_OM(1), F_ZM(1), F_DM(1), F_IM(1),

	FPU_lastInstrPtr(48),
	FPU_lastDataPtr(48),

	FPU_lastOpcode(11);
	
	private int size;
	
	private RExprType(int sz){
		this.size=sz;
	}
	
	/* Use RTL expression string (not symbol as '+/-') to get the RExprType */
	public static RExprType getExprType(String s) {
		for(RExprType e: RExprType.values()) {
			if(e.name().toLowerCase().equals(s.toLowerCase()))
				return e;
		}
		if(s.equals("random"))
			return RANDOM;
		else if(s.equals("StartSS"))
			return SS_S;
		else if(s.equals("LimitSS"))
			return SS_L;
		else if(s.equals("StartDS"))
			return DS_S;
		else if(s.equals("LimitDS"))
			return DS_L;
		else if(s.equals("StartCS"))
			return CS_S;
		else if(s.equals("LimitCS"))
			return CS_L;
		else if(s.equals("StartES"))
			return ES_S;
		else if(s.equals("LimitES"))
			return ES_L;
		else if(s.equals("StartFS"))
			return FS_S;
		else if(s.equals("LimitFS"))
			return FS_L;
		else if(s.equals("StartGS"))
			return GS_S;
		else if(s.equals("LimitGS"))
			return GS_L;
		else if(s.equals("IF"))
			return IF_flag;
		return nop_expr;
	}
	
	/* Use the binary operation string (arith/test) to get the RExprType */
	public static RExprType getBinOpType(String binop) {
		switch(binop) {
		case "+":
			return BV_PLUS;
		case "-":
			return BV_MINUS;
		case "*":
			return BV_MULT;
		case "/s":
			return BV_DIVS;
		case "/u":
			return BV_DIVU;
		case "%u":
			return BV_REMU;
		case "%s":
			return BV_REMS;
		case "&":
			return BV_AND;
		case "|":
			return BV_OR;
		case "^":
			return BV_XOR;
		case "<<":
			return BV_SHIFTL;
		case ">>":
			return BV_SHIFTR;
		case ">>u":
			return BV_USHIFTR;
		case "ROR":
			return BV_ROR;
		case "ROL":
			return BV_ROL;
		case "==":
			return TEST_EQ;
		case "<":
			return TEST_LESS;
		case "<u":
			return TEST_ULESS;
		case "+.":
			return FLOAT_PLUS;
		case "-.":
			return FLOAT_MINUS;
		case "*.":
			return FLOAT_MULT;
		case "/.":
			return FLOAT_DIV;
		default:
			return nop_expr;
		}
	}
	
	/* Use the RExprType to get the respective string for printing */
	public static String symbolForPrint(RExprType rt) {
		switch(rt) {
		case BV_PLUS:
			return "+";
		case BV_MINUS:
			return "-";
		case BV_MULT:
			return "*";
		case BV_DIVS:
			return "/s";
		case BV_DIVU:
			return "/u";
		case BV_REMU:
			return "%u";
		case BV_REMS:
			return "%s";
		case BV_AND:
			return "&";
		case BV_OR:
			return "|";
		case BV_XOR:
			return "^";
		case BV_SHIFTL:
			return "<<";
		case BV_SHIFTR:
			return ">>";
		case BV_USHIFTR:
			return ">>u";
		case BV_ROR:
			return "ROR";
		case BV_ROL:
			return "ROL";
		case TEST_EQ:
			return "==";
		case TEST_LESS:
			return "<";
		case TEST_ULESS:
			return "<u";
		case FLOAT_PLUS:
			return "+.";
		case FLOAT_MINUS:
			return "-.";
		case FLOAT_MULT:
			return "*.";
		case FLOAT_DIV:
			return "/.";
		case ES_S:
			return "StartES";
		case ES_L:
			return "LimitES";
		case CS_S:
			return "StartCS";
		case CS_L:
			return "LimitCS";
		case SS_S:
			return "StartSS";
		case SS_L:
			return "LimitSS";
		case DS_S:
			return "StartDS";
		case DS_L:
			return "LimitDS";
		case FS_S:
			return "StartFS";
		case FS_L:
			return "LimitFS";
		case GS_S:
			return "StartGS";
		case GS_L:
			return "LimitGS";
		case DEREF:
			return "*";
		case EAX:
		case EBX:
		case ECX:
		case EDX:
		case ESP:
		case EBP:
		case ESI:
		case EDI:
		case PC:
		case ID:
		case VIP:
		case VIF:
		case AC:
		case VM:
		case RF:
		case NT:
		case IOPL:
		case OF:
		case DF:
		case IF_flag:
		case TF:
		case SF:
		case ZF:
		case AF:
		case PF:
		case CF:
		case CASTS:
		case CASTU:
		case CR0:
		case CR2:
		case CR3:
		case CR4:
		case DR0:
		case DR1:
		case DR2:
		case DR3:
		case DR6:
		case DR7:
		case RANDOM:
			return rt.name();
		default:
			return "";
		}
	}
}