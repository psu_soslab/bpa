package rtlparser;

import java.util.*;
import java.util.regex.*;

import rtlparser.asmelem.*;

public class BasicBlock{
	/*Coarse pattern to match a basic block*/
	private static final String regexBlock = "\\{[\\.\\s\\w\\[\\]=\\+,\\-\\*]*\\}";
	
	/*More fine-grained pattern to match each field of basic block*/
	private static final String regexBB = "\\{\\s*bb_label = ([0-9a-fA-F]+)\\s+" + 			/*group(1)*/
										"bb_symbol_label = ([\\w\\.]*)\\s+" + 				/*group(2)*/
										"bb_instrs = ([\\s\\w\\[\\]=\\+,\\-\\*]*)" +		/*group(3)*/
										"bb_relAddr = ([0-9a-fA-F]*)\\s+" +					/*group(4)*/
										"bb_succs = ([0-9a-fA-F\\s]*)\\s+" + 				/*group(5)*/
										"bb_inter_succs = ([0-9a-fA-F\\s]*)\\s+" +			/*group(6)*/ 
										"bb_preds = ([0-9a-fA-F\\s]*)\\s+" + 				/*group(7)*/
										"bb_inter_preds = \\s+([0-9a-fA-F\\s]*)\\}";		/*group(8)*/
	
	/*Pattern for a instruction line*/
	private static final String regexInstLine= "^([0-9a-fA-F]+)\\s+([0-9a-fA-F]+)\\s+(\\w+)\\s*([\\w\\s\\,\\[\\]\\+\\-\\*]*)$";
	/*Pattern to identify instruction prefix*/
	private static final String regexInstWithPrefix="(\\w+)\\s*([\\w\\s\\,\\[\\]\\+\\-\\*]*)";
	/*Pattern to identify an RTL instruction*/
	private static final String regexRtlInst="[0-9]+?:[\\s\\w\\+,\\-\\*\\^\\&\\|=<>\\(\\)\\$\\:]*?\\n\\n";
	//private static final String regexRtlInst="[0-9]+?:([(?!\\n)\\s\\w\\+,\\-\\*\\^\\&\\|=<>\\(\\)\\$\\:]*?)";
	
	/*A Map to get assembly opcode through the instruction string*/
	private static Map<String,Opcode> opMap=new HashMap<String, Opcode>();
	static {
		Opcode[] ops=Opcode.values();
		for(Opcode op: ops) {
			opMap.put(op.name().toLowerCase(), op);
		}
	}
	/*A Map to get the assembly prefix through the prefix string*/
	private static Map<String,Prefix> prefixMap=new HashMap<String, Prefix>();
	static {
		Prefix[] pres=Prefix.values();
		for(Prefix pre: pres) {
			prefixMap.put(pre.name().toLowerCase(), pre);
		}
	}
	
	/*Start address of basic block*/
	private long startAddr=-1;
	/* If the basic block is the first one in a function, this field contain the 
	 * function name. Otherwise, this field should be empty*/
	private String procName="";
	/* The instruction sequence of basic block */
	private List<Instruction> insts = new ArrayList<Instruction>();
	
	/* The start addresses of the succ blocks*/
	private List<Long> succs_addrlist=new ArrayList<Long>();
	/* The list points to the succ blocks */
	private List<BasicBlock> succs=new ArrayList<BasicBlock>();
	/* The start addresses of the inter_succ blocks */
	private List<Long> inter_succs_addrlist=new ArrayList<Long>();
	/* The list points to the inter_succ blocks */
	private List<BasicBlock> inter_succs=new ArrayList<BasicBlock>();
	/* The start addresses of the pred blocks */
	private List<Long> preds_addrlist=new ArrayList<Long>();
	/* The list points to the pred blocks */
	private List<BasicBlock> preds=new ArrayList<BasicBlock>();
	/* The start addresses of the inter_pred blocks */
	private List<Long> inter_preds_addrlist=new ArrayList<Long>();
	/* The list points to the inter_pred blocks */
	private List<BasicBlock> inter_preds=new ArrayList<BasicBlock>();
	
	public void parseBlock(String blockStr) {
		Pattern p = Pattern.compile(BasicBlock.regexBlock);
		Matcher m = p.matcher(blockStr);
		if(!m.find()) {
			System.out.println("Block string format incorrect!");
			return;
		}
		
		p=Pattern.compile(BasicBlock.regexBB);
		m=p.matcher(blockStr);
		if(m.find()) {
			System.out.println("----------------------");
			startAddr=Long.parseLong(m.group(1), 16);
			System.out.printf("bb_label = %x\n",startAddr);
			
			procName= m.group(2).isEmpty()? "":m.group(2);
			System.out.println("bb_symbol_label = "+procName);
			
			//Parsing of instructions
			this.parseInstructions(m.group(3).trim());
			
			//we don't care the bb_relAddr now, i.e. m.group(4)
			
			String s=m.group(5).trim();
			if(!s.isEmpty()) {
				String[] ss= s.split("\\s");
				for(int i=0;i<ss.length;i++) {
					if(!ss[i].toLowerCase().equals("ffffffff"))
						succs_addrlist.add(Long.parseLong(ss[i],16));
				}
			}
			//this.PrintAddrList(succs_addrlist);
			
			s=m.group(6).trim();
			if(!s.isEmpty()) {
				String[] ss= s.split("\\s");
				for(int i=0;i<ss.length;i++) {
					if(!ss[i].toLowerCase().equals("ffffffff"))
						inter_succs_addrlist.add(Long.parseLong(ss[i],16));
				}
			}
			//this.PrintAddrList(inter_succs_addrlist);
			
			s=m.group(7).trim();
			if(!s.isEmpty()) {
				String[] ss= s.split("\\s");
				for(int i=0;i<ss.length;i++) {
					if(!ss[i].toLowerCase().equals("ffffffff"))
						preds_addrlist.add(Long.parseLong(ss[i],16));
				}
			}
			//this.PrintAddrList(preds_addrlist);
			
			s=m.group(8).trim();
			if(!s.isEmpty()) {
				String[] ss= s.split("\\s");
				for(int i=0;i<ss.length;i++) {
					if(!ss[i].toLowerCase().equals("ffffffff"))
						inter_preds_addrlist.add(Long.parseLong(ss[i],16));
				}
			}
			//this.PrintAddrList(inter_preds_addrlist);
		}
	}
	
	/* For each string of instruction, generate the Instruction object
	 * and insert it into the list 'insts'*/
	public void parseInstructions(String insts) {
		String[] inst_array=insts.split("\n");

		Pattern instPattern=Pattern.compile(BasicBlock.regexInstLine);
		for(int i=0;i<inst_array.length;i++) {
			Matcher m=instPattern.matcher(inst_array[i].trim());
			if(m.find()) {
				/*derive the instruction address from m.group(1)*/
				int tmp=Integer.parseInt(m.group(1),16);
				long addr=tmp+Integer.parseInt("8048000", 16);
				
				int numOfRTLInsts=Integer.parseInt(m.group(2));

				String instop=m.group(3).toLowerCase().trim();
				String inst_rest=m.group(4).toLowerCase().trim();
				Prefix tmpPrefix=Prefix.NONE;
				
				/*first to deal with the possible prefix, i.e. 'rep'*/
				if(instop.equals("rep") || instop.equals("repz") || instop.equals("repnz") || instop.equals("lock") ||
						instop.equals("op_override")) {
					tmpPrefix=prefixMap.get(instop);
					Pattern instPattern2=Pattern.compile(BasicBlock.regexInstWithPrefix);
					Matcher m2=instPattern2.matcher(m.group(4).trim());
					if(m2.matches()) {
						instop=m2.group(1).toLowerCase();
						inst_rest=m2.group(2).toLowerCase();
					}
				}
				
				if(instop.equals("mov") || instop.equals("movzx") || instop.equals("movsx") || instop.equals("movs") ||
					instop.equals("cmove") || instop.equals("cmovb") || instop.equals("cmovbe") || instop.equals("cmovl") ||
					instop.equals("cmovle") || instop.equals("cmovnb") || instop.equals("cmovnbe") || instop.equals("cmovne") ||
					instop.equals("cmovnl") || instop.equals("cmovnle") || instop.equals("cmovns") || instop.equals("cmovs") ||
					instop.equals("stos") || instop.equals("scas") || instop.equals("and") || instop.equals("or") ||
					instop.equals("xor") || instop.equals("bt") || instop.equals("bsr") || instop.equals("test") ||
					instop.equals("xadd") || instop.equals("add") || instop.equals("adc") || instop.equals("sub") ||
					instop.equals("sbb") || instop.equals("sar") || instop.equals("shr") || instop.equals("sal") ||
					instop.equals("shl") || instop.equals("rcl") || instop.equals("rcr") || instop.equals("rol") ||
					instop.equals("ror") || instop.equals("cmp") || instop.equals("cmps") || instop.equals("cmpxchg") ||
					instop.equals("xchg") || instop.equals("lea")) {
					/*Two-operand instructions*/
					String[] ops=inst_rest.split(",");
					Instruction inst=new Instruction(addr,opMap.get(instop), tmpPrefix, 
													new Operand(ops[0].trim()), new Operand(ops[1].trim()), null,
													numOfRTLInsts);
					inst.Print();
					this.insts.add(inst);
				} else if(instop.equals("call") || instop.equals("jmp") || instop.equals("je") || instop.equals("jne") ||
						instop.equals("jnb") || instop.equals("jnbe") || instop.equals("jnl") || instop.equals("jnle") ||
						instop.equals("jl") || instop.equals("jle") || instop.equals("jb") || instop.equals("jbe") ||
						instop.equals("jns") || instop.equals("js") || instop.equals("push") || instop.equals("pop") ||
						instop.equals("div") || instop.equals("mul") || instop.equals("idiv") || instop.equals("not") ||
						instop.equals("neg") || instop.equals("setnbe") || instop.equals("setb") || instop.equals("sete") ||
						instop.equals("setne") || instop.equals("setbe") || instop.equals("setle") || instop.equals("setnl") ||
						instop.equals("setnle") || instop.equals("inc") || instop.equals("dec")) {
					/*One-operand instructions*/
					Instruction inst=new Instruction(addr,opMap.get(instop), tmpPrefix, 
													new Operand(inst_rest.trim()), null, null, numOfRTLInsts);
					inst.Print();
					this.insts.add(inst);
				} else if(instop.equals("shld") || instop.equals("shrd")) {
					/*Three-operand instructions*/
					String[] ops=inst_rest.split(",");
					Instruction inst=new Instruction(addr,opMap.get(instop), tmpPrefix, 
							new Operand(ops[0].trim()), new Operand(ops[1].trim()), new Operand(ops[2].trim()),
							numOfRTLInsts);
					inst.Print();
					this.insts.add(inst);
				}
				
				else if(instop.equals("imul")) {
					Instruction inst;
					if(inst_rest.indexOf(",")>=0) { //more than one operands
						String[] ops=inst_rest.split(",");
						if(ops.length==2) {
							inst=new Instruction(addr,opMap.get(instop), tmpPrefix, 
									new Operand(ops[0].trim()), new Operand(ops[1].trim()), null,
									numOfRTLInsts);
						} else { //ops.length==3
							inst=new Instruction(addr,opMap.get(instop), tmpPrefix, 
									new Operand(ops[0].trim()), new Operand(ops[1].trim()), new Operand(ops[2].trim()),
									numOfRTLInsts);
						}
					} else { //one operand
						inst=new Instruction(addr,opMap.get(instop), tmpPrefix, new Operand(inst_rest.trim()), null, null,
								numOfRTLInsts);
					}
					inst.Print();
					this.insts.add(inst);
				} 

				else if(instop.equals("leave") || instop.equals("ret") || instop.equals("hlt") || instop.equals("cdq") ||
						instop.equals("cpuid")) {
					/*No-operand instructions*/
					Instruction inst=new Instruction(addr,opMap.get(instop), tmpPrefix, null, null, null, numOfRTLInsts);
					inst.Print();
					this.insts.add(inst);
				} else {// for unsupported instructions, just change it to nop
					Instruction inst=new Instruction(addr,Opcode.NOP, tmpPrefix, null, null, null, numOfRTLInsts);
					inst.Print();
					this.insts.add(inst);
				}
			}
		}
	}
	
	/* Given a string of RTL section, parse to get the RTL instructions 
	 * and attach them to the assembly instruction */
	public void parseRTLs(Instruction inst, String rtlSection) {
		Pattern p=Pattern.compile(CFGParser.regexRTLs);
		Matcher m=p.matcher(rtlSection);
		if(m.matches()) {
			p=Pattern.compile(BasicBlock.regexRtlInst);
			m=p.matcher(m.group(1).trim());
			StringBuffer sb=new StringBuffer();
			while(m.find()) {
				inst.addRTLInstruction(new RTLInstruction(m.group().trim(), inst.getAddress()));
				m.appendReplacement(sb, "");
			}
			m.appendTail(sb);
			//Deal with the last RTL instruction
			inst.addRTLInstruction(new RTLInstruction(sb.toString().trim(), inst.getAddress()));
		}
	}
	
	public Long getInitialAddr() {
		return startAddr;
	}
	
	public String getProcName() {
		return procName;
	}
	
	public List<Long> getSuccsAddrList(){
		return succs_addrlist;
	}
	
	public List<Long> getInterSuccsAddrList(){
		return inter_succs_addrlist;
	}
	
	public List<Long> getPredsAddrList(){
		return preds_addrlist;
	}
	
	public List<Long> getInterPredsAddrList(){
		return inter_preds_addrlist;
	}
	
	public Instruction getInstruction(int index) {
		return this.insts.get(index);
	}
	
	public int getInstructionNumber() {
		return this.insts.size();
	}
	
	public void addSucc(BasicBlock bb) {
		if(bb!=null)
			succs.add(bb);
	}
	
	public void addInterSucc(BasicBlock bb) {
		if(bb!=null)
			inter_succs.add(bb);
	}
	
	public void addPred(BasicBlock bb) {
		if(bb!=null)
			preds.add(bb);
	}
	
	public void addInterPred(BasicBlock bb) {
		if(bb!=null)
			inter_preds.add(bb);
	}
	
	public Opcode getLastInstructionType() {
		Instruction tmpInst=this.insts.get(this.insts.size()-1);
		if(tmpInst.getOpcode()==null)
			System.out.println("cannot get opcode!");
		return tmpInst.getOpcode();
	}
	
	public long getLastInstructionAddr() {
		Instruction tmpInst=this.insts.get(this.insts.size()-1);
		return tmpInst.getAddress();
	}
	
	public void printInstructions() {
		Iterator<Instruction> it= insts.iterator();
		while(it.hasNext()) {
			it.next().Print();
		}
	}
	
	public List<Instruction> getInstructions(){
		return this.insts;
	}

}