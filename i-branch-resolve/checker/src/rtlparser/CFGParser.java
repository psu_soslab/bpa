package rtlparser;

import java.util.regex.*;

import rtlparser.asmelem.*;

import java.io.*;
import java.util.*;

/**
 * Parser to build the structure of CFG raw data
 * @author Cong Sun
 */
public class CFGParser{
	/*Regular expression to trim the head of CFG raw data*/
	private static final String regexHeadToTrim = "^[\\w\\ ]*\\.+$";
	/*Regular expression to identify a basic block in the CFG raw data*/
	private static final String regexBlock = "\\{[\\.\\s\\w\\[\\]=\\+,\\-\\*]*\\}";
	/*Regular expression to identify a RTL instruction block for a specific assembly instruction*/
	static final String regexRTLs = "RTL\\{([\\s\\w\\+,\\-\\*\\^\\&\\|=<>\\(\\)\\$\\:]*?)\\}";

	/*List of basic blocks after parsing*/
	private List<BasicBlock> blocks = new ArrayList<BasicBlock>();
	/*Map for getting the order of functions and identify the upper boundary of function*/
	private Map<Long, BasicBlock> orderedProcMap=new TreeMap<Long, BasicBlock>();
	
	/*Read content of file located at filePath to a StringBuffer*/
	public void ReadToBuffer(StringBuffer sb, String filePath) throws IOException {
		BufferedReader br = new BufferedReader(
						new InputStreamReader(
						new FileInputStream(filePath)));
		String line;
		while((line=br.readLine())!=null){
			sb.append(line).append("\n");
		}
		br.close();
	}
	
	/* Use the content from the CFG raw data to build the list of basic blocks,
	 * then to construct the orderedProcMap, and to build the four lists of basic blocks
	 * (i.e. succs, inter_succs, preds, inter_preds) */
	public void genBlockList(String s) {
		Pattern p = Pattern.compile(regexHeadToTrim, Pattern.MULTILINE);
		Matcher m = p.matcher(s);
		StringBuffer sb=new StringBuffer();
		while(m.find()) {
			m.appendReplacement(sb, "");
		}
		m.appendTail(sb);
		
		String s2=sb.toString().trim();
	
		p=Pattern.compile(regexBlock);
		m=p.matcher(s2);
		sb=new StringBuffer();
		while(m.find()) {
			//System.out.println("match block...");
			BasicBlock bb=new BasicBlock();
			bb.parseBlock(m.group());
			blocks.add(bb);
			
			m.appendReplacement(sb, "");
			m.appendTail(sb);
			
			Pattern p2=Pattern.compile(regexRTLs);
			Matcher m2=p2.matcher(sb.toString().trim());
			sb=new StringBuffer();
			int instNumInBlock=bb.getInstructionNumber();
			//System.out.println("#"+instNumInBlock+"#");
			int idx=0;
			while(m2.find() && idx<instNumInBlock) {
				//System.out.println("match RTL instructions...");
				bb.parseRTLs(bb.getInstruction(idx), m2.group());
				m2.appendReplacement(sb, "");
				idx++;
			}
			m2.appendTail(sb);
			//System.out.println("##"+sb.toString().trim()+"##");
			m=p.matcher(sb.toString().trim());
			sb=new StringBuffer();
		}
		/*just stop to parse, the rest timing info is useless */
		
		/*At this point, the blocks in the block list have not been organized through preds, succs, inter_preds, inter_succs*/
		Iterator<BasicBlock> it=blocks.iterator();
		while(it.hasNext()) {
			BasicBlock bb=it.next();
			
			/*0.Construct the orderedProcMap, to help decide the boundary of each procedure*/
			if(!bb.getProcName().isEmpty()) {
				orderedProcMap.put(bb.getInitialAddr(),bb);
			}
			
			/*1. organize the succs*/
			List<Long> l=bb.getSuccsAddrList();
			Iterator<Long> it2=l.iterator();
			while(it2.hasNext()) {
				bb.addSucc( this.getBasicBlockByAddr(it2.next()));
			}
			
			/*2. organize the inter_succs*/
			l=bb.getInterSuccsAddrList();
			it2=l.iterator();
			while(it2.hasNext()) {
				bb.addInterSucc( this.getBasicBlockByAddr(it2.next()));
			}
			
			/*3. organize the preds*/
			l=bb.getPredsAddrList();
			it2=l.iterator();
			while(it2.hasNext()) {
				bb.addPred( this.getBasicBlockByAddr(it2.next()));
			}
			
			/*4. organize the inter_preds*/
			l=bb.getInterPredsAddrList();
			it2=l.iterator();
			while(it2.hasNext()) {
				bb.addInterPred( this.getBasicBlockByAddr(it2.next()));
			}
		}
	}
	
	/* Print the basic blocks for a specific procedure,
	 * First, find these basic blocks. Then, sort these basic blocks with a Map.
	 * Finally print in order these basic blocks */
	public void PrintBasicBlocks(String procName) {
		Set<BasicBlock> bblist=this.getBasicBlocksForProcedure(procName);
		
		Map<Long, BasicBlock> orderMap=new TreeMap<Long, BasicBlock>();
		Iterator<BasicBlock> it=bblist.iterator();
		while(it.hasNext()) {
			BasicBlock bb=it.next();
			orderMap.put(bb.getInitialAddr(),bb);
		}
		System.out.println("--- " + procName + " ---");
		it=orderMap.values().iterator();
		while(it.hasNext()) {
			it.next().printInstructions();
		}
	}
	
	public List<BasicBlock> getBasicBlocks(){
		return this.blocks;
	}
	
	/* After the list of basic blocks is build, use the name of procedure
	 * to query and get a subset of basic blocks*/
	private Set<BasicBlock> getBasicBlocksForProcedure(String proc_name){
		Set<BasicBlock> resultlist=new HashSet<BasicBlock>();
		
		long upperbound=this.getUpperBoundOfProcedure(proc_name);
		long lowerbound=this.getLowerBoundOfProcedure(proc_name);
		/*find the basic block with proc_name as its bb_symbol_label*/
		BasicBlock initbb=null;
		Iterator<BasicBlock> it=blocks.iterator();
		boolean found=false;
		while(it.hasNext()) {
			initbb=it.next();
			if(initbb.getProcName().equals(proc_name)) {
				found=true;
				break;
			}
		}
		if(found && initbb!=null) {
			List<BasicBlock> worklist=new ArrayList<BasicBlock>();
			worklist.add(initbb);
			/*begin the worklist algorithm*/
			it=worklist.iterator();
			while(it.hasNext()) {
				BasicBlock bb=it.next();
				resultlist.add(bb);
				it.remove();
				
				//System.out.println(bb.getProcName()+";"+bb.getInitialAddr());///////////////
				Opcode instType=bb.getLastInstructionType();
				switch(instType) {
				case CALL:
					//System.out.println("the last instruction is CALL");
					List<Long> inter_succs=bb.getInterSuccsAddrList();
					if(!inter_succs.isEmpty()) {
						BasicBlock nextbb=this.getBasicBlockByAddr(inter_succs.get(0)); //currently, we can only find one inter_succ for each call.
						if(nextbb!=null && !resultlist.contains(nextbb)) {
							worklist.add(nextbb);
							it=worklist.iterator();
						}
					}
					break;
				case JNBE:
				case JNE:
				case JE:
				case JNLE:
				case JNB:
				case JNL:
				case JL:
				case JLE:
				case JB: 
				case JBE:
				case JNS:
				case JS:
				case JMP:
					List<Long> succs=bb.getSuccsAddrList();
					Iterator<Long> it2=succs.iterator();
					while(it2.hasNext()) {
						Long ad=it2.next();
						//Test if the jump target is in the range of current procedure
						if(ad<=upperbound && ad >=lowerbound && lowerbound!=-1 & upperbound!=-1) {
							BasicBlock nextbb=this.getBasicBlockByAddr(ad);
							if(nextbb!=null && !resultlist.contains(nextbb)) {
								worklist.add(nextbb);
							}
						} else {
							continue;
						}
					}
					it=worklist.iterator();
					break;
				case RET:
					//not definitly be the end of procedure. how to proceed?
					break;
				default: //other instructions, if can find one succ, go ahead
					succs=bb.getSuccsAddrList();
					if(!succs.isEmpty()) {
						Long ad=succs.get(0);
						BasicBlock nextbb=this.getBasicBlockByAddr(ad);
						if(nextbb!=null && !resultlist.contains(nextbb)) {
							worklist.add(nextbb);
							it=worklist.iterator();
						}
					}
					break;
				}
			}
		}
		return resultlist;
	}
	
	/*Find the basic block starting with the address 'addr', return null if not found*/
	private BasicBlock getBasicBlockByAddr(long addr) {
		Iterator<BasicBlock> it=blocks.iterator();
		while(it.hasNext()) {
			BasicBlock bb=it.next();
			if(bb.getInitialAddr()==addr)
				return bb; //Each basic block has a unique initial address, we do not have to iterate more.
		}
		return null; //addr is not the initial address of any basic block
	}
	
	/*Use the orderedProcMap to find the upper boundary of procedure, 
	 *the last procedure will have an upper boundary Long.MAX_VALUE*/
	private long getUpperBoundOfProcedure(String procName) {
		Iterator<Map.Entry<Long,BasicBlock>> it=orderedProcMap.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry<Long,BasicBlock> me=it.next();
			if(me.getValue().getProcName().equals(procName)) {
				if(it.hasNext()) {
					return it.next().getKey()-1;
				} else { // current procedure is the last one
					return Long.MAX_VALUE;
				}
			}
		}
		return -1; //not found the bb w.r.t procName
	}
	
	/*Get the lower boundary of procedure, 
	 * i.e. the address of first instruction of the procedure*/
	private long getLowerBoundOfProcedure(String procName) {
		Iterator<BasicBlock> it=blocks.iterator();
		while(it.hasNext()) {
			BasicBlock bb=it.next();
			if(bb.getProcName().equals(procName))
				return bb.getInitialAddr();
		}
		return -1;
	}
}