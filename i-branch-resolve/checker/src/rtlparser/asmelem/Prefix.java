package rtlparser.asmelem;

public enum Prefix{
	REP, REPZ, REPNZ, LOCK, OP_OVERRIDE, NONE
};