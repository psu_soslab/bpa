package rtlparser.asmelem;

public enum Opcode{
	//Three operands:
	SHLD, SHRD,
	//Two operands:
	MOV, LEA, MOVZX, MOVSX, MOVS,
	CMOVE, CMOVB, CMOVBE, CMOVL, CMOVLE, CMOVNB, CMOVNBE, CMOVNE, CMOVNL, CMOVNLE, CMOVS, CMOVNS,
	SUB, ADD, ADC, SBB, XADD,
	CMP, CMPS, 
	TEST, AND, OR, XOR, 
	SAR, SHR, SAL, SHL,
	CMPXCHG, XCHG,
	STOS, SCAS,
	RCL,RCR,ROL,ROR,
	BT, BSR,
	//One operand
	PUSH, POP,
	JMP, 
	JE, JNE, JNB, JNBE, JNL, JNLE, JL, JLE, JB, JBE, JNS, JS,
	SETNBE,SETB, SETE, SETNE, SETBE, SETLE, SETNL, SETNLE,
	MUL, DIV, IDIV,
	INC, DEC,
	NOT, NEG,
	CALL, 
	//No operand
	HLT, LEAVE, CDQ, CPUID, 
	//Various number of operands
	IMUL, RET, 
	NOP	// We treat NOP as no operand till now.
	/*should refer to Coq formalization of x86-32 (X86Syntax.v) for more instructions*/
	//Note: fchs, fsub, fstp, fadd, fldcw, fistp, fnstcw, fld, fild are currently omitted as NOP. 
}