package rtlparser;

import java.util.*;
import java.util.regex.*;
	
/**
 * 
 * @author suncong
 * Format of assembly address. 
 * AddrType1 stands for absolute/relative address 
 * AddrType2 stands for different form of "base + index*scale + disp"
 * 
 */
public class Address{
	enum AddrType1{ABS, REL};
	
	enum AddrType2{	//base and index are regs, scale and disp are numbers
		DIRECT, 	//[disp], e.g. [0x12345678]
		REG, 		//[base], e.g. [eax]
		BASE, 		//[base+disp], e.g. [eax+0x10]
		INDEXED, 	//[index*scale+disp]
		BINDEXED, 	//[base+index+disp]
		BSINDEXED,	//[base+index*scale+disp], e.g. [ebx+edi*4-0xf8]
		BSINDEXED2, //[base+index*scale], e.g. [ebx+edi*4]
		INVALIDTYPE2
	};
	
	private static Set<String> regName32=new HashSet<String>(
			Arrays.asList(new String[] {"eax", "ebx", "ecx", "edx", "esp", "ebp", "edi", "esi"}));
	private static Set<String> regName16=new HashSet<String>(
			Arrays.asList(new String[] {"ax", "bx", "cx", "dx", "sp", "bp", "di", "si"}));
	private static Set<String> regName8=new HashSet<String>(
			Arrays.asList(new String[] {"ah", "bh", "ch", "dh", "al", "bl", "cl", "dl"}));
	
	private static String basePatternStr ="(\\w+)\\s*([\\+\\-])\\s*0x([0-9a-fA-F]+)";
	private static String indexedPatternStr = "(\\w+)\\s*\\*\\s*([1-9])\\s*([\\+\\-])\\s*0x([0-9a-fA-F]+)";
	private static String baseIndexedPatternStr = "(\\w+?)\\+?(\\w+?)([\\+\\-]?)0x([0-9a-fA-F]+?)";
	private static String baseIndexedScalePatternStr = "(\\w+?)\\+?(\\w+?)\\*?([0-9])([\\+\\-])?0x([0-9a-fA-F]+?)";
	private static String baseIndexedScalePattern2Str = "(\\w+?)\\+?(\\w+?)\\*?([0-9])";
	
	long disp=-1;
	String base="";
	String index="";
	int scale=-1;
	
	AddrType1 type1;
	AddrType2 type2=AddrType2.INVALIDTYPE2;
	
	public Address(String addr_str,AddrType1 absOrRel) {
		type1=absOrRel;
		
		if(addr_str.matches("0x[0-9a-fA-F]+")) { //direct address
			type2=AddrType2.DIRECT;
			disp=Long.parseLong(addr_str.substring(2), 16);
		} else if (regName32.contains(addr_str) || regName16.contains(addr_str) || regName8.contains(addr_str)) { // REG type
			type2=AddrType2.REG;
			base=addr_str;
		} else {
			Pattern p=Pattern.compile(basePatternStr);	// resolve BASE type
			Matcher m=p.matcher(addr_str);
			if(m.matches()){
				type2=AddrType2.BASE;
				base=m.group(1);
				disp=Long.parseLong(m.group(3), 16);
				if(m.group(2).equals("-"))
					disp=-disp;
			} else {
				p=Pattern.compile(indexedPatternStr);	// resolve INDEXED type
				m=p.matcher(addr_str);
				if(m.matches()) {
					type2=AddrType2.INDEXED;
					index=m.group(1);
					scale=Integer.parseInt(m.group(2));
					disp=Long.parseLong(m.group(4), 16);
					if(m.group(3).equals("-"))
						disp=-disp;
				} else {
					p=Pattern.compile(baseIndexedPatternStr); // resolve BINDEXED type
					m=p.matcher(addr_str);
					if(m.matches()) {
						type2=AddrType2.BINDEXED;
						base=m.group(1);
						index=m.group(2);
						disp=Long.parseLong(m.group(4), 16);
						if(m.group(3).equals("-"))
							disp=-disp;
					} else {
						p=Pattern.compile(baseIndexedScalePatternStr); // resolve BSINDEXED type
						m=p.matcher(addr_str);
						if(m.matches()) {
							type2=AddrType2.BSINDEXED;
							base=m.group(1);
							index=m.group(2);
							scale=Integer.parseInt(m.group(3));
							disp=Long.parseLong(m.group(5),16);
							if(m.group(4).equals("-"))
								disp=-disp;
						} else {
							p=Pattern.compile(baseIndexedScalePattern2Str); // resolve BSINDEXED type
							m=p.matcher(addr_str);
							if(m.matches()) {
								type2=AddrType2.BSINDEXED2;
								base=m.group(1);
								index=m.group(2);
								scale=Integer.parseInt(m.group(3));
								disp=0;
							}
						}
					}
				}
			}
		}
	}
	
	public void Print() {
		if(this.type1==AddrType1.REL) {
			System.out.print("PC-REL ");
		}
		switch(this.type2) {
		case DIRECT:
			if(disp>=0)
				System.out.printf("%x<%d>", disp, disp);
			else
				System.out.printf("-%x<-%d>", -disp, -disp);
			break;
		case REG:
			System.out.print(base);
			break;
		case BASE:
			System.out.print(base + "+(");
			if(disp>=0)
				System.out.printf("%x<%d>", disp, disp);
			else
				System.out.printf("-%x<-%d>", -disp, -disp);
			System.out.print(")");
			break;
		case INDEXED:
			System.out.print(index+"*"+scale+"+(");
			if(disp>=0)
				System.out.printf("%x<%d>", disp, disp);
			else
				System.out.printf("-%x<-%d>", -disp, -disp);
			System.out.print(")");
			break;
		case BINDEXED:
			System.out.print(base+"+"+index+"+(");
			if(disp>=0)
				System.out.printf("%x<%d>", disp, disp);
			else
				System.out.printf("-%x<-%d>", -disp, -disp);
			System.out.print(")");
			break;
		case BSINDEXED:
			System.out.print(base+"+"+index+"*"+scale+"+(");
			if(disp>=0)
				System.out.printf("%x<%d>", disp, disp);
			else
				System.out.printf("-%x<-%d>", -disp, -disp);
			System.out.print(")");
			break;
		case BSINDEXED2:
			System.out.print(base+"+"+index+"*"+scale+"+(");
			System.out.printf("%x<%d>", disp, disp); //disp should be 0.
			System.out.print(")");
			break;
		default:
			break;
		}
	}
}