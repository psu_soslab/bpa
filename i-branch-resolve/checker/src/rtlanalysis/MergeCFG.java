package rtlanalysis;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class MergeCFG{
	private static String edgeLinePattern="(\\d+)\\s+(\\d+)";
	private static String ibranchLinePattern="(\\d+)\\s\\d+\\s(\\d+)\\s\\w+";
	
	public static void main(String[] args) {
		if(args==null || args.length!=3) {
			System.out.println("Usage:");
			System.out.println("java -jar MergeCFG.jar [path of target edge file] "+
					"[path of source ijump file] [path of source icall file]");
			return;
		}
		
		try {
			MergeCFG merger=new MergeCFG();
			
			Set<Pair> edge_set = new HashSet<Pair>();
			
			//Read the content of edge.facts
			String line_content;
			BufferedReader fread=new BufferedReader(new InputStreamReader(new FileInputStream(args[0])));
			while((line_content=fread.readLine())!=null) {
				Pattern p=Pattern.compile(MergeCFG.edgeLinePattern);
				Matcher m=p.matcher(line_content);
				if(m.matches()) {
					Pair e = merger.new Pair(Integer.parseInt(m.group(1)), 
									  Integer.parseInt(m.group(2)));
					
					if(!edge_set.contains(e)) {
						edge_set.add(e);
					}
				}
			}
			fread.close();
			
			
			boolean new_elem_added=false;
			//Read from the ijump file
			fread=new BufferedReader(new InputStreamReader(new FileInputStream(args[1])));
			
			while((line_content=fread.readLine())!=null) {
				Pattern p=Pattern.compile(MergeCFG.ibranchLinePattern);
				Matcher m=p.matcher(line_content);
				if(m.matches()) {
					Pair e = merger.new Pair(Integer.parseInt(m.group(1)), 
									  Integer.parseInt(m.group(2)));
					
					if (edge_set.add(e) && !new_elem_added)
						new_elem_added = true;
				}
			}
			fread.close();
			
			//Read from the icall file
			fread=new BufferedReader(new InputStreamReader(new FileInputStream(args[2])));
			
			while((line_content=fread.readLine())!=null) {
				Pattern p=Pattern.compile(MergeCFG.ibranchLinePattern);
				Matcher m=p.matcher(line_content);
				if(m.matches()) {
					Pair e = merger.new Pair(Integer.parseInt(m.group(1)), 
									  Integer.parseInt(m.group(2)));
					
					if (edge_set.add(e) && !new_elem_added)
						new_elem_added = true;
				}
			}
			fread.close();
			
			if(new_elem_added) {
				//Rename the old edge.facts
				File file = new File(args[0]);
				File file2 = new File(args[0].concat(".tmp"));
				boolean success = file.renameTo(file2);
				if (!success) {
				   System.out.println("edge.facts renaming failed...");
				}
				
				//Generate new edge.facts
				PrintWriter fwrite=new PrintWriter(
									new FileWriter(args[0]));
				for(Pair p: edge_set) {
					fwrite.printf("%d\t%d\n",p.from,p.to);
				}
				fwrite.close();
				
				System.out.println("EdgeUpdated");
			} else
				System.out.println("EdgeStable");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//Pair stands for both edge and flow
	private class Pair{
		private int from;
		private int to;
		
		public Pair(int frm, int to) {
			this.from = frm;
			this.to= to;
		}
		
		public int getFrom() {
			return this.from;
		}
		
		public int getTo() {
			return this.to;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + from;
			result = prime * result + to;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Pair other = (Pair) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (from != other.from)
				return false;
			if (to != other.to)
				return false;
			return true;
		}

		private MergeCFG getOuterType() {
			return MergeCFG.this;
		}		
	}
}