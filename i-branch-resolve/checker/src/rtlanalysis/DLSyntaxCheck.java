package rtlanalysis;

import java.io.*;
import java.util.*;

import rtlparser.*;
import rtlparser.ast.*;

public class DLSyntaxCheck{
	private static final String arith_exp_str="arith_rtl_exp";
	private static final String test_exp_str="test_rtl_exp";
	private static final String if_exp_str="if_rtl_exp";
	private static final String casts_exp_str="cast_s_rtl_exp";
	private static final String castu_exp_str="cast_u_rtl_exp";
	private static final String imm_exp_str="imm_rtl_exp";
	private static final String get_loc_exp_str="get_loc_rtl_exp";
	private static final String get_byte_exp_str="get_byte_rtl_exp";
	private static final String get_ps_reg_exp_str="get_ps_reg_rtl_exp";
	private static final String get_mem_exp_str="get_mem_rtl_exp";
	private static final String get_random_exp_str="get_random_rtl_exp";

	private static final String set_loc_str="set_loc_rtl";
	private static final String set_ps_reg_str="set_ps_reg_rtl";
	private static final String set_mem_str="set_mem_rtl";
	private static final String set_byte_str="set_byte_rtl";
	private static final String if_str="if_rtl";
	private static final String advance_oracle_str="advance_oracle_rtl";
	private static final String error_str="error_rtl";
	private static final String trap_str="trap_rtl";
	
	//.decl arith_rtl_exp(id:Rtl_exp, size:number, bvec:Bit_vector_op, e1:Rtl_exp, e2:Rtl_exp)
	//.decl test_rtl_exp(id:Rtl_exp, size:number, top:Test_op, e1:Rtl_exp, e2:Rtl_exp)
	//.decl if_rtl_exp(id:Rtl_exp, size:number, cond:Rtl_exp, e1:Rtl_exp, e2:Rtl_exp)
	private class ExprType1{
		String id;
		int size;
		String op;
		String expr1;
		String expr2;
	}
	private Set<ExprType1> arith_rtl_expr=new HashSet<ExprType1>();
	private Set<ExprType1> test_rtl_expr=new HashSet<ExprType1>();
	private Set<ExprType1> if_rtl_expr=new HashSet<ExprType1>();
	
	//.decl cast_s_rtl_exp(id:Rtl_exp, size1:number, size2:number, e:Rtl_exp)
	//.decl cast_u_rtl_exp(id:Rtl_exp, size1:number, size2:number, e:Rtl_exp)
	private class ExprType2{
		String id;
		int size1;
		int size2;
		String expr;
	}
	private Set<ExprType2> cast_s_rtl_exp=new HashSet<ExprType2>();
	private Set<ExprType2> cast_u_rtl_exp=new HashSet<ExprType2>();
	
	//.decl imm_rtl_exp(id:Rtl_exp, size:number, i:number)
	private class ExprType3{
		String id;
		int size;
		int number;
	}
	private Set<ExprType3> imm_rtl_exp=new HashSet<ExprType3>();
	
	//.decl get_loc_rtl_exp(id:Rtl_exp, l:Loc) // removed size
	//.decl get_byte_rtl_exp(id:Rtl_exp, addr:Rtl_exp)
	private class ExprType4{
		String id;
		String expr;
	}
	private Set<ExprType4> get_loc_rtl_exp=new HashSet<ExprType4>();
	private Set<ExprType4> get_byte_rtl_exp=new HashSet<ExprType4>();
	
	//.decl get_ps_reg_rtl_exp(id:Rtl_exp, size:number, p_reg:Pseudo_reg)
	//.decl get_mem_rtl_exp(id:Rtl_exp, size:number, addr:Rtl_exp)
	private class ExprType5{
		String id;
		int size;
		String expr;
	}
	private Set<ExprType5> get_ps_reg_rtl_exp=new HashSet<ExprType5>();
	private Set<ExprType5> get_mem_rtl_exp=new HashSet<ExprType5>();
	
	//.decl get_random_rtl_exp(id:Rtl_exp, size:number)
	private class ExprType6{
		String id;
		int size;
	}
	private Set<ExprType6> get_random_rtl_exp=new HashSet<ExprType6>();
	
	//.decl set_loc_rtl(addr:number, order:number, size:number, e:Rtl_exp, l:Loc)
	//.decl set_ps_reg_rtl(addr:number, order:number, size:number, e:Rtl_exp, p_reg:Pseudo_reg)
	//.decl set_mem_rtl(addr:number, order:number, size:number, e:Rtl_exp, addr1:Rtl_exp)
	private class InstType1{
		int addr;
		int order;
		int size;
		String expr1;
		String expr2;
	}
	private Set<InstType1> set_loc_rtl=new HashSet<InstType1>();
	private Set<InstType1> set_ps_reg_rtl=new HashSet<InstType1>();
	private Set<InstType1> set_mem_rtl=new HashSet<InstType1>();
	
	//.decl set_byte_rtl(addr:number, order:number, e:Rtl_exp, addr1:Rtl_exp)
	//.decl if_rtl(addr:number, order:number, e:Rtl_exp, instr:Rtl_instr)
	private class InstType2{
		int addr;
		int order;
		String expr1;
		String expr2;
	}
	private Set<InstType2> set_byte_rtl=new HashSet<InstType2>();
	private Set<InstType2> if_rtl=new HashSet<InstType2>();
	
	//.decl advance_oracle_rtl(addr:number, order:number)
	//.decl error_rtl(addr:number, order:number)
	//.decl trap_rtl(addr:number, order:number)
	private class InstType3{
		int addr;
		int order;
	}
	private Set<InstType3> advance_oracle_rtl=new HashSet<InstType3>();
	private Set<InstType3> error_rtl=new HashSet<InstType3>();
	private Set<InstType3> trap_rtl=new HashSet<InstType3>();
	
	//todo: Array and Float expressions
	//.decl get_array_rtl_exp(id:Rtl_exp, array_size:number, arr:Array, e:Rtl_exp)
	//.decl farith_rtl_exp(id:Rtl_exp, f_arith_op:Float_arith_op, rounding:Rtl_exp, e1:Rtl_exp, e2:Rtl_exp)
	//.decl fcast_rtl_exp(id:Rtl_exp, rounding:Rtl_exp, e1:Rtl_exp)
	
	//todo: Array instruction
	//.decl set_array_rtl(addr:number, order:number, size:number, arr:Array, e1:Rtl_exp, e2:Rtl_exp)
	
	//todo: edge, flow, enternode, global_aloc, local_aloc
	
	public static void main(String[] args) {
		/* The parsing method from a .rbtbl file. Useless for syntax checker.
		if(args.length!=1) {
			System.out.println("Usage:\njava ParseTest [path of .rbtbl]");
		}
		CFGParser parser=new CFGParser();
		
		StringBuffer sb=new StringBuffer();
		try {
			parser.ReadToBuffer(sb, args[0]);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		parser.genBlockList(sb.toString());
		System.out.println("Parsing finished...");
		
		parser.PrintBasicBlocks("main");
		*/
		
		DLSyntaxCheck checker=new DLSyntaxCheck();
		if(args==null || args.length!=1 || (args.length==1 && args[0].equals("--help"))) {
			System.out.println("Usage:");
			System.out.println("java -jar SyntaxChecker.jar [path of fact files]");
			return;
		}
		checker.readFromFacts(args[0]);
		checker.printStatistics();
		
		//checker.comparativePrint(args[1]); this need facts and *.rbtbl generated from same setting
		checker.printRTLs();
	}
	
	private void printRTLs() {
		Map<KeyType, RTLNode> rtlInstMap=new TreeMap<KeyType, RTLNode>();
		
		//If-rtl instruction should be considered first for recursion
		Iterator<InstType2> it2=this.if_rtl.iterator();
		while(it2.hasNext()) {
			InstType2 itype2=it2.next();
			int addr=itype2.addr;
			int order=itype2.order;
			KeyType key=new KeyType(addr, order);
			RTLNode node=this.buildASTFromFacts(addr, order, RInstType.if_rtl);
			if(node!=null) {
				rtlInstMap.put(key, node);
			}
		}
		
		Iterator<InstType1> it=this.set_loc_rtl.iterator();
		while(it.hasNext()) {
			InstType1 itype1=it.next();
			int addr=itype1.addr;
			int order=itype1.order;
			KeyType key=new KeyType(addr, order);
			RTLNode node=this.buildASTFromFacts(addr, order, RInstType.assign_rtl);
			if(node!=null && rtlInstMap.get(key)==null) {
				//if the key has been occupied by If-rtl instruction, not add anymore
				rtlInstMap.put(key, node);
			}
		}
		it=this.set_ps_reg_rtl.iterator();
		while(it.hasNext()) {
			InstType1 itype1=it.next();
			int addr=itype1.addr;
			int order=itype1.order;
			KeyType key=new KeyType(addr, order);
			RTLNode node=this.buildASTFromFacts(addr, order, RInstType.assign_rtl);
			if(node!=null && rtlInstMap.get(key)==null) {
				rtlInstMap.put(key, node);
			}
		}
		it=this.set_mem_rtl.iterator();
		while(it.hasNext()) {
			InstType1 itype1=it.next();
			int addr=itype1.addr;
			int order=itype1.order;
			KeyType key=new KeyType(addr, order);
			RTLNode node=this.buildASTFromFacts(addr, order, RInstType.assign_rtl);
			if(node!=null && rtlInstMap.get(key)==null)
				rtlInstMap.put(key, node);
		}
		it2=this.set_byte_rtl.iterator();
		while(it2.hasNext()) {
			InstType2 itype2=it2.next();
			int addr=itype2.addr;
			int order=itype2.order;
			KeyType key=new KeyType(addr, order);
			RTLNode node=this.buildASTFromFacts(addr, order, RInstType.assign_rtl);
			if(node!=null && rtlInstMap.get(key)==null)
				rtlInstMap.put(key, node);
		}
		
		Iterator<InstType3> it3=this.advance_oracle_rtl.iterator();
		while(it3.hasNext()) {
			InstType3 itype3=it3.next();
			KeyType key=new KeyType(itype3.addr, itype3.order);
			RTLNode node=this.buildASTFromFacts(itype3.addr, itype3.order, RInstType.advance_oracle_rtl);
			if(node!=null && rtlInstMap.get(key)==null)
				rtlInstMap.put(key, node);
		}
		it3=this.error_rtl.iterator();
		while(it3.hasNext()) {
			InstType3 itype3=it3.next();
			KeyType key=new KeyType(itype3.addr, itype3.order);
			RTLNode node=this.buildASTFromFacts(itype3.addr, itype3.order, RInstType.error_rtl);
			if(node!=null && rtlInstMap.get(key)==null)
				rtlInstMap.put(key, node);
		}
		it3=this.trap_rtl.iterator();
		while(it3.hasNext()) {
			InstType3 itype3=it3.next();
			KeyType key=new KeyType(itype3.addr, itype3.order);
			RTLNode node=this.buildASTFromFacts(itype3.addr, itype3.order, RInstType.trap_rtl);
			if(node!=null && rtlInstMap.get(key)==null)
				rtlInstMap.put(key, node);
		}
		
		Set<Map.Entry<KeyType, RTLNode>> set=rtlInstMap.entrySet();
		Iterator<Map.Entry<KeyType, RTLNode>> iter=set.iterator();
		while(iter.hasNext()) {
			Map.Entry<KeyType, RTLNode> entry=iter.next();
			System.out.print("<"+ entry.getKey().addr+","+entry.getKey().order+">");
			entry.getValue().printInorder();
			System.out.println();
		}
	}
	
	/*Not used. Can bridge the rbtbl from Dongrui and facts from Kim*/
	private void comparativePrint(String rbtblPath) {
		CFGParser parser=new CFGParser();
		
		StringBuffer sb=new StringBuffer();
		try {
			parser.ReadToBuffer(sb, rbtblPath);
		} catch (IOException e) {
			e.getStackTrace();
		}
		
		parser.genBlockList(sb.toString());
		
		List<BasicBlock> blocks=parser.getBasicBlocks();
		Iterator<BasicBlock> it=blocks.iterator();
		while(it.hasNext()) {
			BasicBlock bb=it.next();
			List<Instruction> insts=bb.getInstructions();
			Iterator<Instruction> it2=insts.iterator();
			while(it2.hasNext()) {
				Instruction inst=it2.next();
				List<RTLInstruction> rtlinsts=inst.getRTLInstructions();
				Iterator<RTLInstruction> it3=rtlinsts.iterator();
				while(it3.hasNext()) {
					RTLInstruction rtlinst=it3.next();
					int addr=(int)rtlinst.getAddr();
					addr=addr-(int)Long.parseLong("8048000", 16);
					
					System.out.println("Addr:"+addr);
					
					int order=rtlinst.getOrder();
					RTLNode origin=rtlinst.getAST();
					origin.printInorder();
					
					/* important:
					 * RTLNode node=this.buildASTFromFacts(addr, order);
					System.out.print("DL:");
					node.printInorder();*/
				}
			}
		}
	}
	
	private RTLNode buildASTFromFacts(int addr, int order, RInstType instType) {
		switch(instType) {
		case if_rtl:
			List<InstType2> outlist=new ArrayList<InstType2>();
			this.getRTLInst_If(addr, order, outlist);
			if(!outlist.isEmpty()) {
				InstType2 itype2=outlist.get(0);
				RTLNode node=new RTLNode(addr, order, RType.INST);
				node.setRInstType(RInstType.if_rtl);
				node.setCntChildren(2);
				node.setChild(0, this.buildASTForExpr(addr, order, itype2.expr1));
				node.setChild(1, this.buildASTForSubInst(addr, order, itype2.expr2));
				return node;
			}
			break;
		case assign_rtl:
			List<InstType1> list1=new ArrayList<InstType1>();
			RInstType type=this.getRTLInst_InstType1_LocReg(addr, order, list1);
			if(!list1.isEmpty()) {
				InstType1 elem=list1.get(0);
				RTLNode node=new RTLNode(addr,order,RType.INST);
				node.setRInstType(type);
				node.setCntChildren(2);
				node.setChild(0, this.buildASTForExpr(addr, order, elem.expr2));
				node.setChild(1, this.buildASTForExpr(addr, order, elem.expr1));
				return node;
			}
			type=this.getRTLInst_InstType1_Mem(addr, order, list1);
			if(!list1.isEmpty()) {
				InstType1 elem=list1.get(0);
				
				RTLNode derefnode=new RTLNode(addr, order, RType.EXPR);
				derefnode.setRExprType(RExprType.DEREF);
				derefnode.setCntChildren(1);
				derefnode.setChild(0, this.buildASTForExpr(addr, order, elem.expr2));
							
				RTLNode node=new RTLNode(addr,order,RType.INST);
				node.setRInstType(type);
				node.setCntChildren(2);
				
				node.setChild(0, derefnode);
				node.setChild(1, this.buildASTForExpr(addr, order, elem.expr1));
				return node;
			}
			
			outlist=new ArrayList<InstType2>();
			type=this.getRTLInst_SetByte(addr, order, outlist);
			if(!outlist.isEmpty()) {
				InstType2 elem=outlist.get(0);
				RTLNode node=new RTLNode(addr,order,RType.INST);
				node.setRInstType(type);
				node.setCntChildren(2);
				node.setChild(0, this.buildASTForExpr(addr, order, elem.expr2));
				node.setChild(1, this.buildASTForExpr(addr, order, elem.expr1));
				return node;
			}
			break;
		case advance_oracle_rtl:
			RTLNode node=new RTLNode(addr,order,RType.INST);
			node.setRInstType(RInstType.advance_oracle_rtl);
			node.setCntChildren(0);
			return node;
		case error_rtl:
			node=new RTLNode(addr,order,RType.INST);
			node.setRInstType(RInstType.error_rtl);
			node.setCntChildren(0);
			return node;
		case trap_rtl:
			node=new RTLNode(addr,order,RType.INST);
			node.setRInstType(RInstType.trap_rtl);
			node.setCntChildren(0);
			return node;
		default:
			break;
		}
		return null;
	}
	
	private RTLNode buildASTForExpr(int addr, int order, String exprId) {
		List<ExprType1> lst=this.getEntryByExpr_ArithRTLExpr(exprId);
		if(!lst.isEmpty()) {
			ExprType1 et1=lst.get(0);
			RTLNode left=this.buildASTForExpr(addr, order, et1.expr1);
			RTLNode right=this.buildASTForExpr(addr, order, et1.expr2);
			RTLNode node=new RTLNode(et1.op, addr, order, left, right);
			return node;
		}
		lst=this.getEntryByExpr_TestRTLExpr(exprId);
		if(!lst.isEmpty()) {
			ExprType1 et1=lst.get(0);
			RTLNode left=this.buildASTForExpr(addr, order, et1.expr1);
			RTLNode right=this.buildASTForExpr(addr, order, et1.expr2);
			RTLNode node=new RTLNode(et1.op, addr, order, left, right);
			return node;
		}
		lst=this.getEntryByExpr_IfRTLExpr(exprId);
		if(!lst.isEmpty()) {
			ExprType1 et1=lst.get(0);
			RTLNode if_cond=this.buildASTForExpr(addr, order, et1.op);
			RTLNode left=this.buildASTForExpr(addr, order, et1.expr1);
			RTLNode right=this.buildASTForExpr(addr, order, et1.expr2);
			RTLNode node=new RTLNode(addr, order, if_cond, left, right);
			return node;
		}
		
		List<ExprType2> lst2=this.getEntryByExpr_CASTS(exprId);
		if(!lst2.isEmpty()) {
			ExprType2 et2=lst2.get(0);
			RTLNode node=new RTLNode(addr, order, RType.EXPR);
			node.setRExprType(RExprType.CASTS);
			node.setValue(new Long(et2.size1));
			node.setCastField2(et2.size2);
			node.setCntChildren(1);
			RTLNode child=this.buildASTForExpr(addr, order, et2.expr);
			node.setChild(0, child);
			return node;
		}
		lst2=this.getEntryByExpr_CASTU(exprId);
		if(!lst2.isEmpty()) {
			ExprType2 et2=lst2.get(0);
			RTLNode node=new RTLNode(addr, order, RType.EXPR);
			node.setRExprType(RExprType.CASTU);
			node.setValue(new Long(et2.size1));
			node.setCastField2(et2.size2);
			node.setCntChildren(1);
			RTLNode child=this.buildASTForExpr(addr, order, et2.expr);
			node.setChild(0, child);
			return node;
		}
		
		List<ExprType3> lst3=this.getEntryByExpr_ImmRTLExpr(exprId);
		if(!lst3.isEmpty()) {
			ExprType3 et3=lst3.get(0);
			RTLNode node=new RTLNode(addr, order, RType.EXPR);
			node.setRExprType(RExprType.CONST);
			node.setCntChildren(0);
			node.setValue(new Long(et3.number));
			return node;
		}
		
		List<ExprType4> lst4=this.getEntryByExpr_GetLocRTLExpr(exprId);
		if(!lst4.isEmpty()) {
			ExprType4 et4=lst4.get(0);
			RTLNode node=this.buildASTForExpr(addr, order, et4.expr);
			return node;
		}
		lst4=getEntryByExpr_GetByteRTLExpr(exprId);
		if(!lst4.isEmpty()) {
			ExprType4 et4=lst4.get(0);
			//Added a deref for reading from byte mem
			RTLNode node=new RTLNode(addr, order, RType.EXPR);
			node.setRExprType(RExprType.DEREF);
			node.setCntChildren(1);
			node.setChild(0, this.buildASTForExpr(addr, order, et4.expr));
			return node;
		}
		
		List<ExprType5> lst5=this.getEntryByExpr_GetPsRegRTLExpr(exprId);
		if(!lst5.isEmpty()) {
			ExprType5 et5=lst5.get(0);
			RTLNode node=new RTLNode(addr, order, RType.EXPR);
			//RExprType incomplete
			node.setCntChildren(0);
			node.setValue(Long.parseLong(et5.expr)); //not sure on this form
			return node;
		}
		lst5=this.getEntryByExpr_GetMemRTLExpr(exprId);
		if(!lst5.isEmpty()) {
			ExprType5 et5=lst5.get(0);
			RTLNode node=new RTLNode(addr, order, RType.EXPR);
			node.setRExprType(RExprType.DEREF);
			node.setCntChildren(1);
			node.setChild(0, this.buildASTForExpr(addr, order, et5.expr));
			return node;
		}
		
		List<ExprType6> lst6=this.getEntryByExpr_GetRandomRTLExpr(exprId);
		if(!lst6.isEmpty()) {
			//ExprType6 et6=lst6.get(0);
			RTLNode node=new RTLNode(addr, order, RType.EXPR);
			node.setCntChildren(0);
			node.setRExprType(RExprType.RANDOM);
			return node;
		}
		
		return new RTLNode(exprId, addr, order);
	}
	
	private RTLNode buildASTForSubInst(int addr, int order, String instId) {
		//we suppose if-rtl cannot be nested by another if-rtl
		List<InstType1> list1=new ArrayList<InstType1>();
		RInstType type=this.getRTLInst_InstType1_LocReg(addr, order, list1);
		if(type==RInstType.assign_rtl && !list1.isEmpty()) {
			InstType1 elem=list1.get(0);
			RTLNode node=new RTLNode(addr,order,RType.INST);
			node.setRInstType(type);
			node.setCntChildren(2);
			node.setChild(0, this.buildASTForExpr(addr, order, elem.expr2));
			node.setChild(1, this.buildASTForExpr(addr, order, elem.expr1));
			return node;
		}
		type=this.getRTLInst_InstType1_Mem(addr, order, list1);
		if(type==RInstType.assign_rtl && !list1.isEmpty()) {
			InstType1 elem=list1.get(0);
			
			RTLNode derefnode=new RTLNode(addr, order, RType.EXPR);
			derefnode.setRExprType(RExprType.DEREF);
			derefnode.setCntChildren(1);
			derefnode.setChild(0, this.buildASTForExpr(addr, order, elem.expr2));
						
			RTLNode node=new RTLNode(addr,order,RType.INST);
			node.setRInstType(type);
			node.setCntChildren(2);
			
			node.setChild(0, derefnode);
			node.setChild(1, this.buildASTForExpr(addr, order, elem.expr1));
			return node;
		}
		
		List<InstType2> list2=new ArrayList<InstType2>();
		type=this.getRTLInst_SetByte(addr, order, list2);
		if(type==RInstType.assign_rtl && !list2.isEmpty()) {
			InstType2 elem=list2.get(0);
			RTLNode node=new RTLNode(addr,order,RType.INST);
			node.setRInstType(type);
			node.setCntChildren(2);
			node.setChild(0, this.buildASTForExpr(addr, order, elem.expr2));
			node.setChild(1, this.buildASTForExpr(addr, order, elem.expr1));
			return node;
		}
		//InstType3: advance_oracle, error, trap
		RTLNode node=new RTLNode(addr,order,RType.INST);
		type=this.getRTLInst_Abnormals(addr, order);
		node.setRInstType(type);
		node.setCntChildren(0);
		return node;
	}
	
	private void readFromFacts(String path) {
		String filePath;
		String line;
		StringTokenizer stok;
		File file;
		BufferedReader br;
		try {
			filePath=path +DLSyntaxCheck.arith_exp_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					ExprType1 et=new ExprType1();
					et.id= stok.nextToken();
					et.size=Integer.parseInt(stok.nextToken());
					et.op=stok.nextToken();
					et.expr1=stok.nextToken();
					et.expr2=stok.nextToken();
					this.arith_rtl_expr.add(et);
				}
				br.close();
			}

			filePath=path+DLSyntaxCheck.test_exp_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
			br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					ExprType1 et=new ExprType1();
					et.id= stok.nextToken();
					et.size=Integer.parseInt(stok.nextToken());
					et.op=stok.nextToken();
					et.expr1=stok.nextToken();
					et.expr2=stok.nextToken();
					this.test_rtl_expr.add(et);
				}
				br.close();
			}
			
			filePath=path+DLSyntaxCheck.if_exp_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					ExprType1 et=new ExprType1();
					et.id= stok.nextToken();
					et.size=Integer.parseInt(stok.nextToken());
					et.op=stok.nextToken();
					et.expr1=stok.nextToken();
					et.expr2=stok.nextToken();
					this.if_rtl_expr.add(et);
				}
				br.close();
			}
			
			filePath=path+DLSyntaxCheck.casts_exp_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					ExprType2 et=new ExprType2();
					et.id= stok.nextToken();
					et.size1=Integer.parseInt(stok.nextToken());
					et.size2=Integer.parseInt(stok.nextToken());
					et.expr=stok.nextToken();
					this.cast_s_rtl_exp.add(et);
				}
				br.close();
			}
			
			filePath=path+DLSyntaxCheck.castu_exp_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					ExprType2 et=new ExprType2();
					et.id= stok.nextToken();
					et.size1=Integer.parseInt(stok.nextToken());
					et.size2=Integer.parseInt(stok.nextToken());
					et.expr=stok.nextToken();
					this.cast_u_rtl_exp.add(et);
				}
				br.close();
			}
			
			filePath=path+DLSyntaxCheck.imm_exp_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					ExprType3 et=new ExprType3();
					et.id= stok.nextToken();
					et.size=Integer.parseInt(stok.nextToken());
					et.number=Integer.parseInt(stok.nextToken());
					this.imm_rtl_exp.add(et);
				}
				br.close();
			}
			
			filePath=path+DLSyntaxCheck.get_loc_exp_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					ExprType4 et=new ExprType4();
					et.id= stok.nextToken();
					et.expr=stok.nextToken();
					this.get_loc_rtl_exp.add(et);
				}
				br.close();
			}
			
			filePath=path+DLSyntaxCheck.get_byte_exp_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					ExprType4 et=new ExprType4();
					et.id= stok.nextToken();
					et.expr=stok.nextToken();
					this.get_byte_rtl_exp.add(et);
				}
				br.close();
			}

			filePath=path+DLSyntaxCheck.get_ps_reg_exp_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					ExprType5 et=new ExprType5();
					et.id= stok.nextToken();
					et.size=Integer.parseInt(stok.nextToken());
					et.expr=stok.nextToken();
					this.get_ps_reg_rtl_exp.add(et);
				}
				br.close();
			}
			
			filePath=path+DLSyntaxCheck.get_mem_exp_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					ExprType5 et=new ExprType5();
					et.id= stok.nextToken();
					et.size=Integer.parseInt(stok.nextToken());
					et.expr=stok.nextToken();
					this.get_mem_rtl_exp.add(et);
				}
				br.close();
			}
			
			filePath=path+DLSyntaxCheck.get_random_exp_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					ExprType6 et=new ExprType6();
					et.id= stok.nextToken();
					et.size=Integer.parseInt(stok.nextToken());
					this.get_random_rtl_exp.add(et);
				}
				br.close();
			}
			
			filePath=path+DLSyntaxCheck.set_loc_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					InstType1 et=new InstType1();
					et.addr= Integer.parseInt(stok.nextToken());
					et.order=Integer.parseInt(stok.nextToken());
					et.size=Integer.parseInt(stok.nextToken());
					et.expr1=stok.nextToken();
					et.expr2=stok.nextToken();
					this.set_loc_rtl.add(et);
				}
				br.close();
			}
			
			filePath=path+DLSyntaxCheck.set_ps_reg_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					InstType1 et=new InstType1();
					et.addr= Integer.parseInt(stok.nextToken());
					et.order=Integer.parseInt(stok.nextToken());
					et.size=Integer.parseInt(stok.nextToken());
					et.expr1=stok.nextToken();
					et.expr2=stok.nextToken();
					this.set_ps_reg_rtl.add(et);
				}
				br.close();
			}
			
			filePath=path+DLSyntaxCheck.set_mem_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					InstType1 et=new InstType1();
					et.addr= Integer.parseInt(stok.nextToken());
					et.order=Integer.parseInt(stok.nextToken());
					et.size=Integer.parseInt(stok.nextToken());
					et.expr1=stok.nextToken();
					et.expr2=stok.nextToken();
					this.set_mem_rtl.add(et);
				}
				br.close();
			}
			
			filePath=path+DLSyntaxCheck.set_byte_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					InstType2 et=new InstType2();
					et.addr= Integer.parseInt(stok.nextToken());
					et.order=Integer.parseInt(stok.nextToken());
					et.expr1=stok.nextToken();
					et.expr2=stok.nextToken();
					this.set_byte_rtl.add(et);
				}
				br.close();
			}
			
			filePath=path+DLSyntaxCheck.if_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					InstType2 et=new InstType2();
					et.addr= Integer.parseInt(stok.nextToken());
					et.order=Integer.parseInt(stok.nextToken());
					et.expr1=stok.nextToken();
					et.expr2=stok.nextToken();
					this.if_rtl.add(et);
				}
				br.close();
			}
			
			filePath=path+DLSyntaxCheck.advance_oracle_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					InstType3 et=new InstType3();
					et.addr= Integer.parseInt(stok.nextToken());
					et.order=Integer.parseInt(stok.nextToken());
					this.advance_oracle_rtl.add(et);
				}
				br.close();
			}
			
			filePath=path+DLSyntaxCheck.error_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					InstType3 et=new InstType3();
					et.addr= Integer.parseInt(stok.nextToken());
					et.order=Integer.parseInt(stok.nextToken());
					this.error_rtl.add(et);
				}
				br.close();
			}
			
			filePath=path+DLSyntaxCheck.trap_str+".facts";
			file=new File(filePath);
			if(file.exists()) {
				br=new BufferedReader(new FileReader(file));
				while((line=br.readLine())!=null) {
					stok=new StringTokenizer(line);
					InstType3 et=new InstType3();
					et.addr= Integer.parseInt(stok.nextToken());
					et.order=Integer.parseInt(stok.nextToken());
					this.trap_rtl.add(et);
				}
				br.close();
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	private void printStatistics() {
		System.out.println("----------- Statistics ------------");
		System.out.println(DLSyntaxCheck.arith_exp_str + ":" + this.arith_rtl_expr.size());
		System.out.println(DLSyntaxCheck.test_exp_str+":"+this.test_rtl_expr.size());
		System.out.println(DLSyntaxCheck.if_exp_str+":"+this.if_rtl_expr.size());
		System.out.println(DLSyntaxCheck.casts_exp_str+":"+this.cast_s_rtl_exp.size());
		System.out.println(DLSyntaxCheck.castu_exp_str+":"+this.cast_u_rtl_exp.size());
		System.out.println(DLSyntaxCheck.imm_exp_str+":"+this.imm_rtl_exp.size());
		System.out.println(DLSyntaxCheck.get_loc_exp_str+":"+this.get_loc_rtl_exp.size());
		System.out.println(DLSyntaxCheck.get_byte_exp_str+":"+this.get_byte_rtl_exp.size());
		System.out.println(DLSyntaxCheck.get_ps_reg_exp_str+":"+this.get_ps_reg_rtl_exp.size());
		System.out.println(DLSyntaxCheck.get_mem_exp_str+":"+this.get_mem_rtl_exp.size());
		System.out.println(DLSyntaxCheck.get_random_exp_str+":"+this.get_random_rtl_exp.size());
		
		System.out.println(DLSyntaxCheck.set_loc_str+":"+this.set_loc_rtl.size());
		System.out.println(DLSyntaxCheck.set_ps_reg_str+":"+this.set_ps_reg_rtl.size());
		System.out.println(DLSyntaxCheck.set_mem_str+":"+this.set_mem_rtl.size());
		System.out.println(DLSyntaxCheck.set_byte_str+":"+this.set_byte_rtl.size());
		System.out.println(DLSyntaxCheck.if_str+":"+this.if_rtl.size());
		System.out.println(DLSyntaxCheck.advance_oracle_str+":"+this.advance_oracle_rtl.size());
		System.out.println(DLSyntaxCheck.error_str+":"+this.error_rtl.size());
		System.out.println(DLSyntaxCheck.trap_str+":"+this.trap_rtl.size());
		System.out.println("-----------------------------------");		
	}
	

	
	private RInstType getRTLInst_InstType1_LocReg(int addr, int order, List<InstType1> outlist) {
		outlist.clear();
		getRTLInst_SetLoc(addr,order,outlist);
		getRTLInst_SetPsReg(addr,order,outlist);
		if(!outlist.isEmpty()) {
			return RInstType.assign_rtl;
		}
		return RInstType.nop_rtl;
	}
	
	private void getRTLInst_SetLoc(int addr, int order, List<InstType1> outlist){
		for(InstType1 ity: this.set_loc_rtl) {
			if(ity.addr== addr && ity.order==order)
				outlist.add(ity);
		}
	}
	
	private void getRTLInst_SetPsReg(int addr, int order, List<InstType1> outlist){
		for(InstType1 ity: this.set_ps_reg_rtl) {
			if(ity.addr== addr && ity.order==order)
				outlist.add(ity);
		}
	}
	
	private RInstType getRTLInst_InstType1_Mem(int addr, int order, List<InstType1> outlist) {
		outlist.clear();
		//getRTLInst_SetMem(addr,order,outlist);
		for(InstType1 ity: this.set_mem_rtl) {
			if(ity.addr== addr && ity.order==order)
				outlist.add(ity);
		}
		if(!outlist.isEmpty()) {
			return RInstType.assign_rtl;
		}
		return RInstType.nop_rtl;
	}
	
	private RInstType getRTLInst_SetByte(int addr, int order, List<InstType2> outlist){
		outlist.clear();
		for(InstType2 ity: this.set_byte_rtl) {
			if(ity.addr== addr && ity.order==order)
				outlist.add(ity);
		}
		if(!outlist.isEmpty()) {
			return RInstType.assign_rtl;
		}
		return RInstType.nop_rtl;
	}
	
	private RInstType getRTLInst_If(int addr, int order, List<InstType2> outlist){
		outlist.clear();
		for(InstType2 ity: this.if_rtl) {
			if(ity.addr== addr && ity.order==order)
				outlist.add(ity);
		}
		if(!outlist.isEmpty()) {
			return RInstType.if_rtl;
		}
		return RInstType.nop_rtl;
	}

	private RInstType getRTLInst_Abnormals(int addr, int order){
		for(InstType3 ity: this.error_rtl) {
			if(ity.addr== addr && ity.order==order)
				return RInstType.error_rtl;
		}
		for(InstType3 ity: this.trap_rtl) {
			if(ity.addr== addr && ity.order==order)
				return RInstType.trap_rtl;
		}
		for(InstType3 ity: this.advance_oracle_rtl) {
			if(ity.addr== addr && ity.order==order)
				return RInstType.advance_oracle_rtl;
		}
		return RInstType.nop_rtl;
	}
	
	private List<ExprType1> getEntryByExpr_ArithRTLExpr(String expr){
		List<ExprType1> res=new ArrayList<ExprType1>();
		for(ExprType1 et: this.arith_rtl_expr) {
			if(et.id.equals(expr)) {
				res.add(et);
			}
		}
		return res;
	}
	
	private List<ExprType1> getEntryByExpr_TestRTLExpr(String expr){
		List<ExprType1> res=new ArrayList<ExprType1>();
		for(ExprType1 et: this.test_rtl_expr) {
			if(et.id.equals(expr))
				res.add(et);
		}
		return res;
	}
	
	private List<ExprType1> getEntryByExpr_IfRTLExpr(String expr){
		List<ExprType1> res=new ArrayList<ExprType1>();
		for(ExprType1 et: this.if_rtl_expr) {
			if(et.id.equals(expr))
				res.add(et);
		}
		return res;
	}
	
	private List<ExprType2> getEntryByExpr_CASTU(String expr) {
		List<ExprType2> res=new ArrayList<ExprType2>();
		for(ExprType2 et:this.cast_u_rtl_exp) {
			if(et.id.equals(expr))
				res.add(et);
		}
		return res;
	}
	
	private List<ExprType2> getEntryByExpr_CASTS(String expr) {
		List<ExprType2> res=new ArrayList<ExprType2>();
		for(ExprType2 et:this.cast_s_rtl_exp) {
			if(et.id.equals(expr))
				res.add(et);
		}
		return res;
	}
	
	private List<ExprType3> getEntryByExpr_ImmRTLExpr(String id){
		List<ExprType3> res=new ArrayList<ExprType3>();
		for(ExprType3 et:this.imm_rtl_exp) {
			if(et.id.equals(id))
				res.add(et);
		}
		return res;
	}
	
	private List<ExprType4> getEntryByExpr_GetLocRTLExpr(String id){
		List<ExprType4> res=new ArrayList<ExprType4>();
		for(ExprType4 et:this.get_loc_rtl_exp) {
			if(et.id.equals(id))
				res.add(et);
		}
		return res;
	}
	private List<ExprType4> getEntryByExpr_GetByteRTLExpr(String id){
		List<ExprType4> res=new ArrayList<ExprType4>();
		for(ExprType4 et:this.get_byte_rtl_exp) {
			if(et.id.equals(id))
				res.add(et);
		}
		return res;
	}
	
	private List<ExprType5> getEntryByExpr_GetPsRegRTLExpr(String id){
		List<ExprType5> res=new ArrayList<ExprType5>();
		for(ExprType5 et:this.get_ps_reg_rtl_exp) {
			if(et.id.equals(id))
				res.add(et);
		}
		return res;
	}
	private List<ExprType5> getEntryByExpr_GetMemRTLExpr(String id){
		List<ExprType5> res=new ArrayList<ExprType5>();
		for(ExprType5 et:this.get_mem_rtl_exp) {
			if(et.id.equals(id))
				res.add(et);
		}
		return res;
	}
	
	private List<ExprType6> getEntryByExpr_GetRandomRTLExpr(String id){
		List<ExprType6> res=new ArrayList<ExprType6>();
		for(ExprType6 et:this.get_random_rtl_exp) {
			if(et.id.equals(id))
				res.add(et);
		}
		return res;
	}
	
	private class KeyType implements Comparable<KeyType>{
		int addr;
		int order;
		
		KeyType(int ad, int or){
			this.addr=ad;
			this.order=or;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + addr;
			result = prime * result + order;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			KeyType other = (KeyType) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (addr != other.addr)
				return false;
			if (order != other.order)
				return false;
			return true;
		}

		private DLSyntaxCheck getOuterType() {
			return DLSyntaxCheck.this;
		}
		
		@Override
		public int compareTo(KeyType kt) {
			if ((this.addr<kt.addr)
			|| (this.addr==kt.addr && this.order<kt.order)){
				return -1;
			} else if(this.addr==kt.addr && this.order==kt.order) {
				return 0;
			} else
				return 1;
		}
	}
}
