package rtlanalysis;

import java.io.*;


public class FlagPatternCapture{
	private static String[] reg_array = {"EAX", "EBX", "ECX", "EDX", "EBP", "ESP", "ESI", "EDI"};
	private static String cf_inst_head_regex = "<\\d+,\\d+>CF\\s<--\\s";
	private static String zf_inst_head_regex = "<\\d+,\\d+>ZF\\s<--\\s";
	private static String sf_inst_head_regex = "<\\d+,\\d+>SF\\s<--\\s";
	private static String of_inst_head_regex = "<\\d+,\\d+>OF\\s<--\\s";
	private static String const_regex = "\\$(\\-)?0x[0-9a-fA-F]+\\((\\-)?\\d+\\)";
	
	private static String[] cf_patterns = new String[] {
			//pattern 0:
			cf_inst_head_regex +"\\(\\(\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<u\\s[A-Z]+\\)\\s\\|\\s\\(\\([A-Z]+\\s\\+\\s"+ const_regex + "\\)\\s<u\\s" + const_regex + "\\)\\)",
			//pattern 1:
			cf_inst_head_regex +"\\(\\(\\([A-Z]+\\s\\+\\s[A-Z]+\\)\\s<u\\s[A-Z]+\\)\\s\\|\\s\\(\\([A-Z]+\\s\\+\\s[A-Z]+\\)\\s<u\\s[A-Z]+\\)\\)",
			//pattern 2:
			cf_inst_head_regex +"\\(\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s" + const_regex + "\\)\\s\\*\\s" + const_regex + "\\)\\s==\\s\\(\\*\\s\\([A-Z]+\\s\\+\\s" + const_regex + "\\)\\s\\*\\s" + const_regex + "\\)\\)\\s\\^\\s" + const_regex + "\\)",
			//pattern 3:
			cf_inst_head_regex +"\\(\\(\\([A-Z]+\\s\\+\\s\\*\\s\\([A-Z]+\\s\\+\\s" + const_regex + "\\)\\)\\s<u\\s[A-Z]+\\)\\s\\|\\s\\(\\([A-Z]+\\s\\+\\s\\*\\s\\([A-Z]+\\s\\+\\s" + const_regex + "\\)\\)\\s<u\\s\\*\\s\\([A-Z]+\\s\\+\\s" + const_regex + "\\)\\)\\)",
			//pattern 4:
			cf_inst_head_regex +"\\(\\(\\([A-Z]+\\s\\+\\s\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\)\\s\\+\\s" + const_regex + "\\)\\)\\s<u\\s[A-Z]+\\)\\s\\|\\s\\(\\([A-Z]+\\s\\+\\s\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\)\\s\\+\\s" + const_regex + "\\)\\)\\s<u\\s\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\)\\s\\+\\s" + const_regex + "\\)\\)\\)",
			//pattern 5:
			cf_inst_head_regex +"\\(\\(\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\s==\\s\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\)\\s\\^\\s" + const_regex + "\\)",
			//pattern 6:
			cf_inst_head_regex +"\\(\\(\\(\\*\\s[A-Z]+\\s\\+\\s[A-Z]+\\)\\s<u\\s\\*\\s[A-Z]+\\)\\s\\|\\s\\(\\(\\*\\s[A-Z]+\\s\\+\\s[A-Z]+\\)\\s<u\\s[A-Z]+\\)\\)",
			//pattern 7:
			cf_inst_head_regex +"\\(\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s" + const_regex + "\\)\\s\\+\\s[A-Z]+\\)\\s<u\\s\\*\\s\\([A-Z]+\\s\\+\\s" + const_regex + "\\)\\)\\s\\|\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s" + const_regex + "\\)\\s\\+\\s[A-Z]+\\)\\s<u\\s[A-Z]+\\)\\)",
			//pattern 8:
			cf_inst_head_regex +"\\(\\(\\([A-Z]+\\s\\*\\s\\*\\s\\([A-Z]+\\s\\+\\s" + const_regex + "\\)\\)\\s==\\s\\([A-Z]+\\s\\*\\s\\*\\s\\([A-Z]+\\s\\+\\s" + const_regex + "\\)\\)\\)\\s\\^\\s" + const_regex + "\\)",
			//pattern 9:
			cf_inst_head_regex +"\\(\\(\\(\\([A-Z]+\\s>>u\\s" + const_regex + "\\)\\s\\*\\s\\*\\s\\([A-Z]+\\s\\+\\s" + const_regex + "\\)\\)\\s==\\s\\(\\([A-Z]+\\s>>u\\s" + const_regex + "\\)\\s\\*\\s\\*\\s\\([A-Z]+\\s\\+\\s" + const_regex + "\\)\\)\\)\\s\\^\\s" + const_regex + "\\)",
			//pattern 10:
			cf_inst_head_regex +"\\(\\(\\([A-Z]+\\s\\*\\s[A-Z]+\\)\\s==\\s\\([A-Z]+\\s\\*\\s[A-Z]+\\)\\)\\s\\^\\s" + const_regex + "\\)",
			//pattern 11:
			cf_inst_head_regex +"\\(\\(\\([A-Z]+\\s\\+\\s\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\)\\)\\s<u\\s[A-Z]+\\)\\s\\|\\s\\(\\([A-Z]+\\s\\+\\s\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\)\\)\\s<u\\s\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\)\\)\\)",
			//pattern 12:
			cf_inst_head_regex +"\\(\\([A-Z]+\\s>>u\\s" + const_regex + "\\)\\s<u\\s[A-Z]+\\)",
			//pattern 13:
			cf_inst_head_regex +"\\(\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s" + const_regex + "\\)\\s\\+\\s" + const_regex + "\\)\\s<u\\s\\*\\s\\([A-Z]+\\s\\+\\s" + const_regex + "\\)\\)\\s\\|\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s" + const_regex + "\\)\\s\\+\\s" + const_regex + "\\)\\s<u\\s" + const_regex + "\\)\\)",
			//pattern 14:
			cf_inst_head_regex +"\\(\\(\\(\\*\\s[A-Z]+\\s\\+\\s" + const_regex + "\\)\\s<u\\s\\*\\s[A-Z]+\\)\\s\\|\\s\\(\\(\\*\\s[A-Z]+\\s\\+\\s" + const_regex + "\\)\\s<u\\s" + const_regex + "\\)\\)",
			//pattern 15:
			cf_inst_head_regex +"\\(\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\)\\s\\+\\s" + const_regex + "\\)\\s\\+\\s" + const_regex + "\\)\\s<u\\s\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\)\\s\\+\\s" + const_regex + "\\)\\)\\s\\|\\s\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\)\\s\\+\\s" + const_regex + "\\)\\s\\+\\s" + const_regex + "\\)\\s<u\\s" + const_regex + "\\)\\)",
			//pattern 16:
			cf_inst_head_regex +"\\(\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\)\\s\\+\\s" + const_regex + "\\)\\s<u\\s\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\)\\)\\s\\|\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\)\\s\\+\\s" + const_regex + "\\)\\s<u\\s" + const_regex + "\\)\\)",
			//pattern 17:
			cf_inst_head_regex +"\\(\\(\\([A-Z]+\\s\\+\\s\\*\\s[A-Z]+\\)\\s<u\\s[A-Z]+\\)\\s\\|\\s\\(\\([A-Z]+\\s\\+\\s\\*\\s[A-Z]+\\)\\s<u\\s\\*\\s[A-Z]+\\)\\)",
			//pattern 18:
			cf_inst_head_regex +"\\(\\(\\(\\([A-Z]+\\s>>u\\s" + const_regex + "\\)\\s\\*\\s\\([A-Z]+\\s>>u\\s" + const_regex + "\\)\\)\\s==\\s\\(\\([A-Z]+\\s>>u\\s" + const_regex + "\\)\\s\\*\\s\\([A-Z]+\\s>>u\\s" + const_regex + "\\)\\)\\)\\s\\^\\s" + const_regex + "\\)",
			//pattern 19:
			cf_inst_head_regex +"\\(\\(\\([A-Z]+\\s\\*\\s\\([A-Z]+\\s>>u\\s" + const_regex + "\\)\\)\\s==\\s\\([A-Z]+\\s\\*\\s\\([A-Z]+\\s>>u\\s" + const_regex + "\\)\\)\\)\\s\\^\\s" + const_regex + "\\)",
			//pattern 20:
			cf_inst_head_regex +"\\(\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\)\\s\\+\\s" + const_regex + "\\)\\s\\+\\s[A-Z]+\\)\\s<u\\s\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\)\\s\\+\\s" + const_regex + "\\)\\)\\s\\|\\s\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s" + const_regex + "\\)\\)\\s\\+\\s" + const_regex + "\\)\\s\\+\\s[A-Z]+\\)\\s<u\\s[A-Z]+\\)\\)",
			//pattern 21:
			cf_inst_head_regex +"\\(\\(\\(\\([A-Z]+\\s>>u\\s" + const_regex + "\\)\\s\\*\\s[A-Z]+\\)\\s==\\s\\(\\([A-Z]+\\s>>u\\s" + const_regex + "\\)\\s\\*\\s[A-Z]+\\)\\)\\s\\^\\s" + const_regex + "\\)",
			//pattern 22:
			cf_inst_head_regex +"\\([A-Z]+\\s<u\\s\\([A-Z]+\\s>>u\\s" + const_regex + "\\)\\)",
			//pattern 23:
			cf_inst_head_regex +"\\(\\([A-Z]+\\s<u\\s[A-Z]+\\)\\s\\|\\s\\([A-Z]+\\s<u\\s"+ const_regex +"\\)\\)"
	};
	
	private static String[] zf_patterns = new String[] {
			//pattern 0:
			zf_inst_head_regex +"\\(" + const_regex + "\\s==\\s\\([A-Z]+\\s[\\&\\^]\\s[A-Z]+\\)\\)",
			//pattern 1:
			zf_inst_head_regex +"\\(\\(\\*\\s[A-Z]+\\s\\+\\s[A-Z]+\\)\\s==\\s" + const_regex + "\\)",
			//pattern 2:
			zf_inst_head_regex + "\\(\\([A-Z]+\\s\\+\\s[A-Z]+\\)\\s==\\s" + const_regex + "\\)",
			//pattern 3:
			zf_inst_head_regex + "\\(\\(\\([A-Z]+\\s>>u\\s" + const_regex + "\\)\\s\\-\\s[A-Z]+\\)\\s==\\s" + const_regex +"\\)",
			//pattern 4:
			zf_inst_head_regex + "\\(\\([A-Z]+\\s\\-\\s\\([A-Z]+\\s>>u\\s" + const_regex + "\\)\\)\\s==\\s" + const_regex + "\\)"
	};
	
	private static String[] sf_patterns = new String[] {
			//pattern 0:
			sf_inst_head_regex + "\\(\\([A-Z]+\\s[\\&\\^\\+]\\s[A-Z]+\\)\\s<\\s" + const_regex + "\\)",
			//pattern 1:
			sf_inst_head_regex + "\\(\\(\\*\\s[A-Z]+\\s\\+\\s[A-Z]+\\)\\s<\\s" + const_regex + "\\)",
			//pattern 2:
			sf_inst_head_regex + "\\(\\([A-Z]+\\s\\-\\s\\([A-Z]+\\s>>u\\s" + const_regex + "\\)\\)\\s<\\s" + const_regex + "\\)",
			//pattern 3:
			sf_inst_head_regex + "\\(\\(\\([A-Z]+\\s>>u\\s" + const_regex + "\\)\\s\\-\\s[A-Z]+\\)\\s<\\s" + const_regex + "\\)"
	};
	
	private static String[] of_patterns = new String[] {
			//pattern 0:
			of_inst_head_regex + "\\(\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s[A-Z]+\\)",
			//pattern 1:
			of_inst_head_regex + "\\([A-Z]+\\s<\\s\\([A-Z]+\\s\\-\\s"+ const_regex +"\\)\\)",
			//pattern 2:
			of_inst_head_regex + "\\(\\(\\*\\s[A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s\\*\\s[A-Z]+\\)",
			//pattern 3:
			of_inst_head_regex + "\\(\\(\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\s==\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\^\\s"+ const_regex +"\\)",
			//pattern 4:
			of_inst_head_regex + "\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\)",
			//pattern 5:
			of_inst_head_regex + "\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s\\-\\s"+ const_regex +"\\)\\)",
			//pattern 6:
			of_inst_head_regex + "\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\)",
			//pattern 7:
			of_inst_head_regex + "\\(\\(\\(\\([A-Z]+\\s<\\s"+ const_regex + "\\)\\s\\^\\s"+ const_regex + "\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s\\-\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 8:
			of_inst_head_regex + "\\(\\("+ const_regex +"\\s\\^\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 9:
			of_inst_head_regex + "\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\)",
			//pattern 10:
			of_inst_head_regex + "\\(\\("+ const_regex +"\\s\\^\\s\\(\\(\\*\\s[A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\)\\)\\s\\&\\s\\(\\(\\*\\s[A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s[A-Z]+\\s\\+\\s[A-Z]+\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 11:
			of_inst_head_regex + "\\(\\(" + const_regex + "\\s\\^\\s\\(\\([A-Z]+\\s<\\s" + const_regex + "\\)\\s\\^\\s\\([A-Z]+\\s<\\s" + const_regex + "\\)\\)\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s" + const_regex + "\\)\\s\\^\\s\\(\\([A-Z]+\\s\\+\\s[A-Z]+\\)\\s<\\s" + const_regex + "\\)\\)\\)",
			//pattern 12:
			of_inst_head_regex + "\\(\\(\\(\\([A-Z]+\\s<\\s" + const_regex + "\\)\\s\\^\\s\\(\\([A-Z]+\\s<\\s" + const_regex + "\\)\\s\\^\\s" + const_regex + "\\)\\)\\s\\^\\s" + const_regex + "\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s" + const_regex + "\\)\\s\\^\\s\\(\\([A-Z]+\\s\\-\\s[A-Z]+\\)\\s<\\s" + const_regex + "\\)\\)\\)",
			//pattern 13:
			of_inst_head_regex + "\\(\\("+ const_regex +"\\s\\^\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\)\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s\\+\\s\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 14:
			of_inst_head_regex + "\\(\\(\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 15:
			of_inst_head_regex + "\\(\\(\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s\\-\\s\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 16:
			of_inst_head_regex + "\\(\\(\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s\\-\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 17:
			of_inst_head_regex + "\\(\\(\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s\\-\\s\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 18:
			of_inst_head_regex + "\\(\\("+ const_regex +"\\s\\^\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\)\\)\\s\\&\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s\\+\\s[A-Z]+\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 19:
			of_inst_head_regex + "\\(\\(\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s\\-\\s[A-Z]+\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 20:
			of_inst_head_regex + "\\(\\(\\(\\([A-Z]+\\s<\\s" + const_regex + "\\)\\s\\^\\s\\(\\(\\*\\s[A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s\\-\\s\\*\\s[A-Z]+\\)\\s<\\s"+ const_regex + "\\)\\)\\)",
			//pattern 21:
			of_inst_head_regex + "\\(\\(\\(\\(\\*\\s[A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\(\\*\\s[A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s[A-Z]+\\s\\-\\s[A-Z]+\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 22:
			of_inst_head_regex + "\\(\\("+ const_regex +"\\s\\^\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\*\\s[A-Z]+\\s<\\s"+ const_regex +"\\)\\)\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s\\+\\s\\*\\s[A-Z]+\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 23:
			of_inst_head_regex + "\\(\\("+ const_regex +"\\s\\^\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\)\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s\\+\\s\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 24:
			of_inst_head_regex + "\\(\\(\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 25:
			of_inst_head_regex + "\\(\\(\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s\\-\\s\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 26:
			of_inst_head_regex + "\\(\\(\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s\\-\\s[A-Z]+\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 27:
			of_inst_head_regex + "\\(\\(\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 28:
			of_inst_head_regex + "\\(\\(\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s\\-\\s\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 29:
			of_inst_head_regex + "\\(\\(\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\-\\s[A-Z]+\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 30:
			of_inst_head_regex + "\\(\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s\\*\\s"+ const_regex +"\\)\\s==\\s\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s\\*\\s"+ const_regex +"\\)\\)\\s\\^\\s"+ const_regex +"\\)",
			//pattern 31:
			of_inst_head_regex + "\\(\\(\\(\\(\\*\\s[A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\(\\*\\s[A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\*\\s[A-Z]+\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 32:
			of_inst_head_regex + "\\(\\(\\(\\(\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\(\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\s\\-\\s[A-Z]+\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 33:
			of_inst_head_regex + "\\(\\("+ const_regex +"\\s\\^\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\&\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 34:
			of_inst_head_regex + "\\(\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\s<\\s\\(\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\s\\-\\s"+ const_regex +"\\)\\)",
			//pattern 35:
			of_inst_head_regex + "\\(\\(\\(\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\s\\*\\s[A-Z]+\\)\\s==\\s\\(\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\s\\*\\s[A-Z]+\\)\\)\\s\\^\\s"+ const_regex +"\\)",
			//pattern 36:
			of_inst_head_regex + "\\(\\(\\(\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\s\\*\\s\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\)\\s==\\s\\(\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\s\\*\\s\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\)\\)\\s\\^\\s"+ const_regex +"\\)",
			//pattern 37:
			of_inst_head_regex + "\\(\\("+ const_regex +"\\s\\^\\s\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\)\\)\\s\\&\\s\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s\\+\\s[A-Z]+\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 38:
			of_inst_head_regex + "\\(\\(\\(\\(\\*\\s\\("+ const_regex +"\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\(\\*\\s\\("+ const_regex +"\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s\\("+ const_regex +"\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\-\\s[A-Z]+\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 39:
			of_inst_head_regex + "\\(\\(\\([A-Z]+\\s\\*\\s\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\)\\s==\\s\\([A-Z]+\\s\\*\\s\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\)\\)\\s\\^\\s"+ const_regex +"\\)",
			//pattern 40:
			of_inst_head_regex + "\\(\\*\\s[A-Z]+\\s<\\s\\(\\*\\s[A-Z]+\\s\\-\\s"+ const_regex +"\\)\\)",
			//pattern 41:
			of_inst_head_regex + "\\(\\(\\("+ const_regex +"\\s\\^\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\("+ const_regex +"\\s\\^\\s\\(\\("+ const_regex +"\\s\\-\\s[A-Z]+\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 42:
			of_inst_head_regex + "\\(\\(\\([A-Z]+\\s\\*\\s\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\)\\s==\\s\\([A-Z]+\\s\\*\\s\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\)\\)\\s\\^\\s"+ const_regex +"\\)",
			//pattern 43:
			of_inst_head_regex + "\\(\\(\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\-\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 44:
			of_inst_head_regex + "\\(\\(\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s"+ const_regex +"\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s\\-\\s\\*\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 45:
			of_inst_head_regex + "\\(\\(\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 46:
			of_inst_head_regex + "\\(\\("+ const_regex +"\\s\\^\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\)\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s\\+\\s\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 47:
			of_inst_head_regex + "\\(\\(\\([A-Z]+\\s\\*\\s[A-Z]+\\)\\s==\\s\\([A-Z]+\\s\\*\\s[A-Z]+\\)\\)\\s\\^\\s"+ const_regex +"\\)",
			//pattern 48:
			of_inst_head_regex + "\\(\\(\\(\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\s\\*\\s\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\)\\s==\\s\\(\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\s\\*\\s\\*\\s\\([A-Z]+\\s\\+\\s"+ const_regex +"\\)\\)\\)\\s\\^\\s"+ const_regex +"\\)",
			//pattern 49:
			of_inst_head_regex + "\\(\\(\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s\\([A-Z]+\\s>>u\\s"+ const_regex +"\\)\\)",
			//pattern 50:
			of_inst_head_regex + "\\(\\(\\(\\(\\*\\s[A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\(\\*\\s[A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s[A-Z]+\\s\\-\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 51:
			of_inst_head_regex + "\\(\\("+ const_regex +"\\s\\^\\s\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\&\\s\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s\\(\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 52:
			of_inst_head_regex + "\\(\\("+ const_regex +"\\s\\^\\s\\(\\(\\*\\s[A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\&\\s\\(\\(\\*\\s[A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s[A-Z]+\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 53:
			of_inst_head_regex + "\\(\\("+ const_regex +"\\s\\^\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\&\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s\\([A-Z]+\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s\\+\\s"+ const_regex +"\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 54:
			of_inst_head_regex + "\\(\\(\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\(\\*\\s\\("+ const_regex +"\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\^\\s"+ const_regex +"\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\(\\([A-Z]+\\s\\-\\s\\*\\s\\("+ const_regex +"\\s\\+\\s\\([A-Z]+\\s\\*\\s"+ const_regex +"\\)\\)\\)\\s<\\s"+ const_regex +"\\)\\)\\)",
			//pattern 55:
			of_inst_head_regex + "\\(\\("+ const_regex +"\\s\\^\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s"+ const_regex +"\\)\\)\\s\\&\\s\\(\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\s\\^\\s\\([A-Z]+\\s<\\s"+ const_regex +"\\)\\)\\)"
	};
	
	private static boolean matchTwice(String line) {
		for(String s:reg_array) {
			int pos = line.indexOf(s);
			if(pos != -1) {
				String substr=line.substring(pos+1);
				if( substr.indexOf(s) != -1)
					return true;
				else
					continue;
			} else
				continue;
		}
		return false;
	}
	
	public static void main(String[] args) {
		if(args==null || args.length!=1) {
			System.out.println("Usage:");
			System.out.println("java -jar FlagPatternCapture.jar [path of the RTL instruction text file]");
			return;
		}
		String rtl_text_dir = args[0];
		
		//System.out.println(rtl_text_dir);
		try {
			PrintStream out_zf[] = new PrintStream[zf_patterns.length];
			PrintStream out_cf[] = new PrintStream[cf_patterns.length];
			PrintStream out_sf[] = new PrintStream[sf_patterns.length];
			PrintStream out_of[] = new PrintStream[of_patterns.length];
			
			PrintStream out_cf_rest;
			PrintStream out_of_rest;
			PrintStream out_zf_rest;
			PrintStream out_sf_rest;
			
			int idx = rtl_text_dir.lastIndexOf("/");
			if(idx == -1) {
				for(int i=0; i<zf_patterns.length; i++)
					out_zf[i] = new PrintStream(new FileOutputStream("ZF-pattern"+i+".txt"));
				for(int i=0;i< cf_patterns.length; i++)
					out_cf[i] = new PrintStream(new FileOutputStream("CF-pattern"+i+".txt"));
				for(int i=0; i<sf_patterns.length; i++)
					out_sf[i] = new PrintStream(new FileOutputStream("SF-pattern"+i+".txt"));
				for(int i=0; i<of_patterns.length; i++)
					out_of[i] = new PrintStream(new FileOutputStream("OF-pattern"+i+".txt"));
				out_cf_rest = new PrintStream(new FileOutputStream("CF-rest.txt"));
				out_of_rest = new PrintStream(new FileOutputStream("OF-rest.txt"));
				out_zf_rest = new PrintStream(new FileOutputStream("ZF-rest.txt"));
				out_sf_rest = new PrintStream(new FileOutputStream("SF-rest.txt"));
			} else {
				String path = rtl_text_dir.substring(0, idx);
				for(int i=0; i< zf_patterns.length; i++)
					out_zf[i] = new PrintStream(new FileOutputStream(path + "/ZF-pattern" + i + ".txt"));
				for(int i = 0; i < cf_patterns.length; i++)
					out_cf[i] = new PrintStream(new FileOutputStream(path + "/CF-pattern" + i + ".txt"));
				for(int i = 0; i < sf_patterns.length; i++)
					out_sf[i] = new PrintStream(new FileOutputStream(path + "/SF-pattern" + i + ".txt"));
				for(int i = 0; i < of_patterns.length; i++)
					out_of[i] = new PrintStream(new FileOutputStream(path + "/OF-pattern" + i + ".txt"));
				out_cf_rest = new PrintStream(new FileOutputStream(path + "/CF-rest.txt"));
				out_of_rest = new PrintStream(new FileOutputStream(path + "/OF-rest.txt"));
				out_zf_rest = new PrintStream(new FileOutputStream(path + "/ZF-rest.txt"));
				out_sf_rest = new PrintStream(new FileOutputStream(path + "/SF-rest.txt"));
			}
			BufferedReader in = new BufferedReader(new InputStreamReader(
					new FileInputStream(rtl_text_dir)));
			
			//PrintStream out=new PrintStream(new FileOutputStream(""));
			String line;
			while((line = in.readLine()) != null) {
				if(line.indexOf("ZF <--") != -1 && FlagPatternCapture.matchTwice(line)) {
					boolean found = false;
					for(int i=0; i<zf_patterns.length; i++) {
						if(line.matches(zf_patterns[i])) {
							out_zf[i].println(line);
							found =true;
						}
					}
					if(!found) {
						out_zf_rest.println(line);
					}
				}
				else if(line.indexOf("CF <--") != -1 && FlagPatternCapture.matchTwice(line)) {
					boolean found = false;
					for(int i=0; i< cf_patterns.length; i++) {
						if(line.matches(cf_patterns[i])) {
							out_cf[i].println(line);
							found = true;
						}
					}
					if(! found) {
						out_cf_rest.println(line);
					}
				}
				else if(line.indexOf("SF <--") != -1 && FlagPatternCapture.matchTwice(line)) {
					boolean found = false;
					for(int i=0; i<sf_patterns.length; i++) {
						if(line.matches(sf_patterns[i])) {
							out_sf[i].println(line);
							found = true;
						}							
					}
					if(!found) {
						out_sf_rest.println(line);
					}
				}
				else if(line.indexOf("OF <--") != -1 && FlagPatternCapture.matchTwice(line)) {
					boolean found = false;
					for(int i=0; i<of_patterns.length; i++) {
						if(line.matches(of_patterns[i])) {
							out_of[i].println(line);
							found = true;
						}							
					}
					if(!found) {
						out_of_rest.println(line);
					}
				} else {
					continue;
				}
			}
			
			in.close();
			for(int i=0; i< zf_patterns.length; i++)
				out_zf[i].close();
			for(int i=0; i< cf_patterns.length; i++)
				out_cf[i].close();
			for(int i=0; i< sf_patterns.length; i++)
				out_sf[i].close();
			for(int i=0; i< of_patterns.length; i++)
				out_of[i].close();
			out_cf_rest.close();
			out_of_rest.close();
			out_zf_rest.close();
			out_sf_rest.close();
			
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}