package rtlanalysis;

import java.io.*;
import java.util.regex.*;

public class AlocGenHelper{
	private static String dataPattern="\\s+\\[\\s*\\d+\\]\\s+\\.data"+
	"\\s+[a-zA-Z]+\\s+([0-9a-fA-F]{8})\\s+([0-9a-fA-F]{6})\\s+([0-9a-fA-F]{6})[\\s\\S]*";
	private static String bssPattern="\\s+\\[\\s*\\d+\\]\\s+\\.bss"+
	"\\s+[a-zA-Z]+\\s+([0-9a-fA-F]{8})\\s+([0-9a-fA-F]{6})\\s+([0-9a-fA-F]{6})[\\s\\S]*";
	private static String rodataPattern="\\s+\\[\\s*\\d+\\]\\s+\\.rodata"+
	"\\s+[a-zA-Z]+\\s+([0-9a-fA-F]{8})\\s+([0-9a-fA-F]{6})\\s+([0-9a-fA-F]{6})[\\s\\S]*";

	private Section bss;
	private Section data;
	private Section rodata;
	
	public AlocGenHelper(String filePath) {
		try {
			Process ps = Runtime.getRuntime().exec("readelf -S "+filePath);
			int ptr = 0;
			BufferedInputStream in = new BufferedInputStream(ps.getInputStream());
			StringBuilder sb = new StringBuilder();
			while((ptr=in.read()) != -1 ){
				sb.append((char)ptr);
			}		
			System.out.println(sb.toString());
			
			String s;
			BufferedReader br=new BufferedReader(
					new InputStreamReader(
					new ByteArrayInputStream(sb.toString().getBytes())));
			
			
/*			if((s=br.readLine())!=null) {
				Pattern p=Pattern.compile("0x([0-9a-fA-F]+)");
				Matcher m=p.matcher(s);
				if(m.find()) {
					g_offset=Long.parseLong(m.group(1), 16);
				}
			}*/
			while((s=br.readLine())!=null) {
				if(s.trim().equals("Section Headers:")) {
					//System.out.println("meet section header line");
					break;
				}
			}
			if((s=br.readLine())==null) {
				br.close();
				return;
			}
			//Here header line "[Nr] Name Type ..." is omitted
			
			while((s=br.readLine())!=null) {
				Pattern p=Pattern.compile(AlocGenHelper.dataPattern);
				Matcher m=p.matcher(s);
				if(m.find()) {
					this.data=new Section(".data", m.group(1), m.group(2), m.group(3));
					continue;
				}
				p=Pattern.compile(AlocGenHelper.bssPattern);
				m=p.matcher(s);
				if(m.find()) {
					this.bss=new Section(".bss", m.group(1), m.group(2), m.group(3));
					continue;
				}
				p=Pattern.compile(AlocGenHelper.rodataPattern);
				m=p.matcher(s);
				if(m.find()) {
					this.rodata=new Section(".rodata", m.group(1), m.group(2), m.group(3));
					continue;
				}
			}
			
			//read the content of each section header from binary:
			File file=new File(filePath);
			BufferedInputStream is=new BufferedInputStream(new FileInputStream(file));
			is.mark((int)(file.length())+1);
			long skipped_bytes;
			long left_bytes;
			if(this.bss!=null && this.bss.size>0) {
				left_bytes = this.bss.offset;
				while(left_bytes > 0) {
					skipped_bytes = is.skip(left_bytes);
					left_bytes -= skipped_bytes;
				}
				is.read(this.bss.content, 0, this.bss.size);
				is.reset();
			}
			if(this.data!=null && this.data.size>0) {
				//is.skip(this.data.offset);
				left_bytes = this.data.offset;
				while(left_bytes > 0) {
					skipped_bytes = is.skip(left_bytes);
					left_bytes -= skipped_bytes;
				}
				is.read(this.data.content, 0, this.data.size);
				is.reset();
			}
			if(this.rodata!=null && this.rodata.size>0) {
				//is.skip(this.rodata.offset);
				left_bytes = this.rodata.offset;
				while(left_bytes > 0) {
					skipped_bytes = is.skip(left_bytes);
					left_bytes -= skipped_bytes;
				}
				is.read(this.rodata.content, 0, this.rodata.size);
				is.reset();
			}
			
			br.close();
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void print() {
		if(this.data!=null)
			this.data.print();
		if(this.bss!=null)
			this.bss.print();
		if(this.rodata!=null)
			this.rodata.print();
	}
	
	public void storeBoundaries(String dirPath) {
		String globalRgnPath=dirPath+"/global_rgn.facts";
		String SHInitValuePath=dirPath+"/section_init_value.facts";
		String boundedGlobalAddrPath = dirPath+"/bounded_global_addr_from_binary.facts";
		try {
			PrintStream out=new PrintStream(
									new BufferedOutputStream(
									new FileOutputStream(globalRgnPath)));
			PrintStream out2=new PrintStream(
									new BufferedOutputStream(
									new FileOutputStream(SHInitValuePath)));
			PrintStream out3=new PrintStream(
									new BufferedOutputStream(
									new FileOutputStream(boundedGlobalAddrPath)));
			if(this.data!=null) {
				out.print(".data\t" + this.data.address + "\t");
				out.println(this.data.upperBound);
				
				for(int idx=0;idx<this.data.content.length;idx++) {
					//size=1:
					if(this.data.content[idx]!=0) {
						out2.print(this.data.address+idx);
						out2.print("\t 1\t");
						out2.println(this.data.content[idx]);
					}
					//size=2:
					if(idx+1<this.data.content.length) {
						int wordvalue = this.getWordValue(this.data.content,idx);
						if(wordvalue != 0) {
							out2.print(this.data.address+idx);
							out2.print("\t 2\t");
							out2.println(wordvalue);
						}
					}
					//size=4:
					if(idx+3<this.data.content.length) {
						int dwordvalue = this.getDwordValue(this.data.content,idx);
						if(dwordvalue != 0) {
							out2.print(this.data.address+idx);
							out2.print("\t 4\t");
							out2.println(dwordvalue);
						}
					}
				}
				
				for(int i=0;i< this.data.content.length;i++) {
					out3.println(this.data.address+i);
				}
				
			}
			if(this.rodata!=null) {
				out.print(".rodata\t" + this.rodata.address + "\t");
				out.println(this.rodata.upperBound);
				for(int idx=0;idx<this.rodata.content.length;idx++) {
					//size=1:
					out2.print(this.rodata.address+idx);
					out2.print("\t 1\t");
					out2.println(this.rodata.content[idx]);
					//size=2:
					if(idx+1<this.rodata.content.length) {
						out2.print(this.rodata.address+idx);
						out2.print("\t 2\t");
						out2.println(this.getWordValue(this.rodata.content,idx));
					}
					//size=4:
					if(idx+3<this.rodata.content.length) {
						out2.print(this.rodata.address+idx);
						out2.print("\t 4\t");
						out2.println(this.getDwordValue(this.rodata.content,idx));
					}
				}
				 
				for(int i=0;i< this.rodata.content.length;i++) {
					out3.println(this.rodata.address+i);
				}				
			}
			out2.close();
			
			if(this.bss!=null) {
				out.print(".bss\t" + this.bss.address + "\t");
				out.println(this.bss.upperBound);
				
				for(int i=0;i<this.bss.content.length;i++) {
					out3.println(this.bss.address+i);
				}
			}
			out.close();
			out3.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private int getWordValue(byte[] b, int idx) {
		//return (b[idx]<<8 & 0xFF00) | (b[idx+1] & 0xFF);
		return (b[idx+1]<<8 & 0xFF00) | (b[idx] & 0xFF);
	}

	private int getDwordValue(byte[] b, int idx) {
		/*return ((0xFF & b[idx]) << 24) | 
			   ((0xFF & b[idx+1]) << 16) |
	           ((0xFF & b[idx+2]) << 8) | 
	            (0xFF & b[idx+3]);*/
		return ((0xFF & b[idx+3]) << 24) | 
				   ((0xFF & b[idx+2]) << 16) |
		           ((0xFF & b[idx+1]) << 8) | 
		            (0xFF & b[idx]);
	}
	
	public static void main(String[] args) {
		if(args==null || args.length!=2) {
			System.out.println("Usage:");
			System.out.println("java -jar AlocGenHelper.jar [path of binary] [destination directory of section facts]");
			return;
		}
		AlocGenHelper ag=new AlocGenHelper(args[0]);
		//ag.print();
		ag.storeBoundaries(args[1]);
	}

/*	private long getOffset(String nm) {
		if(this.bss!=null && this.bss.name.equals(nm))
			return this.bss.offset;
		if(this.data!=null && this.data.name.equals(nm))
			return this.data.offset;
		if(this.rodata!=null && this.rodata.name.equals(nm))
			return this.rodata.offset;
		return -1;
	}
	
	private int getSize(String nm) {
		if(this.bss!=null && this.bss.name.equals(nm))
			return this.bss.size;
		if(this.data!=null && this.data.name.equals(nm))
			return this.data.size;
		if(this.rodata!=null && this.rodata.name.equals(nm))
			return this.rodata.size;
		return 0;
	}*/
	
	private class Section{
		long address;
		long offset;
		int size;
		String name="";
		byte []content;
		long upperBound;
		
		Section(String nm, String addr, String off, String sz){
			this.name=nm;
			this.address=Long.parseLong(addr, 16);
			this.offset=Long.parseLong(off, 16);
			this.size=Integer.parseInt(sz, 16);
			this.content=new byte[this.size];
			this.upperBound=this.address+this.size-1;
		}
		
		void print() {
			System.out.printf("<%s, [%d,%d], %d, %d,\n[",
					this.name, this.address, this.upperBound, this.offset, this.size);
			for(int i=0;i<this.content.length-1;i++) {
				System.out.printf("%02x,",this.content[i]);
				if((i+1)%16==0)
					System.out.println();
			}
			if(this.content.length>0)
				System.out.printf("%x",this.content[this.content.length-1]);
			System.out.println("]>");
		}
	}
}