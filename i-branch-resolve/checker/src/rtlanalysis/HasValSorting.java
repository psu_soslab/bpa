package rtlanalysis;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class HasValSorting{
	private static String linePattern="(\\d+)\\s+(\\d+)[\\s\\S]+";
	private static String datePattern="\\d+-\\d+";
	
	Set<Triple> result_set=new TreeSet<Triple>();
	
	public static void main(String[] args) {
		if(args==null || args.length!=2) {
			System.out.println("Usage:");
			System.out.println("java -jar HasValSorting.jar [path of source has_val]"
					+ " [destination directory of sorted has_val]");
			return;
		}
		try {
			HasValSorting hvs=new HasValSorting();
			
			
			//extract the directory of has_val file
			int last_idx = args[0].lastIndexOf("/");
			if(last_idx == -1)
				return;
			String dir=args[0].substring(0, last_idx);
			
			//extract the time-stamp from the name of the has_val file
			last_idx = args[1].lastIndexOf("/");
			String has_val_file_name = args[1].substring(last_idx);
			Pattern pattern = Pattern.compile(HasValSorting.datePattern);
			Matcher matcher = pattern.matcher(has_val_file_name);
			String date="";
			if(matcher.find())
				date=matcher.group();
			
			File oldfile = new File(dir+"/edge_extended.facts");
			File newfile = new File(dir+"/edge_extended_"+ date + ".facts");

			if (newfile.exists())
				throw new java.io.IOException("edge_extended_[time_stamp] file exists. Renaming failed");

			oldfile.renameTo(newfile);
			
			oldfile = new File(dir+"/flow_reduced.facts");
			newfile = new File(dir+"/flow_reduced_"+ date + ".facts");
			
			if (newfile.exists())
				throw new java.io.IOException("flow_reduced_[time_stamp] file exists. Renaming failed");

			oldfile.renameTo(newfile);
			
			oldfile = new File(dir+"/indirect_jmp_target.csv");
			newfile = new File(dir+"/indirect_jmp_target_"+ date + ".csv");
			
			if (newfile.exists())
				throw new java.io.IOException("indirect_jmp_target_[time_stamp] file exists. Renaming failed");

			oldfile.renameTo(newfile);
			
			
			File file =new File(args[0]);
			double megabytes = (double)file.length() / (1024*1024);
			if(megabytes > 200) { // size of has_val file > 200MB
				System.out.println("The size of has_val file > 200MB. Fail to sort it.");
				return;
			}
			
			BufferedReader br=new BufferedReader(
						new InputStreamReader(
						new FileInputStream(args[0])));
			
			String line;
			while((line=br.readLine())!=null) {
				Pattern p=Pattern.compile(HasValSorting.linePattern);
				Matcher m=p.matcher(line);
				if(m.matches()) {
					//System.out.println(m.group(1)+";"+m.group(2));
					hvs.result_set.add( hvs.new Triple(Integer.parseInt(m.group(1)), 
									Integer.parseInt(m.group(2)), line) );
				}
			}
			
			PrintStream out=new PrintStream(new FileOutputStream(args[1]));
			Iterator<Triple> it=hvs.result_set.iterator();
			while(it.hasNext()) {
				//System.out.println(it.next().line);
				out.println(it.next().line);
			}
			
			br.close();
			out.close();

			
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	private class Triple implements Comparable<Triple>{
		private int addr;
		private int order;
		private String line;
		
		Triple(int ad, int ord, String s){
			this.addr=ad;
			this.order=ord;
			this.line=s;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + addr;
			result = prime * result + order;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Triple other = (Triple) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (addr != other.addr)
				return false;
			if (order != other.order)
				return false;
			return true;
		}
		
		@Override
		public int compareTo(Triple o) {
			if ((this.addr<o.addr)
			|| (this.addr==o.addr && this.order<o.order) 
			|| (this.addr==o.addr && this.order==o.order &&
				this.line.compareTo(o.line) < 0) ){
				return -1;
			} else if(this.addr==o.addr && this.order==o.order 
					&& this.line.equals(o.line)) {
				return 0;
			} else
				return 1;
		}
		
		private HasValSorting getOuterType() {
			return HasValSorting.this;
		}
	}
}