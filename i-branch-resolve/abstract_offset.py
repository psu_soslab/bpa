import sys, getopt
import argparse
import os


def bounded_offset(b):
    f = open("bounded_offset.facts", 'w')
    for i in range(-b, b):
        f.write(str(i) + "\n")
    f.write(str(b))
    f.close()

def bound_lower(b):
    f = open("bound_lower.facts", 'w')
    f.write(str(-b - 1))
    f.close()

def bound_upper(b):
    f = open("bound_upper.facts", 'w')
    f.write(str(b + 1))
    f.close()

def top_encoding(b):
    f = open("top_encoding.facts", 'w')
    f.write(str(b + 2))
    f.close()

def order_conn():
    f = open("order_conn.facts", 'w')
    for i in range(22):
        f.write(str(i) + "\t" + str(i + 1) + "\n")
    f.close()

def gen_all(b):
    b = int(b)
    bounded_offset(b)
    bound_lower(b)
    bound_upper(b)
    top_encoding(b)
    order_conn()


def main():

    parser = argparse.ArgumentParser(description='Process arguments')
    parser.add_argument('-b','--b', help='parameter value', default = 16)

    args = parser.parse_args()
    b = int(str(args.b))
    bounded_offset(b)
    bound_lower(b)
    bound_upper(b)
    top_encoding(b)
    order_conn()
if __name__ == '__main__':
    main()
