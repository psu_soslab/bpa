# README #
* This repository contains the project for BPA (NDSS'21)

# Set-up #
* The whole repository (project) has been tested on **Ubuntu 18.04** (x64-bit)
* Follow **0** - **2** below

## 0. Git clone the project ##

* Do git clone

## 1. Download libdwarf-dev ##

* Do ```sudo apt-get install libelf-dev libdwarf-dev```


## 2. Souffle (block-based points-to analysis) ##


* Install Souffle (Recommend using most recent version (v2.0.2) of Souffle): https://souffle-lang.github.io/download.html
* Install either Python 2 or 3.

# Usage - running the provided testcase #
* Usage: 

&nbsp;&nbsp;&nbsp;&nbsp; 1) Do ```cd bpa/i-branch-resolve/tests/kim-ijump-toy-gcc-O0-inter/```
 
 &nbsp;&nbsp;&nbsp;&nbsp; 2) Note that there one testcase directory in the directory above (```test1```).


 &nbsp;&nbsp;&nbsp;&nbsp; 2.1) To run test1, do: 
 &nbsp; ```python run.py -t 4 -d test1```  


* Doing so above will run **BPA**

* For more help, please do ```python run.py -h```

* Please note that ```-d``` represents the input directory, and ```-t``` represents # of threads (cores) to use. 
  If your machine has 4 cores only, ```-t``` with 4 or less will be good.

# Important inputs (testcases)  #

* In the directory ```test1/src```, you can see: 

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;```test1.c``` - the input's source code
  
  
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;```test1_gcc``` - the binary file of the input ```test1.c```
  
  
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;```test1_gcc.asm``` - the assembly code for the input


# Important output & Explanations  #
All Datalog predicate's outputs will be stored in ```test1/facts``` directory. 

**BPA: One major predicate (output):**

* ```indirect_call_target.csv``` 

&nbsp;&nbsp; - BPA's detected indirect call target results


* Note that in the ```BPA/i-branch-resolve/tests/kim-ijump-toy-gcc-O0-inter``` directory, there are three python scripts:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ```int_to_hex.py``` - this script 
converts brief decimal format number to non-brief hex format.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
e.g) ```python int_to_hex.py 1000``` will output ```80483e8```.


&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ```hex_to_int.py``` - opposite script of ```int_to_hex.py```.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
e.g) ```python hex_to_int.py 80483e8``` will output ```1000```.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - ```big_int_to_hex.py``` - this script 
converts non-brief decimal format number to non-brief hex format.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
e.g) ```python big_int_to_hex.py 134512640``` will output ```8048000```.


# Usage - running your own testcases #

* If you want to run your own testcase, create a directory (let's call it test2) : 
```test2``` under the directory 
```BPA/i-branch-resolve/tests/kim-ijump-toy-gcc-O0-inter```, 
and then create ```src``` and ```facts``` directories in ```test2``` directory.
* After that, name your binary file as ```test2_gcc``` (you add ```_gcc``` to the test directory name), 
and then also create ```test2_gcc.its``` file, 
and put them under the ```src``` directory you created. 

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Note that you can use  ```Makefile``` and ```reloc_based_targets.py``` 
(can be found under the directory: ```BPA/i-branch-resolve/tests/kim-ijump-toy-gcc-O0-inter/test1/src```) 
files to create the binary file and ```.its``` file, based on your C (source code) test program. 
For example, once you have a C program (BPA currently only supports C binaries, not C++) named ```test.c```, if you do ```make T=test```, Makefile will run correctly.
Feel free to modify the optimization level (O0 to O3) in ```Makefile``` if wanted.

* Once you create your own testcase directory, 
along with the binary file and ```.its``` file located as the instructions explained above,
you can run your own testcase, following the same procedure as explained above 
(**Usage - running the provided testcase**)

* The disassembler BPA is relying on (RockSalt) does not support PIE binaries (especially gcc-7 or higher). 
To make sure to compile the binary to be compatiable with RockSalt, use the provided ```Makefile``` mentioned above.
Also, this disassembler takes non-stripped binary to provide complete disassembled code for BPA, to provide
better baseline for points-to analysis.

* Some x86 instructions (e.g. pause instruction, floating-point computation instructions) 
are not supported by RockSalt (due to the absence of decoding for such instructions). 
In general, these instructions do not affect BPA's results. 
If decoding error occurs, manually modify the binaries to change these instructions to no-op instruction.


# Important source code #

* ```bpa/i-branch-resolve/logic/souffle/block-pointsto-interproc-toTrack-increment/general-points-to-baseline.dl```
is the main driver file you may want to start looking at, when you want to look into BPA's source code deeper. 